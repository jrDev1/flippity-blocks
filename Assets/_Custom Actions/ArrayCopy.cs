﻿// (c) Copyright HutongGames, LLC 2010-2014. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Array)]
	[Tooltip("Copy values of one array to another array.")]
	public class ArrayCopy : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The Array Variable to use.")]
		public FsmArray array;
		
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[Tooltip("The array to store")]
		public FsmArray storeArray;
		
		public override void Reset()
		{
			array = null;
			storeArray = null;
		}
		
		public override void OnEnter()
		{
			storeArray.Values = array.Values;
			
			Finish();
		}

	}
}
