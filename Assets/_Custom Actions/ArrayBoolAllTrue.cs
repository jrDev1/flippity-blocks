﻿// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Logic)]
	[Tooltip("Tests if all the given Bool Variables are True.")]
	public class ArrayBoolAllTrue : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.Variable)]
		[ArrayEditor(VariableType.Bool)]
		[Tooltip("The Bool variables to check.")]
		public FsmArray boolArray;
		
		[Tooltip("Event to send if all the Bool variables are True.")]
		public FsmEvent sendEvent;
		
		[UIHint(UIHint.Variable)]
		[Tooltip("Store the result in a Bool variable.")]
		public FsmBool storeResult;
		
		[Tooltip("Repeat every frame while the state is active.")]
		public bool everyFrame;
		
		public override void Reset()
		{
			boolArray = null;
			sendEvent = null;
			storeResult = null;
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			DoAllTrue();
			
			if (!everyFrame)
			{
				Finish();
			}		
		}
		
		public override void OnUpdate()
		{
			DoAllTrue();
		}
		
		void DoAllTrue()
		{
			if (boolArray.Values.Length == 0) return;
			
			var allTrue = true;
			
			for (var i = 0; i < boolArray.Values.Length; i++) 
			{
				bool theBool = (bool)boolArray.Values[i];
				
				if (!theBool)
				{
					allTrue = false;
					break;
				}
			}
			
			if (allTrue)
			{
				Fsm.Event(sendEvent);
			}
			
			storeResult.Value = allTrue;
		}
	}
}