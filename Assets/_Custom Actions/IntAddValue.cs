﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Math)]
	[Tooltip("Adds a value to an Integer Variable and store value.")]
	public class IntAddValue : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmInt intVariable;
		[RequiredField]
		public FsmInt add;
		public FsmInt intValue;
		public bool everyFrame;
		
		public override void Reset()
		{
			intVariable = null;
			add = null;
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			intValue.Value = intVariable.Value + add.Value;
			
			if (!everyFrame)
				Finish();
		}
		
		// NOTE: very frame rate dependent!
		public override void OnUpdate()
		{
			if (everyFrame)
				intValue.Value = intVariable.Value + add.Value;
			
		}
	}
}
