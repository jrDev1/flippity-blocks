﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.Math)]
	[Tooltip("Round all Vector3 axis to the specified nearest value.")]
	public class Vector3RoundToNearest : FsmStateAction
	{
		[RequiredField]
		[UIHint(UIHint.Variable)]
		public FsmVector3 vector3Variable;
		
		public FsmFloat nearest;

		[UIHint(UIHint.Variable)]
		public FsmVector3 resultAsVector3;
		
		public bool everyFrame;
		
		public override void Reset()
		{
			vector3Variable = null;
			nearest = 10;
			resultAsVector3 = null;
			
			everyFrame = false;
		}
		
		public override void OnEnter()
		{
			DoFloatRound();
			
			if (!everyFrame)
				Finish();
		}
		
		public override void OnUpdate()
		{
			DoFloatRound();
		}
		
		void DoFloatRound()
		{
			resultAsVector3.Value = new Vector3 (Mathf.Round (vector3Variable.Value.x / nearest.Value) * nearest.Value, Mathf.Round (vector3Variable.Value.y / nearest.Value) * nearest.Value, Mathf.Round (vector3Variable.Value.z / nearest.Value) * nearest.Value);

		}
	}
}
