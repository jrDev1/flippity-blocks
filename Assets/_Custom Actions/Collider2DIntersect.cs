﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions{
	
	[ActionCategory("Physics2D")]
	[Tooltip("Send event if 2D Collider Intersects with another 2D Collider")]
	public class Collider2DInstersect : FsmStateAction{
		
		[UIHint(UIHint.Variable)]
		public FsmOwnerDefault gameObject;
		
		[UIHint(UIHint.Variable)]
		public FsmGameObject target;
		
		public FsmBool insideCollider;
		public FsmEvent insideEvent;
		public FsmEvent outsideEvent;
		
		public override void Reset(){
			
			target = null;
			gameObject = null;
			insideCollider = null;
			insideEvent = null;
			outsideEvent =null;
			
		}

		public override void OnEnter(){
			
			var go = Fsm.GetOwnerDefaultTarget(gameObject);
			
			if (go == null){
				
				Finish();
			}
			
			Collider2D goCollider = go.GetComponent<Collider2D>();
			Collider2D targetCollider =  target.Value.GetComponent<Collider2D>();
			
			Bounds goBounds = goCollider.bounds;
			Bounds targetBounds = targetCollider.bounds;
			
			insideCollider.Value = targetBounds.Intersects(goBounds);
			
			if (insideCollider.Value ==  true){
				
				Fsm.Event(insideEvent);
			}
			
			if (insideCollider.Value ==  false){
				
				Fsm.Event(outsideEvent);
				
			}else{
				
				Finish();
				
			}
			
		}
		
	}
	
}
