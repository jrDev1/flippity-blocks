﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// SRDebugger.UI.Other.BugReportSheetController
struct BugReportSheetController_t1864182511;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.BugReportTabController
struct  BugReportTabController_t3310766069  : public SRMonoBehaviourEx_t3120278648
{
public:
	// SRDebugger.UI.Other.BugReportSheetController SRDebugger.UI.Tabs.BugReportTabController::BugReportSheetPrefab
	BugReportSheetController_t1864182511 * ___BugReportSheetPrefab_9;
	// UnityEngine.RectTransform SRDebugger.UI.Tabs.BugReportTabController::Container
	RectTransform_t3349966182 * ___Container_10;

public:
	inline static int32_t get_offset_of_BugReportSheetPrefab_9() { return static_cast<int32_t>(offsetof(BugReportTabController_t3310766069, ___BugReportSheetPrefab_9)); }
	inline BugReportSheetController_t1864182511 * get_BugReportSheetPrefab_9() const { return ___BugReportSheetPrefab_9; }
	inline BugReportSheetController_t1864182511 ** get_address_of_BugReportSheetPrefab_9() { return &___BugReportSheetPrefab_9; }
	inline void set_BugReportSheetPrefab_9(BugReportSheetController_t1864182511 * value)
	{
		___BugReportSheetPrefab_9 = value;
		Il2CppCodeGenWriteBarrier(&___BugReportSheetPrefab_9, value);
	}

	inline static int32_t get_offset_of_Container_10() { return static_cast<int32_t>(offsetof(BugReportTabController_t3310766069, ___Container_10)); }
	inline RectTransform_t3349966182 * get_Container_10() const { return ___Container_10; }
	inline RectTransform_t3349966182 ** get_address_of_Container_10() { return &___Container_10; }
	inline void set_Container_10(RectTransform_t3349966182 * value)
	{
		___Container_10 = value;
		Il2CppCodeGenWriteBarrier(&___Container_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
