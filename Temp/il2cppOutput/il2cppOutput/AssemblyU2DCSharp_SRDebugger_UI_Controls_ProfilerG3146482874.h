﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerG3712983251.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"

// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// SRDebugger.Services.IProfilerService
struct IProfilerService_t1347272725;
// SRDebugger.UI.Controls.ProfilerGraphAxisLabel[]
struct ProfilerGraphAxisLabelU5BU5D_t3611503679;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ProfilerGraphControl
struct  ProfilerGraphControl_t3146482874  : public Graphic_t2426225576
{
public:
	// SRDebugger.UI.Controls.ProfilerGraphControl/VerticalAlignments SRDebugger.UI.Controls.ProfilerGraphControl::VerticalAlignment
	int32_t ___VerticalAlignment_19;
	// System.Boolean SRDebugger.UI.Controls.ProfilerGraphControl::FloatingScale
	bool ___FloatingScale_21;
	// System.Boolean SRDebugger.UI.Controls.ProfilerGraphControl::TargetFpsUseApplication
	bool ___TargetFpsUseApplication_22;
	// System.Boolean SRDebugger.UI.Controls.ProfilerGraphControl::DrawAxes
	bool ___DrawAxes_23;
	// System.Int32 SRDebugger.UI.Controls.ProfilerGraphControl::TargetFps
	int32_t ___TargetFps_24;
	// System.Boolean SRDebugger.UI.Controls.ProfilerGraphControl::Clip
	bool ___Clip_25;
	// System.Int32 SRDebugger.UI.Controls.ProfilerGraphControl::VerticalPadding
	int32_t ___VerticalPadding_29;
	// UnityEngine.Color[] SRDebugger.UI.Controls.ProfilerGraphControl::LineColours
	ColorU5BU5D_t672350442* ___LineColours_31;
	// SRDebugger.Services.IProfilerService SRDebugger.UI.Controls.ProfilerGraphControl::_profilerService
	Il2CppObject * ____profilerService_32;
	// SRDebugger.UI.Controls.ProfilerGraphAxisLabel[] SRDebugger.UI.Controls.ProfilerGraphControl::_axisLabels
	ProfilerGraphAxisLabelU5BU5D_t3611503679* ____axisLabels_33;
	// UnityEngine.Rect SRDebugger.UI.Controls.ProfilerGraphControl::_clipBounds
	Rect_t3681755626  ____clipBounds_34;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> SRDebugger.UI.Controls.ProfilerGraphControl::_meshVertices
	List_1_t1612828712 * ____meshVertices_35;
	// System.Collections.Generic.List`1<UnityEngine.Color32> SRDebugger.UI.Controls.ProfilerGraphControl::_meshVertexColors
	List_1_t243638650 * ____meshVertexColors_36;
	// System.Collections.Generic.List`1<System.Int32> SRDebugger.UI.Controls.ProfilerGraphControl::_meshTriangles
	List_1_t1440998580 * ____meshTriangles_37;

public:
	inline static int32_t get_offset_of_VerticalAlignment_19() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___VerticalAlignment_19)); }
	inline int32_t get_VerticalAlignment_19() const { return ___VerticalAlignment_19; }
	inline int32_t* get_address_of_VerticalAlignment_19() { return &___VerticalAlignment_19; }
	inline void set_VerticalAlignment_19(int32_t value)
	{
		___VerticalAlignment_19 = value;
	}

	inline static int32_t get_offset_of_FloatingScale_21() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___FloatingScale_21)); }
	inline bool get_FloatingScale_21() const { return ___FloatingScale_21; }
	inline bool* get_address_of_FloatingScale_21() { return &___FloatingScale_21; }
	inline void set_FloatingScale_21(bool value)
	{
		___FloatingScale_21 = value;
	}

	inline static int32_t get_offset_of_TargetFpsUseApplication_22() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___TargetFpsUseApplication_22)); }
	inline bool get_TargetFpsUseApplication_22() const { return ___TargetFpsUseApplication_22; }
	inline bool* get_address_of_TargetFpsUseApplication_22() { return &___TargetFpsUseApplication_22; }
	inline void set_TargetFpsUseApplication_22(bool value)
	{
		___TargetFpsUseApplication_22 = value;
	}

	inline static int32_t get_offset_of_DrawAxes_23() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___DrawAxes_23)); }
	inline bool get_DrawAxes_23() const { return ___DrawAxes_23; }
	inline bool* get_address_of_DrawAxes_23() { return &___DrawAxes_23; }
	inline void set_DrawAxes_23(bool value)
	{
		___DrawAxes_23 = value;
	}

	inline static int32_t get_offset_of_TargetFps_24() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___TargetFps_24)); }
	inline int32_t get_TargetFps_24() const { return ___TargetFps_24; }
	inline int32_t* get_address_of_TargetFps_24() { return &___TargetFps_24; }
	inline void set_TargetFps_24(int32_t value)
	{
		___TargetFps_24 = value;
	}

	inline static int32_t get_offset_of_Clip_25() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___Clip_25)); }
	inline bool get_Clip_25() const { return ___Clip_25; }
	inline bool* get_address_of_Clip_25() { return &___Clip_25; }
	inline void set_Clip_25(bool value)
	{
		___Clip_25 = value;
	}

	inline static int32_t get_offset_of_VerticalPadding_29() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___VerticalPadding_29)); }
	inline int32_t get_VerticalPadding_29() const { return ___VerticalPadding_29; }
	inline int32_t* get_address_of_VerticalPadding_29() { return &___VerticalPadding_29; }
	inline void set_VerticalPadding_29(int32_t value)
	{
		___VerticalPadding_29 = value;
	}

	inline static int32_t get_offset_of_LineColours_31() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ___LineColours_31)); }
	inline ColorU5BU5D_t672350442* get_LineColours_31() const { return ___LineColours_31; }
	inline ColorU5BU5D_t672350442** get_address_of_LineColours_31() { return &___LineColours_31; }
	inline void set_LineColours_31(ColorU5BU5D_t672350442* value)
	{
		___LineColours_31 = value;
		Il2CppCodeGenWriteBarrier(&___LineColours_31, value);
	}

	inline static int32_t get_offset_of__profilerService_32() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ____profilerService_32)); }
	inline Il2CppObject * get__profilerService_32() const { return ____profilerService_32; }
	inline Il2CppObject ** get_address_of__profilerService_32() { return &____profilerService_32; }
	inline void set__profilerService_32(Il2CppObject * value)
	{
		____profilerService_32 = value;
		Il2CppCodeGenWriteBarrier(&____profilerService_32, value);
	}

	inline static int32_t get_offset_of__axisLabels_33() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ____axisLabels_33)); }
	inline ProfilerGraphAxisLabelU5BU5D_t3611503679* get__axisLabels_33() const { return ____axisLabels_33; }
	inline ProfilerGraphAxisLabelU5BU5D_t3611503679** get_address_of__axisLabels_33() { return &____axisLabels_33; }
	inline void set__axisLabels_33(ProfilerGraphAxisLabelU5BU5D_t3611503679* value)
	{
		____axisLabels_33 = value;
		Il2CppCodeGenWriteBarrier(&____axisLabels_33, value);
	}

	inline static int32_t get_offset_of__clipBounds_34() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ____clipBounds_34)); }
	inline Rect_t3681755626  get__clipBounds_34() const { return ____clipBounds_34; }
	inline Rect_t3681755626 * get_address_of__clipBounds_34() { return &____clipBounds_34; }
	inline void set__clipBounds_34(Rect_t3681755626  value)
	{
		____clipBounds_34 = value;
	}

	inline static int32_t get_offset_of__meshVertices_35() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ____meshVertices_35)); }
	inline List_1_t1612828712 * get__meshVertices_35() const { return ____meshVertices_35; }
	inline List_1_t1612828712 ** get_address_of__meshVertices_35() { return &____meshVertices_35; }
	inline void set__meshVertices_35(List_1_t1612828712 * value)
	{
		____meshVertices_35 = value;
		Il2CppCodeGenWriteBarrier(&____meshVertices_35, value);
	}

	inline static int32_t get_offset_of__meshVertexColors_36() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ____meshVertexColors_36)); }
	inline List_1_t243638650 * get__meshVertexColors_36() const { return ____meshVertexColors_36; }
	inline List_1_t243638650 ** get_address_of__meshVertexColors_36() { return &____meshVertexColors_36; }
	inline void set__meshVertexColors_36(List_1_t243638650 * value)
	{
		____meshVertexColors_36 = value;
		Il2CppCodeGenWriteBarrier(&____meshVertexColors_36, value);
	}

	inline static int32_t get_offset_of__meshTriangles_37() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874, ____meshTriangles_37)); }
	inline List_1_t1440998580 * get__meshTriangles_37() const { return ____meshTriangles_37; }
	inline List_1_t1440998580 ** get_address_of__meshTriangles_37() { return &____meshTriangles_37; }
	inline void set__meshTriangles_37(List_1_t1440998580 * value)
	{
		____meshTriangles_37 = value;
		Il2CppCodeGenWriteBarrier(&____meshTriangles_37, value);
	}
};

struct ProfilerGraphControl_t3146482874_StaticFields
{
public:
	// System.Single[] SRDebugger.UI.Controls.ProfilerGraphControl::ScaleSteps
	SingleU5BU5D_t577127397* ___ScaleSteps_20;

public:
	inline static int32_t get_offset_of_ScaleSteps_20() { return static_cast<int32_t>(offsetof(ProfilerGraphControl_t3146482874_StaticFields, ___ScaleSteps_20)); }
	inline SingleU5BU5D_t577127397* get_ScaleSteps_20() const { return ___ScaleSteps_20; }
	inline SingleU5BU5D_t577127397** get_address_of_ScaleSteps_20() { return &___ScaleSteps_20; }
	inline void set_ScaleSteps_20(SingleU5BU5D_t577127397* value)
	{
		___ScaleSteps_20 = value;
		Il2CppCodeGenWriteBarrier(&___ScaleSteps_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
