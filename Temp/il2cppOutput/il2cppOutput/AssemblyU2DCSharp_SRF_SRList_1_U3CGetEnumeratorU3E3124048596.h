﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRF.SRList`1<SRDebugger.Profiler.ProfilerCameraListener>
struct SRList_1_t4287523865;
// SRDebugger.Profiler.ProfilerCameraListener
struct ProfilerCameraListener_t4294893284;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRList`1/<GetEnumerator>c__Iterator0<SRDebugger.Profiler.ProfilerCameraListener>
struct  U3CGetEnumeratorU3Ec__Iterator0_t3124048596  : public Il2CppObject
{
public:
	// System.Int32 SRF.SRList`1/<GetEnumerator>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// SRF.SRList`1<T> SRF.SRList`1/<GetEnumerator>c__Iterator0::$this
	SRList_1_t4287523865 * ___U24this_1;
	// T SRF.SRList`1/<GetEnumerator>c__Iterator0::$current
	ProfilerCameraListener_t4294893284 * ___U24current_2;
	// System.Boolean SRF.SRList`1/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SRF.SRList`1/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3124048596, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3124048596, ___U24this_1)); }
	inline SRList_1_t4287523865 * get_U24this_1() const { return ___U24this_1; }
	inline SRList_1_t4287523865 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SRList_1_t4287523865 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3124048596, ___U24current_2)); }
	inline ProfilerCameraListener_t4294893284 * get_U24current_2() const { return ___U24current_2; }
	inline ProfilerCameraListener_t4294893284 ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(ProfilerCameraListener_t4294893284 * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3124048596, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3124048596, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
