﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "AssemblyU2DCSharp_SRDebugger_DefaultTabs4204632053.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_TriggerEnabl3150100755.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_TriggerBehavi698320438.h"
#include "AssemblyU2DCSharp_SRDebugger_PinAlignment3550534810.h"
#include "AssemblyU2DCSharp_SRDebugger_ConsoleAlignment2755495440.h"

// System.String
struct String_t;
// SRDebugger.Settings
struct Settings_t1299540155;
// SRDebugger.Settings/KeyboardShortcut[]
struct KeyboardShortcutU5BU5D_t3674246146;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// SRDebugger.DefaultTabs[]
struct DefaultTabsU5BU5D_t1346111640;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t3377395111;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Settings
struct  Settings_t1299540155  : public ScriptableObject_t1975622470
{
public:
	// System.Boolean SRDebugger.Settings::_isEnabled
	bool ____isEnabled_5;
	// System.Boolean SRDebugger.Settings::_autoLoad
	bool ____autoLoad_6;
	// SRDebugger.DefaultTabs SRDebugger.Settings::_defaultTab
	int32_t ____defaultTab_7;
	// SRDebugger.Settings/TriggerEnableModes SRDebugger.Settings::_triggerEnableMode
	int32_t ____triggerEnableMode_8;
	// SRDebugger.Settings/TriggerBehaviours SRDebugger.Settings::_triggerBehaviour
	int32_t ____triggerBehaviour_9;
	// System.Boolean SRDebugger.Settings::_enableKeyboardShortcuts
	bool ____enableKeyboardShortcuts_10;
	// SRDebugger.Settings/KeyboardShortcut[] SRDebugger.Settings::_keyboardShortcuts
	KeyboardShortcutU5BU5D_t3674246146* ____keyboardShortcuts_11;
	// SRDebugger.Settings/KeyboardShortcut[] SRDebugger.Settings::_newKeyboardShortcuts
	KeyboardShortcutU5BU5D_t3674246146* ____newKeyboardShortcuts_12;
	// System.Boolean SRDebugger.Settings::_keyboardModifierControl
	bool ____keyboardModifierControl_13;
	// System.Boolean SRDebugger.Settings::_keyboardModifierAlt
	bool ____keyboardModifierAlt_14;
	// System.Boolean SRDebugger.Settings::_keyboardModifierShift
	bool ____keyboardModifierShift_15;
	// System.Boolean SRDebugger.Settings::_keyboardEscapeClose
	bool ____keyboardEscapeClose_16;
	// System.Boolean SRDebugger.Settings::_enableBackgroundTransparency
	bool ____enableBackgroundTransparency_17;
	// System.Boolean SRDebugger.Settings::_collapseDuplicateLogEntries
	bool ____collapseDuplicateLogEntries_18;
	// System.Boolean SRDebugger.Settings::_richTextInConsole
	bool ____richTextInConsole_19;
	// System.Boolean SRDebugger.Settings::_requireEntryCode
	bool ____requireEntryCode_20;
	// System.Boolean SRDebugger.Settings::_requireEntryCodeEveryTime
	bool ____requireEntryCodeEveryTime_21;
	// System.Int32[] SRDebugger.Settings::_entryCode
	Int32U5BU5D_t3030399641* ____entryCode_22;
	// System.Boolean SRDebugger.Settings::_useDebugCamera
	bool ____useDebugCamera_23;
	// System.Int32 SRDebugger.Settings::_debugLayer
	int32_t ____debugLayer_24;
	// System.Single SRDebugger.Settings::_debugCameraDepth
	float ____debugCameraDepth_25;
	// System.String SRDebugger.Settings::_apiKey
	String_t* ____apiKey_26;
	// System.Boolean SRDebugger.Settings::_enableBugReporter
	bool ____enableBugReporter_27;
	// SRDebugger.DefaultTabs[] SRDebugger.Settings::_disabledTabs
	DefaultTabsU5BU5D_t1346111640* ____disabledTabs_28;
	// SRDebugger.PinAlignment SRDebugger.Settings::_profilerAlignment
	int32_t ____profilerAlignment_29;
	// SRDebugger.PinAlignment SRDebugger.Settings::_optionsAlignment
	int32_t ____optionsAlignment_30;
	// SRDebugger.ConsoleAlignment SRDebugger.Settings::_consoleAlignment
	int32_t ____consoleAlignment_31;
	// SRDebugger.PinAlignment SRDebugger.Settings::_triggerPosition
	int32_t ____triggerPosition_32;
	// System.Int32 SRDebugger.Settings::_maximumConsoleEntries
	int32_t ____maximumConsoleEntries_33;
	// System.Boolean SRDebugger.Settings::_enableEventSystemCreation
	bool ____enableEventSystemCreation_34;
	// System.Boolean SRDebugger.Settings::_automaticShowCursor
	bool ____automaticShowCursor_35;

public:
	inline static int32_t get_offset_of__isEnabled_5() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____isEnabled_5)); }
	inline bool get__isEnabled_5() const { return ____isEnabled_5; }
	inline bool* get_address_of__isEnabled_5() { return &____isEnabled_5; }
	inline void set__isEnabled_5(bool value)
	{
		____isEnabled_5 = value;
	}

	inline static int32_t get_offset_of__autoLoad_6() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____autoLoad_6)); }
	inline bool get__autoLoad_6() const { return ____autoLoad_6; }
	inline bool* get_address_of__autoLoad_6() { return &____autoLoad_6; }
	inline void set__autoLoad_6(bool value)
	{
		____autoLoad_6 = value;
	}

	inline static int32_t get_offset_of__defaultTab_7() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____defaultTab_7)); }
	inline int32_t get__defaultTab_7() const { return ____defaultTab_7; }
	inline int32_t* get_address_of__defaultTab_7() { return &____defaultTab_7; }
	inline void set__defaultTab_7(int32_t value)
	{
		____defaultTab_7 = value;
	}

	inline static int32_t get_offset_of__triggerEnableMode_8() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____triggerEnableMode_8)); }
	inline int32_t get__triggerEnableMode_8() const { return ____triggerEnableMode_8; }
	inline int32_t* get_address_of__triggerEnableMode_8() { return &____triggerEnableMode_8; }
	inline void set__triggerEnableMode_8(int32_t value)
	{
		____triggerEnableMode_8 = value;
	}

	inline static int32_t get_offset_of__triggerBehaviour_9() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____triggerBehaviour_9)); }
	inline int32_t get__triggerBehaviour_9() const { return ____triggerBehaviour_9; }
	inline int32_t* get_address_of__triggerBehaviour_9() { return &____triggerBehaviour_9; }
	inline void set__triggerBehaviour_9(int32_t value)
	{
		____triggerBehaviour_9 = value;
	}

	inline static int32_t get_offset_of__enableKeyboardShortcuts_10() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____enableKeyboardShortcuts_10)); }
	inline bool get__enableKeyboardShortcuts_10() const { return ____enableKeyboardShortcuts_10; }
	inline bool* get_address_of__enableKeyboardShortcuts_10() { return &____enableKeyboardShortcuts_10; }
	inline void set__enableKeyboardShortcuts_10(bool value)
	{
		____enableKeyboardShortcuts_10 = value;
	}

	inline static int32_t get_offset_of__keyboardShortcuts_11() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____keyboardShortcuts_11)); }
	inline KeyboardShortcutU5BU5D_t3674246146* get__keyboardShortcuts_11() const { return ____keyboardShortcuts_11; }
	inline KeyboardShortcutU5BU5D_t3674246146** get_address_of__keyboardShortcuts_11() { return &____keyboardShortcuts_11; }
	inline void set__keyboardShortcuts_11(KeyboardShortcutU5BU5D_t3674246146* value)
	{
		____keyboardShortcuts_11 = value;
		Il2CppCodeGenWriteBarrier(&____keyboardShortcuts_11, value);
	}

	inline static int32_t get_offset_of__newKeyboardShortcuts_12() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____newKeyboardShortcuts_12)); }
	inline KeyboardShortcutU5BU5D_t3674246146* get__newKeyboardShortcuts_12() const { return ____newKeyboardShortcuts_12; }
	inline KeyboardShortcutU5BU5D_t3674246146** get_address_of__newKeyboardShortcuts_12() { return &____newKeyboardShortcuts_12; }
	inline void set__newKeyboardShortcuts_12(KeyboardShortcutU5BU5D_t3674246146* value)
	{
		____newKeyboardShortcuts_12 = value;
		Il2CppCodeGenWriteBarrier(&____newKeyboardShortcuts_12, value);
	}

	inline static int32_t get_offset_of__keyboardModifierControl_13() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____keyboardModifierControl_13)); }
	inline bool get__keyboardModifierControl_13() const { return ____keyboardModifierControl_13; }
	inline bool* get_address_of__keyboardModifierControl_13() { return &____keyboardModifierControl_13; }
	inline void set__keyboardModifierControl_13(bool value)
	{
		____keyboardModifierControl_13 = value;
	}

	inline static int32_t get_offset_of__keyboardModifierAlt_14() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____keyboardModifierAlt_14)); }
	inline bool get__keyboardModifierAlt_14() const { return ____keyboardModifierAlt_14; }
	inline bool* get_address_of__keyboardModifierAlt_14() { return &____keyboardModifierAlt_14; }
	inline void set__keyboardModifierAlt_14(bool value)
	{
		____keyboardModifierAlt_14 = value;
	}

	inline static int32_t get_offset_of__keyboardModifierShift_15() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____keyboardModifierShift_15)); }
	inline bool get__keyboardModifierShift_15() const { return ____keyboardModifierShift_15; }
	inline bool* get_address_of__keyboardModifierShift_15() { return &____keyboardModifierShift_15; }
	inline void set__keyboardModifierShift_15(bool value)
	{
		____keyboardModifierShift_15 = value;
	}

	inline static int32_t get_offset_of__keyboardEscapeClose_16() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____keyboardEscapeClose_16)); }
	inline bool get__keyboardEscapeClose_16() const { return ____keyboardEscapeClose_16; }
	inline bool* get_address_of__keyboardEscapeClose_16() { return &____keyboardEscapeClose_16; }
	inline void set__keyboardEscapeClose_16(bool value)
	{
		____keyboardEscapeClose_16 = value;
	}

	inline static int32_t get_offset_of__enableBackgroundTransparency_17() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____enableBackgroundTransparency_17)); }
	inline bool get__enableBackgroundTransparency_17() const { return ____enableBackgroundTransparency_17; }
	inline bool* get_address_of__enableBackgroundTransparency_17() { return &____enableBackgroundTransparency_17; }
	inline void set__enableBackgroundTransparency_17(bool value)
	{
		____enableBackgroundTransparency_17 = value;
	}

	inline static int32_t get_offset_of__collapseDuplicateLogEntries_18() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____collapseDuplicateLogEntries_18)); }
	inline bool get__collapseDuplicateLogEntries_18() const { return ____collapseDuplicateLogEntries_18; }
	inline bool* get_address_of__collapseDuplicateLogEntries_18() { return &____collapseDuplicateLogEntries_18; }
	inline void set__collapseDuplicateLogEntries_18(bool value)
	{
		____collapseDuplicateLogEntries_18 = value;
	}

	inline static int32_t get_offset_of__richTextInConsole_19() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____richTextInConsole_19)); }
	inline bool get__richTextInConsole_19() const { return ____richTextInConsole_19; }
	inline bool* get_address_of__richTextInConsole_19() { return &____richTextInConsole_19; }
	inline void set__richTextInConsole_19(bool value)
	{
		____richTextInConsole_19 = value;
	}

	inline static int32_t get_offset_of__requireEntryCode_20() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____requireEntryCode_20)); }
	inline bool get__requireEntryCode_20() const { return ____requireEntryCode_20; }
	inline bool* get_address_of__requireEntryCode_20() { return &____requireEntryCode_20; }
	inline void set__requireEntryCode_20(bool value)
	{
		____requireEntryCode_20 = value;
	}

	inline static int32_t get_offset_of__requireEntryCodeEveryTime_21() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____requireEntryCodeEveryTime_21)); }
	inline bool get__requireEntryCodeEveryTime_21() const { return ____requireEntryCodeEveryTime_21; }
	inline bool* get_address_of__requireEntryCodeEveryTime_21() { return &____requireEntryCodeEveryTime_21; }
	inline void set__requireEntryCodeEveryTime_21(bool value)
	{
		____requireEntryCodeEveryTime_21 = value;
	}

	inline static int32_t get_offset_of__entryCode_22() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____entryCode_22)); }
	inline Int32U5BU5D_t3030399641* get__entryCode_22() const { return ____entryCode_22; }
	inline Int32U5BU5D_t3030399641** get_address_of__entryCode_22() { return &____entryCode_22; }
	inline void set__entryCode_22(Int32U5BU5D_t3030399641* value)
	{
		____entryCode_22 = value;
		Il2CppCodeGenWriteBarrier(&____entryCode_22, value);
	}

	inline static int32_t get_offset_of__useDebugCamera_23() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____useDebugCamera_23)); }
	inline bool get__useDebugCamera_23() const { return ____useDebugCamera_23; }
	inline bool* get_address_of__useDebugCamera_23() { return &____useDebugCamera_23; }
	inline void set__useDebugCamera_23(bool value)
	{
		____useDebugCamera_23 = value;
	}

	inline static int32_t get_offset_of__debugLayer_24() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____debugLayer_24)); }
	inline int32_t get__debugLayer_24() const { return ____debugLayer_24; }
	inline int32_t* get_address_of__debugLayer_24() { return &____debugLayer_24; }
	inline void set__debugLayer_24(int32_t value)
	{
		____debugLayer_24 = value;
	}

	inline static int32_t get_offset_of__debugCameraDepth_25() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____debugCameraDepth_25)); }
	inline float get__debugCameraDepth_25() const { return ____debugCameraDepth_25; }
	inline float* get_address_of__debugCameraDepth_25() { return &____debugCameraDepth_25; }
	inline void set__debugCameraDepth_25(float value)
	{
		____debugCameraDepth_25 = value;
	}

	inline static int32_t get_offset_of__apiKey_26() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____apiKey_26)); }
	inline String_t* get__apiKey_26() const { return ____apiKey_26; }
	inline String_t** get_address_of__apiKey_26() { return &____apiKey_26; }
	inline void set__apiKey_26(String_t* value)
	{
		____apiKey_26 = value;
		Il2CppCodeGenWriteBarrier(&____apiKey_26, value);
	}

	inline static int32_t get_offset_of__enableBugReporter_27() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____enableBugReporter_27)); }
	inline bool get__enableBugReporter_27() const { return ____enableBugReporter_27; }
	inline bool* get_address_of__enableBugReporter_27() { return &____enableBugReporter_27; }
	inline void set__enableBugReporter_27(bool value)
	{
		____enableBugReporter_27 = value;
	}

	inline static int32_t get_offset_of__disabledTabs_28() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____disabledTabs_28)); }
	inline DefaultTabsU5BU5D_t1346111640* get__disabledTabs_28() const { return ____disabledTabs_28; }
	inline DefaultTabsU5BU5D_t1346111640** get_address_of__disabledTabs_28() { return &____disabledTabs_28; }
	inline void set__disabledTabs_28(DefaultTabsU5BU5D_t1346111640* value)
	{
		____disabledTabs_28 = value;
		Il2CppCodeGenWriteBarrier(&____disabledTabs_28, value);
	}

	inline static int32_t get_offset_of__profilerAlignment_29() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____profilerAlignment_29)); }
	inline int32_t get__profilerAlignment_29() const { return ____profilerAlignment_29; }
	inline int32_t* get_address_of__profilerAlignment_29() { return &____profilerAlignment_29; }
	inline void set__profilerAlignment_29(int32_t value)
	{
		____profilerAlignment_29 = value;
	}

	inline static int32_t get_offset_of__optionsAlignment_30() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____optionsAlignment_30)); }
	inline int32_t get__optionsAlignment_30() const { return ____optionsAlignment_30; }
	inline int32_t* get_address_of__optionsAlignment_30() { return &____optionsAlignment_30; }
	inline void set__optionsAlignment_30(int32_t value)
	{
		____optionsAlignment_30 = value;
	}

	inline static int32_t get_offset_of__consoleAlignment_31() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____consoleAlignment_31)); }
	inline int32_t get__consoleAlignment_31() const { return ____consoleAlignment_31; }
	inline int32_t* get_address_of__consoleAlignment_31() { return &____consoleAlignment_31; }
	inline void set__consoleAlignment_31(int32_t value)
	{
		____consoleAlignment_31 = value;
	}

	inline static int32_t get_offset_of__triggerPosition_32() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____triggerPosition_32)); }
	inline int32_t get__triggerPosition_32() const { return ____triggerPosition_32; }
	inline int32_t* get_address_of__triggerPosition_32() { return &____triggerPosition_32; }
	inline void set__triggerPosition_32(int32_t value)
	{
		____triggerPosition_32 = value;
	}

	inline static int32_t get_offset_of__maximumConsoleEntries_33() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____maximumConsoleEntries_33)); }
	inline int32_t get__maximumConsoleEntries_33() const { return ____maximumConsoleEntries_33; }
	inline int32_t* get_address_of__maximumConsoleEntries_33() { return &____maximumConsoleEntries_33; }
	inline void set__maximumConsoleEntries_33(int32_t value)
	{
		____maximumConsoleEntries_33 = value;
	}

	inline static int32_t get_offset_of__enableEventSystemCreation_34() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____enableEventSystemCreation_34)); }
	inline bool get__enableEventSystemCreation_34() const { return ____enableEventSystemCreation_34; }
	inline bool* get_address_of__enableEventSystemCreation_34() { return &____enableEventSystemCreation_34; }
	inline void set__enableEventSystemCreation_34(bool value)
	{
		____enableEventSystemCreation_34 = value;
	}

	inline static int32_t get_offset_of__automaticShowCursor_35() { return static_cast<int32_t>(offsetof(Settings_t1299540155, ____automaticShowCursor_35)); }
	inline bool get__automaticShowCursor_35() const { return ____automaticShowCursor_35; }
	inline bool* get_address_of__automaticShowCursor_35() { return &____automaticShowCursor_35; }
	inline void set__automaticShowCursor_35(bool value)
	{
		____automaticShowCursor_35 = value;
	}
};

struct Settings_t1299540155_StaticFields
{
public:
	// SRDebugger.Settings SRDebugger.Settings::_instance
	Settings_t1299540155 * ____instance_4;
	// System.Func`2<System.Int32,System.Boolean> SRDebugger.Settings::<>f__am$cache0
	Func_2_t3377395111 * ___U3CU3Ef__amU24cache0_36;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Settings_t1299540155_StaticFields, ____instance_4)); }
	inline Settings_t1299540155 * get__instance_4() const { return ____instance_4; }
	inline Settings_t1299540155 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(Settings_t1299540155 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier(&____instance_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_36() { return static_cast<int32_t>(offsetof(Settings_t1299540155_StaticFields, ___U3CU3Ef__amU24cache0_36)); }
	inline Func_2_t3377395111 * get_U3CU3Ef__amU24cache0_36() const { return ___U3CU3Ef__amU24cache0_36; }
	inline Func_2_t3377395111 ** get_address_of_U3CU3Ef__amU24cache0_36() { return &___U3CU3Ef__amU24cache0_36; }
	inline void set_U3CU3Ef__amU24cache0_36(Func_2_t3377395111 * value)
	{
		___U3CU3Ef__amU24cache0_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
