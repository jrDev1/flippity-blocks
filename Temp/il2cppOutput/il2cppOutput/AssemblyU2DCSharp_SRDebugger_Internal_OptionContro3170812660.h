﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.Internal.OptionDefinition
struct OptionDefinition_t4209375604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.OptionControlFactory/<CreateDataControl>c__AnonStorey0
struct  U3CCreateDataControlU3Ec__AnonStorey0_t3170812660  : public Il2CppObject
{
public:
	// SRDebugger.Internal.OptionDefinition SRDebugger.Internal.OptionControlFactory/<CreateDataControl>c__AnonStorey0::from
	OptionDefinition_t4209375604 * ___from_0;

public:
	inline static int32_t get_offset_of_from_0() { return static_cast<int32_t>(offsetof(U3CCreateDataControlU3Ec__AnonStorey0_t3170812660, ___from_0)); }
	inline OptionDefinition_t4209375604 * get_from_0() const { return ___from_0; }
	inline OptionDefinition_t4209375604 ** get_address_of_from_0() { return &___from_0; }
	inline void set_from_0(OptionDefinition_t4209375604 * value)
	{
		___from_0 = value;
		Il2CppCodeGenWriteBarrier(&___from_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
