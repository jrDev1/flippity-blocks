﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.List`1<SRDebugger.Services.ConsoleEntry>
struct List_1_t2378088731;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct Dictionary_2_t2224040523;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.BugReport
struct  BugReport_t3548726662  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.BugReport::ConsoleLog
	List_1_t2378088731 * ___ConsoleLog_0;
	// System.String SRDebugger.Services.BugReport::Email
	String_t* ___Email_1;
	// System.Byte[] SRDebugger.Services.BugReport::ScreenshotData
	ByteU5BU5D_t3397334013* ___ScreenshotData_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>> SRDebugger.Services.BugReport::SystemInformation
	Dictionary_2_t2224040523 * ___SystemInformation_3;
	// System.String SRDebugger.Services.BugReport::UserDescription
	String_t* ___UserDescription_4;

public:
	inline static int32_t get_offset_of_ConsoleLog_0() { return static_cast<int32_t>(offsetof(BugReport_t3548726662, ___ConsoleLog_0)); }
	inline List_1_t2378088731 * get_ConsoleLog_0() const { return ___ConsoleLog_0; }
	inline List_1_t2378088731 ** get_address_of_ConsoleLog_0() { return &___ConsoleLog_0; }
	inline void set_ConsoleLog_0(List_1_t2378088731 * value)
	{
		___ConsoleLog_0 = value;
		Il2CppCodeGenWriteBarrier(&___ConsoleLog_0, value);
	}

	inline static int32_t get_offset_of_Email_1() { return static_cast<int32_t>(offsetof(BugReport_t3548726662, ___Email_1)); }
	inline String_t* get_Email_1() const { return ___Email_1; }
	inline String_t** get_address_of_Email_1() { return &___Email_1; }
	inline void set_Email_1(String_t* value)
	{
		___Email_1 = value;
		Il2CppCodeGenWriteBarrier(&___Email_1, value);
	}

	inline static int32_t get_offset_of_ScreenshotData_2() { return static_cast<int32_t>(offsetof(BugReport_t3548726662, ___ScreenshotData_2)); }
	inline ByteU5BU5D_t3397334013* get_ScreenshotData_2() const { return ___ScreenshotData_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_ScreenshotData_2() { return &___ScreenshotData_2; }
	inline void set_ScreenshotData_2(ByteU5BU5D_t3397334013* value)
	{
		___ScreenshotData_2 = value;
		Il2CppCodeGenWriteBarrier(&___ScreenshotData_2, value);
	}

	inline static int32_t get_offset_of_SystemInformation_3() { return static_cast<int32_t>(offsetof(BugReport_t3548726662, ___SystemInformation_3)); }
	inline Dictionary_2_t2224040523 * get_SystemInformation_3() const { return ___SystemInformation_3; }
	inline Dictionary_2_t2224040523 ** get_address_of_SystemInformation_3() { return &___SystemInformation_3; }
	inline void set_SystemInformation_3(Dictionary_2_t2224040523 * value)
	{
		___SystemInformation_3 = value;
		Il2CppCodeGenWriteBarrier(&___SystemInformation_3, value);
	}

	inline static int32_t get_offset_of_UserDescription_4() { return static_cast<int32_t>(offsetof(BugReport_t3548726662, ___UserDescription_4)); }
	inline String_t* get_UserDescription_4() const { return ___UserDescription_4; }
	inline String_t** get_address_of_UserDescription_4() { return &___UserDescription_4; }
	inline void set_UserDescription_4(String_t* value)
	{
		___UserDescription_4 = value;
		Il2CppCodeGenWriteBarrier(&___UserDescription_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
