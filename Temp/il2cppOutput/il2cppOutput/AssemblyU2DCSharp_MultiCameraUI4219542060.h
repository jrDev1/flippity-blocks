﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiCameraUI
struct  MultiCameraUI_t4219542060  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera MultiCameraUI::cam2
	Camera_t189460977 * ___cam2_2;
	// UnityEngine.Camera MultiCameraUI::cam3
	Camera_t189460977 * ___cam3_3;

public:
	inline static int32_t get_offset_of_cam2_2() { return static_cast<int32_t>(offsetof(MultiCameraUI_t4219542060, ___cam2_2)); }
	inline Camera_t189460977 * get_cam2_2() const { return ___cam2_2; }
	inline Camera_t189460977 ** get_address_of_cam2_2() { return &___cam2_2; }
	inline void set_cam2_2(Camera_t189460977 * value)
	{
		___cam2_2 = value;
		Il2CppCodeGenWriteBarrier(&___cam2_2, value);
	}

	inline static int32_t get_offset_of_cam3_3() { return static_cast<int32_t>(offsetof(MultiCameraUI_t4219542060, ___cam3_3)); }
	inline Camera_t189460977 * get_cam3_3() const { return ___cam3_3; }
	inline Camera_t189460977 ** get_address_of_cam3_3() { return &___cam3_3; }
	inline void set_cam3_3(Camera_t189460977 * value)
	{
		___cam3_3 = value;
		Il2CppCodeGenWriteBarrier(&___cam3_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
