﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Component[]
struct ComponentU5BU5D_t4136971630;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiDragEventsExecutionProxy
struct  PlayMakerUGuiDragEventsExecutionProxy_t498894609  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerUGuiDragEventsExecutionProxy::AutoDetect
	bool ___AutoDetect_2;
	// UnityEngine.Component[] PlayMakerUGuiDragEventsExecutionProxy::_beginDragTargets
	ComponentU5BU5D_t4136971630* ____beginDragTargets_3;
	// UnityEngine.Component[] PlayMakerUGuiDragEventsExecutionProxy::_dragTargets
	ComponentU5BU5D_t4136971630* ____dragTargets_4;
	// UnityEngine.Component[] PlayMakerUGuiDragEventsExecutionProxy::_endDragTargets
	ComponentU5BU5D_t4136971630* ____endDragTargets_5;

public:
	inline static int32_t get_offset_of_AutoDetect_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsExecutionProxy_t498894609, ___AutoDetect_2)); }
	inline bool get_AutoDetect_2() const { return ___AutoDetect_2; }
	inline bool* get_address_of_AutoDetect_2() { return &___AutoDetect_2; }
	inline void set_AutoDetect_2(bool value)
	{
		___AutoDetect_2 = value;
	}

	inline static int32_t get_offset_of__beginDragTargets_3() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsExecutionProxy_t498894609, ____beginDragTargets_3)); }
	inline ComponentU5BU5D_t4136971630* get__beginDragTargets_3() const { return ____beginDragTargets_3; }
	inline ComponentU5BU5D_t4136971630** get_address_of__beginDragTargets_3() { return &____beginDragTargets_3; }
	inline void set__beginDragTargets_3(ComponentU5BU5D_t4136971630* value)
	{
		____beginDragTargets_3 = value;
		Il2CppCodeGenWriteBarrier(&____beginDragTargets_3, value);
	}

	inline static int32_t get_offset_of__dragTargets_4() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsExecutionProxy_t498894609, ____dragTargets_4)); }
	inline ComponentU5BU5D_t4136971630* get__dragTargets_4() const { return ____dragTargets_4; }
	inline ComponentU5BU5D_t4136971630** get_address_of__dragTargets_4() { return &____dragTargets_4; }
	inline void set__dragTargets_4(ComponentU5BU5D_t4136971630* value)
	{
		____dragTargets_4 = value;
		Il2CppCodeGenWriteBarrier(&____dragTargets_4, value);
	}

	inline static int32_t get_offset_of__endDragTargets_5() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsExecutionProxy_t498894609, ____endDragTargets_5)); }
	inline ComponentU5BU5D_t4136971630* get__endDragTargets_5() const { return ____endDragTargets_5; }
	inline ComponentU5BU5D_t4136971630** get_address_of__endDragTargets_5() { return &____endDragTargets_5; }
	inline void set__endDragTargets_5(ComponentU5BU5D_t4136971630* value)
	{
		____endDragTargets_5 = value;
		Il2CppCodeGenWriteBarrier(&____endDragTargets_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
