﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_DataBound3384854171.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.BoolControl
struct  BoolControl_t3658693475  : public DataBoundControl_t3384854171
{
public:
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.BoolControl::Title
	Text_t356221433 * ___Title_17;
	// UnityEngine.UI.Toggle SRDebugger.UI.Controls.Data.BoolControl::Toggle
	Toggle_t3976754468 * ___Toggle_18;

public:
	inline static int32_t get_offset_of_Title_17() { return static_cast<int32_t>(offsetof(BoolControl_t3658693475, ___Title_17)); }
	inline Text_t356221433 * get_Title_17() const { return ___Title_17; }
	inline Text_t356221433 ** get_address_of_Title_17() { return &___Title_17; }
	inline void set_Title_17(Text_t356221433 * value)
	{
		___Title_17 = value;
		Il2CppCodeGenWriteBarrier(&___Title_17, value);
	}

	inline static int32_t get_offset_of_Toggle_18() { return static_cast<int32_t>(offsetof(BoolControl_t3658693475, ___Toggle_18)); }
	inline Toggle_t3976754468 * get_Toggle_18() const { return ___Toggle_18; }
	inline Toggle_t3976754468 ** get_address_of_Toggle_18() { return &___Toggle_18; }
	inline void set_Toggle_18(Toggle_t3976754468 * value)
	{
		___Toggle_18 = value;
		Il2CppCodeGenWriteBarrier(&___Toggle_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
