﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// System.String
struct String_t;
// SRF.UI.StyleComponent
struct StyleComponent_t3036492378;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.DebugPanelBackgroundBehaviour
struct  DebugPanelBackgroundBehaviour_t4136423076  : public SRMonoBehaviour_t2352136145
{
public:
	// System.String SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::_defaultKey
	String_t* ____defaultKey_8;
	// System.Boolean SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::_isTransparent
	bool ____isTransparent_9;
	// SRF.UI.StyleComponent SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::_styleComponent
	StyleComponent_t3036492378 * ____styleComponent_10;
	// System.String SRDebugger.UI.Other.DebugPanelBackgroundBehaviour::TransparentStyleKey
	String_t* ___TransparentStyleKey_11;

public:
	inline static int32_t get_offset_of__defaultKey_8() { return static_cast<int32_t>(offsetof(DebugPanelBackgroundBehaviour_t4136423076, ____defaultKey_8)); }
	inline String_t* get__defaultKey_8() const { return ____defaultKey_8; }
	inline String_t** get_address_of__defaultKey_8() { return &____defaultKey_8; }
	inline void set__defaultKey_8(String_t* value)
	{
		____defaultKey_8 = value;
		Il2CppCodeGenWriteBarrier(&____defaultKey_8, value);
	}

	inline static int32_t get_offset_of__isTransparent_9() { return static_cast<int32_t>(offsetof(DebugPanelBackgroundBehaviour_t4136423076, ____isTransparent_9)); }
	inline bool get__isTransparent_9() const { return ____isTransparent_9; }
	inline bool* get_address_of__isTransparent_9() { return &____isTransparent_9; }
	inline void set__isTransparent_9(bool value)
	{
		____isTransparent_9 = value;
	}

	inline static int32_t get_offset_of__styleComponent_10() { return static_cast<int32_t>(offsetof(DebugPanelBackgroundBehaviour_t4136423076, ____styleComponent_10)); }
	inline StyleComponent_t3036492378 * get__styleComponent_10() const { return ____styleComponent_10; }
	inline StyleComponent_t3036492378 ** get_address_of__styleComponent_10() { return &____styleComponent_10; }
	inline void set__styleComponent_10(StyleComponent_t3036492378 * value)
	{
		____styleComponent_10 = value;
		Il2CppCodeGenWriteBarrier(&____styleComponent_10, value);
	}

	inline static int32_t get_offset_of_TransparentStyleKey_11() { return static_cast<int32_t>(offsetof(DebugPanelBackgroundBehaviour_t4136423076, ___TransparentStyleKey_11)); }
	inline String_t* get_TransparentStyleKey_11() const { return ___TransparentStyleKey_11; }
	inline String_t** get_address_of_TransparentStyleKey_11() { return &___TransparentStyleKey_11; }
	inline void set_TransparentStyleKey_11(String_t* value)
	{
		___TransparentStyleKey_11 = value;
		Il2CppCodeGenWriteBarrier(&___TransparentStyleKey_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
