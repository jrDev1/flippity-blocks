﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.ActionCompleteCallback
struct ActionCompleteCallback_t2151041428;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.SRDebugService/<ShowBugReportSheet>c__AnonStorey0
struct  U3CShowBugReportSheetU3Ec__AnonStorey0_t1245924925  : public Il2CppObject
{
public:
	// SRDebugger.ActionCompleteCallback SRDebugger.Services.Implementation.SRDebugService/<ShowBugReportSheet>c__AnonStorey0::onComplete
	ActionCompleteCallback_t2151041428 * ___onComplete_0;

public:
	inline static int32_t get_offset_of_onComplete_0() { return static_cast<int32_t>(offsetof(U3CShowBugReportSheetU3Ec__AnonStorey0_t1245924925, ___onComplete_0)); }
	inline ActionCompleteCallback_t2151041428 * get_onComplete_0() const { return ___onComplete_0; }
	inline ActionCompleteCallback_t2151041428 ** get_address_of_onComplete_0() { return &___onComplete_0; }
	inline void set_onComplete_0(ActionCompleteCallback_t2151041428 * value)
	{
		___onComplete_0 = value;
		Il2CppCodeGenWriteBarrier(&___onComplete_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
