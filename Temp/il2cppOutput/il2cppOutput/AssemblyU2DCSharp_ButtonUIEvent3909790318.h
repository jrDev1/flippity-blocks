﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonUIEvent
struct  ButtonUIEvent_t3909790318  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ButtonUIEvent::downText
	Text_t356221433 * ___downText_2;
	// UnityEngine.UI.Text ButtonUIEvent::pressText
	Text_t356221433 * ___pressText_3;
	// UnityEngine.UI.Text ButtonUIEvent::pressValueText
	Text_t356221433 * ___pressValueText_4;
	// UnityEngine.UI.Text ButtonUIEvent::upText
	Text_t356221433 * ___upText_5;

public:
	inline static int32_t get_offset_of_downText_2() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t3909790318, ___downText_2)); }
	inline Text_t356221433 * get_downText_2() const { return ___downText_2; }
	inline Text_t356221433 ** get_address_of_downText_2() { return &___downText_2; }
	inline void set_downText_2(Text_t356221433 * value)
	{
		___downText_2 = value;
		Il2CppCodeGenWriteBarrier(&___downText_2, value);
	}

	inline static int32_t get_offset_of_pressText_3() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t3909790318, ___pressText_3)); }
	inline Text_t356221433 * get_pressText_3() const { return ___pressText_3; }
	inline Text_t356221433 ** get_address_of_pressText_3() { return &___pressText_3; }
	inline void set_pressText_3(Text_t356221433 * value)
	{
		___pressText_3 = value;
		Il2CppCodeGenWriteBarrier(&___pressText_3, value);
	}

	inline static int32_t get_offset_of_pressValueText_4() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t3909790318, ___pressValueText_4)); }
	inline Text_t356221433 * get_pressValueText_4() const { return ___pressValueText_4; }
	inline Text_t356221433 ** get_address_of_pressValueText_4() { return &___pressValueText_4; }
	inline void set_pressValueText_4(Text_t356221433 * value)
	{
		___pressValueText_4 = value;
		Il2CppCodeGenWriteBarrier(&___pressValueText_4, value);
	}

	inline static int32_t get_offset_of_upText_5() { return static_cast<int32_t>(offsetof(ButtonUIEvent_t3909790318, ___upText_5)); }
	inline Text_t356221433 * get_upText_5() const { return ___upText_5; }
	inline Text_t356221433 ** get_address_of_upText_5() { return &___upText_5; }
	inline void set_upText_5(Text_t356221433 * value)
	{
		___upText_5 = value;
		Il2CppCodeGenWriteBarrier(&___upText_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
