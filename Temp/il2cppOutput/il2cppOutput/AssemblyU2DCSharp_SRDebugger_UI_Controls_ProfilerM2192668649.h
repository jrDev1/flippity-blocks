﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ProfilerMonoBlock
struct  ProfilerMonoBlock_t2192668649  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Single SRDebugger.UI.Controls.ProfilerMonoBlock::_lastRefresh
	float ____lastRefresh_9;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerMonoBlock::CurrentUsedText
	Text_t356221433 * ___CurrentUsedText_10;
	// UnityEngine.GameObject SRDebugger.UI.Controls.ProfilerMonoBlock::NotSupportedMessage
	GameObject_t1756533147 * ___NotSupportedMessage_11;
	// UnityEngine.UI.Slider SRDebugger.UI.Controls.ProfilerMonoBlock::Slider
	Slider_t297367283 * ___Slider_12;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerMonoBlock::TotalAllocatedText
	Text_t356221433 * ___TotalAllocatedText_13;
	// System.Boolean SRDebugger.UI.Controls.ProfilerMonoBlock::_isSupported
	bool ____isSupported_14;

public:
	inline static int32_t get_offset_of__lastRefresh_9() { return static_cast<int32_t>(offsetof(ProfilerMonoBlock_t2192668649, ____lastRefresh_9)); }
	inline float get__lastRefresh_9() const { return ____lastRefresh_9; }
	inline float* get_address_of__lastRefresh_9() { return &____lastRefresh_9; }
	inline void set__lastRefresh_9(float value)
	{
		____lastRefresh_9 = value;
	}

	inline static int32_t get_offset_of_CurrentUsedText_10() { return static_cast<int32_t>(offsetof(ProfilerMonoBlock_t2192668649, ___CurrentUsedText_10)); }
	inline Text_t356221433 * get_CurrentUsedText_10() const { return ___CurrentUsedText_10; }
	inline Text_t356221433 ** get_address_of_CurrentUsedText_10() { return &___CurrentUsedText_10; }
	inline void set_CurrentUsedText_10(Text_t356221433 * value)
	{
		___CurrentUsedText_10 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentUsedText_10, value);
	}

	inline static int32_t get_offset_of_NotSupportedMessage_11() { return static_cast<int32_t>(offsetof(ProfilerMonoBlock_t2192668649, ___NotSupportedMessage_11)); }
	inline GameObject_t1756533147 * get_NotSupportedMessage_11() const { return ___NotSupportedMessage_11; }
	inline GameObject_t1756533147 ** get_address_of_NotSupportedMessage_11() { return &___NotSupportedMessage_11; }
	inline void set_NotSupportedMessage_11(GameObject_t1756533147 * value)
	{
		___NotSupportedMessage_11 = value;
		Il2CppCodeGenWriteBarrier(&___NotSupportedMessage_11, value);
	}

	inline static int32_t get_offset_of_Slider_12() { return static_cast<int32_t>(offsetof(ProfilerMonoBlock_t2192668649, ___Slider_12)); }
	inline Slider_t297367283 * get_Slider_12() const { return ___Slider_12; }
	inline Slider_t297367283 ** get_address_of_Slider_12() { return &___Slider_12; }
	inline void set_Slider_12(Slider_t297367283 * value)
	{
		___Slider_12 = value;
		Il2CppCodeGenWriteBarrier(&___Slider_12, value);
	}

	inline static int32_t get_offset_of_TotalAllocatedText_13() { return static_cast<int32_t>(offsetof(ProfilerMonoBlock_t2192668649, ___TotalAllocatedText_13)); }
	inline Text_t356221433 * get_TotalAllocatedText_13() const { return ___TotalAllocatedText_13; }
	inline Text_t356221433 ** get_address_of_TotalAllocatedText_13() { return &___TotalAllocatedText_13; }
	inline void set_TotalAllocatedText_13(Text_t356221433 * value)
	{
		___TotalAllocatedText_13 = value;
		Il2CppCodeGenWriteBarrier(&___TotalAllocatedText_13, value);
	}

	inline static int32_t get_offset_of__isSupported_14() { return static_cast<int32_t>(offsetof(ProfilerMonoBlock_t2192668649, ____isSupported_14)); }
	inline bool get__isSupported_14() const { return ____isSupported_14; }
	inline bool* get_address_of__isSupported_14() { return &____isSupported_14; }
	inline void set__isSupported_14(bool value)
	{
		____isSupported_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
