﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.InsideCollider2DInfo
struct  InsideCollider2DInfo_t967896992  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.InsideCollider2DInfo::colliderTarget
	FsmObject_t2785794313 * ___colliderTarget_11;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.InsideCollider2DInfo::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.InsideCollider2DInfo::insideCollider
	FsmBool_t664485696 * ___insideCollider_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.InsideCollider2DInfo::insideEvent
	FsmEvent_t1258573736 * ___insideEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.InsideCollider2DInfo::outsideEvent
	FsmEvent_t1258573736 * ___outsideEvent_15;

public:
	inline static int32_t get_offset_of_colliderTarget_11() { return static_cast<int32_t>(offsetof(InsideCollider2DInfo_t967896992, ___colliderTarget_11)); }
	inline FsmObject_t2785794313 * get_colliderTarget_11() const { return ___colliderTarget_11; }
	inline FsmObject_t2785794313 ** get_address_of_colliderTarget_11() { return &___colliderTarget_11; }
	inline void set_colliderTarget_11(FsmObject_t2785794313 * value)
	{
		___colliderTarget_11 = value;
		Il2CppCodeGenWriteBarrier(&___colliderTarget_11, value);
	}

	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(InsideCollider2DInfo_t967896992, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_insideCollider_13() { return static_cast<int32_t>(offsetof(InsideCollider2DInfo_t967896992, ___insideCollider_13)); }
	inline FsmBool_t664485696 * get_insideCollider_13() const { return ___insideCollider_13; }
	inline FsmBool_t664485696 ** get_address_of_insideCollider_13() { return &___insideCollider_13; }
	inline void set_insideCollider_13(FsmBool_t664485696 * value)
	{
		___insideCollider_13 = value;
		Il2CppCodeGenWriteBarrier(&___insideCollider_13, value);
	}

	inline static int32_t get_offset_of_insideEvent_14() { return static_cast<int32_t>(offsetof(InsideCollider2DInfo_t967896992, ___insideEvent_14)); }
	inline FsmEvent_t1258573736 * get_insideEvent_14() const { return ___insideEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_insideEvent_14() { return &___insideEvent_14; }
	inline void set_insideEvent_14(FsmEvent_t1258573736 * value)
	{
		___insideEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___insideEvent_14, value);
	}

	inline static int32_t get_offset_of_outsideEvent_15() { return static_cast<int32_t>(offsetof(InsideCollider2DInfo_t967896992, ___outsideEvent_15)); }
	inline FsmEvent_t1258573736 * get_outsideEvent_15() const { return ___outsideEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_outsideEvent_15() { return &___outsideEvent_15; }
	inline void set_outsideEvent_15(FsmEvent_t1258573736 * value)
	{
		___outsideEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___outsideEvent_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
