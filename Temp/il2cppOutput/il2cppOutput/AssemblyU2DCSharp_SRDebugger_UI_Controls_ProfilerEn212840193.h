﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ProfilerEnableControl
struct  ProfilerEnableControl_t212840193  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Boolean SRDebugger.UI.Controls.ProfilerEnableControl::_previousState
	bool ____previousState_9;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerEnableControl::ButtonText
	Text_t356221433 * ___ButtonText_10;
	// UnityEngine.UI.Button SRDebugger.UI.Controls.ProfilerEnableControl::EnableButton
	Button_t2872111280 * ___EnableButton_11;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerEnableControl::Text
	Text_t356221433 * ___Text_12;

public:
	inline static int32_t get_offset_of__previousState_9() { return static_cast<int32_t>(offsetof(ProfilerEnableControl_t212840193, ____previousState_9)); }
	inline bool get__previousState_9() const { return ____previousState_9; }
	inline bool* get_address_of__previousState_9() { return &____previousState_9; }
	inline void set__previousState_9(bool value)
	{
		____previousState_9 = value;
	}

	inline static int32_t get_offset_of_ButtonText_10() { return static_cast<int32_t>(offsetof(ProfilerEnableControl_t212840193, ___ButtonText_10)); }
	inline Text_t356221433 * get_ButtonText_10() const { return ___ButtonText_10; }
	inline Text_t356221433 ** get_address_of_ButtonText_10() { return &___ButtonText_10; }
	inline void set_ButtonText_10(Text_t356221433 * value)
	{
		___ButtonText_10 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonText_10, value);
	}

	inline static int32_t get_offset_of_EnableButton_11() { return static_cast<int32_t>(offsetof(ProfilerEnableControl_t212840193, ___EnableButton_11)); }
	inline Button_t2872111280 * get_EnableButton_11() const { return ___EnableButton_11; }
	inline Button_t2872111280 ** get_address_of_EnableButton_11() { return &___EnableButton_11; }
	inline void set_EnableButton_11(Button_t2872111280 * value)
	{
		___EnableButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___EnableButton_11, value);
	}

	inline static int32_t get_offset_of_Text_12() { return static_cast<int32_t>(offsetof(ProfilerEnableControl_t212840193, ___Text_12)); }
	inline Text_t356221433 * get_Text_12() const { return ___Text_12; }
	inline Text_t356221433 ** get_address_of_Text_12() { return &___Text_12; }
	inline void set_Text_12(Text_t356221433 * value)
	{
		___Text_12 = value;
		Il2CppCodeGenWriteBarrier(&___Text_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
