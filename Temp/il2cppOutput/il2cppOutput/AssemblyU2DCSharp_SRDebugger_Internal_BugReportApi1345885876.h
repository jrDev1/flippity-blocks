﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// SRDebugger.Services.BugReport
struct BugReport_t3548726662;
// UnityEngine.WWW
struct WWW_t2919945039;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.BugReportApi
struct  BugReportApi_t1345885876  : public Il2CppObject
{
public:
	// System.String SRDebugger.Internal.BugReportApi::_apiKey
	String_t* ____apiKey_0;
	// SRDebugger.Services.BugReport SRDebugger.Internal.BugReportApi::_bugReport
	BugReport_t3548726662 * ____bugReport_1;
	// System.Boolean SRDebugger.Internal.BugReportApi::_isBusy
	bool ____isBusy_2;
	// UnityEngine.WWW SRDebugger.Internal.BugReportApi::_www
	WWW_t2919945039 * ____www_3;
	// System.Boolean SRDebugger.Internal.BugReportApi::<IsComplete>k__BackingField
	bool ___U3CIsCompleteU3Ek__BackingField_4;
	// System.Boolean SRDebugger.Internal.BugReportApi::<WasSuccessful>k__BackingField
	bool ___U3CWasSuccessfulU3Ek__BackingField_5;
	// System.String SRDebugger.Internal.BugReportApi::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__apiKey_0() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ____apiKey_0)); }
	inline String_t* get__apiKey_0() const { return ____apiKey_0; }
	inline String_t** get_address_of__apiKey_0() { return &____apiKey_0; }
	inline void set__apiKey_0(String_t* value)
	{
		____apiKey_0 = value;
		Il2CppCodeGenWriteBarrier(&____apiKey_0, value);
	}

	inline static int32_t get_offset_of__bugReport_1() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ____bugReport_1)); }
	inline BugReport_t3548726662 * get__bugReport_1() const { return ____bugReport_1; }
	inline BugReport_t3548726662 ** get_address_of__bugReport_1() { return &____bugReport_1; }
	inline void set__bugReport_1(BugReport_t3548726662 * value)
	{
		____bugReport_1 = value;
		Il2CppCodeGenWriteBarrier(&____bugReport_1, value);
	}

	inline static int32_t get_offset_of__isBusy_2() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ____isBusy_2)); }
	inline bool get__isBusy_2() const { return ____isBusy_2; }
	inline bool* get_address_of__isBusy_2() { return &____isBusy_2; }
	inline void set__isBusy_2(bool value)
	{
		____isBusy_2 = value;
	}

	inline static int32_t get_offset_of__www_3() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ____www_3)); }
	inline WWW_t2919945039 * get__www_3() const { return ____www_3; }
	inline WWW_t2919945039 ** get_address_of__www_3() { return &____www_3; }
	inline void set__www_3(WWW_t2919945039 * value)
	{
		____www_3 = value;
		Il2CppCodeGenWriteBarrier(&____www_3, value);
	}

	inline static int32_t get_offset_of_U3CIsCompleteU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ___U3CIsCompleteU3Ek__BackingField_4)); }
	inline bool get_U3CIsCompleteU3Ek__BackingField_4() const { return ___U3CIsCompleteU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsCompleteU3Ek__BackingField_4() { return &___U3CIsCompleteU3Ek__BackingField_4; }
	inline void set_U3CIsCompleteU3Ek__BackingField_4(bool value)
	{
		___U3CIsCompleteU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CWasSuccessfulU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ___U3CWasSuccessfulU3Ek__BackingField_5)); }
	inline bool get_U3CWasSuccessfulU3Ek__BackingField_5() const { return ___U3CWasSuccessfulU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CWasSuccessfulU3Ek__BackingField_5() { return &___U3CWasSuccessfulU3Ek__BackingField_5; }
	inline void set_U3CWasSuccessfulU3Ek__BackingField_5(bool value)
	{
		___U3CWasSuccessfulU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BugReportApi_t1345885876, ___U3CErrorMessageU3Ek__BackingField_6)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_6() const { return ___U3CErrorMessageU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_6() { return &___U3CErrorMessageU3Ek__BackingField_6; }
	inline void set_U3CErrorMessageU3Ek__BackingField_6(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CErrorMessageU3Ek__BackingField_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
