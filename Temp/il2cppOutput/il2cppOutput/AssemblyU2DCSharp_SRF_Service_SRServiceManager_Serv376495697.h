﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Func`1<System.Object>
struct Func_1_t348874681;
// System.Type
struct Type_t;
// System.Func`1<System.Type>
struct Func_1_t3258195908;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRServiceManager/ServiceStub
struct  ServiceStub_t376495697  : public Il2CppObject
{
public:
	// System.Func`1<System.Object> SRF.Service.SRServiceManager/ServiceStub::Constructor
	Func_1_t348874681 * ___Constructor_0;
	// System.Type SRF.Service.SRServiceManager/ServiceStub::InterfaceType
	Type_t * ___InterfaceType_1;
	// System.Func`1<System.Type> SRF.Service.SRServiceManager/ServiceStub::Selector
	Func_1_t3258195908 * ___Selector_2;
	// System.Type SRF.Service.SRServiceManager/ServiceStub::Type
	Type_t * ___Type_3;

public:
	inline static int32_t get_offset_of_Constructor_0() { return static_cast<int32_t>(offsetof(ServiceStub_t376495697, ___Constructor_0)); }
	inline Func_1_t348874681 * get_Constructor_0() const { return ___Constructor_0; }
	inline Func_1_t348874681 ** get_address_of_Constructor_0() { return &___Constructor_0; }
	inline void set_Constructor_0(Func_1_t348874681 * value)
	{
		___Constructor_0 = value;
		Il2CppCodeGenWriteBarrier(&___Constructor_0, value);
	}

	inline static int32_t get_offset_of_InterfaceType_1() { return static_cast<int32_t>(offsetof(ServiceStub_t376495697, ___InterfaceType_1)); }
	inline Type_t * get_InterfaceType_1() const { return ___InterfaceType_1; }
	inline Type_t ** get_address_of_InterfaceType_1() { return &___InterfaceType_1; }
	inline void set_InterfaceType_1(Type_t * value)
	{
		___InterfaceType_1 = value;
		Il2CppCodeGenWriteBarrier(&___InterfaceType_1, value);
	}

	inline static int32_t get_offset_of_Selector_2() { return static_cast<int32_t>(offsetof(ServiceStub_t376495697, ___Selector_2)); }
	inline Func_1_t3258195908 * get_Selector_2() const { return ___Selector_2; }
	inline Func_1_t3258195908 ** get_address_of_Selector_2() { return &___Selector_2; }
	inline void set_Selector_2(Func_1_t3258195908 * value)
	{
		___Selector_2 = value;
		Il2CppCodeGenWriteBarrier(&___Selector_2, value);
	}

	inline static int32_t get_offset_of_Type_3() { return static_cast<int32_t>(offsetof(ServiceStub_t376495697, ___Type_3)); }
	inline Type_t * get_Type_3() const { return ___Type_3; }
	inline Type_t ** get_address_of_Type_3() { return &___Type_3; }
	inline void set_Type_3(Type_t * value)
	{
		___Type_3 = value;
		Il2CppCodeGenWriteBarrier(&___Type_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
