﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ETCBase118528977.h"
#include "AssemblyU2DCSharp_ETCJoystick_JoystickType3209490911.h"
#include "AssemblyU2DCSharp_ETCJoystick_RadiusBase74140754.h"
#include "AssemblyU2DCSharp_ETCJoystick_JoystickArea1582798202.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// ETCJoystick/OnMoveStartHandler
struct OnMoveStartHandler_t2146188217;
// ETCJoystick/OnMoveHandler
struct OnMoveHandler_t1082130481;
// ETCJoystick/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t3386561924;
// ETCJoystick/OnMoveEndHandler
struct OnMoveEndHandler_t4076635866;
// ETCJoystick/OnTouchStartHandler
struct OnTouchStartHandler_t4286579173;
// ETCJoystick/OnTouchUpHandler
struct OnTouchUpHandler_t2064839710;
// ETCJoystick/OnDownUpHandler
struct OnDownUpHandler_t1598938991;
// ETCJoystick/OnDownDownHandler
struct OnDownDownHandler_t3463443096;
// ETCJoystick/OnDownLeftHandler
struct OnDownLeftHandler_t2704089297;
// ETCJoystick/OnDownRightHandler
struct OnDownRightHandler_t3714567814;
// ETCAxis
struct ETCAxis_t3445562479;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCJoystick
struct  ETCJoystick_t1870347408  : public ETCBase_t118528977
{
public:
	// ETCJoystick/OnMoveStartHandler ETCJoystick::onMoveStart
	OnMoveStartHandler_t2146188217 * ___onMoveStart_48;
	// ETCJoystick/OnMoveHandler ETCJoystick::onMove
	OnMoveHandler_t1082130481 * ___onMove_49;
	// ETCJoystick/OnMoveSpeedHandler ETCJoystick::onMoveSpeed
	OnMoveSpeedHandler_t3386561924 * ___onMoveSpeed_50;
	// ETCJoystick/OnMoveEndHandler ETCJoystick::onMoveEnd
	OnMoveEndHandler_t4076635866 * ___onMoveEnd_51;
	// ETCJoystick/OnTouchStartHandler ETCJoystick::onTouchStart
	OnTouchStartHandler_t4286579173 * ___onTouchStart_52;
	// ETCJoystick/OnTouchUpHandler ETCJoystick::onTouchUp
	OnTouchUpHandler_t2064839710 * ___onTouchUp_53;
	// ETCJoystick/OnDownUpHandler ETCJoystick::OnDownUp
	OnDownUpHandler_t1598938991 * ___OnDownUp_54;
	// ETCJoystick/OnDownDownHandler ETCJoystick::OnDownDown
	OnDownDownHandler_t3463443096 * ___OnDownDown_55;
	// ETCJoystick/OnDownLeftHandler ETCJoystick::OnDownLeft
	OnDownLeftHandler_t2704089297 * ___OnDownLeft_56;
	// ETCJoystick/OnDownRightHandler ETCJoystick::OnDownRight
	OnDownRightHandler_t3714567814 * ___OnDownRight_57;
	// ETCJoystick/OnDownUpHandler ETCJoystick::OnPressUp
	OnDownUpHandler_t1598938991 * ___OnPressUp_58;
	// ETCJoystick/OnDownDownHandler ETCJoystick::OnPressDown
	OnDownDownHandler_t3463443096 * ___OnPressDown_59;
	// ETCJoystick/OnDownLeftHandler ETCJoystick::OnPressLeft
	OnDownLeftHandler_t2704089297 * ___OnPressLeft_60;
	// ETCJoystick/OnDownRightHandler ETCJoystick::OnPressRight
	OnDownRightHandler_t3714567814 * ___OnPressRight_61;
	// ETCJoystick/JoystickType ETCJoystick::joystickType
	int32_t ___joystickType_62;
	// System.Boolean ETCJoystick::allowJoystickOverTouchPad
	bool ___allowJoystickOverTouchPad_63;
	// ETCJoystick/RadiusBase ETCJoystick::radiusBase
	int32_t ___radiusBase_64;
	// System.Single ETCJoystick::radiusBaseValue
	float ___radiusBaseValue_65;
	// ETCAxis ETCJoystick::axisX
	ETCAxis_t3445562479 * ___axisX_66;
	// ETCAxis ETCJoystick::axisY
	ETCAxis_t3445562479 * ___axisY_67;
	// UnityEngine.RectTransform ETCJoystick::thumb
	RectTransform_t3349966182 * ___thumb_68;
	// ETCJoystick/JoystickArea ETCJoystick::joystickArea
	int32_t ___joystickArea_69;
	// UnityEngine.RectTransform ETCJoystick::userArea
	RectTransform_t3349966182 * ___userArea_70;
	// System.Boolean ETCJoystick::isTurnAndMove
	bool ___isTurnAndMove_71;
	// System.Single ETCJoystick::tmSpeed
	float ___tmSpeed_72;
	// System.Single ETCJoystick::tmAdditionnalRotation
	float ___tmAdditionnalRotation_73;
	// UnityEngine.AnimationCurve ETCJoystick::tmMoveCurve
	AnimationCurve_t3306541151 * ___tmMoveCurve_74;
	// System.Boolean ETCJoystick::tmLockInJump
	bool ___tmLockInJump_75;
	// UnityEngine.Vector3 ETCJoystick::tmLastMove
	Vector3_t2243707580  ___tmLastMove_76;
	// UnityEngine.Vector2 ETCJoystick::thumbPosition
	Vector2_t2243707579  ___thumbPosition_77;
	// System.Boolean ETCJoystick::isDynamicActif
	bool ___isDynamicActif_78;
	// UnityEngine.Vector2 ETCJoystick::tmpAxis
	Vector2_t2243707579  ___tmpAxis_79;
	// UnityEngine.Vector2 ETCJoystick::OldTmpAxis
	Vector2_t2243707579  ___OldTmpAxis_80;
	// System.Boolean ETCJoystick::isOnTouch
	bool ___isOnTouch_81;
	// System.Boolean ETCJoystick::isNoReturnThumb
	bool ___isNoReturnThumb_82;
	// UnityEngine.Vector2 ETCJoystick::noReturnPosition
	Vector2_t2243707579  ___noReturnPosition_83;
	// UnityEngine.Vector2 ETCJoystick::noReturnOffset
	Vector2_t2243707579  ___noReturnOffset_84;
	// System.Boolean ETCJoystick::isNoOffsetThumb
	bool ___isNoOffsetThumb_85;

public:
	inline static int32_t get_offset_of_onMoveStart_48() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___onMoveStart_48)); }
	inline OnMoveStartHandler_t2146188217 * get_onMoveStart_48() const { return ___onMoveStart_48; }
	inline OnMoveStartHandler_t2146188217 ** get_address_of_onMoveStart_48() { return &___onMoveStart_48; }
	inline void set_onMoveStart_48(OnMoveStartHandler_t2146188217 * value)
	{
		___onMoveStart_48 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveStart_48, value);
	}

	inline static int32_t get_offset_of_onMove_49() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___onMove_49)); }
	inline OnMoveHandler_t1082130481 * get_onMove_49() const { return ___onMove_49; }
	inline OnMoveHandler_t1082130481 ** get_address_of_onMove_49() { return &___onMove_49; }
	inline void set_onMove_49(OnMoveHandler_t1082130481 * value)
	{
		___onMove_49 = value;
		Il2CppCodeGenWriteBarrier(&___onMove_49, value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_50() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___onMoveSpeed_50)); }
	inline OnMoveSpeedHandler_t3386561924 * get_onMoveSpeed_50() const { return ___onMoveSpeed_50; }
	inline OnMoveSpeedHandler_t3386561924 ** get_address_of_onMoveSpeed_50() { return &___onMoveSpeed_50; }
	inline void set_onMoveSpeed_50(OnMoveSpeedHandler_t3386561924 * value)
	{
		___onMoveSpeed_50 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveSpeed_50, value);
	}

	inline static int32_t get_offset_of_onMoveEnd_51() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___onMoveEnd_51)); }
	inline OnMoveEndHandler_t4076635866 * get_onMoveEnd_51() const { return ___onMoveEnd_51; }
	inline OnMoveEndHandler_t4076635866 ** get_address_of_onMoveEnd_51() { return &___onMoveEnd_51; }
	inline void set_onMoveEnd_51(OnMoveEndHandler_t4076635866 * value)
	{
		___onMoveEnd_51 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveEnd_51, value);
	}

	inline static int32_t get_offset_of_onTouchStart_52() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___onTouchStart_52)); }
	inline OnTouchStartHandler_t4286579173 * get_onTouchStart_52() const { return ___onTouchStart_52; }
	inline OnTouchStartHandler_t4286579173 ** get_address_of_onTouchStart_52() { return &___onTouchStart_52; }
	inline void set_onTouchStart_52(OnTouchStartHandler_t4286579173 * value)
	{
		___onTouchStart_52 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchStart_52, value);
	}

	inline static int32_t get_offset_of_onTouchUp_53() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___onTouchUp_53)); }
	inline OnTouchUpHandler_t2064839710 * get_onTouchUp_53() const { return ___onTouchUp_53; }
	inline OnTouchUpHandler_t2064839710 ** get_address_of_onTouchUp_53() { return &___onTouchUp_53; }
	inline void set_onTouchUp_53(OnTouchUpHandler_t2064839710 * value)
	{
		___onTouchUp_53 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchUp_53, value);
	}

	inline static int32_t get_offset_of_OnDownUp_54() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnDownUp_54)); }
	inline OnDownUpHandler_t1598938991 * get_OnDownUp_54() const { return ___OnDownUp_54; }
	inline OnDownUpHandler_t1598938991 ** get_address_of_OnDownUp_54() { return &___OnDownUp_54; }
	inline void set_OnDownUp_54(OnDownUpHandler_t1598938991 * value)
	{
		___OnDownUp_54 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownUp_54, value);
	}

	inline static int32_t get_offset_of_OnDownDown_55() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnDownDown_55)); }
	inline OnDownDownHandler_t3463443096 * get_OnDownDown_55() const { return ___OnDownDown_55; }
	inline OnDownDownHandler_t3463443096 ** get_address_of_OnDownDown_55() { return &___OnDownDown_55; }
	inline void set_OnDownDown_55(OnDownDownHandler_t3463443096 * value)
	{
		___OnDownDown_55 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownDown_55, value);
	}

	inline static int32_t get_offset_of_OnDownLeft_56() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnDownLeft_56)); }
	inline OnDownLeftHandler_t2704089297 * get_OnDownLeft_56() const { return ___OnDownLeft_56; }
	inline OnDownLeftHandler_t2704089297 ** get_address_of_OnDownLeft_56() { return &___OnDownLeft_56; }
	inline void set_OnDownLeft_56(OnDownLeftHandler_t2704089297 * value)
	{
		___OnDownLeft_56 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownLeft_56, value);
	}

	inline static int32_t get_offset_of_OnDownRight_57() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnDownRight_57)); }
	inline OnDownRightHandler_t3714567814 * get_OnDownRight_57() const { return ___OnDownRight_57; }
	inline OnDownRightHandler_t3714567814 ** get_address_of_OnDownRight_57() { return &___OnDownRight_57; }
	inline void set_OnDownRight_57(OnDownRightHandler_t3714567814 * value)
	{
		___OnDownRight_57 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownRight_57, value);
	}

	inline static int32_t get_offset_of_OnPressUp_58() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnPressUp_58)); }
	inline OnDownUpHandler_t1598938991 * get_OnPressUp_58() const { return ___OnPressUp_58; }
	inline OnDownUpHandler_t1598938991 ** get_address_of_OnPressUp_58() { return &___OnPressUp_58; }
	inline void set_OnPressUp_58(OnDownUpHandler_t1598938991 * value)
	{
		___OnPressUp_58 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressUp_58, value);
	}

	inline static int32_t get_offset_of_OnPressDown_59() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnPressDown_59)); }
	inline OnDownDownHandler_t3463443096 * get_OnPressDown_59() const { return ___OnPressDown_59; }
	inline OnDownDownHandler_t3463443096 ** get_address_of_OnPressDown_59() { return &___OnPressDown_59; }
	inline void set_OnPressDown_59(OnDownDownHandler_t3463443096 * value)
	{
		___OnPressDown_59 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressDown_59, value);
	}

	inline static int32_t get_offset_of_OnPressLeft_60() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnPressLeft_60)); }
	inline OnDownLeftHandler_t2704089297 * get_OnPressLeft_60() const { return ___OnPressLeft_60; }
	inline OnDownLeftHandler_t2704089297 ** get_address_of_OnPressLeft_60() { return &___OnPressLeft_60; }
	inline void set_OnPressLeft_60(OnDownLeftHandler_t2704089297 * value)
	{
		___OnPressLeft_60 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressLeft_60, value);
	}

	inline static int32_t get_offset_of_OnPressRight_61() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OnPressRight_61)); }
	inline OnDownRightHandler_t3714567814 * get_OnPressRight_61() const { return ___OnPressRight_61; }
	inline OnDownRightHandler_t3714567814 ** get_address_of_OnPressRight_61() { return &___OnPressRight_61; }
	inline void set_OnPressRight_61(OnDownRightHandler_t3714567814 * value)
	{
		___OnPressRight_61 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressRight_61, value);
	}

	inline static int32_t get_offset_of_joystickType_62() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___joystickType_62)); }
	inline int32_t get_joystickType_62() const { return ___joystickType_62; }
	inline int32_t* get_address_of_joystickType_62() { return &___joystickType_62; }
	inline void set_joystickType_62(int32_t value)
	{
		___joystickType_62 = value;
	}

	inline static int32_t get_offset_of_allowJoystickOverTouchPad_63() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___allowJoystickOverTouchPad_63)); }
	inline bool get_allowJoystickOverTouchPad_63() const { return ___allowJoystickOverTouchPad_63; }
	inline bool* get_address_of_allowJoystickOverTouchPad_63() { return &___allowJoystickOverTouchPad_63; }
	inline void set_allowJoystickOverTouchPad_63(bool value)
	{
		___allowJoystickOverTouchPad_63 = value;
	}

	inline static int32_t get_offset_of_radiusBase_64() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___radiusBase_64)); }
	inline int32_t get_radiusBase_64() const { return ___radiusBase_64; }
	inline int32_t* get_address_of_radiusBase_64() { return &___radiusBase_64; }
	inline void set_radiusBase_64(int32_t value)
	{
		___radiusBase_64 = value;
	}

	inline static int32_t get_offset_of_radiusBaseValue_65() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___radiusBaseValue_65)); }
	inline float get_radiusBaseValue_65() const { return ___radiusBaseValue_65; }
	inline float* get_address_of_radiusBaseValue_65() { return &___radiusBaseValue_65; }
	inline void set_radiusBaseValue_65(float value)
	{
		___radiusBaseValue_65 = value;
	}

	inline static int32_t get_offset_of_axisX_66() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___axisX_66)); }
	inline ETCAxis_t3445562479 * get_axisX_66() const { return ___axisX_66; }
	inline ETCAxis_t3445562479 ** get_address_of_axisX_66() { return &___axisX_66; }
	inline void set_axisX_66(ETCAxis_t3445562479 * value)
	{
		___axisX_66 = value;
		Il2CppCodeGenWriteBarrier(&___axisX_66, value);
	}

	inline static int32_t get_offset_of_axisY_67() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___axisY_67)); }
	inline ETCAxis_t3445562479 * get_axisY_67() const { return ___axisY_67; }
	inline ETCAxis_t3445562479 ** get_address_of_axisY_67() { return &___axisY_67; }
	inline void set_axisY_67(ETCAxis_t3445562479 * value)
	{
		___axisY_67 = value;
		Il2CppCodeGenWriteBarrier(&___axisY_67, value);
	}

	inline static int32_t get_offset_of_thumb_68() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___thumb_68)); }
	inline RectTransform_t3349966182 * get_thumb_68() const { return ___thumb_68; }
	inline RectTransform_t3349966182 ** get_address_of_thumb_68() { return &___thumb_68; }
	inline void set_thumb_68(RectTransform_t3349966182 * value)
	{
		___thumb_68 = value;
		Il2CppCodeGenWriteBarrier(&___thumb_68, value);
	}

	inline static int32_t get_offset_of_joystickArea_69() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___joystickArea_69)); }
	inline int32_t get_joystickArea_69() const { return ___joystickArea_69; }
	inline int32_t* get_address_of_joystickArea_69() { return &___joystickArea_69; }
	inline void set_joystickArea_69(int32_t value)
	{
		___joystickArea_69 = value;
	}

	inline static int32_t get_offset_of_userArea_70() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___userArea_70)); }
	inline RectTransform_t3349966182 * get_userArea_70() const { return ___userArea_70; }
	inline RectTransform_t3349966182 ** get_address_of_userArea_70() { return &___userArea_70; }
	inline void set_userArea_70(RectTransform_t3349966182 * value)
	{
		___userArea_70 = value;
		Il2CppCodeGenWriteBarrier(&___userArea_70, value);
	}

	inline static int32_t get_offset_of_isTurnAndMove_71() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___isTurnAndMove_71)); }
	inline bool get_isTurnAndMove_71() const { return ___isTurnAndMove_71; }
	inline bool* get_address_of_isTurnAndMove_71() { return &___isTurnAndMove_71; }
	inline void set_isTurnAndMove_71(bool value)
	{
		___isTurnAndMove_71 = value;
	}

	inline static int32_t get_offset_of_tmSpeed_72() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___tmSpeed_72)); }
	inline float get_tmSpeed_72() const { return ___tmSpeed_72; }
	inline float* get_address_of_tmSpeed_72() { return &___tmSpeed_72; }
	inline void set_tmSpeed_72(float value)
	{
		___tmSpeed_72 = value;
	}

	inline static int32_t get_offset_of_tmAdditionnalRotation_73() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___tmAdditionnalRotation_73)); }
	inline float get_tmAdditionnalRotation_73() const { return ___tmAdditionnalRotation_73; }
	inline float* get_address_of_tmAdditionnalRotation_73() { return &___tmAdditionnalRotation_73; }
	inline void set_tmAdditionnalRotation_73(float value)
	{
		___tmAdditionnalRotation_73 = value;
	}

	inline static int32_t get_offset_of_tmMoveCurve_74() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___tmMoveCurve_74)); }
	inline AnimationCurve_t3306541151 * get_tmMoveCurve_74() const { return ___tmMoveCurve_74; }
	inline AnimationCurve_t3306541151 ** get_address_of_tmMoveCurve_74() { return &___tmMoveCurve_74; }
	inline void set_tmMoveCurve_74(AnimationCurve_t3306541151 * value)
	{
		___tmMoveCurve_74 = value;
		Il2CppCodeGenWriteBarrier(&___tmMoveCurve_74, value);
	}

	inline static int32_t get_offset_of_tmLockInJump_75() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___tmLockInJump_75)); }
	inline bool get_tmLockInJump_75() const { return ___tmLockInJump_75; }
	inline bool* get_address_of_tmLockInJump_75() { return &___tmLockInJump_75; }
	inline void set_tmLockInJump_75(bool value)
	{
		___tmLockInJump_75 = value;
	}

	inline static int32_t get_offset_of_tmLastMove_76() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___tmLastMove_76)); }
	inline Vector3_t2243707580  get_tmLastMove_76() const { return ___tmLastMove_76; }
	inline Vector3_t2243707580 * get_address_of_tmLastMove_76() { return &___tmLastMove_76; }
	inline void set_tmLastMove_76(Vector3_t2243707580  value)
	{
		___tmLastMove_76 = value;
	}

	inline static int32_t get_offset_of_thumbPosition_77() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___thumbPosition_77)); }
	inline Vector2_t2243707579  get_thumbPosition_77() const { return ___thumbPosition_77; }
	inline Vector2_t2243707579 * get_address_of_thumbPosition_77() { return &___thumbPosition_77; }
	inline void set_thumbPosition_77(Vector2_t2243707579  value)
	{
		___thumbPosition_77 = value;
	}

	inline static int32_t get_offset_of_isDynamicActif_78() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___isDynamicActif_78)); }
	inline bool get_isDynamicActif_78() const { return ___isDynamicActif_78; }
	inline bool* get_address_of_isDynamicActif_78() { return &___isDynamicActif_78; }
	inline void set_isDynamicActif_78(bool value)
	{
		___isDynamicActif_78 = value;
	}

	inline static int32_t get_offset_of_tmpAxis_79() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___tmpAxis_79)); }
	inline Vector2_t2243707579  get_tmpAxis_79() const { return ___tmpAxis_79; }
	inline Vector2_t2243707579 * get_address_of_tmpAxis_79() { return &___tmpAxis_79; }
	inline void set_tmpAxis_79(Vector2_t2243707579  value)
	{
		___tmpAxis_79 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_80() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___OldTmpAxis_80)); }
	inline Vector2_t2243707579  get_OldTmpAxis_80() const { return ___OldTmpAxis_80; }
	inline Vector2_t2243707579 * get_address_of_OldTmpAxis_80() { return &___OldTmpAxis_80; }
	inline void set_OldTmpAxis_80(Vector2_t2243707579  value)
	{
		___OldTmpAxis_80 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_81() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___isOnTouch_81)); }
	inline bool get_isOnTouch_81() const { return ___isOnTouch_81; }
	inline bool* get_address_of_isOnTouch_81() { return &___isOnTouch_81; }
	inline void set_isOnTouch_81(bool value)
	{
		___isOnTouch_81 = value;
	}

	inline static int32_t get_offset_of_isNoReturnThumb_82() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___isNoReturnThumb_82)); }
	inline bool get_isNoReturnThumb_82() const { return ___isNoReturnThumb_82; }
	inline bool* get_address_of_isNoReturnThumb_82() { return &___isNoReturnThumb_82; }
	inline void set_isNoReturnThumb_82(bool value)
	{
		___isNoReturnThumb_82 = value;
	}

	inline static int32_t get_offset_of_noReturnPosition_83() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___noReturnPosition_83)); }
	inline Vector2_t2243707579  get_noReturnPosition_83() const { return ___noReturnPosition_83; }
	inline Vector2_t2243707579 * get_address_of_noReturnPosition_83() { return &___noReturnPosition_83; }
	inline void set_noReturnPosition_83(Vector2_t2243707579  value)
	{
		___noReturnPosition_83 = value;
	}

	inline static int32_t get_offset_of_noReturnOffset_84() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___noReturnOffset_84)); }
	inline Vector2_t2243707579  get_noReturnOffset_84() const { return ___noReturnOffset_84; }
	inline Vector2_t2243707579 * get_address_of_noReturnOffset_84() { return &___noReturnOffset_84; }
	inline void set_noReturnOffset_84(Vector2_t2243707579  value)
	{
		___noReturnOffset_84 = value;
	}

	inline static int32_t get_offset_of_isNoOffsetThumb_85() { return static_cast<int32_t>(offsetof(ETCJoystick_t1870347408, ___isNoOffsetThumb_85)); }
	inline bool get_isNoOffsetThumb_85() const { return ___isNoOffsetThumb_85; }
	inline bool* get_address_of_isNoOffsetThumb_85() { return &___isNoOffsetThumb_85; }
	inline void set_isNoOffsetThumb_85(bool value)
	{
		___isNoOffsetThumb_85 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
