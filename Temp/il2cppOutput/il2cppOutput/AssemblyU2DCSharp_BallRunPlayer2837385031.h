﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BallRunPlayer
struct  BallRunPlayer_t2837385031  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform BallRunPlayer::ballModel
	Transform_t3275118058 * ___ballModel_2;
	// System.Boolean BallRunPlayer::start
	bool ___start_3;
	// UnityEngine.Vector3 BallRunPlayer::moveDirection
	Vector3_t2243707580  ___moveDirection_4;
	// UnityEngine.CharacterController BallRunPlayer::characterController
	CharacterController_t4094781467 * ___characterController_5;
	// UnityEngine.Vector3 BallRunPlayer::startPosition
	Vector3_t2243707580  ___startPosition_6;
	// System.Boolean BallRunPlayer::isJump
	bool ___isJump_7;

public:
	inline static int32_t get_offset_of_ballModel_2() { return static_cast<int32_t>(offsetof(BallRunPlayer_t2837385031, ___ballModel_2)); }
	inline Transform_t3275118058 * get_ballModel_2() const { return ___ballModel_2; }
	inline Transform_t3275118058 ** get_address_of_ballModel_2() { return &___ballModel_2; }
	inline void set_ballModel_2(Transform_t3275118058 * value)
	{
		___ballModel_2 = value;
		Il2CppCodeGenWriteBarrier(&___ballModel_2, value);
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(BallRunPlayer_t2837385031, ___start_3)); }
	inline bool get_start_3() const { return ___start_3; }
	inline bool* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(bool value)
	{
		___start_3 = value;
	}

	inline static int32_t get_offset_of_moveDirection_4() { return static_cast<int32_t>(offsetof(BallRunPlayer_t2837385031, ___moveDirection_4)); }
	inline Vector3_t2243707580  get_moveDirection_4() const { return ___moveDirection_4; }
	inline Vector3_t2243707580 * get_address_of_moveDirection_4() { return &___moveDirection_4; }
	inline void set_moveDirection_4(Vector3_t2243707580  value)
	{
		___moveDirection_4 = value;
	}

	inline static int32_t get_offset_of_characterController_5() { return static_cast<int32_t>(offsetof(BallRunPlayer_t2837385031, ___characterController_5)); }
	inline CharacterController_t4094781467 * get_characterController_5() const { return ___characterController_5; }
	inline CharacterController_t4094781467 ** get_address_of_characterController_5() { return &___characterController_5; }
	inline void set_characterController_5(CharacterController_t4094781467 * value)
	{
		___characterController_5 = value;
		Il2CppCodeGenWriteBarrier(&___characterController_5, value);
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(BallRunPlayer_t2837385031, ___startPosition_6)); }
	inline Vector3_t2243707580  get_startPosition_6() const { return ___startPosition_6; }
	inline Vector3_t2243707580 * get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(Vector3_t2243707580  value)
	{
		___startPosition_6 = value;
	}

	inline static int32_t get_offset_of_isJump_7() { return static_cast<int32_t>(offsetof(BallRunPlayer_t2837385031, ___isJump_7)); }
	inline bool get_isJump_7() const { return ___isJump_7; }
	inline bool* get_address_of_isJump_7() { return &___isJump_7; }
	inline void set_isJump_7(bool value)
	{
		___isJump_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
