﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetMinPinchLength
struct  EasyTouchGetMinPinchLength_t790063589  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetMinPinchLength::length
	FsmFloat_t937133978 * ___length_11;

public:
	inline static int32_t get_offset_of_length_11() { return static_cast<int32_t>(offsetof(EasyTouchGetMinPinchLength_t790063589, ___length_11)); }
	inline FsmFloat_t937133978 * get_length_11() const { return ___length_11; }
	inline FsmFloat_t937133978 ** get_address_of_length_11() { return &___length_11; }
	inline void set_length_11(FsmFloat_t937133978 * value)
	{
		___length_11 = value;
		Il2CppCodeGenWriteBarrier(&___length_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
