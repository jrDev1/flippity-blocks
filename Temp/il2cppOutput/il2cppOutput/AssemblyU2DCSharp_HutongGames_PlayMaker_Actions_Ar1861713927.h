﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject
struct  ArrayListGetClosestGameObject_t1861713927  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::distanceFrom
	FsmGameObject_t3097142863 * ___distanceFrom_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::orDistanceFromVector3
	FsmVector3_t3996534004 * ___orDistanceFromVector3_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::everyframe
	bool ___everyframe_16;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::closestGameObject
	FsmGameObject_t3097142863 * ___closestGameObject_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetClosestGameObject::closestIndex
	FsmInt_t1273009179 * ___closestIndex_18;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_distanceFrom_14() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___distanceFrom_14)); }
	inline FsmGameObject_t3097142863 * get_distanceFrom_14() const { return ___distanceFrom_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_distanceFrom_14() { return &___distanceFrom_14; }
	inline void set_distanceFrom_14(FsmGameObject_t3097142863 * value)
	{
		___distanceFrom_14 = value;
		Il2CppCodeGenWriteBarrier(&___distanceFrom_14, value);
	}

	inline static int32_t get_offset_of_orDistanceFromVector3_15() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___orDistanceFromVector3_15)); }
	inline FsmVector3_t3996534004 * get_orDistanceFromVector3_15() const { return ___orDistanceFromVector3_15; }
	inline FsmVector3_t3996534004 ** get_address_of_orDistanceFromVector3_15() { return &___orDistanceFromVector3_15; }
	inline void set_orDistanceFromVector3_15(FsmVector3_t3996534004 * value)
	{
		___orDistanceFromVector3_15 = value;
		Il2CppCodeGenWriteBarrier(&___orDistanceFromVector3_15, value);
	}

	inline static int32_t get_offset_of_everyframe_16() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___everyframe_16)); }
	inline bool get_everyframe_16() const { return ___everyframe_16; }
	inline bool* get_address_of_everyframe_16() { return &___everyframe_16; }
	inline void set_everyframe_16(bool value)
	{
		___everyframe_16 = value;
	}

	inline static int32_t get_offset_of_closestGameObject_17() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___closestGameObject_17)); }
	inline FsmGameObject_t3097142863 * get_closestGameObject_17() const { return ___closestGameObject_17; }
	inline FsmGameObject_t3097142863 ** get_address_of_closestGameObject_17() { return &___closestGameObject_17; }
	inline void set_closestGameObject_17(FsmGameObject_t3097142863 * value)
	{
		___closestGameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___closestGameObject_17, value);
	}

	inline static int32_t get_offset_of_closestIndex_18() { return static_cast<int32_t>(offsetof(ArrayListGetClosestGameObject_t1861713927, ___closestIndex_18)); }
	inline FsmInt_t1273009179 * get_closestIndex_18() const { return ___closestIndex_18; }
	inline FsmInt_t1273009179 ** get_address_of_closestIndex_18() { return &___closestIndex_18; }
	inline void set_closestIndex_18(FsmInt_t1273009179 * value)
	{
		___closestIndex_18 = value;
		Il2CppCodeGenWriteBarrier(&___closestIndex_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
