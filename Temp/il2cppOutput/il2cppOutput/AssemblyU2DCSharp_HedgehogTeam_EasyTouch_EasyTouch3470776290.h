﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<RaiseEvent>c__AnonStorey2
struct  U3CRaiseEventU3Ec__AnonStorey2_t3470776290  : public Il2CppObject
{
public:
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch/<RaiseEvent>c__AnonStorey2::gesture
	Gesture_t367995397 * ___gesture_0;

public:
	inline static int32_t get_offset_of_gesture_0() { return static_cast<int32_t>(offsetof(U3CRaiseEventU3Ec__AnonStorey2_t3470776290, ___gesture_0)); }
	inline Gesture_t367995397 * get_gesture_0() const { return ___gesture_0; }
	inline Gesture_t367995397 ** get_address_of_gesture_0() { return &___gesture_0; }
	inline void set_gesture_0(Gesture_t367995397 * value)
	{
		___gesture_0 = value;
		Il2CppCodeGenWriteBarrier(&___gesture_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
