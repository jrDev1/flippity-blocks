﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtGetPoolInstancesCount
struct  PmtGetPoolInstancesCount_t2745957592  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtGetPoolInstancesCount::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.PmtGetPoolInstancesCount::instancesCount
	FsmInt_t1273009179 * ___instancesCount_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtGetPoolInstancesCount::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_13;
	// System.Boolean HutongGames.PlayMaker.Actions.PmtGetPoolInstancesCount::everyFrame
	bool ___everyFrame_14;
	// PathologicalGames.SpawnPool HutongGames.PlayMaker.Actions.PmtGetPoolInstancesCount::_pool
	SpawnPool_t2419717525 * ____pool_15;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtGetPoolInstancesCount_t2745957592, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_instancesCount_12() { return static_cast<int32_t>(offsetof(PmtGetPoolInstancesCount_t2745957592, ___instancesCount_12)); }
	inline FsmInt_t1273009179 * get_instancesCount_12() const { return ___instancesCount_12; }
	inline FsmInt_t1273009179 ** get_address_of_instancesCount_12() { return &___instancesCount_12; }
	inline void set_instancesCount_12(FsmInt_t1273009179 * value)
	{
		___instancesCount_12 = value;
		Il2CppCodeGenWriteBarrier(&___instancesCount_12, value);
	}

	inline static int32_t get_offset_of_failureEvent_13() { return static_cast<int32_t>(offsetof(PmtGetPoolInstancesCount_t2745957592, ___failureEvent_13)); }
	inline FsmEvent_t1258573736 * get_failureEvent_13() const { return ___failureEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_13() { return &___failureEvent_13; }
	inline void set_failureEvent_13(FsmEvent_t1258573736 * value)
	{
		___failureEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(PmtGetPoolInstancesCount_t2745957592, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}

	inline static int32_t get_offset_of__pool_15() { return static_cast<int32_t>(offsetof(PmtGetPoolInstancesCount_t2745957592, ____pool_15)); }
	inline SpawnPool_t2419717525 * get__pool_15() const { return ____pool_15; }
	inline SpawnPool_t2419717525 ** get_address_of__pool_15() { return &____pool_15; }
	inline void set__pool_15(SpawnPool_t2419717525 * value)
	{
		____pool_15 = value;
		Il2CppCodeGenWriteBarrier(&____pool_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
