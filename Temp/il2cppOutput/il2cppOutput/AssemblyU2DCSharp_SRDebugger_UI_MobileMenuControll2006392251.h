﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRDebugger.UI.Other.SRTabController
struct SRTabController_t1363238198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.MobileMenuController
struct  MobileMenuController_t2006392251  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.UI.Button SRDebugger.UI.MobileMenuController::_closeButton
	Button_t2872111280 * ____closeButton_9;
	// System.Single SRDebugger.UI.MobileMenuController::_maxMenuWidth
	float ____maxMenuWidth_10;
	// System.Single SRDebugger.UI.MobileMenuController::_peekAmount
	float ____peekAmount_11;
	// System.Single SRDebugger.UI.MobileMenuController::_targetXPosition
	float ____targetXPosition_12;
	// UnityEngine.RectTransform SRDebugger.UI.MobileMenuController::Content
	RectTransform_t3349966182 * ___Content_13;
	// UnityEngine.RectTransform SRDebugger.UI.MobileMenuController::Menu
	RectTransform_t3349966182 * ___Menu_14;
	// UnityEngine.UI.Button SRDebugger.UI.MobileMenuController::OpenButton
	Button_t2872111280 * ___OpenButton_15;
	// SRDebugger.UI.Other.SRTabController SRDebugger.UI.MobileMenuController::TabController
	SRTabController_t1363238198 * ___TabController_16;

public:
	inline static int32_t get_offset_of__closeButton_9() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ____closeButton_9)); }
	inline Button_t2872111280 * get__closeButton_9() const { return ____closeButton_9; }
	inline Button_t2872111280 ** get_address_of__closeButton_9() { return &____closeButton_9; }
	inline void set__closeButton_9(Button_t2872111280 * value)
	{
		____closeButton_9 = value;
		Il2CppCodeGenWriteBarrier(&____closeButton_9, value);
	}

	inline static int32_t get_offset_of__maxMenuWidth_10() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ____maxMenuWidth_10)); }
	inline float get__maxMenuWidth_10() const { return ____maxMenuWidth_10; }
	inline float* get_address_of__maxMenuWidth_10() { return &____maxMenuWidth_10; }
	inline void set__maxMenuWidth_10(float value)
	{
		____maxMenuWidth_10 = value;
	}

	inline static int32_t get_offset_of__peekAmount_11() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ____peekAmount_11)); }
	inline float get__peekAmount_11() const { return ____peekAmount_11; }
	inline float* get_address_of__peekAmount_11() { return &____peekAmount_11; }
	inline void set__peekAmount_11(float value)
	{
		____peekAmount_11 = value;
	}

	inline static int32_t get_offset_of__targetXPosition_12() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ____targetXPosition_12)); }
	inline float get__targetXPosition_12() const { return ____targetXPosition_12; }
	inline float* get_address_of__targetXPosition_12() { return &____targetXPosition_12; }
	inline void set__targetXPosition_12(float value)
	{
		____targetXPosition_12 = value;
	}

	inline static int32_t get_offset_of_Content_13() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ___Content_13)); }
	inline RectTransform_t3349966182 * get_Content_13() const { return ___Content_13; }
	inline RectTransform_t3349966182 ** get_address_of_Content_13() { return &___Content_13; }
	inline void set_Content_13(RectTransform_t3349966182 * value)
	{
		___Content_13 = value;
		Il2CppCodeGenWriteBarrier(&___Content_13, value);
	}

	inline static int32_t get_offset_of_Menu_14() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ___Menu_14)); }
	inline RectTransform_t3349966182 * get_Menu_14() const { return ___Menu_14; }
	inline RectTransform_t3349966182 ** get_address_of_Menu_14() { return &___Menu_14; }
	inline void set_Menu_14(RectTransform_t3349966182 * value)
	{
		___Menu_14 = value;
		Il2CppCodeGenWriteBarrier(&___Menu_14, value);
	}

	inline static int32_t get_offset_of_OpenButton_15() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ___OpenButton_15)); }
	inline Button_t2872111280 * get_OpenButton_15() const { return ___OpenButton_15; }
	inline Button_t2872111280 ** get_address_of_OpenButton_15() { return &___OpenButton_15; }
	inline void set_OpenButton_15(Button_t2872111280 * value)
	{
		___OpenButton_15 = value;
		Il2CppCodeGenWriteBarrier(&___OpenButton_15, value);
	}

	inline static int32_t get_offset_of_TabController_16() { return static_cast<int32_t>(offsetof(MobileMenuController_t2006392251, ___TabController_16)); }
	inline SRTabController_t1363238198 * get_TabController_16() const { return ___TabController_16; }
	inline SRTabController_t1363238198 ** get_address_of_TabController_16() { return &___TabController_16; }
	inline void set_TabController_16(SRTabController_t1363238198 * value)
	{
		___TabController_16 = value;
		Il2CppCodeGenWriteBarrier(&___TabController_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
