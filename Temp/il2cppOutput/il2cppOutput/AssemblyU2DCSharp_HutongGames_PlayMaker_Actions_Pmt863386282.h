﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtGetPoolGroup
struct  PmtGetPoolGroup_t863386282  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtGetPoolGroup::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtGetPoolGroup::poolGroup
	FsmGameObject_t3097142863 * ___poolGroup_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtGetPoolGroup::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_13;
	// PathologicalGames.SpawnPool HutongGames.PlayMaker.Actions.PmtGetPoolGroup::_pool
	SpawnPool_t2419717525 * ____pool_14;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtGetPoolGroup_t863386282, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_poolGroup_12() { return static_cast<int32_t>(offsetof(PmtGetPoolGroup_t863386282, ___poolGroup_12)); }
	inline FsmGameObject_t3097142863 * get_poolGroup_12() const { return ___poolGroup_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_poolGroup_12() { return &___poolGroup_12; }
	inline void set_poolGroup_12(FsmGameObject_t3097142863 * value)
	{
		___poolGroup_12 = value;
		Il2CppCodeGenWriteBarrier(&___poolGroup_12, value);
	}

	inline static int32_t get_offset_of_failureEvent_13() { return static_cast<int32_t>(offsetof(PmtGetPoolGroup_t863386282, ___failureEvent_13)); }
	inline FsmEvent_t1258573736 * get_failureEvent_13() const { return ___failureEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_13() { return &___failureEvent_13; }
	inline void set_failureEvent_13(FsmEvent_t1258573736 * value)
	{
		___failureEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_13, value);
	}

	inline static int32_t get_offset_of__pool_14() { return static_cast<int32_t>(offsetof(PmtGetPoolGroup_t863386282, ____pool_14)); }
	inline SpawnPool_t2419717525 * get__pool_14() const { return ____pool_14; }
	inline SpawnPool_t2419717525 ** get_address_of__pool_14() { return &____pool_14; }
	inline void set__pool_14(SpawnPool_t2419717525 * value)
	{
		____pool_14 = value;
		Il2CppCodeGenWriteBarrier(&____pool_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
