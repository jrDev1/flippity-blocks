﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_UnfinityGames_U2DEX_u2dexGrid_Gr3851649610.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnfinityGames.U2DEX.u2dexGrid
struct  u2dexGrid_t3875635992  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 UnfinityGames.U2DEX.u2dexGrid::gridSize
	Vector2_t2243707579  ___gridSize_2;
	// UnityEngine.Color UnfinityGames.U2DEX.u2dexGrid::color
	Color_t2020392075  ___color_3;
	// UnfinityGames.U2DEX.u2dexGrid/GridScale UnfinityGames.U2DEX.u2dexGrid::gridScale
	int32_t ___gridScale_4;
	// System.Single UnfinityGames.U2DEX.u2dexGrid::lineLength
	float ___lineLength_5;
	// System.Single UnfinityGames.U2DEX.u2dexGrid::maximumDistance
	float ___maximumDistance_7;

public:
	inline static int32_t get_offset_of_gridSize_2() { return static_cast<int32_t>(offsetof(u2dexGrid_t3875635992, ___gridSize_2)); }
	inline Vector2_t2243707579  get_gridSize_2() const { return ___gridSize_2; }
	inline Vector2_t2243707579 * get_address_of_gridSize_2() { return &___gridSize_2; }
	inline void set_gridSize_2(Vector2_t2243707579  value)
	{
		___gridSize_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(u2dexGrid_t3875635992, ___color_3)); }
	inline Color_t2020392075  get_color_3() const { return ___color_3; }
	inline Color_t2020392075 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color_t2020392075  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_gridScale_4() { return static_cast<int32_t>(offsetof(u2dexGrid_t3875635992, ___gridScale_4)); }
	inline int32_t get_gridScale_4() const { return ___gridScale_4; }
	inline int32_t* get_address_of_gridScale_4() { return &___gridScale_4; }
	inline void set_gridScale_4(int32_t value)
	{
		___gridScale_4 = value;
	}

	inline static int32_t get_offset_of_lineLength_5() { return static_cast<int32_t>(offsetof(u2dexGrid_t3875635992, ___lineLength_5)); }
	inline float get_lineLength_5() const { return ___lineLength_5; }
	inline float* get_address_of_lineLength_5() { return &___lineLength_5; }
	inline void set_lineLength_5(float value)
	{
		___lineLength_5 = value;
	}

	inline static int32_t get_offset_of_maximumDistance_7() { return static_cast<int32_t>(offsetof(u2dexGrid_t3875635992, ___maximumDistance_7)); }
	inline float get_maximumDistance_7() const { return ___maximumDistance_7; }
	inline float* get_address_of_maximumDistance_7() { return &___maximumDistance_7; }
	inline void set_maximumDistance_7(float value)
	{
		___maximumDistance_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
