﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen2068033852.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRSceneServiceBase`2<System.Object,System.Object>
struct  SRSceneServiceBase_2_t4131603446  : public SRServiceBase_1_t2068033852
{
public:
	// TImpl SRF.Service.SRSceneServiceBase`2::_rootObject
	Il2CppObject * ____rootObject_9;

public:
	inline static int32_t get_offset_of__rootObject_9() { return static_cast<int32_t>(offsetof(SRSceneServiceBase_2_t4131603446, ____rootObject_9)); }
	inline Il2CppObject * get__rootObject_9() const { return ____rootObject_9; }
	inline Il2CppObject ** get_address_of__rootObject_9() { return &____rootObject_9; }
	inline void set__rootObject_9(Il2CppObject * value)
	{
		____rootObject_9 = value;
		Il2CppCodeGenWriteBarrier(&____rootObject_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
