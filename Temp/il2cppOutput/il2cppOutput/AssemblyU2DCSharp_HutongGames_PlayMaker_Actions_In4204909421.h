﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntSubtract
struct  IntSubtract_t4204909421  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntSubtract::intVariable
	FsmInt_t1273009179 * ___intVariable_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntSubtract::subtract
	FsmInt_t1273009179 * ___subtract_12;
	// System.Boolean HutongGames.PlayMaker.Actions.IntSubtract::everyFrame
	bool ___everyFrame_13;
	// System.Boolean HutongGames.PlayMaker.Actions.IntSubtract::perSecond
	bool ___perSecond_14;
	// System.Single HutongGames.PlayMaker.Actions.IntSubtract::_acc
	float ____acc_15;

public:
	inline static int32_t get_offset_of_intVariable_11() { return static_cast<int32_t>(offsetof(IntSubtract_t4204909421, ___intVariable_11)); }
	inline FsmInt_t1273009179 * get_intVariable_11() const { return ___intVariable_11; }
	inline FsmInt_t1273009179 ** get_address_of_intVariable_11() { return &___intVariable_11; }
	inline void set_intVariable_11(FsmInt_t1273009179 * value)
	{
		___intVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_11, value);
	}

	inline static int32_t get_offset_of_subtract_12() { return static_cast<int32_t>(offsetof(IntSubtract_t4204909421, ___subtract_12)); }
	inline FsmInt_t1273009179 * get_subtract_12() const { return ___subtract_12; }
	inline FsmInt_t1273009179 ** get_address_of_subtract_12() { return &___subtract_12; }
	inline void set_subtract_12(FsmInt_t1273009179 * value)
	{
		___subtract_12 = value;
		Il2CppCodeGenWriteBarrier(&___subtract_12, value);
	}

	inline static int32_t get_offset_of_everyFrame_13() { return static_cast<int32_t>(offsetof(IntSubtract_t4204909421, ___everyFrame_13)); }
	inline bool get_everyFrame_13() const { return ___everyFrame_13; }
	inline bool* get_address_of_everyFrame_13() { return &___everyFrame_13; }
	inline void set_everyFrame_13(bool value)
	{
		___everyFrame_13 = value;
	}

	inline static int32_t get_offset_of_perSecond_14() { return static_cast<int32_t>(offsetof(IntSubtract_t4204909421, ___perSecond_14)); }
	inline bool get_perSecond_14() const { return ___perSecond_14; }
	inline bool* get_address_of_perSecond_14() { return &___perSecond_14; }
	inline void set_perSecond_14(bool value)
	{
		___perSecond_14 = value;
	}

	inline static int32_t get_offset_of__acc_15() { return static_cast<int32_t>(offsetof(IntSubtract_t4204909421, ____acc_15)); }
	inline float get__acc_15() const { return ____acc_15; }
	inline float* get_address_of__acc_15() { return &____acc_15; }
	inline void set__acc_15(float value)
	{
		____acc_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
