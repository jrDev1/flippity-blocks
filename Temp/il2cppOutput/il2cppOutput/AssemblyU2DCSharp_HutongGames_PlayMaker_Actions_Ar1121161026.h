﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.VariableType[]
struct VariableTypeU5BU5D_t4294461311;
// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetMinValue
struct  ArrayListGetMinValue_t1121161026  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetMinValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetMinValue::reference
	FsmString_t2414474701 * ___reference_14;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetMinValue::everyframe
	bool ___everyframe_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayListGetMinValue::minimumValue
	FsmVar_t2872592513 * ___minimumValue_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetMinValue::minimumValueIndex
	FsmInt_t1273009179 * ___minimumValueIndex_17;

public:
	inline static int32_t get_offset_of_gameObject_13() { return static_cast<int32_t>(offsetof(ArrayListGetMinValue_t1121161026, ___gameObject_13)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_13() const { return ___gameObject_13; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_13() { return &___gameObject_13; }
	inline void set_gameObject_13(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_13 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_13, value);
	}

	inline static int32_t get_offset_of_reference_14() { return static_cast<int32_t>(offsetof(ArrayListGetMinValue_t1121161026, ___reference_14)); }
	inline FsmString_t2414474701 * get_reference_14() const { return ___reference_14; }
	inline FsmString_t2414474701 ** get_address_of_reference_14() { return &___reference_14; }
	inline void set_reference_14(FsmString_t2414474701 * value)
	{
		___reference_14 = value;
		Il2CppCodeGenWriteBarrier(&___reference_14, value);
	}

	inline static int32_t get_offset_of_everyframe_15() { return static_cast<int32_t>(offsetof(ArrayListGetMinValue_t1121161026, ___everyframe_15)); }
	inline bool get_everyframe_15() const { return ___everyframe_15; }
	inline bool* get_address_of_everyframe_15() { return &___everyframe_15; }
	inline void set_everyframe_15(bool value)
	{
		___everyframe_15 = value;
	}

	inline static int32_t get_offset_of_minimumValue_16() { return static_cast<int32_t>(offsetof(ArrayListGetMinValue_t1121161026, ___minimumValue_16)); }
	inline FsmVar_t2872592513 * get_minimumValue_16() const { return ___minimumValue_16; }
	inline FsmVar_t2872592513 ** get_address_of_minimumValue_16() { return &___minimumValue_16; }
	inline void set_minimumValue_16(FsmVar_t2872592513 * value)
	{
		___minimumValue_16 = value;
		Il2CppCodeGenWriteBarrier(&___minimumValue_16, value);
	}

	inline static int32_t get_offset_of_minimumValueIndex_17() { return static_cast<int32_t>(offsetof(ArrayListGetMinValue_t1121161026, ___minimumValueIndex_17)); }
	inline FsmInt_t1273009179 * get_minimumValueIndex_17() const { return ___minimumValueIndex_17; }
	inline FsmInt_t1273009179 ** get_address_of_minimumValueIndex_17() { return &___minimumValueIndex_17; }
	inline void set_minimumValueIndex_17(FsmInt_t1273009179 * value)
	{
		___minimumValueIndex_17 = value;
		Il2CppCodeGenWriteBarrier(&___minimumValueIndex_17, value);
	}
};

struct ArrayListGetMinValue_t1121161026_StaticFields
{
public:
	// HutongGames.PlayMaker.VariableType[] HutongGames.PlayMaker.Actions.ArrayListGetMinValue::supportedTypes
	VariableTypeU5BU5D_t4294461311* ___supportedTypes_12;

public:
	inline static int32_t get_offset_of_supportedTypes_12() { return static_cast<int32_t>(offsetof(ArrayListGetMinValue_t1121161026_StaticFields, ___supportedTypes_12)); }
	inline VariableTypeU5BU5D_t4294461311* get_supportedTypes_12() const { return ___supportedTypes_12; }
	inline VariableTypeU5BU5D_t4294461311** get_address_of_supportedTypes_12() { return &___supportedTypes_12; }
	inline void set_supportedTypes_12(VariableTypeU5BU5D_t4294461311* value)
	{
		___supportedTypes_12 = value;
		Il2CppCodeGenWriteBarrier(&___supportedTypes_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
