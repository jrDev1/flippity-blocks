﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_U378386173.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget
struct  PlayMakerFsmTarget_t1869733351  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.ProxyFsmTarget HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::target
	int32_t ___target_0;
	// UnityEngine.GameObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::fsmName
	String_t* ___fsmName_2;
	// PlayMakerFSM HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::_fsmComponent
	PlayMakerFSM_t437737208 * ____fsmComponent_3;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmTarget::_initialized
	bool ____initialized_4;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(PlayMakerFsmTarget_t1869733351, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(PlayMakerFsmTarget_t1869733351, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}

	inline static int32_t get_offset_of_fsmName_2() { return static_cast<int32_t>(offsetof(PlayMakerFsmTarget_t1869733351, ___fsmName_2)); }
	inline String_t* get_fsmName_2() const { return ___fsmName_2; }
	inline String_t** get_address_of_fsmName_2() { return &___fsmName_2; }
	inline void set_fsmName_2(String_t* value)
	{
		___fsmName_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_2, value);
	}

	inline static int32_t get_offset_of__fsmComponent_3() { return static_cast<int32_t>(offsetof(PlayMakerFsmTarget_t1869733351, ____fsmComponent_3)); }
	inline PlayMakerFSM_t437737208 * get__fsmComponent_3() const { return ____fsmComponent_3; }
	inline PlayMakerFSM_t437737208 ** get_address_of__fsmComponent_3() { return &____fsmComponent_3; }
	inline void set__fsmComponent_3(PlayMakerFSM_t437737208 * value)
	{
		____fsmComponent_3 = value;
		Il2CppCodeGenWriteBarrier(&____fsmComponent_3, value);
	}

	inline static int32_t get_offset_of__initialized_4() { return static_cast<int32_t>(offsetof(PlayMakerFsmTarget_t1869733351, ____initialized_4)); }
	inline bool get__initialized_4() const { return ____initialized_4; }
	inline bool* get_address_of__initialized_4() { return &____initialized_4; }
	inline void set__initialized_4(bool value)
	{
		____initialized_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
