﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen3673400906.h"
#include "AssemblyU2DCSharp_SRDebugger_PinAlignment3550534810.h"

// SRDebugger.UI.Other.TriggerRoot
struct TriggerRoot_t735737282;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t606618774;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.DebugTriggerImpl
struct  DebugTriggerImpl_t2290374229  : public SRServiceBase_1_t3673400906
{
public:
	// SRDebugger.PinAlignment SRDebugger.Services.Implementation.DebugTriggerImpl::_position
	int32_t ____position_9;
	// SRDebugger.UI.Other.TriggerRoot SRDebugger.Services.Implementation.DebugTriggerImpl::_trigger
	TriggerRoot_t735737282 * ____trigger_10;

public:
	inline static int32_t get_offset_of__position_9() { return static_cast<int32_t>(offsetof(DebugTriggerImpl_t2290374229, ____position_9)); }
	inline int32_t get__position_9() const { return ____position_9; }
	inline int32_t* get_address_of__position_9() { return &____position_9; }
	inline void set__position_9(int32_t value)
	{
		____position_9 = value;
	}

	inline static int32_t get_offset_of__trigger_10() { return static_cast<int32_t>(offsetof(DebugTriggerImpl_t2290374229, ____trigger_10)); }
	inline TriggerRoot_t735737282 * get__trigger_10() const { return ____trigger_10; }
	inline TriggerRoot_t735737282 ** get_address_of__trigger_10() { return &____trigger_10; }
	inline void set__trigger_10(TriggerRoot_t735737282 * value)
	{
		____trigger_10 = value;
		Il2CppCodeGenWriteBarrier(&____trigger_10, value);
	}
};

struct DebugTriggerImpl_t2290374229_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene> SRDebugger.Services.Implementation.DebugTriggerImpl::<>f__mg$cache0
	UnityAction_2_t606618774 * ___U3CU3Ef__mgU24cache0_11;
	// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene> SRDebugger.Services.Implementation.DebugTriggerImpl::<>f__mg$cache1
	UnityAction_2_t606618774 * ___U3CU3Ef__mgU24cache1_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_11() { return static_cast<int32_t>(offsetof(DebugTriggerImpl_t2290374229_StaticFields, ___U3CU3Ef__mgU24cache0_11)); }
	inline UnityAction_2_t606618774 * get_U3CU3Ef__mgU24cache0_11() const { return ___U3CU3Ef__mgU24cache0_11; }
	inline UnityAction_2_t606618774 ** get_address_of_U3CU3Ef__mgU24cache0_11() { return &___U3CU3Ef__mgU24cache0_11; }
	inline void set_U3CU3Ef__mgU24cache0_11(UnityAction_2_t606618774 * value)
	{
		___U3CU3Ef__mgU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_12() { return static_cast<int32_t>(offsetof(DebugTriggerImpl_t2290374229_StaticFields, ___U3CU3Ef__mgU24cache1_12)); }
	inline UnityAction_2_t606618774 * get_U3CU3Ef__mgU24cache1_12() const { return ___U3CU3Ef__mgU24cache1_12; }
	inline UnityAction_2_t606618774 ** get_address_of_U3CU3Ef__mgU24cache1_12() { return &___U3CU3Ef__mgU24cache1_12; }
	inline void set_U3CU3Ef__mgU24cache1_12(UnityAction_2_t606618774 * value)
	{
		___U3CU3Ef__mgU24cache1_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
