﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// SRF.UI.Style
struct Style_t1578998053;
// SRF.UI.StyleRoot
struct StyleRoot_t1691968825;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.StyleComponent
struct  StyleComponent_t3036492378  : public SRMonoBehaviour_t2352136145
{
public:
	// SRF.UI.Style SRF.UI.StyleComponent::_activeStyle
	Style_t1578998053 * ____activeStyle_8;
	// SRF.UI.StyleRoot SRF.UI.StyleComponent::_cachedRoot
	StyleRoot_t1691968825 * ____cachedRoot_9;
	// UnityEngine.UI.Graphic SRF.UI.StyleComponent::_graphic
	Graphic_t2426225576 * ____graphic_10;
	// System.Boolean SRF.UI.StyleComponent::_hasStarted
	bool ____hasStarted_11;
	// UnityEngine.UI.Image SRF.UI.StyleComponent::_image
	Image_t2042527209 * ____image_12;
	// UnityEngine.UI.Selectable SRF.UI.StyleComponent::_selectable
	Selectable_t1490392188 * ____selectable_13;
	// System.String SRF.UI.StyleComponent::_styleKey
	String_t* ____styleKey_14;
	// System.Boolean SRF.UI.StyleComponent::IgnoreImage
	bool ___IgnoreImage_15;

public:
	inline static int32_t get_offset_of__activeStyle_8() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____activeStyle_8)); }
	inline Style_t1578998053 * get__activeStyle_8() const { return ____activeStyle_8; }
	inline Style_t1578998053 ** get_address_of__activeStyle_8() { return &____activeStyle_8; }
	inline void set__activeStyle_8(Style_t1578998053 * value)
	{
		____activeStyle_8 = value;
		Il2CppCodeGenWriteBarrier(&____activeStyle_8, value);
	}

	inline static int32_t get_offset_of__cachedRoot_9() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____cachedRoot_9)); }
	inline StyleRoot_t1691968825 * get__cachedRoot_9() const { return ____cachedRoot_9; }
	inline StyleRoot_t1691968825 ** get_address_of__cachedRoot_9() { return &____cachedRoot_9; }
	inline void set__cachedRoot_9(StyleRoot_t1691968825 * value)
	{
		____cachedRoot_9 = value;
		Il2CppCodeGenWriteBarrier(&____cachedRoot_9, value);
	}

	inline static int32_t get_offset_of__graphic_10() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____graphic_10)); }
	inline Graphic_t2426225576 * get__graphic_10() const { return ____graphic_10; }
	inline Graphic_t2426225576 ** get_address_of__graphic_10() { return &____graphic_10; }
	inline void set__graphic_10(Graphic_t2426225576 * value)
	{
		____graphic_10 = value;
		Il2CppCodeGenWriteBarrier(&____graphic_10, value);
	}

	inline static int32_t get_offset_of__hasStarted_11() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____hasStarted_11)); }
	inline bool get__hasStarted_11() const { return ____hasStarted_11; }
	inline bool* get_address_of__hasStarted_11() { return &____hasStarted_11; }
	inline void set__hasStarted_11(bool value)
	{
		____hasStarted_11 = value;
	}

	inline static int32_t get_offset_of__image_12() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____image_12)); }
	inline Image_t2042527209 * get__image_12() const { return ____image_12; }
	inline Image_t2042527209 ** get_address_of__image_12() { return &____image_12; }
	inline void set__image_12(Image_t2042527209 * value)
	{
		____image_12 = value;
		Il2CppCodeGenWriteBarrier(&____image_12, value);
	}

	inline static int32_t get_offset_of__selectable_13() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____selectable_13)); }
	inline Selectable_t1490392188 * get__selectable_13() const { return ____selectable_13; }
	inline Selectable_t1490392188 ** get_address_of__selectable_13() { return &____selectable_13; }
	inline void set__selectable_13(Selectable_t1490392188 * value)
	{
		____selectable_13 = value;
		Il2CppCodeGenWriteBarrier(&____selectable_13, value);
	}

	inline static int32_t get_offset_of__styleKey_14() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ____styleKey_14)); }
	inline String_t* get__styleKey_14() const { return ____styleKey_14; }
	inline String_t** get_address_of__styleKey_14() { return &____styleKey_14; }
	inline void set__styleKey_14(String_t* value)
	{
		____styleKey_14 = value;
		Il2CppCodeGenWriteBarrier(&____styleKey_14, value);
	}

	inline static int32_t get_offset_of_IgnoreImage_15() { return static_cast<int32_t>(offsetof(StyleComponent_t3036492378, ___IgnoreImage_15)); }
	inline bool get_IgnoreImage_15() const { return ___IgnoreImage_15; }
	inline bool* get_address_of_IgnoreImage_15() { return &___IgnoreImage_15; }
	inline void set_IgnoreImage_15(bool value)
	{
		___IgnoreImage_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
