﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ETCDPad_OnMoveSpeedHandler684365173.h"
#include "AssemblyU2DCSharp_ETCDPad_OnMoveEndHandler630021049.h"
#include "AssemblyU2DCSharp_ETCDPad_OnTouchStartHandler1271022964.h"
#include "AssemblyU2DCSharp_ETCDPad_OnTouchUPHandler595775759.h"
#include "AssemblyU2DCSharp_ETCDPad_OnDownUpHandler864821988.h"
#include "AssemblyU2DCSharp_ETCDPad_OnDownDownHandler715706099.h"
#include "AssemblyU2DCSharp_ETCDPad_OnDownLeftHandler1079412668.h"
#include "AssemblyU2DCSharp_ETCDPad_OnDownRightHandler41790613.h"
#include "AssemblyU2DCSharp_ETCDPad_OnPressUpHandler3799634613.h"
#include "AssemblyU2DCSharp_ETCDPad_OnPressDownHandler3776599780.h"
#include "AssemblyU2DCSharp_ETCDPad_OnPressLeftHandler3287812215.h"
#include "AssemblyU2DCSharp_ETCDPad_OnPressRightHandler1400205914.h"
#include "AssemblyU2DCSharp_ETCInput659679246.h"
#include "AssemblyU2DCSharp_ETCJoystick1870347408.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnMoveStartHandler2146188217.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnMoveSpeedHandler3386561924.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnMoveHandler1082130481.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnMoveEndHandler4076635866.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnTouchStartHandler4286579173.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnTouchUpHandler2064839710.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnDownUpHandler1598938991.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnDownDownHandler3463443096.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnDownLeftHandler2704089297.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnDownRightHandler3714567814.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnPressUpHandler2298493766.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnPressDownHandler2022684885.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnPressLeftHandler3600680754.h"
#include "AssemblyU2DCSharp_ETCJoystick_OnPressRightHandler3455550427.h"
#include "AssemblyU2DCSharp_ETCJoystick_JoystickArea1582798202.h"
#include "AssemblyU2DCSharp_ETCJoystick_JoystickType3209490911.h"
#include "AssemblyU2DCSharp_ETCJoystick_RadiusBase74140754.h"
#include "AssemblyU2DCSharp_ETCTouchPad3808707874.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnMoveStartHandler2914368815.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnMoveHandler1350262459.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnMoveSpeedHandler2314708994.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnMoveEndHandler1768956876.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnTouchStartHandler1876210983.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnTouchUPHandler1894489300.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnDownUpHandler3467319937.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnDownDownHandler1928173934.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnDownLeftHandler105288103.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnDownRightHandler3623032720.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnPressUpHandler3444680112.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnPressDownHandler827435863.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnPressLeftHandler1907891844.h"
#include "AssemblyU2DCSharp_ETCTouchPad_OnPressRightHandler1167042669.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1463536044.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr343917765.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr226112424.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr163014501.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3890704214.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3839887165.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3103064790.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4018671617.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2794349149.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3460516520.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr693110670.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr957002120.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1849268766.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2104755886.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4274965434.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1878537788.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3219836369.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1819720793.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3991006866.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1477928536.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1751962620.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1598637523.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar3323918276.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1262022945.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr350463858.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2392362357.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar1245773342.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4085186282.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar2643307797.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De1040878829.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fi2738033168.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3789948198.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_De3610008612.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3571497505.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha4129373816.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3919206191.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3568209330.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has970090917.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha2348818626.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3852786858.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha2501191333.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has296592325.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3128921806.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has607343316.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3319063194.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has220573833.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has738169715.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3451311881.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha2860985875.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha2220888646.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has637629068.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3832551928.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3369984408.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Has834694381.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (OnMoveSpeedHandler_t684365173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (OnMoveEndHandler_t630021049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (OnTouchStartHandler_t1271022964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (OnTouchUPHandler_t595775759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (OnDownUpHandler_t864821988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (OnDownDownHandler_t715706099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (OnDownLeftHandler_t1079412668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (OnDownRightHandler_t41790613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (OnPressUpHandler_t3799634613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (OnPressDownHandler_t3776599780), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (OnPressLeftHandler_t3287812215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (OnPressRightHandler_t1400205914), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (ETCInput_t659679246), -1, sizeof(ETCInput_t659679246_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[5] = 
{
	ETCInput_t659679246_StaticFields::get_offset_of__instance_2(),
	ETCInput_t659679246::get_offset_of_axes_3(),
	ETCInput_t659679246::get_offset_of_controls_4(),
	ETCInput_t659679246_StaticFields::get_offset_of_control_5(),
	ETCInput_t659679246_StaticFields::get_offset_of_axis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (ETCJoystick_t1870347408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[38] = 
{
	ETCJoystick_t1870347408::get_offset_of_onMoveStart_48(),
	ETCJoystick_t1870347408::get_offset_of_onMove_49(),
	ETCJoystick_t1870347408::get_offset_of_onMoveSpeed_50(),
	ETCJoystick_t1870347408::get_offset_of_onMoveEnd_51(),
	ETCJoystick_t1870347408::get_offset_of_onTouchStart_52(),
	ETCJoystick_t1870347408::get_offset_of_onTouchUp_53(),
	ETCJoystick_t1870347408::get_offset_of_OnDownUp_54(),
	ETCJoystick_t1870347408::get_offset_of_OnDownDown_55(),
	ETCJoystick_t1870347408::get_offset_of_OnDownLeft_56(),
	ETCJoystick_t1870347408::get_offset_of_OnDownRight_57(),
	ETCJoystick_t1870347408::get_offset_of_OnPressUp_58(),
	ETCJoystick_t1870347408::get_offset_of_OnPressDown_59(),
	ETCJoystick_t1870347408::get_offset_of_OnPressLeft_60(),
	ETCJoystick_t1870347408::get_offset_of_OnPressRight_61(),
	ETCJoystick_t1870347408::get_offset_of_joystickType_62(),
	ETCJoystick_t1870347408::get_offset_of_allowJoystickOverTouchPad_63(),
	ETCJoystick_t1870347408::get_offset_of_radiusBase_64(),
	ETCJoystick_t1870347408::get_offset_of_radiusBaseValue_65(),
	ETCJoystick_t1870347408::get_offset_of_axisX_66(),
	ETCJoystick_t1870347408::get_offset_of_axisY_67(),
	ETCJoystick_t1870347408::get_offset_of_thumb_68(),
	ETCJoystick_t1870347408::get_offset_of_joystickArea_69(),
	ETCJoystick_t1870347408::get_offset_of_userArea_70(),
	ETCJoystick_t1870347408::get_offset_of_isTurnAndMove_71(),
	ETCJoystick_t1870347408::get_offset_of_tmSpeed_72(),
	ETCJoystick_t1870347408::get_offset_of_tmAdditionnalRotation_73(),
	ETCJoystick_t1870347408::get_offset_of_tmMoveCurve_74(),
	ETCJoystick_t1870347408::get_offset_of_tmLockInJump_75(),
	ETCJoystick_t1870347408::get_offset_of_tmLastMove_76(),
	ETCJoystick_t1870347408::get_offset_of_thumbPosition_77(),
	ETCJoystick_t1870347408::get_offset_of_isDynamicActif_78(),
	ETCJoystick_t1870347408::get_offset_of_tmpAxis_79(),
	ETCJoystick_t1870347408::get_offset_of_OldTmpAxis_80(),
	ETCJoystick_t1870347408::get_offset_of_isOnTouch_81(),
	ETCJoystick_t1870347408::get_offset_of_isNoReturnThumb_82(),
	ETCJoystick_t1870347408::get_offset_of_noReturnPosition_83(),
	ETCJoystick_t1870347408::get_offset_of_noReturnOffset_84(),
	ETCJoystick_t1870347408::get_offset_of_isNoOffsetThumb_85(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (OnMoveStartHandler_t2146188217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (OnMoveSpeedHandler_t3386561924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (OnMoveHandler_t1082130481), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (OnMoveEndHandler_t4076635866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (OnTouchStartHandler_t4286579173), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (OnTouchUpHandler_t2064839710), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (OnDownUpHandler_t1598938991), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (OnDownDownHandler_t3463443096), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (OnDownLeftHandler_t2704089297), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (OnDownRightHandler_t3714567814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (OnPressUpHandler_t2298493766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (OnPressDownHandler_t2022684885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (OnPressLeftHandler_t3600680754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (OnPressRightHandler_t3455550427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (JoystickArea_t1582798202)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2328[11] = 
{
	JoystickArea_t1582798202::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (JoystickType_t3209490911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2329[3] = 
{
	JoystickType_t3209490911::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (RadiusBase_t74140754)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2330[4] = 
{
	RadiusBase_t74140754::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (ETCTouchPad_t3808707874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[24] = 
{
	ETCTouchPad_t3808707874::get_offset_of_onMoveStart_48(),
	ETCTouchPad_t3808707874::get_offset_of_onMove_49(),
	ETCTouchPad_t3808707874::get_offset_of_onMoveSpeed_50(),
	ETCTouchPad_t3808707874::get_offset_of_onMoveEnd_51(),
	ETCTouchPad_t3808707874::get_offset_of_onTouchStart_52(),
	ETCTouchPad_t3808707874::get_offset_of_onTouchUp_53(),
	ETCTouchPad_t3808707874::get_offset_of_OnDownUp_54(),
	ETCTouchPad_t3808707874::get_offset_of_OnDownDown_55(),
	ETCTouchPad_t3808707874::get_offset_of_OnDownLeft_56(),
	ETCTouchPad_t3808707874::get_offset_of_OnDownRight_57(),
	ETCTouchPad_t3808707874::get_offset_of_OnPressUp_58(),
	ETCTouchPad_t3808707874::get_offset_of_OnPressDown_59(),
	ETCTouchPad_t3808707874::get_offset_of_OnPressLeft_60(),
	ETCTouchPad_t3808707874::get_offset_of_OnPressRight_61(),
	ETCTouchPad_t3808707874::get_offset_of_axisX_62(),
	ETCTouchPad_t3808707874::get_offset_of_axisY_63(),
	ETCTouchPad_t3808707874::get_offset_of_isDPI_64(),
	ETCTouchPad_t3808707874::get_offset_of_cachedImage_65(),
	ETCTouchPad_t3808707874::get_offset_of_tmpAxis_66(),
	ETCTouchPad_t3808707874::get_offset_of_OldTmpAxis_67(),
	ETCTouchPad_t3808707874::get_offset_of_previousDargObject_68(),
	ETCTouchPad_t3808707874::get_offset_of_isOut_69(),
	ETCTouchPad_t3808707874::get_offset_of_isOnTouch_70(),
	ETCTouchPad_t3808707874::get_offset_of_cachedVisible_71(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (OnMoveStartHandler_t2914368815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (OnMoveHandler_t1350262459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (OnMoveSpeedHandler_t2314708994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (OnMoveEndHandler_t1768956876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (OnTouchStartHandler_t1876210983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (OnTouchUPHandler_t1894489300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (OnDownUpHandler_t3467319937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (OnDownDownHandler_t1928173934), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (OnDownLeftHandler_t105288103), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (OnDownRightHandler_t3623032720), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (OnPressUpHandler_t3444680112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (OnPressDownHandler_t827435863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (OnPressLeftHandler_t1907891844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (OnPressRightHandler_t1167042669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (ArrayListAdd_t1463536044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[5] = 
{
	ArrayListAdd_t1463536044::get_offset_of_gameObject_12(),
	ArrayListAdd_t1463536044::get_offset_of_reference_13(),
	ArrayListAdd_t1463536044::get_offset_of_variable_14(),
	ArrayListAdd_t1463536044::get_offset_of_convertIntToByte_15(),
	ArrayListAdd_t1463536044::get_offset_of_index_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (ArrayListAddRange_t343917765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[4] = 
{
	ArrayListAddRange_t343917765::get_offset_of_gameObject_12(),
	ArrayListAddRange_t343917765::get_offset_of_reference_13(),
	ArrayListAddRange_t343917765::get_offset_of_variables_14(),
	ArrayListAddRange_t343917765::get_offset_of_convertIntsToBytes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (ArrayListClear_t226112424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[2] = 
{
	ArrayListClear_t226112424::get_offset_of_gameObject_12(),
	ArrayListClear_t226112424::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (ArrayListConcat_t163014501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[4] = 
{
	ArrayListConcat_t163014501::get_offset_of_gameObject_12(),
	ArrayListConcat_t163014501::get_offset_of_reference_13(),
	ArrayListConcat_t163014501::get_offset_of_arrayListGameObjectTargets_14(),
	ArrayListConcat_t163014501::get_offset_of_referenceTargets_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (ArrayListContains_t3890704214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[6] = 
{
	ArrayListContains_t3890704214::get_offset_of_gameObject_12(),
	ArrayListContains_t3890704214::get_offset_of_reference_13(),
	ArrayListContains_t3890704214::get_offset_of_variable_14(),
	ArrayListContains_t3890704214::get_offset_of_isContained_15(),
	ArrayListContains_t3890704214::get_offset_of_isContainedEvent_16(),
	ArrayListContains_t3890704214::get_offset_of_isNotContainedEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (ArrayListCopyTo_t3839887165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[6] = 
{
	ArrayListCopyTo_t3839887165::get_offset_of_gameObject_12(),
	ArrayListCopyTo_t3839887165::get_offset_of_reference_13(),
	ArrayListCopyTo_t3839887165::get_offset_of_gameObjectTarget_14(),
	ArrayListCopyTo_t3839887165::get_offset_of_referenceTarget_15(),
	ArrayListCopyTo_t3839887165::get_offset_of_startIndex_16(),
	ArrayListCopyTo_t3839887165::get_offset_of_count_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (ArrayListCount_t3103064790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	ArrayListCount_t3103064790::get_offset_of_gameObject_12(),
	ArrayListCount_t3103064790::get_offset_of_reference_13(),
	ArrayListCount_t3103064790::get_offset_of_count_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ArrayListExists_t4018671617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[5] = 
{
	ArrayListExists_t4018671617::get_offset_of_gameObject_12(),
	ArrayListExists_t4018671617::get_offset_of_reference_13(),
	ArrayListExists_t4018671617::get_offset_of_doesExists_14(),
	ArrayListExists_t4018671617::get_offset_of_doesExistsEvent_15(),
	ArrayListExists_t4018671617::get_offset_of_doesNotExistsEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (ArrayListGet_t2794349149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	ArrayListGet_t2794349149::get_offset_of_gameObject_12(),
	ArrayListGet_t2794349149::get_offset_of_reference_13(),
	ArrayListGet_t2794349149::get_offset_of_atIndex_14(),
	ArrayListGet_t2794349149::get_offset_of_result_15(),
	ArrayListGet_t2794349149::get_offset_of_failureEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (ArrayListGetNext_t3460516520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[11] = 
{
	ArrayListGetNext_t3460516520::get_offset_of_gameObject_12(),
	ArrayListGetNext_t3460516520::get_offset_of_reference_13(),
	ArrayListGetNext_t3460516520::get_offset_of_reset_14(),
	ArrayListGetNext_t3460516520::get_offset_of_startIndex_15(),
	ArrayListGetNext_t3460516520::get_offset_of_endIndex_16(),
	ArrayListGetNext_t3460516520::get_offset_of_loopEvent_17(),
	ArrayListGetNext_t3460516520::get_offset_of_finishedEvent_18(),
	ArrayListGetNext_t3460516520::get_offset_of_failureEvent_19(),
	ArrayListGetNext_t3460516520::get_offset_of_currentIndex_20(),
	ArrayListGetNext_t3460516520::get_offset_of_result_21(),
	ArrayListGetNext_t3460516520::get_offset_of_nextItemIndex_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (ArrayListGetPrevious_t693110670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[12] = 
{
	ArrayListGetPrevious_t693110670::get_offset_of_gameObject_12(),
	ArrayListGetPrevious_t693110670::get_offset_of_reference_13(),
	ArrayListGetPrevious_t693110670::get_offset_of_reset_14(),
	ArrayListGetPrevious_t693110670::get_offset_of_startIndex_15(),
	ArrayListGetPrevious_t693110670::get_offset_of_endIndex_16(),
	ArrayListGetPrevious_t693110670::get_offset_of_loopEvent_17(),
	ArrayListGetPrevious_t693110670::get_offset_of_finishedEvent_18(),
	ArrayListGetPrevious_t693110670::get_offset_of_failureEvent_19(),
	ArrayListGetPrevious_t693110670::get_offset_of_currentIndex_20(),
	ArrayListGetPrevious_t693110670::get_offset_of_result_21(),
	ArrayListGetPrevious_t693110670::get_offset_of_nextItemIndex_22(),
	ArrayListGetPrevious_t693110670::get_offset_of_countBase_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (ArrayListGetRandom_t957002120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[5] = 
{
	ArrayListGetRandom_t957002120::get_offset_of_gameObject_12(),
	ArrayListGetRandom_t957002120::get_offset_of_reference_13(),
	ArrayListGetRandom_t957002120::get_offset_of_randomItem_14(),
	ArrayListGetRandom_t957002120::get_offset_of_randomIndex_15(),
	ArrayListGetRandom_t957002120::get_offset_of_failureEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (ArrayListIndexOf_t1849268766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[9] = 
{
	ArrayListIndexOf_t1849268766::get_offset_of_gameObject_12(),
	ArrayListIndexOf_t1849268766::get_offset_of_reference_13(),
	ArrayListIndexOf_t1849268766::get_offset_of_startIndex_14(),
	ArrayListIndexOf_t1849268766::get_offset_of_count_15(),
	ArrayListIndexOf_t1849268766::get_offset_of_variable_16(),
	ArrayListIndexOf_t1849268766::get_offset_of_indexOf_17(),
	ArrayListIndexOf_t1849268766::get_offset_of_itemFound_18(),
	ArrayListIndexOf_t1849268766::get_offset_of_itemNotFound_19(),
	ArrayListIndexOf_t1849268766::get_offset_of_failureEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ArrayListInsert_t2104755886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[6] = 
{
	ArrayListInsert_t2104755886::get_offset_of_gameObject_12(),
	ArrayListInsert_t2104755886::get_offset_of_reference_13(),
	ArrayListInsert_t2104755886::get_offset_of_index_14(),
	ArrayListInsert_t2104755886::get_offset_of_variable_15(),
	ArrayListInsert_t2104755886::get_offset_of_convertIntToByte_16(),
	ArrayListInsert_t2104755886::get_offset_of_failureEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (ArrayListIsEmpty_t4274965434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[5] = 
{
	ArrayListIsEmpty_t4274965434::get_offset_of_gameObject_12(),
	ArrayListIsEmpty_t4274965434::get_offset_of_reference_13(),
	ArrayListIsEmpty_t4274965434::get_offset_of_isEmpty_14(),
	ArrayListIsEmpty_t4274965434::get_offset_of_isEmptyEvent_15(),
	ArrayListIsEmpty_t4274965434::get_offset_of_isNotEmptyEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (ArrayListLastIndexOf_t1878537788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[9] = 
{
	ArrayListLastIndexOf_t1878537788::get_offset_of_gameObject_12(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_reference_13(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_startIndex_14(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_count_15(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_variable_16(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_lastIndexOf_17(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_itemFound_18(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_itemNotFound_19(),
	ArrayListLastIndexOf_t1878537788::get_offset_of_failureEvent_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (ArrayListGetRelative_t3219836369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[6] = 
{
	ArrayListGetRelative_t3219836369::get_offset_of_gameObject_12(),
	ArrayListGetRelative_t3219836369::get_offset_of_reference_13(),
	ArrayListGetRelative_t3219836369::get_offset_of_baseIndex_14(),
	ArrayListGetRelative_t3219836369::get_offset_of_increment_15(),
	ArrayListGetRelative_t3219836369::get_offset_of_result_16(),
	ArrayListGetRelative_t3219836369::get_offset_of_resultIndex_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (ArrayListRemove_t1819720793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[4] = 
{
	ArrayListRemove_t1819720793::get_offset_of_gameObject_12(),
	ArrayListRemove_t1819720793::get_offset_of_reference_13(),
	ArrayListRemove_t1819720793::get_offset_of_variable_14(),
	ArrayListRemove_t1819720793::get_offset_of_notFoundEvent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (ArrayListRemoveAt_t3991006866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[4] = 
{
	ArrayListRemoveAt_t3991006866::get_offset_of_gameObject_12(),
	ArrayListRemoveAt_t3991006866::get_offset_of_reference_13(),
	ArrayListRemoveAt_t3991006866::get_offset_of_index_14(),
	ArrayListRemoveAt_t3991006866::get_offset_of_failureEvent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ArrayListRemoveRange_t1477928536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[5] = 
{
	ArrayListRemoveRange_t1477928536::get_offset_of_gameObject_12(),
	ArrayListRemoveRange_t1477928536::get_offset_of_reference_13(),
	ArrayListRemoveRange_t1477928536::get_offset_of_index_14(),
	ArrayListRemoveRange_t1477928536::get_offset_of_count_15(),
	ArrayListRemoveRange_t1477928536::get_offset_of_failureEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ArrayListResetValues_t1751962620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[3] = 
{
	ArrayListResetValues_t1751962620::get_offset_of_gameObject_12(),
	ArrayListResetValues_t1751962620::get_offset_of_reference_13(),
	ArrayListResetValues_t1751962620::get_offset_of_resetValue_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (ArrayListReverse_t1598637523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[2] = 
{
	ArrayListReverse_t1598637523::get_offset_of_gameObject_12(),
	ArrayListReverse_t1598637523::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ArrayListRevertToSnapShot_t3323918276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[2] = 
{
	ArrayListRevertToSnapShot_t3323918276::get_offset_of_gameObject_12(),
	ArrayListRevertToSnapShot_t3323918276::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (ArrayListSet_t1262022945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[6] = 
{
	ArrayListSet_t1262022945::get_offset_of_gameObject_12(),
	ArrayListSet_t1262022945::get_offset_of_reference_13(),
	ArrayListSet_t1262022945::get_offset_of_atIndex_14(),
	ArrayListSet_t1262022945::get_offset_of_forceResizeIdNeeded_15(),
	ArrayListSet_t1262022945::get_offset_of_everyFrame_16(),
	ArrayListSet_t1262022945::get_offset_of_variable_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (ArrayListShuffle_t350463858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[4] = 
{
	ArrayListShuffle_t350463858::get_offset_of_gameObject_12(),
	ArrayListShuffle_t350463858::get_offset_of_reference_13(),
	ArrayListShuffle_t350463858::get_offset_of_startIndex_14(),
	ArrayListShuffle_t350463858::get_offset_of_shufflingRange_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ArrayListSort_t2392362357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[2] = 
{
	ArrayListSort_t2392362357::get_offset_of_gameObject_12(),
	ArrayListSort_t2392362357::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (ArrayListSwapItems_t1245773342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[5] = 
{
	ArrayListSwapItems_t1245773342::get_offset_of_gameObject_12(),
	ArrayListSwapItems_t1245773342::get_offset_of_reference_13(),
	ArrayListSwapItems_t1245773342::get_offset_of_index1_14(),
	ArrayListSwapItems_t1245773342::get_offset_of_index2_15(),
	ArrayListSwapItems_t1245773342::get_offset_of_failureEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (ArrayListTakeSnapShot_t4085186282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[2] = 
{
	ArrayListTakeSnapShot_t4085186282::get_offset_of_gameObject_12(),
	ArrayListTakeSnapShot_t4085186282::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ArrayListCreate_t2643307797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[5] = 
{
	ArrayListCreate_t2643307797::get_offset_of_gameObject_12(),
	ArrayListCreate_t2643307797::get_offset_of_reference_13(),
	ArrayListCreate_t2643307797::get_offset_of_removeOnExit_14(),
	ArrayListCreate_t2643307797::get_offset_of_alreadyExistsEvent_15(),
	ArrayListCreate_t2643307797::get_offset_of_addedComponent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (DestroyArrayList_t1040878829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[4] = 
{
	DestroyArrayList_t1040878829::get_offset_of_gameObject_12(),
	DestroyArrayList_t1040878829::get_offset_of_reference_13(),
	DestroyArrayList_t1040878829::get_offset_of_successEvent_14(),
	DestroyArrayList_t1040878829::get_offset_of_notFoundEvent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (FindArrayList_t2738033168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[4] = 
{
	FindArrayList_t2738033168::get_offset_of_ArrayListReference_11(),
	FindArrayList_t2738033168::get_offset_of_store_12(),
	FindArrayList_t2738033168::get_offset_of_foundEvent_13(),
	FindArrayList_t2738033168::get_offset_of_notFoundEvent_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (HashTableCreate_t3789948198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[5] = 
{
	HashTableCreate_t3789948198::get_offset_of_gameObject_12(),
	HashTableCreate_t3789948198::get_offset_of_reference_13(),
	HashTableCreate_t3789948198::get_offset_of_removeOnExit_14(),
	HashTableCreate_t3789948198::get_offset_of_alreadyExistsEvent_15(),
	HashTableCreate_t3789948198::get_offset_of_addedComponent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (DestroyHashTable_t3610008612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[4] = 
{
	DestroyHashTable_t3610008612::get_offset_of_gameObject_12(),
	DestroyHashTable_t3610008612::get_offset_of_reference_13(),
	DestroyHashTable_t3610008612::get_offset_of_successEvent_14(),
	DestroyHashTable_t3610008612::get_offset_of_notFoundEvent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (HashTableAdd_t3571497505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[7] = 
{
	HashTableAdd_t3571497505::get_offset_of_gameObject_12(),
	HashTableAdd_t3571497505::get_offset_of_reference_13(),
	HashTableAdd_t3571497505::get_offset_of_key_14(),
	HashTableAdd_t3571497505::get_offset_of_variable_15(),
	HashTableAdd_t3571497505::get_offset_of_convertIntToByte_16(),
	HashTableAdd_t3571497505::get_offset_of_successEvent_17(),
	HashTableAdd_t3571497505::get_offset_of_keyExistsAlreadyEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (HashTableAddMany_t4129373816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[7] = 
{
	HashTableAddMany_t4129373816::get_offset_of_gameObject_12(),
	HashTableAddMany_t4129373816::get_offset_of_reference_13(),
	HashTableAddMany_t4129373816::get_offset_of_keys_14(),
	HashTableAddMany_t4129373816::get_offset_of_variables_15(),
	HashTableAddMany_t4129373816::get_offset_of_convertIntsToBytes_16(),
	HashTableAddMany_t4129373816::get_offset_of_successEvent_17(),
	HashTableAddMany_t4129373816::get_offset_of_keyExistsAlreadyEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (HashTableClear_t3919206191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[2] = 
{
	HashTableClear_t3919206191::get_offset_of_gameObject_12(),
	HashTableClear_t3919206191::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (HashTableConcat_t3568209330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[5] = 
{
	HashTableConcat_t3568209330::get_offset_of_gameObject_12(),
	HashTableConcat_t3568209330::get_offset_of_reference_13(),
	HashTableConcat_t3568209330::get_offset_of_hashTableGameObjectTargets_14(),
	HashTableConcat_t3568209330::get_offset_of_referenceTargets_15(),
	HashTableConcat_t3568209330::get_offset_of_overwriteExistingKey_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (HashTableContains_t970090917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[6] = 
{
	HashTableContains_t970090917::get_offset_of_gameObject_12(),
	HashTableContains_t970090917::get_offset_of_reference_13(),
	HashTableContains_t970090917::get_offset_of_key_14(),
	HashTableContains_t970090917::get_offset_of_containsKey_15(),
	HashTableContains_t970090917::get_offset_of_keyFoundEvent_16(),
	HashTableContains_t970090917::get_offset_of_keyNotFoundEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (HashTableContainsKey_t2348818626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[6] = 
{
	HashTableContainsKey_t2348818626::get_offset_of_gameObject_12(),
	HashTableContainsKey_t2348818626::get_offset_of_reference_13(),
	HashTableContainsKey_t2348818626::get_offset_of_key_14(),
	HashTableContainsKey_t2348818626::get_offset_of_containsKey_15(),
	HashTableContainsKey_t2348818626::get_offset_of_keyFoundEvent_16(),
	HashTableContainsKey_t2348818626::get_offset_of_keyNotFoundEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (HashTableContainsValue_t3852786858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[6] = 
{
	HashTableContainsValue_t3852786858::get_offset_of_gameObject_12(),
	HashTableContainsValue_t3852786858::get_offset_of_reference_13(),
	HashTableContainsValue_t3852786858::get_offset_of_variable_14(),
	HashTableContainsValue_t3852786858::get_offset_of_containsValue_15(),
	HashTableContainsValue_t3852786858::get_offset_of_valueFoundEvent_16(),
	HashTableContainsValue_t3852786858::get_offset_of_valueNotFoundEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (HashTableCount_t2501191333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[3] = 
{
	HashTableCount_t2501191333::get_offset_of_gameObject_12(),
	HashTableCount_t2501191333::get_offset_of_reference_13(),
	HashTableCount_t2501191333::get_offset_of_count_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (HashTableEditKey_t296592325), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[6] = 
{
	HashTableEditKey_t296592325::get_offset_of_gameObject_12(),
	HashTableEditKey_t296592325::get_offset_of_reference_13(),
	HashTableEditKey_t296592325::get_offset_of_key_14(),
	HashTableEditKey_t296592325::get_offset_of_newKey_15(),
	HashTableEditKey_t296592325::get_offset_of_keyNotFoundEvent_16(),
	HashTableEditKey_t296592325::get_offset_of_newKeyExistsAlreadyEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (HashTableExists_t3128921806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[5] = 
{
	HashTableExists_t3128921806::get_offset_of_gameObject_12(),
	HashTableExists_t3128921806::get_offset_of_reference_13(),
	HashTableExists_t3128921806::get_offset_of_doesExists_14(),
	HashTableExists_t3128921806::get_offset_of_doesExistsEvent_15(),
	HashTableExists_t3128921806::get_offset_of_doesNotExistsEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (HashTableGet_t607343316), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[6] = 
{
	HashTableGet_t607343316::get_offset_of_gameObject_12(),
	HashTableGet_t607343316::get_offset_of_reference_13(),
	HashTableGet_t607343316::get_offset_of_key_14(),
	HashTableGet_t607343316::get_offset_of_result_15(),
	HashTableGet_t607343316::get_offset_of_KeyFoundEvent_16(),
	HashTableGet_t607343316::get_offset_of_KeyNotFoundEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (HashTableGetKeyFromValue_t3319063194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[6] = 
{
	HashTableGetKeyFromValue_t3319063194::get_offset_of_gameObject_12(),
	HashTableGetKeyFromValue_t3319063194::get_offset_of_reference_13(),
	HashTableGetKeyFromValue_t3319063194::get_offset_of_theValue_14(),
	HashTableGetKeyFromValue_t3319063194::get_offset_of_result_15(),
	HashTableGetKeyFromValue_t3319063194::get_offset_of_KeyFoundEvent_16(),
	HashTableGetKeyFromValue_t3319063194::get_offset_of_KeyNotFoundEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (HashTableGetMany_t220573833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[4] = 
{
	HashTableGetMany_t220573833::get_offset_of_gameObject_12(),
	HashTableGetMany_t220573833::get_offset_of_reference_13(),
	HashTableGetMany_t220573833::get_offset_of_keys_14(),
	HashTableGetMany_t220573833::get_offset_of_results_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (HashTableGetNext_t738169715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[12] = 
{
	HashTableGetNext_t738169715::get_offset_of_gameObject_12(),
	HashTableGetNext_t738169715::get_offset_of_reference_13(),
	HashTableGetNext_t738169715::get_offset_of_reset_14(),
	HashTableGetNext_t738169715::get_offset_of_startIndex_15(),
	HashTableGetNext_t738169715::get_offset_of_endIndex_16(),
	HashTableGetNext_t738169715::get_offset_of_loopEvent_17(),
	HashTableGetNext_t738169715::get_offset_of_finishedEvent_18(),
	HashTableGetNext_t738169715::get_offset_of_failureEvent_19(),
	HashTableGetNext_t738169715::get_offset_of_key_20(),
	HashTableGetNext_t738169715::get_offset_of_result_21(),
	HashTableGetNext_t738169715::get_offset_of__keys_22(),
	HashTableGetNext_t738169715::get_offset_of_nextItemIndex_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (HashTableGetRandom_t3451311881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[6] = 
{
	HashTableGetRandom_t3451311881::get_offset_of_gameObject_12(),
	HashTableGetRandom_t3451311881::get_offset_of_reference_13(),
	HashTableGetRandom_t3451311881::get_offset_of_failureEvent_14(),
	HashTableGetRandom_t3451311881::get_offset_of_key_15(),
	HashTableGetRandom_t3451311881::get_offset_of_result_16(),
	HashTableGetRandom_t3451311881::get_offset_of__keys_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (HashTableIsEmpty_t2860985875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[5] = 
{
	HashTableIsEmpty_t2860985875::get_offset_of_gameObject_12(),
	HashTableIsEmpty_t2860985875::get_offset_of_reference_13(),
	HashTableIsEmpty_t2860985875::get_offset_of_isEmpty_14(),
	HashTableIsEmpty_t2860985875::get_offset_of_isEmptyEvent_15(),
	HashTableIsEmpty_t2860985875::get_offset_of_isNotEmptyEvent_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (HashTableKeys_t2220888646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	HashTableKeys_t2220888646::get_offset_of_gameObject_12(),
	HashTableKeys_t2220888646::get_offset_of_reference_13(),
	HashTableKeys_t2220888646::get_offset_of_arrayListGameObject_14(),
	HashTableKeys_t2220888646::get_offset_of_arrayListReference_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (HashTableRemove_t637629068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[3] = 
{
	HashTableRemove_t637629068::get_offset_of_gameObject_12(),
	HashTableRemove_t637629068::get_offset_of_reference_13(),
	HashTableRemove_t637629068::get_offset_of_key_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (HashTableRevertSnapShot_t3832551928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[2] = 
{
	HashTableRevertSnapShot_t3832551928::get_offset_of_gameObject_12(),
	HashTableRevertSnapShot_t3832551928::get_offset_of_reference_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (HashTableSet_t3369984408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[5] = 
{
	HashTableSet_t3369984408::get_offset_of_gameObject_12(),
	HashTableSet_t3369984408::get_offset_of_reference_13(),
	HashTableSet_t3369984408::get_offset_of_key_14(),
	HashTableSet_t3369984408::get_offset_of_variable_15(),
	HashTableSet_t3369984408::get_offset_of_convertIntToByte_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (HashTableSetMany_t834694381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[5] = 
{
	HashTableSetMany_t834694381::get_offset_of_gameObject_12(),
	HashTableSetMany_t834694381::get_offset_of_reference_13(),
	HashTableSetMany_t834694381::get_offset_of_keys_14(),
	HashTableSetMany_t834694381::get_offset_of_variables_15(),
	HashTableSetMany_t834694381::get_offset_of_convertIntsToBytes_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
