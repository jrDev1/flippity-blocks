﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.RequiredFieldAttribute
struct  RequiredFieldAttribute_t2121822791  : public Attribute_t542643598
{
public:
	// System.Boolean SRF.RequiredFieldAttribute::_autoCreate
	bool ____autoCreate_0;
	// System.Boolean SRF.RequiredFieldAttribute::_autoSearch
	bool ____autoSearch_1;
	// System.Boolean SRF.RequiredFieldAttribute::_editorOnly
	bool ____editorOnly_2;

public:
	inline static int32_t get_offset_of__autoCreate_0() { return static_cast<int32_t>(offsetof(RequiredFieldAttribute_t2121822791, ____autoCreate_0)); }
	inline bool get__autoCreate_0() const { return ____autoCreate_0; }
	inline bool* get_address_of__autoCreate_0() { return &____autoCreate_0; }
	inline void set__autoCreate_0(bool value)
	{
		____autoCreate_0 = value;
	}

	inline static int32_t get_offset_of__autoSearch_1() { return static_cast<int32_t>(offsetof(RequiredFieldAttribute_t2121822791, ____autoSearch_1)); }
	inline bool get__autoSearch_1() const { return ____autoSearch_1; }
	inline bool* get_address_of__autoSearch_1() { return &____autoSearch_1; }
	inline void set__autoSearch_1(bool value)
	{
		____autoSearch_1 = value;
	}

	inline static int32_t get_offset_of__editorOnly_2() { return static_cast<int32_t>(offsetof(RequiredFieldAttribute_t2121822791, ____editorOnly_2)); }
	inline bool get__editorOnly_2() const { return ____editorOnly_2; }
	inline bool* get_address_of__editorOnly_2() { return &____editorOnly_2; }
	inline void set__editorOnly_2(bool value)
	{
		____editorOnly_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
