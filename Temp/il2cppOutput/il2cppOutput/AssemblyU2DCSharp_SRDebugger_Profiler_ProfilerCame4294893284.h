﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// System.Action`2<SRDebugger.Profiler.ProfilerCameraListener,System.Double>
struct Action_2_t4150018422;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Profiler.ProfilerCameraListener
struct  ProfilerCameraListener_t4294893284  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera SRDebugger.Profiler.ProfilerCameraListener::_camera
	Camera_t189460977 * ____camera_2;
	// System.Diagnostics.Stopwatch SRDebugger.Profiler.ProfilerCameraListener::_stopwatch
	Stopwatch_t1380178105 * ____stopwatch_3;
	// System.Action`2<SRDebugger.Profiler.ProfilerCameraListener,System.Double> SRDebugger.Profiler.ProfilerCameraListener::RenderDurationCallback
	Action_2_t4150018422 * ___RenderDurationCallback_4;

public:
	inline static int32_t get_offset_of__camera_2() { return static_cast<int32_t>(offsetof(ProfilerCameraListener_t4294893284, ____camera_2)); }
	inline Camera_t189460977 * get__camera_2() const { return ____camera_2; }
	inline Camera_t189460977 ** get_address_of__camera_2() { return &____camera_2; }
	inline void set__camera_2(Camera_t189460977 * value)
	{
		____camera_2 = value;
		Il2CppCodeGenWriteBarrier(&____camera_2, value);
	}

	inline static int32_t get_offset_of__stopwatch_3() { return static_cast<int32_t>(offsetof(ProfilerCameraListener_t4294893284, ____stopwatch_3)); }
	inline Stopwatch_t1380178105 * get__stopwatch_3() const { return ____stopwatch_3; }
	inline Stopwatch_t1380178105 ** get_address_of__stopwatch_3() { return &____stopwatch_3; }
	inline void set__stopwatch_3(Stopwatch_t1380178105 * value)
	{
		____stopwatch_3 = value;
		Il2CppCodeGenWriteBarrier(&____stopwatch_3, value);
	}

	inline static int32_t get_offset_of_RenderDurationCallback_4() { return static_cast<int32_t>(offsetof(ProfilerCameraListener_t4294893284, ___RenderDurationCallback_4)); }
	inline Action_2_t4150018422 * get_RenderDurationCallback_4() const { return ___RenderDurationCallback_4; }
	inline Action_2_t4150018422 ** get_address_of_RenderDurationCallback_4() { return &___RenderDurationCallback_4; }
	inline void set_RenderDurationCallback_4(Action_2_t4150018422 * value)
	{
		___RenderDurationCallback_4 = value;
		Il2CppCodeGenWriteBarrier(&___RenderDurationCallback_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
