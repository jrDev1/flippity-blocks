﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ETCBase_RectAnchor1252084535.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_ETCBase_CameraMode264751676.h"
#include "AssemblyU2DCSharp_ETCBase_CameraTargetMode2480359029.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_ETCBase_DPadAxis2053989764.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Canvas
struct Canvas_t209405766;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCBase
struct  ETCBase_t118528977  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform ETCBase::cachedRectTransform
	RectTransform_t3349966182 * ___cachedRectTransform_2;
	// UnityEngine.Canvas ETCBase::cachedRootCanvas
	Canvas_t209405766 * ___cachedRootCanvas_3;
	// System.Boolean ETCBase::isUnregisterAtDisable
	bool ___isUnregisterAtDisable_4;
	// System.Boolean ETCBase::visibleAtStart
	bool ___visibleAtStart_5;
	// System.Boolean ETCBase::activatedAtStart
	bool ___activatedAtStart_6;
	// ETCBase/RectAnchor ETCBase::_anchor
	int32_t ____anchor_7;
	// UnityEngine.Vector2 ETCBase::_anchorOffet
	Vector2_t2243707579  ____anchorOffet_8;
	// System.Boolean ETCBase::_visible
	bool ____visible_9;
	// System.Boolean ETCBase::_activated
	bool ____activated_10;
	// System.Boolean ETCBase::enableCamera
	bool ___enableCamera_11;
	// ETCBase/CameraMode ETCBase::cameraMode
	int32_t ___cameraMode_12;
	// System.String ETCBase::camTargetTag
	String_t* ___camTargetTag_13;
	// System.Boolean ETCBase::autoLinkTagCam
	bool ___autoLinkTagCam_14;
	// System.String ETCBase::autoCamTag
	String_t* ___autoCamTag_15;
	// UnityEngine.Transform ETCBase::cameraTransform
	Transform_t3275118058 * ___cameraTransform_16;
	// ETCBase/CameraTargetMode ETCBase::cameraTargetMode
	int32_t ___cameraTargetMode_17;
	// System.Boolean ETCBase::enableWallDetection
	bool ___enableWallDetection_18;
	// UnityEngine.LayerMask ETCBase::wallLayer
	LayerMask_t3188175821  ___wallLayer_19;
	// UnityEngine.Transform ETCBase::cameraLookAt
	Transform_t3275118058 * ___cameraLookAt_20;
	// UnityEngine.CharacterController ETCBase::cameraLookAtCC
	CharacterController_t4094781467 * ___cameraLookAtCC_21;
	// UnityEngine.Vector3 ETCBase::followOffset
	Vector3_t2243707580  ___followOffset_22;
	// System.Single ETCBase::followDistance
	float ___followDistance_23;
	// System.Single ETCBase::followHeight
	float ___followHeight_24;
	// System.Single ETCBase::followRotationDamping
	float ___followRotationDamping_25;
	// System.Single ETCBase::followHeightDamping
	float ___followHeightDamping_26;
	// System.Int32 ETCBase::pointId
	int32_t ___pointId_27;
	// System.Boolean ETCBase::enableKeySimulation
	bool ___enableKeySimulation_28;
	// System.Boolean ETCBase::allowSimulationStandalone
	bool ___allowSimulationStandalone_29;
	// System.Boolean ETCBase::visibleOnStandalone
	bool ___visibleOnStandalone_30;
	// ETCBase/DPadAxis ETCBase::dPadAxisCount
	int32_t ___dPadAxisCount_31;
	// System.Boolean ETCBase::useFixedUpdate
	bool ___useFixedUpdate_32;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> ETCBase::uiRaycastResultCache
	List_1_t3685274804 * ___uiRaycastResultCache_33;
	// UnityEngine.EventSystems.PointerEventData ETCBase::uiPointerEventData
	PointerEventData_t1599784723 * ___uiPointerEventData_34;
	// UnityEngine.EventSystems.EventSystem ETCBase::uiEventSystem
	EventSystem_t3466835263 * ___uiEventSystem_35;
	// System.Boolean ETCBase::isOnDrag
	bool ___isOnDrag_36;
	// System.Boolean ETCBase::isSwipeIn
	bool ___isSwipeIn_37;
	// System.Boolean ETCBase::isSwipeOut
	bool ___isSwipeOut_38;
	// System.Boolean ETCBase::showPSInspector
	bool ___showPSInspector_39;
	// System.Boolean ETCBase::showSpriteInspector
	bool ___showSpriteInspector_40;
	// System.Boolean ETCBase::showEventInspector
	bool ___showEventInspector_41;
	// System.Boolean ETCBase::showBehaviourInspector
	bool ___showBehaviourInspector_42;
	// System.Boolean ETCBase::showAxesInspector
	bool ___showAxesInspector_43;
	// System.Boolean ETCBase::showTouchEventInspector
	bool ___showTouchEventInspector_44;
	// System.Boolean ETCBase::showDownEventInspector
	bool ___showDownEventInspector_45;
	// System.Boolean ETCBase::showPressEventInspector
	bool ___showPressEventInspector_46;
	// System.Boolean ETCBase::showCameraInspector
	bool ___showCameraInspector_47;

public:
	inline static int32_t get_offset_of_cachedRectTransform_2() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cachedRectTransform_2)); }
	inline RectTransform_t3349966182 * get_cachedRectTransform_2() const { return ___cachedRectTransform_2; }
	inline RectTransform_t3349966182 ** get_address_of_cachedRectTransform_2() { return &___cachedRectTransform_2; }
	inline void set_cachedRectTransform_2(RectTransform_t3349966182 * value)
	{
		___cachedRectTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___cachedRectTransform_2, value);
	}

	inline static int32_t get_offset_of_cachedRootCanvas_3() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cachedRootCanvas_3)); }
	inline Canvas_t209405766 * get_cachedRootCanvas_3() const { return ___cachedRootCanvas_3; }
	inline Canvas_t209405766 ** get_address_of_cachedRootCanvas_3() { return &___cachedRootCanvas_3; }
	inline void set_cachedRootCanvas_3(Canvas_t209405766 * value)
	{
		___cachedRootCanvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___cachedRootCanvas_3, value);
	}

	inline static int32_t get_offset_of_isUnregisterAtDisable_4() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___isUnregisterAtDisable_4)); }
	inline bool get_isUnregisterAtDisable_4() const { return ___isUnregisterAtDisable_4; }
	inline bool* get_address_of_isUnregisterAtDisable_4() { return &___isUnregisterAtDisable_4; }
	inline void set_isUnregisterAtDisable_4(bool value)
	{
		___isUnregisterAtDisable_4 = value;
	}

	inline static int32_t get_offset_of_visibleAtStart_5() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___visibleAtStart_5)); }
	inline bool get_visibleAtStart_5() const { return ___visibleAtStart_5; }
	inline bool* get_address_of_visibleAtStart_5() { return &___visibleAtStart_5; }
	inline void set_visibleAtStart_5(bool value)
	{
		___visibleAtStart_5 = value;
	}

	inline static int32_t get_offset_of_activatedAtStart_6() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___activatedAtStart_6)); }
	inline bool get_activatedAtStart_6() const { return ___activatedAtStart_6; }
	inline bool* get_address_of_activatedAtStart_6() { return &___activatedAtStart_6; }
	inline void set_activatedAtStart_6(bool value)
	{
		___activatedAtStart_6 = value;
	}

	inline static int32_t get_offset_of__anchor_7() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ____anchor_7)); }
	inline int32_t get__anchor_7() const { return ____anchor_7; }
	inline int32_t* get_address_of__anchor_7() { return &____anchor_7; }
	inline void set__anchor_7(int32_t value)
	{
		____anchor_7 = value;
	}

	inline static int32_t get_offset_of__anchorOffet_8() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ____anchorOffet_8)); }
	inline Vector2_t2243707579  get__anchorOffet_8() const { return ____anchorOffet_8; }
	inline Vector2_t2243707579 * get_address_of__anchorOffet_8() { return &____anchorOffet_8; }
	inline void set__anchorOffet_8(Vector2_t2243707579  value)
	{
		____anchorOffet_8 = value;
	}

	inline static int32_t get_offset_of__visible_9() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ____visible_9)); }
	inline bool get__visible_9() const { return ____visible_9; }
	inline bool* get_address_of__visible_9() { return &____visible_9; }
	inline void set__visible_9(bool value)
	{
		____visible_9 = value;
	}

	inline static int32_t get_offset_of__activated_10() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ____activated_10)); }
	inline bool get__activated_10() const { return ____activated_10; }
	inline bool* get_address_of__activated_10() { return &____activated_10; }
	inline void set__activated_10(bool value)
	{
		____activated_10 = value;
	}

	inline static int32_t get_offset_of_enableCamera_11() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___enableCamera_11)); }
	inline bool get_enableCamera_11() const { return ___enableCamera_11; }
	inline bool* get_address_of_enableCamera_11() { return &___enableCamera_11; }
	inline void set_enableCamera_11(bool value)
	{
		___enableCamera_11 = value;
	}

	inline static int32_t get_offset_of_cameraMode_12() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cameraMode_12)); }
	inline int32_t get_cameraMode_12() const { return ___cameraMode_12; }
	inline int32_t* get_address_of_cameraMode_12() { return &___cameraMode_12; }
	inline void set_cameraMode_12(int32_t value)
	{
		___cameraMode_12 = value;
	}

	inline static int32_t get_offset_of_camTargetTag_13() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___camTargetTag_13)); }
	inline String_t* get_camTargetTag_13() const { return ___camTargetTag_13; }
	inline String_t** get_address_of_camTargetTag_13() { return &___camTargetTag_13; }
	inline void set_camTargetTag_13(String_t* value)
	{
		___camTargetTag_13 = value;
		Il2CppCodeGenWriteBarrier(&___camTargetTag_13, value);
	}

	inline static int32_t get_offset_of_autoLinkTagCam_14() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___autoLinkTagCam_14)); }
	inline bool get_autoLinkTagCam_14() const { return ___autoLinkTagCam_14; }
	inline bool* get_address_of_autoLinkTagCam_14() { return &___autoLinkTagCam_14; }
	inline void set_autoLinkTagCam_14(bool value)
	{
		___autoLinkTagCam_14 = value;
	}

	inline static int32_t get_offset_of_autoCamTag_15() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___autoCamTag_15)); }
	inline String_t* get_autoCamTag_15() const { return ___autoCamTag_15; }
	inline String_t** get_address_of_autoCamTag_15() { return &___autoCamTag_15; }
	inline void set_autoCamTag_15(String_t* value)
	{
		___autoCamTag_15 = value;
		Il2CppCodeGenWriteBarrier(&___autoCamTag_15, value);
	}

	inline static int32_t get_offset_of_cameraTransform_16() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cameraTransform_16)); }
	inline Transform_t3275118058 * get_cameraTransform_16() const { return ___cameraTransform_16; }
	inline Transform_t3275118058 ** get_address_of_cameraTransform_16() { return &___cameraTransform_16; }
	inline void set_cameraTransform_16(Transform_t3275118058 * value)
	{
		___cameraTransform_16 = value;
		Il2CppCodeGenWriteBarrier(&___cameraTransform_16, value);
	}

	inline static int32_t get_offset_of_cameraTargetMode_17() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cameraTargetMode_17)); }
	inline int32_t get_cameraTargetMode_17() const { return ___cameraTargetMode_17; }
	inline int32_t* get_address_of_cameraTargetMode_17() { return &___cameraTargetMode_17; }
	inline void set_cameraTargetMode_17(int32_t value)
	{
		___cameraTargetMode_17 = value;
	}

	inline static int32_t get_offset_of_enableWallDetection_18() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___enableWallDetection_18)); }
	inline bool get_enableWallDetection_18() const { return ___enableWallDetection_18; }
	inline bool* get_address_of_enableWallDetection_18() { return &___enableWallDetection_18; }
	inline void set_enableWallDetection_18(bool value)
	{
		___enableWallDetection_18 = value;
	}

	inline static int32_t get_offset_of_wallLayer_19() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___wallLayer_19)); }
	inline LayerMask_t3188175821  get_wallLayer_19() const { return ___wallLayer_19; }
	inline LayerMask_t3188175821 * get_address_of_wallLayer_19() { return &___wallLayer_19; }
	inline void set_wallLayer_19(LayerMask_t3188175821  value)
	{
		___wallLayer_19 = value;
	}

	inline static int32_t get_offset_of_cameraLookAt_20() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cameraLookAt_20)); }
	inline Transform_t3275118058 * get_cameraLookAt_20() const { return ___cameraLookAt_20; }
	inline Transform_t3275118058 ** get_address_of_cameraLookAt_20() { return &___cameraLookAt_20; }
	inline void set_cameraLookAt_20(Transform_t3275118058 * value)
	{
		___cameraLookAt_20 = value;
		Il2CppCodeGenWriteBarrier(&___cameraLookAt_20, value);
	}

	inline static int32_t get_offset_of_cameraLookAtCC_21() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___cameraLookAtCC_21)); }
	inline CharacterController_t4094781467 * get_cameraLookAtCC_21() const { return ___cameraLookAtCC_21; }
	inline CharacterController_t4094781467 ** get_address_of_cameraLookAtCC_21() { return &___cameraLookAtCC_21; }
	inline void set_cameraLookAtCC_21(CharacterController_t4094781467 * value)
	{
		___cameraLookAtCC_21 = value;
		Il2CppCodeGenWriteBarrier(&___cameraLookAtCC_21, value);
	}

	inline static int32_t get_offset_of_followOffset_22() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___followOffset_22)); }
	inline Vector3_t2243707580  get_followOffset_22() const { return ___followOffset_22; }
	inline Vector3_t2243707580 * get_address_of_followOffset_22() { return &___followOffset_22; }
	inline void set_followOffset_22(Vector3_t2243707580  value)
	{
		___followOffset_22 = value;
	}

	inline static int32_t get_offset_of_followDistance_23() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___followDistance_23)); }
	inline float get_followDistance_23() const { return ___followDistance_23; }
	inline float* get_address_of_followDistance_23() { return &___followDistance_23; }
	inline void set_followDistance_23(float value)
	{
		___followDistance_23 = value;
	}

	inline static int32_t get_offset_of_followHeight_24() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___followHeight_24)); }
	inline float get_followHeight_24() const { return ___followHeight_24; }
	inline float* get_address_of_followHeight_24() { return &___followHeight_24; }
	inline void set_followHeight_24(float value)
	{
		___followHeight_24 = value;
	}

	inline static int32_t get_offset_of_followRotationDamping_25() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___followRotationDamping_25)); }
	inline float get_followRotationDamping_25() const { return ___followRotationDamping_25; }
	inline float* get_address_of_followRotationDamping_25() { return &___followRotationDamping_25; }
	inline void set_followRotationDamping_25(float value)
	{
		___followRotationDamping_25 = value;
	}

	inline static int32_t get_offset_of_followHeightDamping_26() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___followHeightDamping_26)); }
	inline float get_followHeightDamping_26() const { return ___followHeightDamping_26; }
	inline float* get_address_of_followHeightDamping_26() { return &___followHeightDamping_26; }
	inline void set_followHeightDamping_26(float value)
	{
		___followHeightDamping_26 = value;
	}

	inline static int32_t get_offset_of_pointId_27() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___pointId_27)); }
	inline int32_t get_pointId_27() const { return ___pointId_27; }
	inline int32_t* get_address_of_pointId_27() { return &___pointId_27; }
	inline void set_pointId_27(int32_t value)
	{
		___pointId_27 = value;
	}

	inline static int32_t get_offset_of_enableKeySimulation_28() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___enableKeySimulation_28)); }
	inline bool get_enableKeySimulation_28() const { return ___enableKeySimulation_28; }
	inline bool* get_address_of_enableKeySimulation_28() { return &___enableKeySimulation_28; }
	inline void set_enableKeySimulation_28(bool value)
	{
		___enableKeySimulation_28 = value;
	}

	inline static int32_t get_offset_of_allowSimulationStandalone_29() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___allowSimulationStandalone_29)); }
	inline bool get_allowSimulationStandalone_29() const { return ___allowSimulationStandalone_29; }
	inline bool* get_address_of_allowSimulationStandalone_29() { return &___allowSimulationStandalone_29; }
	inline void set_allowSimulationStandalone_29(bool value)
	{
		___allowSimulationStandalone_29 = value;
	}

	inline static int32_t get_offset_of_visibleOnStandalone_30() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___visibleOnStandalone_30)); }
	inline bool get_visibleOnStandalone_30() const { return ___visibleOnStandalone_30; }
	inline bool* get_address_of_visibleOnStandalone_30() { return &___visibleOnStandalone_30; }
	inline void set_visibleOnStandalone_30(bool value)
	{
		___visibleOnStandalone_30 = value;
	}

	inline static int32_t get_offset_of_dPadAxisCount_31() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___dPadAxisCount_31)); }
	inline int32_t get_dPadAxisCount_31() const { return ___dPadAxisCount_31; }
	inline int32_t* get_address_of_dPadAxisCount_31() { return &___dPadAxisCount_31; }
	inline void set_dPadAxisCount_31(int32_t value)
	{
		___dPadAxisCount_31 = value;
	}

	inline static int32_t get_offset_of_useFixedUpdate_32() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___useFixedUpdate_32)); }
	inline bool get_useFixedUpdate_32() const { return ___useFixedUpdate_32; }
	inline bool* get_address_of_useFixedUpdate_32() { return &___useFixedUpdate_32; }
	inline void set_useFixedUpdate_32(bool value)
	{
		___useFixedUpdate_32 = value;
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_33() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___uiRaycastResultCache_33)); }
	inline List_1_t3685274804 * get_uiRaycastResultCache_33() const { return ___uiRaycastResultCache_33; }
	inline List_1_t3685274804 ** get_address_of_uiRaycastResultCache_33() { return &___uiRaycastResultCache_33; }
	inline void set_uiRaycastResultCache_33(List_1_t3685274804 * value)
	{
		___uiRaycastResultCache_33 = value;
		Il2CppCodeGenWriteBarrier(&___uiRaycastResultCache_33, value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_34() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___uiPointerEventData_34)); }
	inline PointerEventData_t1599784723 * get_uiPointerEventData_34() const { return ___uiPointerEventData_34; }
	inline PointerEventData_t1599784723 ** get_address_of_uiPointerEventData_34() { return &___uiPointerEventData_34; }
	inline void set_uiPointerEventData_34(PointerEventData_t1599784723 * value)
	{
		___uiPointerEventData_34 = value;
		Il2CppCodeGenWriteBarrier(&___uiPointerEventData_34, value);
	}

	inline static int32_t get_offset_of_uiEventSystem_35() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___uiEventSystem_35)); }
	inline EventSystem_t3466835263 * get_uiEventSystem_35() const { return ___uiEventSystem_35; }
	inline EventSystem_t3466835263 ** get_address_of_uiEventSystem_35() { return &___uiEventSystem_35; }
	inline void set_uiEventSystem_35(EventSystem_t3466835263 * value)
	{
		___uiEventSystem_35 = value;
		Il2CppCodeGenWriteBarrier(&___uiEventSystem_35, value);
	}

	inline static int32_t get_offset_of_isOnDrag_36() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___isOnDrag_36)); }
	inline bool get_isOnDrag_36() const { return ___isOnDrag_36; }
	inline bool* get_address_of_isOnDrag_36() { return &___isOnDrag_36; }
	inline void set_isOnDrag_36(bool value)
	{
		___isOnDrag_36 = value;
	}

	inline static int32_t get_offset_of_isSwipeIn_37() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___isSwipeIn_37)); }
	inline bool get_isSwipeIn_37() const { return ___isSwipeIn_37; }
	inline bool* get_address_of_isSwipeIn_37() { return &___isSwipeIn_37; }
	inline void set_isSwipeIn_37(bool value)
	{
		___isSwipeIn_37 = value;
	}

	inline static int32_t get_offset_of_isSwipeOut_38() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___isSwipeOut_38)); }
	inline bool get_isSwipeOut_38() const { return ___isSwipeOut_38; }
	inline bool* get_address_of_isSwipeOut_38() { return &___isSwipeOut_38; }
	inline void set_isSwipeOut_38(bool value)
	{
		___isSwipeOut_38 = value;
	}

	inline static int32_t get_offset_of_showPSInspector_39() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showPSInspector_39)); }
	inline bool get_showPSInspector_39() const { return ___showPSInspector_39; }
	inline bool* get_address_of_showPSInspector_39() { return &___showPSInspector_39; }
	inline void set_showPSInspector_39(bool value)
	{
		___showPSInspector_39 = value;
	}

	inline static int32_t get_offset_of_showSpriteInspector_40() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showSpriteInspector_40)); }
	inline bool get_showSpriteInspector_40() const { return ___showSpriteInspector_40; }
	inline bool* get_address_of_showSpriteInspector_40() { return &___showSpriteInspector_40; }
	inline void set_showSpriteInspector_40(bool value)
	{
		___showSpriteInspector_40 = value;
	}

	inline static int32_t get_offset_of_showEventInspector_41() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showEventInspector_41)); }
	inline bool get_showEventInspector_41() const { return ___showEventInspector_41; }
	inline bool* get_address_of_showEventInspector_41() { return &___showEventInspector_41; }
	inline void set_showEventInspector_41(bool value)
	{
		___showEventInspector_41 = value;
	}

	inline static int32_t get_offset_of_showBehaviourInspector_42() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showBehaviourInspector_42)); }
	inline bool get_showBehaviourInspector_42() const { return ___showBehaviourInspector_42; }
	inline bool* get_address_of_showBehaviourInspector_42() { return &___showBehaviourInspector_42; }
	inline void set_showBehaviourInspector_42(bool value)
	{
		___showBehaviourInspector_42 = value;
	}

	inline static int32_t get_offset_of_showAxesInspector_43() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showAxesInspector_43)); }
	inline bool get_showAxesInspector_43() const { return ___showAxesInspector_43; }
	inline bool* get_address_of_showAxesInspector_43() { return &___showAxesInspector_43; }
	inline void set_showAxesInspector_43(bool value)
	{
		___showAxesInspector_43 = value;
	}

	inline static int32_t get_offset_of_showTouchEventInspector_44() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showTouchEventInspector_44)); }
	inline bool get_showTouchEventInspector_44() const { return ___showTouchEventInspector_44; }
	inline bool* get_address_of_showTouchEventInspector_44() { return &___showTouchEventInspector_44; }
	inline void set_showTouchEventInspector_44(bool value)
	{
		___showTouchEventInspector_44 = value;
	}

	inline static int32_t get_offset_of_showDownEventInspector_45() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showDownEventInspector_45)); }
	inline bool get_showDownEventInspector_45() const { return ___showDownEventInspector_45; }
	inline bool* get_address_of_showDownEventInspector_45() { return &___showDownEventInspector_45; }
	inline void set_showDownEventInspector_45(bool value)
	{
		___showDownEventInspector_45 = value;
	}

	inline static int32_t get_offset_of_showPressEventInspector_46() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showPressEventInspector_46)); }
	inline bool get_showPressEventInspector_46() const { return ___showPressEventInspector_46; }
	inline bool* get_address_of_showPressEventInspector_46() { return &___showPressEventInspector_46; }
	inline void set_showPressEventInspector_46(bool value)
	{
		___showPressEventInspector_46 = value;
	}

	inline static int32_t get_offset_of_showCameraInspector_47() { return static_cast<int32_t>(offsetof(ETCBase_t118528977, ___showCameraInspector_47)); }
	inline bool get_showCameraInspector_47() const { return ___showCameraInspector_47; }
	inline bool* get_address_of_showCameraInspector_47() { return &___showCameraInspector_47; }
	inline void set_showCameraInspector_47(bool value)
	{
		___showCameraInspector_47 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
