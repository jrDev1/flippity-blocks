﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase2859877475.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1635427347.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase_331736139.h"

// System.String
struct String_t;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickBase
struct  QuickBase_t1253264426  : public MonoBehaviour_t1158329972
{
public:
	// System.String HedgehogTeam.EasyTouch.QuickBase::quickActionName
	String_t* ___quickActionName_2;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isMultiTouch
	bool ___isMultiTouch_3;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::is2Finger
	bool ___is2Finger_4;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isOnTouch
	bool ___isOnTouch_5;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::enablePickOverUI
	bool ___enablePickOverUI_6;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::resetPhysic
	bool ___resetPhysic_7;
	// HedgehogTeam.EasyTouch.QuickBase/DirectAction HedgehogTeam.EasyTouch.QuickBase::directAction
	int32_t ___directAction_8;
	// HedgehogTeam.EasyTouch.QuickBase/AffectedAxesAction HedgehogTeam.EasyTouch.QuickBase::axesAction
	int32_t ___axesAction_9;
	// System.Single HedgehogTeam.EasyTouch.QuickBase::sensibility
	float ___sensibility_10;
	// UnityEngine.CharacterController HedgehogTeam.EasyTouch.QuickBase::directCharacterController
	CharacterController_t4094781467 * ___directCharacterController_11;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::inverseAxisValue
	bool ___inverseAxisValue_12;
	// UnityEngine.Rigidbody HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody
	Rigidbody_t4233889191 * ___cachedRigidBody_13;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic
	bool ___isKinematic_14;
	// UnityEngine.Rigidbody2D HedgehogTeam.EasyTouch.QuickBase::cachedRigidBody2D
	Rigidbody2D_t502193897 * ___cachedRigidBody2D_15;
	// System.Boolean HedgehogTeam.EasyTouch.QuickBase::isKinematic2D
	bool ___isKinematic2D_16;
	// HedgehogTeam.EasyTouch.QuickBase/GameObjectType HedgehogTeam.EasyTouch.QuickBase::realType
	int32_t ___realType_17;
	// System.Int32 HedgehogTeam.EasyTouch.QuickBase::fingerIndex
	int32_t ___fingerIndex_18;

public:
	inline static int32_t get_offset_of_quickActionName_2() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___quickActionName_2)); }
	inline String_t* get_quickActionName_2() const { return ___quickActionName_2; }
	inline String_t** get_address_of_quickActionName_2() { return &___quickActionName_2; }
	inline void set_quickActionName_2(String_t* value)
	{
		___quickActionName_2 = value;
		Il2CppCodeGenWriteBarrier(&___quickActionName_2, value);
	}

	inline static int32_t get_offset_of_isMultiTouch_3() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___isMultiTouch_3)); }
	inline bool get_isMultiTouch_3() const { return ___isMultiTouch_3; }
	inline bool* get_address_of_isMultiTouch_3() { return &___isMultiTouch_3; }
	inline void set_isMultiTouch_3(bool value)
	{
		___isMultiTouch_3 = value;
	}

	inline static int32_t get_offset_of_is2Finger_4() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___is2Finger_4)); }
	inline bool get_is2Finger_4() const { return ___is2Finger_4; }
	inline bool* get_address_of_is2Finger_4() { return &___is2Finger_4; }
	inline void set_is2Finger_4(bool value)
	{
		___is2Finger_4 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_5() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___isOnTouch_5)); }
	inline bool get_isOnTouch_5() const { return ___isOnTouch_5; }
	inline bool* get_address_of_isOnTouch_5() { return &___isOnTouch_5; }
	inline void set_isOnTouch_5(bool value)
	{
		___isOnTouch_5 = value;
	}

	inline static int32_t get_offset_of_enablePickOverUI_6() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___enablePickOverUI_6)); }
	inline bool get_enablePickOverUI_6() const { return ___enablePickOverUI_6; }
	inline bool* get_address_of_enablePickOverUI_6() { return &___enablePickOverUI_6; }
	inline void set_enablePickOverUI_6(bool value)
	{
		___enablePickOverUI_6 = value;
	}

	inline static int32_t get_offset_of_resetPhysic_7() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___resetPhysic_7)); }
	inline bool get_resetPhysic_7() const { return ___resetPhysic_7; }
	inline bool* get_address_of_resetPhysic_7() { return &___resetPhysic_7; }
	inline void set_resetPhysic_7(bool value)
	{
		___resetPhysic_7 = value;
	}

	inline static int32_t get_offset_of_directAction_8() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___directAction_8)); }
	inline int32_t get_directAction_8() const { return ___directAction_8; }
	inline int32_t* get_address_of_directAction_8() { return &___directAction_8; }
	inline void set_directAction_8(int32_t value)
	{
		___directAction_8 = value;
	}

	inline static int32_t get_offset_of_axesAction_9() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___axesAction_9)); }
	inline int32_t get_axesAction_9() const { return ___axesAction_9; }
	inline int32_t* get_address_of_axesAction_9() { return &___axesAction_9; }
	inline void set_axesAction_9(int32_t value)
	{
		___axesAction_9 = value;
	}

	inline static int32_t get_offset_of_sensibility_10() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___sensibility_10)); }
	inline float get_sensibility_10() const { return ___sensibility_10; }
	inline float* get_address_of_sensibility_10() { return &___sensibility_10; }
	inline void set_sensibility_10(float value)
	{
		___sensibility_10 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_11() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___directCharacterController_11)); }
	inline CharacterController_t4094781467 * get_directCharacterController_11() const { return ___directCharacterController_11; }
	inline CharacterController_t4094781467 ** get_address_of_directCharacterController_11() { return &___directCharacterController_11; }
	inline void set_directCharacterController_11(CharacterController_t4094781467 * value)
	{
		___directCharacterController_11 = value;
		Il2CppCodeGenWriteBarrier(&___directCharacterController_11, value);
	}

	inline static int32_t get_offset_of_inverseAxisValue_12() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___inverseAxisValue_12)); }
	inline bool get_inverseAxisValue_12() const { return ___inverseAxisValue_12; }
	inline bool* get_address_of_inverseAxisValue_12() { return &___inverseAxisValue_12; }
	inline void set_inverseAxisValue_12(bool value)
	{
		___inverseAxisValue_12 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody_13() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___cachedRigidBody_13)); }
	inline Rigidbody_t4233889191 * get_cachedRigidBody_13() const { return ___cachedRigidBody_13; }
	inline Rigidbody_t4233889191 ** get_address_of_cachedRigidBody_13() { return &___cachedRigidBody_13; }
	inline void set_cachedRigidBody_13(Rigidbody_t4233889191 * value)
	{
		___cachedRigidBody_13 = value;
		Il2CppCodeGenWriteBarrier(&___cachedRigidBody_13, value);
	}

	inline static int32_t get_offset_of_isKinematic_14() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___isKinematic_14)); }
	inline bool get_isKinematic_14() const { return ___isKinematic_14; }
	inline bool* get_address_of_isKinematic_14() { return &___isKinematic_14; }
	inline void set_isKinematic_14(bool value)
	{
		___isKinematic_14 = value;
	}

	inline static int32_t get_offset_of_cachedRigidBody2D_15() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___cachedRigidBody2D_15)); }
	inline Rigidbody2D_t502193897 * get_cachedRigidBody2D_15() const { return ___cachedRigidBody2D_15; }
	inline Rigidbody2D_t502193897 ** get_address_of_cachedRigidBody2D_15() { return &___cachedRigidBody2D_15; }
	inline void set_cachedRigidBody2D_15(Rigidbody2D_t502193897 * value)
	{
		___cachedRigidBody2D_15 = value;
		Il2CppCodeGenWriteBarrier(&___cachedRigidBody2D_15, value);
	}

	inline static int32_t get_offset_of_isKinematic2D_16() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___isKinematic2D_16)); }
	inline bool get_isKinematic2D_16() const { return ___isKinematic2D_16; }
	inline bool* get_address_of_isKinematic2D_16() { return &___isKinematic2D_16; }
	inline void set_isKinematic2D_16(bool value)
	{
		___isKinematic2D_16 = value;
	}

	inline static int32_t get_offset_of_realType_17() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___realType_17)); }
	inline int32_t get_realType_17() const { return ___realType_17; }
	inline int32_t* get_address_of_realType_17() { return &___realType_17; }
	inline void set_realType_17(int32_t value)
	{
		___realType_17 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_18() { return static_cast<int32_t>(offsetof(QuickBase_t1253264426, ___fingerIndex_18)); }
	inline int32_t get_fingerIndex_18() const { return ___fingerIndex_18; }
	inline int32_t* get_address_of_fingerIndex_18() { return &___fingerIndex_18; }
	inline void set_fingerIndex_18(int32_t value)
	{
		___fingerIndex_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
