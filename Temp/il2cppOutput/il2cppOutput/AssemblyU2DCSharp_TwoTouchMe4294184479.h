﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.TextMesh
struct TextMesh_t1641806576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TwoTouchMe
struct  TwoTouchMe_t4294184479  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh TwoTouchMe::textMesh
	TextMesh_t1641806576 * ___textMesh_2;
	// UnityEngine.Color TwoTouchMe::startColor
	Color_t2020392075  ___startColor_3;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(TwoTouchMe_t4294184479, ___textMesh_2)); }
	inline TextMesh_t1641806576 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1641806576 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_2, value);
	}

	inline static int32_t get_offset_of_startColor_3() { return static_cast<int32_t>(offsetof(TwoTouchMe_t4294184479, ___startColor_3)); }
	inline Color_t2020392075  get_startColor_3() const { return ___startColor_3; }
	inline Color_t2020392075 * get_address_of_startColor_3() { return &___startColor_3; }
	inline void set_startColor_3(Color_t2020392075  value)
	{
		___startColor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
