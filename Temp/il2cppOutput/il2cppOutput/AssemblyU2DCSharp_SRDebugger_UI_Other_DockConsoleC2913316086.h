﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// SRDebugger.UI.Controls.ConsoleLogControl
struct ConsoleLogControl_t2638108020;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.DockConsoleController
struct  DockConsoleController_t2913316086  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Boolean SRDebugger.UI.Other.DockConsoleController::_isDirty
	bool ____isDirty_10;
	// System.Boolean SRDebugger.UI.Other.DockConsoleController::_isDragging
	bool ____isDragging_11;
	// System.Int32 SRDebugger.UI.Other.DockConsoleController::_pointersOver
	int32_t ____pointersOver_12;
	// UnityEngine.GameObject SRDebugger.UI.Other.DockConsoleController::BottomHandle
	GameObject_t1756533147 * ___BottomHandle_13;
	// UnityEngine.CanvasGroup SRDebugger.UI.Other.DockConsoleController::CanvasGroup
	CanvasGroup_t3296560743 * ___CanvasGroup_14;
	// SRDebugger.UI.Controls.ConsoleLogControl SRDebugger.UI.Other.DockConsoleController::Console
	ConsoleLogControl_t2638108020 * ___Console_15;
	// UnityEngine.GameObject SRDebugger.UI.Other.DockConsoleController::Dropdown
	GameObject_t1756533147 * ___Dropdown_16;
	// UnityEngine.UI.Image SRDebugger.UI.Other.DockConsoleController::DropdownToggleSprite
	Image_t2042527209 * ___DropdownToggleSprite_17;
	// UnityEngine.UI.Text SRDebugger.UI.Other.DockConsoleController::TextErrors
	Text_t356221433 * ___TextErrors_18;
	// UnityEngine.UI.Text SRDebugger.UI.Other.DockConsoleController::TextInfo
	Text_t356221433 * ___TextInfo_19;
	// UnityEngine.UI.Text SRDebugger.UI.Other.DockConsoleController::TextWarnings
	Text_t356221433 * ___TextWarnings_20;
	// UnityEngine.UI.Toggle SRDebugger.UI.Other.DockConsoleController::ToggleErrors
	Toggle_t3976754468 * ___ToggleErrors_21;
	// UnityEngine.UI.Toggle SRDebugger.UI.Other.DockConsoleController::ToggleInfo
	Toggle_t3976754468 * ___ToggleInfo_22;
	// UnityEngine.UI.Toggle SRDebugger.UI.Other.DockConsoleController::ToggleWarnings
	Toggle_t3976754468 * ___ToggleWarnings_23;
	// UnityEngine.GameObject SRDebugger.UI.Other.DockConsoleController::TopBar
	GameObject_t1756533147 * ___TopBar_24;
	// UnityEngine.GameObject SRDebugger.UI.Other.DockConsoleController::TopHandle
	GameObject_t1756533147 * ___TopHandle_25;

public:
	inline static int32_t get_offset_of__isDirty_10() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ____isDirty_10)); }
	inline bool get__isDirty_10() const { return ____isDirty_10; }
	inline bool* get_address_of__isDirty_10() { return &____isDirty_10; }
	inline void set__isDirty_10(bool value)
	{
		____isDirty_10 = value;
	}

	inline static int32_t get_offset_of__isDragging_11() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ____isDragging_11)); }
	inline bool get__isDragging_11() const { return ____isDragging_11; }
	inline bool* get_address_of__isDragging_11() { return &____isDragging_11; }
	inline void set__isDragging_11(bool value)
	{
		____isDragging_11 = value;
	}

	inline static int32_t get_offset_of__pointersOver_12() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ____pointersOver_12)); }
	inline int32_t get__pointersOver_12() const { return ____pointersOver_12; }
	inline int32_t* get_address_of__pointersOver_12() { return &____pointersOver_12; }
	inline void set__pointersOver_12(int32_t value)
	{
		____pointersOver_12 = value;
	}

	inline static int32_t get_offset_of_BottomHandle_13() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___BottomHandle_13)); }
	inline GameObject_t1756533147 * get_BottomHandle_13() const { return ___BottomHandle_13; }
	inline GameObject_t1756533147 ** get_address_of_BottomHandle_13() { return &___BottomHandle_13; }
	inline void set_BottomHandle_13(GameObject_t1756533147 * value)
	{
		___BottomHandle_13 = value;
		Il2CppCodeGenWriteBarrier(&___BottomHandle_13, value);
	}

	inline static int32_t get_offset_of_CanvasGroup_14() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___CanvasGroup_14)); }
	inline CanvasGroup_t3296560743 * get_CanvasGroup_14() const { return ___CanvasGroup_14; }
	inline CanvasGroup_t3296560743 ** get_address_of_CanvasGroup_14() { return &___CanvasGroup_14; }
	inline void set_CanvasGroup_14(CanvasGroup_t3296560743 * value)
	{
		___CanvasGroup_14 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasGroup_14, value);
	}

	inline static int32_t get_offset_of_Console_15() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___Console_15)); }
	inline ConsoleLogControl_t2638108020 * get_Console_15() const { return ___Console_15; }
	inline ConsoleLogControl_t2638108020 ** get_address_of_Console_15() { return &___Console_15; }
	inline void set_Console_15(ConsoleLogControl_t2638108020 * value)
	{
		___Console_15 = value;
		Il2CppCodeGenWriteBarrier(&___Console_15, value);
	}

	inline static int32_t get_offset_of_Dropdown_16() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___Dropdown_16)); }
	inline GameObject_t1756533147 * get_Dropdown_16() const { return ___Dropdown_16; }
	inline GameObject_t1756533147 ** get_address_of_Dropdown_16() { return &___Dropdown_16; }
	inline void set_Dropdown_16(GameObject_t1756533147 * value)
	{
		___Dropdown_16 = value;
		Il2CppCodeGenWriteBarrier(&___Dropdown_16, value);
	}

	inline static int32_t get_offset_of_DropdownToggleSprite_17() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___DropdownToggleSprite_17)); }
	inline Image_t2042527209 * get_DropdownToggleSprite_17() const { return ___DropdownToggleSprite_17; }
	inline Image_t2042527209 ** get_address_of_DropdownToggleSprite_17() { return &___DropdownToggleSprite_17; }
	inline void set_DropdownToggleSprite_17(Image_t2042527209 * value)
	{
		___DropdownToggleSprite_17 = value;
		Il2CppCodeGenWriteBarrier(&___DropdownToggleSprite_17, value);
	}

	inline static int32_t get_offset_of_TextErrors_18() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___TextErrors_18)); }
	inline Text_t356221433 * get_TextErrors_18() const { return ___TextErrors_18; }
	inline Text_t356221433 ** get_address_of_TextErrors_18() { return &___TextErrors_18; }
	inline void set_TextErrors_18(Text_t356221433 * value)
	{
		___TextErrors_18 = value;
		Il2CppCodeGenWriteBarrier(&___TextErrors_18, value);
	}

	inline static int32_t get_offset_of_TextInfo_19() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___TextInfo_19)); }
	inline Text_t356221433 * get_TextInfo_19() const { return ___TextInfo_19; }
	inline Text_t356221433 ** get_address_of_TextInfo_19() { return &___TextInfo_19; }
	inline void set_TextInfo_19(Text_t356221433 * value)
	{
		___TextInfo_19 = value;
		Il2CppCodeGenWriteBarrier(&___TextInfo_19, value);
	}

	inline static int32_t get_offset_of_TextWarnings_20() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___TextWarnings_20)); }
	inline Text_t356221433 * get_TextWarnings_20() const { return ___TextWarnings_20; }
	inline Text_t356221433 ** get_address_of_TextWarnings_20() { return &___TextWarnings_20; }
	inline void set_TextWarnings_20(Text_t356221433 * value)
	{
		___TextWarnings_20 = value;
		Il2CppCodeGenWriteBarrier(&___TextWarnings_20, value);
	}

	inline static int32_t get_offset_of_ToggleErrors_21() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___ToggleErrors_21)); }
	inline Toggle_t3976754468 * get_ToggleErrors_21() const { return ___ToggleErrors_21; }
	inline Toggle_t3976754468 ** get_address_of_ToggleErrors_21() { return &___ToggleErrors_21; }
	inline void set_ToggleErrors_21(Toggle_t3976754468 * value)
	{
		___ToggleErrors_21 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleErrors_21, value);
	}

	inline static int32_t get_offset_of_ToggleInfo_22() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___ToggleInfo_22)); }
	inline Toggle_t3976754468 * get_ToggleInfo_22() const { return ___ToggleInfo_22; }
	inline Toggle_t3976754468 ** get_address_of_ToggleInfo_22() { return &___ToggleInfo_22; }
	inline void set_ToggleInfo_22(Toggle_t3976754468 * value)
	{
		___ToggleInfo_22 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleInfo_22, value);
	}

	inline static int32_t get_offset_of_ToggleWarnings_23() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___ToggleWarnings_23)); }
	inline Toggle_t3976754468 * get_ToggleWarnings_23() const { return ___ToggleWarnings_23; }
	inline Toggle_t3976754468 ** get_address_of_ToggleWarnings_23() { return &___ToggleWarnings_23; }
	inline void set_ToggleWarnings_23(Toggle_t3976754468 * value)
	{
		___ToggleWarnings_23 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleWarnings_23, value);
	}

	inline static int32_t get_offset_of_TopBar_24() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___TopBar_24)); }
	inline GameObject_t1756533147 * get_TopBar_24() const { return ___TopBar_24; }
	inline GameObject_t1756533147 ** get_address_of_TopBar_24() { return &___TopBar_24; }
	inline void set_TopBar_24(GameObject_t1756533147 * value)
	{
		___TopBar_24 = value;
		Il2CppCodeGenWriteBarrier(&___TopBar_24, value);
	}

	inline static int32_t get_offset_of_TopHandle_25() { return static_cast<int32_t>(offsetof(DockConsoleController_t2913316086, ___TopHandle_25)); }
	inline GameObject_t1756533147 * get_TopHandle_25() const { return ___TopHandle_25; }
	inline GameObject_t1756533147 ** get_address_of_TopHandle_25() { return &___TopHandle_25; }
	inline void set_TopHandle_25(GameObject_t1756533147 * value)
	{
		___TopHandle_25 = value;
		Il2CppCodeGenWriteBarrier(&___TopHandle_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
