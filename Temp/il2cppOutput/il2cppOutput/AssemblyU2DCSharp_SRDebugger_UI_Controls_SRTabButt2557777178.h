﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.Behaviour
struct Behaviour_t955675639;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRF.UI.StyleComponent
struct StyleComponent_t3036492378;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.SRTabButton
struct  SRTabButton_t2557777178  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.Behaviour SRDebugger.UI.Controls.SRTabButton::ActiveToggle
	Behaviour_t955675639 * ___ActiveToggle_9;
	// UnityEngine.UI.Button SRDebugger.UI.Controls.SRTabButton::Button
	Button_t2872111280 * ___Button_10;
	// UnityEngine.RectTransform SRDebugger.UI.Controls.SRTabButton::ExtraContentContainer
	RectTransform_t3349966182 * ___ExtraContentContainer_11;
	// SRF.UI.StyleComponent SRDebugger.UI.Controls.SRTabButton::IconStyleComponent
	StyleComponent_t3036492378 * ___IconStyleComponent_12;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.SRTabButton::TitleText
	Text_t356221433 * ___TitleText_13;

public:
	inline static int32_t get_offset_of_ActiveToggle_9() { return static_cast<int32_t>(offsetof(SRTabButton_t2557777178, ___ActiveToggle_9)); }
	inline Behaviour_t955675639 * get_ActiveToggle_9() const { return ___ActiveToggle_9; }
	inline Behaviour_t955675639 ** get_address_of_ActiveToggle_9() { return &___ActiveToggle_9; }
	inline void set_ActiveToggle_9(Behaviour_t955675639 * value)
	{
		___ActiveToggle_9 = value;
		Il2CppCodeGenWriteBarrier(&___ActiveToggle_9, value);
	}

	inline static int32_t get_offset_of_Button_10() { return static_cast<int32_t>(offsetof(SRTabButton_t2557777178, ___Button_10)); }
	inline Button_t2872111280 * get_Button_10() const { return ___Button_10; }
	inline Button_t2872111280 ** get_address_of_Button_10() { return &___Button_10; }
	inline void set_Button_10(Button_t2872111280 * value)
	{
		___Button_10 = value;
		Il2CppCodeGenWriteBarrier(&___Button_10, value);
	}

	inline static int32_t get_offset_of_ExtraContentContainer_11() { return static_cast<int32_t>(offsetof(SRTabButton_t2557777178, ___ExtraContentContainer_11)); }
	inline RectTransform_t3349966182 * get_ExtraContentContainer_11() const { return ___ExtraContentContainer_11; }
	inline RectTransform_t3349966182 ** get_address_of_ExtraContentContainer_11() { return &___ExtraContentContainer_11; }
	inline void set_ExtraContentContainer_11(RectTransform_t3349966182 * value)
	{
		___ExtraContentContainer_11 = value;
		Il2CppCodeGenWriteBarrier(&___ExtraContentContainer_11, value);
	}

	inline static int32_t get_offset_of_IconStyleComponent_12() { return static_cast<int32_t>(offsetof(SRTabButton_t2557777178, ___IconStyleComponent_12)); }
	inline StyleComponent_t3036492378 * get_IconStyleComponent_12() const { return ___IconStyleComponent_12; }
	inline StyleComponent_t3036492378 ** get_address_of_IconStyleComponent_12() { return &___IconStyleComponent_12; }
	inline void set_IconStyleComponent_12(StyleComponent_t3036492378 * value)
	{
		___IconStyleComponent_12 = value;
		Il2CppCodeGenWriteBarrier(&___IconStyleComponent_12, value);
	}

	inline static int32_t get_offset_of_TitleText_13() { return static_cast<int32_t>(offsetof(SRTabButton_t2557777178, ___TitleText_13)); }
	inline Text_t356221433 * get_TitleText_13() const { return ___TitleText_13; }
	inline Text_t356221433 ** get_address_of_TitleText_13() { return &___TitleText_13; }
	inline void set_TitleText_13(Text_t356221433 * value)
	{
		___TitleText_13 = value;
		Il2CppCodeGenWriteBarrier(&___TitleText_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
