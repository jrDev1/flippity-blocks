﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Object
struct Il2CppObject;
// SRF.Service.SRSceneServiceBase`2<System.Object,System.Object>
struct SRSceneServiceBase_2_t4131603446;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0<System.Object,System.Object>
struct  U3CLoadCoroutineU3Ec__Iterator0_t3771457050  : public Il2CppObject
{
public:
	// UnityEngine.GameObject SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0::<go>__0
	GameObject_t1756533147 * ___U3CgoU3E__0_0;
	// TImpl SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0::<timpl>__0
	Il2CppObject * ___U3CtimplU3E__0_1;
	// SRF.Service.SRSceneServiceBase`2<T,TImpl> SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0::$this
	SRSceneServiceBase_2_t4131603446 * ___U24this_2;
	// System.Object SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 SRF.Service.SRSceneServiceBase`2/<LoadCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CgoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3771457050, ___U3CgoU3E__0_0)); }
	inline GameObject_t1756533147 * get_U3CgoU3E__0_0() const { return ___U3CgoU3E__0_0; }
	inline GameObject_t1756533147 ** get_address_of_U3CgoU3E__0_0() { return &___U3CgoU3E__0_0; }
	inline void set_U3CgoU3E__0_0(GameObject_t1756533147 * value)
	{
		___U3CgoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgoU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CtimplU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3771457050, ___U3CtimplU3E__0_1)); }
	inline Il2CppObject * get_U3CtimplU3E__0_1() const { return ___U3CtimplU3E__0_1; }
	inline Il2CppObject ** get_address_of_U3CtimplU3E__0_1() { return &___U3CtimplU3E__0_1; }
	inline void set_U3CtimplU3E__0_1(Il2CppObject * value)
	{
		___U3CtimplU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtimplU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3771457050, ___U24this_2)); }
	inline SRSceneServiceBase_2_t4131603446 * get_U24this_2() const { return ___U24this_2; }
	inline SRSceneServiceBase_2_t4131603446 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SRSceneServiceBase_2_t4131603446 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3771457050, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3771457050, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ec__Iterator0_t3771457050, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
