﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Animation
struct Animation_t2068071072;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CharacterAnimationDungeon
struct  CharacterAnimationDungeon_t2186168437  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.CharacterController CharacterAnimationDungeon::cc
	CharacterController_t4094781467 * ___cc_2;
	// UnityEngine.Animation CharacterAnimationDungeon::anim
	Animation_t2068071072 * ___anim_3;

public:
	inline static int32_t get_offset_of_cc_2() { return static_cast<int32_t>(offsetof(CharacterAnimationDungeon_t2186168437, ___cc_2)); }
	inline CharacterController_t4094781467 * get_cc_2() const { return ___cc_2; }
	inline CharacterController_t4094781467 ** get_address_of_cc_2() { return &___cc_2; }
	inline void set_cc_2(CharacterController_t4094781467 * value)
	{
		___cc_2 = value;
		Il2CppCodeGenWriteBarrier(&___cc_2, value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(CharacterAnimationDungeon_t2186168437, ___anim_3)); }
	inline Animation_t2068071072 * get_anim_3() const { return ___anim_3; }
	inline Animation_t2068071072 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animation_t2068071072 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier(&___anim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
