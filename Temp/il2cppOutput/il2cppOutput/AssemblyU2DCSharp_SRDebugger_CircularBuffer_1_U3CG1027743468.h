﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_ArraySegment_1_gen1920080645.h"

// System.ArraySegment`1<SRDebugger.Services.ConsoleEntry>[]
struct ArraySegment_1U5BU5D_t194529096;
// SRDebugger.CircularBuffer`1<SRDebugger.Services.ConsoleEntry>
struct CircularBuffer_1_t4034286414;
// SRDebugger.Services.ConsoleEntry
struct ConsoleEntry_t3008967599;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0<SRDebugger.Services.ConsoleEntry>
struct  U3CGetEnumeratorU3Ec__Iterator0_t1027743468  : public Il2CppObject
{
public:
	// System.ArraySegment`1<T>[] SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::<segments>__0
	ArraySegment_1U5BU5D_t194529096* ___U3CsegmentsU3E__0_0;
	// System.ArraySegment`1<T>[] SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::$locvar0
	ArraySegment_1U5BU5D_t194529096* ___U24locvar0_1;
	// System.Int32 SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.ArraySegment`1<T> SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::<segment>__1
	ArraySegment_1_t1920080645  ___U3CsegmentU3E__1_3;
	// System.Int32 SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::<i>__2
	int32_t ___U3CiU3E__2_4;
	// SRDebugger.CircularBuffer`1<T> SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::$this
	CircularBuffer_1_t4034286414 * ___U24this_5;
	// T SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::$current
	ConsoleEntry_t3008967599 * ___U24current_6;
	// System.Boolean SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 SRDebugger.CircularBuffer`1/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CsegmentsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U3CsegmentsU3E__0_0)); }
	inline ArraySegment_1U5BU5D_t194529096* get_U3CsegmentsU3E__0_0() const { return ___U3CsegmentsU3E__0_0; }
	inline ArraySegment_1U5BU5D_t194529096** get_address_of_U3CsegmentsU3E__0_0() { return &___U3CsegmentsU3E__0_0; }
	inline void set_U3CsegmentsU3E__0_0(ArraySegment_1U5BU5D_t194529096* value)
	{
		___U3CsegmentsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsegmentsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U24locvar0_1)); }
	inline ArraySegment_1U5BU5D_t194529096* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline ArraySegment_1U5BU5D_t194529096** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(ArraySegment_1U5BU5D_t194529096* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CsegmentU3E__1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U3CsegmentU3E__1_3)); }
	inline ArraySegment_1_t1920080645  get_U3CsegmentU3E__1_3() const { return ___U3CsegmentU3E__1_3; }
	inline ArraySegment_1_t1920080645 * get_address_of_U3CsegmentU3E__1_3() { return &___U3CsegmentU3E__1_3; }
	inline void set_U3CsegmentU3E__1_3(ArraySegment_1_t1920080645  value)
	{
		___U3CsegmentU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__2_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U3CiU3E__2_4)); }
	inline int32_t get_U3CiU3E__2_4() const { return ___U3CiU3E__2_4; }
	inline int32_t* get_address_of_U3CiU3E__2_4() { return &___U3CiU3E__2_4; }
	inline void set_U3CiU3E__2_4(int32_t value)
	{
		___U3CiU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U24this_5)); }
	inline CircularBuffer_1_t4034286414 * get_U24this_5() const { return ___U24this_5; }
	inline CircularBuffer_1_t4034286414 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(CircularBuffer_1_t4034286414 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U24current_6)); }
	inline ConsoleEntry_t3008967599 * get_U24current_6() const { return ___U24current_6; }
	inline ConsoleEntry_t3008967599 ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(ConsoleEntry_t3008967599 * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t1027743468, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
