﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"

// SRF.UI.SRSpinner/SpinEvent
struct SpinEvent_t3474549388;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.SRSpinner
struct  SRSpinner_t2158684882  : public Selectable_t1490392188
{
public:
	// System.Single SRF.UI.SRSpinner::_dragDelta
	float ____dragDelta_16;
	// SRF.UI.SRSpinner/SpinEvent SRF.UI.SRSpinner::_onSpinDecrement
	SpinEvent_t3474549388 * ____onSpinDecrement_17;
	// SRF.UI.SRSpinner/SpinEvent SRF.UI.SRSpinner::_onSpinIncrement
	SpinEvent_t3474549388 * ____onSpinIncrement_18;
	// System.Single SRF.UI.SRSpinner::DragThreshold
	float ___DragThreshold_19;

public:
	inline static int32_t get_offset_of__dragDelta_16() { return static_cast<int32_t>(offsetof(SRSpinner_t2158684882, ____dragDelta_16)); }
	inline float get__dragDelta_16() const { return ____dragDelta_16; }
	inline float* get_address_of__dragDelta_16() { return &____dragDelta_16; }
	inline void set__dragDelta_16(float value)
	{
		____dragDelta_16 = value;
	}

	inline static int32_t get_offset_of__onSpinDecrement_17() { return static_cast<int32_t>(offsetof(SRSpinner_t2158684882, ____onSpinDecrement_17)); }
	inline SpinEvent_t3474549388 * get__onSpinDecrement_17() const { return ____onSpinDecrement_17; }
	inline SpinEvent_t3474549388 ** get_address_of__onSpinDecrement_17() { return &____onSpinDecrement_17; }
	inline void set__onSpinDecrement_17(SpinEvent_t3474549388 * value)
	{
		____onSpinDecrement_17 = value;
		Il2CppCodeGenWriteBarrier(&____onSpinDecrement_17, value);
	}

	inline static int32_t get_offset_of__onSpinIncrement_18() { return static_cast<int32_t>(offsetof(SRSpinner_t2158684882, ____onSpinIncrement_18)); }
	inline SpinEvent_t3474549388 * get__onSpinIncrement_18() const { return ____onSpinIncrement_18; }
	inline SpinEvent_t3474549388 ** get_address_of__onSpinIncrement_18() { return &____onSpinIncrement_18; }
	inline void set__onSpinIncrement_18(SpinEvent_t3474549388 * value)
	{
		____onSpinIncrement_18 = value;
		Il2CppCodeGenWriteBarrier(&____onSpinIncrement_18, value);
	}

	inline static int32_t get_offset_of_DragThreshold_19() { return static_cast<int32_t>(offsetof(SRSpinner_t2158684882, ___DragThreshold_19)); }
	inline float get_DragThreshold_19() const { return ___DragThreshold_19; }
	inline float* get_address_of_DragThreshold_19() { return &___DragThreshold_19; }
	inline void set_DragThreshold_19(float value)
	{
		___DragThreshold_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
