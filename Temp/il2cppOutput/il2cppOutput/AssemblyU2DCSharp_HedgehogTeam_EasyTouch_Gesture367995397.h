﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_BaseFinge1402521766.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2889416990.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_110068694.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Gesture
struct  Gesture_t367995397  : public BaseFinger_t1402521766
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeDirection HedgehogTeam.EasyTouch.Gesture::swipe
	int32_t ___swipe_19;
	// System.Single HedgehogTeam.EasyTouch.Gesture::swipeLength
	float ___swipeLength_20;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Gesture::swipeVector
	Vector2_t2243707579  ___swipeVector_21;
	// System.Single HedgehogTeam.EasyTouch.Gesture::deltaPinch
	float ___deltaPinch_22;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twistAngle
	float ___twistAngle_23;
	// System.Single HedgehogTeam.EasyTouch.Gesture::twoFingerDistance
	float ___twoFingerDistance_24;
	// HedgehogTeam.EasyTouch.EasyTouch/EvtType HedgehogTeam.EasyTouch.Gesture::type
	int32_t ___type_25;

public:
	inline static int32_t get_offset_of_swipe_19() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___swipe_19)); }
	inline int32_t get_swipe_19() const { return ___swipe_19; }
	inline int32_t* get_address_of_swipe_19() { return &___swipe_19; }
	inline void set_swipe_19(int32_t value)
	{
		___swipe_19 = value;
	}

	inline static int32_t get_offset_of_swipeLength_20() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___swipeLength_20)); }
	inline float get_swipeLength_20() const { return ___swipeLength_20; }
	inline float* get_address_of_swipeLength_20() { return &___swipeLength_20; }
	inline void set_swipeLength_20(float value)
	{
		___swipeLength_20 = value;
	}

	inline static int32_t get_offset_of_swipeVector_21() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___swipeVector_21)); }
	inline Vector2_t2243707579  get_swipeVector_21() const { return ___swipeVector_21; }
	inline Vector2_t2243707579 * get_address_of_swipeVector_21() { return &___swipeVector_21; }
	inline void set_swipeVector_21(Vector2_t2243707579  value)
	{
		___swipeVector_21 = value;
	}

	inline static int32_t get_offset_of_deltaPinch_22() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___deltaPinch_22)); }
	inline float get_deltaPinch_22() const { return ___deltaPinch_22; }
	inline float* get_address_of_deltaPinch_22() { return &___deltaPinch_22; }
	inline void set_deltaPinch_22(float value)
	{
		___deltaPinch_22 = value;
	}

	inline static int32_t get_offset_of_twistAngle_23() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___twistAngle_23)); }
	inline float get_twistAngle_23() const { return ___twistAngle_23; }
	inline float* get_address_of_twistAngle_23() { return &___twistAngle_23; }
	inline void set_twistAngle_23(float value)
	{
		___twistAngle_23 = value;
	}

	inline static int32_t get_offset_of_twoFingerDistance_24() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___twoFingerDistance_24)); }
	inline float get_twoFingerDistance_24() const { return ___twoFingerDistance_24; }
	inline float* get_address_of_twoFingerDistance_24() { return &___twoFingerDistance_24; }
	inline void set_twoFingerDistance_24(float value)
	{
		___twoFingerDistance_24 = value;
	}

	inline static int32_t get_offset_of_type_25() { return static_cast<int32_t>(offsetof(Gesture_t367995397, ___type_25)); }
	inline int32_t get_type_25() const { return ___type_25; }
	inline int32_t* get_address_of_type_25() { return &___type_25; }
	inline void set_type_25(int32_t value)
	{
		___type_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
