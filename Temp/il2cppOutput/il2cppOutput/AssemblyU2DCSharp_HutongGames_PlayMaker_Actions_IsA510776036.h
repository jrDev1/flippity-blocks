﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IsActive
struct  IsActive_t510776036  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.IsActive::gameObject
	FsmGameObject_t3097142863 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.IsActive::isActive
	FsmBool_t664485696 * ___isActive_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsActive::isActiveEvent
	FsmEvent_t1258573736 * ___isActiveEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.IsActive::isNotActiveEvent
	FsmEvent_t1258573736 * ___isNotActiveEvent_14;
	// System.Boolean HutongGames.PlayMaker.Actions.IsActive::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(IsActive_t510776036, ___gameObject_11)); }
	inline FsmGameObject_t3097142863 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t3097142863 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isActive_12() { return static_cast<int32_t>(offsetof(IsActive_t510776036, ___isActive_12)); }
	inline FsmBool_t664485696 * get_isActive_12() const { return ___isActive_12; }
	inline FsmBool_t664485696 ** get_address_of_isActive_12() { return &___isActive_12; }
	inline void set_isActive_12(FsmBool_t664485696 * value)
	{
		___isActive_12 = value;
		Il2CppCodeGenWriteBarrier(&___isActive_12, value);
	}

	inline static int32_t get_offset_of_isActiveEvent_13() { return static_cast<int32_t>(offsetof(IsActive_t510776036, ___isActiveEvent_13)); }
	inline FsmEvent_t1258573736 * get_isActiveEvent_13() const { return ___isActiveEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isActiveEvent_13() { return &___isActiveEvent_13; }
	inline void set_isActiveEvent_13(FsmEvent_t1258573736 * value)
	{
		___isActiveEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___isActiveEvent_13, value);
	}

	inline static int32_t get_offset_of_isNotActiveEvent_14() { return static_cast<int32_t>(offsetof(IsActive_t510776036, ___isNotActiveEvent_14)); }
	inline FsmEvent_t1258573736 * get_isNotActiveEvent_14() const { return ___isNotActiveEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotActiveEvent_14() { return &___isNotActiveEvent_14; }
	inline void set_isNotActiveEvent_14(FsmEvent_t1258573736 * value)
	{
		___isNotActiveEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___isNotActiveEvent_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(IsActive_t510776036, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
