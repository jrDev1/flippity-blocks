﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// SRF.Helpers.MethodReference
struct MethodReference_t3498339100;
// SRF.Helpers.PropertyReference
struct PropertyReference_t1009137956;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.OptionDefinition
struct  OptionDefinition_t4209375604  : public Il2CppObject
{
public:
	// System.String SRDebugger.Internal.OptionDefinition::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String SRDebugger.Internal.OptionDefinition::<Category>k__BackingField
	String_t* ___U3CCategoryU3Ek__BackingField_1;
	// System.Int32 SRDebugger.Internal.OptionDefinition::<SortPriority>k__BackingField
	int32_t ___U3CSortPriorityU3Ek__BackingField_2;
	// SRF.Helpers.MethodReference SRDebugger.Internal.OptionDefinition::<Method>k__BackingField
	MethodReference_t3498339100 * ___U3CMethodU3Ek__BackingField_3;
	// SRF.Helpers.PropertyReference SRDebugger.Internal.OptionDefinition::<Property>k__BackingField
	PropertyReference_t1009137956 * ___U3CPropertyU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OptionDefinition_t4209375604, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CCategoryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OptionDefinition_t4209375604, ___U3CCategoryU3Ek__BackingField_1)); }
	inline String_t* get_U3CCategoryU3Ek__BackingField_1() const { return ___U3CCategoryU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CCategoryU3Ek__BackingField_1() { return &___U3CCategoryU3Ek__BackingField_1; }
	inline void set_U3CCategoryU3Ek__BackingField_1(String_t* value)
	{
		___U3CCategoryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCategoryU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CSortPriorityU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OptionDefinition_t4209375604, ___U3CSortPriorityU3Ek__BackingField_2)); }
	inline int32_t get_U3CSortPriorityU3Ek__BackingField_2() const { return ___U3CSortPriorityU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSortPriorityU3Ek__BackingField_2() { return &___U3CSortPriorityU3Ek__BackingField_2; }
	inline void set_U3CSortPriorityU3Ek__BackingField_2(int32_t value)
	{
		___U3CSortPriorityU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(OptionDefinition_t4209375604, ___U3CMethodU3Ek__BackingField_3)); }
	inline MethodReference_t3498339100 * get_U3CMethodU3Ek__BackingField_3() const { return ___U3CMethodU3Ek__BackingField_3; }
	inline MethodReference_t3498339100 ** get_address_of_U3CMethodU3Ek__BackingField_3() { return &___U3CMethodU3Ek__BackingField_3; }
	inline void set_U3CMethodU3Ek__BackingField_3(MethodReference_t3498339100 * value)
	{
		___U3CMethodU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMethodU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPropertyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(OptionDefinition_t4209375604, ___U3CPropertyU3Ek__BackingField_4)); }
	inline PropertyReference_t1009137956 * get_U3CPropertyU3Ek__BackingField_4() const { return ___U3CPropertyU3Ek__BackingField_4; }
	inline PropertyReference_t1009137956 ** get_address_of_U3CPropertyU3Ek__BackingField_4() { return &___U3CPropertyU3Ek__BackingField_4; }
	inline void set_U3CPropertyU3Ek__BackingField_4(PropertyReference_t1009137956 * value)
	{
		___U3CPropertyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertyU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
