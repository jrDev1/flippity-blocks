﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_PlayMakerCollectionProxy_Variabl2025463706.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t3194695850;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t3857795355;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t390723249;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t3399195050;
// System.Collections.Generic.List`1<UnityEngine.Rect>
struct List_1_t3050876758;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t2912116861;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_t1301679762;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t3052225568;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t3973682211;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerCollectionProxy
struct  PlayMakerCollectionProxy_t398511462  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerCollectionProxy::showEvents
	bool ___showEvents_2;
	// System.Boolean PlayMakerCollectionProxy::showContent
	bool ___showContent_3;
	// System.Boolean PlayMakerCollectionProxy::TextureElementSmall
	bool ___TextureElementSmall_4;
	// System.Boolean PlayMakerCollectionProxy::condensedView
	bool ___condensedView_5;
	// System.Boolean PlayMakerCollectionProxy::liveUpdate
	bool ___liveUpdate_6;
	// System.String PlayMakerCollectionProxy::referenceName
	String_t* ___referenceName_7;
	// System.Boolean PlayMakerCollectionProxy::enablePlayMakerEvents
	bool ___enablePlayMakerEvents_8;
	// System.String PlayMakerCollectionProxy::addEvent
	String_t* ___addEvent_9;
	// System.String PlayMakerCollectionProxy::setEvent
	String_t* ___setEvent_10;
	// System.String PlayMakerCollectionProxy::removeEvent
	String_t* ___removeEvent_11;
	// System.Int32 PlayMakerCollectionProxy::contentPreviewStartIndex
	int32_t ___contentPreviewStartIndex_12;
	// System.Int32 PlayMakerCollectionProxy::contentPreviewMaxRows
	int32_t ___contentPreviewMaxRows_13;
	// PlayMakerCollectionProxy/VariableEnum PlayMakerCollectionProxy::preFillType
	int32_t ___preFillType_14;
	// System.Int32 PlayMakerCollectionProxy::preFillObjectTypeIndex
	int32_t ___preFillObjectTypeIndex_15;
	// System.Int32 PlayMakerCollectionProxy::preFillCount
	int32_t ___preFillCount_16;
	// System.Collections.Generic.List`1<System.String> PlayMakerCollectionProxy::preFillKeyList
	List_1_t1398341365 * ___preFillKeyList_17;
	// System.Collections.Generic.List`1<System.Boolean> PlayMakerCollectionProxy::preFillBoolList
	List_1_t3194695850 * ___preFillBoolList_18;
	// System.Collections.Generic.List`1<UnityEngine.Color> PlayMakerCollectionProxy::preFillColorList
	List_1_t1389513207 * ___preFillColorList_19;
	// System.Collections.Generic.List`1<System.Single> PlayMakerCollectionProxy::preFillFloatList
	List_1_t1445631064 * ___preFillFloatList_20;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlayMakerCollectionProxy::preFillGameObjectList
	List_1_t1125654279 * ___preFillGameObjectList_21;
	// System.Collections.Generic.List`1<System.Int32> PlayMakerCollectionProxy::preFillIntList
	List_1_t1440998580 * ___preFillIntList_22;
	// System.Collections.Generic.List`1<UnityEngine.Material> PlayMakerCollectionProxy::preFillMaterialList
	List_1_t3857795355 * ___preFillMaterialList_23;
	// System.Collections.Generic.List`1<UnityEngine.Object> PlayMakerCollectionProxy::preFillObjectList
	List_1_t390723249 * ___preFillObjectList_24;
	// System.Collections.Generic.List`1<UnityEngine.Quaternion> PlayMakerCollectionProxy::preFillQuaternionList
	List_1_t3399195050 * ___preFillQuaternionList_25;
	// System.Collections.Generic.List`1<UnityEngine.Rect> PlayMakerCollectionProxy::preFillRectList
	List_1_t3050876758 * ___preFillRectList_26;
	// System.Collections.Generic.List`1<System.String> PlayMakerCollectionProxy::preFillStringList
	List_1_t1398341365 * ___preFillStringList_27;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> PlayMakerCollectionProxy::preFillTextureList
	List_1_t2912116861 * ___preFillTextureList_28;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> PlayMakerCollectionProxy::preFillVector2List
	List_1_t1612828711 * ___preFillVector2List_29;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> PlayMakerCollectionProxy::preFillVector3List
	List_1_t1612828712 * ___preFillVector3List_30;
	// System.Collections.Generic.List`1<UnityEngine.AudioClip> PlayMakerCollectionProxy::preFillAudioClipList
	List_1_t1301679762 * ___preFillAudioClipList_31;
	// System.Collections.Generic.List`1<System.Byte> PlayMakerCollectionProxy::preFillByteList
	List_1_t3052225568 * ___preFillByteList_32;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> PlayMakerCollectionProxy::preFillSpriteList
	List_1_t3973682211 * ___preFillSpriteList_33;

public:
	inline static int32_t get_offset_of_showEvents_2() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___showEvents_2)); }
	inline bool get_showEvents_2() const { return ___showEvents_2; }
	inline bool* get_address_of_showEvents_2() { return &___showEvents_2; }
	inline void set_showEvents_2(bool value)
	{
		___showEvents_2 = value;
	}

	inline static int32_t get_offset_of_showContent_3() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___showContent_3)); }
	inline bool get_showContent_3() const { return ___showContent_3; }
	inline bool* get_address_of_showContent_3() { return &___showContent_3; }
	inline void set_showContent_3(bool value)
	{
		___showContent_3 = value;
	}

	inline static int32_t get_offset_of_TextureElementSmall_4() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___TextureElementSmall_4)); }
	inline bool get_TextureElementSmall_4() const { return ___TextureElementSmall_4; }
	inline bool* get_address_of_TextureElementSmall_4() { return &___TextureElementSmall_4; }
	inline void set_TextureElementSmall_4(bool value)
	{
		___TextureElementSmall_4 = value;
	}

	inline static int32_t get_offset_of_condensedView_5() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___condensedView_5)); }
	inline bool get_condensedView_5() const { return ___condensedView_5; }
	inline bool* get_address_of_condensedView_5() { return &___condensedView_5; }
	inline void set_condensedView_5(bool value)
	{
		___condensedView_5 = value;
	}

	inline static int32_t get_offset_of_liveUpdate_6() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___liveUpdate_6)); }
	inline bool get_liveUpdate_6() const { return ___liveUpdate_6; }
	inline bool* get_address_of_liveUpdate_6() { return &___liveUpdate_6; }
	inline void set_liveUpdate_6(bool value)
	{
		___liveUpdate_6 = value;
	}

	inline static int32_t get_offset_of_referenceName_7() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___referenceName_7)); }
	inline String_t* get_referenceName_7() const { return ___referenceName_7; }
	inline String_t** get_address_of_referenceName_7() { return &___referenceName_7; }
	inline void set_referenceName_7(String_t* value)
	{
		___referenceName_7 = value;
		Il2CppCodeGenWriteBarrier(&___referenceName_7, value);
	}

	inline static int32_t get_offset_of_enablePlayMakerEvents_8() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___enablePlayMakerEvents_8)); }
	inline bool get_enablePlayMakerEvents_8() const { return ___enablePlayMakerEvents_8; }
	inline bool* get_address_of_enablePlayMakerEvents_8() { return &___enablePlayMakerEvents_8; }
	inline void set_enablePlayMakerEvents_8(bool value)
	{
		___enablePlayMakerEvents_8 = value;
	}

	inline static int32_t get_offset_of_addEvent_9() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___addEvent_9)); }
	inline String_t* get_addEvent_9() const { return ___addEvent_9; }
	inline String_t** get_address_of_addEvent_9() { return &___addEvent_9; }
	inline void set_addEvent_9(String_t* value)
	{
		___addEvent_9 = value;
		Il2CppCodeGenWriteBarrier(&___addEvent_9, value);
	}

	inline static int32_t get_offset_of_setEvent_10() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___setEvent_10)); }
	inline String_t* get_setEvent_10() const { return ___setEvent_10; }
	inline String_t** get_address_of_setEvent_10() { return &___setEvent_10; }
	inline void set_setEvent_10(String_t* value)
	{
		___setEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___setEvent_10, value);
	}

	inline static int32_t get_offset_of_removeEvent_11() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___removeEvent_11)); }
	inline String_t* get_removeEvent_11() const { return ___removeEvent_11; }
	inline String_t** get_address_of_removeEvent_11() { return &___removeEvent_11; }
	inline void set_removeEvent_11(String_t* value)
	{
		___removeEvent_11 = value;
		Il2CppCodeGenWriteBarrier(&___removeEvent_11, value);
	}

	inline static int32_t get_offset_of_contentPreviewStartIndex_12() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___contentPreviewStartIndex_12)); }
	inline int32_t get_contentPreviewStartIndex_12() const { return ___contentPreviewStartIndex_12; }
	inline int32_t* get_address_of_contentPreviewStartIndex_12() { return &___contentPreviewStartIndex_12; }
	inline void set_contentPreviewStartIndex_12(int32_t value)
	{
		___contentPreviewStartIndex_12 = value;
	}

	inline static int32_t get_offset_of_contentPreviewMaxRows_13() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___contentPreviewMaxRows_13)); }
	inline int32_t get_contentPreviewMaxRows_13() const { return ___contentPreviewMaxRows_13; }
	inline int32_t* get_address_of_contentPreviewMaxRows_13() { return &___contentPreviewMaxRows_13; }
	inline void set_contentPreviewMaxRows_13(int32_t value)
	{
		___contentPreviewMaxRows_13 = value;
	}

	inline static int32_t get_offset_of_preFillType_14() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillType_14)); }
	inline int32_t get_preFillType_14() const { return ___preFillType_14; }
	inline int32_t* get_address_of_preFillType_14() { return &___preFillType_14; }
	inline void set_preFillType_14(int32_t value)
	{
		___preFillType_14 = value;
	}

	inline static int32_t get_offset_of_preFillObjectTypeIndex_15() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillObjectTypeIndex_15)); }
	inline int32_t get_preFillObjectTypeIndex_15() const { return ___preFillObjectTypeIndex_15; }
	inline int32_t* get_address_of_preFillObjectTypeIndex_15() { return &___preFillObjectTypeIndex_15; }
	inline void set_preFillObjectTypeIndex_15(int32_t value)
	{
		___preFillObjectTypeIndex_15 = value;
	}

	inline static int32_t get_offset_of_preFillCount_16() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillCount_16)); }
	inline int32_t get_preFillCount_16() const { return ___preFillCount_16; }
	inline int32_t* get_address_of_preFillCount_16() { return &___preFillCount_16; }
	inline void set_preFillCount_16(int32_t value)
	{
		___preFillCount_16 = value;
	}

	inline static int32_t get_offset_of_preFillKeyList_17() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillKeyList_17)); }
	inline List_1_t1398341365 * get_preFillKeyList_17() const { return ___preFillKeyList_17; }
	inline List_1_t1398341365 ** get_address_of_preFillKeyList_17() { return &___preFillKeyList_17; }
	inline void set_preFillKeyList_17(List_1_t1398341365 * value)
	{
		___preFillKeyList_17 = value;
		Il2CppCodeGenWriteBarrier(&___preFillKeyList_17, value);
	}

	inline static int32_t get_offset_of_preFillBoolList_18() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillBoolList_18)); }
	inline List_1_t3194695850 * get_preFillBoolList_18() const { return ___preFillBoolList_18; }
	inline List_1_t3194695850 ** get_address_of_preFillBoolList_18() { return &___preFillBoolList_18; }
	inline void set_preFillBoolList_18(List_1_t3194695850 * value)
	{
		___preFillBoolList_18 = value;
		Il2CppCodeGenWriteBarrier(&___preFillBoolList_18, value);
	}

	inline static int32_t get_offset_of_preFillColorList_19() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillColorList_19)); }
	inline List_1_t1389513207 * get_preFillColorList_19() const { return ___preFillColorList_19; }
	inline List_1_t1389513207 ** get_address_of_preFillColorList_19() { return &___preFillColorList_19; }
	inline void set_preFillColorList_19(List_1_t1389513207 * value)
	{
		___preFillColorList_19 = value;
		Il2CppCodeGenWriteBarrier(&___preFillColorList_19, value);
	}

	inline static int32_t get_offset_of_preFillFloatList_20() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillFloatList_20)); }
	inline List_1_t1445631064 * get_preFillFloatList_20() const { return ___preFillFloatList_20; }
	inline List_1_t1445631064 ** get_address_of_preFillFloatList_20() { return &___preFillFloatList_20; }
	inline void set_preFillFloatList_20(List_1_t1445631064 * value)
	{
		___preFillFloatList_20 = value;
		Il2CppCodeGenWriteBarrier(&___preFillFloatList_20, value);
	}

	inline static int32_t get_offset_of_preFillGameObjectList_21() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillGameObjectList_21)); }
	inline List_1_t1125654279 * get_preFillGameObjectList_21() const { return ___preFillGameObjectList_21; }
	inline List_1_t1125654279 ** get_address_of_preFillGameObjectList_21() { return &___preFillGameObjectList_21; }
	inline void set_preFillGameObjectList_21(List_1_t1125654279 * value)
	{
		___preFillGameObjectList_21 = value;
		Il2CppCodeGenWriteBarrier(&___preFillGameObjectList_21, value);
	}

	inline static int32_t get_offset_of_preFillIntList_22() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillIntList_22)); }
	inline List_1_t1440998580 * get_preFillIntList_22() const { return ___preFillIntList_22; }
	inline List_1_t1440998580 ** get_address_of_preFillIntList_22() { return &___preFillIntList_22; }
	inline void set_preFillIntList_22(List_1_t1440998580 * value)
	{
		___preFillIntList_22 = value;
		Il2CppCodeGenWriteBarrier(&___preFillIntList_22, value);
	}

	inline static int32_t get_offset_of_preFillMaterialList_23() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillMaterialList_23)); }
	inline List_1_t3857795355 * get_preFillMaterialList_23() const { return ___preFillMaterialList_23; }
	inline List_1_t3857795355 ** get_address_of_preFillMaterialList_23() { return &___preFillMaterialList_23; }
	inline void set_preFillMaterialList_23(List_1_t3857795355 * value)
	{
		___preFillMaterialList_23 = value;
		Il2CppCodeGenWriteBarrier(&___preFillMaterialList_23, value);
	}

	inline static int32_t get_offset_of_preFillObjectList_24() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillObjectList_24)); }
	inline List_1_t390723249 * get_preFillObjectList_24() const { return ___preFillObjectList_24; }
	inline List_1_t390723249 ** get_address_of_preFillObjectList_24() { return &___preFillObjectList_24; }
	inline void set_preFillObjectList_24(List_1_t390723249 * value)
	{
		___preFillObjectList_24 = value;
		Il2CppCodeGenWriteBarrier(&___preFillObjectList_24, value);
	}

	inline static int32_t get_offset_of_preFillQuaternionList_25() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillQuaternionList_25)); }
	inline List_1_t3399195050 * get_preFillQuaternionList_25() const { return ___preFillQuaternionList_25; }
	inline List_1_t3399195050 ** get_address_of_preFillQuaternionList_25() { return &___preFillQuaternionList_25; }
	inline void set_preFillQuaternionList_25(List_1_t3399195050 * value)
	{
		___preFillQuaternionList_25 = value;
		Il2CppCodeGenWriteBarrier(&___preFillQuaternionList_25, value);
	}

	inline static int32_t get_offset_of_preFillRectList_26() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillRectList_26)); }
	inline List_1_t3050876758 * get_preFillRectList_26() const { return ___preFillRectList_26; }
	inline List_1_t3050876758 ** get_address_of_preFillRectList_26() { return &___preFillRectList_26; }
	inline void set_preFillRectList_26(List_1_t3050876758 * value)
	{
		___preFillRectList_26 = value;
		Il2CppCodeGenWriteBarrier(&___preFillRectList_26, value);
	}

	inline static int32_t get_offset_of_preFillStringList_27() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillStringList_27)); }
	inline List_1_t1398341365 * get_preFillStringList_27() const { return ___preFillStringList_27; }
	inline List_1_t1398341365 ** get_address_of_preFillStringList_27() { return &___preFillStringList_27; }
	inline void set_preFillStringList_27(List_1_t1398341365 * value)
	{
		___preFillStringList_27 = value;
		Il2CppCodeGenWriteBarrier(&___preFillStringList_27, value);
	}

	inline static int32_t get_offset_of_preFillTextureList_28() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillTextureList_28)); }
	inline List_1_t2912116861 * get_preFillTextureList_28() const { return ___preFillTextureList_28; }
	inline List_1_t2912116861 ** get_address_of_preFillTextureList_28() { return &___preFillTextureList_28; }
	inline void set_preFillTextureList_28(List_1_t2912116861 * value)
	{
		___preFillTextureList_28 = value;
		Il2CppCodeGenWriteBarrier(&___preFillTextureList_28, value);
	}

	inline static int32_t get_offset_of_preFillVector2List_29() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillVector2List_29)); }
	inline List_1_t1612828711 * get_preFillVector2List_29() const { return ___preFillVector2List_29; }
	inline List_1_t1612828711 ** get_address_of_preFillVector2List_29() { return &___preFillVector2List_29; }
	inline void set_preFillVector2List_29(List_1_t1612828711 * value)
	{
		___preFillVector2List_29 = value;
		Il2CppCodeGenWriteBarrier(&___preFillVector2List_29, value);
	}

	inline static int32_t get_offset_of_preFillVector3List_30() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillVector3List_30)); }
	inline List_1_t1612828712 * get_preFillVector3List_30() const { return ___preFillVector3List_30; }
	inline List_1_t1612828712 ** get_address_of_preFillVector3List_30() { return &___preFillVector3List_30; }
	inline void set_preFillVector3List_30(List_1_t1612828712 * value)
	{
		___preFillVector3List_30 = value;
		Il2CppCodeGenWriteBarrier(&___preFillVector3List_30, value);
	}

	inline static int32_t get_offset_of_preFillAudioClipList_31() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillAudioClipList_31)); }
	inline List_1_t1301679762 * get_preFillAudioClipList_31() const { return ___preFillAudioClipList_31; }
	inline List_1_t1301679762 ** get_address_of_preFillAudioClipList_31() { return &___preFillAudioClipList_31; }
	inline void set_preFillAudioClipList_31(List_1_t1301679762 * value)
	{
		___preFillAudioClipList_31 = value;
		Il2CppCodeGenWriteBarrier(&___preFillAudioClipList_31, value);
	}

	inline static int32_t get_offset_of_preFillByteList_32() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillByteList_32)); }
	inline List_1_t3052225568 * get_preFillByteList_32() const { return ___preFillByteList_32; }
	inline List_1_t3052225568 ** get_address_of_preFillByteList_32() { return &___preFillByteList_32; }
	inline void set_preFillByteList_32(List_1_t3052225568 * value)
	{
		___preFillByteList_32 = value;
		Il2CppCodeGenWriteBarrier(&___preFillByteList_32, value);
	}

	inline static int32_t get_offset_of_preFillSpriteList_33() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462, ___preFillSpriteList_33)); }
	inline List_1_t3973682211 * get_preFillSpriteList_33() const { return ___preFillSpriteList_33; }
	inline List_1_t3973682211 ** get_address_of_preFillSpriteList_33() { return &___preFillSpriteList_33; }
	inline void set_preFillSpriteList_33(List_1_t3973682211 * value)
	{
		___preFillSpriteList_33 = value;
		Il2CppCodeGenWriteBarrier(&___preFillSpriteList_33, value);
	}
};

struct PlayMakerCollectionProxy_t398511462_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PlayMakerCollectionProxy::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_34;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_34() { return static_cast<int32_t>(offsetof(PlayMakerCollectionProxy_t398511462_StaticFields, ___U3CU3Ef__switchU24map0_34)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_34() const { return ___U3CU3Ef__switchU24map0_34; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_34() { return &___U3CU3Ef__switchU24map0_34; }
	inline void set_U3CU3Ef__switchU24map0_34(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_34, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
