﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// SRF.UI.StyleSheet
struct StyleSheet_t2708161410;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.StyleRoot
struct  StyleRoot_t1691968825  : public SRMonoBehaviour_t2352136145
{
public:
	// SRF.UI.StyleSheet SRF.UI.StyleRoot::_activeStyleSheet
	StyleSheet_t2708161410 * ____activeStyleSheet_8;
	// SRF.UI.StyleSheet SRF.UI.StyleRoot::StyleSheet
	StyleSheet_t2708161410 * ___StyleSheet_9;

public:
	inline static int32_t get_offset_of__activeStyleSheet_8() { return static_cast<int32_t>(offsetof(StyleRoot_t1691968825, ____activeStyleSheet_8)); }
	inline StyleSheet_t2708161410 * get__activeStyleSheet_8() const { return ____activeStyleSheet_8; }
	inline StyleSheet_t2708161410 ** get_address_of__activeStyleSheet_8() { return &____activeStyleSheet_8; }
	inline void set__activeStyleSheet_8(StyleSheet_t2708161410 * value)
	{
		____activeStyleSheet_8 = value;
		Il2CppCodeGenWriteBarrier(&____activeStyleSheet_8, value);
	}

	inline static int32_t get_offset_of_StyleSheet_9() { return static_cast<int32_t>(offsetof(StyleRoot_t1691968825, ___StyleSheet_9)); }
	inline StyleSheet_t2708161410 * get_StyleSheet_9() const { return ___StyleSheet_9; }
	inline StyleSheet_t2708161410 ** get_address_of_StyleSheet_9() { return &___StyleSheet_9; }
	inline void set_StyleSheet_9(StyleSheet_t2708161410 * value)
	{
		___StyleSheet_9 = value;
		Il2CppCodeGenWriteBarrier(&___StyleSheet_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
