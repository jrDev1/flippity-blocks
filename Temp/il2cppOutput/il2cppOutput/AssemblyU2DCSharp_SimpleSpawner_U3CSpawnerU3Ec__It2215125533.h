﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;
// UnityEngine.Transform
struct Transform_t3275118058;
// SimpleSpawner
struct SimpleSpawner_t3347155754;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleSpawner/<Spawner>c__Iterator1
struct  U3CSpawnerU3Ec__Iterator1_t2215125533  : public Il2CppObject
{
public:
	// System.Int32 SimpleSpawner/<Spawner>c__Iterator1::<count>__0
	int32_t ___U3CcountU3E__0_0;
	// PathologicalGames.SpawnPool SimpleSpawner/<Spawner>c__Iterator1::<shapesPool>__0
	SpawnPool_t2419717525 * ___U3CshapesPoolU3E__0_1;
	// UnityEngine.Transform SimpleSpawner/<Spawner>c__Iterator1::<inst>__1
	Transform_t3275118058 * ___U3CinstU3E__1_2;
	// SimpleSpawner SimpleSpawner/<Spawner>c__Iterator1::$this
	SimpleSpawner_t3347155754 * ___U24this_3;
	// System.Object SimpleSpawner/<Spawner>c__Iterator1::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean SimpleSpawner/<Spawner>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 SimpleSpawner/<Spawner>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CcountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U3CcountU3E__0_0)); }
	inline int32_t get_U3CcountU3E__0_0() const { return ___U3CcountU3E__0_0; }
	inline int32_t* get_address_of_U3CcountU3E__0_0() { return &___U3CcountU3E__0_0; }
	inline void set_U3CcountU3E__0_0(int32_t value)
	{
		___U3CcountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CshapesPoolU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U3CshapesPoolU3E__0_1)); }
	inline SpawnPool_t2419717525 * get_U3CshapesPoolU3E__0_1() const { return ___U3CshapesPoolU3E__0_1; }
	inline SpawnPool_t2419717525 ** get_address_of_U3CshapesPoolU3E__0_1() { return &___U3CshapesPoolU3E__0_1; }
	inline void set_U3CshapesPoolU3E__0_1(SpawnPool_t2419717525 * value)
	{
		___U3CshapesPoolU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CshapesPoolU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CinstU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U3CinstU3E__1_2)); }
	inline Transform_t3275118058 * get_U3CinstU3E__1_2() const { return ___U3CinstU3E__1_2; }
	inline Transform_t3275118058 ** get_address_of_U3CinstU3E__1_2() { return &___U3CinstU3E__1_2; }
	inline void set_U3CinstU3E__1_2(Transform_t3275118058 * value)
	{
		___U3CinstU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinstU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U24this_3)); }
	inline SimpleSpawner_t3347155754 * get_U24this_3() const { return ___U24this_3; }
	inline SimpleSpawner_t3347155754 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SimpleSpawner_t3347155754 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t2215125533, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
