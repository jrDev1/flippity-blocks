﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ETCBase118528977.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// ETCTouchPad/OnMoveStartHandler
struct OnMoveStartHandler_t2914368815;
// ETCTouchPad/OnMoveHandler
struct OnMoveHandler_t1350262459;
// ETCTouchPad/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t2314708994;
// ETCTouchPad/OnMoveEndHandler
struct OnMoveEndHandler_t1768956876;
// ETCTouchPad/OnTouchStartHandler
struct OnTouchStartHandler_t1876210983;
// ETCTouchPad/OnTouchUPHandler
struct OnTouchUPHandler_t1894489300;
// ETCTouchPad/OnDownUpHandler
struct OnDownUpHandler_t3467319937;
// ETCTouchPad/OnDownDownHandler
struct OnDownDownHandler_t1928173934;
// ETCTouchPad/OnDownLeftHandler
struct OnDownLeftHandler_t105288103;
// ETCTouchPad/OnDownRightHandler
struct OnDownRightHandler_t3623032720;
// ETCAxis
struct ETCAxis_t3445562479;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCTouchPad
struct  ETCTouchPad_t3808707874  : public ETCBase_t118528977
{
public:
	// ETCTouchPad/OnMoveStartHandler ETCTouchPad::onMoveStart
	OnMoveStartHandler_t2914368815 * ___onMoveStart_48;
	// ETCTouchPad/OnMoveHandler ETCTouchPad::onMove
	OnMoveHandler_t1350262459 * ___onMove_49;
	// ETCTouchPad/OnMoveSpeedHandler ETCTouchPad::onMoveSpeed
	OnMoveSpeedHandler_t2314708994 * ___onMoveSpeed_50;
	// ETCTouchPad/OnMoveEndHandler ETCTouchPad::onMoveEnd
	OnMoveEndHandler_t1768956876 * ___onMoveEnd_51;
	// ETCTouchPad/OnTouchStartHandler ETCTouchPad::onTouchStart
	OnTouchStartHandler_t1876210983 * ___onTouchStart_52;
	// ETCTouchPad/OnTouchUPHandler ETCTouchPad::onTouchUp
	OnTouchUPHandler_t1894489300 * ___onTouchUp_53;
	// ETCTouchPad/OnDownUpHandler ETCTouchPad::OnDownUp
	OnDownUpHandler_t3467319937 * ___OnDownUp_54;
	// ETCTouchPad/OnDownDownHandler ETCTouchPad::OnDownDown
	OnDownDownHandler_t1928173934 * ___OnDownDown_55;
	// ETCTouchPad/OnDownLeftHandler ETCTouchPad::OnDownLeft
	OnDownLeftHandler_t105288103 * ___OnDownLeft_56;
	// ETCTouchPad/OnDownRightHandler ETCTouchPad::OnDownRight
	OnDownRightHandler_t3623032720 * ___OnDownRight_57;
	// ETCTouchPad/OnDownUpHandler ETCTouchPad::OnPressUp
	OnDownUpHandler_t3467319937 * ___OnPressUp_58;
	// ETCTouchPad/OnDownDownHandler ETCTouchPad::OnPressDown
	OnDownDownHandler_t1928173934 * ___OnPressDown_59;
	// ETCTouchPad/OnDownLeftHandler ETCTouchPad::OnPressLeft
	OnDownLeftHandler_t105288103 * ___OnPressLeft_60;
	// ETCTouchPad/OnDownRightHandler ETCTouchPad::OnPressRight
	OnDownRightHandler_t3623032720 * ___OnPressRight_61;
	// ETCAxis ETCTouchPad::axisX
	ETCAxis_t3445562479 * ___axisX_62;
	// ETCAxis ETCTouchPad::axisY
	ETCAxis_t3445562479 * ___axisY_63;
	// System.Boolean ETCTouchPad::isDPI
	bool ___isDPI_64;
	// UnityEngine.UI.Image ETCTouchPad::cachedImage
	Image_t2042527209 * ___cachedImage_65;
	// UnityEngine.Vector2 ETCTouchPad::tmpAxis
	Vector2_t2243707579  ___tmpAxis_66;
	// UnityEngine.Vector2 ETCTouchPad::OldTmpAxis
	Vector2_t2243707579  ___OldTmpAxis_67;
	// UnityEngine.GameObject ETCTouchPad::previousDargObject
	GameObject_t1756533147 * ___previousDargObject_68;
	// System.Boolean ETCTouchPad::isOut
	bool ___isOut_69;
	// System.Boolean ETCTouchPad::isOnTouch
	bool ___isOnTouch_70;
	// System.Boolean ETCTouchPad::cachedVisible
	bool ___cachedVisible_71;

public:
	inline static int32_t get_offset_of_onMoveStart_48() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___onMoveStart_48)); }
	inline OnMoveStartHandler_t2914368815 * get_onMoveStart_48() const { return ___onMoveStart_48; }
	inline OnMoveStartHandler_t2914368815 ** get_address_of_onMoveStart_48() { return &___onMoveStart_48; }
	inline void set_onMoveStart_48(OnMoveStartHandler_t2914368815 * value)
	{
		___onMoveStart_48 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveStart_48, value);
	}

	inline static int32_t get_offset_of_onMove_49() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___onMove_49)); }
	inline OnMoveHandler_t1350262459 * get_onMove_49() const { return ___onMove_49; }
	inline OnMoveHandler_t1350262459 ** get_address_of_onMove_49() { return &___onMove_49; }
	inline void set_onMove_49(OnMoveHandler_t1350262459 * value)
	{
		___onMove_49 = value;
		Il2CppCodeGenWriteBarrier(&___onMove_49, value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_50() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___onMoveSpeed_50)); }
	inline OnMoveSpeedHandler_t2314708994 * get_onMoveSpeed_50() const { return ___onMoveSpeed_50; }
	inline OnMoveSpeedHandler_t2314708994 ** get_address_of_onMoveSpeed_50() { return &___onMoveSpeed_50; }
	inline void set_onMoveSpeed_50(OnMoveSpeedHandler_t2314708994 * value)
	{
		___onMoveSpeed_50 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveSpeed_50, value);
	}

	inline static int32_t get_offset_of_onMoveEnd_51() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___onMoveEnd_51)); }
	inline OnMoveEndHandler_t1768956876 * get_onMoveEnd_51() const { return ___onMoveEnd_51; }
	inline OnMoveEndHandler_t1768956876 ** get_address_of_onMoveEnd_51() { return &___onMoveEnd_51; }
	inline void set_onMoveEnd_51(OnMoveEndHandler_t1768956876 * value)
	{
		___onMoveEnd_51 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveEnd_51, value);
	}

	inline static int32_t get_offset_of_onTouchStart_52() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___onTouchStart_52)); }
	inline OnTouchStartHandler_t1876210983 * get_onTouchStart_52() const { return ___onTouchStart_52; }
	inline OnTouchStartHandler_t1876210983 ** get_address_of_onTouchStart_52() { return &___onTouchStart_52; }
	inline void set_onTouchStart_52(OnTouchStartHandler_t1876210983 * value)
	{
		___onTouchStart_52 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchStart_52, value);
	}

	inline static int32_t get_offset_of_onTouchUp_53() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___onTouchUp_53)); }
	inline OnTouchUPHandler_t1894489300 * get_onTouchUp_53() const { return ___onTouchUp_53; }
	inline OnTouchUPHandler_t1894489300 ** get_address_of_onTouchUp_53() { return &___onTouchUp_53; }
	inline void set_onTouchUp_53(OnTouchUPHandler_t1894489300 * value)
	{
		___onTouchUp_53 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchUp_53, value);
	}

	inline static int32_t get_offset_of_OnDownUp_54() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnDownUp_54)); }
	inline OnDownUpHandler_t3467319937 * get_OnDownUp_54() const { return ___OnDownUp_54; }
	inline OnDownUpHandler_t3467319937 ** get_address_of_OnDownUp_54() { return &___OnDownUp_54; }
	inline void set_OnDownUp_54(OnDownUpHandler_t3467319937 * value)
	{
		___OnDownUp_54 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownUp_54, value);
	}

	inline static int32_t get_offset_of_OnDownDown_55() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnDownDown_55)); }
	inline OnDownDownHandler_t1928173934 * get_OnDownDown_55() const { return ___OnDownDown_55; }
	inline OnDownDownHandler_t1928173934 ** get_address_of_OnDownDown_55() { return &___OnDownDown_55; }
	inline void set_OnDownDown_55(OnDownDownHandler_t1928173934 * value)
	{
		___OnDownDown_55 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownDown_55, value);
	}

	inline static int32_t get_offset_of_OnDownLeft_56() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnDownLeft_56)); }
	inline OnDownLeftHandler_t105288103 * get_OnDownLeft_56() const { return ___OnDownLeft_56; }
	inline OnDownLeftHandler_t105288103 ** get_address_of_OnDownLeft_56() { return &___OnDownLeft_56; }
	inline void set_OnDownLeft_56(OnDownLeftHandler_t105288103 * value)
	{
		___OnDownLeft_56 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownLeft_56, value);
	}

	inline static int32_t get_offset_of_OnDownRight_57() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnDownRight_57)); }
	inline OnDownRightHandler_t3623032720 * get_OnDownRight_57() const { return ___OnDownRight_57; }
	inline OnDownRightHandler_t3623032720 ** get_address_of_OnDownRight_57() { return &___OnDownRight_57; }
	inline void set_OnDownRight_57(OnDownRightHandler_t3623032720 * value)
	{
		___OnDownRight_57 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownRight_57, value);
	}

	inline static int32_t get_offset_of_OnPressUp_58() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnPressUp_58)); }
	inline OnDownUpHandler_t3467319937 * get_OnPressUp_58() const { return ___OnPressUp_58; }
	inline OnDownUpHandler_t3467319937 ** get_address_of_OnPressUp_58() { return &___OnPressUp_58; }
	inline void set_OnPressUp_58(OnDownUpHandler_t3467319937 * value)
	{
		___OnPressUp_58 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressUp_58, value);
	}

	inline static int32_t get_offset_of_OnPressDown_59() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnPressDown_59)); }
	inline OnDownDownHandler_t1928173934 * get_OnPressDown_59() const { return ___OnPressDown_59; }
	inline OnDownDownHandler_t1928173934 ** get_address_of_OnPressDown_59() { return &___OnPressDown_59; }
	inline void set_OnPressDown_59(OnDownDownHandler_t1928173934 * value)
	{
		___OnPressDown_59 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressDown_59, value);
	}

	inline static int32_t get_offset_of_OnPressLeft_60() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnPressLeft_60)); }
	inline OnDownLeftHandler_t105288103 * get_OnPressLeft_60() const { return ___OnPressLeft_60; }
	inline OnDownLeftHandler_t105288103 ** get_address_of_OnPressLeft_60() { return &___OnPressLeft_60; }
	inline void set_OnPressLeft_60(OnDownLeftHandler_t105288103 * value)
	{
		___OnPressLeft_60 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressLeft_60, value);
	}

	inline static int32_t get_offset_of_OnPressRight_61() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OnPressRight_61)); }
	inline OnDownRightHandler_t3623032720 * get_OnPressRight_61() const { return ___OnPressRight_61; }
	inline OnDownRightHandler_t3623032720 ** get_address_of_OnPressRight_61() { return &___OnPressRight_61; }
	inline void set_OnPressRight_61(OnDownRightHandler_t3623032720 * value)
	{
		___OnPressRight_61 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressRight_61, value);
	}

	inline static int32_t get_offset_of_axisX_62() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___axisX_62)); }
	inline ETCAxis_t3445562479 * get_axisX_62() const { return ___axisX_62; }
	inline ETCAxis_t3445562479 ** get_address_of_axisX_62() { return &___axisX_62; }
	inline void set_axisX_62(ETCAxis_t3445562479 * value)
	{
		___axisX_62 = value;
		Il2CppCodeGenWriteBarrier(&___axisX_62, value);
	}

	inline static int32_t get_offset_of_axisY_63() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___axisY_63)); }
	inline ETCAxis_t3445562479 * get_axisY_63() const { return ___axisY_63; }
	inline ETCAxis_t3445562479 ** get_address_of_axisY_63() { return &___axisY_63; }
	inline void set_axisY_63(ETCAxis_t3445562479 * value)
	{
		___axisY_63 = value;
		Il2CppCodeGenWriteBarrier(&___axisY_63, value);
	}

	inline static int32_t get_offset_of_isDPI_64() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___isDPI_64)); }
	inline bool get_isDPI_64() const { return ___isDPI_64; }
	inline bool* get_address_of_isDPI_64() { return &___isDPI_64; }
	inline void set_isDPI_64(bool value)
	{
		___isDPI_64 = value;
	}

	inline static int32_t get_offset_of_cachedImage_65() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___cachedImage_65)); }
	inline Image_t2042527209 * get_cachedImage_65() const { return ___cachedImage_65; }
	inline Image_t2042527209 ** get_address_of_cachedImage_65() { return &___cachedImage_65; }
	inline void set_cachedImage_65(Image_t2042527209 * value)
	{
		___cachedImage_65 = value;
		Il2CppCodeGenWriteBarrier(&___cachedImage_65, value);
	}

	inline static int32_t get_offset_of_tmpAxis_66() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___tmpAxis_66)); }
	inline Vector2_t2243707579  get_tmpAxis_66() const { return ___tmpAxis_66; }
	inline Vector2_t2243707579 * get_address_of_tmpAxis_66() { return &___tmpAxis_66; }
	inline void set_tmpAxis_66(Vector2_t2243707579  value)
	{
		___tmpAxis_66 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_67() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___OldTmpAxis_67)); }
	inline Vector2_t2243707579  get_OldTmpAxis_67() const { return ___OldTmpAxis_67; }
	inline Vector2_t2243707579 * get_address_of_OldTmpAxis_67() { return &___OldTmpAxis_67; }
	inline void set_OldTmpAxis_67(Vector2_t2243707579  value)
	{
		___OldTmpAxis_67 = value;
	}

	inline static int32_t get_offset_of_previousDargObject_68() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___previousDargObject_68)); }
	inline GameObject_t1756533147 * get_previousDargObject_68() const { return ___previousDargObject_68; }
	inline GameObject_t1756533147 ** get_address_of_previousDargObject_68() { return &___previousDargObject_68; }
	inline void set_previousDargObject_68(GameObject_t1756533147 * value)
	{
		___previousDargObject_68 = value;
		Il2CppCodeGenWriteBarrier(&___previousDargObject_68, value);
	}

	inline static int32_t get_offset_of_isOut_69() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___isOut_69)); }
	inline bool get_isOut_69() const { return ___isOut_69; }
	inline bool* get_address_of_isOut_69() { return &___isOut_69; }
	inline void set_isOut_69(bool value)
	{
		___isOut_69 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_70() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___isOnTouch_70)); }
	inline bool get_isOnTouch_70() const { return ___isOnTouch_70; }
	inline bool* get_address_of_isOnTouch_70() { return &___isOnTouch_70; }
	inline void set_isOnTouch_70(bool value)
	{
		___isOnTouch_70 = value;
	}

	inline static int32_t get_offset_of_cachedVisible_71() { return static_cast<int32_t>(offsetof(ETCTouchPad_t3808707874, ___cachedVisible_71)); }
	inline bool get_cachedVisible_71() const { return ___cachedVisible_71; }
	inline bool* get_address_of_cachedVisible_71() { return &___cachedVisible_71; }
	inline void set_cachedVisible_71(bool value)
	{
		___cachedVisible_71 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
