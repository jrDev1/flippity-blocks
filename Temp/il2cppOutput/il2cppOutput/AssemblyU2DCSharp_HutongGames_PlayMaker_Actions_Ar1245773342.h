﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListSwapItems
struct  ArrayListSwapItems_t1245773342  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListSwapItems::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListSwapItems::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListSwapItems::index1
	FsmInt_t1273009179 * ___index1_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListSwapItems::index2
	FsmInt_t1273009179 * ___index2_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListSwapItems::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_16;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListSwapItems_t1245773342, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListSwapItems_t1245773342, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_index1_14() { return static_cast<int32_t>(offsetof(ArrayListSwapItems_t1245773342, ___index1_14)); }
	inline FsmInt_t1273009179 * get_index1_14() const { return ___index1_14; }
	inline FsmInt_t1273009179 ** get_address_of_index1_14() { return &___index1_14; }
	inline void set_index1_14(FsmInt_t1273009179 * value)
	{
		___index1_14 = value;
		Il2CppCodeGenWriteBarrier(&___index1_14, value);
	}

	inline static int32_t get_offset_of_index2_15() { return static_cast<int32_t>(offsetof(ArrayListSwapItems_t1245773342, ___index2_15)); }
	inline FsmInt_t1273009179 * get_index2_15() const { return ___index2_15; }
	inline FsmInt_t1273009179 ** get_address_of_index2_15() { return &___index2_15; }
	inline void set_index2_15(FsmInt_t1273009179 * value)
	{
		___index2_15 = value;
		Il2CppCodeGenWriteBarrier(&___index2_15, value);
	}

	inline static int32_t get_offset_of_failureEvent_16() { return static_cast<int32_t>(offsetof(ArrayListSwapItems_t1245773342, ___failureEvent_16)); }
	inline FsmEvent_t1258573736 * get_failureEvent_16() const { return ___failureEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_16() { return &___failureEvent_16; }
	inline void set_failureEvent_16(FsmEvent_t1258573736 * value)
	{
		___failureEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
