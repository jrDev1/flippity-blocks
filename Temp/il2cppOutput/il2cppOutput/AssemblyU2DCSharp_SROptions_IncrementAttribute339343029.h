﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SROptions/IncrementAttribute
struct  IncrementAttribute_t339343029  : public Attribute_t542643598
{
public:
	// System.Double SROptions/IncrementAttribute::Increment
	double ___Increment_0;

public:
	inline static int32_t get_offset_of_Increment_0() { return static_cast<int32_t>(offsetof(IncrementAttribute_t339343029, ___Increment_0)); }
	inline double get_Increment_0() const { return ___Increment_0; }
	inline double* get_address_of_Increment_0() { return &___Increment_0; }
	inline void set_Increment_0(double value)
	{
		___Increment_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
