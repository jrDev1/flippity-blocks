﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3Compare
struct  Vector3Compare_t1322954433  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Compare::vector3Variable1
	FsmVector3_t3996534004 * ___vector3Variable1_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3Compare::vector3Variable2
	FsmVector3_t3996534004 * ___vector3Variable2_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3Compare::tolerance
	FsmFloat_t937133978 * ___tolerance_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.Vector3Compare::result
	FsmBool_t664485696 * ___result_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Vector3Compare::equal
	FsmEvent_t1258573736 * ___equal_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Vector3Compare::notEqual
	FsmEvent_t1258573736 * ___notEqual_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3Compare::everyFrame
	bool ___everyFrame_17;

public:
	inline static int32_t get_offset_of_vector3Variable1_11() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___vector3Variable1_11)); }
	inline FsmVector3_t3996534004 * get_vector3Variable1_11() const { return ___vector3Variable1_11; }
	inline FsmVector3_t3996534004 ** get_address_of_vector3Variable1_11() { return &___vector3Variable1_11; }
	inline void set_vector3Variable1_11(FsmVector3_t3996534004 * value)
	{
		___vector3Variable1_11 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable1_11, value);
	}

	inline static int32_t get_offset_of_vector3Variable2_12() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___vector3Variable2_12)); }
	inline FsmVector3_t3996534004 * get_vector3Variable2_12() const { return ___vector3Variable2_12; }
	inline FsmVector3_t3996534004 ** get_address_of_vector3Variable2_12() { return &___vector3Variable2_12; }
	inline void set_vector3Variable2_12(FsmVector3_t3996534004 * value)
	{
		___vector3Variable2_12 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable2_12, value);
	}

	inline static int32_t get_offset_of_tolerance_13() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___tolerance_13)); }
	inline FsmFloat_t937133978 * get_tolerance_13() const { return ___tolerance_13; }
	inline FsmFloat_t937133978 ** get_address_of_tolerance_13() { return &___tolerance_13; }
	inline void set_tolerance_13(FsmFloat_t937133978 * value)
	{
		___tolerance_13 = value;
		Il2CppCodeGenWriteBarrier(&___tolerance_13, value);
	}

	inline static int32_t get_offset_of_result_14() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___result_14)); }
	inline FsmBool_t664485696 * get_result_14() const { return ___result_14; }
	inline FsmBool_t664485696 ** get_address_of_result_14() { return &___result_14; }
	inline void set_result_14(FsmBool_t664485696 * value)
	{
		___result_14 = value;
		Il2CppCodeGenWriteBarrier(&___result_14, value);
	}

	inline static int32_t get_offset_of_equal_15() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___equal_15)); }
	inline FsmEvent_t1258573736 * get_equal_15() const { return ___equal_15; }
	inline FsmEvent_t1258573736 ** get_address_of_equal_15() { return &___equal_15; }
	inline void set_equal_15(FsmEvent_t1258573736 * value)
	{
		___equal_15 = value;
		Il2CppCodeGenWriteBarrier(&___equal_15, value);
	}

	inline static int32_t get_offset_of_notEqual_16() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___notEqual_16)); }
	inline FsmEvent_t1258573736 * get_notEqual_16() const { return ___notEqual_16; }
	inline FsmEvent_t1258573736 ** get_address_of_notEqual_16() { return &___notEqual_16; }
	inline void set_notEqual_16(FsmEvent_t1258573736 * value)
	{
		___notEqual_16 = value;
		Il2CppCodeGenWriteBarrier(&___notEqual_16, value);
	}

	inline static int32_t get_offset_of_everyFrame_17() { return static_cast<int32_t>(offsetof(Vector3Compare_t1322954433, ___everyFrame_17)); }
	inline bool get_everyFrame_17() const { return ___everyFrame_17; }
	inline bool* get_address_of_everyFrame_17() { return &___everyFrame_17; }
	inline void set_everyFrame_17(bool value)
	{
		___everyFrame_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
