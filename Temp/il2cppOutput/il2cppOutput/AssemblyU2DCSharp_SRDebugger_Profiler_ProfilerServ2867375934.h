﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen725857282.h"

// SRF.SRList`1<SRDebugger.Profiler.ProfilerCameraListener>
struct SRList_1_t4287523865;
// SRDebugger.CircularBuffer`1<SRDebugger.Services.ProfilerFrame>
struct CircularBuffer_1_t299480543;
// UnityEngine.Camera[]
struct CameraU5BU5D_t3079764780;
// SRDebugger.Profiler.ProfilerLateUpdateListener
struct ProfilerLateUpdateListener_t3472284740;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Profiler.ProfilerServiceImpl
struct  ProfilerServiceImpl_t2867375934  : public SRServiceBase_1_t725857282
{
public:
	// SRF.SRList`1<SRDebugger.Profiler.ProfilerCameraListener> SRDebugger.Profiler.ProfilerServiceImpl::_cameraListeners
	SRList_1_t4287523865 * ____cameraListeners_10;
	// SRDebugger.CircularBuffer`1<SRDebugger.Services.ProfilerFrame> SRDebugger.Profiler.ProfilerServiceImpl::_frameBuffer
	CircularBuffer_1_t299480543 * ____frameBuffer_11;
	// UnityEngine.Camera[] SRDebugger.Profiler.ProfilerServiceImpl::_cameraCache
	CameraU5BU5D_t3079764780* ____cameraCache_12;
	// SRDebugger.Profiler.ProfilerLateUpdateListener SRDebugger.Profiler.ProfilerServiceImpl::_lateUpdateListener
	ProfilerLateUpdateListener_t3472284740 * ____lateUpdateListener_13;
	// System.Double SRDebugger.Profiler.ProfilerServiceImpl::_renderDuration
	double ____renderDuration_14;
	// System.Int32 SRDebugger.Profiler.ProfilerServiceImpl::_reportedCameras
	int32_t ____reportedCameras_15;
	// System.Diagnostics.Stopwatch SRDebugger.Profiler.ProfilerServiceImpl::_stopwatch
	Stopwatch_t1380178105 * ____stopwatch_16;
	// System.Double SRDebugger.Profiler.ProfilerServiceImpl::_updateDuration
	double ____updateDuration_17;
	// System.Double SRDebugger.Profiler.ProfilerServiceImpl::_updateToRenderDuration
	double ____updateToRenderDuration_18;
	// System.Single SRDebugger.Profiler.ProfilerServiceImpl::<AverageFrameTime>k__BackingField
	float ___U3CAverageFrameTimeU3Ek__BackingField_19;
	// System.Single SRDebugger.Profiler.ProfilerServiceImpl::<LastFrameTime>k__BackingField
	float ___U3CLastFrameTimeU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of__cameraListeners_10() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____cameraListeners_10)); }
	inline SRList_1_t4287523865 * get__cameraListeners_10() const { return ____cameraListeners_10; }
	inline SRList_1_t4287523865 ** get_address_of__cameraListeners_10() { return &____cameraListeners_10; }
	inline void set__cameraListeners_10(SRList_1_t4287523865 * value)
	{
		____cameraListeners_10 = value;
		Il2CppCodeGenWriteBarrier(&____cameraListeners_10, value);
	}

	inline static int32_t get_offset_of__frameBuffer_11() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____frameBuffer_11)); }
	inline CircularBuffer_1_t299480543 * get__frameBuffer_11() const { return ____frameBuffer_11; }
	inline CircularBuffer_1_t299480543 ** get_address_of__frameBuffer_11() { return &____frameBuffer_11; }
	inline void set__frameBuffer_11(CircularBuffer_1_t299480543 * value)
	{
		____frameBuffer_11 = value;
		Il2CppCodeGenWriteBarrier(&____frameBuffer_11, value);
	}

	inline static int32_t get_offset_of__cameraCache_12() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____cameraCache_12)); }
	inline CameraU5BU5D_t3079764780* get__cameraCache_12() const { return ____cameraCache_12; }
	inline CameraU5BU5D_t3079764780** get_address_of__cameraCache_12() { return &____cameraCache_12; }
	inline void set__cameraCache_12(CameraU5BU5D_t3079764780* value)
	{
		____cameraCache_12 = value;
		Il2CppCodeGenWriteBarrier(&____cameraCache_12, value);
	}

	inline static int32_t get_offset_of__lateUpdateListener_13() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____lateUpdateListener_13)); }
	inline ProfilerLateUpdateListener_t3472284740 * get__lateUpdateListener_13() const { return ____lateUpdateListener_13; }
	inline ProfilerLateUpdateListener_t3472284740 ** get_address_of__lateUpdateListener_13() { return &____lateUpdateListener_13; }
	inline void set__lateUpdateListener_13(ProfilerLateUpdateListener_t3472284740 * value)
	{
		____lateUpdateListener_13 = value;
		Il2CppCodeGenWriteBarrier(&____lateUpdateListener_13, value);
	}

	inline static int32_t get_offset_of__renderDuration_14() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____renderDuration_14)); }
	inline double get__renderDuration_14() const { return ____renderDuration_14; }
	inline double* get_address_of__renderDuration_14() { return &____renderDuration_14; }
	inline void set__renderDuration_14(double value)
	{
		____renderDuration_14 = value;
	}

	inline static int32_t get_offset_of__reportedCameras_15() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____reportedCameras_15)); }
	inline int32_t get__reportedCameras_15() const { return ____reportedCameras_15; }
	inline int32_t* get_address_of__reportedCameras_15() { return &____reportedCameras_15; }
	inline void set__reportedCameras_15(int32_t value)
	{
		____reportedCameras_15 = value;
	}

	inline static int32_t get_offset_of__stopwatch_16() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____stopwatch_16)); }
	inline Stopwatch_t1380178105 * get__stopwatch_16() const { return ____stopwatch_16; }
	inline Stopwatch_t1380178105 ** get_address_of__stopwatch_16() { return &____stopwatch_16; }
	inline void set__stopwatch_16(Stopwatch_t1380178105 * value)
	{
		____stopwatch_16 = value;
		Il2CppCodeGenWriteBarrier(&____stopwatch_16, value);
	}

	inline static int32_t get_offset_of__updateDuration_17() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____updateDuration_17)); }
	inline double get__updateDuration_17() const { return ____updateDuration_17; }
	inline double* get_address_of__updateDuration_17() { return &____updateDuration_17; }
	inline void set__updateDuration_17(double value)
	{
		____updateDuration_17 = value;
	}

	inline static int32_t get_offset_of__updateToRenderDuration_18() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ____updateToRenderDuration_18)); }
	inline double get__updateToRenderDuration_18() const { return ____updateToRenderDuration_18; }
	inline double* get_address_of__updateToRenderDuration_18() { return &____updateToRenderDuration_18; }
	inline void set__updateToRenderDuration_18(double value)
	{
		____updateToRenderDuration_18 = value;
	}

	inline static int32_t get_offset_of_U3CAverageFrameTimeU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ___U3CAverageFrameTimeU3Ek__BackingField_19)); }
	inline float get_U3CAverageFrameTimeU3Ek__BackingField_19() const { return ___U3CAverageFrameTimeU3Ek__BackingField_19; }
	inline float* get_address_of_U3CAverageFrameTimeU3Ek__BackingField_19() { return &___U3CAverageFrameTimeU3Ek__BackingField_19; }
	inline void set_U3CAverageFrameTimeU3Ek__BackingField_19(float value)
	{
		___U3CAverageFrameTimeU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CLastFrameTimeU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(ProfilerServiceImpl_t2867375934, ___U3CLastFrameTimeU3Ek__BackingField_20)); }
	inline float get_U3CLastFrameTimeU3Ek__BackingField_20() const { return ___U3CLastFrameTimeU3Ek__BackingField_20; }
	inline float* get_address_of_U3CLastFrameTimeU3Ek__BackingField_20() { return &___U3CLastFrameTimeU3Ek__BackingField_20; }
	inline void set_U3CLastFrameTimeU3Ek__BackingField_20(float value)
	{
		___U3CLastFrameTimeU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
