﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_GUI899174319.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField
struct  GUILayoutConfirmPasswordField_t3514733796  : public GUILayoutAction_t899174319
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::text
	FsmString_t2414474701 * ___text_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::maxLength
	FsmInt_t1273009179 * ___maxLength_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::style
	FsmString_t2414474701 * ___style_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::changedEvent
	FsmEvent_t1258573736 * ___changedEvent_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::mask
	FsmString_t2414474701 * ___mask_17;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::confirm
	FsmBool_t664485696 * ___confirm_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutConfirmPasswordField::password
	FsmString_t2414474701 * ___password_19;

public:
	inline static int32_t get_offset_of_text_13() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___text_13)); }
	inline FsmString_t2414474701 * get_text_13() const { return ___text_13; }
	inline FsmString_t2414474701 ** get_address_of_text_13() { return &___text_13; }
	inline void set_text_13(FsmString_t2414474701 * value)
	{
		___text_13 = value;
		Il2CppCodeGenWriteBarrier(&___text_13, value);
	}

	inline static int32_t get_offset_of_maxLength_14() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___maxLength_14)); }
	inline FsmInt_t1273009179 * get_maxLength_14() const { return ___maxLength_14; }
	inline FsmInt_t1273009179 ** get_address_of_maxLength_14() { return &___maxLength_14; }
	inline void set_maxLength_14(FsmInt_t1273009179 * value)
	{
		___maxLength_14 = value;
		Il2CppCodeGenWriteBarrier(&___maxLength_14, value);
	}

	inline static int32_t get_offset_of_style_15() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___style_15)); }
	inline FsmString_t2414474701 * get_style_15() const { return ___style_15; }
	inline FsmString_t2414474701 ** get_address_of_style_15() { return &___style_15; }
	inline void set_style_15(FsmString_t2414474701 * value)
	{
		___style_15 = value;
		Il2CppCodeGenWriteBarrier(&___style_15, value);
	}

	inline static int32_t get_offset_of_changedEvent_16() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___changedEvent_16)); }
	inline FsmEvent_t1258573736 * get_changedEvent_16() const { return ___changedEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_changedEvent_16() { return &___changedEvent_16; }
	inline void set_changedEvent_16(FsmEvent_t1258573736 * value)
	{
		___changedEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___changedEvent_16, value);
	}

	inline static int32_t get_offset_of_mask_17() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___mask_17)); }
	inline FsmString_t2414474701 * get_mask_17() const { return ___mask_17; }
	inline FsmString_t2414474701 ** get_address_of_mask_17() { return &___mask_17; }
	inline void set_mask_17(FsmString_t2414474701 * value)
	{
		___mask_17 = value;
		Il2CppCodeGenWriteBarrier(&___mask_17, value);
	}

	inline static int32_t get_offset_of_confirm_18() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___confirm_18)); }
	inline FsmBool_t664485696 * get_confirm_18() const { return ___confirm_18; }
	inline FsmBool_t664485696 ** get_address_of_confirm_18() { return &___confirm_18; }
	inline void set_confirm_18(FsmBool_t664485696 * value)
	{
		___confirm_18 = value;
		Il2CppCodeGenWriteBarrier(&___confirm_18, value);
	}

	inline static int32_t get_offset_of_password_19() { return static_cast<int32_t>(offsetof(GUILayoutConfirmPasswordField_t3514733796, ___password_19)); }
	inline FsmString_t2414474701 * get_password_19() const { return ___password_19; }
	inline FsmString_t2414474701 ** get_address_of_password_19() { return &___password_19; }
	inline void set_password_19(FsmString_t2414474701 * value)
	{
		___password_19 = value;
		Il2CppCodeGenWriteBarrier(&___password_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
