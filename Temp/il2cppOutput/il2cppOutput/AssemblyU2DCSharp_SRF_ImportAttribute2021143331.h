﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.ImportAttribute
struct  ImportAttribute_t2021143331  : public Attribute_t542643598
{
public:
	// System.Type SRF.ImportAttribute::Service
	Type_t * ___Service_0;

public:
	inline static int32_t get_offset_of_Service_0() { return static_cast<int32_t>(offsetof(ImportAttribute_t2021143331, ___Service_0)); }
	inline Type_t * get_Service_0() const { return ___Service_0; }
	inline Type_t ** get_address_of_Service_0() { return &___Service_0; }
	inline void set_Service_0(Type_t * value)
	{
		___Service_0 = value;
		Il2CppCodeGenWriteBarrier(&___Service_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
