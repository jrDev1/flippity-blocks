﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2178968864.h"

// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// UnityEngine.Transform
struct Transform_t3275118058;
// SimpleSpawner
struct SimpleSpawner_t3347155754;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleSpawner/<Despawner>c__Iterator2
struct  U3CDespawnerU3Ec__Iterator2_t3410270119  : public Il2CppObject
{
public:
	// PathologicalGames.SpawnPool SimpleSpawner/<Despawner>c__Iterator2::<shapesPool>__0
	SpawnPool_t2419717525 * ___U3CshapesPoolU3E__0_0;
	// System.Collections.Generic.List`1<UnityEngine.Transform> SimpleSpawner/<Despawner>c__Iterator2::<spawnedCopy>__0
	List_1_t2644239190 * ___U3CspawnedCopyU3E__0_1;
	// System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform> SimpleSpawner/<Despawner>c__Iterator2::$locvar0
	Enumerator_t2178968864  ___U24locvar0_2;
	// UnityEngine.Transform SimpleSpawner/<Despawner>c__Iterator2::<instance>__1
	Transform_t3275118058 * ___U3CinstanceU3E__1_3;
	// SimpleSpawner SimpleSpawner/<Despawner>c__Iterator2::$this
	SimpleSpawner_t3347155754 * ___U24this_4;
	// System.Object SimpleSpawner/<Despawner>c__Iterator2::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean SimpleSpawner/<Despawner>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 SimpleSpawner/<Despawner>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CshapesPoolU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U3CshapesPoolU3E__0_0)); }
	inline SpawnPool_t2419717525 * get_U3CshapesPoolU3E__0_0() const { return ___U3CshapesPoolU3E__0_0; }
	inline SpawnPool_t2419717525 ** get_address_of_U3CshapesPoolU3E__0_0() { return &___U3CshapesPoolU3E__0_0; }
	inline void set_U3CshapesPoolU3E__0_0(SpawnPool_t2419717525 * value)
	{
		___U3CshapesPoolU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CshapesPoolU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CspawnedCopyU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U3CspawnedCopyU3E__0_1)); }
	inline List_1_t2644239190 * get_U3CspawnedCopyU3E__0_1() const { return ___U3CspawnedCopyU3E__0_1; }
	inline List_1_t2644239190 ** get_address_of_U3CspawnedCopyU3E__0_1() { return &___U3CspawnedCopyU3E__0_1; }
	inline void set_U3CspawnedCopyU3E__0_1(List_1_t2644239190 * value)
	{
		___U3CspawnedCopyU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CspawnedCopyU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U24locvar0_2)); }
	inline Enumerator_t2178968864  get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline Enumerator_t2178968864 * get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(Enumerator_t2178968864  value)
	{
		___U24locvar0_2 = value;
	}

	inline static int32_t get_offset_of_U3CinstanceU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U3CinstanceU3E__1_3)); }
	inline Transform_t3275118058 * get_U3CinstanceU3E__1_3() const { return ___U3CinstanceU3E__1_3; }
	inline Transform_t3275118058 ** get_address_of_U3CinstanceU3E__1_3() { return &___U3CinstanceU3E__1_3; }
	inline void set_U3CinstanceU3E__1_3(Transform_t3275118058 * value)
	{
		___U3CinstanceU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinstanceU3E__1_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U24this_4)); }
	inline SimpleSpawner_t3347155754 * get_U24this_4() const { return ___U24this_4; }
	inline SimpleSpawner_t3347155754 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SimpleSpawner_t3347155754 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDespawnerU3Ec__Iterator2_t3410270119, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
