﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRFTransformExtensions/<GetChildren>c__Iterator0
struct  U3CGetChildrenU3Ec__Iterator0_t2448828195  : public Il2CppObject
{
public:
	// System.Int32 SRF.SRFTransformExtensions/<GetChildren>c__Iterator0::<i>__0
	int32_t ___U3CiU3E__0_0;
	// UnityEngine.Transform SRF.SRFTransformExtensions/<GetChildren>c__Iterator0::t
	Transform_t3275118058 * ___t_1;
	// UnityEngine.Transform SRF.SRFTransformExtensions/<GetChildren>c__Iterator0::$current
	Transform_t3275118058 * ___U24current_2;
	// System.Boolean SRF.SRFTransformExtensions/<GetChildren>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 SRF.SRFTransformExtensions/<GetChildren>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__Iterator0_t2448828195, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__Iterator0_t2448828195, ___t_1)); }
	inline Transform_t3275118058 * get_t_1() const { return ___t_1; }
	inline Transform_t3275118058 ** get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(Transform_t3275118058 * value)
	{
		___t_1 = value;
		Il2CppCodeGenWriteBarrier(&___t_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__Iterator0_t2448828195, ___U24current_2)); }
	inline Transform_t3275118058 * get_U24current_2() const { return ___U24current_2; }
	inline Transform_t3275118058 ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Transform_t3275118058 * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__Iterator0_t2448828195, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetChildrenU3Ec__Iterator0_t2448828195, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
