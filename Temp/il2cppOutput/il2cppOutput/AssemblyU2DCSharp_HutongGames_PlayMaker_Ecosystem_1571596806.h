﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct  PlayMakerEvent_t1571596806  : public Il2CppObject
{
public:
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::eventName
	String_t* ___eventName_0;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::allowLocalEvents
	bool ___allowLocalEvents_1;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent::defaultEventName
	String_t* ___defaultEventName_2;

public:
	inline static int32_t get_offset_of_eventName_0() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806, ___eventName_0)); }
	inline String_t* get_eventName_0() const { return ___eventName_0; }
	inline String_t** get_address_of_eventName_0() { return &___eventName_0; }
	inline void set_eventName_0(String_t* value)
	{
		___eventName_0 = value;
		Il2CppCodeGenWriteBarrier(&___eventName_0, value);
	}

	inline static int32_t get_offset_of_allowLocalEvents_1() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806, ___allowLocalEvents_1)); }
	inline bool get_allowLocalEvents_1() const { return ___allowLocalEvents_1; }
	inline bool* get_address_of_allowLocalEvents_1() { return &___allowLocalEvents_1; }
	inline void set_allowLocalEvents_1(bool value)
	{
		___allowLocalEvents_1 = value;
	}

	inline static int32_t get_offset_of_defaultEventName_2() { return static_cast<int32_t>(offsetof(PlayMakerEvent_t1571596806, ___defaultEventName_2)); }
	inline String_t* get_defaultEventName_2() const { return ___defaultEventName_2; }
	inline String_t** get_address_of_defaultEventName_2() { return &___defaultEventName_2; }
	inline void set_defaultEventName_2(String_t* value)
	{
		___defaultEventName_2 = value;
		Il2CppCodeGenWriteBarrier(&___defaultEventName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
