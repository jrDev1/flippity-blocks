﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugUtil
struct  SRDebugUtil_t1529540768  : public Il2CppObject
{
public:

public:
};

struct SRDebugUtil_t1529540768_StaticFields
{
public:
	// System.Boolean SRDebugUtil::<IsFixedUpdate>k__BackingField
	bool ___U3CIsFixedUpdateU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CIsFixedUpdateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SRDebugUtil_t1529540768_StaticFields, ___U3CIsFixedUpdateU3Ek__BackingField_1)); }
	inline bool get_U3CIsFixedUpdateU3Ek__BackingField_1() const { return ___U3CIsFixedUpdateU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsFixedUpdateU3Ek__BackingField_1() { return &___U3CIsFixedUpdateU3Ek__BackingField_1; }
	inline void set_U3CIsFixedUpdateU3Ek__BackingField_1(bool value)
	{
		___U3CIsFixedUpdateU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
