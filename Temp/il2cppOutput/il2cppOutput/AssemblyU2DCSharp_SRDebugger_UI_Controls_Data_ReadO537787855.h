﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_DataBound3384854171.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.ReadOnlyControl
struct  ReadOnlyControl_t537787855  : public DataBoundControl_t3384854171
{
public:
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.ReadOnlyControl::ValueText
	Text_t356221433 * ___ValueText_17;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.ReadOnlyControl::Title
	Text_t356221433 * ___Title_18;

public:
	inline static int32_t get_offset_of_ValueText_17() { return static_cast<int32_t>(offsetof(ReadOnlyControl_t537787855, ___ValueText_17)); }
	inline Text_t356221433 * get_ValueText_17() const { return ___ValueText_17; }
	inline Text_t356221433 ** get_address_of_ValueText_17() { return &___ValueText_17; }
	inline void set_ValueText_17(Text_t356221433 * value)
	{
		___ValueText_17 = value;
		Il2CppCodeGenWriteBarrier(&___ValueText_17, value);
	}

	inline static int32_t get_offset_of_Title_18() { return static_cast<int32_t>(offsetof(ReadOnlyControl_t537787855, ___Title_18)); }
	inline Text_t356221433 * get_Title_18() const { return ___Title_18; }
	inline Text_t356221433 ** get_address_of_Title_18() { return &___Title_18; }
	inline void set_Title_18(Text_t356221433 * value)
	{
		___Title_18 = value;
		Il2CppCodeGenWriteBarrier(&___Title_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
