﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_PlayMakerUGuiComponentProxy_PlayM532382195.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiComponentProxy/FsmVariableSetup
struct  FsmVariableSetup_t2450860713 
{
public:
	// PlayMakerUGuiComponentProxy/PlayMakerProxyVariableTarget PlayMakerUGuiComponentProxy/FsmVariableSetup::target
	int32_t ___target_0;
	// UnityEngine.GameObject PlayMakerUGuiComponentProxy/FsmVariableSetup::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// PlayMakerFSM PlayMakerUGuiComponentProxy/FsmVariableSetup::fsmComponent
	PlayMakerFSM_t437737208 * ___fsmComponent_2;
	// System.Int32 PlayMakerUGuiComponentProxy/FsmVariableSetup::fsmIndex
	int32_t ___fsmIndex_3;
	// System.Int32 PlayMakerUGuiComponentProxy/FsmVariableSetup::variableIndex
	int32_t ___variableIndex_4;
	// HutongGames.PlayMaker.VariableType PlayMakerUGuiComponentProxy/FsmVariableSetup::variableType
	int32_t ___variableType_5;
	// System.String PlayMakerUGuiComponentProxy/FsmVariableSetup::variableName
	String_t* ___variableName_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}

	inline static int32_t get_offset_of_fsmComponent_2() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___fsmComponent_2)); }
	inline PlayMakerFSM_t437737208 * get_fsmComponent_2() const { return ___fsmComponent_2; }
	inline PlayMakerFSM_t437737208 ** get_address_of_fsmComponent_2() { return &___fsmComponent_2; }
	inline void set_fsmComponent_2(PlayMakerFSM_t437737208 * value)
	{
		___fsmComponent_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsmComponent_2, value);
	}

	inline static int32_t get_offset_of_fsmIndex_3() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___fsmIndex_3)); }
	inline int32_t get_fsmIndex_3() const { return ___fsmIndex_3; }
	inline int32_t* get_address_of_fsmIndex_3() { return &___fsmIndex_3; }
	inline void set_fsmIndex_3(int32_t value)
	{
		___fsmIndex_3 = value;
	}

	inline static int32_t get_offset_of_variableIndex_4() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___variableIndex_4)); }
	inline int32_t get_variableIndex_4() const { return ___variableIndex_4; }
	inline int32_t* get_address_of_variableIndex_4() { return &___variableIndex_4; }
	inline void set_variableIndex_4(int32_t value)
	{
		___variableIndex_4 = value;
	}

	inline static int32_t get_offset_of_variableType_5() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___variableType_5)); }
	inline int32_t get_variableType_5() const { return ___variableType_5; }
	inline int32_t* get_address_of_variableType_5() { return &___variableType_5; }
	inline void set_variableType_5(int32_t value)
	{
		___variableType_5 = value;
	}

	inline static int32_t get_offset_of_variableName_6() { return static_cast<int32_t>(offsetof(FsmVariableSetup_t2450860713, ___variableName_6)); }
	inline String_t* get_variableName_6() const { return ___variableName_6; }
	inline String_t** get_address_of_variableName_6() { return &___variableName_6; }
	inline void set_variableName_6(String_t* value)
	{
		___variableName_6 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PlayMakerUGuiComponentProxy/FsmVariableSetup
struct FsmVariableSetup_t2450860713_marshaled_pinvoke
{
	int32_t ___target_0;
	GameObject_t1756533147 * ___gameObject_1;
	PlayMakerFSM_t437737208 * ___fsmComponent_2;
	int32_t ___fsmIndex_3;
	int32_t ___variableIndex_4;
	int32_t ___variableType_5;
	char* ___variableName_6;
};
// Native definition for COM marshalling of PlayMakerUGuiComponentProxy/FsmVariableSetup
struct FsmVariableSetup_t2450860713_marshaled_com
{
	int32_t ___target_0;
	GameObject_t1756533147 * ___gameObject_1;
	PlayMakerFSM_t437737208 * ___fsmComponent_2;
	int32_t ___fsmIndex_3;
	int32_t ___variableIndex_4;
	int32_t ___variableType_5;
	Il2CppChar* ___variableName_6;
};
