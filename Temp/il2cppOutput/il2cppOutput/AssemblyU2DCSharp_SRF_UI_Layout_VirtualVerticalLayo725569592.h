﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRF.UI.StyleRoot
struct StyleRoot_t1691968825;
// SRF.UI.Layout.IVirtualView
struct IVirtualView_t279565859;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.Layout.VirtualVerticalLayoutGroup/Row
struct  Row_t725569592  : public Il2CppObject
{
public:
	// System.Object SRF.UI.Layout.VirtualVerticalLayoutGroup/Row::Data
	Il2CppObject * ___Data_0;
	// System.Int32 SRF.UI.Layout.VirtualVerticalLayoutGroup/Row::Index
	int32_t ___Index_1;
	// UnityEngine.RectTransform SRF.UI.Layout.VirtualVerticalLayoutGroup/Row::Rect
	RectTransform_t3349966182 * ___Rect_2;
	// SRF.UI.StyleRoot SRF.UI.Layout.VirtualVerticalLayoutGroup/Row::Root
	StyleRoot_t1691968825 * ___Root_3;
	// SRF.UI.Layout.IVirtualView SRF.UI.Layout.VirtualVerticalLayoutGroup/Row::View
	Il2CppObject * ___View_4;

public:
	inline static int32_t get_offset_of_Data_0() { return static_cast<int32_t>(offsetof(Row_t725569592, ___Data_0)); }
	inline Il2CppObject * get_Data_0() const { return ___Data_0; }
	inline Il2CppObject ** get_address_of_Data_0() { return &___Data_0; }
	inline void set_Data_0(Il2CppObject * value)
	{
		___Data_0 = value;
		Il2CppCodeGenWriteBarrier(&___Data_0, value);
	}

	inline static int32_t get_offset_of_Index_1() { return static_cast<int32_t>(offsetof(Row_t725569592, ___Index_1)); }
	inline int32_t get_Index_1() const { return ___Index_1; }
	inline int32_t* get_address_of_Index_1() { return &___Index_1; }
	inline void set_Index_1(int32_t value)
	{
		___Index_1 = value;
	}

	inline static int32_t get_offset_of_Rect_2() { return static_cast<int32_t>(offsetof(Row_t725569592, ___Rect_2)); }
	inline RectTransform_t3349966182 * get_Rect_2() const { return ___Rect_2; }
	inline RectTransform_t3349966182 ** get_address_of_Rect_2() { return &___Rect_2; }
	inline void set_Rect_2(RectTransform_t3349966182 * value)
	{
		___Rect_2 = value;
		Il2CppCodeGenWriteBarrier(&___Rect_2, value);
	}

	inline static int32_t get_offset_of_Root_3() { return static_cast<int32_t>(offsetof(Row_t725569592, ___Root_3)); }
	inline StyleRoot_t1691968825 * get_Root_3() const { return ___Root_3; }
	inline StyleRoot_t1691968825 ** get_address_of_Root_3() { return &___Root_3; }
	inline void set_Root_3(StyleRoot_t1691968825 * value)
	{
		___Root_3 = value;
		Il2CppCodeGenWriteBarrier(&___Root_3, value);
	}

	inline static int32_t get_offset_of_View_4() { return static_cast<int32_t>(offsetof(Row_t725569592, ___View_4)); }
	inline Il2CppObject * get_View_4() const { return ___View_4; }
	inline Il2CppObject ** get_address_of_View_4() { return &___View_4; }
	inline void set_View_4(Il2CppObject * value)
	{
		___View_4 = value;
		Il2CppCodeGenWriteBarrier(&___View_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
