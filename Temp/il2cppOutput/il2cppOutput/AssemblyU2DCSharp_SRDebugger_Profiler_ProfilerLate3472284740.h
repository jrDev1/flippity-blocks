﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Action
struct Action_t3226471752;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Profiler.ProfilerLateUpdateListener
struct  ProfilerLateUpdateListener_t3472284740  : public MonoBehaviour_t1158329972
{
public:
	// System.Action SRDebugger.Profiler.ProfilerLateUpdateListener::OnLateUpdate
	Action_t3226471752 * ___OnLateUpdate_2;

public:
	inline static int32_t get_offset_of_OnLateUpdate_2() { return static_cast<int32_t>(offsetof(ProfilerLateUpdateListener_t3472284740, ___OnLateUpdate_2)); }
	inline Action_t3226471752 * get_OnLateUpdate_2() const { return ___OnLateUpdate_2; }
	inline Action_t3226471752 ** get_address_of_OnLateUpdate_2() { return &___OnLateUpdate_2; }
	inline void set_OnLateUpdate_2(Action_t3226471752 * value)
	{
		___OnLateUpdate_2 = value;
		Il2CppCodeGenWriteBarrier(&___OnLateUpdate_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
