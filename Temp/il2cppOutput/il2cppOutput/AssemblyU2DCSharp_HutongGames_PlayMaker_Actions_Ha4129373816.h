﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t16885852;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableAddMany
struct  HashTableAddMany_t4129373816  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableAddMany::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableAddMany::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.HashTableAddMany::keys
	FsmStringU5BU5D_t2699231328* ___keys_14;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.HashTableAddMany::variables
	FsmVarU5BU5D_t16885852* ___variables_15;
	// System.Boolean HutongGames.PlayMaker.Actions.HashTableAddMany::convertIntsToBytes
	bool ___convertIntsToBytes_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableAddMany::successEvent
	FsmEvent_t1258573736 * ___successEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableAddMany::keyExistsAlreadyEvent
	FsmEvent_t1258573736 * ___keyExistsAlreadyEvent_18;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___keys_14)); }
	inline FsmStringU5BU5D_t2699231328* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t2699231328** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t2699231328* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier(&___keys_14, value);
	}

	inline static int32_t get_offset_of_variables_15() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___variables_15)); }
	inline FsmVarU5BU5D_t16885852* get_variables_15() const { return ___variables_15; }
	inline FsmVarU5BU5D_t16885852** get_address_of_variables_15() { return &___variables_15; }
	inline void set_variables_15(FsmVarU5BU5D_t16885852* value)
	{
		___variables_15 = value;
		Il2CppCodeGenWriteBarrier(&___variables_15, value);
	}

	inline static int32_t get_offset_of_convertIntsToBytes_16() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___convertIntsToBytes_16)); }
	inline bool get_convertIntsToBytes_16() const { return ___convertIntsToBytes_16; }
	inline bool* get_address_of_convertIntsToBytes_16() { return &___convertIntsToBytes_16; }
	inline void set_convertIntsToBytes_16(bool value)
	{
		___convertIntsToBytes_16 = value;
	}

	inline static int32_t get_offset_of_successEvent_17() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___successEvent_17)); }
	inline FsmEvent_t1258573736 * get_successEvent_17() const { return ___successEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_successEvent_17() { return &___successEvent_17; }
	inline void set_successEvent_17(FsmEvent_t1258573736 * value)
	{
		___successEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_17, value);
	}

	inline static int32_t get_offset_of_keyExistsAlreadyEvent_18() { return static_cast<int32_t>(offsetof(HashTableAddMany_t4129373816, ___keyExistsAlreadyEvent_18)); }
	inline FsmEvent_t1258573736 * get_keyExistsAlreadyEvent_18() const { return ___keyExistsAlreadyEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_keyExistsAlreadyEvent_18() { return &___keyExistsAlreadyEvent_18; }
	inline void set_keyExistsAlreadyEvent_18(FsmEvent_t1258573736 * value)
	{
		___keyExistsAlreadyEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___keyExistsAlreadyEvent_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
