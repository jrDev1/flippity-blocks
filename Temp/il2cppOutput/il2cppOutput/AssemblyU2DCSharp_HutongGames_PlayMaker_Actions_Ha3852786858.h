﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableContainsValue
struct  HashTableContainsValue_t3852786858  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableContainsValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableContainsValue::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.HashTableContainsValue::variable
	FsmVar_t2872592513 * ___variable_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.HashTableContainsValue::containsValue
	FsmBool_t664485696 * ___containsValue_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableContainsValue::valueFoundEvent
	FsmEvent_t1258573736 * ___valueFoundEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableContainsValue::valueNotFoundEvent
	FsmEvent_t1258573736 * ___valueNotFoundEvent_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableContainsValue_t3852786858, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableContainsValue_t3852786858, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_variable_14() { return static_cast<int32_t>(offsetof(HashTableContainsValue_t3852786858, ___variable_14)); }
	inline FsmVar_t2872592513 * get_variable_14() const { return ___variable_14; }
	inline FsmVar_t2872592513 ** get_address_of_variable_14() { return &___variable_14; }
	inline void set_variable_14(FsmVar_t2872592513 * value)
	{
		___variable_14 = value;
		Il2CppCodeGenWriteBarrier(&___variable_14, value);
	}

	inline static int32_t get_offset_of_containsValue_15() { return static_cast<int32_t>(offsetof(HashTableContainsValue_t3852786858, ___containsValue_15)); }
	inline FsmBool_t664485696 * get_containsValue_15() const { return ___containsValue_15; }
	inline FsmBool_t664485696 ** get_address_of_containsValue_15() { return &___containsValue_15; }
	inline void set_containsValue_15(FsmBool_t664485696 * value)
	{
		___containsValue_15 = value;
		Il2CppCodeGenWriteBarrier(&___containsValue_15, value);
	}

	inline static int32_t get_offset_of_valueFoundEvent_16() { return static_cast<int32_t>(offsetof(HashTableContainsValue_t3852786858, ___valueFoundEvent_16)); }
	inline FsmEvent_t1258573736 * get_valueFoundEvent_16() const { return ___valueFoundEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_valueFoundEvent_16() { return &___valueFoundEvent_16; }
	inline void set_valueFoundEvent_16(FsmEvent_t1258573736 * value)
	{
		___valueFoundEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___valueFoundEvent_16, value);
	}

	inline static int32_t get_offset_of_valueNotFoundEvent_17() { return static_cast<int32_t>(offsetof(HashTableContainsValue_t3852786858, ___valueNotFoundEvent_17)); }
	inline FsmEvent_t1258573736 * get_valueNotFoundEvent_17() const { return ___valueNotFoundEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_valueNotFoundEvent_17() { return &___valueNotFoundEvent_17; }
	inline void set_valueNotFoundEvent_17(FsmEvent_t1258573736 * value)
	{
		___valueNotFoundEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___valueNotFoundEvent_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
