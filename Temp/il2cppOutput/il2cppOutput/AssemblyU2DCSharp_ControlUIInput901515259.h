﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlUIInput
struct  ControlUIInput_t901515259  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ControlUIInput::getAxisText
	Text_t356221433 * ___getAxisText_2;
	// UnityEngine.UI.Text ControlUIInput::getAxisSpeedText
	Text_t356221433 * ___getAxisSpeedText_3;
	// UnityEngine.UI.Text ControlUIInput::getAxisYText
	Text_t356221433 * ___getAxisYText_4;
	// UnityEngine.UI.Text ControlUIInput::getAxisYSpeedText
	Text_t356221433 * ___getAxisYSpeedText_5;
	// UnityEngine.UI.Text ControlUIInput::downRightText
	Text_t356221433 * ___downRightText_6;
	// UnityEngine.UI.Text ControlUIInput::downDownText
	Text_t356221433 * ___downDownText_7;
	// UnityEngine.UI.Text ControlUIInput::downLeftText
	Text_t356221433 * ___downLeftText_8;
	// UnityEngine.UI.Text ControlUIInput::downUpText
	Text_t356221433 * ___downUpText_9;
	// UnityEngine.UI.Text ControlUIInput::rightText
	Text_t356221433 * ___rightText_10;
	// UnityEngine.UI.Text ControlUIInput::downText
	Text_t356221433 * ___downText_11;
	// UnityEngine.UI.Text ControlUIInput::leftText
	Text_t356221433 * ___leftText_12;
	// UnityEngine.UI.Text ControlUIInput::upText
	Text_t356221433 * ___upText_13;

public:
	inline static int32_t get_offset_of_getAxisText_2() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___getAxisText_2)); }
	inline Text_t356221433 * get_getAxisText_2() const { return ___getAxisText_2; }
	inline Text_t356221433 ** get_address_of_getAxisText_2() { return &___getAxisText_2; }
	inline void set_getAxisText_2(Text_t356221433 * value)
	{
		___getAxisText_2 = value;
		Il2CppCodeGenWriteBarrier(&___getAxisText_2, value);
	}

	inline static int32_t get_offset_of_getAxisSpeedText_3() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___getAxisSpeedText_3)); }
	inline Text_t356221433 * get_getAxisSpeedText_3() const { return ___getAxisSpeedText_3; }
	inline Text_t356221433 ** get_address_of_getAxisSpeedText_3() { return &___getAxisSpeedText_3; }
	inline void set_getAxisSpeedText_3(Text_t356221433 * value)
	{
		___getAxisSpeedText_3 = value;
		Il2CppCodeGenWriteBarrier(&___getAxisSpeedText_3, value);
	}

	inline static int32_t get_offset_of_getAxisYText_4() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___getAxisYText_4)); }
	inline Text_t356221433 * get_getAxisYText_4() const { return ___getAxisYText_4; }
	inline Text_t356221433 ** get_address_of_getAxisYText_4() { return &___getAxisYText_4; }
	inline void set_getAxisYText_4(Text_t356221433 * value)
	{
		___getAxisYText_4 = value;
		Il2CppCodeGenWriteBarrier(&___getAxisYText_4, value);
	}

	inline static int32_t get_offset_of_getAxisYSpeedText_5() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___getAxisYSpeedText_5)); }
	inline Text_t356221433 * get_getAxisYSpeedText_5() const { return ___getAxisYSpeedText_5; }
	inline Text_t356221433 ** get_address_of_getAxisYSpeedText_5() { return &___getAxisYSpeedText_5; }
	inline void set_getAxisYSpeedText_5(Text_t356221433 * value)
	{
		___getAxisYSpeedText_5 = value;
		Il2CppCodeGenWriteBarrier(&___getAxisYSpeedText_5, value);
	}

	inline static int32_t get_offset_of_downRightText_6() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___downRightText_6)); }
	inline Text_t356221433 * get_downRightText_6() const { return ___downRightText_6; }
	inline Text_t356221433 ** get_address_of_downRightText_6() { return &___downRightText_6; }
	inline void set_downRightText_6(Text_t356221433 * value)
	{
		___downRightText_6 = value;
		Il2CppCodeGenWriteBarrier(&___downRightText_6, value);
	}

	inline static int32_t get_offset_of_downDownText_7() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___downDownText_7)); }
	inline Text_t356221433 * get_downDownText_7() const { return ___downDownText_7; }
	inline Text_t356221433 ** get_address_of_downDownText_7() { return &___downDownText_7; }
	inline void set_downDownText_7(Text_t356221433 * value)
	{
		___downDownText_7 = value;
		Il2CppCodeGenWriteBarrier(&___downDownText_7, value);
	}

	inline static int32_t get_offset_of_downLeftText_8() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___downLeftText_8)); }
	inline Text_t356221433 * get_downLeftText_8() const { return ___downLeftText_8; }
	inline Text_t356221433 ** get_address_of_downLeftText_8() { return &___downLeftText_8; }
	inline void set_downLeftText_8(Text_t356221433 * value)
	{
		___downLeftText_8 = value;
		Il2CppCodeGenWriteBarrier(&___downLeftText_8, value);
	}

	inline static int32_t get_offset_of_downUpText_9() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___downUpText_9)); }
	inline Text_t356221433 * get_downUpText_9() const { return ___downUpText_9; }
	inline Text_t356221433 ** get_address_of_downUpText_9() { return &___downUpText_9; }
	inline void set_downUpText_9(Text_t356221433 * value)
	{
		___downUpText_9 = value;
		Il2CppCodeGenWriteBarrier(&___downUpText_9, value);
	}

	inline static int32_t get_offset_of_rightText_10() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___rightText_10)); }
	inline Text_t356221433 * get_rightText_10() const { return ___rightText_10; }
	inline Text_t356221433 ** get_address_of_rightText_10() { return &___rightText_10; }
	inline void set_rightText_10(Text_t356221433 * value)
	{
		___rightText_10 = value;
		Il2CppCodeGenWriteBarrier(&___rightText_10, value);
	}

	inline static int32_t get_offset_of_downText_11() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___downText_11)); }
	inline Text_t356221433 * get_downText_11() const { return ___downText_11; }
	inline Text_t356221433 ** get_address_of_downText_11() { return &___downText_11; }
	inline void set_downText_11(Text_t356221433 * value)
	{
		___downText_11 = value;
		Il2CppCodeGenWriteBarrier(&___downText_11, value);
	}

	inline static int32_t get_offset_of_leftText_12() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___leftText_12)); }
	inline Text_t356221433 * get_leftText_12() const { return ___leftText_12; }
	inline Text_t356221433 ** get_address_of_leftText_12() { return &___leftText_12; }
	inline void set_leftText_12(Text_t356221433 * value)
	{
		___leftText_12 = value;
		Il2CppCodeGenWriteBarrier(&___leftText_12, value);
	}

	inline static int32_t get_offset_of_upText_13() { return static_cast<int32_t>(offsetof(ControlUIInput_t901515259, ___upText_13)); }
	inline Text_t356221433 * get_upText_13() const { return ___upText_13; }
	inline Text_t356221433 ** get_address_of_upText_13() { return &___upText_13; }
	inline void set_upText_13(Text_t356221433 * value)
	{
		___upText_13 = value;
		Il2CppCodeGenWriteBarrier(&___upText_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
