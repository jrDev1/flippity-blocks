﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/PickedObject
struct  PickedObject_t3877774310  : public Il2CppObject
{
public:
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouch/PickedObject::pickedObj
	GameObject_t1756533147 * ___pickedObj_0;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch/PickedObject::pickedCamera
	Camera_t189460977 * ___pickedCamera_1;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/PickedObject::isGUI
	bool ___isGUI_2;

public:
	inline static int32_t get_offset_of_pickedObj_0() { return static_cast<int32_t>(offsetof(PickedObject_t3877774310, ___pickedObj_0)); }
	inline GameObject_t1756533147 * get_pickedObj_0() const { return ___pickedObj_0; }
	inline GameObject_t1756533147 ** get_address_of_pickedObj_0() { return &___pickedObj_0; }
	inline void set_pickedObj_0(GameObject_t1756533147 * value)
	{
		___pickedObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___pickedObj_0, value);
	}

	inline static int32_t get_offset_of_pickedCamera_1() { return static_cast<int32_t>(offsetof(PickedObject_t3877774310, ___pickedCamera_1)); }
	inline Camera_t189460977 * get_pickedCamera_1() const { return ___pickedCamera_1; }
	inline Camera_t189460977 ** get_address_of_pickedCamera_1() { return &___pickedCamera_1; }
	inline void set_pickedCamera_1(Camera_t189460977 * value)
	{
		___pickedCamera_1 = value;
		Il2CppCodeGenWriteBarrier(&___pickedCamera_1, value);
	}

	inline static int32_t get_offset_of_isGUI_2() { return static_cast<int32_t>(offsetof(PickedObject_t3877774310, ___isGUI_2)); }
	inline bool get_isGUI_2() const { return ___isGUI_2; }
	inline bool* get_address_of_isGUI_2() { return &___isGUI_2; }
	inline void set_isGUI_2(bool value)
	{
		___isGUI_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
