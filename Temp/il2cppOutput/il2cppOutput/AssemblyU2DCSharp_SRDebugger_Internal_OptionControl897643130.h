﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.IList`1<SRDebugger.UI.Controls.DataBoundControl>
struct IList_1_t3925794772;
// SRDebugger.UI.Controls.Data.ActionControl
struct ActionControl_t3457065707;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.OptionControlFactory
struct  OptionControlFactory_t897643130  : public Il2CppObject
{
public:

public:
};

struct OptionControlFactory_t897643130_StaticFields
{
public:
	// System.Collections.Generic.IList`1<SRDebugger.UI.Controls.DataBoundControl> SRDebugger.Internal.OptionControlFactory::_dataControlPrefabs
	Il2CppObject* ____dataControlPrefabs_0;
	// SRDebugger.UI.Controls.Data.ActionControl SRDebugger.Internal.OptionControlFactory::_actionControlPrefab
	ActionControl_t3457065707 * ____actionControlPrefab_1;

public:
	inline static int32_t get_offset_of__dataControlPrefabs_0() { return static_cast<int32_t>(offsetof(OptionControlFactory_t897643130_StaticFields, ____dataControlPrefabs_0)); }
	inline Il2CppObject* get__dataControlPrefabs_0() const { return ____dataControlPrefabs_0; }
	inline Il2CppObject** get_address_of__dataControlPrefabs_0() { return &____dataControlPrefabs_0; }
	inline void set__dataControlPrefabs_0(Il2CppObject* value)
	{
		____dataControlPrefabs_0 = value;
		Il2CppCodeGenWriteBarrier(&____dataControlPrefabs_0, value);
	}

	inline static int32_t get_offset_of__actionControlPrefab_1() { return static_cast<int32_t>(offsetof(OptionControlFactory_t897643130_StaticFields, ____actionControlPrefab_1)); }
	inline ActionControl_t3457065707 * get__actionControlPrefab_1() const { return ____actionControlPrefab_1; }
	inline ActionControl_t3457065707 ** get_address_of__actionControlPrefab_1() { return &____actionControlPrefab_1; }
	inline void set__actionControlPrefab_1(ActionControl_t3457065707 * value)
	{
		____actionControlPrefab_1 = value;
		Il2CppCodeGenWriteBarrier(&____actionControlPrefab_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
