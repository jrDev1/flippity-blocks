﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// SRF.UI.LongPressButton
struct LongPressButton_t1525827641;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRDebugger.UI.Controls.MultiTapButton
struct MultiTapButton_t3820879164;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.TriggerRoot
struct  TriggerRoot_t735737282  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.Canvas SRDebugger.UI.Other.TriggerRoot::Canvas
	Canvas_t209405766 * ___Canvas_9;
	// SRF.UI.LongPressButton SRDebugger.UI.Other.TriggerRoot::TapHoldButton
	LongPressButton_t1525827641 * ___TapHoldButton_10;
	// UnityEngine.RectTransform SRDebugger.UI.Other.TriggerRoot::TriggerTransform
	RectTransform_t3349966182 * ___TriggerTransform_11;
	// SRDebugger.UI.Controls.MultiTapButton SRDebugger.UI.Other.TriggerRoot::TripleTapButton
	MultiTapButton_t3820879164 * ___TripleTapButton_12;

public:
	inline static int32_t get_offset_of_Canvas_9() { return static_cast<int32_t>(offsetof(TriggerRoot_t735737282, ___Canvas_9)); }
	inline Canvas_t209405766 * get_Canvas_9() const { return ___Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_Canvas_9() { return &___Canvas_9; }
	inline void set_Canvas_9(Canvas_t209405766 * value)
	{
		___Canvas_9 = value;
		Il2CppCodeGenWriteBarrier(&___Canvas_9, value);
	}

	inline static int32_t get_offset_of_TapHoldButton_10() { return static_cast<int32_t>(offsetof(TriggerRoot_t735737282, ___TapHoldButton_10)); }
	inline LongPressButton_t1525827641 * get_TapHoldButton_10() const { return ___TapHoldButton_10; }
	inline LongPressButton_t1525827641 ** get_address_of_TapHoldButton_10() { return &___TapHoldButton_10; }
	inline void set_TapHoldButton_10(LongPressButton_t1525827641 * value)
	{
		___TapHoldButton_10 = value;
		Il2CppCodeGenWriteBarrier(&___TapHoldButton_10, value);
	}

	inline static int32_t get_offset_of_TriggerTransform_11() { return static_cast<int32_t>(offsetof(TriggerRoot_t735737282, ___TriggerTransform_11)); }
	inline RectTransform_t3349966182 * get_TriggerTransform_11() const { return ___TriggerTransform_11; }
	inline RectTransform_t3349966182 ** get_address_of_TriggerTransform_11() { return &___TriggerTransform_11; }
	inline void set_TriggerTransform_11(RectTransform_t3349966182 * value)
	{
		___TriggerTransform_11 = value;
		Il2CppCodeGenWriteBarrier(&___TriggerTransform_11, value);
	}

	inline static int32_t get_offset_of_TripleTapButton_12() { return static_cast<int32_t>(offsetof(TriggerRoot_t735737282, ___TripleTapButton_12)); }
	inline MultiTapButton_t3820879164 * get_TripleTapButton_12() const { return ___TripleTapButton_12; }
	inline MultiTapButton_t3820879164 ** get_address_of_TripleTapButton_12() { return &___TripleTapButton_12; }
	inline void set_TripleTapButton_12(MultiTapButton_t3820879164 * value)
	{
		___TripleTapButton_12 = value;
		Il2CppCodeGenWriteBarrier(&___TripleTapButton_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
