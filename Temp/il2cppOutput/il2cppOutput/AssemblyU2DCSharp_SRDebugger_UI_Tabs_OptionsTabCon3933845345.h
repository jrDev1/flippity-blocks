﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance
struct CategoryInstance_t2626261001;
// SRDebugger.UI.Tabs.OptionsTabController
struct OptionsTabController_t2739246795;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.OptionsTabController/<CreateCategory>c__AnonStorey0
struct  U3CCreateCategoryU3Ec__AnonStorey0_t3933845345  : public Il2CppObject
{
public:
	// SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance SRDebugger.UI.Tabs.OptionsTabController/<CreateCategory>c__AnonStorey0::categoryInstance
	CategoryInstance_t2626261001 * ___categoryInstance_0;
	// SRDebugger.UI.Tabs.OptionsTabController SRDebugger.UI.Tabs.OptionsTabController/<CreateCategory>c__AnonStorey0::$this
	OptionsTabController_t2739246795 * ___U24this_1;

public:
	inline static int32_t get_offset_of_categoryInstance_0() { return static_cast<int32_t>(offsetof(U3CCreateCategoryU3Ec__AnonStorey0_t3933845345, ___categoryInstance_0)); }
	inline CategoryInstance_t2626261001 * get_categoryInstance_0() const { return ___categoryInstance_0; }
	inline CategoryInstance_t2626261001 ** get_address_of_categoryInstance_0() { return &___categoryInstance_0; }
	inline void set_categoryInstance_0(CategoryInstance_t2626261001 * value)
	{
		___categoryInstance_0 = value;
		Il2CppCodeGenWriteBarrier(&___categoryInstance_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCreateCategoryU3Ec__AnonStorey0_t3933845345, ___U24this_1)); }
	inline OptionsTabController_t2739246795 * get_U24this_1() const { return ___U24this_1; }
	inline OptionsTabController_t2739246795 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(OptionsTabController_t2739246795 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
