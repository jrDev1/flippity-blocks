﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_ConsoleEntry3008967599.h"
#include "AssemblyU2DCSharp_SRDebugger_VisibilityChangedDele2318869097.h"
#include "AssemblyU2DCSharp_SRDebugger_ActionCompleteCallbac2151041428.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_PinEntryComp4175557387.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_ProfilerFram3569129024.h"
#include "AssemblyU2DCSharp_SRDebugger_InfoEntry2565792942.h"
#include "AssemblyU2DCSharp_SRDebugger_InfoEntry_U3CCreateU31271666224.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio501715199.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio956868698.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati2244406016.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati2487113893.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio106306942.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati2290374229.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati4146292305.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio113027528.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati3177763977.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio798579932.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati4043451869.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio586412289.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati1245924925.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati1562805833.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementati3828701795.h"
#include "AssemblyU2DCSharp_SRDebugger_Services_Implementatio340348724.h"
#include "AssemblyU2DCSharp_SRDebugger_DefaultTabs4204632053.h"
#include "AssemblyU2DCSharp_SRDebugger_PinAlignment3550534810.h"
#include "AssemblyU2DCSharp_SRDebugger_ConsoleAlignment2755495440.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings1299540155.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_ShortcutActio299253119.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_TriggerBehavi698320438.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_TriggerEnabl3150100755.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_KeyboardShor2512121171.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ConsoleEn2517313264.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ConsoleLo2638108020.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ConsoleLogC94540193.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_Acti3457065707.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_Bool3658693475.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_DataBound3384854171.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_Enum2277533208.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_Numb1939034700.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_Numb1714395645.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_OptionsCo3483301704.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_ReadO537787855.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_Data_Stri1342716692.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_InfoBlock310368301.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_MultiTapB3820879164.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_PinEntryCo496076559.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_PinEntryC1988638876.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_PinEntryC1450473563.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_ProfilerFPSLabel3584209780.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerM2730562703.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerM3633601589.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerM2192668649.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerEn212840193.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerG2595259930.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerG3146482874.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_ProfilerG3712983251.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_SRTabButt2557777178.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_DebugPanelRoot2681537317.h"
#include "AssemblyU2DCSharp_SRDebugger_Scripts_DebuggerTabCo1358735436.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_MobileMenuControll2006392251.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_BugReportPop3609447367.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_BugReportShe1864182511.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_BugReportShee782864041.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_CategoryGrou4174071633.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_ConfigureCan1276933513.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_ConsoleTabQu3410852549.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_DebugPanelBa4136423076.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_DockConsoleC2913316086.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_HandleManager305543651.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_LoadingSpinn2001614914.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_PinnedUIRoot3479234366.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_SRTab1974039238.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_SRTabControl1363238198.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_SRTabControll986591490.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_ScrollRectPa2098116529.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_ScrollSettin2515531383.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_SetLayerFromS189878958.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_TriggerRoot735737282.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Other_VersionTextBe707946140.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_BugReportTabC3310766069.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_ConsoleTabCont999068710.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_InfoTabContro2308368305.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_OptionsTabCon2739246795.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_OptionsTabCon2626261001.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_OptionsTabCon3933845345.h"
#include "AssemblyU2DCSharp_SRDebugger_UI_Tabs_ProfilerTabCo2689247202.h"
#include "AssemblyU2DCSharp_SRDebugger_VersionInfo2179972510.h"
#include "AssemblyU2DCSharp_SRF_Json1296389786.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3301 = { sizeof (ConsoleEntry_t3008967599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3301[8] = 
{
	0,
	0,
	ConsoleEntry_t3008967599::get_offset_of__messagePreview_2(),
	ConsoleEntry_t3008967599::get_offset_of__stackTracePreview_3(),
	ConsoleEntry_t3008967599::get_offset_of_Count_4(),
	ConsoleEntry_t3008967599::get_offset_of_LogType_5(),
	ConsoleEntry_t3008967599::get_offset_of_Message_6(),
	ConsoleEntry_t3008967599::get_offset_of_StackTrace_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3302 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3303 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3304 = { sizeof (VisibilityChangedDelegate_t2318869097), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3305 = { sizeof (ActionCompleteCallback_t2151041428), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3306 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3307 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3308 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3309 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3310 = { sizeof (PinEntryCompleteCallback_t4175557387), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3311 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3313 = { sizeof (ProfilerFrame_t3569129024)+ sizeof (Il2CppObject), sizeof(ProfilerFrame_t3569129024 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3313[4] = 
{
	ProfilerFrame_t3569129024::get_offset_of_FrameTime_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfilerFrame_t3569129024::get_offset_of_OtherTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfilerFrame_t3569129024::get_offset_of_RenderTime_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfilerFrame_t3569129024::get_offset_of_UpdateTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3314 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3315 = { sizeof (InfoEntry_t2565792942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3315[3] = 
{
	InfoEntry_t2565792942::get_offset_of_U3CTitleU3Ek__BackingField_0(),
	InfoEntry_t2565792942::get_offset_of_U3CIsPrivateU3Ek__BackingField_1(),
	InfoEntry_t2565792942::get_offset_of__valueGetter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3316 = { sizeof (U3CCreateU3Ec__AnonStorey0_t1271666224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3316[1] = 
{
	U3CCreateU3Ec__AnonStorey0_t1271666224::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3318 = { sizeof (BugReportApiService_t501715199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3318[7] = 
{
	0,
	BugReportApiService_t501715199::get_offset_of__completeCallback_10(),
	BugReportApiService_t501715199::get_offset_of__errorMessage_11(),
	BugReportApiService_t501715199::get_offset_of__isBusy_12(),
	BugReportApiService_t501715199::get_offset_of__previousProgress_13(),
	BugReportApiService_t501715199::get_offset_of__progressCallback_14(),
	BugReportApiService_t501715199::get_offset_of__reportApi_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3319 = { sizeof (BugReportPopoverService_t956868698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3319[4] = 
{
	BugReportPopoverService_t956868698::get_offset_of__callback_9(),
	BugReportPopoverService_t956868698::get_offset_of__isVisible_10(),
	BugReportPopoverService_t956868698::get_offset_of__popover_11(),
	BugReportPopoverService_t956868698::get_offset_of__sheet_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3320 = { sizeof (U3COpenCoU3Ec__Iterator0_t2244406016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3320[6] = 
{
	U3COpenCoU3Ec__Iterator0_t2244406016::get_offset_of_takeScreenshot_0(),
	U3COpenCoU3Ec__Iterator0_t2244406016::get_offset_of_descriptionText_1(),
	U3COpenCoU3Ec__Iterator0_t2244406016::get_offset_of_U24this_2(),
	U3COpenCoU3Ec__Iterator0_t2244406016::get_offset_of_U24current_3(),
	U3COpenCoU3Ec__Iterator0_t2244406016::get_offset_of_U24disposing_4(),
	U3COpenCoU3Ec__Iterator0_t2244406016::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3321 = { sizeof (DebugCameraServiceImpl_t2487113893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3321[1] = 
{
	DebugCameraServiceImpl_t2487113893::get_offset_of__debugCamera_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3322 = { sizeof (DebugPanelServiceImpl_t106306942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3322[5] = 
{
	DebugPanelServiceImpl_t106306942::get_offset_of__debugPanelRootObject_2(),
	DebugPanelServiceImpl_t106306942::get_offset_of_VisibilityChanged_3(),
	DebugPanelServiceImpl_t106306942::get_offset_of__isVisible_4(),
	DebugPanelServiceImpl_t106306942::get_offset_of__cursorWasVisible_5(),
	DebugPanelServiceImpl_t106306942::get_offset_of__cursorLockMode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3323 = { sizeof (DebugTriggerImpl_t2290374229), -1, sizeof(DebugTriggerImpl_t2290374229_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3323[4] = 
{
	DebugTriggerImpl_t2290374229::get_offset_of__position_9(),
	DebugTriggerImpl_t2290374229::get_offset_of__trigger_10(),
	DebugTriggerImpl_t2290374229_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_11(),
	DebugTriggerImpl_t2290374229_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3324 = { sizeof (DockConsoleServiceImpl_t4146292305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3324[5] = 
{
	DockConsoleServiceImpl_t4146292305::get_offset_of__alignment_0(),
	DockConsoleServiceImpl_t4146292305::get_offset_of__consoleRoot_1(),
	DockConsoleServiceImpl_t4146292305::get_offset_of__didSuspendTrigger_2(),
	DockConsoleServiceImpl_t4146292305::get_offset_of__isExpanded_3(),
	DockConsoleServiceImpl_t4146292305::get_offset_of__isVisible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3325 = { sizeof (KeyboardShortcutListenerService_t113027528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3325[1] = 
{
	KeyboardShortcutListenerService_t113027528::get_offset_of__shortcuts_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3326 = { sizeof (OptionsServiceImpl_t3177763977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3326[5] = 
{
	OptionsServiceImpl_t3177763977::get_offset_of_OptionsUpdated_0(),
	OptionsServiceImpl_t3177763977::get_offset_of_OptionsValueUpdated_1(),
	OptionsServiceImpl_t3177763977::get_offset_of__optionContainerLookup_2(),
	OptionsServiceImpl_t3177763977::get_offset_of__options_3(),
	OptionsServiceImpl_t3177763977::get_offset_of__optionsReadonly_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3327 = { sizeof (PinEntryServiceImpl_t798579932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3327[4] = 
{
	PinEntryServiceImpl_t798579932::get_offset_of__callback_9(),
	PinEntryServiceImpl_t798579932::get_offset_of__isVisible_10(),
	PinEntryServiceImpl_t798579932::get_offset_of__pinControl_11(),
	PinEntryServiceImpl_t798579932::get_offset_of__requiredPin_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3328 = { sizeof (PinnedUIServiceImpl_t4043451869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3328[5] = 
{
	PinnedUIServiceImpl_t4043451869::get_offset_of__controlList_9(),
	PinnedUIServiceImpl_t4043451869::get_offset_of__pinnedObjects_10(),
	PinnedUIServiceImpl_t4043451869::get_offset_of__queueRefresh_11(),
	PinnedUIServiceImpl_t4043451869::get_offset_of__uiRoot_12(),
	PinnedUIServiceImpl_t4043451869::get_offset_of_OptionPinStateChanged_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3329 = { sizeof (SRDebugService_t586412289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3329[10] = 
{
	SRDebugService_t586412289::get_offset_of__debugPanelService_0(),
	SRDebugService_t586412289::get_offset_of__debugTrigger_1(),
	SRDebugService_t586412289::get_offset_of__informationService_2(),
	SRDebugService_t586412289::get_offset_of__optionsService_3(),
	SRDebugService_t586412289::get_offset_of__pinnedUiService_4(),
	SRDebugService_t586412289::get_offset_of__entryCodeEnabled_5(),
	SRDebugService_t586412289::get_offset_of__hasAuthorised_6(),
	SRDebugService_t586412289::get_offset_of__queuedTab_7(),
	SRDebugService_t586412289::get_offset_of__worldSpaceTransform_8(),
	SRDebugService_t586412289::get_offset_of_PanelVisibilityChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3330 = { sizeof (U3CShowBugReportSheetU3Ec__AnonStorey0_t1245924925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3330[1] = 
{
	U3CShowBugReportSheetU3Ec__AnonStorey0_t1245924925::get_offset_of_onComplete_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3331 = { sizeof (StandardConsoleService_t1562805833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3331[9] = 
{
	StandardConsoleService_t1562805833::get_offset_of__collapseEnabled_0(),
	StandardConsoleService_t1562805833::get_offset_of__hasCleared_1(),
	StandardConsoleService_t1562805833::get_offset_of__allConsoleEntries_2(),
	StandardConsoleService_t1562805833::get_offset_of__consoleEntries_3(),
	StandardConsoleService_t1562805833::get_offset_of__threadLock_4(),
	StandardConsoleService_t1562805833::get_offset_of_U3CErrorCountU3Ek__BackingField_5(),
	StandardConsoleService_t1562805833::get_offset_of_U3CWarningCountU3Ek__BackingField_6(),
	StandardConsoleService_t1562805833::get_offset_of_U3CInfoCountU3Ek__BackingField_7(),
	StandardConsoleService_t1562805833::get_offset_of_Updated_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3332 = { sizeof (StandardSystemInformationService_t3828701795), -1, sizeof(StandardSystemInformationService_t3828701795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3332[14] = 
{
	StandardSystemInformationService_t3828701795::get_offset_of__info_0(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_3(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_4(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_5(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_8(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_9(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_10(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_11(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_12(),
	StandardSystemInformationService_t3828701795_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3333 = { sizeof (U3CAddU3Ec__AnonStorey0_t340348724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3333[1] = 
{
	U3CAddU3Ec__AnonStorey0_t340348724::get_offset_of_info_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3334 = { sizeof (DefaultTabs_t4204632053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3334[6] = 
{
	DefaultTabs_t4204632053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3335 = { sizeof (PinAlignment_t3550534810)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3335[9] = 
{
	PinAlignment_t3550534810::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3336 = { sizeof (ConsoleAlignment_t2755495440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3336[3] = 
{
	ConsoleAlignment_t2755495440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3337 = { sizeof (Settings_t1299540155), -1, sizeof(Settings_t1299540155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3337[35] = 
{
	0,
	0,
	Settings_t1299540155_StaticFields::get_offset_of__instance_4(),
	Settings_t1299540155::get_offset_of__isEnabled_5(),
	Settings_t1299540155::get_offset_of__autoLoad_6(),
	Settings_t1299540155::get_offset_of__defaultTab_7(),
	Settings_t1299540155::get_offset_of__triggerEnableMode_8(),
	Settings_t1299540155::get_offset_of__triggerBehaviour_9(),
	Settings_t1299540155::get_offset_of__enableKeyboardShortcuts_10(),
	Settings_t1299540155::get_offset_of__keyboardShortcuts_11(),
	Settings_t1299540155::get_offset_of__newKeyboardShortcuts_12(),
	Settings_t1299540155::get_offset_of__keyboardModifierControl_13(),
	Settings_t1299540155::get_offset_of__keyboardModifierAlt_14(),
	Settings_t1299540155::get_offset_of__keyboardModifierShift_15(),
	Settings_t1299540155::get_offset_of__keyboardEscapeClose_16(),
	Settings_t1299540155::get_offset_of__enableBackgroundTransparency_17(),
	Settings_t1299540155::get_offset_of__collapseDuplicateLogEntries_18(),
	Settings_t1299540155::get_offset_of__richTextInConsole_19(),
	Settings_t1299540155::get_offset_of__requireEntryCode_20(),
	Settings_t1299540155::get_offset_of__requireEntryCodeEveryTime_21(),
	Settings_t1299540155::get_offset_of__entryCode_22(),
	Settings_t1299540155::get_offset_of__useDebugCamera_23(),
	Settings_t1299540155::get_offset_of__debugLayer_24(),
	Settings_t1299540155::get_offset_of__debugCameraDepth_25(),
	Settings_t1299540155::get_offset_of__apiKey_26(),
	Settings_t1299540155::get_offset_of__enableBugReporter_27(),
	Settings_t1299540155::get_offset_of__disabledTabs_28(),
	Settings_t1299540155::get_offset_of__profilerAlignment_29(),
	Settings_t1299540155::get_offset_of__optionsAlignment_30(),
	Settings_t1299540155::get_offset_of__consoleAlignment_31(),
	Settings_t1299540155::get_offset_of__triggerPosition_32(),
	Settings_t1299540155::get_offset_of__maximumConsoleEntries_33(),
	Settings_t1299540155::get_offset_of__enableEventSystemCreation_34(),
	Settings_t1299540155::get_offset_of__automaticShowCursor_35(),
	Settings_t1299540155_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3338 = { sizeof (ShortcutActions_t299253119)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3338[13] = 
{
	ShortcutActions_t299253119::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3339 = { sizeof (TriggerBehaviours_t698320438)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3339[4] = 
{
	TriggerBehaviours_t698320438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3340 = { sizeof (TriggerEnableModes_t3150100755)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3340[5] = 
{
	TriggerEnableModes_t3150100755::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3341 = { sizeof (KeyboardShortcut_t2512121171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3341[5] = 
{
	KeyboardShortcut_t2512121171::get_offset_of_Action_0(),
	KeyboardShortcut_t2512121171::get_offset_of_Alt_1(),
	KeyboardShortcut_t2512121171::get_offset_of_Control_2(),
	KeyboardShortcut_t2512121171::get_offset_of_Key_3(),
	KeyboardShortcut_t2512121171::get_offset_of_Shift_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3342 = { sizeof (ConsoleEntryView_t2517313264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3342[12] = 
{
	0,
	0,
	0,
	ConsoleEntryView_t2517313264::get_offset_of__count_12(),
	ConsoleEntryView_t2517313264::get_offset_of__hasCount_13(),
	ConsoleEntryView_t2517313264::get_offset_of__prevData_14(),
	ConsoleEntryView_t2517313264::get_offset_of__rectTransform_15(),
	ConsoleEntryView_t2517313264::get_offset_of_Count_16(),
	ConsoleEntryView_t2517313264::get_offset_of_CountContainer_17(),
	ConsoleEntryView_t2517313264::get_offset_of_ImageStyle_18(),
	ConsoleEntryView_t2517313264::get_offset_of_Message_19(),
	ConsoleEntryView_t2517313264::get_offset_of_StackTrace_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3343 = { sizeof (ConsoleLogControl_t2638108020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3343[9] = 
{
	ConsoleLogControl_t2638108020::get_offset_of__consoleScrollLayoutGroup_9(),
	ConsoleLogControl_t2638108020::get_offset_of__consoleScrollRect_10(),
	ConsoleLogControl_t2638108020::get_offset_of__isDirty_11(),
	ConsoleLogControl_t2638108020::get_offset_of__scrollPosition_12(),
	ConsoleLogControl_t2638108020::get_offset_of__showErrors_13(),
	ConsoleLogControl_t2638108020::get_offset_of__showInfo_14(),
	ConsoleLogControl_t2638108020::get_offset_of__showWarnings_15(),
	ConsoleLogControl_t2638108020::get_offset_of_SelectedItemChanged_16(),
	ConsoleLogControl_t2638108020::get_offset_of__filter_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3344 = { sizeof (U3CScrollToBottomU3Ec__Iterator0_t94540193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3344[4] = 
{
	U3CScrollToBottomU3Ec__Iterator0_t94540193::get_offset_of_U24this_0(),
	U3CScrollToBottomU3Ec__Iterator0_t94540193::get_offset_of_U24current_1(),
	U3CScrollToBottomU3Ec__Iterator0_t94540193::get_offset_of_U24disposing_2(),
	U3CScrollToBottomU3Ec__Iterator0_t94540193::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3345 = { sizeof (ActionControl_t3457065707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3345[3] = 
{
	ActionControl_t3457065707::get_offset_of__method_12(),
	ActionControl_t3457065707::get_offset_of_Button_13(),
	ActionControl_t3457065707::get_offset_of_Title_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3346 = { sizeof (BoolControl_t3658693475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3346[2] = 
{
	BoolControl_t3658693475::get_offset_of_Title_17(),
	BoolControl_t3658693475::get_offset_of_Toggle_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3347 = { sizeof (DataBoundControl_t3384854171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3347[5] = 
{
	DataBoundControl_t3384854171::get_offset_of__hasStarted_12(),
	DataBoundControl_t3384854171::get_offset_of__isReadOnly_13(),
	DataBoundControl_t3384854171::get_offset_of__prevValue_14(),
	DataBoundControl_t3384854171::get_offset_of__prop_15(),
	DataBoundControl_t3384854171::get_offset_of_U3CPropertyNameU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3348 = { sizeof (EnumControl_t2277533208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3348[8] = 
{
	EnumControl_t2277533208::get_offset_of__lastValue_17(),
	EnumControl_t2277533208::get_offset_of__names_18(),
	EnumControl_t2277533208::get_offset_of__values_19(),
	EnumControl_t2277533208::get_offset_of_ContentLayoutElement_20(),
	EnumControl_t2277533208::get_offset_of_DisableOnReadOnly_21(),
	EnumControl_t2277533208::get_offset_of_Spinner_22(),
	EnumControl_t2277533208::get_offset_of_Title_23(),
	EnumControl_t2277533208::get_offset_of_Value_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3349 = { sizeof (NumberControl_t1939034700), -1, sizeof(NumberControl_t1939034700_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3349[10] = 
{
	NumberControl_t1939034700_StaticFields::get_offset_of_IntegerTypes_17(),
	NumberControl_t1939034700_StaticFields::get_offset_of_DecimalTypes_18(),
	NumberControl_t1939034700_StaticFields::get_offset_of_ValueRanges_19(),
	NumberControl_t1939034700::get_offset_of__lastValue_20(),
	NumberControl_t1939034700::get_offset_of__type_21(),
	NumberControl_t1939034700::get_offset_of_DisableOnReadOnly_22(),
	NumberControl_t1939034700::get_offset_of_DownNumberButton_23(),
	NumberControl_t1939034700::get_offset_of_NumberSpinner_24(),
	NumberControl_t1939034700::get_offset_of_Title_25(),
	NumberControl_t1939034700::get_offset_of_UpNumberButton_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3350 = { sizeof (ValueRange_t1714395645)+ sizeof (Il2CppObject), sizeof(ValueRange_t1714395645 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3350[2] = 
{
	ValueRange_t1714395645::get_offset_of_MaxValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ValueRange_t1714395645::get_offset_of_MinValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3351 = { sizeof (OptionsControlBase_t3483301704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3351[3] = 
{
	OptionsControlBase_t3483301704::get_offset_of__selectionModeEnabled_9(),
	OptionsControlBase_t3483301704::get_offset_of_SelectionModeToggle_10(),
	OptionsControlBase_t3483301704::get_offset_of_Option_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3352 = { sizeof (ReadOnlyControl_t537787855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3352[2] = 
{
	ReadOnlyControl_t537787855::get_offset_of_ValueText_17(),
	ReadOnlyControl_t537787855::get_offset_of_Title_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3353 = { sizeof (StringControl_t1342716692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3353[2] = 
{
	StringControl_t1342716692::get_offset_of_InputField_17(),
	StringControl_t1342716692::get_offset_of_Title_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3354 = { sizeof (InfoBlock_t310368301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3354[2] = 
{
	InfoBlock_t310368301::get_offset_of_Content_9(),
	InfoBlock_t310368301::get_offset_of_Title_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3355 = { sizeof (MultiTapButton_t3820879164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3355[4] = 
{
	MultiTapButton_t3820879164::get_offset_of__lastTap_17(),
	MultiTapButton_t3820879164::get_offset_of__tapCount_18(),
	MultiTapButton_t3820879164::get_offset_of_RequiredTapCount_19(),
	MultiTapButton_t3820879164::get_offset_of_ResetTime_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3356 = { sizeof (PinEntryControlCallback_t496076559), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3357 = { sizeof (PinEntryControl_t1988638876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3357[12] = 
{
	PinEntryControl_t1988638876::get_offset_of__isVisible_9(),
	PinEntryControl_t1988638876::get_offset_of__numbers_10(),
	PinEntryControl_t1988638876::get_offset_of_Background_11(),
	PinEntryControl_t1988638876::get_offset_of_CanCancel_12(),
	PinEntryControl_t1988638876::get_offset_of_CancelButton_13(),
	PinEntryControl_t1988638876::get_offset_of_CancelButtonText_14(),
	PinEntryControl_t1988638876::get_offset_of_CanvasGroup_15(),
	PinEntryControl_t1988638876::get_offset_of_DotAnimator_16(),
	PinEntryControl_t1988638876::get_offset_of_NumberButtons_17(),
	PinEntryControl_t1988638876::get_offset_of_NumberDots_18(),
	PinEntryControl_t1988638876::get_offset_of_PromptText_19(),
	PinEntryControl_t1988638876::get_offset_of_Complete_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3358 = { sizeof (U3CAwakeU3Ec__AnonStorey0_t1450473563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3358[2] = 
{
	U3CAwakeU3Ec__AnonStorey0_t1450473563::get_offset_of_number_0(),
	U3CAwakeU3Ec__AnonStorey0_t1450473563::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3359 = { sizeof (ProfilerFPSLabel_t3584209780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3359[4] = 
{
	ProfilerFPSLabel_t3584209780::get_offset_of__nextUpdate_9(),
	ProfilerFPSLabel_t3584209780::get_offset_of__profilerService_10(),
	ProfilerFPSLabel_t3584209780::get_offset_of_UpdateFrequency_11(),
	ProfilerFPSLabel_t3584209780::get_offset_of__text_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3360 = { sizeof (ProfilerMemoryBlock_t2730562703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3360[4] = 
{
	ProfilerMemoryBlock_t2730562703::get_offset_of__lastRefresh_9(),
	ProfilerMemoryBlock_t2730562703::get_offset_of_CurrentUsedText_10(),
	ProfilerMemoryBlock_t2730562703::get_offset_of_Slider_11(),
	ProfilerMemoryBlock_t2730562703::get_offset_of_TotalAllocatedText_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3361 = { sizeof (U3CCleanUpU3Ec__Iterator0_t3633601589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3361[4] = 
{
	U3CCleanUpU3Ec__Iterator0_t3633601589::get_offset_of_U24this_0(),
	U3CCleanUpU3Ec__Iterator0_t3633601589::get_offset_of_U24current_1(),
	U3CCleanUpU3Ec__Iterator0_t3633601589::get_offset_of_U24disposing_2(),
	U3CCleanUpU3Ec__Iterator0_t3633601589::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3362 = { sizeof (ProfilerMonoBlock_t2192668649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3362[6] = 
{
	ProfilerMonoBlock_t2192668649::get_offset_of__lastRefresh_9(),
	ProfilerMonoBlock_t2192668649::get_offset_of_CurrentUsedText_10(),
	ProfilerMonoBlock_t2192668649::get_offset_of_NotSupportedMessage_11(),
	ProfilerMonoBlock_t2192668649::get_offset_of_Slider_12(),
	ProfilerMonoBlock_t2192668649::get_offset_of_TotalAllocatedText_13(),
	ProfilerMonoBlock_t2192668649::get_offset_of__isSupported_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3363 = { sizeof (ProfilerEnableControl_t212840193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3363[4] = 
{
	ProfilerEnableControl_t212840193::get_offset_of__previousState_9(),
	ProfilerEnableControl_t212840193::get_offset_of_ButtonText_10(),
	ProfilerEnableControl_t212840193::get_offset_of_EnableButton_11(),
	ProfilerEnableControl_t212840193::get_offset_of_Text_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3364 = { sizeof (ProfilerGraphAxisLabel_t2595259930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3364[4] = 
{
	ProfilerGraphAxisLabel_t2595259930::get_offset_of__prevFrameTime_9(),
	ProfilerGraphAxisLabel_t2595259930::get_offset_of__queuedFrameTime_10(),
	ProfilerGraphAxisLabel_t2595259930::get_offset_of__yPosition_11(),
	ProfilerGraphAxisLabel_t2595259930::get_offset_of_Text_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3365 = { sizeof (ProfilerGraphControl_t3146482874), -1, sizeof(ProfilerGraphControl_t3146482874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3365[19] = 
{
	ProfilerGraphControl_t3146482874::get_offset_of_VerticalAlignment_19(),
	ProfilerGraphControl_t3146482874_StaticFields::get_offset_of_ScaleSteps_20(),
	ProfilerGraphControl_t3146482874::get_offset_of_FloatingScale_21(),
	ProfilerGraphControl_t3146482874::get_offset_of_TargetFpsUseApplication_22(),
	ProfilerGraphControl_t3146482874::get_offset_of_DrawAxes_23(),
	ProfilerGraphControl_t3146482874::get_offset_of_TargetFps_24(),
	ProfilerGraphControl_t3146482874::get_offset_of_Clip_25(),
	0,
	0,
	0,
	ProfilerGraphControl_t3146482874::get_offset_of_VerticalPadding_29(),
	0,
	ProfilerGraphControl_t3146482874::get_offset_of_LineColours_31(),
	ProfilerGraphControl_t3146482874::get_offset_of__profilerService_32(),
	ProfilerGraphControl_t3146482874::get_offset_of__axisLabels_33(),
	ProfilerGraphControl_t3146482874::get_offset_of__clipBounds_34(),
	ProfilerGraphControl_t3146482874::get_offset_of__meshVertices_35(),
	ProfilerGraphControl_t3146482874::get_offset_of__meshVertexColors_36(),
	ProfilerGraphControl_t3146482874::get_offset_of__meshTriangles_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3366 = { sizeof (VerticalAlignments_t3712983251)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3366[3] = 
{
	VerticalAlignments_t3712983251::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3367 = { sizeof (SRTabButton_t2557777178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3367[5] = 
{
	SRTabButton_t2557777178::get_offset_of_ActiveToggle_9(),
	SRTabButton_t2557777178::get_offset_of_Button_10(),
	SRTabButton_t2557777178::get_offset_of_ExtraContentContainer_11(),
	SRTabButton_t2557777178::get_offset_of_IconStyleComponent_12(),
	SRTabButton_t2557777178::get_offset_of_TitleText_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3368 = { sizeof (DebugPanelRoot_t2681537317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3368[3] = 
{
	DebugPanelRoot_t2681537317::get_offset_of_Canvas_9(),
	DebugPanelRoot_t2681537317::get_offset_of_CanvasGroup_10(),
	DebugPanelRoot_t2681537317::get_offset_of_TabController_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3369 = { sizeof (DebuggerTabController_t1358735436), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3369[5] = 
{
	DebuggerTabController_t1358735436::get_offset_of__aboutTabInstance_9(),
	DebuggerTabController_t1358735436::get_offset_of__activeTab_10(),
	DebuggerTabController_t1358735436::get_offset_of__hasStarted_11(),
	DebuggerTabController_t1358735436::get_offset_of_AboutTab_12(),
	DebuggerTabController_t1358735436::get_offset_of_TabController_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3370 = { sizeof (MobileMenuController_t2006392251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3370[8] = 
{
	MobileMenuController_t2006392251::get_offset_of__closeButton_9(),
	MobileMenuController_t2006392251::get_offset_of__maxMenuWidth_10(),
	MobileMenuController_t2006392251::get_offset_of__peekAmount_11(),
	MobileMenuController_t2006392251::get_offset_of__targetXPosition_12(),
	MobileMenuController_t2006392251::get_offset_of_Content_13(),
	MobileMenuController_t2006392251::get_offset_of_Menu_14(),
	MobileMenuController_t2006392251::get_offset_of_OpenButton_15(),
	MobileMenuController_t2006392251::get_offset_of_TabController_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3371 = { sizeof (BugReportPopoverRoot_t3609447367), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3371[2] = 
{
	BugReportPopoverRoot_t3609447367::get_offset_of_CanvasGroup_9(),
	BugReportPopoverRoot_t3609447367::get_offset_of_Container_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3372 = { sizeof (BugReportSheetController_t1864182511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3372[12] = 
{
	BugReportSheetController_t1864182511::get_offset_of_ButtonContainer_9(),
	BugReportSheetController_t1864182511::get_offset_of_ButtonText_10(),
	BugReportSheetController_t1864182511::get_offset_of_CancelButton_11(),
	BugReportSheetController_t1864182511::get_offset_of_CancelPressed_12(),
	BugReportSheetController_t1864182511::get_offset_of_DescriptionField_13(),
	BugReportSheetController_t1864182511::get_offset_of_EmailField_14(),
	BugReportSheetController_t1864182511::get_offset_of_ProgressBar_15(),
	BugReportSheetController_t1864182511::get_offset_of_ResultMessageText_16(),
	BugReportSheetController_t1864182511::get_offset_of_ScreenshotComplete_17(),
	BugReportSheetController_t1864182511::get_offset_of_SubmitButton_18(),
	BugReportSheetController_t1864182511::get_offset_of_SubmitComplete_19(),
	BugReportSheetController_t1864182511::get_offset_of_TakingScreenshot_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3373 = { sizeof (U3CSubmitCoU3Ec__Iterator0_t782864041), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3373[6] = 
{
	U3CSubmitCoU3Ec__Iterator0_t782864041::get_offset_of_U3CsU3E__0_0(),
	U3CSubmitCoU3Ec__Iterator0_t782864041::get_offset_of_U3CrU3E__0_1(),
	U3CSubmitCoU3Ec__Iterator0_t782864041::get_offset_of_U24this_2(),
	U3CSubmitCoU3Ec__Iterator0_t782864041::get_offset_of_U24current_3(),
	U3CSubmitCoU3Ec__Iterator0_t782864041::get_offset_of_U24disposing_4(),
	U3CSubmitCoU3Ec__Iterator0_t782864041::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3374 = { sizeof (CategoryGroup_t4174071633), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3374[6] = 
{
	CategoryGroup_t4174071633::get_offset_of_Container_9(),
	CategoryGroup_t4174071633::get_offset_of_Header_10(),
	CategoryGroup_t4174071633::get_offset_of_Background_11(),
	CategoryGroup_t4174071633::get_offset_of_SelectionToggle_12(),
	CategoryGroup_t4174071633::get_offset_of_EnabledDuringSelectionMode_13(),
	CategoryGroup_t4174071633::get_offset_of__selectionModeEnabled_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3375 = { sizeof (ConfigureCanvasFromSettings_t1276933513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3376 = { sizeof (ConsoleTabQuickViewControl_t3410852549), -1, sizeof(ConsoleTabQuickViewControl_t3410852549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3376[9] = 
{
	0,
	ConsoleTabQuickViewControl_t3410852549_StaticFields::get_offset_of_MaxString_10(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of__prevErrorCount_11(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of__prevInfoCount_12(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of__prevWarningCount_13(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of_ConsoleService_14(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of_ErrorCountText_15(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of_InfoCountText_16(),
	ConsoleTabQuickViewControl_t3410852549::get_offset_of_WarningCountText_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3377 = { sizeof (DebugPanelBackgroundBehaviour_t4136423076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3377[4] = 
{
	DebugPanelBackgroundBehaviour_t4136423076::get_offset_of__defaultKey_8(),
	DebugPanelBackgroundBehaviour_t4136423076::get_offset_of__isTransparent_9(),
	DebugPanelBackgroundBehaviour_t4136423076::get_offset_of__styleComponent_10(),
	DebugPanelBackgroundBehaviour_t4136423076::get_offset_of_TransparentStyleKey_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3378 = { sizeof (DockConsoleController_t2913316086), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3378[17] = 
{
	0,
	DockConsoleController_t2913316086::get_offset_of__isDirty_10(),
	DockConsoleController_t2913316086::get_offset_of__isDragging_11(),
	DockConsoleController_t2913316086::get_offset_of__pointersOver_12(),
	DockConsoleController_t2913316086::get_offset_of_BottomHandle_13(),
	DockConsoleController_t2913316086::get_offset_of_CanvasGroup_14(),
	DockConsoleController_t2913316086::get_offset_of_Console_15(),
	DockConsoleController_t2913316086::get_offset_of_Dropdown_16(),
	DockConsoleController_t2913316086::get_offset_of_DropdownToggleSprite_17(),
	DockConsoleController_t2913316086::get_offset_of_TextErrors_18(),
	DockConsoleController_t2913316086::get_offset_of_TextInfo_19(),
	DockConsoleController_t2913316086::get_offset_of_TextWarnings_20(),
	DockConsoleController_t2913316086::get_offset_of_ToggleErrors_21(),
	DockConsoleController_t2913316086::get_offset_of_ToggleInfo_22(),
	DockConsoleController_t2913316086::get_offset_of_ToggleWarnings_23(),
	DockConsoleController_t2913316086::get_offset_of_TopBar_24(),
	DockConsoleController_t2913316086::get_offset_of_TopHandle_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3379 = { sizeof (HandleManager_t305543651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3379[10] = 
{
	HandleManager_t305543651::get_offset_of__hasSet_8(),
	HandleManager_t305543651::get_offset_of_BottomHandle_9(),
	HandleManager_t305543651::get_offset_of_BottomLeftHandle_10(),
	HandleManager_t305543651::get_offset_of_BottomRightHandle_11(),
	HandleManager_t305543651::get_offset_of_DefaultAlignment_12(),
	HandleManager_t305543651::get_offset_of_LeftHandle_13(),
	HandleManager_t305543651::get_offset_of_RightHandle_14(),
	HandleManager_t305543651::get_offset_of_TopHandle_15(),
	HandleManager_t305543651::get_offset_of_TopLeftHandle_16(),
	HandleManager_t305543651::get_offset_of_TopRightHandle_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3380 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3381 = { sizeof (LoadingSpinnerBehaviour_t2001614914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3381[3] = 
{
	LoadingSpinnerBehaviour_t2001614914::get_offset_of__dt_8(),
	LoadingSpinnerBehaviour_t2001614914::get_offset_of_FrameCount_9(),
	LoadingSpinnerBehaviour_t2001614914::get_offset_of_SpinDuration_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3382 = { sizeof (PinnedUIRoot_t3479234366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3382[8] = 
{
	PinnedUIRoot_t3479234366::get_offset_of_Canvas_9(),
	PinnedUIRoot_t3479234366::get_offset_of_Container_10(),
	PinnedUIRoot_t3479234366::get_offset_of_DockConsoleController_11(),
	PinnedUIRoot_t3479234366::get_offset_of_Options_12(),
	PinnedUIRoot_t3479234366::get_offset_of_OptionsLayoutGroup_13(),
	PinnedUIRoot_t3479234366::get_offset_of_Profiler_14(),
	PinnedUIRoot_t3479234366::get_offset_of_ProfilerHandleManager_15(),
	PinnedUIRoot_t3479234366::get_offset_of_ProfilerVerticalLayoutGroup_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3383 = { sizeof (SRTab_t1974039238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3383[9] = 
{
	SRTab_t1974039238::get_offset_of_HeaderExtraContent_9(),
	SRTab_t1974039238::get_offset_of_Icon_10(),
	SRTab_t1974039238::get_offset_of_IconExtraContent_11(),
	SRTab_t1974039238::get_offset_of_IconStyleKey_12(),
	SRTab_t1974039238::get_offset_of_SortIndex_13(),
	SRTab_t1974039238::get_offset_of_TabButton_14(),
	SRTab_t1974039238::get_offset_of__title_15(),
	SRTab_t1974039238::get_offset_of__longTitle_16(),
	SRTab_t1974039238::get_offset_of__key_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3384 = { sizeof (SRTabController_t1363238198), -1, sizeof(SRTabController_t1363238198_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3384[9] = 
{
	SRTabController_t1363238198::get_offset_of__tabs_9(),
	SRTabController_t1363238198::get_offset_of__activeTab_10(),
	SRTabController_t1363238198::get_offset_of_TabButtonContainer_11(),
	SRTabController_t1363238198::get_offset_of_TabButtonPrefab_12(),
	SRTabController_t1363238198::get_offset_of_TabContentsContainer_13(),
	SRTabController_t1363238198::get_offset_of_TabHeaderContentContainer_14(),
	SRTabController_t1363238198::get_offset_of_TabHeaderText_15(),
	SRTabController_t1363238198::get_offset_of_ActiveTabChanged_16(),
	SRTabController_t1363238198_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3385 = { sizeof (U3CAddTabU3Ec__AnonStorey0_t986591490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3385[2] = 
{
	U3CAddTabU3Ec__AnonStorey0_t986591490::get_offset_of_tab_0(),
	U3CAddTabU3Ec__AnonStorey0_t986591490::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3386 = { sizeof (ScrollRectPatch_t2098116529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3386[3] = 
{
	ScrollRectPatch_t2098116529::get_offset_of_Content_2(),
	ScrollRectPatch_t2098116529::get_offset_of_ReplaceMask_3(),
	ScrollRectPatch_t2098116529::get_offset_of_Viewport_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3387 = { sizeof (ScrollSettingsBehaviour_t2515531383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3387[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3388 = { sizeof (SetLayerFromSettings_t189878958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3389 = { sizeof (TriggerRoot_t735737282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3389[4] = 
{
	TriggerRoot_t735737282::get_offset_of_Canvas_9(),
	TriggerRoot_t735737282::get_offset_of_TapHoldButton_10(),
	TriggerRoot_t735737282::get_offset_of_TriggerTransform_11(),
	TriggerRoot_t735737282::get_offset_of_TripleTapButton_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3390 = { sizeof (VersionTextBehaviour_t707946140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3390[2] = 
{
	VersionTextBehaviour_t707946140::get_offset_of_Format_9(),
	VersionTextBehaviour_t707946140::get_offset_of_Text_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3391 = { sizeof (BugReportTabController_t3310766069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3391[2] = 
{
	BugReportTabController_t3310766069::get_offset_of_BugReportSheetPrefab_9(),
	BugReportTabController_t3310766069::get_offset_of_Container_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3392 = { sizeof (ConsoleTabController_t999068710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3392[16] = 
{
	0,
	ConsoleTabController_t999068710::get_offset_of__consoleCanvas_10(),
	ConsoleTabController_t999068710::get_offset_of__isDirty_11(),
	ConsoleTabController_t999068710::get_offset_of_ConsoleLogControl_12(),
	ConsoleTabController_t999068710::get_offset_of_PinToggle_13(),
	ConsoleTabController_t999068710::get_offset_of_StackTraceScrollRect_14(),
	ConsoleTabController_t999068710::get_offset_of_StackTraceText_15(),
	ConsoleTabController_t999068710::get_offset_of_ToggleErrors_16(),
	ConsoleTabController_t999068710::get_offset_of_ToggleErrorsText_17(),
	ConsoleTabController_t999068710::get_offset_of_ToggleInfo_18(),
	ConsoleTabController_t999068710::get_offset_of_ToggleInfoText_19(),
	ConsoleTabController_t999068710::get_offset_of_ToggleWarnings_20(),
	ConsoleTabController_t999068710::get_offset_of_ToggleWarningsText_21(),
	ConsoleTabController_t999068710::get_offset_of_FilterToggle_22(),
	ConsoleTabController_t999068710::get_offset_of_FilterField_23(),
	ConsoleTabController_t999068710::get_offset_of_FilterBarContainer_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3393 = { sizeof (InfoTabController_t2308368305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3393[6] = 
{
	0,
	0,
	0,
	InfoTabController_t2308368305::get_offset_of__infoBlocks_12(),
	InfoTabController_t2308368305::get_offset_of_InfoBlockPrefab_13(),
	InfoTabController_t2308368305::get_offset_of_LayoutContainer_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3394 = { sizeof (OptionsTabController_t2739246795), -1, sizeof(OptionsTabController_t2739246795_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3394[15] = 
{
	OptionsTabController_t2739246795::get_offset_of__controls_9(),
	OptionsTabController_t2739246795::get_offset_of__categories_10(),
	OptionsTabController_t2739246795::get_offset_of__options_11(),
	OptionsTabController_t2739246795::get_offset_of__queueRefresh_12(),
	OptionsTabController_t2739246795::get_offset_of__selectionModeEnabled_13(),
	OptionsTabController_t2739246795::get_offset_of__optionCanvas_14(),
	OptionsTabController_t2739246795::get_offset_of_ActionControlPrefab_15(),
	OptionsTabController_t2739246795::get_offset_of_CategoryGroupPrefab_16(),
	OptionsTabController_t2739246795::get_offset_of_ContentContainer_17(),
	OptionsTabController_t2739246795::get_offset_of_NoOptionsNotice_18(),
	OptionsTabController_t2739246795::get_offset_of_PinButton_19(),
	OptionsTabController_t2739246795::get_offset_of_PinPromptSpacer_20(),
	OptionsTabController_t2739246795::get_offset_of_PinPromptText_21(),
	OptionsTabController_t2739246795::get_offset_of__isTogglingCategory_22(),
	OptionsTabController_t2739246795_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3395 = { sizeof (CategoryInstance_t2626261001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3395[2] = 
{
	CategoryInstance_t2626261001::get_offset_of_U3CCategoryGroupU3Ek__BackingField_0(),
	CategoryInstance_t2626261001::get_offset_of_Options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3396 = { sizeof (U3CCreateCategoryU3Ec__AnonStorey0_t3933845345), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3396[2] = 
{
	U3CCreateCategoryU3Ec__AnonStorey0_t3933845345::get_offset_of_categoryInstance_0(),
	U3CCreateCategoryU3Ec__AnonStorey0_t3933845345::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3397 = { sizeof (ProfilerTabController_t2689247202), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3397[2] = 
{
	ProfilerTabController_t2689247202::get_offset_of__isDirty_9(),
	ProfilerTabController_t2689247202::get_offset_of_PinToggle_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3398 = { sizeof (VersionInfo_t2179972510), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3398[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3399 = { sizeof (Json_t1296389786), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
