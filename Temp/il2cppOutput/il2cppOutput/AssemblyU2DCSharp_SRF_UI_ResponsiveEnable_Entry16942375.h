﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveEnable_Modes1535981351.h"

// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t4084040270;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ResponsiveEnable/Entry
struct  Entry_t16942375 
{
public:
	// UnityEngine.Behaviour[] SRF.UI.ResponsiveEnable/Entry::Components
	BehaviourU5BU5D_t4084040270* ___Components_0;
	// UnityEngine.GameObject[] SRF.UI.ResponsiveEnable/Entry::GameObjects
	GameObjectU5BU5D_t3057952154* ___GameObjects_1;
	// SRF.UI.ResponsiveEnable/Modes SRF.UI.ResponsiveEnable/Entry::Mode
	int32_t ___Mode_2;
	// System.Single SRF.UI.ResponsiveEnable/Entry::ThresholdHeight
	float ___ThresholdHeight_3;
	// System.Single SRF.UI.ResponsiveEnable/Entry::ThresholdWidth
	float ___ThresholdWidth_4;

public:
	inline static int32_t get_offset_of_Components_0() { return static_cast<int32_t>(offsetof(Entry_t16942375, ___Components_0)); }
	inline BehaviourU5BU5D_t4084040270* get_Components_0() const { return ___Components_0; }
	inline BehaviourU5BU5D_t4084040270** get_address_of_Components_0() { return &___Components_0; }
	inline void set_Components_0(BehaviourU5BU5D_t4084040270* value)
	{
		___Components_0 = value;
		Il2CppCodeGenWriteBarrier(&___Components_0, value);
	}

	inline static int32_t get_offset_of_GameObjects_1() { return static_cast<int32_t>(offsetof(Entry_t16942375, ___GameObjects_1)); }
	inline GameObjectU5BU5D_t3057952154* get_GameObjects_1() const { return ___GameObjects_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_GameObjects_1() { return &___GameObjects_1; }
	inline void set_GameObjects_1(GameObjectU5BU5D_t3057952154* value)
	{
		___GameObjects_1 = value;
		Il2CppCodeGenWriteBarrier(&___GameObjects_1, value);
	}

	inline static int32_t get_offset_of_Mode_2() { return static_cast<int32_t>(offsetof(Entry_t16942375, ___Mode_2)); }
	inline int32_t get_Mode_2() const { return ___Mode_2; }
	inline int32_t* get_address_of_Mode_2() { return &___Mode_2; }
	inline void set_Mode_2(int32_t value)
	{
		___Mode_2 = value;
	}

	inline static int32_t get_offset_of_ThresholdHeight_3() { return static_cast<int32_t>(offsetof(Entry_t16942375, ___ThresholdHeight_3)); }
	inline float get_ThresholdHeight_3() const { return ___ThresholdHeight_3; }
	inline float* get_address_of_ThresholdHeight_3() { return &___ThresholdHeight_3; }
	inline void set_ThresholdHeight_3(float value)
	{
		___ThresholdHeight_3 = value;
	}

	inline static int32_t get_offset_of_ThresholdWidth_4() { return static_cast<int32_t>(offsetof(Entry_t16942375, ___ThresholdWidth_4)); }
	inline float get_ThresholdWidth_4() const { return ___ThresholdWidth_4; }
	inline float* get_address_of_ThresholdWidth_4() { return &___ThresholdWidth_4; }
	inline void set_ThresholdWidth_4(float value)
	{
		___ThresholdWidth_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SRF.UI.ResponsiveEnable/Entry
struct Entry_t16942375_marshaled_pinvoke
{
	BehaviourU5BU5D_t4084040270* ___Components_0;
	GameObjectU5BU5D_t3057952154* ___GameObjects_1;
	int32_t ___Mode_2;
	float ___ThresholdHeight_3;
	float ___ThresholdWidth_4;
};
// Native definition for COM marshalling of SRF.UI.ResponsiveEnable/Entry
struct Entry_t16942375_marshaled_com
{
	BehaviourU5BU5D_t4084040270* ___Components_0;
	GameObjectU5BU5D_t3057952154* ___GameObjects_1;
	int32_t ___Mode_2;
	float ___ThresholdHeight_3;
	float ___ThresholdWidth_4;
};
