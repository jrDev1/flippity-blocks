﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRF.Service.ServiceConstructorAttribute
struct ServiceConstructorAttribute_t57773005;
// System.Reflection.MethodInfo
struct MethodInfo_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRServiceManager/<ScanTypeForConstructors>c__AnonStorey1
struct  U3CScanTypeForConstructorsU3Ec__AnonStorey1_t3510309839  : public Il2CppObject
{
public:
	// SRF.Service.ServiceConstructorAttribute SRF.Service.SRServiceManager/<ScanTypeForConstructors>c__AnonStorey1::attrib
	ServiceConstructorAttribute_t57773005 * ___attrib_0;
	// System.Reflection.MethodInfo SRF.Service.SRServiceManager/<ScanTypeForConstructors>c__AnonStorey1::m
	MethodInfo_t * ___m_1;

public:
	inline static int32_t get_offset_of_attrib_0() { return static_cast<int32_t>(offsetof(U3CScanTypeForConstructorsU3Ec__AnonStorey1_t3510309839, ___attrib_0)); }
	inline ServiceConstructorAttribute_t57773005 * get_attrib_0() const { return ___attrib_0; }
	inline ServiceConstructorAttribute_t57773005 ** get_address_of_attrib_0() { return &___attrib_0; }
	inline void set_attrib_0(ServiceConstructorAttribute_t57773005 * value)
	{
		___attrib_0 = value;
		Il2CppCodeGenWriteBarrier(&___attrib_0, value);
	}

	inline static int32_t get_offset_of_m_1() { return static_cast<int32_t>(offsetof(U3CScanTypeForConstructorsU3Ec__AnonStorey1_t3510309839, ___m_1)); }
	inline MethodInfo_t * get_m_1() const { return ___m_1; }
	inline MethodInfo_t ** get_address_of_m_1() { return &___m_1; }
	inline void set_m_1(MethodInfo_t * value)
	{
		___m_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
