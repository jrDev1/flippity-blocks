﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1629559380.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea3962745861.h"

// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchQuickTouch
struct  EasyTouchQuickTouch_t3569589299  : public EasyTouchQuickFSM_t1629559380
{
public:
	// HutongGames.PlayMaker.Actions.EasyTouchQuickTouch/ActionTriggering HutongGames.PlayMaker.Actions.EasyTouchQuickTouch::actionTriggering
	int32_t ___actionTriggering_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EasyTouchQuickTouch::sendEventNotOverMe
	FsmEvent_t1258573736 * ___sendEventNotOverMe_20;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchQuickTouch::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_21;

public:
	inline static int32_t get_offset_of_actionTriggering_19() { return static_cast<int32_t>(offsetof(EasyTouchQuickTouch_t3569589299, ___actionTriggering_19)); }
	inline int32_t get_actionTriggering_19() const { return ___actionTriggering_19; }
	inline int32_t* get_address_of_actionTriggering_19() { return &___actionTriggering_19; }
	inline void set_actionTriggering_19(int32_t value)
	{
		___actionTriggering_19 = value;
	}

	inline static int32_t get_offset_of_sendEventNotOverMe_20() { return static_cast<int32_t>(offsetof(EasyTouchQuickTouch_t3569589299, ___sendEventNotOverMe_20)); }
	inline FsmEvent_t1258573736 * get_sendEventNotOverMe_20() const { return ___sendEventNotOverMe_20; }
	inline FsmEvent_t1258573736 ** get_address_of_sendEventNotOverMe_20() { return &___sendEventNotOverMe_20; }
	inline void set_sendEventNotOverMe_20(FsmEvent_t1258573736 * value)
	{
		___sendEventNotOverMe_20 = value;
		Il2CppCodeGenWriteBarrier(&___sendEventNotOverMe_20, value);
	}

	inline static int32_t get_offset_of_proxy_21() { return static_cast<int32_t>(offsetof(EasyTouchQuickTouch_t3569589299, ___proxy_21)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_21() const { return ___proxy_21; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_21() { return &___proxy_21; }
	inline void set_proxy_21(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_21 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
