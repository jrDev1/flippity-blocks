﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtSpawn
struct  PmtSpawn_t3548987524  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtSpawn::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtSpawn::gameObject
	FsmGameObject_t3097142863 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtSpawn::spawnTransform
	FsmGameObject_t3097142863 * ___spawnTransform_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.PmtSpawn::OrSpawnPosition
	FsmVector3_t3996534004 * ___OrSpawnPosition_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtSpawn::spawnedGameObject
	FsmGameObject_t3097142863 * ___spawnedGameObject_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtSpawn::successEvent
	FsmEvent_t1258573736 * ___successEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtSpawn::failEvent
	FsmEvent_t1258573736 * ___failEvent_17;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___gameObject_12)); }
	inline FsmGameObject_t3097142863 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmGameObject_t3097142863 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_spawnTransform_13() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___spawnTransform_13)); }
	inline FsmGameObject_t3097142863 * get_spawnTransform_13() const { return ___spawnTransform_13; }
	inline FsmGameObject_t3097142863 ** get_address_of_spawnTransform_13() { return &___spawnTransform_13; }
	inline void set_spawnTransform_13(FsmGameObject_t3097142863 * value)
	{
		___spawnTransform_13 = value;
		Il2CppCodeGenWriteBarrier(&___spawnTransform_13, value);
	}

	inline static int32_t get_offset_of_OrSpawnPosition_14() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___OrSpawnPosition_14)); }
	inline FsmVector3_t3996534004 * get_OrSpawnPosition_14() const { return ___OrSpawnPosition_14; }
	inline FsmVector3_t3996534004 ** get_address_of_OrSpawnPosition_14() { return &___OrSpawnPosition_14; }
	inline void set_OrSpawnPosition_14(FsmVector3_t3996534004 * value)
	{
		___OrSpawnPosition_14 = value;
		Il2CppCodeGenWriteBarrier(&___OrSpawnPosition_14, value);
	}

	inline static int32_t get_offset_of_spawnedGameObject_15() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___spawnedGameObject_15)); }
	inline FsmGameObject_t3097142863 * get_spawnedGameObject_15() const { return ___spawnedGameObject_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_spawnedGameObject_15() { return &___spawnedGameObject_15; }
	inline void set_spawnedGameObject_15(FsmGameObject_t3097142863 * value)
	{
		___spawnedGameObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___spawnedGameObject_15, value);
	}

	inline static int32_t get_offset_of_successEvent_16() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___successEvent_16)); }
	inline FsmEvent_t1258573736 * get_successEvent_16() const { return ___successEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_successEvent_16() { return &___successEvent_16; }
	inline void set_successEvent_16(FsmEvent_t1258573736 * value)
	{
		___successEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_16, value);
	}

	inline static int32_t get_offset_of_failEvent_17() { return static_cast<int32_t>(offsetof(PmtSpawn_t3548987524, ___failEvent_17)); }
	inline FsmEvent_t1258573736 * get_failEvent_17() const { return ___failEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_failEvent_17() { return &___failEvent_17; }
	inline void set_failEvent_17(FsmEvent_t1258573736 * value)
	{
		___failEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___failEvent_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
