﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3272313708.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchSet2FingersPickMethod
struct  EasyTouchSet2FingersPickMethod_t1505188749  : public FsmStateAction_t2862378169
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/TwoFingerPickMethod HutongGames.PlayMaker.Actions.EasyTouchSet2FingersPickMethod::method
	int32_t ___method_11;

public:
	inline static int32_t get_offset_of_method_11() { return static_cast<int32_t>(offsetof(EasyTouchSet2FingersPickMethod_t1505188749, ___method_11)); }
	inline int32_t get_method_11() const { return ___method_11; }
	inline int32_t* get_address_of_method_11() { return &___method_11; }
	inline void set_method_11(int32_t value)
	{
		___method_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
