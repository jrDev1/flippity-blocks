﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.ProfilerTabController
struct  ProfilerTabController_t2689247202  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Boolean SRDebugger.UI.Tabs.ProfilerTabController::_isDirty
	bool ____isDirty_9;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.ProfilerTabController::PinToggle
	Toggle_t3976754468 * ___PinToggle_10;

public:
	inline static int32_t get_offset_of__isDirty_9() { return static_cast<int32_t>(offsetof(ProfilerTabController_t2689247202, ____isDirty_9)); }
	inline bool get__isDirty_9() const { return ____isDirty_9; }
	inline bool* get_address_of__isDirty_9() { return &____isDirty_9; }
	inline void set__isDirty_9(bool value)
	{
		____isDirty_9 = value;
	}

	inline static int32_t get_offset_of_PinToggle_10() { return static_cast<int32_t>(offsetof(ProfilerTabController_t2689247202, ___PinToggle_10)); }
	inline Toggle_t3976754468 * get_PinToggle_10() const { return ___PinToggle_10; }
	inline Toggle_t3976754468 ** get_address_of_PinToggle_10() { return &___PinToggle_10; }
	inline void set_PinToggle_10(Toggle_t3976754468 * value)
	{
		___PinToggle_10 = value;
		Il2CppCodeGenWriteBarrier(&___PinToggle_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
