﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.IList`1<SRDebugger.InfoEntry>>
struct Dictionary_2_t726545509;
// System.Func`1<System.Object>
struct Func_1_t348874681;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.StandardSystemInformationService
struct  StandardSystemInformationService_t3828701795  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.IList`1<SRDebugger.InfoEntry>> SRDebugger.Services.Implementation.StandardSystemInformationService::_info
	Dictionary_2_t726545509 * ____info_0;

public:
	inline static int32_t get_offset_of__info_0() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795, ____info_0)); }
	inline Dictionary_2_t726545509 * get__info_0() const { return ____info_0; }
	inline Dictionary_2_t726545509 ** get_address_of__info_0() { return &____info_0; }
	inline void set__info_0(Dictionary_2_t726545509 * value)
	{
		____info_0 = value;
		Il2CppCodeGenWriteBarrier(&____info_0, value);
	}
};

struct StandardSystemInformationService_t3828701795_StaticFields
{
public:
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__mg$cache0
	Func_1_t348874681 * ___U3CU3Ef__mgU24cache0_1;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__mg$cache1
	Func_1_t348874681 * ___U3CU3Ef__mgU24cache1_2;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__mg$cache2
	Func_1_t348874681 * ___U3CU3Ef__mgU24cache2_3;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__mg$cache3
	Func_1_t348874681 * ___U3CU3Ef__mgU24cache3_4;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__mg$cache4
	Func_1_t348874681 * ___U3CU3Ef__mgU24cache4_5;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache0
	Func_1_t348874681 * ___U3CU3Ef__amU24cache0_6;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache1
	Func_1_t348874681 * ___U3CU3Ef__amU24cache1_7;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache2
	Func_1_t348874681 * ___U3CU3Ef__amU24cache2_8;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache3
	Func_1_t348874681 * ___U3CU3Ef__amU24cache3_9;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache4
	Func_1_t348874681 * ___U3CU3Ef__amU24cache4_10;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache5
	Func_1_t348874681 * ___U3CU3Ef__amU24cache5_11;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache6
	Func_1_t348874681 * ___U3CU3Ef__amU24cache6_12;
	// System.Func`1<System.Object> SRDebugger.Services.Implementation.StandardSystemInformationService::<>f__am$cache7
	Func_1_t348874681 * ___U3CU3Ef__amU24cache7_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Func_1_t348874681 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Func_1_t348874681 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_2() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__mgU24cache1_2)); }
	inline Func_1_t348874681 * get_U3CU3Ef__mgU24cache1_2() const { return ___U3CU3Ef__mgU24cache1_2; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__mgU24cache1_2() { return &___U3CU3Ef__mgU24cache1_2; }
	inline void set_U3CU3Ef__mgU24cache1_2(Func_1_t348874681 * value)
	{
		___U3CU3Ef__mgU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_3() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__mgU24cache2_3)); }
	inline Func_1_t348874681 * get_U3CU3Ef__mgU24cache2_3() const { return ___U3CU3Ef__mgU24cache2_3; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__mgU24cache2_3() { return &___U3CU3Ef__mgU24cache2_3; }
	inline void set_U3CU3Ef__mgU24cache2_3(Func_1_t348874681 * value)
	{
		___U3CU3Ef__mgU24cache2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache2_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_4() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__mgU24cache3_4)); }
	inline Func_1_t348874681 * get_U3CU3Ef__mgU24cache3_4() const { return ___U3CU3Ef__mgU24cache3_4; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__mgU24cache3_4() { return &___U3CU3Ef__mgU24cache3_4; }
	inline void set_U3CU3Ef__mgU24cache3_4(Func_1_t348874681 * value)
	{
		___U3CU3Ef__mgU24cache3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache3_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache4_5() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__mgU24cache4_5)); }
	inline Func_1_t348874681 * get_U3CU3Ef__mgU24cache4_5() const { return ___U3CU3Ef__mgU24cache4_5; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__mgU24cache4_5() { return &___U3CU3Ef__mgU24cache4_5; }
	inline void set_U3CU3Ef__mgU24cache4_5(Func_1_t348874681 * value)
	{
		___U3CU3Ef__mgU24cache4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache4_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_8() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache2_8)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache2_8() const { return ___U3CU3Ef__amU24cache2_8; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache2_8() { return &___U3CU3Ef__amU24cache2_8; }
	inline void set_U3CU3Ef__amU24cache2_8(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache2_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_9() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache3_9)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache3_9() const { return ___U3CU3Ef__amU24cache3_9; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache3_9() { return &___U3CU3Ef__amU24cache3_9; }
	inline void set_U3CU3Ef__amU24cache3_9(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache3_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_10() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache4_10)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache4_10() const { return ___U3CU3Ef__amU24cache4_10; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache4_10() { return &___U3CU3Ef__amU24cache4_10; }
	inline void set_U3CU3Ef__amU24cache4_10(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache4_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_11() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache5_11)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache5_11() const { return ___U3CU3Ef__amU24cache5_11; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache5_11() { return &___U3CU3Ef__amU24cache5_11; }
	inline void set_U3CU3Ef__amU24cache5_11(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache5_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_12() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache6_12)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache6_12() const { return ___U3CU3Ef__amU24cache6_12; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache6_12() { return &___U3CU3Ef__amU24cache6_12; }
	inline void set_U3CU3Ef__amU24cache6_12(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache6_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_13() { return static_cast<int32_t>(offsetof(StandardSystemInformationService_t3828701795_StaticFields, ___U3CU3Ef__amU24cache7_13)); }
	inline Func_1_t348874681 * get_U3CU3Ef__amU24cache7_13() const { return ___U3CU3Ef__amU24cache7_13; }
	inline Func_1_t348874681 ** get_address_of_U3CU3Ef__amU24cache7_13() { return &___U3CU3Ef__amU24cache7_13; }
	inline void set_U3CU3Ef__amU24cache7_13(Func_1_t348874681 * value)
	{
		___U3CU3Ef__amU24cache7_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
