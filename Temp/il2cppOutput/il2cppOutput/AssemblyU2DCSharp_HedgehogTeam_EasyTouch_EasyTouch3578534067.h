﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_316495900.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch1293824952.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3272313708.h"
#include "UnityEngine_UnityEngine_LayerMask3188175821.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

// HedgehogTeam.EasyTouch.EasyTouch/TouchCancelHandler
struct TouchCancelHandler_t3720797970;
// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler
struct Cancel2FingersHandler_t2820220009;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler
struct TouchStartHandler_t3045666224;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler
struct TouchDownHandler_t16499120;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler
struct TouchUpHandler_t3694099103;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler
struct SimpleTapHandler_t2041759604;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapHandler
struct DoubleTapHandler_t3910199663;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStartHandler
struct LongTapStartHandler_t4142433656;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapHandler
struct LongTapHandler_t2327584816;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEndHandler
struct LongTapEndHandler_t2540206763;
// HedgehogTeam.EasyTouch.EasyTouch/DragStartHandler
struct DragStartHandler_t1509548139;
// HedgehogTeam.EasyTouch.EasyTouch/DragHandler
struct DragHandler_t3855953415;
// HedgehogTeam.EasyTouch.EasyTouch/DragEndHandler
struct DragEndHandler_t2953676952;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler
struct SwipeStartHandler_t1813931207;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler
struct SwipeHandler_t4041355147;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler
struct SwipeEndHandler_t567777398;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler
struct TouchStart2FingersHandler_t3096813332;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler
struct TouchDown2FingersHandler_t3403539080;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler
struct TouchUp2FingersHandler_t384986787;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler
struct SimpleTap2FingersHandler_t3725456324;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler
struct DoubleTap2FingersHandler_t313188403;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler
struct LongTapStart2FingersHandler_t59253952;
// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler
struct LongTap2FingersHandler_t2742766376;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler
struct LongTapEnd2FingersHandler_t3611206799;
// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler
struct TwistHandler_t117269532;
// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler
struct TwistEndHandler_t1042340097;
// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler
struct PinchHandler_t3939026435;
// HedgehogTeam.EasyTouch.EasyTouch/PinchInHandler
struct PinchInHandler_t2898198212;
// HedgehogTeam.EasyTouch.EasyTouch/PinchOutHandler
struct PinchOutHandler_t3267676377;
// HedgehogTeam.EasyTouch.EasyTouch/PinchEndHandler
struct PinchEndHandler_t2735783548;
// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler
struct DragStart2FingersHandler_t1996108375;
// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler
struct Drag2FingersHandler_t599932411;
// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler
struct DragEnd2FingersHandler_t1069006048;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler
struct SwipeStart2FingersHandler_t3085603867;
// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler
struct Swipe2FingersHandler_t2386328279;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler
struct SwipeEnd2FingersHandler_t2863096110;
// HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler
struct EasyTouchIsReadyHandler_t970770497;
// HedgehogTeam.EasyTouch.EasyTouch/OverUIElementHandler
struct OverUIElementHandler_t4133021939;
// HedgehogTeam.EasyTouch.EasyTouch/UIElementTouchUpHandler
struct UIElementTouchUpHandler_t3300966889;
// HedgehogTeam.EasyTouch.EasyTouch
struct EasyTouch_t3578534067;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.Gesture>
struct List_1_t4032083825;
// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.ECamera>
struct List_1_t2963993122;
// System.Collections.Generic.List`1<UnityEngine.Camera>
struct List_1_t3853549405;
// HedgehogTeam.EasyTouch.EasyTouchInput
struct EasyTouchInput_t389322783;
// HedgehogTeam.EasyTouch.Finger[]
struct FingerU5BU5D_t3562528856;
// UnityEngine.Texture
struct Texture_t2243626319;
// HedgehogTeam.EasyTouch.TwoFingerGesture
struct TwoFingerGesture_t2848072384;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap[]
struct DoubleTapU5BU5D_t1276610926;
// HedgehogTeam.EasyTouch.EasyTouch/PickedObject
struct PickedObject_t3877774310;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// System.Predicate`1<HedgehogTeam.EasyTouch.ECamera>
struct Predicate_1_t2037842105;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch
struct  EasyTouch_t3578534067  : public MonoBehaviour_t1158329972
{
public:
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.EasyTouch::_currentGesture
	Gesture_t367995397 * ____currentGesture_42;
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.Gesture> HedgehogTeam.EasyTouch.EasyTouch::_currentGestures
	List_1_t4032083825 * ____currentGestures_43;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable
	bool ___enable_44;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableRemote
	bool ___enableRemote_45;
	// HedgehogTeam.EasyTouch.EasyTouch/GesturePriority HedgehogTeam.EasyTouch.EasyTouch::gesturePriority
	int32_t ___gesturePriority_46;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::StationaryTolerance
	float ___StationaryTolerance_47;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::longTapTime
	float ___longTapTime_48;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::swipeTolerance
	float ___swipeTolerance_49;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::minPinchLength
	float ___minPinchLength_50;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::minTwistAngle
	float ___minTwistAngle_51;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapDetection HedgehogTeam.EasyTouch.EasyTouch::doubleTapDetection
	int32_t ___doubleTapDetection_52;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch::doubleTapTime
	float ___doubleTapTime_53;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::alwaysSendSwipe
	bool ___alwaysSendSwipe_54;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2FingersGesture
	bool ___enable2FingersGesture_55;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableTwist
	bool ___enableTwist_56;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enablePinch
	bool ___enablePinch_57;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2FingersSwipe
	bool ___enable2FingersSwipe_58;
	// HedgehogTeam.EasyTouch.EasyTouch/TwoFingerPickMethod HedgehogTeam.EasyTouch.EasyTouch::twoFingerPickMethod
	int32_t ___twoFingerPickMethod_59;
	// System.Collections.Generic.List`1<HedgehogTeam.EasyTouch.ECamera> HedgehogTeam.EasyTouch.EasyTouch::touchCameras
	List_1_t2963993122 * ___touchCameras_60;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoSelect
	bool ___autoSelect_61;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::pickableLayers3D
	LayerMask_t3188175821  ___pickableLayers3D_62;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enable2D
	bool ___enable2D_63;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::pickableLayers2D
	LayerMask_t3188175821  ___pickableLayers2D_64;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoUpdatePickedObject
	bool ___autoUpdatePickedObject_65;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::allowUIDetection
	bool ___allowUIDetection_66;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableUIMode
	bool ___enableUIMode_67;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::autoUpdatePickedUI
	bool ___autoUpdatePickedUI_68;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enabledNGuiMode
	bool ___enabledNGuiMode_69;
	// UnityEngine.LayerMask HedgehogTeam.EasyTouch.EasyTouch::nGUILayers
	LayerMask_t3188175821  ___nGUILayers_70;
	// System.Collections.Generic.List`1<UnityEngine.Camera> HedgehogTeam.EasyTouch.EasyTouch::nGUICameras
	List_1_t3853549405 * ___nGUICameras_71;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::enableSimulation
	bool ___enableSimulation_72;
	// UnityEngine.KeyCode HedgehogTeam.EasyTouch.EasyTouch::twistKey
	int32_t ___twistKey_73;
	// UnityEngine.KeyCode HedgehogTeam.EasyTouch.EasyTouch::swipeKey
	int32_t ___swipeKey_74;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showGuiInspector
	bool ___showGuiInspector_75;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showSelectInspector
	bool ___showSelectInspector_76;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showGestureInspector
	bool ___showGestureInspector_77;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showTwoFingerInspector
	bool ___showTwoFingerInspector_78;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch::showSecondFingerInspector
	bool ___showSecondFingerInspector_79;
	// HedgehogTeam.EasyTouch.EasyTouchInput HedgehogTeam.EasyTouch.EasyTouch::input
	EasyTouchInput_t389322783 * ___input_80;
	// HedgehogTeam.EasyTouch.Finger[] HedgehogTeam.EasyTouch.EasyTouch::fingers
	FingerU5BU5D_t3562528856* ___fingers_81;
	// UnityEngine.Texture HedgehogTeam.EasyTouch.EasyTouch::secondFingerTexture
	Texture_t2243626319 * ___secondFingerTexture_82;
	// HedgehogTeam.EasyTouch.TwoFingerGesture HedgehogTeam.EasyTouch.EasyTouch::twoFinger
	TwoFingerGesture_t2848072384 * ___twoFinger_83;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch::oldTouchCount
	int32_t ___oldTouchCount_84;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap[] HedgehogTeam.EasyTouch.EasyTouch::singleDoubleTap
	DoubleTapU5BU5D_t1276610926* ___singleDoubleTap_85;
	// HedgehogTeam.EasyTouch.Finger[] HedgehogTeam.EasyTouch.EasyTouch::tmpArray
	FingerU5BU5D_t3562528856* ___tmpArray_86;
	// HedgehogTeam.EasyTouch.EasyTouch/PickedObject HedgehogTeam.EasyTouch.EasyTouch::pickedObject
	PickedObject_t3877774310 * ___pickedObject_87;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> HedgehogTeam.EasyTouch.EasyTouch::uiRaycastResultCache
	List_1_t3685274804 * ___uiRaycastResultCache_88;
	// UnityEngine.EventSystems.PointerEventData HedgehogTeam.EasyTouch.EasyTouch::uiPointerEventData
	PointerEventData_t1599784723 * ___uiPointerEventData_89;
	// UnityEngine.EventSystems.EventSystem HedgehogTeam.EasyTouch.EasyTouch::uiEventSystem
	EventSystem_t3466835263 * ___uiEventSystem_90;

public:
	inline static int32_t get_offset_of__currentGesture_42() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ____currentGesture_42)); }
	inline Gesture_t367995397 * get__currentGesture_42() const { return ____currentGesture_42; }
	inline Gesture_t367995397 ** get_address_of__currentGesture_42() { return &____currentGesture_42; }
	inline void set__currentGesture_42(Gesture_t367995397 * value)
	{
		____currentGesture_42 = value;
		Il2CppCodeGenWriteBarrier(&____currentGesture_42, value);
	}

	inline static int32_t get_offset_of__currentGestures_43() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ____currentGestures_43)); }
	inline List_1_t4032083825 * get__currentGestures_43() const { return ____currentGestures_43; }
	inline List_1_t4032083825 ** get_address_of__currentGestures_43() { return &____currentGestures_43; }
	inline void set__currentGestures_43(List_1_t4032083825 * value)
	{
		____currentGestures_43 = value;
		Il2CppCodeGenWriteBarrier(&____currentGestures_43, value);
	}

	inline static int32_t get_offset_of_enable_44() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enable_44)); }
	inline bool get_enable_44() const { return ___enable_44; }
	inline bool* get_address_of_enable_44() { return &___enable_44; }
	inline void set_enable_44(bool value)
	{
		___enable_44 = value;
	}

	inline static int32_t get_offset_of_enableRemote_45() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enableRemote_45)); }
	inline bool get_enableRemote_45() const { return ___enableRemote_45; }
	inline bool* get_address_of_enableRemote_45() { return &___enableRemote_45; }
	inline void set_enableRemote_45(bool value)
	{
		___enableRemote_45 = value;
	}

	inline static int32_t get_offset_of_gesturePriority_46() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___gesturePriority_46)); }
	inline int32_t get_gesturePriority_46() const { return ___gesturePriority_46; }
	inline int32_t* get_address_of_gesturePriority_46() { return &___gesturePriority_46; }
	inline void set_gesturePriority_46(int32_t value)
	{
		___gesturePriority_46 = value;
	}

	inline static int32_t get_offset_of_StationaryTolerance_47() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___StationaryTolerance_47)); }
	inline float get_StationaryTolerance_47() const { return ___StationaryTolerance_47; }
	inline float* get_address_of_StationaryTolerance_47() { return &___StationaryTolerance_47; }
	inline void set_StationaryTolerance_47(float value)
	{
		___StationaryTolerance_47 = value;
	}

	inline static int32_t get_offset_of_longTapTime_48() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___longTapTime_48)); }
	inline float get_longTapTime_48() const { return ___longTapTime_48; }
	inline float* get_address_of_longTapTime_48() { return &___longTapTime_48; }
	inline void set_longTapTime_48(float value)
	{
		___longTapTime_48 = value;
	}

	inline static int32_t get_offset_of_swipeTolerance_49() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___swipeTolerance_49)); }
	inline float get_swipeTolerance_49() const { return ___swipeTolerance_49; }
	inline float* get_address_of_swipeTolerance_49() { return &___swipeTolerance_49; }
	inline void set_swipeTolerance_49(float value)
	{
		___swipeTolerance_49 = value;
	}

	inline static int32_t get_offset_of_minPinchLength_50() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___minPinchLength_50)); }
	inline float get_minPinchLength_50() const { return ___minPinchLength_50; }
	inline float* get_address_of_minPinchLength_50() { return &___minPinchLength_50; }
	inline void set_minPinchLength_50(float value)
	{
		___minPinchLength_50 = value;
	}

	inline static int32_t get_offset_of_minTwistAngle_51() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___minTwistAngle_51)); }
	inline float get_minTwistAngle_51() const { return ___minTwistAngle_51; }
	inline float* get_address_of_minTwistAngle_51() { return &___minTwistAngle_51; }
	inline void set_minTwistAngle_51(float value)
	{
		___minTwistAngle_51 = value;
	}

	inline static int32_t get_offset_of_doubleTapDetection_52() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___doubleTapDetection_52)); }
	inline int32_t get_doubleTapDetection_52() const { return ___doubleTapDetection_52; }
	inline int32_t* get_address_of_doubleTapDetection_52() { return &___doubleTapDetection_52; }
	inline void set_doubleTapDetection_52(int32_t value)
	{
		___doubleTapDetection_52 = value;
	}

	inline static int32_t get_offset_of_doubleTapTime_53() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___doubleTapTime_53)); }
	inline float get_doubleTapTime_53() const { return ___doubleTapTime_53; }
	inline float* get_address_of_doubleTapTime_53() { return &___doubleTapTime_53; }
	inline void set_doubleTapTime_53(float value)
	{
		___doubleTapTime_53 = value;
	}

	inline static int32_t get_offset_of_alwaysSendSwipe_54() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___alwaysSendSwipe_54)); }
	inline bool get_alwaysSendSwipe_54() const { return ___alwaysSendSwipe_54; }
	inline bool* get_address_of_alwaysSendSwipe_54() { return &___alwaysSendSwipe_54; }
	inline void set_alwaysSendSwipe_54(bool value)
	{
		___alwaysSendSwipe_54 = value;
	}

	inline static int32_t get_offset_of_enable2FingersGesture_55() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enable2FingersGesture_55)); }
	inline bool get_enable2FingersGesture_55() const { return ___enable2FingersGesture_55; }
	inline bool* get_address_of_enable2FingersGesture_55() { return &___enable2FingersGesture_55; }
	inline void set_enable2FingersGesture_55(bool value)
	{
		___enable2FingersGesture_55 = value;
	}

	inline static int32_t get_offset_of_enableTwist_56() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enableTwist_56)); }
	inline bool get_enableTwist_56() const { return ___enableTwist_56; }
	inline bool* get_address_of_enableTwist_56() { return &___enableTwist_56; }
	inline void set_enableTwist_56(bool value)
	{
		___enableTwist_56 = value;
	}

	inline static int32_t get_offset_of_enablePinch_57() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enablePinch_57)); }
	inline bool get_enablePinch_57() const { return ___enablePinch_57; }
	inline bool* get_address_of_enablePinch_57() { return &___enablePinch_57; }
	inline void set_enablePinch_57(bool value)
	{
		___enablePinch_57 = value;
	}

	inline static int32_t get_offset_of_enable2FingersSwipe_58() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enable2FingersSwipe_58)); }
	inline bool get_enable2FingersSwipe_58() const { return ___enable2FingersSwipe_58; }
	inline bool* get_address_of_enable2FingersSwipe_58() { return &___enable2FingersSwipe_58; }
	inline void set_enable2FingersSwipe_58(bool value)
	{
		___enable2FingersSwipe_58 = value;
	}

	inline static int32_t get_offset_of_twoFingerPickMethod_59() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___twoFingerPickMethod_59)); }
	inline int32_t get_twoFingerPickMethod_59() const { return ___twoFingerPickMethod_59; }
	inline int32_t* get_address_of_twoFingerPickMethod_59() { return &___twoFingerPickMethod_59; }
	inline void set_twoFingerPickMethod_59(int32_t value)
	{
		___twoFingerPickMethod_59 = value;
	}

	inline static int32_t get_offset_of_touchCameras_60() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___touchCameras_60)); }
	inline List_1_t2963993122 * get_touchCameras_60() const { return ___touchCameras_60; }
	inline List_1_t2963993122 ** get_address_of_touchCameras_60() { return &___touchCameras_60; }
	inline void set_touchCameras_60(List_1_t2963993122 * value)
	{
		___touchCameras_60 = value;
		Il2CppCodeGenWriteBarrier(&___touchCameras_60, value);
	}

	inline static int32_t get_offset_of_autoSelect_61() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___autoSelect_61)); }
	inline bool get_autoSelect_61() const { return ___autoSelect_61; }
	inline bool* get_address_of_autoSelect_61() { return &___autoSelect_61; }
	inline void set_autoSelect_61(bool value)
	{
		___autoSelect_61 = value;
	}

	inline static int32_t get_offset_of_pickableLayers3D_62() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___pickableLayers3D_62)); }
	inline LayerMask_t3188175821  get_pickableLayers3D_62() const { return ___pickableLayers3D_62; }
	inline LayerMask_t3188175821 * get_address_of_pickableLayers3D_62() { return &___pickableLayers3D_62; }
	inline void set_pickableLayers3D_62(LayerMask_t3188175821  value)
	{
		___pickableLayers3D_62 = value;
	}

	inline static int32_t get_offset_of_enable2D_63() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enable2D_63)); }
	inline bool get_enable2D_63() const { return ___enable2D_63; }
	inline bool* get_address_of_enable2D_63() { return &___enable2D_63; }
	inline void set_enable2D_63(bool value)
	{
		___enable2D_63 = value;
	}

	inline static int32_t get_offset_of_pickableLayers2D_64() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___pickableLayers2D_64)); }
	inline LayerMask_t3188175821  get_pickableLayers2D_64() const { return ___pickableLayers2D_64; }
	inline LayerMask_t3188175821 * get_address_of_pickableLayers2D_64() { return &___pickableLayers2D_64; }
	inline void set_pickableLayers2D_64(LayerMask_t3188175821  value)
	{
		___pickableLayers2D_64 = value;
	}

	inline static int32_t get_offset_of_autoUpdatePickedObject_65() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___autoUpdatePickedObject_65)); }
	inline bool get_autoUpdatePickedObject_65() const { return ___autoUpdatePickedObject_65; }
	inline bool* get_address_of_autoUpdatePickedObject_65() { return &___autoUpdatePickedObject_65; }
	inline void set_autoUpdatePickedObject_65(bool value)
	{
		___autoUpdatePickedObject_65 = value;
	}

	inline static int32_t get_offset_of_allowUIDetection_66() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___allowUIDetection_66)); }
	inline bool get_allowUIDetection_66() const { return ___allowUIDetection_66; }
	inline bool* get_address_of_allowUIDetection_66() { return &___allowUIDetection_66; }
	inline void set_allowUIDetection_66(bool value)
	{
		___allowUIDetection_66 = value;
	}

	inline static int32_t get_offset_of_enableUIMode_67() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enableUIMode_67)); }
	inline bool get_enableUIMode_67() const { return ___enableUIMode_67; }
	inline bool* get_address_of_enableUIMode_67() { return &___enableUIMode_67; }
	inline void set_enableUIMode_67(bool value)
	{
		___enableUIMode_67 = value;
	}

	inline static int32_t get_offset_of_autoUpdatePickedUI_68() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___autoUpdatePickedUI_68)); }
	inline bool get_autoUpdatePickedUI_68() const { return ___autoUpdatePickedUI_68; }
	inline bool* get_address_of_autoUpdatePickedUI_68() { return &___autoUpdatePickedUI_68; }
	inline void set_autoUpdatePickedUI_68(bool value)
	{
		___autoUpdatePickedUI_68 = value;
	}

	inline static int32_t get_offset_of_enabledNGuiMode_69() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enabledNGuiMode_69)); }
	inline bool get_enabledNGuiMode_69() const { return ___enabledNGuiMode_69; }
	inline bool* get_address_of_enabledNGuiMode_69() { return &___enabledNGuiMode_69; }
	inline void set_enabledNGuiMode_69(bool value)
	{
		___enabledNGuiMode_69 = value;
	}

	inline static int32_t get_offset_of_nGUILayers_70() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___nGUILayers_70)); }
	inline LayerMask_t3188175821  get_nGUILayers_70() const { return ___nGUILayers_70; }
	inline LayerMask_t3188175821 * get_address_of_nGUILayers_70() { return &___nGUILayers_70; }
	inline void set_nGUILayers_70(LayerMask_t3188175821  value)
	{
		___nGUILayers_70 = value;
	}

	inline static int32_t get_offset_of_nGUICameras_71() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___nGUICameras_71)); }
	inline List_1_t3853549405 * get_nGUICameras_71() const { return ___nGUICameras_71; }
	inline List_1_t3853549405 ** get_address_of_nGUICameras_71() { return &___nGUICameras_71; }
	inline void set_nGUICameras_71(List_1_t3853549405 * value)
	{
		___nGUICameras_71 = value;
		Il2CppCodeGenWriteBarrier(&___nGUICameras_71, value);
	}

	inline static int32_t get_offset_of_enableSimulation_72() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___enableSimulation_72)); }
	inline bool get_enableSimulation_72() const { return ___enableSimulation_72; }
	inline bool* get_address_of_enableSimulation_72() { return &___enableSimulation_72; }
	inline void set_enableSimulation_72(bool value)
	{
		___enableSimulation_72 = value;
	}

	inline static int32_t get_offset_of_twistKey_73() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___twistKey_73)); }
	inline int32_t get_twistKey_73() const { return ___twistKey_73; }
	inline int32_t* get_address_of_twistKey_73() { return &___twistKey_73; }
	inline void set_twistKey_73(int32_t value)
	{
		___twistKey_73 = value;
	}

	inline static int32_t get_offset_of_swipeKey_74() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___swipeKey_74)); }
	inline int32_t get_swipeKey_74() const { return ___swipeKey_74; }
	inline int32_t* get_address_of_swipeKey_74() { return &___swipeKey_74; }
	inline void set_swipeKey_74(int32_t value)
	{
		___swipeKey_74 = value;
	}

	inline static int32_t get_offset_of_showGuiInspector_75() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___showGuiInspector_75)); }
	inline bool get_showGuiInspector_75() const { return ___showGuiInspector_75; }
	inline bool* get_address_of_showGuiInspector_75() { return &___showGuiInspector_75; }
	inline void set_showGuiInspector_75(bool value)
	{
		___showGuiInspector_75 = value;
	}

	inline static int32_t get_offset_of_showSelectInspector_76() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___showSelectInspector_76)); }
	inline bool get_showSelectInspector_76() const { return ___showSelectInspector_76; }
	inline bool* get_address_of_showSelectInspector_76() { return &___showSelectInspector_76; }
	inline void set_showSelectInspector_76(bool value)
	{
		___showSelectInspector_76 = value;
	}

	inline static int32_t get_offset_of_showGestureInspector_77() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___showGestureInspector_77)); }
	inline bool get_showGestureInspector_77() const { return ___showGestureInspector_77; }
	inline bool* get_address_of_showGestureInspector_77() { return &___showGestureInspector_77; }
	inline void set_showGestureInspector_77(bool value)
	{
		___showGestureInspector_77 = value;
	}

	inline static int32_t get_offset_of_showTwoFingerInspector_78() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___showTwoFingerInspector_78)); }
	inline bool get_showTwoFingerInspector_78() const { return ___showTwoFingerInspector_78; }
	inline bool* get_address_of_showTwoFingerInspector_78() { return &___showTwoFingerInspector_78; }
	inline void set_showTwoFingerInspector_78(bool value)
	{
		___showTwoFingerInspector_78 = value;
	}

	inline static int32_t get_offset_of_showSecondFingerInspector_79() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___showSecondFingerInspector_79)); }
	inline bool get_showSecondFingerInspector_79() const { return ___showSecondFingerInspector_79; }
	inline bool* get_address_of_showSecondFingerInspector_79() { return &___showSecondFingerInspector_79; }
	inline void set_showSecondFingerInspector_79(bool value)
	{
		___showSecondFingerInspector_79 = value;
	}

	inline static int32_t get_offset_of_input_80() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___input_80)); }
	inline EasyTouchInput_t389322783 * get_input_80() const { return ___input_80; }
	inline EasyTouchInput_t389322783 ** get_address_of_input_80() { return &___input_80; }
	inline void set_input_80(EasyTouchInput_t389322783 * value)
	{
		___input_80 = value;
		Il2CppCodeGenWriteBarrier(&___input_80, value);
	}

	inline static int32_t get_offset_of_fingers_81() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___fingers_81)); }
	inline FingerU5BU5D_t3562528856* get_fingers_81() const { return ___fingers_81; }
	inline FingerU5BU5D_t3562528856** get_address_of_fingers_81() { return &___fingers_81; }
	inline void set_fingers_81(FingerU5BU5D_t3562528856* value)
	{
		___fingers_81 = value;
		Il2CppCodeGenWriteBarrier(&___fingers_81, value);
	}

	inline static int32_t get_offset_of_secondFingerTexture_82() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___secondFingerTexture_82)); }
	inline Texture_t2243626319 * get_secondFingerTexture_82() const { return ___secondFingerTexture_82; }
	inline Texture_t2243626319 ** get_address_of_secondFingerTexture_82() { return &___secondFingerTexture_82; }
	inline void set_secondFingerTexture_82(Texture_t2243626319 * value)
	{
		___secondFingerTexture_82 = value;
		Il2CppCodeGenWriteBarrier(&___secondFingerTexture_82, value);
	}

	inline static int32_t get_offset_of_twoFinger_83() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___twoFinger_83)); }
	inline TwoFingerGesture_t2848072384 * get_twoFinger_83() const { return ___twoFinger_83; }
	inline TwoFingerGesture_t2848072384 ** get_address_of_twoFinger_83() { return &___twoFinger_83; }
	inline void set_twoFinger_83(TwoFingerGesture_t2848072384 * value)
	{
		___twoFinger_83 = value;
		Il2CppCodeGenWriteBarrier(&___twoFinger_83, value);
	}

	inline static int32_t get_offset_of_oldTouchCount_84() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___oldTouchCount_84)); }
	inline int32_t get_oldTouchCount_84() const { return ___oldTouchCount_84; }
	inline int32_t* get_address_of_oldTouchCount_84() { return &___oldTouchCount_84; }
	inline void set_oldTouchCount_84(int32_t value)
	{
		___oldTouchCount_84 = value;
	}

	inline static int32_t get_offset_of_singleDoubleTap_85() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___singleDoubleTap_85)); }
	inline DoubleTapU5BU5D_t1276610926* get_singleDoubleTap_85() const { return ___singleDoubleTap_85; }
	inline DoubleTapU5BU5D_t1276610926** get_address_of_singleDoubleTap_85() { return &___singleDoubleTap_85; }
	inline void set_singleDoubleTap_85(DoubleTapU5BU5D_t1276610926* value)
	{
		___singleDoubleTap_85 = value;
		Il2CppCodeGenWriteBarrier(&___singleDoubleTap_85, value);
	}

	inline static int32_t get_offset_of_tmpArray_86() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___tmpArray_86)); }
	inline FingerU5BU5D_t3562528856* get_tmpArray_86() const { return ___tmpArray_86; }
	inline FingerU5BU5D_t3562528856** get_address_of_tmpArray_86() { return &___tmpArray_86; }
	inline void set_tmpArray_86(FingerU5BU5D_t3562528856* value)
	{
		___tmpArray_86 = value;
		Il2CppCodeGenWriteBarrier(&___tmpArray_86, value);
	}

	inline static int32_t get_offset_of_pickedObject_87() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___pickedObject_87)); }
	inline PickedObject_t3877774310 * get_pickedObject_87() const { return ___pickedObject_87; }
	inline PickedObject_t3877774310 ** get_address_of_pickedObject_87() { return &___pickedObject_87; }
	inline void set_pickedObject_87(PickedObject_t3877774310 * value)
	{
		___pickedObject_87 = value;
		Il2CppCodeGenWriteBarrier(&___pickedObject_87, value);
	}

	inline static int32_t get_offset_of_uiRaycastResultCache_88() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___uiRaycastResultCache_88)); }
	inline List_1_t3685274804 * get_uiRaycastResultCache_88() const { return ___uiRaycastResultCache_88; }
	inline List_1_t3685274804 ** get_address_of_uiRaycastResultCache_88() { return &___uiRaycastResultCache_88; }
	inline void set_uiRaycastResultCache_88(List_1_t3685274804 * value)
	{
		___uiRaycastResultCache_88 = value;
		Il2CppCodeGenWriteBarrier(&___uiRaycastResultCache_88, value);
	}

	inline static int32_t get_offset_of_uiPointerEventData_89() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___uiPointerEventData_89)); }
	inline PointerEventData_t1599784723 * get_uiPointerEventData_89() const { return ___uiPointerEventData_89; }
	inline PointerEventData_t1599784723 ** get_address_of_uiPointerEventData_89() { return &___uiPointerEventData_89; }
	inline void set_uiPointerEventData_89(PointerEventData_t1599784723 * value)
	{
		___uiPointerEventData_89 = value;
		Il2CppCodeGenWriteBarrier(&___uiPointerEventData_89, value);
	}

	inline static int32_t get_offset_of_uiEventSystem_90() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067, ___uiEventSystem_90)); }
	inline EventSystem_t3466835263 * get_uiEventSystem_90() const { return ___uiEventSystem_90; }
	inline EventSystem_t3466835263 ** get_address_of_uiEventSystem_90() { return &___uiEventSystem_90; }
	inline void set_uiEventSystem_90(EventSystem_t3466835263 * value)
	{
		___uiEventSystem_90 = value;
		Il2CppCodeGenWriteBarrier(&___uiEventSystem_90, value);
	}
};

struct EasyTouch_t3578534067_StaticFields
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/TouchCancelHandler HedgehogTeam.EasyTouch.EasyTouch::On_Cancel
	TouchCancelHandler_t3720797970 * ___On_Cancel_2;
	// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Cancel2Fingers
	Cancel2FingersHandler_t2820220009 * ___On_Cancel2Fingers_3;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchStart
	TouchStartHandler_t3045666224 * ___On_TouchStart_4;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchDown
	TouchDownHandler_t16499120 * ___On_TouchDown_5;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchUp
	TouchUpHandler_t3694099103 * ___On_TouchUp_6;
	// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_SimpleTap
	SimpleTapHandler_t2041759604 * ___On_SimpleTap_7;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_DoubleTap
	DoubleTapHandler_t3910199663 * ___On_DoubleTap_8;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapStart
	LongTapStartHandler_t4142433656 * ___On_LongTapStart_9;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTap
	LongTapHandler_t2327584816 * ___On_LongTap_10;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapEnd
	LongTapEndHandler_t2540206763 * ___On_LongTapEnd_11;
	// HedgehogTeam.EasyTouch.EasyTouch/DragStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragStart
	DragStartHandler_t1509548139 * ___On_DragStart_12;
	// HedgehogTeam.EasyTouch.EasyTouch/DragHandler HedgehogTeam.EasyTouch.EasyTouch::On_Drag
	DragHandler_t3855953415 * ___On_Drag_13;
	// HedgehogTeam.EasyTouch.EasyTouch/DragEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragEnd
	DragEndHandler_t2953676952 * ___On_DragEnd_14;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeStart
	SwipeStartHandler_t1813931207 * ___On_SwipeStart_15;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler HedgehogTeam.EasyTouch.EasyTouch::On_Swipe
	SwipeHandler_t4041355147 * ___On_Swipe_16;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeEnd
	SwipeEndHandler_t567777398 * ___On_SwipeEnd_17;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchStart2Fingers
	TouchStart2FingersHandler_t3096813332 * ___On_TouchStart2Fingers_18;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchDown2Fingers
	TouchDown2FingersHandler_t3403539080 * ___On_TouchDown2Fingers_19;
	// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_TouchUp2Fingers
	TouchUp2FingersHandler_t384986787 * ___On_TouchUp2Fingers_20;
	// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SimpleTap2Fingers
	SimpleTap2FingersHandler_t3725456324 * ___On_SimpleTap2Fingers_21;
	// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DoubleTap2Fingers
	DoubleTap2FingersHandler_t313188403 * ___On_DoubleTap2Fingers_22;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapStart2Fingers
	LongTapStart2FingersHandler_t59253952 * ___On_LongTapStart2Fingers_23;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTap2Fingers
	LongTap2FingersHandler_t2742766376 * ___On_LongTap2Fingers_24;
	// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_LongTapEnd2Fingers
	LongTapEnd2FingersHandler_t3611206799 * ___On_LongTapEnd2Fingers_25;
	// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler HedgehogTeam.EasyTouch.EasyTouch::On_Twist
	TwistHandler_t117269532 * ___On_Twist_26;
	// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_TwistEnd
	TwistEndHandler_t1042340097 * ___On_TwistEnd_27;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler HedgehogTeam.EasyTouch.EasyTouch::On_Pinch
	PinchHandler_t3939026435 * ___On_Pinch_28;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchInHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchIn
	PinchInHandler_t2898198212 * ___On_PinchIn_29;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchOutHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchOut
	PinchOutHandler_t3267676377 * ___On_PinchOut_30;
	// HedgehogTeam.EasyTouch.EasyTouch/PinchEndHandler HedgehogTeam.EasyTouch.EasyTouch::On_PinchEnd
	PinchEndHandler_t2735783548 * ___On_PinchEnd_31;
	// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragStart2Fingers
	DragStart2FingersHandler_t1996108375 * ___On_DragStart2Fingers_32;
	// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Drag2Fingers
	Drag2FingersHandler_t599932411 * ___On_Drag2Fingers_33;
	// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_DragEnd2Fingers
	DragEnd2FingersHandler_t1069006048 * ___On_DragEnd2Fingers_34;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeStart2Fingers
	SwipeStart2FingersHandler_t3085603867 * ___On_SwipeStart2Fingers_35;
	// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_Swipe2Fingers
	Swipe2FingersHandler_t2386328279 * ___On_Swipe2Fingers_36;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler HedgehogTeam.EasyTouch.EasyTouch::On_SwipeEnd2Fingers
	SwipeEnd2FingersHandler_t2863096110 * ___On_SwipeEnd2Fingers_37;
	// HedgehogTeam.EasyTouch.EasyTouch/EasyTouchIsReadyHandler HedgehogTeam.EasyTouch.EasyTouch::On_EasyTouchIsReady
	EasyTouchIsReadyHandler_t970770497 * ___On_EasyTouchIsReady_38;
	// HedgehogTeam.EasyTouch.EasyTouch/OverUIElementHandler HedgehogTeam.EasyTouch.EasyTouch::On_OverUIElement
	OverUIElementHandler_t4133021939 * ___On_OverUIElement_39;
	// HedgehogTeam.EasyTouch.EasyTouch/UIElementTouchUpHandler HedgehogTeam.EasyTouch.EasyTouch::On_UIElementTouchUp
	UIElementTouchUpHandler_t3300966889 * ___On_UIElementTouchUp_40;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch::_instance
	EasyTouch_t3578534067 * ____instance_41;
	// System.Predicate`1<HedgehogTeam.EasyTouch.ECamera> HedgehogTeam.EasyTouch.EasyTouch::<>f__am$cache0
	Predicate_1_t2037842105 * ___U3CU3Ef__amU24cache0_91;

public:
	inline static int32_t get_offset_of_On_Cancel_2() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Cancel_2)); }
	inline TouchCancelHandler_t3720797970 * get_On_Cancel_2() const { return ___On_Cancel_2; }
	inline TouchCancelHandler_t3720797970 ** get_address_of_On_Cancel_2() { return &___On_Cancel_2; }
	inline void set_On_Cancel_2(TouchCancelHandler_t3720797970 * value)
	{
		___On_Cancel_2 = value;
		Il2CppCodeGenWriteBarrier(&___On_Cancel_2, value);
	}

	inline static int32_t get_offset_of_On_Cancel2Fingers_3() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Cancel2Fingers_3)); }
	inline Cancel2FingersHandler_t2820220009 * get_On_Cancel2Fingers_3() const { return ___On_Cancel2Fingers_3; }
	inline Cancel2FingersHandler_t2820220009 ** get_address_of_On_Cancel2Fingers_3() { return &___On_Cancel2Fingers_3; }
	inline void set_On_Cancel2Fingers_3(Cancel2FingersHandler_t2820220009 * value)
	{
		___On_Cancel2Fingers_3 = value;
		Il2CppCodeGenWriteBarrier(&___On_Cancel2Fingers_3, value);
	}

	inline static int32_t get_offset_of_On_TouchStart_4() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TouchStart_4)); }
	inline TouchStartHandler_t3045666224 * get_On_TouchStart_4() const { return ___On_TouchStart_4; }
	inline TouchStartHandler_t3045666224 ** get_address_of_On_TouchStart_4() { return &___On_TouchStart_4; }
	inline void set_On_TouchStart_4(TouchStartHandler_t3045666224 * value)
	{
		___On_TouchStart_4 = value;
		Il2CppCodeGenWriteBarrier(&___On_TouchStart_4, value);
	}

	inline static int32_t get_offset_of_On_TouchDown_5() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TouchDown_5)); }
	inline TouchDownHandler_t16499120 * get_On_TouchDown_5() const { return ___On_TouchDown_5; }
	inline TouchDownHandler_t16499120 ** get_address_of_On_TouchDown_5() { return &___On_TouchDown_5; }
	inline void set_On_TouchDown_5(TouchDownHandler_t16499120 * value)
	{
		___On_TouchDown_5 = value;
		Il2CppCodeGenWriteBarrier(&___On_TouchDown_5, value);
	}

	inline static int32_t get_offset_of_On_TouchUp_6() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TouchUp_6)); }
	inline TouchUpHandler_t3694099103 * get_On_TouchUp_6() const { return ___On_TouchUp_6; }
	inline TouchUpHandler_t3694099103 ** get_address_of_On_TouchUp_6() { return &___On_TouchUp_6; }
	inline void set_On_TouchUp_6(TouchUpHandler_t3694099103 * value)
	{
		___On_TouchUp_6 = value;
		Il2CppCodeGenWriteBarrier(&___On_TouchUp_6, value);
	}

	inline static int32_t get_offset_of_On_SimpleTap_7() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_SimpleTap_7)); }
	inline SimpleTapHandler_t2041759604 * get_On_SimpleTap_7() const { return ___On_SimpleTap_7; }
	inline SimpleTapHandler_t2041759604 ** get_address_of_On_SimpleTap_7() { return &___On_SimpleTap_7; }
	inline void set_On_SimpleTap_7(SimpleTapHandler_t2041759604 * value)
	{
		___On_SimpleTap_7 = value;
		Il2CppCodeGenWriteBarrier(&___On_SimpleTap_7, value);
	}

	inline static int32_t get_offset_of_On_DoubleTap_8() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_DoubleTap_8)); }
	inline DoubleTapHandler_t3910199663 * get_On_DoubleTap_8() const { return ___On_DoubleTap_8; }
	inline DoubleTapHandler_t3910199663 ** get_address_of_On_DoubleTap_8() { return &___On_DoubleTap_8; }
	inline void set_On_DoubleTap_8(DoubleTapHandler_t3910199663 * value)
	{
		___On_DoubleTap_8 = value;
		Il2CppCodeGenWriteBarrier(&___On_DoubleTap_8, value);
	}

	inline static int32_t get_offset_of_On_LongTapStart_9() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_LongTapStart_9)); }
	inline LongTapStartHandler_t4142433656 * get_On_LongTapStart_9() const { return ___On_LongTapStart_9; }
	inline LongTapStartHandler_t4142433656 ** get_address_of_On_LongTapStart_9() { return &___On_LongTapStart_9; }
	inline void set_On_LongTapStart_9(LongTapStartHandler_t4142433656 * value)
	{
		___On_LongTapStart_9 = value;
		Il2CppCodeGenWriteBarrier(&___On_LongTapStart_9, value);
	}

	inline static int32_t get_offset_of_On_LongTap_10() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_LongTap_10)); }
	inline LongTapHandler_t2327584816 * get_On_LongTap_10() const { return ___On_LongTap_10; }
	inline LongTapHandler_t2327584816 ** get_address_of_On_LongTap_10() { return &___On_LongTap_10; }
	inline void set_On_LongTap_10(LongTapHandler_t2327584816 * value)
	{
		___On_LongTap_10 = value;
		Il2CppCodeGenWriteBarrier(&___On_LongTap_10, value);
	}

	inline static int32_t get_offset_of_On_LongTapEnd_11() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_LongTapEnd_11)); }
	inline LongTapEndHandler_t2540206763 * get_On_LongTapEnd_11() const { return ___On_LongTapEnd_11; }
	inline LongTapEndHandler_t2540206763 ** get_address_of_On_LongTapEnd_11() { return &___On_LongTapEnd_11; }
	inline void set_On_LongTapEnd_11(LongTapEndHandler_t2540206763 * value)
	{
		___On_LongTapEnd_11 = value;
		Il2CppCodeGenWriteBarrier(&___On_LongTapEnd_11, value);
	}

	inline static int32_t get_offset_of_On_DragStart_12() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_DragStart_12)); }
	inline DragStartHandler_t1509548139 * get_On_DragStart_12() const { return ___On_DragStart_12; }
	inline DragStartHandler_t1509548139 ** get_address_of_On_DragStart_12() { return &___On_DragStart_12; }
	inline void set_On_DragStart_12(DragStartHandler_t1509548139 * value)
	{
		___On_DragStart_12 = value;
		Il2CppCodeGenWriteBarrier(&___On_DragStart_12, value);
	}

	inline static int32_t get_offset_of_On_Drag_13() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Drag_13)); }
	inline DragHandler_t3855953415 * get_On_Drag_13() const { return ___On_Drag_13; }
	inline DragHandler_t3855953415 ** get_address_of_On_Drag_13() { return &___On_Drag_13; }
	inline void set_On_Drag_13(DragHandler_t3855953415 * value)
	{
		___On_Drag_13 = value;
		Il2CppCodeGenWriteBarrier(&___On_Drag_13, value);
	}

	inline static int32_t get_offset_of_On_DragEnd_14() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_DragEnd_14)); }
	inline DragEndHandler_t2953676952 * get_On_DragEnd_14() const { return ___On_DragEnd_14; }
	inline DragEndHandler_t2953676952 ** get_address_of_On_DragEnd_14() { return &___On_DragEnd_14; }
	inline void set_On_DragEnd_14(DragEndHandler_t2953676952 * value)
	{
		___On_DragEnd_14 = value;
		Il2CppCodeGenWriteBarrier(&___On_DragEnd_14, value);
	}

	inline static int32_t get_offset_of_On_SwipeStart_15() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_SwipeStart_15)); }
	inline SwipeStartHandler_t1813931207 * get_On_SwipeStart_15() const { return ___On_SwipeStart_15; }
	inline SwipeStartHandler_t1813931207 ** get_address_of_On_SwipeStart_15() { return &___On_SwipeStart_15; }
	inline void set_On_SwipeStart_15(SwipeStartHandler_t1813931207 * value)
	{
		___On_SwipeStart_15 = value;
		Il2CppCodeGenWriteBarrier(&___On_SwipeStart_15, value);
	}

	inline static int32_t get_offset_of_On_Swipe_16() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Swipe_16)); }
	inline SwipeHandler_t4041355147 * get_On_Swipe_16() const { return ___On_Swipe_16; }
	inline SwipeHandler_t4041355147 ** get_address_of_On_Swipe_16() { return &___On_Swipe_16; }
	inline void set_On_Swipe_16(SwipeHandler_t4041355147 * value)
	{
		___On_Swipe_16 = value;
		Il2CppCodeGenWriteBarrier(&___On_Swipe_16, value);
	}

	inline static int32_t get_offset_of_On_SwipeEnd_17() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_SwipeEnd_17)); }
	inline SwipeEndHandler_t567777398 * get_On_SwipeEnd_17() const { return ___On_SwipeEnd_17; }
	inline SwipeEndHandler_t567777398 ** get_address_of_On_SwipeEnd_17() { return &___On_SwipeEnd_17; }
	inline void set_On_SwipeEnd_17(SwipeEndHandler_t567777398 * value)
	{
		___On_SwipeEnd_17 = value;
		Il2CppCodeGenWriteBarrier(&___On_SwipeEnd_17, value);
	}

	inline static int32_t get_offset_of_On_TouchStart2Fingers_18() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TouchStart2Fingers_18)); }
	inline TouchStart2FingersHandler_t3096813332 * get_On_TouchStart2Fingers_18() const { return ___On_TouchStart2Fingers_18; }
	inline TouchStart2FingersHandler_t3096813332 ** get_address_of_On_TouchStart2Fingers_18() { return &___On_TouchStart2Fingers_18; }
	inline void set_On_TouchStart2Fingers_18(TouchStart2FingersHandler_t3096813332 * value)
	{
		___On_TouchStart2Fingers_18 = value;
		Il2CppCodeGenWriteBarrier(&___On_TouchStart2Fingers_18, value);
	}

	inline static int32_t get_offset_of_On_TouchDown2Fingers_19() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TouchDown2Fingers_19)); }
	inline TouchDown2FingersHandler_t3403539080 * get_On_TouchDown2Fingers_19() const { return ___On_TouchDown2Fingers_19; }
	inline TouchDown2FingersHandler_t3403539080 ** get_address_of_On_TouchDown2Fingers_19() { return &___On_TouchDown2Fingers_19; }
	inline void set_On_TouchDown2Fingers_19(TouchDown2FingersHandler_t3403539080 * value)
	{
		___On_TouchDown2Fingers_19 = value;
		Il2CppCodeGenWriteBarrier(&___On_TouchDown2Fingers_19, value);
	}

	inline static int32_t get_offset_of_On_TouchUp2Fingers_20() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TouchUp2Fingers_20)); }
	inline TouchUp2FingersHandler_t384986787 * get_On_TouchUp2Fingers_20() const { return ___On_TouchUp2Fingers_20; }
	inline TouchUp2FingersHandler_t384986787 ** get_address_of_On_TouchUp2Fingers_20() { return &___On_TouchUp2Fingers_20; }
	inline void set_On_TouchUp2Fingers_20(TouchUp2FingersHandler_t384986787 * value)
	{
		___On_TouchUp2Fingers_20 = value;
		Il2CppCodeGenWriteBarrier(&___On_TouchUp2Fingers_20, value);
	}

	inline static int32_t get_offset_of_On_SimpleTap2Fingers_21() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_SimpleTap2Fingers_21)); }
	inline SimpleTap2FingersHandler_t3725456324 * get_On_SimpleTap2Fingers_21() const { return ___On_SimpleTap2Fingers_21; }
	inline SimpleTap2FingersHandler_t3725456324 ** get_address_of_On_SimpleTap2Fingers_21() { return &___On_SimpleTap2Fingers_21; }
	inline void set_On_SimpleTap2Fingers_21(SimpleTap2FingersHandler_t3725456324 * value)
	{
		___On_SimpleTap2Fingers_21 = value;
		Il2CppCodeGenWriteBarrier(&___On_SimpleTap2Fingers_21, value);
	}

	inline static int32_t get_offset_of_On_DoubleTap2Fingers_22() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_DoubleTap2Fingers_22)); }
	inline DoubleTap2FingersHandler_t313188403 * get_On_DoubleTap2Fingers_22() const { return ___On_DoubleTap2Fingers_22; }
	inline DoubleTap2FingersHandler_t313188403 ** get_address_of_On_DoubleTap2Fingers_22() { return &___On_DoubleTap2Fingers_22; }
	inline void set_On_DoubleTap2Fingers_22(DoubleTap2FingersHandler_t313188403 * value)
	{
		___On_DoubleTap2Fingers_22 = value;
		Il2CppCodeGenWriteBarrier(&___On_DoubleTap2Fingers_22, value);
	}

	inline static int32_t get_offset_of_On_LongTapStart2Fingers_23() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_LongTapStart2Fingers_23)); }
	inline LongTapStart2FingersHandler_t59253952 * get_On_LongTapStart2Fingers_23() const { return ___On_LongTapStart2Fingers_23; }
	inline LongTapStart2FingersHandler_t59253952 ** get_address_of_On_LongTapStart2Fingers_23() { return &___On_LongTapStart2Fingers_23; }
	inline void set_On_LongTapStart2Fingers_23(LongTapStart2FingersHandler_t59253952 * value)
	{
		___On_LongTapStart2Fingers_23 = value;
		Il2CppCodeGenWriteBarrier(&___On_LongTapStart2Fingers_23, value);
	}

	inline static int32_t get_offset_of_On_LongTap2Fingers_24() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_LongTap2Fingers_24)); }
	inline LongTap2FingersHandler_t2742766376 * get_On_LongTap2Fingers_24() const { return ___On_LongTap2Fingers_24; }
	inline LongTap2FingersHandler_t2742766376 ** get_address_of_On_LongTap2Fingers_24() { return &___On_LongTap2Fingers_24; }
	inline void set_On_LongTap2Fingers_24(LongTap2FingersHandler_t2742766376 * value)
	{
		___On_LongTap2Fingers_24 = value;
		Il2CppCodeGenWriteBarrier(&___On_LongTap2Fingers_24, value);
	}

	inline static int32_t get_offset_of_On_LongTapEnd2Fingers_25() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_LongTapEnd2Fingers_25)); }
	inline LongTapEnd2FingersHandler_t3611206799 * get_On_LongTapEnd2Fingers_25() const { return ___On_LongTapEnd2Fingers_25; }
	inline LongTapEnd2FingersHandler_t3611206799 ** get_address_of_On_LongTapEnd2Fingers_25() { return &___On_LongTapEnd2Fingers_25; }
	inline void set_On_LongTapEnd2Fingers_25(LongTapEnd2FingersHandler_t3611206799 * value)
	{
		___On_LongTapEnd2Fingers_25 = value;
		Il2CppCodeGenWriteBarrier(&___On_LongTapEnd2Fingers_25, value);
	}

	inline static int32_t get_offset_of_On_Twist_26() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Twist_26)); }
	inline TwistHandler_t117269532 * get_On_Twist_26() const { return ___On_Twist_26; }
	inline TwistHandler_t117269532 ** get_address_of_On_Twist_26() { return &___On_Twist_26; }
	inline void set_On_Twist_26(TwistHandler_t117269532 * value)
	{
		___On_Twist_26 = value;
		Il2CppCodeGenWriteBarrier(&___On_Twist_26, value);
	}

	inline static int32_t get_offset_of_On_TwistEnd_27() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_TwistEnd_27)); }
	inline TwistEndHandler_t1042340097 * get_On_TwistEnd_27() const { return ___On_TwistEnd_27; }
	inline TwistEndHandler_t1042340097 ** get_address_of_On_TwistEnd_27() { return &___On_TwistEnd_27; }
	inline void set_On_TwistEnd_27(TwistEndHandler_t1042340097 * value)
	{
		___On_TwistEnd_27 = value;
		Il2CppCodeGenWriteBarrier(&___On_TwistEnd_27, value);
	}

	inline static int32_t get_offset_of_On_Pinch_28() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Pinch_28)); }
	inline PinchHandler_t3939026435 * get_On_Pinch_28() const { return ___On_Pinch_28; }
	inline PinchHandler_t3939026435 ** get_address_of_On_Pinch_28() { return &___On_Pinch_28; }
	inline void set_On_Pinch_28(PinchHandler_t3939026435 * value)
	{
		___On_Pinch_28 = value;
		Il2CppCodeGenWriteBarrier(&___On_Pinch_28, value);
	}

	inline static int32_t get_offset_of_On_PinchIn_29() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_PinchIn_29)); }
	inline PinchInHandler_t2898198212 * get_On_PinchIn_29() const { return ___On_PinchIn_29; }
	inline PinchInHandler_t2898198212 ** get_address_of_On_PinchIn_29() { return &___On_PinchIn_29; }
	inline void set_On_PinchIn_29(PinchInHandler_t2898198212 * value)
	{
		___On_PinchIn_29 = value;
		Il2CppCodeGenWriteBarrier(&___On_PinchIn_29, value);
	}

	inline static int32_t get_offset_of_On_PinchOut_30() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_PinchOut_30)); }
	inline PinchOutHandler_t3267676377 * get_On_PinchOut_30() const { return ___On_PinchOut_30; }
	inline PinchOutHandler_t3267676377 ** get_address_of_On_PinchOut_30() { return &___On_PinchOut_30; }
	inline void set_On_PinchOut_30(PinchOutHandler_t3267676377 * value)
	{
		___On_PinchOut_30 = value;
		Il2CppCodeGenWriteBarrier(&___On_PinchOut_30, value);
	}

	inline static int32_t get_offset_of_On_PinchEnd_31() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_PinchEnd_31)); }
	inline PinchEndHandler_t2735783548 * get_On_PinchEnd_31() const { return ___On_PinchEnd_31; }
	inline PinchEndHandler_t2735783548 ** get_address_of_On_PinchEnd_31() { return &___On_PinchEnd_31; }
	inline void set_On_PinchEnd_31(PinchEndHandler_t2735783548 * value)
	{
		___On_PinchEnd_31 = value;
		Il2CppCodeGenWriteBarrier(&___On_PinchEnd_31, value);
	}

	inline static int32_t get_offset_of_On_DragStart2Fingers_32() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_DragStart2Fingers_32)); }
	inline DragStart2FingersHandler_t1996108375 * get_On_DragStart2Fingers_32() const { return ___On_DragStart2Fingers_32; }
	inline DragStart2FingersHandler_t1996108375 ** get_address_of_On_DragStart2Fingers_32() { return &___On_DragStart2Fingers_32; }
	inline void set_On_DragStart2Fingers_32(DragStart2FingersHandler_t1996108375 * value)
	{
		___On_DragStart2Fingers_32 = value;
		Il2CppCodeGenWriteBarrier(&___On_DragStart2Fingers_32, value);
	}

	inline static int32_t get_offset_of_On_Drag2Fingers_33() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Drag2Fingers_33)); }
	inline Drag2FingersHandler_t599932411 * get_On_Drag2Fingers_33() const { return ___On_Drag2Fingers_33; }
	inline Drag2FingersHandler_t599932411 ** get_address_of_On_Drag2Fingers_33() { return &___On_Drag2Fingers_33; }
	inline void set_On_Drag2Fingers_33(Drag2FingersHandler_t599932411 * value)
	{
		___On_Drag2Fingers_33 = value;
		Il2CppCodeGenWriteBarrier(&___On_Drag2Fingers_33, value);
	}

	inline static int32_t get_offset_of_On_DragEnd2Fingers_34() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_DragEnd2Fingers_34)); }
	inline DragEnd2FingersHandler_t1069006048 * get_On_DragEnd2Fingers_34() const { return ___On_DragEnd2Fingers_34; }
	inline DragEnd2FingersHandler_t1069006048 ** get_address_of_On_DragEnd2Fingers_34() { return &___On_DragEnd2Fingers_34; }
	inline void set_On_DragEnd2Fingers_34(DragEnd2FingersHandler_t1069006048 * value)
	{
		___On_DragEnd2Fingers_34 = value;
		Il2CppCodeGenWriteBarrier(&___On_DragEnd2Fingers_34, value);
	}

	inline static int32_t get_offset_of_On_SwipeStart2Fingers_35() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_SwipeStart2Fingers_35)); }
	inline SwipeStart2FingersHandler_t3085603867 * get_On_SwipeStart2Fingers_35() const { return ___On_SwipeStart2Fingers_35; }
	inline SwipeStart2FingersHandler_t3085603867 ** get_address_of_On_SwipeStart2Fingers_35() { return &___On_SwipeStart2Fingers_35; }
	inline void set_On_SwipeStart2Fingers_35(SwipeStart2FingersHandler_t3085603867 * value)
	{
		___On_SwipeStart2Fingers_35 = value;
		Il2CppCodeGenWriteBarrier(&___On_SwipeStart2Fingers_35, value);
	}

	inline static int32_t get_offset_of_On_Swipe2Fingers_36() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_Swipe2Fingers_36)); }
	inline Swipe2FingersHandler_t2386328279 * get_On_Swipe2Fingers_36() const { return ___On_Swipe2Fingers_36; }
	inline Swipe2FingersHandler_t2386328279 ** get_address_of_On_Swipe2Fingers_36() { return &___On_Swipe2Fingers_36; }
	inline void set_On_Swipe2Fingers_36(Swipe2FingersHandler_t2386328279 * value)
	{
		___On_Swipe2Fingers_36 = value;
		Il2CppCodeGenWriteBarrier(&___On_Swipe2Fingers_36, value);
	}

	inline static int32_t get_offset_of_On_SwipeEnd2Fingers_37() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_SwipeEnd2Fingers_37)); }
	inline SwipeEnd2FingersHandler_t2863096110 * get_On_SwipeEnd2Fingers_37() const { return ___On_SwipeEnd2Fingers_37; }
	inline SwipeEnd2FingersHandler_t2863096110 ** get_address_of_On_SwipeEnd2Fingers_37() { return &___On_SwipeEnd2Fingers_37; }
	inline void set_On_SwipeEnd2Fingers_37(SwipeEnd2FingersHandler_t2863096110 * value)
	{
		___On_SwipeEnd2Fingers_37 = value;
		Il2CppCodeGenWriteBarrier(&___On_SwipeEnd2Fingers_37, value);
	}

	inline static int32_t get_offset_of_On_EasyTouchIsReady_38() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_EasyTouchIsReady_38)); }
	inline EasyTouchIsReadyHandler_t970770497 * get_On_EasyTouchIsReady_38() const { return ___On_EasyTouchIsReady_38; }
	inline EasyTouchIsReadyHandler_t970770497 ** get_address_of_On_EasyTouchIsReady_38() { return &___On_EasyTouchIsReady_38; }
	inline void set_On_EasyTouchIsReady_38(EasyTouchIsReadyHandler_t970770497 * value)
	{
		___On_EasyTouchIsReady_38 = value;
		Il2CppCodeGenWriteBarrier(&___On_EasyTouchIsReady_38, value);
	}

	inline static int32_t get_offset_of_On_OverUIElement_39() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_OverUIElement_39)); }
	inline OverUIElementHandler_t4133021939 * get_On_OverUIElement_39() const { return ___On_OverUIElement_39; }
	inline OverUIElementHandler_t4133021939 ** get_address_of_On_OverUIElement_39() { return &___On_OverUIElement_39; }
	inline void set_On_OverUIElement_39(OverUIElementHandler_t4133021939 * value)
	{
		___On_OverUIElement_39 = value;
		Il2CppCodeGenWriteBarrier(&___On_OverUIElement_39, value);
	}

	inline static int32_t get_offset_of_On_UIElementTouchUp_40() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___On_UIElementTouchUp_40)); }
	inline UIElementTouchUpHandler_t3300966889 * get_On_UIElementTouchUp_40() const { return ___On_UIElementTouchUp_40; }
	inline UIElementTouchUpHandler_t3300966889 ** get_address_of_On_UIElementTouchUp_40() { return &___On_UIElementTouchUp_40; }
	inline void set_On_UIElementTouchUp_40(UIElementTouchUpHandler_t3300966889 * value)
	{
		___On_UIElementTouchUp_40 = value;
		Il2CppCodeGenWriteBarrier(&___On_UIElementTouchUp_40, value);
	}

	inline static int32_t get_offset_of__instance_41() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ____instance_41)); }
	inline EasyTouch_t3578534067 * get__instance_41() const { return ____instance_41; }
	inline EasyTouch_t3578534067 ** get_address_of__instance_41() { return &____instance_41; }
	inline void set__instance_41(EasyTouch_t3578534067 * value)
	{
		____instance_41 = value;
		Il2CppCodeGenWriteBarrier(&____instance_41, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_91() { return static_cast<int32_t>(offsetof(EasyTouch_t3578534067_StaticFields, ___U3CU3Ef__amU24cache0_91)); }
	inline Predicate_1_t2037842105 * get_U3CU3Ef__amU24cache0_91() const { return ___U3CU3Ef__amU24cache0_91; }
	inline Predicate_1_t2037842105 ** get_address_of_U3CU3Ef__amU24cache0_91() { return &___U3CU3Ef__amU24cache0_91; }
	inline void set_U3CU3Ef__amU24cache0_91(Predicate_1_t2037842105 * value)
	{
		___U3CU3Ef__amU24cache0_91 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_91, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
