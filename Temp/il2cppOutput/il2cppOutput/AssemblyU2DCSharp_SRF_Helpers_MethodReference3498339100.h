﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Helpers.MethodReference
struct  MethodReference_t3498339100  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo SRF.Helpers.MethodReference::_method
	MethodInfo_t * ____method_0;
	// System.Object SRF.Helpers.MethodReference::_target
	Il2CppObject * ____target_1;

public:
	inline static int32_t get_offset_of__method_0() { return static_cast<int32_t>(offsetof(MethodReference_t3498339100, ____method_0)); }
	inline MethodInfo_t * get__method_0() const { return ____method_0; }
	inline MethodInfo_t ** get_address_of__method_0() { return &____method_0; }
	inline void set__method_0(MethodInfo_t * value)
	{
		____method_0 = value;
		Il2CppCodeGenWriteBarrier(&____method_0, value);
	}

	inline static int32_t get_offset_of__target_1() { return static_cast<int32_t>(offsetof(MethodReference_t3498339100, ____target_1)); }
	inline Il2CppObject * get__target_1() const { return ____target_1; }
	inline Il2CppObject ** get_address_of__target_1() { return &____target_1; }
	inline void set__target_1(Il2CppObject * value)
	{
		____target_1 = value;
		Il2CppCodeGenWriteBarrier(&____target_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
