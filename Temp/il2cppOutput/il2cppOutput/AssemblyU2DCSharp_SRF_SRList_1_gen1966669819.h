﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.UI.Other.SRTab[]
struct SRTabU5BU5D_t2683468643;
// System.Collections.Generic.EqualityComparer`1<SRDebugger.UI.Other.SRTab>
struct EqualityComparer_1_t547674509;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SRDebugger.UI.Other.SRTab>
struct ReadOnlyCollection_1_t2159824930;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRList`1<SRDebugger.UI.Other.SRTab>
struct  SRList_1_t1966669819  : public Il2CppObject
{
public:
	// T[] SRF.SRList`1::_buffer
	SRTabU5BU5D_t2683468643* ____buffer_0;
	// System.Int32 SRF.SRList`1::_count
	int32_t ____count_1;
	// System.Collections.Generic.EqualityComparer`1<T> SRF.SRList`1::_equalityComparer
	EqualityComparer_1_t547674509 * ____equalityComparer_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<T> SRF.SRList`1::_readOnlyWrapper
	ReadOnlyCollection_1_t2159824930 * ____readOnlyWrapper_3;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(SRList_1_t1966669819, ____buffer_0)); }
	inline SRTabU5BU5D_t2683468643* get__buffer_0() const { return ____buffer_0; }
	inline SRTabU5BU5D_t2683468643** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(SRTabU5BU5D_t2683468643* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_0, value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(SRList_1_t1966669819, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__equalityComparer_2() { return static_cast<int32_t>(offsetof(SRList_1_t1966669819, ____equalityComparer_2)); }
	inline EqualityComparer_1_t547674509 * get__equalityComparer_2() const { return ____equalityComparer_2; }
	inline EqualityComparer_1_t547674509 ** get_address_of__equalityComparer_2() { return &____equalityComparer_2; }
	inline void set__equalityComparer_2(EqualityComparer_1_t547674509 * value)
	{
		____equalityComparer_2 = value;
		Il2CppCodeGenWriteBarrier(&____equalityComparer_2, value);
	}

	inline static int32_t get_offset_of__readOnlyWrapper_3() { return static_cast<int32_t>(offsetof(SRList_1_t1966669819, ____readOnlyWrapper_3)); }
	inline ReadOnlyCollection_1_t2159824930 * get__readOnlyWrapper_3() const { return ____readOnlyWrapper_3; }
	inline ReadOnlyCollection_1_t2159824930 ** get_address_of__readOnlyWrapper_3() { return &____readOnlyWrapper_3; }
	inline void set__readOnlyWrapper_3(ReadOnlyCollection_1_t2159824930 * value)
	{
		____readOnlyWrapper_3 = value;
		Il2CppCodeGenWriteBarrier(&____readOnlyWrapper_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
