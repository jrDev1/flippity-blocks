﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRMonoBehaviourEx/FieldInfo
struct  FieldInfo_t2603195560 
{
public:
	// System.Boolean SRF.SRMonoBehaviourEx/FieldInfo::AutoCreate
	bool ___AutoCreate_0;
	// System.Boolean SRF.SRMonoBehaviourEx/FieldInfo::AutoSet
	bool ___AutoSet_1;
	// System.Reflection.FieldInfo SRF.SRMonoBehaviourEx/FieldInfo::Field
	FieldInfo_t * ___Field_2;
	// System.Boolean SRF.SRMonoBehaviourEx/FieldInfo::Import
	bool ___Import_3;
	// System.Type SRF.SRMonoBehaviourEx/FieldInfo::ImportType
	Type_t * ___ImportType_4;

public:
	inline static int32_t get_offset_of_AutoCreate_0() { return static_cast<int32_t>(offsetof(FieldInfo_t2603195560, ___AutoCreate_0)); }
	inline bool get_AutoCreate_0() const { return ___AutoCreate_0; }
	inline bool* get_address_of_AutoCreate_0() { return &___AutoCreate_0; }
	inline void set_AutoCreate_0(bool value)
	{
		___AutoCreate_0 = value;
	}

	inline static int32_t get_offset_of_AutoSet_1() { return static_cast<int32_t>(offsetof(FieldInfo_t2603195560, ___AutoSet_1)); }
	inline bool get_AutoSet_1() const { return ___AutoSet_1; }
	inline bool* get_address_of_AutoSet_1() { return &___AutoSet_1; }
	inline void set_AutoSet_1(bool value)
	{
		___AutoSet_1 = value;
	}

	inline static int32_t get_offset_of_Field_2() { return static_cast<int32_t>(offsetof(FieldInfo_t2603195560, ___Field_2)); }
	inline FieldInfo_t * get_Field_2() const { return ___Field_2; }
	inline FieldInfo_t ** get_address_of_Field_2() { return &___Field_2; }
	inline void set_Field_2(FieldInfo_t * value)
	{
		___Field_2 = value;
		Il2CppCodeGenWriteBarrier(&___Field_2, value);
	}

	inline static int32_t get_offset_of_Import_3() { return static_cast<int32_t>(offsetof(FieldInfo_t2603195560, ___Import_3)); }
	inline bool get_Import_3() const { return ___Import_3; }
	inline bool* get_address_of_Import_3() { return &___Import_3; }
	inline void set_Import_3(bool value)
	{
		___Import_3 = value;
	}

	inline static int32_t get_offset_of_ImportType_4() { return static_cast<int32_t>(offsetof(FieldInfo_t2603195560, ___ImportType_4)); }
	inline Type_t * get_ImportType_4() const { return ___ImportType_4; }
	inline Type_t ** get_address_of_ImportType_4() { return &___ImportType_4; }
	inline void set_ImportType_4(Type_t * value)
	{
		___ImportType_4 = value;
		Il2CppCodeGenWriteBarrier(&___ImportType_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SRF.SRMonoBehaviourEx/FieldInfo
struct FieldInfo_t2603195560_marshaled_pinvoke
{
	int32_t ___AutoCreate_0;
	int32_t ___AutoSet_1;
	FieldInfo_t * ___Field_2;
	int32_t ___Import_3;
	Type_t * ___ImportType_4;
};
// Native definition for COM marshalling of SRF.SRMonoBehaviourEx/FieldInfo
struct FieldInfo_t2603195560_marshaled_com
{
	int32_t ___AutoCreate_0;
	int32_t ___AutoSet_1;
	FieldInfo_t * ___Field_2;
	int32_t ___Import_3;
	Type_t * ___ImportType_4;
};
