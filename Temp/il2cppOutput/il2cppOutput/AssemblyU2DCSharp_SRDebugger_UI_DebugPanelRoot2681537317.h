﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// SRDebugger.Scripts.DebuggerTabController
struct DebuggerTabController_t1358735436;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.DebugPanelRoot
struct  DebugPanelRoot_t2681537317  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.Canvas SRDebugger.UI.DebugPanelRoot::Canvas
	Canvas_t209405766 * ___Canvas_9;
	// UnityEngine.CanvasGroup SRDebugger.UI.DebugPanelRoot::CanvasGroup
	CanvasGroup_t3296560743 * ___CanvasGroup_10;
	// SRDebugger.Scripts.DebuggerTabController SRDebugger.UI.DebugPanelRoot::TabController
	DebuggerTabController_t1358735436 * ___TabController_11;

public:
	inline static int32_t get_offset_of_Canvas_9() { return static_cast<int32_t>(offsetof(DebugPanelRoot_t2681537317, ___Canvas_9)); }
	inline Canvas_t209405766 * get_Canvas_9() const { return ___Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_Canvas_9() { return &___Canvas_9; }
	inline void set_Canvas_9(Canvas_t209405766 * value)
	{
		___Canvas_9 = value;
		Il2CppCodeGenWriteBarrier(&___Canvas_9, value);
	}

	inline static int32_t get_offset_of_CanvasGroup_10() { return static_cast<int32_t>(offsetof(DebugPanelRoot_t2681537317, ___CanvasGroup_10)); }
	inline CanvasGroup_t3296560743 * get_CanvasGroup_10() const { return ___CanvasGroup_10; }
	inline CanvasGroup_t3296560743 ** get_address_of_CanvasGroup_10() { return &___CanvasGroup_10; }
	inline void set_CanvasGroup_10(CanvasGroup_t3296560743 * value)
	{
		___CanvasGroup_10 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasGroup_10, value);
	}

	inline static int32_t get_offset_of_TabController_11() { return static_cast<int32_t>(offsetof(DebugPanelRoot_t2681537317, ___TabController_11)); }
	inline DebuggerTabController_t1358735436 * get_TabController_11() const { return ___TabController_11; }
	inline DebuggerTabController_t1358735436 ** get_address_of_TabController_11() { return &___TabController_11; }
	inline void set_TabController_11(DebuggerTabController_t1358735436 * value)
	{
		___TabController_11 = value;
		Il2CppCodeGenWriteBarrier(&___TabController_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
