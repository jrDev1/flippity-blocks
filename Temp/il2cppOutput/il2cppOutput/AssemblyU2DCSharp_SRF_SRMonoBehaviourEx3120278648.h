﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IList`1<SRF.SRMonoBehaviourEx/FieldInfo>>
struct Dictionary_2_t786526762;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRMonoBehaviourEx
struct  SRMonoBehaviourEx_t3120278648  : public SRMonoBehaviour_t2352136145
{
public:

public:
};

struct SRMonoBehaviourEx_t3120278648_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.IList`1<SRF.SRMonoBehaviourEx/FieldInfo>> SRF.SRMonoBehaviourEx::_checkedFields
	Dictionary_2_t786526762 * ____checkedFields_8;

public:
	inline static int32_t get_offset_of__checkedFields_8() { return static_cast<int32_t>(offsetof(SRMonoBehaviourEx_t3120278648_StaticFields, ____checkedFields_8)); }
	inline Dictionary_2_t786526762 * get__checkedFields_8() const { return ____checkedFields_8; }
	inline Dictionary_2_t786526762 ** get_address_of__checkedFields_8() { return &____checkedFields_8; }
	inline void set__checkedFields_8(Dictionary_2_t786526762 * value)
	{
		____checkedFields_8 = value;
		Il2CppCodeGenWriteBarrier(&____checkedFields_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
