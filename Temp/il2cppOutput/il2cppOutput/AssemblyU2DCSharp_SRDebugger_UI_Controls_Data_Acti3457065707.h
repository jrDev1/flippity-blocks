﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_OptionsCo3483301704.h"

// SRF.Helpers.MethodReference
struct MethodReference_t3498339100;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.ActionControl
struct  ActionControl_t3457065707  : public OptionsControlBase_t3483301704
{
public:
	// SRF.Helpers.MethodReference SRDebugger.UI.Controls.Data.ActionControl::_method
	MethodReference_t3498339100 * ____method_12;
	// UnityEngine.UI.Button SRDebugger.UI.Controls.Data.ActionControl::Button
	Button_t2872111280 * ___Button_13;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.ActionControl::Title
	Text_t356221433 * ___Title_14;

public:
	inline static int32_t get_offset_of__method_12() { return static_cast<int32_t>(offsetof(ActionControl_t3457065707, ____method_12)); }
	inline MethodReference_t3498339100 * get__method_12() const { return ____method_12; }
	inline MethodReference_t3498339100 ** get_address_of__method_12() { return &____method_12; }
	inline void set__method_12(MethodReference_t3498339100 * value)
	{
		____method_12 = value;
		Il2CppCodeGenWriteBarrier(&____method_12, value);
	}

	inline static int32_t get_offset_of_Button_13() { return static_cast<int32_t>(offsetof(ActionControl_t3457065707, ___Button_13)); }
	inline Button_t2872111280 * get_Button_13() const { return ___Button_13; }
	inline Button_t2872111280 ** get_address_of_Button_13() { return &___Button_13; }
	inline void set_Button_13(Button_t2872111280 * value)
	{
		___Button_13 = value;
		Il2CppCodeGenWriteBarrier(&___Button_13, value);
	}

	inline static int32_t get_offset_of_Title_14() { return static_cast<int32_t>(offsetof(ActionControl_t3457065707, ___Title_14)); }
	inline Text_t356221433 * get_Title_14() const { return ___Title_14; }
	inline Text_t356221433 ** get_address_of_Title_14() { return &___Title_14; }
	inline void set_Title_14(Text_t356221433 * value)
	{
		___Title_14 = value;
		Il2CppCodeGenWriteBarrier(&___Title_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
