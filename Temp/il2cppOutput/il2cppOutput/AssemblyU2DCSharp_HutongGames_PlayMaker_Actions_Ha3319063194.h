﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue
struct  HashTableGetKeyFromValue_t3319063194  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::theValue
	FsmVar_t2872592513 * ___theValue_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::result
	FsmString_t2414474701 * ___result_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::KeyFoundEvent
	FsmEvent_t1258573736 * ___KeyFoundEvent_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableGetKeyFromValue::KeyNotFoundEvent
	FsmEvent_t1258573736 * ___KeyNotFoundEvent_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableGetKeyFromValue_t3319063194, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableGetKeyFromValue_t3319063194, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_theValue_14() { return static_cast<int32_t>(offsetof(HashTableGetKeyFromValue_t3319063194, ___theValue_14)); }
	inline FsmVar_t2872592513 * get_theValue_14() const { return ___theValue_14; }
	inline FsmVar_t2872592513 ** get_address_of_theValue_14() { return &___theValue_14; }
	inline void set_theValue_14(FsmVar_t2872592513 * value)
	{
		___theValue_14 = value;
		Il2CppCodeGenWriteBarrier(&___theValue_14, value);
	}

	inline static int32_t get_offset_of_result_15() { return static_cast<int32_t>(offsetof(HashTableGetKeyFromValue_t3319063194, ___result_15)); }
	inline FsmString_t2414474701 * get_result_15() const { return ___result_15; }
	inline FsmString_t2414474701 ** get_address_of_result_15() { return &___result_15; }
	inline void set_result_15(FsmString_t2414474701 * value)
	{
		___result_15 = value;
		Il2CppCodeGenWriteBarrier(&___result_15, value);
	}

	inline static int32_t get_offset_of_KeyFoundEvent_16() { return static_cast<int32_t>(offsetof(HashTableGetKeyFromValue_t3319063194, ___KeyFoundEvent_16)); }
	inline FsmEvent_t1258573736 * get_KeyFoundEvent_16() const { return ___KeyFoundEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_KeyFoundEvent_16() { return &___KeyFoundEvent_16; }
	inline void set_KeyFoundEvent_16(FsmEvent_t1258573736 * value)
	{
		___KeyFoundEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___KeyFoundEvent_16, value);
	}

	inline static int32_t get_offset_of_KeyNotFoundEvent_17() { return static_cast<int32_t>(offsetof(HashTableGetKeyFromValue_t3319063194, ___KeyNotFoundEvent_17)); }
	inline FsmEvent_t1258573736 * get_KeyNotFoundEvent_17() const { return ___KeyNotFoundEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_KeyNotFoundEvent_17() { return &___KeyNotFoundEvent_17; }
	inline void set_KeyNotFoundEvent_17(FsmEvent_t1258573736 * value)
	{
		___KeyNotFoundEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___KeyNotFoundEvent_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
