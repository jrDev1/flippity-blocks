﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1393552581.h"

// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchQuickFSM
struct  EasyTouchQuickFSM_t1629559380  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.Actions.EasyTouchQuickFSM/GameObjectType HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::realType
	int32_t ___realType_11;
	// System.Int32 HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::fingerIndex
	int32_t ___fingerIndex_12;
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::enablePickOverUI
	bool ___enablePickOverUI_13;
	// HedgehogTeam.EasyTouch.Gesture HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::currentGesture
	Gesture_t367995397 * ___currentGesture_14;
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::twoFingerGesture
	bool ___twoFingerGesture_15;
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::allowMultiTouch
	bool ___allowMultiTouch_16;
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::allowOverUIElement
	bool ___allowOverUIElement_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EasyTouchQuickFSM::sendEvent
	FsmEvent_t1258573736 * ___sendEvent_18;

public:
	inline static int32_t get_offset_of_realType_11() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___realType_11)); }
	inline int32_t get_realType_11() const { return ___realType_11; }
	inline int32_t* get_address_of_realType_11() { return &___realType_11; }
	inline void set_realType_11(int32_t value)
	{
		___realType_11 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_12() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___fingerIndex_12)); }
	inline int32_t get_fingerIndex_12() const { return ___fingerIndex_12; }
	inline int32_t* get_address_of_fingerIndex_12() { return &___fingerIndex_12; }
	inline void set_fingerIndex_12(int32_t value)
	{
		___fingerIndex_12 = value;
	}

	inline static int32_t get_offset_of_enablePickOverUI_13() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___enablePickOverUI_13)); }
	inline bool get_enablePickOverUI_13() const { return ___enablePickOverUI_13; }
	inline bool* get_address_of_enablePickOverUI_13() { return &___enablePickOverUI_13; }
	inline void set_enablePickOverUI_13(bool value)
	{
		___enablePickOverUI_13 = value;
	}

	inline static int32_t get_offset_of_currentGesture_14() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___currentGesture_14)); }
	inline Gesture_t367995397 * get_currentGesture_14() const { return ___currentGesture_14; }
	inline Gesture_t367995397 ** get_address_of_currentGesture_14() { return &___currentGesture_14; }
	inline void set_currentGesture_14(Gesture_t367995397 * value)
	{
		___currentGesture_14 = value;
		Il2CppCodeGenWriteBarrier(&___currentGesture_14, value);
	}

	inline static int32_t get_offset_of_twoFingerGesture_15() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___twoFingerGesture_15)); }
	inline bool get_twoFingerGesture_15() const { return ___twoFingerGesture_15; }
	inline bool* get_address_of_twoFingerGesture_15() { return &___twoFingerGesture_15; }
	inline void set_twoFingerGesture_15(bool value)
	{
		___twoFingerGesture_15 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_16() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___allowMultiTouch_16)); }
	inline bool get_allowMultiTouch_16() const { return ___allowMultiTouch_16; }
	inline bool* get_address_of_allowMultiTouch_16() { return &___allowMultiTouch_16; }
	inline void set_allowMultiTouch_16(bool value)
	{
		___allowMultiTouch_16 = value;
	}

	inline static int32_t get_offset_of_allowOverUIElement_17() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___allowOverUIElement_17)); }
	inline bool get_allowOverUIElement_17() const { return ___allowOverUIElement_17; }
	inline bool* get_address_of_allowOverUIElement_17() { return &___allowOverUIElement_17; }
	inline void set_allowOverUIElement_17(bool value)
	{
		___allowOverUIElement_17 = value;
	}

	inline static int32_t get_offset_of_sendEvent_18() { return static_cast<int32_t>(offsetof(EasyTouchQuickFSM_t1629559380, ___sendEvent_18)); }
	inline FsmEvent_t1258573736 * get_sendEvent_18() const { return ___sendEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_sendEvent_18() { return &___sendEvent_18; }
	inline void set_sendEvent_18(FsmEvent_t1258573736 * value)
	{
		___sendEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___sendEvent_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
