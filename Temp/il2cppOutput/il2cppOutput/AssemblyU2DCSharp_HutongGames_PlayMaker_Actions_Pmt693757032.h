﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists
struct  PmtCheckIfPrefabPoolExists_t693757032  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists::prefab
	FsmGameObject_t3097142863 * ___prefab_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists::alreadyExistsEvent
	FsmEvent_t1258573736 * ___alreadyExistsEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists::doesntExistsEvent
	FsmEvent_t1258573736 * ___doesntExistsEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_15;
	// PathologicalGames.SpawnPool HutongGames.PlayMaker.Actions.PmtCheckIfPrefabPoolExists::_pool
	SpawnPool_t2419717525 * ____pool_16;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtCheckIfPrefabPoolExists_t693757032, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_prefab_12() { return static_cast<int32_t>(offsetof(PmtCheckIfPrefabPoolExists_t693757032, ___prefab_12)); }
	inline FsmGameObject_t3097142863 * get_prefab_12() const { return ___prefab_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_prefab_12() { return &___prefab_12; }
	inline void set_prefab_12(FsmGameObject_t3097142863 * value)
	{
		___prefab_12 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_12, value);
	}

	inline static int32_t get_offset_of_alreadyExistsEvent_13() { return static_cast<int32_t>(offsetof(PmtCheckIfPrefabPoolExists_t693757032, ___alreadyExistsEvent_13)); }
	inline FsmEvent_t1258573736 * get_alreadyExistsEvent_13() const { return ___alreadyExistsEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_alreadyExistsEvent_13() { return &___alreadyExistsEvent_13; }
	inline void set_alreadyExistsEvent_13(FsmEvent_t1258573736 * value)
	{
		___alreadyExistsEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___alreadyExistsEvent_13, value);
	}

	inline static int32_t get_offset_of_doesntExistsEvent_14() { return static_cast<int32_t>(offsetof(PmtCheckIfPrefabPoolExists_t693757032, ___doesntExistsEvent_14)); }
	inline FsmEvent_t1258573736 * get_doesntExistsEvent_14() const { return ___doesntExistsEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_doesntExistsEvent_14() { return &___doesntExistsEvent_14; }
	inline void set_doesntExistsEvent_14(FsmEvent_t1258573736 * value)
	{
		___doesntExistsEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___doesntExistsEvent_14, value);
	}

	inline static int32_t get_offset_of_failureEvent_15() { return static_cast<int32_t>(offsetof(PmtCheckIfPrefabPoolExists_t693757032, ___failureEvent_15)); }
	inline FsmEvent_t1258573736 * get_failureEvent_15() const { return ___failureEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_15() { return &___failureEvent_15; }
	inline void set_failureEvent_15(FsmEvent_t1258573736 * value)
	{
		___failureEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_15, value);
	}

	inline static int32_t get_offset_of__pool_16() { return static_cast<int32_t>(offsetof(PmtCheckIfPrefabPoolExists_t693757032, ____pool_16)); }
	inline SpawnPool_t2419717525 * get__pool_16() const { return ____pool_16; }
	inline SpawnPool_t2419717525 ** get_address_of__pool_16() { return &____pool_16; }
	inline void set__pool_16(SpawnPool_t2419717525 * value)
	{
		____pool_16 = value;
		Il2CppCodeGenWriteBarrier(&____pool_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
