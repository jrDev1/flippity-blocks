﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Components_SRAutoSingleton_14224086282.h"

// SRF.SRList`1<SRF.Service.SRServiceManager/Service>
struct SRList_1_t4015835886;
// System.Collections.Generic.List`1<SRF.Service.SRServiceManager/ServiceStub>
struct List_1_t4040584125;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRServiceManager
struct  SRServiceManager_t2267823955  : public SRAutoSingleton_1_t4224086282
{
public:
	// SRF.SRList`1<SRF.Service.SRServiceManager/Service> SRF.Service.SRServiceManager::_services
	SRList_1_t4015835886 * ____services_11;
	// System.Collections.Generic.List`1<SRF.Service.SRServiceManager/ServiceStub> SRF.Service.SRServiceManager::_serviceStubs
	List_1_t4040584125 * ____serviceStubs_12;

public:
	inline static int32_t get_offset_of__services_11() { return static_cast<int32_t>(offsetof(SRServiceManager_t2267823955, ____services_11)); }
	inline SRList_1_t4015835886 * get__services_11() const { return ____services_11; }
	inline SRList_1_t4015835886 ** get_address_of__services_11() { return &____services_11; }
	inline void set__services_11(SRList_1_t4015835886 * value)
	{
		____services_11 = value;
		Il2CppCodeGenWriteBarrier(&____services_11, value);
	}

	inline static int32_t get_offset_of__serviceStubs_12() { return static_cast<int32_t>(offsetof(SRServiceManager_t2267823955, ____serviceStubs_12)); }
	inline List_1_t4040584125 * get__serviceStubs_12() const { return ____serviceStubs_12; }
	inline List_1_t4040584125 ** get_address_of__serviceStubs_12() { return &____serviceStubs_12; }
	inline void set__serviceStubs_12(List_1_t4040584125 * value)
	{
		____serviceStubs_12 = value;
		Il2CppCodeGenWriteBarrier(&____serviceStubs_12, value);
	}
};

struct SRServiceManager_t2267823955_StaticFields
{
public:
	// System.Int32 SRF.Service.SRServiceManager::LoadingCount
	int32_t ___LoadingCount_10;
	// System.Boolean SRF.Service.SRServiceManager::_hasQuit
	bool ____hasQuit_13;

public:
	inline static int32_t get_offset_of_LoadingCount_10() { return static_cast<int32_t>(offsetof(SRServiceManager_t2267823955_StaticFields, ___LoadingCount_10)); }
	inline int32_t get_LoadingCount_10() const { return ___LoadingCount_10; }
	inline int32_t* get_address_of_LoadingCount_10() { return &___LoadingCount_10; }
	inline void set_LoadingCount_10(int32_t value)
	{
		___LoadingCount_10 = value;
	}

	inline static int32_t get_offset_of__hasQuit_13() { return static_cast<int32_t>(offsetof(SRServiceManager_t2267823955_StaticFields, ____hasQuit_13)); }
	inline bool get__hasQuit_13() const { return ____hasQuit_13; }
	inline bool* get_address_of__hasQuit_13() { return &____hasQuit_13; }
	inline void set__hasQuit_13(bool value)
	{
		____hasQuit_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
