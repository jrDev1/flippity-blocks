﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.BugReportPopoverRoot
struct  BugReportPopoverRoot_t3609447367  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.CanvasGroup SRDebugger.UI.Other.BugReportPopoverRoot::CanvasGroup
	CanvasGroup_t3296560743 * ___CanvasGroup_9;
	// UnityEngine.RectTransform SRDebugger.UI.Other.BugReportPopoverRoot::Container
	RectTransform_t3349966182 * ___Container_10;

public:
	inline static int32_t get_offset_of_CanvasGroup_9() { return static_cast<int32_t>(offsetof(BugReportPopoverRoot_t3609447367, ___CanvasGroup_9)); }
	inline CanvasGroup_t3296560743 * get_CanvasGroup_9() const { return ___CanvasGroup_9; }
	inline CanvasGroup_t3296560743 ** get_address_of_CanvasGroup_9() { return &___CanvasGroup_9; }
	inline void set_CanvasGroup_9(CanvasGroup_t3296560743 * value)
	{
		___CanvasGroup_9 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasGroup_9, value);
	}

	inline static int32_t get_offset_of_Container_10() { return static_cast<int32_t>(offsetof(BugReportPopoverRoot_t3609447367, ___Container_10)); }
	inline RectTransform_t3349966182 * get_Container_10() const { return ___Container_10; }
	inline RectTransform_t3349966182 ** get_address_of_Container_10() { return &___Container_10; }
	inline void set_Container_10(RectTransform_t3349966182 * value)
	{
		___Container_10 = value;
		Il2CppCodeGenWriteBarrier(&___Container_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
