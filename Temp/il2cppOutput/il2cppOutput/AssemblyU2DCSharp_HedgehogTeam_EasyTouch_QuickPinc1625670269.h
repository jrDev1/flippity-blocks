﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickPinc4075590181.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickPinch295160812.h"

// HedgehogTeam.EasyTouch.QuickPinch/OnPinchAction
struct OnPinchAction_t815763932;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickPinch
struct  QuickPinch_t1625670269  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickPinch/OnPinchAction HedgehogTeam.EasyTouch.QuickPinch::onPinchAction
	OnPinchAction_t815763932 * ___onPinchAction_19;
	// System.Boolean HedgehogTeam.EasyTouch.QuickPinch::isGestureOnMe
	bool ___isGestureOnMe_20;
	// HedgehogTeam.EasyTouch.QuickPinch/ActionTiggering HedgehogTeam.EasyTouch.QuickPinch::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.QuickPinch/ActionPinchDirection HedgehogTeam.EasyTouch.QuickPinch::pinchDirection
	int32_t ___pinchDirection_22;
	// System.Single HedgehogTeam.EasyTouch.QuickPinch::axisActionValue
	float ___axisActionValue_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickPinch::enableSimpleAction
	bool ___enableSimpleAction_24;

public:
	inline static int32_t get_offset_of_onPinchAction_19() { return static_cast<int32_t>(offsetof(QuickPinch_t1625670269, ___onPinchAction_19)); }
	inline OnPinchAction_t815763932 * get_onPinchAction_19() const { return ___onPinchAction_19; }
	inline OnPinchAction_t815763932 ** get_address_of_onPinchAction_19() { return &___onPinchAction_19; }
	inline void set_onPinchAction_19(OnPinchAction_t815763932 * value)
	{
		___onPinchAction_19 = value;
		Il2CppCodeGenWriteBarrier(&___onPinchAction_19, value);
	}

	inline static int32_t get_offset_of_isGestureOnMe_20() { return static_cast<int32_t>(offsetof(QuickPinch_t1625670269, ___isGestureOnMe_20)); }
	inline bool get_isGestureOnMe_20() const { return ___isGestureOnMe_20; }
	inline bool* get_address_of_isGestureOnMe_20() { return &___isGestureOnMe_20; }
	inline void set_isGestureOnMe_20(bool value)
	{
		___isGestureOnMe_20 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickPinch_t1625670269, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_pinchDirection_22() { return static_cast<int32_t>(offsetof(QuickPinch_t1625670269, ___pinchDirection_22)); }
	inline int32_t get_pinchDirection_22() const { return ___pinchDirection_22; }
	inline int32_t* get_address_of_pinchDirection_22() { return &___pinchDirection_22; }
	inline void set_pinchDirection_22(int32_t value)
	{
		___pinchDirection_22 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_23() { return static_cast<int32_t>(offsetof(QuickPinch_t1625670269, ___axisActionValue_23)); }
	inline float get_axisActionValue_23() const { return ___axisActionValue_23; }
	inline float* get_address_of_axisActionValue_23() { return &___axisActionValue_23; }
	inline void set_axisActionValue_23(float value)
	{
		___axisActionValue_23 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_24() { return static_cast<int32_t>(offsetof(QuickPinch_t1625670269, ___enableSimpleAction_24)); }
	inline bool get_enableSimpleAction_24() const { return ___enableSimpleAction_24; }
	inline bool* get_address_of_enableSimpleAction_24() { return &___enableSimpleAction_24; }
	inline void set_enableSimpleAction_24(bool value)
	{
		___enableSimpleAction_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
