﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Nullable_1_gen2467698368.h"

// SRDebugger.Services.IDebugPanelService
struct IDebugPanelService_t763743455;
// SRDebugger.Services.IDebugTriggerService
struct IDebugTriggerService_t4294816349;
// SRDebugger.Services.ISystemInformationService
struct ISystemInformationService_t4268864261;
// SRDebugger.Services.IOptionsService
struct IOptionsService_t4289539304;
// SRDebugger.Services.IPinnedUIService
struct IPinnedUIService_t3740776164;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRDebugger.VisibilityChangedDelegate
struct VisibilityChangedDelegate_t2318869097;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.SRDebugService
struct  SRDebugService_t586412289  : public Il2CppObject
{
public:
	// SRDebugger.Services.IDebugPanelService SRDebugger.Services.Implementation.SRDebugService::_debugPanelService
	Il2CppObject * ____debugPanelService_0;
	// SRDebugger.Services.IDebugTriggerService SRDebugger.Services.Implementation.SRDebugService::_debugTrigger
	Il2CppObject * ____debugTrigger_1;
	// SRDebugger.Services.ISystemInformationService SRDebugger.Services.Implementation.SRDebugService::_informationService
	Il2CppObject * ____informationService_2;
	// SRDebugger.Services.IOptionsService SRDebugger.Services.Implementation.SRDebugService::_optionsService
	Il2CppObject * ____optionsService_3;
	// SRDebugger.Services.IPinnedUIService SRDebugger.Services.Implementation.SRDebugService::_pinnedUiService
	Il2CppObject * ____pinnedUiService_4;
	// System.Boolean SRDebugger.Services.Implementation.SRDebugService::_entryCodeEnabled
	bool ____entryCodeEnabled_5;
	// System.Boolean SRDebugger.Services.Implementation.SRDebugService::_hasAuthorised
	bool ____hasAuthorised_6;
	// System.Nullable`1<SRDebugger.DefaultTabs> SRDebugger.Services.Implementation.SRDebugService::_queuedTab
	Nullable_1_t2467698368  ____queuedTab_7;
	// UnityEngine.RectTransform SRDebugger.Services.Implementation.SRDebugService::_worldSpaceTransform
	RectTransform_t3349966182 * ____worldSpaceTransform_8;
	// SRDebugger.VisibilityChangedDelegate SRDebugger.Services.Implementation.SRDebugService::PanelVisibilityChanged
	VisibilityChangedDelegate_t2318869097 * ___PanelVisibilityChanged_9;

public:
	inline static int32_t get_offset_of__debugPanelService_0() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____debugPanelService_0)); }
	inline Il2CppObject * get__debugPanelService_0() const { return ____debugPanelService_0; }
	inline Il2CppObject ** get_address_of__debugPanelService_0() { return &____debugPanelService_0; }
	inline void set__debugPanelService_0(Il2CppObject * value)
	{
		____debugPanelService_0 = value;
		Il2CppCodeGenWriteBarrier(&____debugPanelService_0, value);
	}

	inline static int32_t get_offset_of__debugTrigger_1() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____debugTrigger_1)); }
	inline Il2CppObject * get__debugTrigger_1() const { return ____debugTrigger_1; }
	inline Il2CppObject ** get_address_of__debugTrigger_1() { return &____debugTrigger_1; }
	inline void set__debugTrigger_1(Il2CppObject * value)
	{
		____debugTrigger_1 = value;
		Il2CppCodeGenWriteBarrier(&____debugTrigger_1, value);
	}

	inline static int32_t get_offset_of__informationService_2() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____informationService_2)); }
	inline Il2CppObject * get__informationService_2() const { return ____informationService_2; }
	inline Il2CppObject ** get_address_of__informationService_2() { return &____informationService_2; }
	inline void set__informationService_2(Il2CppObject * value)
	{
		____informationService_2 = value;
		Il2CppCodeGenWriteBarrier(&____informationService_2, value);
	}

	inline static int32_t get_offset_of__optionsService_3() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____optionsService_3)); }
	inline Il2CppObject * get__optionsService_3() const { return ____optionsService_3; }
	inline Il2CppObject ** get_address_of__optionsService_3() { return &____optionsService_3; }
	inline void set__optionsService_3(Il2CppObject * value)
	{
		____optionsService_3 = value;
		Il2CppCodeGenWriteBarrier(&____optionsService_3, value);
	}

	inline static int32_t get_offset_of__pinnedUiService_4() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____pinnedUiService_4)); }
	inline Il2CppObject * get__pinnedUiService_4() const { return ____pinnedUiService_4; }
	inline Il2CppObject ** get_address_of__pinnedUiService_4() { return &____pinnedUiService_4; }
	inline void set__pinnedUiService_4(Il2CppObject * value)
	{
		____pinnedUiService_4 = value;
		Il2CppCodeGenWriteBarrier(&____pinnedUiService_4, value);
	}

	inline static int32_t get_offset_of__entryCodeEnabled_5() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____entryCodeEnabled_5)); }
	inline bool get__entryCodeEnabled_5() const { return ____entryCodeEnabled_5; }
	inline bool* get_address_of__entryCodeEnabled_5() { return &____entryCodeEnabled_5; }
	inline void set__entryCodeEnabled_5(bool value)
	{
		____entryCodeEnabled_5 = value;
	}

	inline static int32_t get_offset_of__hasAuthorised_6() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____hasAuthorised_6)); }
	inline bool get__hasAuthorised_6() const { return ____hasAuthorised_6; }
	inline bool* get_address_of__hasAuthorised_6() { return &____hasAuthorised_6; }
	inline void set__hasAuthorised_6(bool value)
	{
		____hasAuthorised_6 = value;
	}

	inline static int32_t get_offset_of__queuedTab_7() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____queuedTab_7)); }
	inline Nullable_1_t2467698368  get__queuedTab_7() const { return ____queuedTab_7; }
	inline Nullable_1_t2467698368 * get_address_of__queuedTab_7() { return &____queuedTab_7; }
	inline void set__queuedTab_7(Nullable_1_t2467698368  value)
	{
		____queuedTab_7 = value;
	}

	inline static int32_t get_offset_of__worldSpaceTransform_8() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ____worldSpaceTransform_8)); }
	inline RectTransform_t3349966182 * get__worldSpaceTransform_8() const { return ____worldSpaceTransform_8; }
	inline RectTransform_t3349966182 ** get_address_of__worldSpaceTransform_8() { return &____worldSpaceTransform_8; }
	inline void set__worldSpaceTransform_8(RectTransform_t3349966182 * value)
	{
		____worldSpaceTransform_8 = value;
		Il2CppCodeGenWriteBarrier(&____worldSpaceTransform_8, value);
	}

	inline static int32_t get_offset_of_PanelVisibilityChanged_9() { return static_cast<int32_t>(offsetof(SRDebugService_t586412289, ___PanelVisibilityChanged_9)); }
	inline VisibilityChangedDelegate_t2318869097 * get_PanelVisibilityChanged_9() const { return ___PanelVisibilityChanged_9; }
	inline VisibilityChangedDelegate_t2318869097 ** get_address_of_PanelVisibilityChanged_9() { return &___PanelVisibilityChanged_9; }
	inline void set_PanelVisibilityChanged_9(VisibilityChangedDelegate_t2318869097 * value)
	{
		___PanelVisibilityChanged_9 = value;
		Il2CppCodeGenWriteBarrier(&___PanelVisibilityChanged_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
