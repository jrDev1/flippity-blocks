﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_PlayMakerCollectionProxy398511462.h"

// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerArrayListProxy
struct  PlayMakerArrayListProxy_t4012441185  : public PlayMakerCollectionProxy_t398511462
{
public:
	// System.Collections.ArrayList PlayMakerArrayListProxy::_arrayList
	ArrayList_t4252133567 * ____arrayList_35;
	// System.Collections.ArrayList PlayMakerArrayListProxy::_snapShot
	ArrayList_t4252133567 * ____snapShot_36;

public:
	inline static int32_t get_offset_of__arrayList_35() { return static_cast<int32_t>(offsetof(PlayMakerArrayListProxy_t4012441185, ____arrayList_35)); }
	inline ArrayList_t4252133567 * get__arrayList_35() const { return ____arrayList_35; }
	inline ArrayList_t4252133567 ** get_address_of__arrayList_35() { return &____arrayList_35; }
	inline void set__arrayList_35(ArrayList_t4252133567 * value)
	{
		____arrayList_35 = value;
		Il2CppCodeGenWriteBarrier(&____arrayList_35, value);
	}

	inline static int32_t get_offset_of__snapShot_36() { return static_cast<int32_t>(offsetof(PlayMakerArrayListProxy_t4012441185, ____snapShot_36)); }
	inline ArrayList_t4252133567 * get__snapShot_36() const { return ____snapShot_36; }
	inline ArrayList_t4252133567 ** get_address_of__snapShot_36() { return &____snapShot_36; }
	inline void set__snapShot_36(ArrayList_t4252133567 * value)
	{
		____snapShot_36 = value;
		Il2CppCodeGenWriteBarrier(&____snapShot_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
