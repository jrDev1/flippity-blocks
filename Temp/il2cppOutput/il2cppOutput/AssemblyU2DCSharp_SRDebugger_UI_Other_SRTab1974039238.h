﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.String
struct String_t;
// SRDebugger.UI.Controls.SRTabButton
struct SRTabButton_t2557777178;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.SRTab
struct  SRTab_t1974039238  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.RectTransform SRDebugger.UI.Other.SRTab::HeaderExtraContent
	RectTransform_t3349966182 * ___HeaderExtraContent_9;
	// UnityEngine.Sprite SRDebugger.UI.Other.SRTab::Icon
	Sprite_t309593783 * ___Icon_10;
	// UnityEngine.RectTransform SRDebugger.UI.Other.SRTab::IconExtraContent
	RectTransform_t3349966182 * ___IconExtraContent_11;
	// System.String SRDebugger.UI.Other.SRTab::IconStyleKey
	String_t* ___IconStyleKey_12;
	// System.Int32 SRDebugger.UI.Other.SRTab::SortIndex
	int32_t ___SortIndex_13;
	// SRDebugger.UI.Controls.SRTabButton SRDebugger.UI.Other.SRTab::TabButton
	SRTabButton_t2557777178 * ___TabButton_14;
	// System.String SRDebugger.UI.Other.SRTab::_title
	String_t* ____title_15;
	// System.String SRDebugger.UI.Other.SRTab::_longTitle
	String_t* ____longTitle_16;
	// System.String SRDebugger.UI.Other.SRTab::_key
	String_t* ____key_17;

public:
	inline static int32_t get_offset_of_HeaderExtraContent_9() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ___HeaderExtraContent_9)); }
	inline RectTransform_t3349966182 * get_HeaderExtraContent_9() const { return ___HeaderExtraContent_9; }
	inline RectTransform_t3349966182 ** get_address_of_HeaderExtraContent_9() { return &___HeaderExtraContent_9; }
	inline void set_HeaderExtraContent_9(RectTransform_t3349966182 * value)
	{
		___HeaderExtraContent_9 = value;
		Il2CppCodeGenWriteBarrier(&___HeaderExtraContent_9, value);
	}

	inline static int32_t get_offset_of_Icon_10() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ___Icon_10)); }
	inline Sprite_t309593783 * get_Icon_10() const { return ___Icon_10; }
	inline Sprite_t309593783 ** get_address_of_Icon_10() { return &___Icon_10; }
	inline void set_Icon_10(Sprite_t309593783 * value)
	{
		___Icon_10 = value;
		Il2CppCodeGenWriteBarrier(&___Icon_10, value);
	}

	inline static int32_t get_offset_of_IconExtraContent_11() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ___IconExtraContent_11)); }
	inline RectTransform_t3349966182 * get_IconExtraContent_11() const { return ___IconExtraContent_11; }
	inline RectTransform_t3349966182 ** get_address_of_IconExtraContent_11() { return &___IconExtraContent_11; }
	inline void set_IconExtraContent_11(RectTransform_t3349966182 * value)
	{
		___IconExtraContent_11 = value;
		Il2CppCodeGenWriteBarrier(&___IconExtraContent_11, value);
	}

	inline static int32_t get_offset_of_IconStyleKey_12() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ___IconStyleKey_12)); }
	inline String_t* get_IconStyleKey_12() const { return ___IconStyleKey_12; }
	inline String_t** get_address_of_IconStyleKey_12() { return &___IconStyleKey_12; }
	inline void set_IconStyleKey_12(String_t* value)
	{
		___IconStyleKey_12 = value;
		Il2CppCodeGenWriteBarrier(&___IconStyleKey_12, value);
	}

	inline static int32_t get_offset_of_SortIndex_13() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ___SortIndex_13)); }
	inline int32_t get_SortIndex_13() const { return ___SortIndex_13; }
	inline int32_t* get_address_of_SortIndex_13() { return &___SortIndex_13; }
	inline void set_SortIndex_13(int32_t value)
	{
		___SortIndex_13 = value;
	}

	inline static int32_t get_offset_of_TabButton_14() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ___TabButton_14)); }
	inline SRTabButton_t2557777178 * get_TabButton_14() const { return ___TabButton_14; }
	inline SRTabButton_t2557777178 ** get_address_of_TabButton_14() { return &___TabButton_14; }
	inline void set_TabButton_14(SRTabButton_t2557777178 * value)
	{
		___TabButton_14 = value;
		Il2CppCodeGenWriteBarrier(&___TabButton_14, value);
	}

	inline static int32_t get_offset_of__title_15() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ____title_15)); }
	inline String_t* get__title_15() const { return ____title_15; }
	inline String_t** get_address_of__title_15() { return &____title_15; }
	inline void set__title_15(String_t* value)
	{
		____title_15 = value;
		Il2CppCodeGenWriteBarrier(&____title_15, value);
	}

	inline static int32_t get_offset_of__longTitle_16() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ____longTitle_16)); }
	inline String_t* get__longTitle_16() const { return ____longTitle_16; }
	inline String_t** get_address_of__longTitle_16() { return &____longTitle_16; }
	inline void set__longTitle_16(String_t* value)
	{
		____longTitle_16 = value;
		Il2CppCodeGenWriteBarrier(&____longTitle_16, value);
	}

	inline static int32_t get_offset_of__key_17() { return static_cast<int32_t>(offsetof(SRTab_t1974039238, ____key_17)); }
	inline String_t* get__key_17() const { return ____key_17; }
	inline String_t** get_address_of__key_17() { return &____key_17; }
	inline void set__key_17(String_t* value)
	{
		____key_17 = value;
		Il2CppCodeGenWriteBarrier(&____key_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
