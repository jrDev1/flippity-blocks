﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/<GetTrigger>c__AnonStorey1
struct  U3CGetTriggerU3Ec__AnonStorey1_t1790378461  : public Il2CppObject
{
public:
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger/<GetTrigger>c__AnonStorey1::triggerName
	String_t* ___triggerName_0;

public:
	inline static int32_t get_offset_of_triggerName_0() { return static_cast<int32_t>(offsetof(U3CGetTriggerU3Ec__AnonStorey1_t1790378461, ___triggerName_0)); }
	inline String_t* get_triggerName_0() const { return ___triggerName_0; }
	inline String_t** get_address_of_triggerName_0() { return &___triggerName_0; }
	inline void set_triggerName_0(String_t* value)
	{
		___triggerName_0 = value;
		Il2CppCodeGenWriteBarrier(&___triggerName_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
