﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_TouchType2732027771.h"

// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.BaseFinger
struct  BaseFinger_t1402521766  : public Il2CppObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::fingerIndex
	int32_t ___fingerIndex_0;
	// System.Int32 HedgehogTeam.EasyTouch.BaseFinger::touchCount
	int32_t ___touchCount_1;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::startPosition
	Vector2_t2243707579  ___startPosition_2;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::position
	Vector2_t2243707579  ___position_3;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.BaseFinger::deltaPosition
	Vector2_t2243707579  ___deltaPosition_4;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::actionTime
	float ___actionTime_5;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::deltaTime
	float ___deltaTime_6;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.BaseFinger::pickedCamera
	Camera_t189460977 * ___pickedCamera_7;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedObject
	GameObject_t1756533147 * ___pickedObject_8;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isGuiCamera
	bool ___isGuiCamera_9;
	// System.Boolean HedgehogTeam.EasyTouch.BaseFinger::isOverGui
	bool ___isOverGui_10;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.BaseFinger::pickedUIElement
	GameObject_t1756533147 * ___pickedUIElement_11;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::altitudeAngle
	float ___altitudeAngle_12;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::azimuthAngle
	float ___azimuthAngle_13;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::maximumPossiblePressure
	float ___maximumPossiblePressure_14;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::pressure
	float ___pressure_15;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radius
	float ___radius_16;
	// System.Single HedgehogTeam.EasyTouch.BaseFinger::radiusVariance
	float ___radiusVariance_17;
	// UnityEngine.TouchType HedgehogTeam.EasyTouch.BaseFinger::touchType
	int32_t ___touchType_18;

public:
	inline static int32_t get_offset_of_fingerIndex_0() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___fingerIndex_0)); }
	inline int32_t get_fingerIndex_0() const { return ___fingerIndex_0; }
	inline int32_t* get_address_of_fingerIndex_0() { return &___fingerIndex_0; }
	inline void set_fingerIndex_0(int32_t value)
	{
		___fingerIndex_0 = value;
	}

	inline static int32_t get_offset_of_touchCount_1() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___touchCount_1)); }
	inline int32_t get_touchCount_1() const { return ___touchCount_1; }
	inline int32_t* get_address_of_touchCount_1() { return &___touchCount_1; }
	inline void set_touchCount_1(int32_t value)
	{
		___touchCount_1 = value;
	}

	inline static int32_t get_offset_of_startPosition_2() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___startPosition_2)); }
	inline Vector2_t2243707579  get_startPosition_2() const { return ___startPosition_2; }
	inline Vector2_t2243707579 * get_address_of_startPosition_2() { return &___startPosition_2; }
	inline void set_startPosition_2(Vector2_t2243707579  value)
	{
		___startPosition_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___position_3)); }
	inline Vector2_t2243707579  get_position_3() const { return ___position_3; }
	inline Vector2_t2243707579 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector2_t2243707579  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_4() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___deltaPosition_4)); }
	inline Vector2_t2243707579  get_deltaPosition_4() const { return ___deltaPosition_4; }
	inline Vector2_t2243707579 * get_address_of_deltaPosition_4() { return &___deltaPosition_4; }
	inline void set_deltaPosition_4(Vector2_t2243707579  value)
	{
		___deltaPosition_4 = value;
	}

	inline static int32_t get_offset_of_actionTime_5() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___actionTime_5)); }
	inline float get_actionTime_5() const { return ___actionTime_5; }
	inline float* get_address_of_actionTime_5() { return &___actionTime_5; }
	inline void set_actionTime_5(float value)
	{
		___actionTime_5 = value;
	}

	inline static int32_t get_offset_of_deltaTime_6() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___deltaTime_6)); }
	inline float get_deltaTime_6() const { return ___deltaTime_6; }
	inline float* get_address_of_deltaTime_6() { return &___deltaTime_6; }
	inline void set_deltaTime_6(float value)
	{
		___deltaTime_6 = value;
	}

	inline static int32_t get_offset_of_pickedCamera_7() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___pickedCamera_7)); }
	inline Camera_t189460977 * get_pickedCamera_7() const { return ___pickedCamera_7; }
	inline Camera_t189460977 ** get_address_of_pickedCamera_7() { return &___pickedCamera_7; }
	inline void set_pickedCamera_7(Camera_t189460977 * value)
	{
		___pickedCamera_7 = value;
		Il2CppCodeGenWriteBarrier(&___pickedCamera_7, value);
	}

	inline static int32_t get_offset_of_pickedObject_8() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___pickedObject_8)); }
	inline GameObject_t1756533147 * get_pickedObject_8() const { return ___pickedObject_8; }
	inline GameObject_t1756533147 ** get_address_of_pickedObject_8() { return &___pickedObject_8; }
	inline void set_pickedObject_8(GameObject_t1756533147 * value)
	{
		___pickedObject_8 = value;
		Il2CppCodeGenWriteBarrier(&___pickedObject_8, value);
	}

	inline static int32_t get_offset_of_isGuiCamera_9() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___isGuiCamera_9)); }
	inline bool get_isGuiCamera_9() const { return ___isGuiCamera_9; }
	inline bool* get_address_of_isGuiCamera_9() { return &___isGuiCamera_9; }
	inline void set_isGuiCamera_9(bool value)
	{
		___isGuiCamera_9 = value;
	}

	inline static int32_t get_offset_of_isOverGui_10() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___isOverGui_10)); }
	inline bool get_isOverGui_10() const { return ___isOverGui_10; }
	inline bool* get_address_of_isOverGui_10() { return &___isOverGui_10; }
	inline void set_isOverGui_10(bool value)
	{
		___isOverGui_10 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_11() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___pickedUIElement_11)); }
	inline GameObject_t1756533147 * get_pickedUIElement_11() const { return ___pickedUIElement_11; }
	inline GameObject_t1756533147 ** get_address_of_pickedUIElement_11() { return &___pickedUIElement_11; }
	inline void set_pickedUIElement_11(GameObject_t1756533147 * value)
	{
		___pickedUIElement_11 = value;
		Il2CppCodeGenWriteBarrier(&___pickedUIElement_11, value);
	}

	inline static int32_t get_offset_of_altitudeAngle_12() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___altitudeAngle_12)); }
	inline float get_altitudeAngle_12() const { return ___altitudeAngle_12; }
	inline float* get_address_of_altitudeAngle_12() { return &___altitudeAngle_12; }
	inline void set_altitudeAngle_12(float value)
	{
		___altitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_azimuthAngle_13() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___azimuthAngle_13)); }
	inline float get_azimuthAngle_13() const { return ___azimuthAngle_13; }
	inline float* get_address_of_azimuthAngle_13() { return &___azimuthAngle_13; }
	inline void set_azimuthAngle_13(float value)
	{
		___azimuthAngle_13 = value;
	}

	inline static int32_t get_offset_of_maximumPossiblePressure_14() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___maximumPossiblePressure_14)); }
	inline float get_maximumPossiblePressure_14() const { return ___maximumPossiblePressure_14; }
	inline float* get_address_of_maximumPossiblePressure_14() { return &___maximumPossiblePressure_14; }
	inline void set_maximumPossiblePressure_14(float value)
	{
		___maximumPossiblePressure_14 = value;
	}

	inline static int32_t get_offset_of_pressure_15() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___pressure_15)); }
	inline float get_pressure_15() const { return ___pressure_15; }
	inline float* get_address_of_pressure_15() { return &___pressure_15; }
	inline void set_pressure_15(float value)
	{
		___pressure_15 = value;
	}

	inline static int32_t get_offset_of_radius_16() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___radius_16)); }
	inline float get_radius_16() const { return ___radius_16; }
	inline float* get_address_of_radius_16() { return &___radius_16; }
	inline void set_radius_16(float value)
	{
		___radius_16 = value;
	}

	inline static int32_t get_offset_of_radiusVariance_17() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___radiusVariance_17)); }
	inline float get_radiusVariance_17() const { return ___radiusVariance_17; }
	inline float* get_address_of_radiusVariance_17() { return &___radiusVariance_17; }
	inline void set_radiusVariance_17(float value)
	{
		___radiusVariance_17 = value;
	}

	inline static int32_t get_offset_of_touchType_18() { return static_cast<int32_t>(offsetof(BaseFinger_t1402521766, ___touchType_18)); }
	inline int32_t get_touchType_18() const { return ___touchType_18; }
	inline int32_t* get_address_of_touchType_18() { return &___touchType_18; }
	inline void set_touchType_18(int32_t value)
	{
		___touchType_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
