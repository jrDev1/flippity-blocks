﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickTouc1348873172.h"

// HedgehogTeam.EasyTouch.QuickTouch/OnTouch
struct OnTouch_t2461077158;
// HedgehogTeam.EasyTouch.QuickTouch/OnTouchNotOverMe
struct OnTouchNotOverMe_t1832035339;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTouch
struct  QuickTouch_t778691482  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickTouch/OnTouch HedgehogTeam.EasyTouch.QuickTouch::onTouch
	OnTouch_t2461077158 * ___onTouch_19;
	// HedgehogTeam.EasyTouch.QuickTouch/OnTouchNotOverMe HedgehogTeam.EasyTouch.QuickTouch::onTouchNotOverMe
	OnTouchNotOverMe_t1832035339 * ___onTouchNotOverMe_20;
	// HedgehogTeam.EasyTouch.QuickTouch/ActionTriggering HedgehogTeam.EasyTouch.QuickTouch::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickTouch::currentGesture
	Gesture_t367995397 * ___currentGesture_22;

public:
	inline static int32_t get_offset_of_onTouch_19() { return static_cast<int32_t>(offsetof(QuickTouch_t778691482, ___onTouch_19)); }
	inline OnTouch_t2461077158 * get_onTouch_19() const { return ___onTouch_19; }
	inline OnTouch_t2461077158 ** get_address_of_onTouch_19() { return &___onTouch_19; }
	inline void set_onTouch_19(OnTouch_t2461077158 * value)
	{
		___onTouch_19 = value;
		Il2CppCodeGenWriteBarrier(&___onTouch_19, value);
	}

	inline static int32_t get_offset_of_onTouchNotOverMe_20() { return static_cast<int32_t>(offsetof(QuickTouch_t778691482, ___onTouchNotOverMe_20)); }
	inline OnTouchNotOverMe_t1832035339 * get_onTouchNotOverMe_20() const { return ___onTouchNotOverMe_20; }
	inline OnTouchNotOverMe_t1832035339 ** get_address_of_onTouchNotOverMe_20() { return &___onTouchNotOverMe_20; }
	inline void set_onTouchNotOverMe_20(OnTouchNotOverMe_t1832035339 * value)
	{
		___onTouchNotOverMe_20 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchNotOverMe_20, value);
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickTouch_t778691482, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_currentGesture_22() { return static_cast<int32_t>(offsetof(QuickTouch_t778691482, ___currentGesture_22)); }
	inline Gesture_t367995397 * get_currentGesture_22() const { return ___currentGesture_22; }
	inline Gesture_t367995397 ** get_address_of_currentGesture_22() { return &___currentGesture_22; }
	inline void set_currentGesture_22(Gesture_t367995397 * value)
	{
		___currentGesture_22 = value;
		Il2CppCodeGenWriteBarrier(&___currentGesture_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
