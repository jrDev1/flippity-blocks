﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_ETCAxis_AxisValueMethod3519790939.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_ETCAxis_AxisState3105999474.h"
#include "AssemblyU2DCSharp_ETCAxis_DirectAction1624957987.h"
#include "AssemblyU2DCSharp_ETCAxis_AxisInfluenced2373105132.h"
#include "AssemblyU2DCSharp_ETCAxis_ActionOn983595283.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.CharacterController
struct CharacterController_t4094781467;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCAxis
struct  ETCAxis_t3445562479  : public Il2CppObject
{
public:
	// System.String ETCAxis::name
	String_t* ___name_0;
	// System.Boolean ETCAxis::autoLinkTagPlayer
	bool ___autoLinkTagPlayer_1;
	// System.String ETCAxis::autoTag
	String_t* ___autoTag_2;
	// UnityEngine.GameObject ETCAxis::player
	GameObject_t1756533147 * ___player_3;
	// System.Boolean ETCAxis::enable
	bool ___enable_4;
	// System.Boolean ETCAxis::invertedAxis
	bool ___invertedAxis_5;
	// System.Single ETCAxis::speed
	float ___speed_6;
	// System.Single ETCAxis::deadValue
	float ___deadValue_7;
	// ETCAxis/AxisValueMethod ETCAxis::valueMethod
	int32_t ___valueMethod_8;
	// UnityEngine.AnimationCurve ETCAxis::curveValue
	AnimationCurve_t3306541151 * ___curveValue_9;
	// System.Boolean ETCAxis::isEnertia
	bool ___isEnertia_10;
	// System.Single ETCAxis::inertia
	float ___inertia_11;
	// System.Single ETCAxis::inertiaThreshold
	float ___inertiaThreshold_12;
	// System.Boolean ETCAxis::isAutoStab
	bool ___isAutoStab_13;
	// System.Single ETCAxis::autoStabThreshold
	float ___autoStabThreshold_14;
	// System.Single ETCAxis::autoStabSpeed
	float ___autoStabSpeed_15;
	// System.Single ETCAxis::startAngle
	float ___startAngle_16;
	// System.Boolean ETCAxis::isClampRotation
	bool ___isClampRotation_17;
	// System.Single ETCAxis::maxAngle
	float ___maxAngle_18;
	// System.Single ETCAxis::minAngle
	float ___minAngle_19;
	// System.Boolean ETCAxis::isValueOverTime
	bool ___isValueOverTime_20;
	// System.Single ETCAxis::overTimeStep
	float ___overTimeStep_21;
	// System.Single ETCAxis::maxOverTimeValue
	float ___maxOverTimeValue_22;
	// System.Single ETCAxis::axisValue
	float ___axisValue_23;
	// System.Single ETCAxis::axisSpeedValue
	float ___axisSpeedValue_24;
	// System.Single ETCAxis::axisThreshold
	float ___axisThreshold_25;
	// System.Boolean ETCAxis::isLockinJump
	bool ___isLockinJump_26;
	// UnityEngine.Vector3 ETCAxis::lastMove
	Vector3_t2243707580  ___lastMove_27;
	// ETCAxis/AxisState ETCAxis::axisState
	int32_t ___axisState_28;
	// UnityEngine.Transform ETCAxis::_directTransform
	Transform_t3275118058 * ____directTransform_29;
	// ETCAxis/DirectAction ETCAxis::directAction
	int32_t ___directAction_30;
	// ETCAxis/AxisInfluenced ETCAxis::axisInfluenced
	int32_t ___axisInfluenced_31;
	// ETCAxis/ActionOn ETCAxis::actionOn
	int32_t ___actionOn_32;
	// UnityEngine.CharacterController ETCAxis::directCharacterController
	CharacterController_t4094781467 * ___directCharacterController_33;
	// UnityEngine.Rigidbody ETCAxis::directRigidBody
	Rigidbody_t4233889191 * ___directRigidBody_34;
	// System.Single ETCAxis::gravity
	float ___gravity_35;
	// System.Single ETCAxis::currentGravity
	float ___currentGravity_36;
	// System.Boolean ETCAxis::isJump
	bool ___isJump_37;
	// System.String ETCAxis::unityAxis
	String_t* ___unityAxis_38;
	// System.Boolean ETCAxis::showGeneralInspector
	bool ___showGeneralInspector_39;
	// System.Boolean ETCAxis::showDirectInspector
	bool ___showDirectInspector_40;
	// System.Boolean ETCAxis::showInertiaInspector
	bool ___showInertiaInspector_41;
	// System.Boolean ETCAxis::showSimulatinInspector
	bool ___showSimulatinInspector_42;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier(&___name_0, value);
	}

	inline static int32_t get_offset_of_autoLinkTagPlayer_1() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___autoLinkTagPlayer_1)); }
	inline bool get_autoLinkTagPlayer_1() const { return ___autoLinkTagPlayer_1; }
	inline bool* get_address_of_autoLinkTagPlayer_1() { return &___autoLinkTagPlayer_1; }
	inline void set_autoLinkTagPlayer_1(bool value)
	{
		___autoLinkTagPlayer_1 = value;
	}

	inline static int32_t get_offset_of_autoTag_2() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___autoTag_2)); }
	inline String_t* get_autoTag_2() const { return ___autoTag_2; }
	inline String_t** get_address_of_autoTag_2() { return &___autoTag_2; }
	inline void set_autoTag_2(String_t* value)
	{
		___autoTag_2 = value;
		Il2CppCodeGenWriteBarrier(&___autoTag_2, value);
	}

	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___player_3)); }
	inline GameObject_t1756533147 * get_player_3() const { return ___player_3; }
	inline GameObject_t1756533147 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1756533147 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier(&___player_3, value);
	}

	inline static int32_t get_offset_of_enable_4() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___enable_4)); }
	inline bool get_enable_4() const { return ___enable_4; }
	inline bool* get_address_of_enable_4() { return &___enable_4; }
	inline void set_enable_4(bool value)
	{
		___enable_4 = value;
	}

	inline static int32_t get_offset_of_invertedAxis_5() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___invertedAxis_5)); }
	inline bool get_invertedAxis_5() const { return ___invertedAxis_5; }
	inline bool* get_address_of_invertedAxis_5() { return &___invertedAxis_5; }
	inline void set_invertedAxis_5(bool value)
	{
		___invertedAxis_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_deadValue_7() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___deadValue_7)); }
	inline float get_deadValue_7() const { return ___deadValue_7; }
	inline float* get_address_of_deadValue_7() { return &___deadValue_7; }
	inline void set_deadValue_7(float value)
	{
		___deadValue_7 = value;
	}

	inline static int32_t get_offset_of_valueMethod_8() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___valueMethod_8)); }
	inline int32_t get_valueMethod_8() const { return ___valueMethod_8; }
	inline int32_t* get_address_of_valueMethod_8() { return &___valueMethod_8; }
	inline void set_valueMethod_8(int32_t value)
	{
		___valueMethod_8 = value;
	}

	inline static int32_t get_offset_of_curveValue_9() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___curveValue_9)); }
	inline AnimationCurve_t3306541151 * get_curveValue_9() const { return ___curveValue_9; }
	inline AnimationCurve_t3306541151 ** get_address_of_curveValue_9() { return &___curveValue_9; }
	inline void set_curveValue_9(AnimationCurve_t3306541151 * value)
	{
		___curveValue_9 = value;
		Il2CppCodeGenWriteBarrier(&___curveValue_9, value);
	}

	inline static int32_t get_offset_of_isEnertia_10() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___isEnertia_10)); }
	inline bool get_isEnertia_10() const { return ___isEnertia_10; }
	inline bool* get_address_of_isEnertia_10() { return &___isEnertia_10; }
	inline void set_isEnertia_10(bool value)
	{
		___isEnertia_10 = value;
	}

	inline static int32_t get_offset_of_inertia_11() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___inertia_11)); }
	inline float get_inertia_11() const { return ___inertia_11; }
	inline float* get_address_of_inertia_11() { return &___inertia_11; }
	inline void set_inertia_11(float value)
	{
		___inertia_11 = value;
	}

	inline static int32_t get_offset_of_inertiaThreshold_12() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___inertiaThreshold_12)); }
	inline float get_inertiaThreshold_12() const { return ___inertiaThreshold_12; }
	inline float* get_address_of_inertiaThreshold_12() { return &___inertiaThreshold_12; }
	inline void set_inertiaThreshold_12(float value)
	{
		___inertiaThreshold_12 = value;
	}

	inline static int32_t get_offset_of_isAutoStab_13() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___isAutoStab_13)); }
	inline bool get_isAutoStab_13() const { return ___isAutoStab_13; }
	inline bool* get_address_of_isAutoStab_13() { return &___isAutoStab_13; }
	inline void set_isAutoStab_13(bool value)
	{
		___isAutoStab_13 = value;
	}

	inline static int32_t get_offset_of_autoStabThreshold_14() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___autoStabThreshold_14)); }
	inline float get_autoStabThreshold_14() const { return ___autoStabThreshold_14; }
	inline float* get_address_of_autoStabThreshold_14() { return &___autoStabThreshold_14; }
	inline void set_autoStabThreshold_14(float value)
	{
		___autoStabThreshold_14 = value;
	}

	inline static int32_t get_offset_of_autoStabSpeed_15() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___autoStabSpeed_15)); }
	inline float get_autoStabSpeed_15() const { return ___autoStabSpeed_15; }
	inline float* get_address_of_autoStabSpeed_15() { return &___autoStabSpeed_15; }
	inline void set_autoStabSpeed_15(float value)
	{
		___autoStabSpeed_15 = value;
	}

	inline static int32_t get_offset_of_startAngle_16() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___startAngle_16)); }
	inline float get_startAngle_16() const { return ___startAngle_16; }
	inline float* get_address_of_startAngle_16() { return &___startAngle_16; }
	inline void set_startAngle_16(float value)
	{
		___startAngle_16 = value;
	}

	inline static int32_t get_offset_of_isClampRotation_17() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___isClampRotation_17)); }
	inline bool get_isClampRotation_17() const { return ___isClampRotation_17; }
	inline bool* get_address_of_isClampRotation_17() { return &___isClampRotation_17; }
	inline void set_isClampRotation_17(bool value)
	{
		___isClampRotation_17 = value;
	}

	inline static int32_t get_offset_of_maxAngle_18() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___maxAngle_18)); }
	inline float get_maxAngle_18() const { return ___maxAngle_18; }
	inline float* get_address_of_maxAngle_18() { return &___maxAngle_18; }
	inline void set_maxAngle_18(float value)
	{
		___maxAngle_18 = value;
	}

	inline static int32_t get_offset_of_minAngle_19() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___minAngle_19)); }
	inline float get_minAngle_19() const { return ___minAngle_19; }
	inline float* get_address_of_minAngle_19() { return &___minAngle_19; }
	inline void set_minAngle_19(float value)
	{
		___minAngle_19 = value;
	}

	inline static int32_t get_offset_of_isValueOverTime_20() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___isValueOverTime_20)); }
	inline bool get_isValueOverTime_20() const { return ___isValueOverTime_20; }
	inline bool* get_address_of_isValueOverTime_20() { return &___isValueOverTime_20; }
	inline void set_isValueOverTime_20(bool value)
	{
		___isValueOverTime_20 = value;
	}

	inline static int32_t get_offset_of_overTimeStep_21() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___overTimeStep_21)); }
	inline float get_overTimeStep_21() const { return ___overTimeStep_21; }
	inline float* get_address_of_overTimeStep_21() { return &___overTimeStep_21; }
	inline void set_overTimeStep_21(float value)
	{
		___overTimeStep_21 = value;
	}

	inline static int32_t get_offset_of_maxOverTimeValue_22() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___maxOverTimeValue_22)); }
	inline float get_maxOverTimeValue_22() const { return ___maxOverTimeValue_22; }
	inline float* get_address_of_maxOverTimeValue_22() { return &___maxOverTimeValue_22; }
	inline void set_maxOverTimeValue_22(float value)
	{
		___maxOverTimeValue_22 = value;
	}

	inline static int32_t get_offset_of_axisValue_23() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___axisValue_23)); }
	inline float get_axisValue_23() const { return ___axisValue_23; }
	inline float* get_address_of_axisValue_23() { return &___axisValue_23; }
	inline void set_axisValue_23(float value)
	{
		___axisValue_23 = value;
	}

	inline static int32_t get_offset_of_axisSpeedValue_24() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___axisSpeedValue_24)); }
	inline float get_axisSpeedValue_24() const { return ___axisSpeedValue_24; }
	inline float* get_address_of_axisSpeedValue_24() { return &___axisSpeedValue_24; }
	inline void set_axisSpeedValue_24(float value)
	{
		___axisSpeedValue_24 = value;
	}

	inline static int32_t get_offset_of_axisThreshold_25() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___axisThreshold_25)); }
	inline float get_axisThreshold_25() const { return ___axisThreshold_25; }
	inline float* get_address_of_axisThreshold_25() { return &___axisThreshold_25; }
	inline void set_axisThreshold_25(float value)
	{
		___axisThreshold_25 = value;
	}

	inline static int32_t get_offset_of_isLockinJump_26() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___isLockinJump_26)); }
	inline bool get_isLockinJump_26() const { return ___isLockinJump_26; }
	inline bool* get_address_of_isLockinJump_26() { return &___isLockinJump_26; }
	inline void set_isLockinJump_26(bool value)
	{
		___isLockinJump_26 = value;
	}

	inline static int32_t get_offset_of_lastMove_27() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___lastMove_27)); }
	inline Vector3_t2243707580  get_lastMove_27() const { return ___lastMove_27; }
	inline Vector3_t2243707580 * get_address_of_lastMove_27() { return &___lastMove_27; }
	inline void set_lastMove_27(Vector3_t2243707580  value)
	{
		___lastMove_27 = value;
	}

	inline static int32_t get_offset_of_axisState_28() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___axisState_28)); }
	inline int32_t get_axisState_28() const { return ___axisState_28; }
	inline int32_t* get_address_of_axisState_28() { return &___axisState_28; }
	inline void set_axisState_28(int32_t value)
	{
		___axisState_28 = value;
	}

	inline static int32_t get_offset_of__directTransform_29() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ____directTransform_29)); }
	inline Transform_t3275118058 * get__directTransform_29() const { return ____directTransform_29; }
	inline Transform_t3275118058 ** get_address_of__directTransform_29() { return &____directTransform_29; }
	inline void set__directTransform_29(Transform_t3275118058 * value)
	{
		____directTransform_29 = value;
		Il2CppCodeGenWriteBarrier(&____directTransform_29, value);
	}

	inline static int32_t get_offset_of_directAction_30() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___directAction_30)); }
	inline int32_t get_directAction_30() const { return ___directAction_30; }
	inline int32_t* get_address_of_directAction_30() { return &___directAction_30; }
	inline void set_directAction_30(int32_t value)
	{
		___directAction_30 = value;
	}

	inline static int32_t get_offset_of_axisInfluenced_31() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___axisInfluenced_31)); }
	inline int32_t get_axisInfluenced_31() const { return ___axisInfluenced_31; }
	inline int32_t* get_address_of_axisInfluenced_31() { return &___axisInfluenced_31; }
	inline void set_axisInfluenced_31(int32_t value)
	{
		___axisInfluenced_31 = value;
	}

	inline static int32_t get_offset_of_actionOn_32() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___actionOn_32)); }
	inline int32_t get_actionOn_32() const { return ___actionOn_32; }
	inline int32_t* get_address_of_actionOn_32() { return &___actionOn_32; }
	inline void set_actionOn_32(int32_t value)
	{
		___actionOn_32 = value;
	}

	inline static int32_t get_offset_of_directCharacterController_33() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___directCharacterController_33)); }
	inline CharacterController_t4094781467 * get_directCharacterController_33() const { return ___directCharacterController_33; }
	inline CharacterController_t4094781467 ** get_address_of_directCharacterController_33() { return &___directCharacterController_33; }
	inline void set_directCharacterController_33(CharacterController_t4094781467 * value)
	{
		___directCharacterController_33 = value;
		Il2CppCodeGenWriteBarrier(&___directCharacterController_33, value);
	}

	inline static int32_t get_offset_of_directRigidBody_34() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___directRigidBody_34)); }
	inline Rigidbody_t4233889191 * get_directRigidBody_34() const { return ___directRigidBody_34; }
	inline Rigidbody_t4233889191 ** get_address_of_directRigidBody_34() { return &___directRigidBody_34; }
	inline void set_directRigidBody_34(Rigidbody_t4233889191 * value)
	{
		___directRigidBody_34 = value;
		Il2CppCodeGenWriteBarrier(&___directRigidBody_34, value);
	}

	inline static int32_t get_offset_of_gravity_35() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___gravity_35)); }
	inline float get_gravity_35() const { return ___gravity_35; }
	inline float* get_address_of_gravity_35() { return &___gravity_35; }
	inline void set_gravity_35(float value)
	{
		___gravity_35 = value;
	}

	inline static int32_t get_offset_of_currentGravity_36() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___currentGravity_36)); }
	inline float get_currentGravity_36() const { return ___currentGravity_36; }
	inline float* get_address_of_currentGravity_36() { return &___currentGravity_36; }
	inline void set_currentGravity_36(float value)
	{
		___currentGravity_36 = value;
	}

	inline static int32_t get_offset_of_isJump_37() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___isJump_37)); }
	inline bool get_isJump_37() const { return ___isJump_37; }
	inline bool* get_address_of_isJump_37() { return &___isJump_37; }
	inline void set_isJump_37(bool value)
	{
		___isJump_37 = value;
	}

	inline static int32_t get_offset_of_unityAxis_38() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___unityAxis_38)); }
	inline String_t* get_unityAxis_38() const { return ___unityAxis_38; }
	inline String_t** get_address_of_unityAxis_38() { return &___unityAxis_38; }
	inline void set_unityAxis_38(String_t* value)
	{
		___unityAxis_38 = value;
		Il2CppCodeGenWriteBarrier(&___unityAxis_38, value);
	}

	inline static int32_t get_offset_of_showGeneralInspector_39() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___showGeneralInspector_39)); }
	inline bool get_showGeneralInspector_39() const { return ___showGeneralInspector_39; }
	inline bool* get_address_of_showGeneralInspector_39() { return &___showGeneralInspector_39; }
	inline void set_showGeneralInspector_39(bool value)
	{
		___showGeneralInspector_39 = value;
	}

	inline static int32_t get_offset_of_showDirectInspector_40() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___showDirectInspector_40)); }
	inline bool get_showDirectInspector_40() const { return ___showDirectInspector_40; }
	inline bool* get_address_of_showDirectInspector_40() { return &___showDirectInspector_40; }
	inline void set_showDirectInspector_40(bool value)
	{
		___showDirectInspector_40 = value;
	}

	inline static int32_t get_offset_of_showInertiaInspector_41() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___showInertiaInspector_41)); }
	inline bool get_showInertiaInspector_41() const { return ___showInertiaInspector_41; }
	inline bool* get_address_of_showInertiaInspector_41() { return &___showInertiaInspector_41; }
	inline void set_showInertiaInspector_41(bool value)
	{
		___showInertiaInspector_41 = value;
	}

	inline static int32_t get_offset_of_showSimulatinInspector_42() { return static_cast<int32_t>(offsetof(ETCAxis_t3445562479, ___showSimulatinInspector_42)); }
	inline bool get_showSimulatinInspector_42() const { return ___showSimulatinInspector_42; }
	inline bool* get_address_of_showSimulatinInspector_42() { return &___showSimulatinInspector_42; }
	inline void set_showSimulatinInspector_42(bool value)
	{
		___showSimulatinInspector_42 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
