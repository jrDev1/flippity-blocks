﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.LoadingSpinnerBehaviour
struct  LoadingSpinnerBehaviour_t2001614914  : public SRMonoBehaviour_t2352136145
{
public:
	// System.Single SRDebugger.UI.Other.LoadingSpinnerBehaviour::_dt
	float ____dt_8;
	// System.Int32 SRDebugger.UI.Other.LoadingSpinnerBehaviour::FrameCount
	int32_t ___FrameCount_9;
	// System.Single SRDebugger.UI.Other.LoadingSpinnerBehaviour::SpinDuration
	float ___SpinDuration_10;

public:
	inline static int32_t get_offset_of__dt_8() { return static_cast<int32_t>(offsetof(LoadingSpinnerBehaviour_t2001614914, ____dt_8)); }
	inline float get__dt_8() const { return ____dt_8; }
	inline float* get_address_of__dt_8() { return &____dt_8; }
	inline void set__dt_8(float value)
	{
		____dt_8 = value;
	}

	inline static int32_t get_offset_of_FrameCount_9() { return static_cast<int32_t>(offsetof(LoadingSpinnerBehaviour_t2001614914, ___FrameCount_9)); }
	inline int32_t get_FrameCount_9() const { return ___FrameCount_9; }
	inline int32_t* get_address_of_FrameCount_9() { return &___FrameCount_9; }
	inline void set_FrameCount_9(int32_t value)
	{
		___FrameCount_9 = value;
	}

	inline static int32_t get_offset_of_SpinDuration_10() { return static_cast<int32_t>(offsetof(LoadingSpinnerBehaviour_t2001614914, ___SpinDuration_10)); }
	inline float get_SpinDuration_10() const { return ___SpinDuration_10; }
	inline float* get_address_of_SpinDuration_10() { return &___SpinDuration_10; }
	inline void set_SpinDuration_10(float value)
	{
		___SpinDuration_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
