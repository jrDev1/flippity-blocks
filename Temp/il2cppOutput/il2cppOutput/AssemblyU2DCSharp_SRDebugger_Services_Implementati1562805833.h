﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.CircularBuffer`1<SRDebugger.Services.ConsoleEntry>
struct CircularBuffer_1_t4034286414;
// System.Object
struct Il2CppObject;
// SRDebugger.Services.ConsoleUpdatedEventHandler
struct ConsoleUpdatedEventHandler_t2943869110;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.StandardConsoleService
struct  StandardConsoleService_t1562805833  : public Il2CppObject
{
public:
	// System.Boolean SRDebugger.Services.Implementation.StandardConsoleService::_collapseEnabled
	bool ____collapseEnabled_0;
	// System.Boolean SRDebugger.Services.Implementation.StandardConsoleService::_hasCleared
	bool ____hasCleared_1;
	// SRDebugger.CircularBuffer`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.Implementation.StandardConsoleService::_allConsoleEntries
	CircularBuffer_1_t4034286414 * ____allConsoleEntries_2;
	// SRDebugger.CircularBuffer`1<SRDebugger.Services.ConsoleEntry> SRDebugger.Services.Implementation.StandardConsoleService::_consoleEntries
	CircularBuffer_1_t4034286414 * ____consoleEntries_3;
	// System.Object SRDebugger.Services.Implementation.StandardConsoleService::_threadLock
	Il2CppObject * ____threadLock_4;
	// System.Int32 SRDebugger.Services.Implementation.StandardConsoleService::<ErrorCount>k__BackingField
	int32_t ___U3CErrorCountU3Ek__BackingField_5;
	// System.Int32 SRDebugger.Services.Implementation.StandardConsoleService::<WarningCount>k__BackingField
	int32_t ___U3CWarningCountU3Ek__BackingField_6;
	// System.Int32 SRDebugger.Services.Implementation.StandardConsoleService::<InfoCount>k__BackingField
	int32_t ___U3CInfoCountU3Ek__BackingField_7;
	// SRDebugger.Services.ConsoleUpdatedEventHandler SRDebugger.Services.Implementation.StandardConsoleService::Updated
	ConsoleUpdatedEventHandler_t2943869110 * ___Updated_8;

public:
	inline static int32_t get_offset_of__collapseEnabled_0() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ____collapseEnabled_0)); }
	inline bool get__collapseEnabled_0() const { return ____collapseEnabled_0; }
	inline bool* get_address_of__collapseEnabled_0() { return &____collapseEnabled_0; }
	inline void set__collapseEnabled_0(bool value)
	{
		____collapseEnabled_0 = value;
	}

	inline static int32_t get_offset_of__hasCleared_1() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ____hasCleared_1)); }
	inline bool get__hasCleared_1() const { return ____hasCleared_1; }
	inline bool* get_address_of__hasCleared_1() { return &____hasCleared_1; }
	inline void set__hasCleared_1(bool value)
	{
		____hasCleared_1 = value;
	}

	inline static int32_t get_offset_of__allConsoleEntries_2() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ____allConsoleEntries_2)); }
	inline CircularBuffer_1_t4034286414 * get__allConsoleEntries_2() const { return ____allConsoleEntries_2; }
	inline CircularBuffer_1_t4034286414 ** get_address_of__allConsoleEntries_2() { return &____allConsoleEntries_2; }
	inline void set__allConsoleEntries_2(CircularBuffer_1_t4034286414 * value)
	{
		____allConsoleEntries_2 = value;
		Il2CppCodeGenWriteBarrier(&____allConsoleEntries_2, value);
	}

	inline static int32_t get_offset_of__consoleEntries_3() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ____consoleEntries_3)); }
	inline CircularBuffer_1_t4034286414 * get__consoleEntries_3() const { return ____consoleEntries_3; }
	inline CircularBuffer_1_t4034286414 ** get_address_of__consoleEntries_3() { return &____consoleEntries_3; }
	inline void set__consoleEntries_3(CircularBuffer_1_t4034286414 * value)
	{
		____consoleEntries_3 = value;
		Il2CppCodeGenWriteBarrier(&____consoleEntries_3, value);
	}

	inline static int32_t get_offset_of__threadLock_4() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ____threadLock_4)); }
	inline Il2CppObject * get__threadLock_4() const { return ____threadLock_4; }
	inline Il2CppObject ** get_address_of__threadLock_4() { return &____threadLock_4; }
	inline void set__threadLock_4(Il2CppObject * value)
	{
		____threadLock_4 = value;
		Il2CppCodeGenWriteBarrier(&____threadLock_4, value);
	}

	inline static int32_t get_offset_of_U3CErrorCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ___U3CErrorCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CErrorCountU3Ek__BackingField_5() const { return ___U3CErrorCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CErrorCountU3Ek__BackingField_5() { return &___U3CErrorCountU3Ek__BackingField_5; }
	inline void set_U3CErrorCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CErrorCountU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CWarningCountU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ___U3CWarningCountU3Ek__BackingField_6)); }
	inline int32_t get_U3CWarningCountU3Ek__BackingField_6() const { return ___U3CWarningCountU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CWarningCountU3Ek__BackingField_6() { return &___U3CWarningCountU3Ek__BackingField_6; }
	inline void set_U3CWarningCountU3Ek__BackingField_6(int32_t value)
	{
		___U3CWarningCountU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CInfoCountU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ___U3CInfoCountU3Ek__BackingField_7)); }
	inline int32_t get_U3CInfoCountU3Ek__BackingField_7() const { return ___U3CInfoCountU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CInfoCountU3Ek__BackingField_7() { return &___U3CInfoCountU3Ek__BackingField_7; }
	inline void set_U3CInfoCountU3Ek__BackingField_7(int32_t value)
	{
		___U3CInfoCountU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_Updated_8() { return static_cast<int32_t>(offsetof(StandardConsoleService_t1562805833, ___Updated_8)); }
	inline ConsoleUpdatedEventHandler_t2943869110 * get_Updated_8() const { return ___Updated_8; }
	inline ConsoleUpdatedEventHandler_t2943869110 ** get_address_of_Updated_8() { return &___Updated_8; }
	inline void set_Updated_8(ConsoleUpdatedEventHandler_t2943869110 * value)
	{
		___Updated_8 = value;
		Il2CppCodeGenWriteBarrier(&___Updated_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
