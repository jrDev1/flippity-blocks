﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultiLayerTouch
struct  MultiLayerTouch_t2295387221  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text MultiLayerTouch::label
	Text_t356221433 * ___label_2;
	// UnityEngine.UI.Text MultiLayerTouch::label2
	Text_t356221433 * ___label2_3;

public:
	inline static int32_t get_offset_of_label_2() { return static_cast<int32_t>(offsetof(MultiLayerTouch_t2295387221, ___label_2)); }
	inline Text_t356221433 * get_label_2() const { return ___label_2; }
	inline Text_t356221433 ** get_address_of_label_2() { return &___label_2; }
	inline void set_label_2(Text_t356221433 * value)
	{
		___label_2 = value;
		Il2CppCodeGenWriteBarrier(&___label_2, value);
	}

	inline static int32_t get_offset_of_label2_3() { return static_cast<int32_t>(offsetof(MultiLayerTouch_t2295387221, ___label2_3)); }
	inline Text_t356221433 * get_label2_3() const { return ___label2_3; }
	inline Text_t356221433 ** get_address_of_label2_3() { return &___label2_3; }
	inline void set_label2_3(Text_t356221433 * value)
	{
		___label2_3 = value;
		Il2CppCodeGenWriteBarrier(&___label2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
