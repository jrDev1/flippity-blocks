﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3toVector2
struct  Vector3toVector2_t323540756  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3toVector2::vector3
	FsmVector3_t3996534004 * ___vector3_11;
	// HutongGames.PlayMaker.FsmVector2 HutongGames.PlayMaker.Actions.Vector3toVector2::vector2
	FsmVector2_t2430450063 * ___vector2_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3toVector2::zValue
	FsmFloat_t937133978 * ___zValue_13;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3toVector2::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_vector3_11() { return static_cast<int32_t>(offsetof(Vector3toVector2_t323540756, ___vector3_11)); }
	inline FsmVector3_t3996534004 * get_vector3_11() const { return ___vector3_11; }
	inline FsmVector3_t3996534004 ** get_address_of_vector3_11() { return &___vector3_11; }
	inline void set_vector3_11(FsmVector3_t3996534004 * value)
	{
		___vector3_11 = value;
		Il2CppCodeGenWriteBarrier(&___vector3_11, value);
	}

	inline static int32_t get_offset_of_vector2_12() { return static_cast<int32_t>(offsetof(Vector3toVector2_t323540756, ___vector2_12)); }
	inline FsmVector2_t2430450063 * get_vector2_12() const { return ___vector2_12; }
	inline FsmVector2_t2430450063 ** get_address_of_vector2_12() { return &___vector2_12; }
	inline void set_vector2_12(FsmVector2_t2430450063 * value)
	{
		___vector2_12 = value;
		Il2CppCodeGenWriteBarrier(&___vector2_12, value);
	}

	inline static int32_t get_offset_of_zValue_13() { return static_cast<int32_t>(offsetof(Vector3toVector2_t323540756, ___zValue_13)); }
	inline FsmFloat_t937133978 * get_zValue_13() const { return ___zValue_13; }
	inline FsmFloat_t937133978 ** get_address_of_zValue_13() { return &___zValue_13; }
	inline void set_zValue_13(FsmFloat_t937133978 * value)
	{
		___zValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___zValue_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(Vector3toVector2_t323540756, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
