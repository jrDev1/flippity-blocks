void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.AI.NavMeshAgent

		//System.Void UnityEngine.AI.NavMeshAgent::INTERNAL_set_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_AI_NavMeshAgent_INTERNAL_set_velocity();
		Register_UnityEngine_AI_NavMeshAgent_INTERNAL_set_velocity();

	//End Registrations for type : UnityEngine.AI.NavMeshAgent

	//Start Registrations for type : UnityEngine.Animation

		//System.Boolean UnityEngine.Animation::Play(System.String,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_Play();
		Register_UnityEngine_Animation_Play();

		//System.Int32 UnityEngine.Animation::GetStateCount()
		void Register_UnityEngine_Animation_GetStateCount();
		Register_UnityEngine_Animation_GetStateCount();

		//System.Void UnityEngine.Animation::AddClip(UnityEngine.AnimationClip,System.String,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Animation_AddClip();
		Register_UnityEngine_Animation_AddClip();

		//System.Void UnityEngine.Animation::Blend(System.String,System.Single,System.Single)
		void Register_UnityEngine_Animation_Blend();
		Register_UnityEngine_Animation_Blend();

		//System.Void UnityEngine.Animation::CrossFade(System.String,System.Single,UnityEngine.PlayMode)
		void Register_UnityEngine_Animation_CrossFade();
		Register_UnityEngine_Animation_CrossFade();

		//System.Void UnityEngine.Animation::INTERNAL_CALL_Stop(UnityEngine.Animation)
		void Register_UnityEngine_Animation_INTERNAL_CALL_Stop();
		Register_UnityEngine_Animation_INTERNAL_CALL_Stop();

		//System.Void UnityEngine.Animation::Internal_RewindByName(System.String)
		void Register_UnityEngine_Animation_Internal_RewindByName();
		Register_UnityEngine_Animation_Internal_RewindByName();

		//System.Void UnityEngine.Animation::Internal_StopByName(System.String)
		void Register_UnityEngine_Animation_Internal_StopByName();
		Register_UnityEngine_Animation_Internal_StopByName();

		//UnityEngine.AnimationState UnityEngine.Animation::GetState(System.String)
		void Register_UnityEngine_Animation_GetState();
		Register_UnityEngine_Animation_GetState();

		//UnityEngine.AnimationState UnityEngine.Animation::GetStateAtIndex(System.Int32)
		void Register_UnityEngine_Animation_GetStateAtIndex();
		Register_UnityEngine_Animation_GetStateAtIndex();

	//End Registrations for type : UnityEngine.Animation

	//Start Registrations for type : UnityEngine.AnimationClip

		//System.Void UnityEngine.AnimationClip::Internal_CreateAnimationClip(UnityEngine.AnimationClip)
		void Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();
		Register_UnityEngine_AnimationClip_Internal_CreateAnimationClip();

		//System.Void UnityEngine.AnimationClip::SetCurve(System.String,System.Type,System.String,UnityEngine.AnimationCurve)
		void Register_UnityEngine_AnimationClip_SetCurve();
		Register_UnityEngine_AnimationClip_SetCurve();

	//End Registrations for type : UnityEngine.AnimationClip

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Int32 UnityEngine.AnimationCurve::get_length()
		void Register_UnityEngine_AnimationCurve_get_length();
		Register_UnityEngine_AnimationCurve_get_length();

		//System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
		void Register_UnityEngine_AnimationCurve_Evaluate();
		Register_UnityEngine_AnimationCurve_Evaluate();

		//System.Void UnityEngine.AnimationCurve::Cleanup()
		void Register_UnityEngine_AnimationCurve_Cleanup();
		Register_UnityEngine_AnimationCurve_Cleanup();

		//System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Init();
		Register_UnityEngine_AnimationCurve_Init();

		//System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_SetKeys();
		Register_UnityEngine_AnimationCurve_SetKeys();

		//System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_postWrapMode();
		Register_UnityEngine_AnimationCurve_set_postWrapMode();

		//System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationCurve_set_preWrapMode();
		Register_UnityEngine_AnimationCurve_set_preWrapMode();

		//UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
		void Register_UnityEngine_AnimationCurve_GetKeys();
		Register_UnityEngine_AnimationCurve_GetKeys();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
		void Register_UnityEngine_AnimationCurve_get_postWrapMode();
		Register_UnityEngine_AnimationCurve_get_postWrapMode();

		//UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
		void Register_UnityEngine_AnimationCurve_get_preWrapMode();
		Register_UnityEngine_AnimationCurve_get_preWrapMode();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.AnimationState

		//System.Boolean UnityEngine.AnimationState::get_enabled()
		void Register_UnityEngine_AnimationState_get_enabled();
		Register_UnityEngine_AnimationState_get_enabled();

		//System.Single UnityEngine.AnimationState::get_length()
		void Register_UnityEngine_AnimationState_get_length();
		Register_UnityEngine_AnimationState_get_length();

		//System.Single UnityEngine.AnimationState::get_time()
		void Register_UnityEngine_AnimationState_get_time();
		Register_UnityEngine_AnimationState_get_time();

		//System.String UnityEngine.AnimationState::get_name()
		void Register_UnityEngine_AnimationState_get_name();
		Register_UnityEngine_AnimationState_get_name();

		//System.Void UnityEngine.AnimationState::AddMixingTransform(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_AnimationState_AddMixingTransform();
		Register_UnityEngine_AnimationState_AddMixingTransform();

		//System.Void UnityEngine.AnimationState::set_blendMode(UnityEngine.AnimationBlendMode)
		void Register_UnityEngine_AnimationState_set_blendMode();
		Register_UnityEngine_AnimationState_set_blendMode();

		//System.Void UnityEngine.AnimationState::set_enabled(System.Boolean)
		void Register_UnityEngine_AnimationState_set_enabled();
		Register_UnityEngine_AnimationState_set_enabled();

		//System.Void UnityEngine.AnimationState::set_layer(System.Int32)
		void Register_UnityEngine_AnimationState_set_layer();
		Register_UnityEngine_AnimationState_set_layer();

		//System.Void UnityEngine.AnimationState::set_normalizedTime(System.Single)
		void Register_UnityEngine_AnimationState_set_normalizedTime();
		Register_UnityEngine_AnimationState_set_normalizedTime();

		//System.Void UnityEngine.AnimationState::set_speed(System.Single)
		void Register_UnityEngine_AnimationState_set_speed();
		Register_UnityEngine_AnimationState_set_speed();

		//System.Void UnityEngine.AnimationState::set_time(System.Single)
		void Register_UnityEngine_AnimationState_set_time();
		Register_UnityEngine_AnimationState_set_time();

		//System.Void UnityEngine.AnimationState::set_weight(System.Single)
		void Register_UnityEngine_AnimationState_set_weight();
		Register_UnityEngine_AnimationState_set_weight();

		//System.Void UnityEngine.AnimationState::set_wrapMode(UnityEngine.WrapMode)
		void Register_UnityEngine_AnimationState_set_wrapMode();
		Register_UnityEngine_AnimationState_set_wrapMode();

		//UnityEngine.WrapMode UnityEngine.AnimationState::get_wrapMode()
		void Register_UnityEngine_AnimationState_get_wrapMode();
		Register_UnityEngine_AnimationState_get_wrapMode();

	//End Registrations for type : UnityEngine.AnimationState

	//Start Registrations for type : UnityEngine.Animator

		//System.Boolean UnityEngine.Animator::CheckIfInIKPassInternal()
		void Register_UnityEngine_Animator_CheckIfInIKPassInternal();
		Register_UnityEngine_Animator_CheckIfInIKPassInternal();

		//System.Boolean UnityEngine.Animator::GetBoolID(System.Int32)
		void Register_UnityEngine_Animator_GetBoolID();
		Register_UnityEngine_Animator_GetBoolID();

		//System.Boolean UnityEngine.Animator::IsInTransition(System.Int32)
		void Register_UnityEngine_Animator_IsInTransition();
		Register_UnityEngine_Animator_IsInTransition();

		//System.Boolean UnityEngine.Animator::IsParameterControlledByCurveString(System.String)
		void Register_UnityEngine_Animator_IsParameterControlledByCurveString();
		Register_UnityEngine_Animator_IsParameterControlledByCurveString();

		//System.Boolean UnityEngine.Animator::get_applyRootMotion()
		void Register_UnityEngine_Animator_get_applyRootMotion();
		Register_UnityEngine_Animator_get_applyRootMotion();

		//System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
		void Register_UnityEngine_Animator_get_hasBoundPlayables();
		Register_UnityEngine_Animator_get_hasBoundPlayables();

		//System.Boolean UnityEngine.Animator::get_isHuman()
		void Register_UnityEngine_Animator_get_isHuman();
		Register_UnityEngine_Animator_get_isHuman();

		//System.Boolean UnityEngine.Animator::get_isMatchingTarget()
		void Register_UnityEngine_Animator_get_isMatchingTarget();
		Register_UnityEngine_Animator_get_isMatchingTarget();

		//System.Boolean UnityEngine.Animator::get_layersAffectMassCenter()
		void Register_UnityEngine_Animator_get_layersAffectMassCenter();
		Register_UnityEngine_Animator_get_layersAffectMassCenter();

		//System.Boolean UnityEngine.Animator::get_logWarnings()
		void Register_UnityEngine_Animator_get_logWarnings();
		Register_UnityEngine_Animator_get_logWarnings();

		//System.Int32 UnityEngine.Animator::GetIntegerID(System.Int32)
		void Register_UnityEngine_Animator_GetIntegerID();
		Register_UnityEngine_Animator_GetIntegerID();

		//System.Int32 UnityEngine.Animator::StringToHash(System.String)
		void Register_UnityEngine_Animator_StringToHash();
		Register_UnityEngine_Animator_StringToHash();

		//System.Int32 UnityEngine.Animator::get_layerCount()
		void Register_UnityEngine_Animator_get_layerCount();
		Register_UnityEngine_Animator_get_layerCount();

		//System.Single UnityEngine.Animator::GetFloatID(System.Int32)
		void Register_UnityEngine_Animator_GetFloatID();
		Register_UnityEngine_Animator_GetFloatID();

		//System.Single UnityEngine.Animator::GetIKPositionWeightInternal(UnityEngine.AvatarIKGoal)
		void Register_UnityEngine_Animator_GetIKPositionWeightInternal();
		Register_UnityEngine_Animator_GetIKPositionWeightInternal();

		//System.Single UnityEngine.Animator::GetIKRotationWeightInternal(UnityEngine.AvatarIKGoal)
		void Register_UnityEngine_Animator_GetIKRotationWeightInternal();
		Register_UnityEngine_Animator_GetIKRotationWeightInternal();

		//System.Single UnityEngine.Animator::GetLayerWeight(System.Int32)
		void Register_UnityEngine_Animator_GetLayerWeight();
		Register_UnityEngine_Animator_GetLayerWeight();

		//System.Single UnityEngine.Animator::get_feetPivotActive()
		void Register_UnityEngine_Animator_get_feetPivotActive();
		Register_UnityEngine_Animator_get_feetPivotActive();

		//System.Single UnityEngine.Animator::get_gravityWeight()
		void Register_UnityEngine_Animator_get_gravityWeight();
		Register_UnityEngine_Animator_get_gravityWeight();

		//System.Single UnityEngine.Animator::get_humanScale()
		void Register_UnityEngine_Animator_get_humanScale();
		Register_UnityEngine_Animator_get_humanScale();

		//System.Single UnityEngine.Animator::get_leftFeetBottomHeight()
		void Register_UnityEngine_Animator_get_leftFeetBottomHeight();
		Register_UnityEngine_Animator_get_leftFeetBottomHeight();

		//System.Single UnityEngine.Animator::get_pivotWeight()
		void Register_UnityEngine_Animator_get_pivotWeight();
		Register_UnityEngine_Animator_get_pivotWeight();

		//System.Single UnityEngine.Animator::get_playbackTime()
		void Register_UnityEngine_Animator_get_playbackTime();
		Register_UnityEngine_Animator_get_playbackTime();

		//System.Single UnityEngine.Animator::get_recorderStartTime()
		void Register_UnityEngine_Animator_get_recorderStartTime();
		Register_UnityEngine_Animator_get_recorderStartTime();

		//System.Single UnityEngine.Animator::get_recorderStopTime()
		void Register_UnityEngine_Animator_get_recorderStopTime();
		Register_UnityEngine_Animator_get_recorderStopTime();

		//System.Single UnityEngine.Animator::get_rightFeetBottomHeight()
		void Register_UnityEngine_Animator_get_rightFeetBottomHeight();
		Register_UnityEngine_Animator_get_rightFeetBottomHeight();

		//System.Single UnityEngine.Animator::get_speed()
		void Register_UnityEngine_Animator_get_speed();
		Register_UnityEngine_Animator_get_speed();

		//System.String UnityEngine.Animator::GetLayerName(System.Int32)
		void Register_UnityEngine_Animator_GetLayerName();
		Register_UnityEngine_Animator_GetLayerName();

		//System.Void UnityEngine.Animator::CrossFade(System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Animator_CrossFade();
		Register_UnityEngine_Animator_CrossFade();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetBodyPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetBodyPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetBodyRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetBodyRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetIKPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetIKPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_GetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_GetIKRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_GetIKRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_MatchTarget(UnityEngine.Animator,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.AvatarTarget,UnityEngine.MatchTargetWeightMask&,System.Single,System.Single)
		void Register_UnityEngine_Animator_INTERNAL_CALL_MatchTarget();
		Register_UnityEngine_Animator_INTERNAL_CALL_MatchTarget();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetBodyPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetBodyPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetBodyRotationInternal(UnityEngine.Animator,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetBodyRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetBodyRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKPositionInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetIKRotationInternal(UnityEngine.Animator,UnityEngine.AvatarIKGoal,UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetIKRotationInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetIKRotationInternal();

		//System.Void UnityEngine.Animator::INTERNAL_CALL_SetLookAtPositionInternal(UnityEngine.Animator,UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_CALL_SetLookAtPositionInternal();
		Register_UnityEngine_Animator_INTERNAL_CALL_SetLookAtPositionInternal();

		//System.Void UnityEngine.Animator::INTERNAL_get_deltaPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_deltaPosition();
		Register_UnityEngine_Animator_INTERNAL_get_deltaPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_deltaRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_deltaRotation();
		Register_UnityEngine_Animator_INTERNAL_get_deltaRotation();

		//System.Void UnityEngine.Animator::INTERNAL_get_pivotPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_pivotPosition();
		Register_UnityEngine_Animator_INTERNAL_get_pivotPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_rootPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_rootPosition();
		Register_UnityEngine_Animator_INTERNAL_get_rootPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_rootRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_rootRotation();
		Register_UnityEngine_Animator_INTERNAL_get_rootRotation();

		//System.Void UnityEngine.Animator::INTERNAL_get_targetPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Animator_INTERNAL_get_targetPosition();
		Register_UnityEngine_Animator_INTERNAL_get_targetPosition();

		//System.Void UnityEngine.Animator::INTERNAL_get_targetRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Animator_INTERNAL_get_targetRotation();
		Register_UnityEngine_Animator_INTERNAL_get_targetRotation();

		//System.Void UnityEngine.Animator::InterruptMatchTarget(System.Boolean)
		void Register_UnityEngine_Animator_InterruptMatchTarget();
		Register_UnityEngine_Animator_InterruptMatchTarget();

		//System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Animator_Play();
		Register_UnityEngine_Animator_Play();

		//System.Void UnityEngine.Animator::ResetTriggerString(System.String)
		void Register_UnityEngine_Animator_ResetTriggerString();
		Register_UnityEngine_Animator_ResetTriggerString();

		//System.Void UnityEngine.Animator::SetBoolID(System.Int32,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolID();
		Register_UnityEngine_Animator_SetBoolID();

		//System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
		void Register_UnityEngine_Animator_SetBoolString();
		Register_UnityEngine_Animator_SetBoolString();

		//System.Void UnityEngine.Animator::SetFloatID(System.Int32,System.Single)
		void Register_UnityEngine_Animator_SetFloatID();
		Register_UnityEngine_Animator_SetFloatID();

		//System.Void UnityEngine.Animator::SetFloatIDDamp(System.Int32,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Animator_SetFloatIDDamp();
		Register_UnityEngine_Animator_SetFloatIDDamp();

		//System.Void UnityEngine.Animator::SetIKPositionWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
		void Register_UnityEngine_Animator_SetIKPositionWeightInternal();
		Register_UnityEngine_Animator_SetIKPositionWeightInternal();

		//System.Void UnityEngine.Animator::SetIKRotationWeightInternal(UnityEngine.AvatarIKGoal,System.Single)
		void Register_UnityEngine_Animator_SetIKRotationWeightInternal();
		Register_UnityEngine_Animator_SetIKRotationWeightInternal();

		//System.Void UnityEngine.Animator::SetIntegerID(System.Int32,System.Int32)
		void Register_UnityEngine_Animator_SetIntegerID();
		Register_UnityEngine_Animator_SetIntegerID();

		//System.Void UnityEngine.Animator::SetLayerWeight(System.Int32,System.Single)
		void Register_UnityEngine_Animator_SetLayerWeight();
		Register_UnityEngine_Animator_SetLayerWeight();

		//System.Void UnityEngine.Animator::SetLookAtWeightInternal(System.Single,System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Animator_SetLookAtWeightInternal();
		Register_UnityEngine_Animator_SetLookAtWeightInternal();

		//System.Void UnityEngine.Animator::SetTarget(UnityEngine.AvatarTarget,System.Single)
		void Register_UnityEngine_Animator_SetTarget();
		Register_UnityEngine_Animator_SetTarget();

		//System.Void UnityEngine.Animator::SetTriggerString(System.String)
		void Register_UnityEngine_Animator_SetTriggerString();
		Register_UnityEngine_Animator_SetTriggerString();

		//System.Void UnityEngine.Animator::StartPlayback()
		void Register_UnityEngine_Animator_StartPlayback();
		Register_UnityEngine_Animator_StartPlayback();

		//System.Void UnityEngine.Animator::StartRecording(System.Int32)
		void Register_UnityEngine_Animator_StartRecording();
		Register_UnityEngine_Animator_StartRecording();

		//System.Void UnityEngine.Animator::StopPlayback()
		void Register_UnityEngine_Animator_StopPlayback();
		Register_UnityEngine_Animator_StopPlayback();

		//System.Void UnityEngine.Animator::StopRecording()
		void Register_UnityEngine_Animator_StopRecording();
		Register_UnityEngine_Animator_StopRecording();

		//System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
		void Register_UnityEngine_Animator_set_applyRootMotion();
		Register_UnityEngine_Animator_set_applyRootMotion();

		//System.Void UnityEngine.Animator::set_cullingMode(UnityEngine.AnimatorCullingMode)
		void Register_UnityEngine_Animator_set_cullingMode();
		Register_UnityEngine_Animator_set_cullingMode();

		//System.Void UnityEngine.Animator::set_feetPivotActive(System.Single)
		void Register_UnityEngine_Animator_set_feetPivotActive();
		Register_UnityEngine_Animator_set_feetPivotActive();

		//System.Void UnityEngine.Animator::set_layersAffectMassCenter(System.Boolean)
		void Register_UnityEngine_Animator_set_layersAffectMassCenter();
		Register_UnityEngine_Animator_set_layersAffectMassCenter();

		//System.Void UnityEngine.Animator::set_playbackTime(System.Single)
		void Register_UnityEngine_Animator_set_playbackTime();
		Register_UnityEngine_Animator_set_playbackTime();

		//System.Void UnityEngine.Animator::set_speed(System.Single)
		void Register_UnityEngine_Animator_set_speed();
		Register_UnityEngine_Animator_set_speed();

		//System.Void UnityEngine.Animator::set_stabilizeFeet(System.Boolean)
		void Register_UnityEngine_Animator_set_stabilizeFeet();
		Register_UnityEngine_Animator_set_stabilizeFeet();

		//UnityEngine.AnimatorCullingMode UnityEngine.Animator::get_cullingMode()
		void Register_UnityEngine_Animator_get_cullingMode();
		Register_UnityEngine_Animator_get_cullingMode();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetCurrentAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();
		Register_UnityEngine_Animator_GetCurrentAnimatorStateInfo();

		//UnityEngine.AnimatorStateInfo UnityEngine.Animator::GetNextAnimatorStateInfo(System.Int32)
		void Register_UnityEngine_Animator_GetNextAnimatorStateInfo();
		Register_UnityEngine_Animator_GetNextAnimatorStateInfo();

		//UnityEngine.AnimatorTransitionInfo UnityEngine.Animator::GetAnimatorTransitionInfo(System.Int32)
		void Register_UnityEngine_Animator_GetAnimatorTransitionInfo();
		Register_UnityEngine_Animator_GetAnimatorTransitionInfo();

		//UnityEngine.Transform UnityEngine.Animator::GetBoneTransformInternal(System.Int32)
		void Register_UnityEngine_Animator_GetBoneTransformInternal();
		Register_UnityEngine_Animator_GetBoneTransformInternal();

	//End Registrations for type : UnityEngine.Animator

	//Start Registrations for type : UnityEngine.Application

		//System.Boolean UnityEngine.Application::CanStreamedLevelBeLoaded(System.Int32)
		void Register_UnityEngine_Application_CanStreamedLevelBeLoaded();
		Register_UnityEngine_Application_CanStreamedLevelBeLoaded();

		//System.Boolean UnityEngine.Application::CanStreamedLevelBeLoadedByName(System.String)
		void Register_UnityEngine_Application_CanStreamedLevelBeLoadedByName();
		Register_UnityEngine_Application_CanStreamedLevelBeLoadedByName();

		//System.Boolean UnityEngine.Application::HasProLicense()
		void Register_UnityEngine_Application_HasProLicense();
		Register_UnityEngine_Application_HasProLicense();

		//System.Boolean UnityEngine.Application::get_genuine()
		void Register_UnityEngine_Application_get_genuine();
		Register_UnityEngine_Application_get_genuine();

		//System.Boolean UnityEngine.Application::get_genuineCheckAvailable()
		void Register_UnityEngine_Application_get_genuineCheckAvailable();
		Register_UnityEngine_Application_get_genuineCheckAvailable();

		//System.Boolean UnityEngine.Application::get_isEditor()
		void Register_UnityEngine_Application_get_isEditor();
		Register_UnityEngine_Application_get_isEditor();

		//System.Boolean UnityEngine.Application::get_isPlaying()
		void Register_UnityEngine_Application_get_isPlaying();
		Register_UnityEngine_Application_get_isPlaying();

		//System.Int32 UnityEngine.Application::get_targetFrameRate()
		void Register_UnityEngine_Application_get_targetFrameRate();
		Register_UnityEngine_Application_get_targetFrameRate();

		//System.String UnityEngine.Application::get_persistentDataPath()
		void Register_UnityEngine_Application_get_persistentDataPath();
		Register_UnityEngine_Application_get_persistentDataPath();

		//System.String UnityEngine.Application::get_unityVersion()
		void Register_UnityEngine_Application_get_unityVersion();
		Register_UnityEngine_Application_get_unityVersion();

		//System.Void UnityEngine.Application::CaptureScreenshot(System.String,System.Int32)
		void Register_UnityEngine_Application_CaptureScreenshot();
		Register_UnityEngine_Application_CaptureScreenshot();

		//System.Void UnityEngine.Application::Quit()
		void Register_UnityEngine_Application_Quit();
		Register_UnityEngine_Application_Quit();

		//System.Void UnityEngine.Application::SetLogCallbackDefined(System.Boolean)
		void Register_UnityEngine_Application_SetLogCallbackDefined();
		Register_UnityEngine_Application_SetLogCallbackDefined();

		//System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
		void Register_UnityEngine_Application_set_runInBackground();
		Register_UnityEngine_Application_set_runInBackground();

		//UnityEngine.NetworkReachability UnityEngine.Application::get_internetReachability()
		void Register_UnityEngine_Application_get_internetReachability();
		Register_UnityEngine_Application_get_internetReachability();

		//UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
		void Register_UnityEngine_Application_get_platform();
		Register_UnityEngine_Application_get_platform();

		//UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
		void Register_UnityEngine_Application_get_systemLanguage();
		Register_UnityEngine_Application_get_systemLanguage();

	//End Registrations for type : UnityEngine.Application

	//Start Registrations for type : UnityEngine.AssetBundleCreateRequest

		//System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
		void Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();
		Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();

		//UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
		void Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();
		Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();

	//End Registrations for type : UnityEngine.AssetBundleCreateRequest

	//Start Registrations for type : UnityEngine.AssetBundleRequest

		//UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
		void Register_UnityEngine_AssetBundleRequest_get_asset();
		Register_UnityEngine_AssetBundleRequest_get_asset();

		//UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
		void Register_UnityEngine_AssetBundleRequest_get_allAssets();
		Register_UnityEngine_AssetBundleRequest_get_allAssets();

	//End Registrations for type : UnityEngine.AssetBundleRequest

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
		void Register_UnityEngine_AsyncOperation_get_allowSceneActivation();
		Register_UnityEngine_AsyncOperation_get_allowSceneActivation();

		//System.Boolean UnityEngine.AsyncOperation::get_isDone()
		void Register_UnityEngine_AsyncOperation_get_isDone();
		Register_UnityEngine_AsyncOperation_get_isDone();

		//System.Int32 UnityEngine.AsyncOperation::get_priority()
		void Register_UnityEngine_AsyncOperation_get_priority();
		Register_UnityEngine_AsyncOperation_get_priority();

		//System.Single UnityEngine.AsyncOperation::get_progress()
		void Register_UnityEngine_AsyncOperation_get_progress();
		Register_UnityEngine_AsyncOperation_get_progress();

		//System.Void UnityEngine.AsyncOperation::InternalDestroy()
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

		//System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
		void Register_UnityEngine_AsyncOperation_set_allowSceneActivation();
		Register_UnityEngine_AsyncOperation_set_allowSceneActivation();

		//System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
		void Register_UnityEngine_AsyncOperation_set_priority();
		Register_UnityEngine_AsyncOperation_set_priority();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.AudioClip

		//System.Single UnityEngine.AudioClip::get_length()
		void Register_UnityEngine_AudioClip_get_length();
		Register_UnityEngine_AudioClip_get_length();

	//End Registrations for type : UnityEngine.AudioClip

	//Start Registrations for type : UnityEngine.AudioListener

		//System.Void UnityEngine.AudioListener::set_volume(System.Single)
		void Register_UnityEngine_AudioListener_set_volume();
		Register_UnityEngine_AudioListener_set_volume();

	//End Registrations for type : UnityEngine.AudioListener

	//Start Registrations for type : UnityEngine.AudioSource

		//System.Boolean UnityEngine.AudioSource::get_isPlaying()
		void Register_UnityEngine_AudioSource_get_isPlaying();
		Register_UnityEngine_AudioSource_get_isPlaying();

		//System.Single UnityEngine.AudioSource::get_pitch()
		void Register_UnityEngine_AudioSource_get_pitch();
		Register_UnityEngine_AudioSource_get_pitch();

		//System.Single UnityEngine.AudioSource::get_volume()
		void Register_UnityEngine_AudioSource_get_volume();
		Register_UnityEngine_AudioSource_get_volume();

		//System.Void UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)
		void Register_UnityEngine_AudioSource_INTERNAL_CALL_Pause();
		Register_UnityEngine_AudioSource_INTERNAL_CALL_Pause();

		//System.Void UnityEngine.AudioSource::Play(System.UInt64)
		void Register_UnityEngine_AudioSource_Play();
		Register_UnityEngine_AudioSource_Play();

		//System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
		void Register_UnityEngine_AudioSource_PlayOneShot();
		Register_UnityEngine_AudioSource_PlayOneShot();

		//System.Void UnityEngine.AudioSource::Stop()
		void Register_UnityEngine_AudioSource_Stop();
		Register_UnityEngine_AudioSource_Stop();

		//System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
		void Register_UnityEngine_AudioSource_set_clip();
		Register_UnityEngine_AudioSource_set_clip();

		//System.Void UnityEngine.AudioSource::set_loop(System.Boolean)
		void Register_UnityEngine_AudioSource_set_loop();
		Register_UnityEngine_AudioSource_set_loop();

		//System.Void UnityEngine.AudioSource::set_mute(System.Boolean)
		void Register_UnityEngine_AudioSource_set_mute();
		Register_UnityEngine_AudioSource_set_mute();

		//System.Void UnityEngine.AudioSource::set_pitch(System.Single)
		void Register_UnityEngine_AudioSource_set_pitch();
		Register_UnityEngine_AudioSource_set_pitch();

		//System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
		void Register_UnityEngine_AudioSource_set_playOnAwake();
		Register_UnityEngine_AudioSource_set_playOnAwake();

		//System.Void UnityEngine.AudioSource::set_spatialBlend(System.Single)
		void Register_UnityEngine_AudioSource_set_spatialBlend();
		Register_UnityEngine_AudioSource_set_spatialBlend();

		//System.Void UnityEngine.AudioSource::set_volume(System.Single)
		void Register_UnityEngine_AudioSource_set_volume();
		Register_UnityEngine_AudioSource_set_volume();

		//UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
		void Register_UnityEngine_AudioSource_get_clip();
		Register_UnityEngine_AudioSource_get_clip();

	//End Registrations for type : UnityEngine.AudioSource

	//Start Registrations for type : UnityEngine.Behaviour

		//System.Boolean UnityEngine.Behaviour::get_enabled()
		void Register_UnityEngine_Behaviour_get_enabled();
		Register_UnityEngine_Behaviour_get_enabled();

		//System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
		void Register_UnityEngine_Behaviour_get_isActiveAndEnabled();
		Register_UnityEngine_Behaviour_get_isActiveAndEnabled();

		//System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
		void Register_UnityEngine_Behaviour_set_enabled();
		Register_UnityEngine_Behaviour_set_enabled();

	//End Registrations for type : UnityEngine.Behaviour

	//Start Registrations for type : UnityEngine.BitStream

		//System.Boolean UnityEngine.BitStream::get_isReading()
		void Register_UnityEngine_BitStream_get_isReading();
		Register_UnityEngine_BitStream_get_isReading();

		//System.Boolean UnityEngine.BitStream::get_isWriting()
		void Register_UnityEngine_BitStream_get_isWriting();
		Register_UnityEngine_BitStream_get_isWriting();

		//System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializen(UnityEngine.BitStream,UnityEngine.NetworkViewID&)
		void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializen();
		Register_UnityEngine_BitStream_INTERNAL_CALL_Serializen();

		//System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializeq(UnityEngine.BitStream,UnityEngine.Quaternion&,System.Single)
		void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializeq();
		Register_UnityEngine_BitStream_INTERNAL_CALL_Serializeq();

		//System.Void UnityEngine.BitStream::INTERNAL_CALL_Serializev(UnityEngine.BitStream,UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_BitStream_INTERNAL_CALL_Serializev();
		Register_UnityEngine_BitStream_INTERNAL_CALL_Serializev();

		//System.Void UnityEngine.BitStream::Serialize(System.String&)
		void Register_UnityEngine_BitStream_Serialize();
		Register_UnityEngine_BitStream_Serialize();

		//System.Void UnityEngine.BitStream::Serializeb(System.Int32&)
		void Register_UnityEngine_BitStream_Serializeb();
		Register_UnityEngine_BitStream_Serializeb();

		//System.Void UnityEngine.BitStream::Serializec(System.Char&)
		void Register_UnityEngine_BitStream_Serializec();
		Register_UnityEngine_BitStream_Serializec();

		//System.Void UnityEngine.BitStream::Serializef(System.Single&,System.Single)
		void Register_UnityEngine_BitStream_Serializef();
		Register_UnityEngine_BitStream_Serializef();

		//System.Void UnityEngine.BitStream::Serializei(System.Int32&)
		void Register_UnityEngine_BitStream_Serializei();
		Register_UnityEngine_BitStream_Serializei();

		//System.Void UnityEngine.BitStream::Serializes(System.Int16&)
		void Register_UnityEngine_BitStream_Serializes();
		Register_UnityEngine_BitStream_Serializes();

	//End Registrations for type : UnityEngine.BitStream

	//Start Registrations for type : UnityEngine.Camera

		//System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCameras();
		Register_UnityEngine_Camera_GetAllCameras();

		//System.Int32 UnityEngine.Camera::get_allCamerasCount()
		void Register_UnityEngine_Camera_get_allCamerasCount();
		Register_UnityEngine_Camera_get_allCamerasCount();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Int32 UnityEngine.Camera::get_targetDisplay()
		void Register_UnityEngine_Camera_get_targetDisplay();
		Register_UnityEngine_Camera_get_targetDisplay();

		//System.Single UnityEngine.Camera::get_depth()
		void Register_UnityEngine_Camera_get_depth();
		Register_UnityEngine_Camera_get_depth();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_fieldOfView()
		void Register_UnityEngine_Camera_get_fieldOfView();
		Register_UnityEngine_Camera_get_fieldOfView();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToViewportPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenToWorldPoint();

		//System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint();
		Register_UnityEngine_Camera_INTERNAL_CALL_WorldToScreenPoint();

		//System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_get_pixelRect();

		//System.Void UnityEngine.Camera::INTERNAL_set_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();
		Register_UnityEngine_Camera_INTERNAL_set_backgroundColor();

		//System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
		void Register_UnityEngine_Camera_set_clearFlags();
		Register_UnityEngine_Camera_set_clearFlags();

		//System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
		void Register_UnityEngine_Camera_set_cullingMask();
		Register_UnityEngine_Camera_set_cullingMask();

		//System.Void UnityEngine.Camera::set_depth(System.Single)
		void Register_UnityEngine_Camera_set_depth();
		Register_UnityEngine_Camera_set_depth();

		//System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
		void Register_UnityEngine_Camera_set_fieldOfView();
		Register_UnityEngine_Camera_set_fieldOfView();

		//UnityEngine.Camera UnityEngine.Camera::get_main()
		void Register_UnityEngine_Camera_get_main();
		Register_UnityEngine_Camera_get_main();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Canvas

		//System.Boolean UnityEngine.Canvas::get_isRootCanvas()
		void Register_UnityEngine_Canvas_get_isRootCanvas();
		Register_UnityEngine_Canvas_get_isRootCanvas();

		//System.Boolean UnityEngine.Canvas::get_overrideSorting()
		void Register_UnityEngine_Canvas_get_overrideSorting();
		Register_UnityEngine_Canvas_get_overrideSorting();

		//System.Boolean UnityEngine.Canvas::get_pixelPerfect()
		void Register_UnityEngine_Canvas_get_pixelPerfect();
		Register_UnityEngine_Canvas_get_pixelPerfect();

		//System.Int32 UnityEngine.Canvas::get_renderOrder()
		void Register_UnityEngine_Canvas_get_renderOrder();
		Register_UnityEngine_Canvas_get_renderOrder();

		//System.Int32 UnityEngine.Canvas::get_sortingLayerID()
		void Register_UnityEngine_Canvas_get_sortingLayerID();
		Register_UnityEngine_Canvas_get_sortingLayerID();

		//System.Int32 UnityEngine.Canvas::get_sortingOrder()
		void Register_UnityEngine_Canvas_get_sortingOrder();
		Register_UnityEngine_Canvas_get_sortingOrder();

		//System.Int32 UnityEngine.Canvas::get_targetDisplay()
		void Register_UnityEngine_Canvas_get_targetDisplay();
		Register_UnityEngine_Canvas_get_targetDisplay();

		//System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
		void Register_UnityEngine_Canvas_get_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_get_referencePixelsPerUnit();

		//System.Single UnityEngine.Canvas::get_scaleFactor()
		void Register_UnityEngine_Canvas_get_scaleFactor();
		Register_UnityEngine_Canvas_get_scaleFactor();

		//System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
		void Register_UnityEngine_Canvas_set_overrideSorting();
		Register_UnityEngine_Canvas_set_overrideSorting();

		//System.Void UnityEngine.Canvas::set_pixelPerfect(System.Boolean)
		void Register_UnityEngine_Canvas_set_pixelPerfect();
		Register_UnityEngine_Canvas_set_pixelPerfect();

		//System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
		void Register_UnityEngine_Canvas_set_referencePixelsPerUnit();
		Register_UnityEngine_Canvas_set_referencePixelsPerUnit();

		//System.Void UnityEngine.Canvas::set_renderMode(UnityEngine.RenderMode)
		void Register_UnityEngine_Canvas_set_renderMode();
		Register_UnityEngine_Canvas_set_renderMode();

		//System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
		void Register_UnityEngine_Canvas_set_scaleFactor();
		Register_UnityEngine_Canvas_set_scaleFactor();

		//System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingLayerID();
		Register_UnityEngine_Canvas_set_sortingLayerID();

		//System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
		void Register_UnityEngine_Canvas_set_sortingOrder();
		Register_UnityEngine_Canvas_set_sortingOrder();

		//System.Void UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)
		void Register_UnityEngine_Canvas_set_worldCamera();
		Register_UnityEngine_Canvas_set_worldCamera();

		//UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
		void Register_UnityEngine_Canvas_get_worldCamera();
		Register_UnityEngine_Canvas_get_worldCamera();

		//UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
		void Register_UnityEngine_Canvas_get_rootCanvas();
		Register_UnityEngine_Canvas_get_rootCanvas();

		//UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
		void Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();
		Register_UnityEngine_Canvas_GetDefaultCanvasMaterial();

		//UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
		void Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();
		Register_UnityEngine_Canvas_GetETC1SupportedCanvasMaterial();

		//UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
		void Register_UnityEngine_Canvas_get_renderMode();
		Register_UnityEngine_Canvas_get_renderMode();

	//End Registrations for type : UnityEngine.Canvas

	//Start Registrations for type : UnityEngine.CanvasGroup

		//System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
		void Register_UnityEngine_CanvasGroup_get_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_get_blocksRaycasts();

		//System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
		void Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();
		Register_UnityEngine_CanvasGroup_get_ignoreParentGroups();

		//System.Boolean UnityEngine.CanvasGroup::get_interactable()
		void Register_UnityEngine_CanvasGroup_get_interactable();
		Register_UnityEngine_CanvasGroup_get_interactable();

		//System.Single UnityEngine.CanvasGroup::get_alpha()
		void Register_UnityEngine_CanvasGroup_get_alpha();
		Register_UnityEngine_CanvasGroup_get_alpha();

		//System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
		void Register_UnityEngine_CanvasGroup_set_alpha();
		Register_UnityEngine_CanvasGroup_set_alpha();

		//System.Void UnityEngine.CanvasGroup::set_blocksRaycasts(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_blocksRaycasts();
		Register_UnityEngine_CanvasGroup_set_blocksRaycasts();

		//System.Void UnityEngine.CanvasGroup::set_interactable(System.Boolean)
		void Register_UnityEngine_CanvasGroup_set_interactable();
		Register_UnityEngine_CanvasGroup_set_interactable();

	//End Registrations for type : UnityEngine.CanvasGroup

	//Start Registrations for type : UnityEngine.CanvasRenderer

		//System.Boolean UnityEngine.CanvasRenderer::get_cull()
		void Register_UnityEngine_CanvasRenderer_get_cull();
		Register_UnityEngine_CanvasRenderer_get_cull();

		//System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
		void Register_UnityEngine_CanvasRenderer_get_hasMoved();
		Register_UnityEngine_CanvasRenderer_get_hasMoved();

		//System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
		void Register_UnityEngine_CanvasRenderer_get_absoluteDepth();
		Register_UnityEngine_CanvasRenderer_get_absoluteDepth();

		//System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
		void Register_UnityEngine_CanvasRenderer_get_materialCount();
		Register_UnityEngine_CanvasRenderer_get_materialCount();

		//System.Void UnityEngine.CanvasRenderer::Clear()
		void Register_UnityEngine_CanvasRenderer_Clear();
		Register_UnityEngine_CanvasRenderer_Clear();

		//System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();
		Register_UnityEngine_CanvasRenderer_CreateUIVertexStreamInternal();

		//System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
		void Register_UnityEngine_CanvasRenderer_DisableRectClipping();
		Register_UnityEngine_CanvasRenderer_DisableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_EnableRectClipping();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_GetColor();

		//System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
		void Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();
		Register_UnityEngine_CanvasRenderer_INTERNAL_CALL_SetColor();

		//System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetAlphaTexture();
		Register_UnityEngine_CanvasRenderer_SetAlphaTexture();

		//System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetMaterial();
		Register_UnityEngine_CanvasRenderer_SetMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
		void Register_UnityEngine_CanvasRenderer_SetMesh();
		Register_UnityEngine_CanvasRenderer_SetMesh();

		//System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
		void Register_UnityEngine_CanvasRenderer_SetPopMaterial();
		Register_UnityEngine_CanvasRenderer_SetPopMaterial();

		//System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
		void Register_UnityEngine_CanvasRenderer_SetTexture();
		Register_UnityEngine_CanvasRenderer_SetTexture();

		//System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitIndiciesStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitIndiciesStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
		void Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();
		Register_UnityEngine_CanvasRenderer_SplitUIVertexStreamsInternal();

		//System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_cull();
		Register_UnityEngine_CanvasRenderer_set_cull();

		//System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
		void Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();
		Register_UnityEngine_CanvasRenderer_set_hasPopInstruction();

		//System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_materialCount();
		Register_UnityEngine_CanvasRenderer_set_materialCount();

		//System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
		void Register_UnityEngine_CanvasRenderer_set_popMaterialCount();
		Register_UnityEngine_CanvasRenderer_set_popMaterialCount();

	//End Registrations for type : UnityEngine.CanvasRenderer

	//Start Registrations for type : UnityEngine.CharacterController

		//System.Boolean UnityEngine.CharacterController::INTERNAL_CALL_SimpleMove(UnityEngine.CharacterController,UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_CALL_SimpleMove();
		Register_UnityEngine_CharacterController_INTERNAL_CALL_SimpleMove();

		//System.Boolean UnityEngine.CharacterController::get_isGrounded()
		void Register_UnityEngine_CharacterController_get_isGrounded();
		Register_UnityEngine_CharacterController_get_isGrounded();

		//System.Void UnityEngine.CharacterController::INTERNAL_set_center(UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_set_center();
		Register_UnityEngine_CharacterController_INTERNAL_set_center();

		//System.Void UnityEngine.CharacterController::set_detectCollisions(System.Boolean)
		void Register_UnityEngine_CharacterController_set_detectCollisions();
		Register_UnityEngine_CharacterController_set_detectCollisions();

		//System.Void UnityEngine.CharacterController::set_height(System.Single)
		void Register_UnityEngine_CharacterController_set_height();
		Register_UnityEngine_CharacterController_set_height();

		//System.Void UnityEngine.CharacterController::set_radius(System.Single)
		void Register_UnityEngine_CharacterController_set_radius();
		Register_UnityEngine_CharacterController_set_radius();

		//System.Void UnityEngine.CharacterController::set_slopeLimit(System.Single)
		void Register_UnityEngine_CharacterController_set_slopeLimit();
		Register_UnityEngine_CharacterController_set_slopeLimit();

		//System.Void UnityEngine.CharacterController::set_stepOffset(System.Single)
		void Register_UnityEngine_CharacterController_set_stepOffset();
		Register_UnityEngine_CharacterController_set_stepOffset();

		//UnityEngine.CollisionFlags UnityEngine.CharacterController::INTERNAL_CALL_Move(UnityEngine.CharacterController,UnityEngine.Vector3&)
		void Register_UnityEngine_CharacterController_INTERNAL_CALL_Move();
		Register_UnityEngine_CharacterController_INTERNAL_CALL_Move();

		//UnityEngine.CollisionFlags UnityEngine.CharacterController::get_collisionFlags()
		void Register_UnityEngine_CharacterController_get_collisionFlags();
		Register_UnityEngine_CharacterController_get_collisionFlags();

	//End Registrations for type : UnityEngine.CharacterController

	//Start Registrations for type : UnityEngine.Collider

		//UnityEngine.PhysicMaterial UnityEngine.Collider::get_material()
		void Register_UnityEngine_Collider_get_material();
		Register_UnityEngine_Collider_get_material();

		//UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
		void Register_UnityEngine_Collider_get_attachedRigidbody();
		Register_UnityEngine_Collider_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider

	//Start Registrations for type : UnityEngine.Collider2D

		//System.Int32 UnityEngine.Collider2D::get_shapeCount()
		void Register_UnityEngine_Collider2D_get_shapeCount();
		Register_UnityEngine_Collider2D_get_shapeCount();

		//System.Void UnityEngine.Collider2D::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Collider2D_INTERNAL_get_bounds();
		Register_UnityEngine_Collider2D_INTERNAL_get_bounds();

		//System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
		void Register_UnityEngine_Collider2D_set_isTrigger();
		Register_UnityEngine_Collider2D_set_isTrigger();

		//UnityEngine.PhysicsMaterial2D UnityEngine.Collider2D::get_sharedMaterial()
		void Register_UnityEngine_Collider2D_get_sharedMaterial();
		Register_UnityEngine_Collider2D_get_sharedMaterial();

		//UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
		void Register_UnityEngine_Collider2D_get_attachedRigidbody();
		Register_UnityEngine_Collider2D_get_attachedRigidbody();

	//End Registrations for type : UnityEngine.Collider2D

	//Start Registrations for type : UnityEngine.Component

		//System.Boolean UnityEngine.Component::CompareTag(System.String)
		void Register_UnityEngine_Component_CompareTag();
		Register_UnityEngine_Component_CompareTag();

		//System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_BroadcastMessage();
		Register_UnityEngine_Component_BroadcastMessage();

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
		void Register_UnityEngine_Component_GetComponentsForListInternal();
		Register_UnityEngine_Component_GetComponentsForListInternal();

		//System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_SendMessage();
		Register_UnityEngine_Component_SendMessage();

		//System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_Component_SendMessageUpwards();
		Register_UnityEngine_Component_SendMessageUpwards();

		//UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
		void Register_UnityEngine_Component_GetComponent();
		Register_UnityEngine_Component_GetComponent();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

		//UnityEngine.Transform UnityEngine.Component::get_transform()
		void Register_UnityEngine_Component_get_transform();
		Register_UnityEngine_Component_get_transform();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine()
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.CullingGroup

		//System.Void UnityEngine.CullingGroup::Dispose()
		void Register_UnityEngine_CullingGroup_Dispose();
		Register_UnityEngine_CullingGroup_Dispose();

		//System.Void UnityEngine.CullingGroup::FinalizerFailure()
		void Register_UnityEngine_CullingGroup_FinalizerFailure();
		Register_UnityEngine_CullingGroup_FinalizerFailure();

	//End Registrations for type : UnityEngine.CullingGroup

	//Start Registrations for type : UnityEngine.Cursor

		//System.Boolean UnityEngine.Cursor::get_visible()
		void Register_UnityEngine_Cursor_get_visible();
		Register_UnityEngine_Cursor_get_visible();

		//System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
		void Register_UnityEngine_Cursor_set_lockState();
		Register_UnityEngine_Cursor_set_lockState();

		//System.Void UnityEngine.Cursor::set_visible(System.Boolean)
		void Register_UnityEngine_Cursor_set_visible();
		Register_UnityEngine_Cursor_set_visible();

		//UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
		void Register_UnityEngine_Cursor_get_lockState();
		Register_UnityEngine_Cursor_get_lockState();

	//End Registrations for type : UnityEngine.Cursor

	//Start Registrations for type : UnityEngine.Debug

		//System.Boolean UnityEngine.Debug::get_isDebugBuild()
		void Register_UnityEngine_Debug_get_isDebugBuild();
		Register_UnityEngine_Debug_get_isDebugBuild();

		//System.Void UnityEngine.Debug::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
		void Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Debug_INTERNAL_CALL_DrawLine();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.DebugLogHandler

		//System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_Log();
		Register_UnityEngine_DebugLogHandler_Internal_Log();

		//System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
		void Register_UnityEngine_DebugLogHandler_Internal_LogException();
		Register_UnityEngine_DebugLogHandler_Internal_LogException();

	//End Registrations for type : UnityEngine.DebugLogHandler

	//Start Registrations for type : UnityEngine.Display

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.Event

		//System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
		void Register_UnityEngine_Event_PopEvent();
		Register_UnityEngine_Event_PopEvent();

		//System.Char UnityEngine.Event::get_character()
		void Register_UnityEngine_Event_get_character();
		Register_UnityEngine_Event_get_character();

		//System.Int32 UnityEngine.Event::get_clickCount()
		void Register_UnityEngine_Event_get_clickCount();
		Register_UnityEngine_Event_get_clickCount();

		//System.String UnityEngine.Event::get_commandName()
		void Register_UnityEngine_Event_get_commandName();
		Register_UnityEngine_Event_get_commandName();

		//System.Void UnityEngine.Event::Cleanup()
		void Register_UnityEngine_Event_Cleanup();
		Register_UnityEngine_Event_Cleanup();

		//System.Void UnityEngine.Event::Init(System.Int32)
		void Register_UnityEngine_Event_Init();
		Register_UnityEngine_Event_Init();

		//System.Void UnityEngine.Event::Internal_GetMouseDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMouseDelta();
		Register_UnityEngine_Event_Internal_GetMouseDelta();

		//System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
		void Register_UnityEngine_Event_Internal_GetMousePosition();
		Register_UnityEngine_Event_Internal_GetMousePosition();

		//System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
		void Register_UnityEngine_Event_Internal_SetNativeEvent();
		Register_UnityEngine_Event_Internal_SetNativeEvent();

		//System.Void UnityEngine.Event::Internal_Use()
		void Register_UnityEngine_Event_Internal_Use();
		Register_UnityEngine_Event_Internal_Use();

		//System.Void UnityEngine.Event::set_character(System.Char)
		void Register_UnityEngine_Event_set_character();
		Register_UnityEngine_Event_set_character();

		//System.Void UnityEngine.Event::set_displayIndex(System.Int32)
		void Register_UnityEngine_Event_set_displayIndex();
		Register_UnityEngine_Event_set_displayIndex();

		//System.Void UnityEngine.Event::set_keyCode(UnityEngine.KeyCode)
		void Register_UnityEngine_Event_set_keyCode();
		Register_UnityEngine_Event_set_keyCode();

		//System.Void UnityEngine.Event::set_modifiers(UnityEngine.EventModifiers)
		void Register_UnityEngine_Event_set_modifiers();
		Register_UnityEngine_Event_set_modifiers();

		//System.Void UnityEngine.Event::set_type(UnityEngine.EventType)
		void Register_UnityEngine_Event_set_type();
		Register_UnityEngine_Event_set_type();

		//UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
		void Register_UnityEngine_Event_get_modifiers();
		Register_UnityEngine_Event_get_modifiers();

		//UnityEngine.EventType UnityEngine.Event::GetTypeForControl(System.Int32)
		void Register_UnityEngine_Event_GetTypeForControl();
		Register_UnityEngine_Event_GetTypeForControl();

		//UnityEngine.EventType UnityEngine.Event::get_rawType()
		void Register_UnityEngine_Event_get_rawType();
		Register_UnityEngine_Event_get_rawType();

		//UnityEngine.EventType UnityEngine.Event::get_type()
		void Register_UnityEngine_Event_get_type();
		Register_UnityEngine_Event_get_type();

		//UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
		void Register_UnityEngine_Event_get_keyCode();
		Register_UnityEngine_Event_get_keyCode();

	//End Registrations for type : UnityEngine.Event

	//Start Registrations for type : UnityEngine.Experimental.Director.AnimationClipPlayable

		//System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK();

		//System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset();

		//System.Single UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetSpeed(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetSpeed();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetSpeed();

		//System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK();

		//System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset();

		//System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetSpeed(UnityEngine.Experimental.Director.PlayableHandle&,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_SetSpeed();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_SetSpeed();

		//UnityEngine.AnimationClip UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClip(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip();
		Register_UnityEngine_Experimental_Director_AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip();

	//End Registrations for type : UnityEngine.Experimental.Director.AnimationClipPlayable

	//Start Registrations for type : UnityEngine.Experimental.Director.AnimationOffsetPlayable

		//System.Void UnityEngine.Experimental.Director.AnimationOffsetPlayable::INTERNAL_CALL_GetPosition(UnityEngine.Experimental.Director.PlayableHandle&,UnityEngine.Vector3&)
		void Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_GetPosition();
		Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_GetPosition();

		//System.Void UnityEngine.Experimental.Director.AnimationOffsetPlayable::INTERNAL_CALL_GetRotation(UnityEngine.Experimental.Director.PlayableHandle&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_GetRotation();
		Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_GetRotation();

		//System.Void UnityEngine.Experimental.Director.AnimationOffsetPlayable::INTERNAL_CALL_SetPosition(UnityEngine.Experimental.Director.PlayableHandle&,UnityEngine.Vector3&)
		void Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_SetPosition();
		Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_SetPosition();

		//System.Void UnityEngine.Experimental.Director.AnimationOffsetPlayable::INTERNAL_CALL_SetRotation(UnityEngine.Experimental.Director.PlayableHandle&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_SetRotation();
		Register_UnityEngine_Experimental_Director_AnimationOffsetPlayable_INTERNAL_CALL_SetRotation();

	//End Registrations for type : UnityEngine.Experimental.Director.AnimationOffsetPlayable

	//Start Registrations for type : UnityEngine.Experimental.Director.AnimatorControllerPlayable

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetBoolID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetBoolID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetBoolID();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetBoolString(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetBoolString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetBoolString();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_HasStateInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_HasStateInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_HasStateInternal();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_IsInTransitionInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_IsInTransitionInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_IsInTransitionInternal();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_IsParameterControlledByCurveID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_IsParameterControlledByCurveID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_IsParameterControlledByCurveID();

		//System.Boolean UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_IsParameterControlledByCurveString(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_IsParameterControlledByCurveString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_IsParameterControlledByCurveString();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetAnimatorClipInfoCountInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorClipInfoCountInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorClipInfoCountInternal();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetIntegerID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetIntegerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetIntegerID();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetIntegerString(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetIntegerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetIntegerString();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetLayerCountInternal(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerCountInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerCountInternal();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetLayerIndexInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerIndexInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerIndexInternal();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetParameterCountInternal(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetParameterCountInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetParameterCountInternal();

		//System.Int32 UnityEngine.Experimental.Director.AnimatorControllerPlayable::StringToHash(System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_StringToHash();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_StringToHash();

		//System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetFloatID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetFloatID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetFloatID();

		//System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetFloatString(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetFloatString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetFloatString();

		//System.Single UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetLayerWeightInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerWeightInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerWeightInternal();

		//System.String UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetLayerNameInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerNameInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetLayerNameInternal();

		//System.String UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_ResolveHashInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_ResolveHashInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_ResolveHashInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_CrossFadeInFixedTimeInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_CrossFadeInFixedTimeInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_CrossFadeInFixedTimeInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_CrossFadeInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Single,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_CrossFadeInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_CrossFadeInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetAnimatorClipInfoInternal(UnityEngine.Experimental.Director.AnimatorControllerPlayable,UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Boolean,System.Object)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorClipInfoInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorClipInfoInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_PlayInFixedTimeInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_PlayInFixedTimeInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_PlayInFixedTimeInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_PlayInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_PlayInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_PlayInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_ResetTriggerID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_ResetTriggerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_ResetTriggerID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_ResetTriggerString(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_ResetTriggerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_ResetTriggerString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetBoolID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetBoolID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetBoolID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetBoolString(UnityEngine.Experimental.Director.PlayableHandle&,System.String,System.Boolean)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetBoolString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetBoolString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetFloatID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetFloatID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetFloatID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetFloatString(UnityEngine.Experimental.Director.PlayableHandle&,System.String,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetFloatString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetFloatString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetIntegerID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetIntegerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetIntegerID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetIntegerString(UnityEngine.Experimental.Director.PlayableHandle&,System.String,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetIntegerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetIntegerString();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetLayerWeightInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32,System.Single)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetLayerWeightInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetLayerWeightInternal();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetTriggerID(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetTriggerID();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetTriggerID();

		//System.Void UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_SetTriggerString(UnityEngine.Experimental.Director.PlayableHandle&,System.String)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetTriggerString();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_SetTriggerString();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetCurrentAnimatorClipInfoInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetCurrentAnimatorClipInfoInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetCurrentAnimatorClipInfoInternal();

		//UnityEngine.AnimatorClipInfo[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetNextAnimatorClipInfoInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetNextAnimatorClipInfoInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetNextAnimatorClipInfoInternal();

		//UnityEngine.AnimatorControllerParameter[] UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetParametersArrayInternal(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetParametersArrayInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetParametersArrayInternal();

		//UnityEngine.AnimatorStateInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetCurrentAnimatorStateInfoInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetCurrentAnimatorStateInfoInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetCurrentAnimatorStateInfoInternal();

		//UnityEngine.AnimatorStateInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetNextAnimatorStateInfoInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetNextAnimatorStateInfoInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetNextAnimatorStateInfoInternal();

		//UnityEngine.AnimatorTransitionInfo UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetAnimatorTransitionInfoInternal(UnityEngine.Experimental.Director.PlayableHandle&,System.Int32)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorTransitionInfoInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorTransitionInfoInternal();

		//UnityEngine.RuntimeAnimatorController UnityEngine.Experimental.Director.AnimatorControllerPlayable::INTERNAL_CALL_GetAnimatorControllerInternal(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorControllerInternal();
		Register_UnityEngine_Experimental_Director_AnimatorControllerPlayable_INTERNAL_CALL_GetAnimatorControllerInternal();

	//End Registrations for type : UnityEngine.Experimental.Director.AnimatorControllerPlayable

	//Start Registrations for type : UnityEngine.Experimental.Director.PlayableHandle

		//System.Boolean UnityEngine.Experimental.Director.PlayableHandle::INTERNAL_CALL_IsValidInternal(UnityEngine.Experimental.Director.PlayableHandle&)
		void Register_UnityEngine_Experimental_Director_PlayableHandle_INTERNAL_CALL_IsValidInternal();
		Register_UnityEngine_Experimental_Director_PlayableHandle_INTERNAL_CALL_IsValidInternal();

	//End Registrations for type : UnityEngine.Experimental.Director.PlayableHandle

	//Start Registrations for type : UnityEngine.Font

		//System.Boolean UnityEngine.Font::HasCharacter(System.Char)
		void Register_UnityEngine_Font_HasCharacter();
		Register_UnityEngine_Font_HasCharacter();

		//System.Boolean UnityEngine.Font::get_dynamic()
		void Register_UnityEngine_Font_get_dynamic();
		Register_UnityEngine_Font_get_dynamic();

		//System.Int32 UnityEngine.Font::get_fontSize()
		void Register_UnityEngine_Font_get_fontSize();
		Register_UnityEngine_Font_get_fontSize();

		//UnityEngine.Material UnityEngine.Font::get_material()
		void Register_UnityEngine_Font_get_material();
		Register_UnityEngine_Font_get_material();

	//End Registrations for type : UnityEngine.Font

	//Start Registrations for type : UnityEngine.GameObject

		//System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
		void Register_UnityEngine_GameObject_GetComponentsInternal();
		Register_UnityEngine_GameObject_GetComponentsInternal();

		//System.Boolean UnityEngine.GameObject::CompareTag(System.String)
		void Register_UnityEngine_GameObject_CompareTag();
		Register_UnityEngine_GameObject_CompareTag();

		//System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
		void Register_UnityEngine_GameObject_get_activeInHierarchy();
		Register_UnityEngine_GameObject_get_activeInHierarchy();

		//System.Boolean UnityEngine.GameObject::get_activeSelf()
		void Register_UnityEngine_GameObject_get_activeSelf();
		Register_UnityEngine_GameObject_get_activeSelf();

		//System.Int32 UnityEngine.GameObject::get_layer()
		void Register_UnityEngine_GameObject_get_layer();
		Register_UnityEngine_GameObject_get_layer();

		//System.String UnityEngine.GameObject::get_tag()
		void Register_UnityEngine_GameObject_get_tag();
		Register_UnityEngine_GameObject_get_tag();

		//System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_BroadcastMessage();
		Register_UnityEngine_GameObject_BroadcastMessage();

		//System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_GameObject_GetComponentFastPath();
		Register_UnityEngine_GameObject_GetComponentFastPath();

		//System.Void UnityEngine.GameObject::INTERNAL_get_scene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_GameObject_INTERNAL_get_scene();
		Register_UnityEngine_GameObject_INTERNAL_get_scene();

		//System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
		void Register_UnityEngine_GameObject_Internal_CreateGameObject();
		Register_UnityEngine_GameObject_Internal_CreateGameObject();

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

		//System.Void UnityEngine.GameObject::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessageUpwards();
		Register_UnityEngine_GameObject_SendMessageUpwards();

		//System.Void UnityEngine.GameObject::SetActive(System.Boolean)
		void Register_UnityEngine_GameObject_SetActive();
		Register_UnityEngine_GameObject_SetActive();

		//System.Void UnityEngine.GameObject::set_layer(System.Int32)
		void Register_UnityEngine_GameObject_set_layer();
		Register_UnityEngine_GameObject_set_layer();

		//System.Void UnityEngine.GameObject::set_tag(System.String)
		void Register_UnityEngine_GameObject_set_tag();
		Register_UnityEngine_GameObject_set_tag();

		//UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
		void Register_UnityEngine_GameObject_GetComponent();
		Register_UnityEngine_GameObject_GetComponent();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
		void Register_UnityEngine_GameObject_GetComponentInChildren();
		Register_UnityEngine_GameObject_GetComponentInChildren();

		//UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
		void Register_UnityEngine_GameObject_GetComponentInParent();
		Register_UnityEngine_GameObject_GetComponentInParent();

		//UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
		void Register_UnityEngine_GameObject_Internal_AddComponentWithType();
		Register_UnityEngine_GameObject_Internal_AddComponentWithType();

		//UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
		void Register_UnityEngine_GameObject_Find();
		Register_UnityEngine_GameObject_Find();

		//UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectWithTag();
		Register_UnityEngine_GameObject_FindGameObjectWithTag();

		//UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
		void Register_UnityEngine_GameObject_FindGameObjectsWithTag();
		Register_UnityEngine_GameObject_FindGameObjectsWithTag();

		//UnityEngine.Transform UnityEngine.GameObject::get_transform()
		void Register_UnityEngine_GameObject_get_transform();
		Register_UnityEngine_GameObject_get_transform();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.Gizmos

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawCube();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawCube();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawIcon(UnityEngine.Vector3&,System.String,System.Boolean)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawIcon();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawIcon();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawLine();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireCube();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireCube();

		//System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireSphere(UnityEngine.Vector3&,System.Single)
		void Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();
		Register_UnityEngine_Gizmos_INTERNAL_CALL_DrawWireSphere();

		//System.Void UnityEngine.Gizmos::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_get_color();
		Register_UnityEngine_Gizmos_INTERNAL_get_color();

		//System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Gizmos_INTERNAL_set_color();
		Register_UnityEngine_Gizmos_INTERNAL_set_color();

	//End Registrations for type : UnityEngine.Gizmos

	//Start Registrations for type : UnityEngine.Gradient

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.Graphics

		//System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
		void Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();
		Register_UnityEngine_Graphics_Internal_GetMaxDrawMeshInstanceCount();

		//System.Void UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)
		void Register_UnityEngine_Graphics_Internal_DrawTexture();
		Register_UnityEngine_Graphics_Internal_DrawTexture();

	//End Registrations for type : UnityEngine.Graphics

	//Start Registrations for type : UnityEngine.GUI

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoButton();

		//System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoToggle(UnityEngine.Rect&,System.Int32,System.Boolean,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoToggle();

		//System.Boolean UnityEngine.GUI::get_changed()
		void Register_UnityEngine_GUI_get_changed();
		Register_UnityEngine_GUI_get_changed();

		//System.Boolean UnityEngine.GUI::get_enabled()
		void Register_UnityEngine_GUI_get_enabled();
		Register_UnityEngine_GUI_get_enabled();

		//System.Boolean UnityEngine.GUI::get_usePageScrollbars()
		void Register_UnityEngine_GUI_get_usePageScrollbars();
		Register_UnityEngine_GUI_get_usePageScrollbars();

		//System.String UnityEngine.GUI::Internal_GetTooltip()
		void Register_UnityEngine_GUI_Internal_GetTooltip();
		Register_UnityEngine_GUI_Internal_GetTooltip();

		//System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
		void Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();
		Register_UnityEngine_GUI_INTERNAL_CALL_DoLabel();

		//System.Void UnityEngine.GUI::INTERNAL_get_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_backgroundColor();
		Register_UnityEngine_GUI_INTERNAL_get_backgroundColor();

		//System.Void UnityEngine.GUI::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_color();
		Register_UnityEngine_GUI_INTERNAL_get_color();

		//System.Void UnityEngine.GUI::INTERNAL_get_contentColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_get_contentColor();
		Register_UnityEngine_GUI_INTERNAL_get_contentColor();

		//System.Void UnityEngine.GUI::INTERNAL_set_backgroundColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_backgroundColor();
		Register_UnityEngine_GUI_INTERNAL_set_backgroundColor();

		//System.Void UnityEngine.GUI::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_color();
		Register_UnityEngine_GUI_INTERNAL_set_color();

		//System.Void UnityEngine.GUI::INTERNAL_set_contentColor(UnityEngine.Color&)
		void Register_UnityEngine_GUI_INTERNAL_set_contentColor();
		Register_UnityEngine_GUI_INTERNAL_set_contentColor();

		//System.Void UnityEngine.GUI::InternalRepaintEditorWindow()
		void Register_UnityEngine_GUI_InternalRepaintEditorWindow();
		Register_UnityEngine_GUI_InternalRepaintEditorWindow();

		//System.Void UnityEngine.GUI::Internal_SetTooltip(System.String)
		void Register_UnityEngine_GUI_Internal_SetTooltip();
		Register_UnityEngine_GUI_Internal_SetTooltip();

		//System.Void UnityEngine.GUI::set_changed(System.Boolean)
		void Register_UnityEngine_GUI_set_changed();
		Register_UnityEngine_GUI_set_changed();

		//System.Void UnityEngine.GUI::set_depth(System.Int32)
		void Register_UnityEngine_GUI_set_depth();
		Register_UnityEngine_GUI_set_depth();

		//UnityEngine.Material UnityEngine.GUI::get_blendMaterial()
		void Register_UnityEngine_GUI_get_blendMaterial();
		Register_UnityEngine_GUI_get_blendMaterial();

		//UnityEngine.Material UnityEngine.GUI::get_blitMaterial()
		void Register_UnityEngine_GUI_get_blitMaterial();
		Register_UnityEngine_GUI_get_blitMaterial();

	//End Registrations for type : UnityEngine.GUI

	//Start Registrations for type : UnityEngine.GUIClip

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_GetMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_GetMatrix();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Internal_Push(UnityEngine.Rect&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Boolean)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Internal_Push();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Internal_Push();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_SetMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_SetMatrix();

		//System.Void UnityEngine.GUIClip::INTERNAL_CALL_Unclip_Vector2(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2();
		Register_UnityEngine_GUIClip_INTERNAL_CALL_Unclip_Vector2();

		//System.Void UnityEngine.GUIClip::Internal_Pop()
		void Register_UnityEngine_GUIClip_Internal_Pop();
		Register_UnityEngine_GUIClip_Internal_Pop();

	//End Registrations for type : UnityEngine.GUIClip

	//Start Registrations for type : UnityEngine.GUIElement

		//System.Boolean UnityEngine.GUIElement::INTERNAL_CALL_HitTest(UnityEngine.GUIElement,UnityEngine.Vector3&,UnityEngine.Camera)
		void Register_UnityEngine_GUIElement_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUIElement_INTERNAL_CALL_HitTest();

		//System.Void UnityEngine.GUIElement::INTERNAL_CALL_GetScreenRect(UnityEngine.GUIElement,UnityEngine.Camera,UnityEngine.Rect&)
		void Register_UnityEngine_GUIElement_INTERNAL_CALL_GetScreenRect();
		Register_UnityEngine_GUIElement_INTERNAL_CALL_GetScreenRect();

	//End Registrations for type : UnityEngine.GUIElement

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
		void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.GUILayoutUtility

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_GetWindowRect(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_GetWindowRect();

		//System.Void UnityEngine.GUILayoutUtility::INTERNAL_CALL_Internal_MoveWindow(System.Int32,UnityEngine.Rect&)
		void Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();
		Register_UnityEngine_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow();

	//End Registrations for type : UnityEngine.GUILayoutUtility

	//Start Registrations for type : UnityEngine.GUISettings

		//System.Single UnityEngine.GUISettings::Internal_GetCursorFlashSpeed()
		void Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed();
		Register_UnityEngine_GUISettings_Internal_GetCursorFlashSpeed();

	//End Registrations for type : UnityEngine.GUISettings

	//Start Registrations for type : UnityEngine.GUIStyle

		//System.Boolean UnityEngine.GUIStyle::get_richText()
		void Register_UnityEngine_GUIStyle_get_richText();
		Register_UnityEngine_GUIStyle_get_richText();

		//System.Boolean UnityEngine.GUIStyle::get_stretchHeight()
		void Register_UnityEngine_GUIStyle_get_stretchHeight();
		Register_UnityEngine_GUIStyle_get_stretchHeight();

		//System.Boolean UnityEngine.GUIStyle::get_stretchWidth()
		void Register_UnityEngine_GUIStyle_get_stretchWidth();
		Register_UnityEngine_GUIStyle_get_stretchWidth();

		//System.Boolean UnityEngine.GUIStyle::get_wordWrap()
		void Register_UnityEngine_GUIStyle_get_wordWrap();
		Register_UnityEngine_GUIStyle_get_wordWrap();

		//System.Int32 UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorStringIndex(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorStringIndex();

		//System.Int32 UnityEngine.GUIStyle::Internal_GetNumCharactersThatFitWithinWidth(System.IntPtr,System.String,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_GetNumCharactersThatFitWithinWidth();
		Register_UnityEngine_GUIStyle_Internal_GetNumCharactersThatFitWithinWidth();

		//System.Int32 UnityEngine.GUIStyle::get_fontSize()
		void Register_UnityEngine_GUIStyle_get_fontSize();
		Register_UnityEngine_GUIStyle_get_fontSize();

		//System.Single UnityEngine.GUIStyle::Internal_CalcHeight(System.IntPtr,UnityEngine.GUIContent,System.Single)
		void Register_UnityEngine_GUIStyle_Internal_CalcHeight();
		Register_UnityEngine_GUIStyle_Internal_CalcHeight();

		//System.Single UnityEngine.GUIStyle::Internal_GetCursorFlashOffset()
		void Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset();
		Register_UnityEngine_GUIStyle_Internal_GetCursorFlashOffset();

		//System.Single UnityEngine.GUIStyle::Internal_GetLineHeight(System.IntPtr)
		void Register_UnityEngine_GUIStyle_Internal_GetLineHeight();
		Register_UnityEngine_GUIStyle_Internal_GetLineHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedHeight()
		void Register_UnityEngine_GUIStyle_get_fixedHeight();
		Register_UnityEngine_GUIStyle_get_fixedHeight();

		//System.Single UnityEngine.GUIStyle::get_fixedWidth()
		void Register_UnityEngine_GUIStyle_get_fixedWidth();
		Register_UnityEngine_GUIStyle_get_fixedWidth();

		//System.String UnityEngine.GUIStyle::get_name()
		void Register_UnityEngine_GUIStyle_get_name();
		Register_UnityEngine_GUIStyle_get_name();

		//System.Void UnityEngine.GUIStyle::AssignRectOffset(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignRectOffset();
		Register_UnityEngine_GUIStyle_AssignRectOffset();

		//System.Void UnityEngine.GUIStyle::AssignStyleState(System.Int32,System.IntPtr)
		void Register_UnityEngine_GUIStyle_AssignStyleState();
		Register_UnityEngine_GUIStyle_AssignStyleState();

		//System.Void UnityEngine.GUIStyle::Cleanup()
		void Register_UnityEngine_GUIStyle_Cleanup();
		Register_UnityEngine_GUIStyle_Cleanup();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_GetRectOffsetPtr(UnityEngine.GUIStyle,System.Int32,System.IntPtr&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetRectOffsetPtr();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_GetStyleStatePtr(UnityEngine.GUIStyle,System.Int32,System.IntPtr&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetStyleStatePtr();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_GetStyleStatePtr();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_CalcSizeWithConstraints(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_CalcSizeWithConstraints();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_Draw2(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_Draw2();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawCursor(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawCursor();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawCursor();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_DrawPrefixLabel(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,System.Boolean)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawPrefixLabel();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_DrawPrefixLabel();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_Internal_GetCursorPixelPosition(System.IntPtr,UnityEngine.Rect&,UnityEngine.GUIContent,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition();

		//System.Void UnityEngine.GUIStyle::INTERNAL_CALL_SetMouseTooltip(UnityEngine.GUIStyle,System.String,UnityEngine.Rect&)
		void Register_UnityEngine_GUIStyle_INTERNAL_CALL_SetMouseTooltip();
		Register_UnityEngine_GUIStyle_INTERNAL_CALL_SetMouseTooltip();

		//System.Void UnityEngine.GUIStyle::INTERNAL_get_Internal_clipOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_get_Internal_clipOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_get_Internal_clipOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_get_contentOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_get_contentOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_set_Internal_clipOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_set_Internal_clipOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_set_Internal_clipOffset();

		//System.Void UnityEngine.GUIStyle::INTERNAL_set_contentOffset(UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset();
		Register_UnityEngine_GUIStyle_INTERNAL_set_contentOffset();

		//System.Void UnityEngine.GUIStyle::Init()
		void Register_UnityEngine_GUIStyle_Init();
		Register_UnityEngine_GUIStyle_Init();

		//System.Void UnityEngine.GUIStyle::InitCopy(UnityEngine.GUIStyle)
		void Register_UnityEngine_GUIStyle_InitCopy();
		Register_UnityEngine_GUIStyle_InitCopy();

		//System.Void UnityEngine.GUIStyle::Internal_CalcMinMaxWidth(System.IntPtr,UnityEngine.GUIContent,System.Single&,System.Single&)
		void Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();
		Register_UnityEngine_GUIStyle_Internal_CalcMinMaxWidth();

		//System.Void UnityEngine.GUIStyle::Internal_CalcSize(System.IntPtr,UnityEngine.GUIContent,UnityEngine.Vector2&)
		void Register_UnityEngine_GUIStyle_Internal_CalcSize();
		Register_UnityEngine_GUIStyle_Internal_CalcSize();

		//System.Void UnityEngine.GUIStyle::Internal_Draw(UnityEngine.GUIContent,UnityEngine.Internal_DrawArguments&)
		void Register_UnityEngine_GUIStyle_Internal_Draw();
		Register_UnityEngine_GUIStyle_Internal_Draw();

		//System.Void UnityEngine.GUIStyle::Internal_DrawWithTextSelection(UnityEngine.GUIContent,UnityEngine.Internal_DrawWithTextSelectionArguments&)
		void Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection();
		Register_UnityEngine_GUIStyle_Internal_DrawWithTextSelection();

		//System.Void UnityEngine.GUIStyle::SetDefaultFont(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetDefaultFont();
		Register_UnityEngine_GUIStyle_SetDefaultFont();

		//System.Void UnityEngine.GUIStyle::SetFontInternal(UnityEngine.Font)
		void Register_UnityEngine_GUIStyle_SetFontInternal();
		Register_UnityEngine_GUIStyle_SetFontInternal();

		//System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
		void Register_UnityEngine_GUIStyle_set_alignment();
		Register_UnityEngine_GUIStyle_set_alignment();

		//System.Void UnityEngine.GUIStyle::set_clipping(UnityEngine.TextClipping)
		void Register_UnityEngine_GUIStyle_set_clipping();
		Register_UnityEngine_GUIStyle_set_clipping();

		//System.Void UnityEngine.GUIStyle::set_fixedHeight(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedHeight();
		Register_UnityEngine_GUIStyle_set_fixedHeight();

		//System.Void UnityEngine.GUIStyle::set_fixedWidth(System.Single)
		void Register_UnityEngine_GUIStyle_set_fixedWidth();
		Register_UnityEngine_GUIStyle_set_fixedWidth();

		//System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
		void Register_UnityEngine_GUIStyle_set_fontSize();
		Register_UnityEngine_GUIStyle_set_fontSize();

		//System.Void UnityEngine.GUIStyle::set_fontStyle(UnityEngine.FontStyle)
		void Register_UnityEngine_GUIStyle_set_fontStyle();
		Register_UnityEngine_GUIStyle_set_fontStyle();

		//System.Void UnityEngine.GUIStyle::set_imagePosition(UnityEngine.ImagePosition)
		void Register_UnityEngine_GUIStyle_set_imagePosition();
		Register_UnityEngine_GUIStyle_set_imagePosition();

		//System.Void UnityEngine.GUIStyle::set_name(System.String)
		void Register_UnityEngine_GUIStyle_set_name();
		Register_UnityEngine_GUIStyle_set_name();

		//System.Void UnityEngine.GUIStyle::set_richText(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_richText();
		Register_UnityEngine_GUIStyle_set_richText();

		//System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchHeight();
		Register_UnityEngine_GUIStyle_set_stretchHeight();

		//System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_stretchWidth();
		Register_UnityEngine_GUIStyle_set_stretchWidth();

		//System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
		void Register_UnityEngine_GUIStyle_set_wordWrap();
		Register_UnityEngine_GUIStyle_set_wordWrap();

		//UnityEngine.Font UnityEngine.GUIStyle::GetFontInternal()
		void Register_UnityEngine_GUIStyle_GetFontInternal();
		Register_UnityEngine_GUIStyle_GetFontInternal();

		//UnityEngine.Font UnityEngine.GUIStyle::GetFontInternalDuringLoadingThread()
		void Register_UnityEngine_GUIStyle_GetFontInternalDuringLoadingThread();
		Register_UnityEngine_GUIStyle_GetFontInternalDuringLoadingThread();

		//UnityEngine.FontStyle UnityEngine.GUIStyle::get_fontStyle()
		void Register_UnityEngine_GUIStyle_get_fontStyle();
		Register_UnityEngine_GUIStyle_get_fontStyle();

		//UnityEngine.ImagePosition UnityEngine.GUIStyle::get_imagePosition()
		void Register_UnityEngine_GUIStyle_get_imagePosition();
		Register_UnityEngine_GUIStyle_get_imagePosition();

		//UnityEngine.TextAnchor UnityEngine.GUIStyle::get_alignment()
		void Register_UnityEngine_GUIStyle_get_alignment();
		Register_UnityEngine_GUIStyle_get_alignment();

		//UnityEngine.TextClipping UnityEngine.GUIStyle::get_clipping()
		void Register_UnityEngine_GUIStyle_get_clipping();
		Register_UnityEngine_GUIStyle_get_clipping();

	//End Registrations for type : UnityEngine.GUIStyle

	//Start Registrations for type : UnityEngine.GUIStyleState

		//System.Void UnityEngine.GUIStyleState::Cleanup()
		void Register_UnityEngine_GUIStyleState_Cleanup();
		Register_UnityEngine_GUIStyleState_Cleanup();

		//System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
		void Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();
		Register_UnityEngine_GUIStyleState_INTERNAL_set_textColor();

		//System.Void UnityEngine.GUIStyleState::Init()
		void Register_UnityEngine_GUIStyleState_Init();
		Register_UnityEngine_GUIStyleState_Init();

		//System.Void UnityEngine.GUIStyleState::SetBackgroundInternal(UnityEngine.Texture2D)
		void Register_UnityEngine_GUIStyleState_SetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_SetBackgroundInternal();

		//UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
		void Register_UnityEngine_GUIStyleState_GetBackgroundInternal();
		Register_UnityEngine_GUIStyleState_GetBackgroundInternal();

		//UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternalFromDeserialization()
		void Register_UnityEngine_GUIStyleState_GetBackgroundInternalFromDeserialization();
		Register_UnityEngine_GUIStyleState_GetBackgroundInternalFromDeserialization();

	//End Registrations for type : UnityEngine.GUIStyleState

	//Start Registrations for type : UnityEngine.GUIText

		//System.Void UnityEngine.GUIText::set_text(System.String)
		void Register_UnityEngine_GUIText_set_text();
		Register_UnityEngine_GUIText_set_text();

		//UnityEngine.Material UnityEngine.GUIText::get_material()
		void Register_UnityEngine_GUIText_get_material();
		Register_UnityEngine_GUIText_get_material();

	//End Registrations for type : UnityEngine.GUIText

	//Start Registrations for type : UnityEngine.GUITexture

		//System.Void UnityEngine.GUITexture::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_GUITexture_INTERNAL_get_color();
		Register_UnityEngine_GUITexture_INTERNAL_get_color();

		//System.Void UnityEngine.GUITexture::INTERNAL_get_pixelInset(UnityEngine.Rect&)
		void Register_UnityEngine_GUITexture_INTERNAL_get_pixelInset();
		Register_UnityEngine_GUITexture_INTERNAL_get_pixelInset();

		//System.Void UnityEngine.GUITexture::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_GUITexture_INTERNAL_set_color();
		Register_UnityEngine_GUITexture_INTERNAL_set_color();

		//System.Void UnityEngine.GUITexture::set_texture(UnityEngine.Texture)
		void Register_UnityEngine_GUITexture_set_texture();
		Register_UnityEngine_GUITexture_set_texture();

	//End Registrations for type : UnityEngine.GUITexture

	//Start Registrations for type : UnityEngine.GUIUtility

		//System.Boolean UnityEngine.GUIUtility::get_mouseUsed()
		void Register_UnityEngine_GUIUtility_get_mouseUsed();
		Register_UnityEngine_GUIUtility_get_mouseUsed();

		//System.Int32 UnityEngine.GUIUtility::GetControlID(System.Int32,UnityEngine.FocusType)
		void Register_UnityEngine_GUIUtility_GetControlID();
		Register_UnityEngine_GUIUtility_GetControlID();

		//System.Int32 UnityEngine.GUIUtility::INTERNAL_CALL_Internal_GetNextControlID2(System.Int32,UnityEngine.FocusType,UnityEngine.Rect&)
		void Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();
		Register_UnityEngine_GUIUtility_INTERNAL_CALL_Internal_GetNextControlID2();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetGUIDepth()
		void Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();
		Register_UnityEngine_GUIUtility_Internal_GetGUIDepth();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetHotControl()
		void Register_UnityEngine_GUIUtility_Internal_GetHotControl();
		Register_UnityEngine_GUIUtility_Internal_GetHotControl();

		//System.Int32 UnityEngine.GUIUtility::Internal_GetKeyboardControl()
		void Register_UnityEngine_GUIUtility_Internal_GetKeyboardControl();
		Register_UnityEngine_GUIUtility_Internal_GetKeyboardControl();

		//System.Single UnityEngine.GUIUtility::Internal_GetPixelsPerPoint()
		void Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();
		Register_UnityEngine_GUIUtility_Internal_GetPixelsPerPoint();

		//System.String UnityEngine.GUIUtility::get_systemCopyBuffer()
		void Register_UnityEngine_GUIUtility_get_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_get_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::Internal_ExitGUI()
		void Register_UnityEngine_GUIUtility_Internal_ExitGUI();
		Register_UnityEngine_GUIUtility_Internal_ExitGUI();

		//System.Void UnityEngine.GUIUtility::Internal_SetHotControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetHotControl();
		Register_UnityEngine_GUIUtility_Internal_SetHotControl();

		//System.Void UnityEngine.GUIUtility::Internal_SetKeyboardControl(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_SetKeyboardControl();
		Register_UnityEngine_GUIUtility_Internal_SetKeyboardControl();

		//System.Void UnityEngine.GUIUtility::set_mouseUsed(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_mouseUsed();
		Register_UnityEngine_GUIUtility_set_mouseUsed();

		//System.Void UnityEngine.GUIUtility::set_systemCopyBuffer(System.String)
		void Register_UnityEngine_GUIUtility_set_systemCopyBuffer();
		Register_UnityEngine_GUIUtility_set_systemCopyBuffer();

		//System.Void UnityEngine.GUIUtility::set_textFieldInput(System.Boolean)
		void Register_UnityEngine_GUIUtility_set_textFieldInput();
		Register_UnityEngine_GUIUtility_set_textFieldInput();

		//UnityEngine.GUISkin UnityEngine.GUIUtility::Internal_GetDefaultSkin(System.Int32)
		void Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();
		Register_UnityEngine_GUIUtility_Internal_GetDefaultSkin();

	//End Registrations for type : UnityEngine.GUIUtility

	//Start Registrations for type : UnityEngine.Handheld

		//System.Boolean UnityEngine.Handheld::INTERNAL_CALL_PlayFullScreenMovie(System.String,UnityEngine.Color&,UnityEngine.FullScreenMovieControlMode,UnityEngine.FullScreenMovieScalingMode)
		void Register_UnityEngine_Handheld_INTERNAL_CALL_PlayFullScreenMovie();
		Register_UnityEngine_Handheld_INTERNAL_CALL_PlayFullScreenMovie();

		//System.Void UnityEngine.Handheld::Vibrate()
		void Register_UnityEngine_Handheld_Vibrate();
		Register_UnityEngine_Handheld_Vibrate();

	//End Registrations for type : UnityEngine.Handheld

	//Start Registrations for type : UnityEngine.HingeJoint2D

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_get_limits(UnityEngine.JointAngleLimits2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_get_limits();
		Register_UnityEngine_HingeJoint2D_INTERNAL_get_limits();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_get_motor();
		Register_UnityEngine_HingeJoint2D_INTERNAL_get_motor();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_set_limits(UnityEngine.JointAngleLimits2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_set_limits();
		Register_UnityEngine_HingeJoint2D_INTERNAL_set_limits();

		//System.Void UnityEngine.HingeJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_HingeJoint2D_INTERNAL_set_motor();
		Register_UnityEngine_HingeJoint2D_INTERNAL_set_motor();

		//System.Void UnityEngine.HingeJoint2D::set_useLimits(System.Boolean)
		void Register_UnityEngine_HingeJoint2D_set_useLimits();
		Register_UnityEngine_HingeJoint2D_set_useLimits();

		//System.Void UnityEngine.HingeJoint2D::set_useMotor(System.Boolean)
		void Register_UnityEngine_HingeJoint2D_set_useMotor();
		Register_UnityEngine_HingeJoint2D_set_useMotor();

	//End Registrations for type : UnityEngine.HingeJoint2D

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetButton(System.String)
		void Register_UnityEngine_Input_GetButton();
		Register_UnityEngine_Input_GetButton();

		//System.Boolean UnityEngine.Input::GetButtonDown(System.String)
		void Register_UnityEngine_Input_GetButtonDown();
		Register_UnityEngine_Input_GetButtonDown();

		//System.Boolean UnityEngine.Input::GetButtonUp(System.String)
		void Register_UnityEngine_Input_GetButtonUp();
		Register_UnityEngine_Input_GetButtonUp();

		//System.Boolean UnityEngine.Input::GetKeyDownInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyDownInt();
		Register_UnityEngine_Input_GetKeyDownInt();

		//System.Boolean UnityEngine.Input::GetKeyInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyInt();
		Register_UnityEngine_Input_GetKeyInt();

		//System.Boolean UnityEngine.Input::GetKeyUpInt(System.Int32)
		void Register_UnityEngine_Input_GetKeyUpInt();
		Register_UnityEngine_Input_GetKeyUpInt();

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonUp();
		Register_UnityEngine_Input_GetMouseButtonUp();

		//System.Boolean UnityEngine.Input::get_anyKeyDown()
		void Register_UnityEngine_Input_get_anyKeyDown();
		Register_UnityEngine_Input_get_anyKeyDown();

		//System.Boolean UnityEngine.Input::get_mousePresent()
		void Register_UnityEngine_Input_get_mousePresent();
		Register_UnityEngine_Input_get_mousePresent();

		//System.Boolean UnityEngine.Input::get_touchSupported()
		void Register_UnityEngine_Input_get_touchSupported();
		Register_UnityEngine_Input_get_touchSupported();

		//System.Int32 UnityEngine.Input::get_touchCount()
		void Register_UnityEngine_Input_get_touchCount();
		Register_UnityEngine_Input_get_touchCount();

		//System.Single UnityEngine.Input::GetAxis(System.String)
		void Register_UnityEngine_Input_GetAxis();
		Register_UnityEngine_Input_GetAxis();

		//System.Single UnityEngine.Input::GetAxisRaw(System.String)
		void Register_UnityEngine_Input_GetAxisRaw();
		Register_UnityEngine_Input_GetAxisRaw();

		//System.String UnityEngine.Input::get_compositionString()
		void Register_UnityEngine_Input_get_compositionString();
		Register_UnityEngine_Input_get_compositionString();

		//System.String UnityEngine.Input::get_inputString()
		void Register_UnityEngine_Input_get_inputString();
		Register_UnityEngine_Input_get_inputString();

		//System.Void UnityEngine.Input::INTERNAL_CALL_GetTouch(System.Int32,UnityEngine.Touch&)
		void Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();
		Register_UnityEngine_Input_INTERNAL_CALL_GetTouch();

		//System.Void UnityEngine.Input::INTERNAL_get_acceleration(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_acceleration();
		Register_UnityEngine_Input_INTERNAL_get_acceleration();

		//System.Void UnityEngine.Input::INTERNAL_get_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_get_compositionCursorPos();

		//System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_mousePosition();
		Register_UnityEngine_Input_INTERNAL_get_mousePosition();

		//System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();
		Register_UnityEngine_Input_INTERNAL_get_mouseScrollDelta();

		//System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
		void Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();
		Register_UnityEngine_Input_INTERNAL_set_compositionCursorPos();

		//System.Void UnityEngine.Input::ResetInputAxes()
		void Register_UnityEngine_Input_ResetInputAxes();
		Register_UnityEngine_Input_ResetInputAxes();

		//System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
		void Register_UnityEngine_Input_set_imeCompositionMode();
		Register_UnityEngine_Input_set_imeCompositionMode();

		//UnityEngine.DeviceOrientation UnityEngine.Input::get_deviceOrientation()
		void Register_UnityEngine_Input_get_deviceOrientation();
		Register_UnityEngine_Input_get_deviceOrientation();

		//UnityEngine.IMECompositionMode UnityEngine.Input::get_imeCompositionMode()
		void Register_UnityEngine_Input_get_imeCompositionMode();
		Register_UnityEngine_Input_get_imeCompositionMode();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.iOS.Device

		//System.Boolean UnityEngine.iOS.Device::get_advertisingTrackingEnabled()
		void Register_UnityEngine_iOS_Device_get_advertisingTrackingEnabled();
		Register_UnityEngine_iOS_Device_get_advertisingTrackingEnabled();

		//UnityEngine.iOS.DeviceGeneration UnityEngine.iOS.Device::get_generation()
		void Register_UnityEngine_iOS_Device_get_generation();
		Register_UnityEngine_iOS_Device_get_generation();

	//End Registrations for type : UnityEngine.iOS.Device

	//Start Registrations for type : UnityEngine.iOS.LocalNotification

		//System.Boolean UnityEngine.iOS.LocalNotification::get_hasAction()
		void Register_UnityEngine_iOS_LocalNotification_get_hasAction();
		Register_UnityEngine_iOS_LocalNotification_get_hasAction();

		//System.Collections.IDictionary UnityEngine.iOS.LocalNotification::get_userInfo()
		void Register_UnityEngine_iOS_LocalNotification_get_userInfo();
		Register_UnityEngine_iOS_LocalNotification_get_userInfo();

		//System.Double UnityEngine.iOS.LocalNotification::GetFireDate()
		void Register_UnityEngine_iOS_LocalNotification_GetFireDate();
		Register_UnityEngine_iOS_LocalNotification_GetFireDate();

		//System.Int32 UnityEngine.iOS.LocalNotification::get_applicationIconBadgeNumber()
		void Register_UnityEngine_iOS_LocalNotification_get_applicationIconBadgeNumber();
		Register_UnityEngine_iOS_LocalNotification_get_applicationIconBadgeNumber();

		//System.String UnityEngine.iOS.LocalNotification::get_alertAction()
		void Register_UnityEngine_iOS_LocalNotification_get_alertAction();
		Register_UnityEngine_iOS_LocalNotification_get_alertAction();

		//System.String UnityEngine.iOS.LocalNotification::get_alertBody()
		void Register_UnityEngine_iOS_LocalNotification_get_alertBody();
		Register_UnityEngine_iOS_LocalNotification_get_alertBody();

		//System.String UnityEngine.iOS.LocalNotification::get_alertLaunchImage()
		void Register_UnityEngine_iOS_LocalNotification_get_alertLaunchImage();
		Register_UnityEngine_iOS_LocalNotification_get_alertLaunchImage();

		//System.String UnityEngine.iOS.LocalNotification::get_defaultSoundName()
		void Register_UnityEngine_iOS_LocalNotification_get_defaultSoundName();
		Register_UnityEngine_iOS_LocalNotification_get_defaultSoundName();

		//System.String UnityEngine.iOS.LocalNotification::get_soundName()
		void Register_UnityEngine_iOS_LocalNotification_get_soundName();
		Register_UnityEngine_iOS_LocalNotification_get_soundName();

		//System.String UnityEngine.iOS.LocalNotification::get_timeZone()
		void Register_UnityEngine_iOS_LocalNotification_get_timeZone();
		Register_UnityEngine_iOS_LocalNotification_get_timeZone();

		//System.Void UnityEngine.iOS.LocalNotification::Destroy()
		void Register_UnityEngine_iOS_LocalNotification_Destroy();
		Register_UnityEngine_iOS_LocalNotification_Destroy();

		//System.Void UnityEngine.iOS.LocalNotification::InitWrapper()
		void Register_UnityEngine_iOS_LocalNotification_InitWrapper();
		Register_UnityEngine_iOS_LocalNotification_InitWrapper();

		//System.Void UnityEngine.iOS.LocalNotification::SetFireDate(System.Double)
		void Register_UnityEngine_iOS_LocalNotification_SetFireDate();
		Register_UnityEngine_iOS_LocalNotification_SetFireDate();

		//System.Void UnityEngine.iOS.LocalNotification::set_alertAction(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_alertAction();
		Register_UnityEngine_iOS_LocalNotification_set_alertAction();

		//System.Void UnityEngine.iOS.LocalNotification::set_alertBody(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_alertBody();
		Register_UnityEngine_iOS_LocalNotification_set_alertBody();

		//System.Void UnityEngine.iOS.LocalNotification::set_alertLaunchImage(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_alertLaunchImage();
		Register_UnityEngine_iOS_LocalNotification_set_alertLaunchImage();

		//System.Void UnityEngine.iOS.LocalNotification::set_applicationIconBadgeNumber(System.Int32)
		void Register_UnityEngine_iOS_LocalNotification_set_applicationIconBadgeNumber();
		Register_UnityEngine_iOS_LocalNotification_set_applicationIconBadgeNumber();

		//System.Void UnityEngine.iOS.LocalNotification::set_hasAction(System.Boolean)
		void Register_UnityEngine_iOS_LocalNotification_set_hasAction();
		Register_UnityEngine_iOS_LocalNotification_set_hasAction();

		//System.Void UnityEngine.iOS.LocalNotification::set_repeatCalendar(UnityEngine.iOS.CalendarIdentifier)
		void Register_UnityEngine_iOS_LocalNotification_set_repeatCalendar();
		Register_UnityEngine_iOS_LocalNotification_set_repeatCalendar();

		//System.Void UnityEngine.iOS.LocalNotification::set_repeatInterval(UnityEngine.iOS.CalendarUnit)
		void Register_UnityEngine_iOS_LocalNotification_set_repeatInterval();
		Register_UnityEngine_iOS_LocalNotification_set_repeatInterval();

		//System.Void UnityEngine.iOS.LocalNotification::set_soundName(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_soundName();
		Register_UnityEngine_iOS_LocalNotification_set_soundName();

		//System.Void UnityEngine.iOS.LocalNotification::set_timeZone(System.String)
		void Register_UnityEngine_iOS_LocalNotification_set_timeZone();
		Register_UnityEngine_iOS_LocalNotification_set_timeZone();

		//System.Void UnityEngine.iOS.LocalNotification::set_userInfo(System.Collections.IDictionary)
		void Register_UnityEngine_iOS_LocalNotification_set_userInfo();
		Register_UnityEngine_iOS_LocalNotification_set_userInfo();

		//UnityEngine.iOS.CalendarIdentifier UnityEngine.iOS.LocalNotification::get_repeatCalendar()
		void Register_UnityEngine_iOS_LocalNotification_get_repeatCalendar();
		Register_UnityEngine_iOS_LocalNotification_get_repeatCalendar();

		//UnityEngine.iOS.CalendarUnit UnityEngine.iOS.LocalNotification::get_repeatInterval()
		void Register_UnityEngine_iOS_LocalNotification_get_repeatInterval();
		Register_UnityEngine_iOS_LocalNotification_get_repeatInterval();

	//End Registrations for type : UnityEngine.iOS.LocalNotification

	//Start Registrations for type : UnityEngine.iOS.RemoteNotification

		//System.Boolean UnityEngine.iOS.RemoteNotification::get_hasAction()
		void Register_UnityEngine_iOS_RemoteNotification_get_hasAction();
		Register_UnityEngine_iOS_RemoteNotification_get_hasAction();

		//System.Collections.IDictionary UnityEngine.iOS.RemoteNotification::get_userInfo()
		void Register_UnityEngine_iOS_RemoteNotification_get_userInfo();
		Register_UnityEngine_iOS_RemoteNotification_get_userInfo();

		//System.Int32 UnityEngine.iOS.RemoteNotification::get_applicationIconBadgeNumber()
		void Register_UnityEngine_iOS_RemoteNotification_get_applicationIconBadgeNumber();
		Register_UnityEngine_iOS_RemoteNotification_get_applicationIconBadgeNumber();

		//System.String UnityEngine.iOS.RemoteNotification::get_alertBody()
		void Register_UnityEngine_iOS_RemoteNotification_get_alertBody();
		Register_UnityEngine_iOS_RemoteNotification_get_alertBody();

		//System.String UnityEngine.iOS.RemoteNotification::get_soundName()
		void Register_UnityEngine_iOS_RemoteNotification_get_soundName();
		Register_UnityEngine_iOS_RemoteNotification_get_soundName();

		//System.Void UnityEngine.iOS.RemoteNotification::Destroy()
		void Register_UnityEngine_iOS_RemoteNotification_Destroy();
		Register_UnityEngine_iOS_RemoteNotification_Destroy();

	//End Registrations for type : UnityEngine.iOS.RemoteNotification

	//Start Registrations for type : UnityEngine.Joint

		//System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
		void Register_UnityEngine_Joint_set_connectedBody();
		Register_UnityEngine_Joint_set_connectedBody();

	//End Registrations for type : UnityEngine.Joint

	//Start Registrations for type : UnityEngine.Joint2D

		//System.Single UnityEngine.Joint2D::INTERNAL_CALL_GetReactionTorque(UnityEngine.Joint2D,System.Single)
		void Register_UnityEngine_Joint2D_INTERNAL_CALL_GetReactionTorque();
		Register_UnityEngine_Joint2D_INTERNAL_CALL_GetReactionTorque();

		//System.Void UnityEngine.Joint2D::Internal_GetReactionForce(UnityEngine.Joint2D,System.Single,UnityEngine.Vector2&)
		void Register_UnityEngine_Joint2D_Internal_GetReactionForce();
		Register_UnityEngine_Joint2D_Internal_GetReactionForce();

	//End Registrations for type : UnityEngine.Joint2D

	//Start Registrations for type : UnityEngine.LayerMask

		//System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
		void Register_UnityEngine_LayerMask_NameToLayer();
		Register_UnityEngine_LayerMask_NameToLayer();

	//End Registrations for type : UnityEngine.LayerMask

	//Start Registrations for type : UnityEngine.Light

		//System.Void UnityEngine.Light::INTERNAL_get_color(UnityEngine.Color&)
		void Register_UnityEngine_Light_INTERNAL_get_color();
		Register_UnityEngine_Light_INTERNAL_get_color();

		//System.Void UnityEngine.Light::INTERNAL_set_color(UnityEngine.Color&)
		void Register_UnityEngine_Light_INTERNAL_set_color();
		Register_UnityEngine_Light_INTERNAL_set_color();

		//System.Void UnityEngine.Light::set_cookie(UnityEngine.Texture)
		void Register_UnityEngine_Light_set_cookie();
		Register_UnityEngine_Light_set_cookie();

		//System.Void UnityEngine.Light::set_flare(UnityEngine.Flare)
		void Register_UnityEngine_Light_set_flare();
		Register_UnityEngine_Light_set_flare();

		//System.Void UnityEngine.Light::set_intensity(System.Single)
		void Register_UnityEngine_Light_set_intensity();
		Register_UnityEngine_Light_set_intensity();

		//System.Void UnityEngine.Light::set_range(System.Single)
		void Register_UnityEngine_Light_set_range();
		Register_UnityEngine_Light_set_range();

		//System.Void UnityEngine.Light::set_shadowStrength(System.Single)
		void Register_UnityEngine_Light_set_shadowStrength();
		Register_UnityEngine_Light_set_shadowStrength();

		//System.Void UnityEngine.Light::set_spotAngle(System.Single)
		void Register_UnityEngine_Light_set_spotAngle();
		Register_UnityEngine_Light_set_spotAngle();

		//System.Void UnityEngine.Light::set_type(UnityEngine.LightType)
		void Register_UnityEngine_Light_set_type();
		Register_UnityEngine_Light_set_type();

	//End Registrations for type : UnityEngine.Light

	//Start Registrations for type : UnityEngine.LocationService

		//System.Void UnityEngine.LocationService::Start(System.Single,System.Single)
		void Register_UnityEngine_LocationService_Start();
		Register_UnityEngine_LocationService_Start();

		//System.Void UnityEngine.LocationService::Stop()
		void Register_UnityEngine_LocationService_Stop();
		Register_UnityEngine_LocationService_Stop();

		//UnityEngine.LocationInfo UnityEngine.LocationService::get_lastData()
		void Register_UnityEngine_LocationService_get_lastData();
		Register_UnityEngine_LocationService_get_lastData();

		//UnityEngine.LocationServiceStatus UnityEngine.LocationService::get_status()
		void Register_UnityEngine_LocationService_get_status();
		Register_UnityEngine_LocationService_get_status();

	//End Registrations for type : UnityEngine.LocationService

	//Start Registrations for type : UnityEngine.Material

		//System.Boolean UnityEngine.Material::HasProperty(System.Int32)
		void Register_UnityEngine_Material_HasProperty();
		Register_UnityEngine_Material_HasProperty();

		//System.Void UnityEngine.Material::DisableKeyword(System.String)
		void Register_UnityEngine_Material_DisableKeyword();
		Register_UnityEngine_Material_DisableKeyword();

		//System.Void UnityEngine.Material::EnableKeyword(System.String)
		void Register_UnityEngine_Material_EnableKeyword();
		Register_UnityEngine_Material_EnableKeyword();

		//System.Void UnityEngine.Material::INTERNAL_CALL_GetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_GetColorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_GetColorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetColorImpl(UnityEngine.Material,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetColorImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetColorImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffsetImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetTextureOffsetImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetTextureOffsetImpl();

		//System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureScaleImpl(UnityEngine.Material,System.Int32,UnityEngine.Vector2&)
		void Register_UnityEngine_Material_INTERNAL_CALL_SetTextureScaleImpl();
		Register_UnityEngine_Material_INTERNAL_CALL_SetTextureScaleImpl();

		//System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
		void Register_UnityEngine_Material_Internal_CreateWithMaterial();
		Register_UnityEngine_Material_Internal_CreateWithMaterial();

		//System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
		void Register_UnityEngine_Material_SetFloatImpl();
		Register_UnityEngine_Material_SetFloatImpl();

		//System.Void UnityEngine.Material::SetIntImpl(System.Int32,System.Int32)
		void Register_UnityEngine_Material_SetIntImpl();
		Register_UnityEngine_Material_SetIntImpl();

		//System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
		void Register_UnityEngine_Material_SetTextureImpl();
		Register_UnityEngine_Material_SetTextureImpl();

		//UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
		void Register_UnityEngine_Material_GetTextureImpl();
		Register_UnityEngine_Material_GetTextureImpl();

	//End Registrations for type : UnityEngine.Material

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Void UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Mesh

		//System.Array UnityEngine.Mesh::ExtractArrayFromList(System.Object)
		void Register_UnityEngine_Mesh_ExtractArrayFromList();
		Register_UnityEngine_Mesh_ExtractArrayFromList();

		//System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32)
		void Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();
		Register_UnityEngine_Mesh_GetAllocArrayFromChannelImpl();

		//System.Boolean UnityEngine.Mesh::HasChannel(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_HasChannel();
		Register_UnityEngine_Mesh_HasChannel();

		//System.Boolean UnityEngine.Mesh::get_canAccess()
		void Register_UnityEngine_Mesh_get_canAccess();
		Register_UnityEngine_Mesh_get_canAccess();

		//System.Int32 UnityEngine.Mesh::get_subMeshCount()
		void Register_UnityEngine_Mesh_get_subMeshCount();
		Register_UnityEngine_Mesh_get_subMeshCount();

		//System.Int32 UnityEngine.Mesh::get_vertexCount()
		void Register_UnityEngine_Mesh_get_vertexCount();
		Register_UnityEngine_Mesh_get_vertexCount();

		//System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32)
		void Register_UnityEngine_Mesh_GetIndicesImpl();
		Register_UnityEngine_Mesh_GetIndicesImpl();

		//System.Void UnityEngine.Mesh::Clear(System.Boolean)
		void Register_UnityEngine_Mesh_Clear();
		Register_UnityEngine_Mesh_Clear();

		//System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
		void Register_UnityEngine_Mesh_Internal_Create();
		Register_UnityEngine_Mesh_Internal_Create();

		//System.Void UnityEngine.Mesh::PrintErrorBadSubmeshIndexIndices()
		void Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexIndices();
		Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexIndices();

		//System.Void UnityEngine.Mesh::PrintErrorBadSubmeshIndexTriangles()
		void Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexTriangles();
		Register_UnityEngine_Mesh_PrintErrorBadSubmeshIndexTriangles();

		//System.Void UnityEngine.Mesh::PrintErrorCantAccessMesh(UnityEngine.Mesh/InternalShaderChannel)
		void Register_UnityEngine_Mesh_PrintErrorCantAccessMesh();
		Register_UnityEngine_Mesh_PrintErrorCantAccessMesh();

		//System.Void UnityEngine.Mesh::PrintErrorCantAccessMeshForIndices()
		void Register_UnityEngine_Mesh_PrintErrorCantAccessMeshForIndices();
		Register_UnityEngine_Mesh_PrintErrorCantAccessMeshForIndices();

		//System.Void UnityEngine.Mesh::RecalculateBounds()
		void Register_UnityEngine_Mesh_RecalculateBounds();
		Register_UnityEngine_Mesh_RecalculateBounds();

		//System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Mesh/InternalShaderChannel,UnityEngine.Mesh/InternalVertexChannelType,System.Int32,System.Array,System.Int32)
		void Register_UnityEngine_Mesh_SetArrayForChannelImpl();
		Register_UnityEngine_Mesh_SetArrayForChannelImpl();

		//System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,System.Array,System.Int32,System.Boolean)
		void Register_UnityEngine_Mesh_SetTrianglesImpl();
		Register_UnityEngine_Mesh_SetTrianglesImpl();

	//End Registrations for type : UnityEngine.Mesh

	//Start Registrations for type : UnityEngine.MeshFilter

		//UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
		void Register_UnityEngine_MeshFilter_get_mesh();
		Register_UnityEngine_MeshFilter_get_mesh();

	//End Registrations for type : UnityEngine.MeshFilter

	//Start Registrations for type : UnityEngine.MonoBehaviour

		//System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll()
		void Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll();
		Register_UnityEngine_MonoBehaviour_Internal_IsInvokingAll();

		//System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
		void Register_UnityEngine_MonoBehaviour_IsInvoking();
		Register_UnityEngine_MonoBehaviour_IsInvoking();

		//System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
		void Register_UnityEngine_MonoBehaviour_get_useGUILayout();
		Register_UnityEngine_MonoBehaviour_get_useGUILayout();

		//System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
		void Register_UnityEngine_MonoBehaviour_CancelInvoke();
		Register_UnityEngine_MonoBehaviour_CancelInvoke();

		//System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll()
		void Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();
		Register_UnityEngine_MonoBehaviour_Internal_CancelInvokeAll();

		//System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
		void Register_UnityEngine_MonoBehaviour_Invoke();
		Register_UnityEngine_MonoBehaviour_Invoke();

		//System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
		void Register_UnityEngine_MonoBehaviour_InvokeRepeating();
		Register_UnityEngine_MonoBehaviour_InvokeRepeating();

		//System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
		void Register_UnityEngine_MonoBehaviour_StopAllCoroutines();
		Register_UnityEngine_MonoBehaviour_StopAllCoroutines();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine();
		Register_UnityEngine_MonoBehaviour_StopCoroutine();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutineViaEnumerator_Auto();

		//System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
		void Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();
		Register_UnityEngine_MonoBehaviour_StopCoroutine_Auto();

		//System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
		void Register_UnityEngine_MonoBehaviour_set_useGUILayout();
		Register_UnityEngine_MonoBehaviour_set_useGUILayout();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine();
		Register_UnityEngine_MonoBehaviour_StartCoroutine();

		//UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto_Internal(System.Collections.IEnumerator)
		void Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal();
		Register_UnityEngine_MonoBehaviour_StartCoroutine_Auto_Internal();

	//End Registrations for type : UnityEngine.MonoBehaviour

	//Start Registrations for type : UnityEngine.NetworkMessageInfo

		//UnityEngine.NetworkView UnityEngine.NetworkMessageInfo::NullNetworkView()
		void Register_UnityEngine_NetworkMessageInfo_NullNetworkView();
		Register_UnityEngine_NetworkMessageInfo_NullNetworkView();

	//End Registrations for type : UnityEngine.NetworkMessageInfo

	//Start Registrations for type : UnityEngine.NetworkPlayer

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetExternalPort()
		void Register_UnityEngine_NetworkPlayer_Internal_GetExternalPort();
		Register_UnityEngine_NetworkPlayer_Internal_GetExternalPort();

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetLocalPort()
		void Register_UnityEngine_NetworkPlayer_Internal_GetLocalPort();
		Register_UnityEngine_NetworkPlayer_Internal_GetLocalPort();

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetPlayerIndex()
		void Register_UnityEngine_NetworkPlayer_Internal_GetPlayerIndex();
		Register_UnityEngine_NetworkPlayer_Internal_GetPlayerIndex();

		//System.Int32 UnityEngine.NetworkPlayer::Internal_GetPort(System.Int32)
		void Register_UnityEngine_NetworkPlayer_Internal_GetPort();
		Register_UnityEngine_NetworkPlayer_Internal_GetPort();

		//System.String UnityEngine.NetworkPlayer::Internal_GetExternalIP()
		void Register_UnityEngine_NetworkPlayer_Internal_GetExternalIP();
		Register_UnityEngine_NetworkPlayer_Internal_GetExternalIP();

		//System.String UnityEngine.NetworkPlayer::Internal_GetGUID(System.Int32)
		void Register_UnityEngine_NetworkPlayer_Internal_GetGUID();
		Register_UnityEngine_NetworkPlayer_Internal_GetGUID();

		//System.String UnityEngine.NetworkPlayer::Internal_GetIPAddress(System.Int32)
		void Register_UnityEngine_NetworkPlayer_Internal_GetIPAddress();
		Register_UnityEngine_NetworkPlayer_Internal_GetIPAddress();

		//System.String UnityEngine.NetworkPlayer::Internal_GetLocalGUID()
		void Register_UnityEngine_NetworkPlayer_Internal_GetLocalGUID();
		Register_UnityEngine_NetworkPlayer_Internal_GetLocalGUID();

		//System.String UnityEngine.NetworkPlayer::Internal_GetLocalIP()
		void Register_UnityEngine_NetworkPlayer_Internal_GetLocalIP();
		Register_UnityEngine_NetworkPlayer_Internal_GetLocalIP();

	//End Registrations for type : UnityEngine.NetworkPlayer

	//Start Registrations for type : UnityEngine.NetworkView

		//UnityEngine.NetworkView UnityEngine.NetworkView::INTERNAL_CALL_Find(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkView_INTERNAL_CALL_Find();
		Register_UnityEngine_NetworkView_INTERNAL_CALL_Find();

	//End Registrations for type : UnityEngine.NetworkView

	//Start Registrations for type : UnityEngine.NetworkViewID

		//System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_Compare(UnityEngine.NetworkViewID&,UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_Compare();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_Compare();

		//System.Boolean UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_IsMine(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_IsMine();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_IsMine();

		//System.String UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetString(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetString();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetString();

		//System.Void UnityEngine.NetworkViewID::INTERNAL_CALL_Internal_GetOwner(UnityEngine.NetworkViewID&,UnityEngine.NetworkPlayer&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetOwner();
		Register_UnityEngine_NetworkViewID_INTERNAL_CALL_Internal_GetOwner();

		//System.Void UnityEngine.NetworkViewID::INTERNAL_get_unassigned(UnityEngine.NetworkViewID&)
		void Register_UnityEngine_NetworkViewID_INTERNAL_get_unassigned();
		Register_UnityEngine_NetworkViewID_INTERNAL_get_unassigned();

	//End Registrations for type : UnityEngine.NetworkViewID

	//Start Registrations for type : UnityEngine.Object

		//System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
		void Register_UnityEngine_Object_DoesObjectWithInstanceIDExist();
		Register_UnityEngine_Object_DoesObjectWithInstanceIDExist();

		//System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
		void Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();
		Register_UnityEngine_Object_GetOffsetOfInstanceIDInCPlusPlusObject();

		//System.String UnityEngine.Object::ToString()
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

		//System.String UnityEngine.Object::get_name()
		void Register_UnityEngine_Object_get_name();
		Register_UnityEngine_Object_get_name();

		//System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_Destroy();
		Register_UnityEngine_Object_Destroy();

		//System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
		void Register_UnityEngine_Object_DestroyImmediate();
		Register_UnityEngine_Object_DestroyImmediate();

		//System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
		void Register_UnityEngine_Object_DestroyObject();
		Register_UnityEngine_Object_DestroyObject();

		//System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
		void Register_UnityEngine_Object_DontDestroyOnLoad();
		Register_UnityEngine_Object_DontDestroyOnLoad();

		//System.Void UnityEngine.Object::EnsureRunningOnMainThread()
		void Register_UnityEngine_Object_EnsureRunningOnMainThread();
		Register_UnityEngine_Object_EnsureRunningOnMainThread();

		//System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
		void Register_UnityEngine_Object_set_hideFlags();
		Register_UnityEngine_Object_set_hideFlags();

		//System.Void UnityEngine.Object::set_name(System.String)
		void Register_UnityEngine_Object_set_name();
		Register_UnityEngine_Object_set_name();

		//UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
		void Register_UnityEngine_Object_get_hideFlags();
		Register_UnityEngine_Object_get_hideFlags();

		//UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle();
		Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingle();

		//UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent();
		Register_UnityEngine_Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
		void Register_UnityEngine_Object_Internal_CloneSingle();
		Register_UnityEngine_Object_Internal_CloneSingle();

		//UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Object_Internal_CloneSingleWithParent();
		Register_UnityEngine_Object_Internal_CloneSingleWithParent();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfType();
		Register_UnityEngine_Object_FindObjectsOfType();

		//UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
		void Register_UnityEngine_Object_FindObjectsOfTypeIncludingAssets();
		Register_UnityEngine_Object_FindObjectsOfTypeIncludingAssets();

		//UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
		void Register_UnityEngine_Object_FindSceneObjectsOfType();
		Register_UnityEngine_Object_FindSceneObjectsOfType();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.ParticleSystem

		//System.Boolean UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_Clear();
		Register_UnityEngine_ParticleSystem_Internal_Clear();

		//System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
		void Register_UnityEngine_ParticleSystem_Internal_IsAlive();
		Register_UnityEngine_ParticleSystem_Internal_IsAlive();

		//System.Single UnityEngine.ParticleSystem::get_startDelay()
		void Register_UnityEngine_ParticleSystem_get_startDelay();
		Register_UnityEngine_ParticleSystem_get_startDelay();

		//System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
		void Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();
		Register_UnityEngine_ParticleSystem_INTERNAL_CALL_Emit();

	//End Registrations for type : UnityEngine.ParticleSystem

	//Start Registrations for type : UnityEngine.Physics

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_Raycast();

		//System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();
		Register_UnityEngine_Physics_INTERNAL_CALL_Internal_RaycastTest();

		//System.Void UnityEngine.Physics::INTERNAL_set_gravity(UnityEngine.Vector3&)
		void Register_UnityEngine_Physics_INTERNAL_set_gravity();
		Register_UnityEngine_Physics_INTERNAL_set_gravity();

		//UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere();
		Register_UnityEngine_Physics_INTERNAL_CALL_OverlapSphere();

		//UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();
		Register_UnityEngine_Physics_INTERNAL_CALL_RaycastAll();

	//End Registrations for type : UnityEngine.Physics

	//Start Registrations for type : UnityEngine.Physics2D

		//System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
		void Register_UnityEngine_Physics2D_get_queriesHitTriggers();
		Register_UnityEngine_Physics2D_get_queriesHitTriggers();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionNonAlloc(UnityEngine.Ray&,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionNonAlloc();

		//System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_GetRayIntersection(UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_GetRayIntersection();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_GetRayIntersection();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Linecast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Linecast();

		//System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_Raycast();

		//System.Void UnityEngine.Physics2D::INTERNAL_set_gravity(UnityEngine.Vector2&)
		void Register_UnityEngine_Physics2D_INTERNAL_set_gravity();
		Register_UnityEngine_Physics2D_INTERNAL_set_gravity();

		//UnityEngine.Collider2D UnityEngine.Physics2D::GetColliderFromInstanceID(System.Int32)
		void Register_UnityEngine_Physics2D_GetColliderFromInstanceID();
		Register_UnityEngine_Physics2D_GetColliderFromInstanceID();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_Internal_OverlapAreaAll(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_OverlapAreaAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_OverlapAreaAll();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_Internal_OverlapCircleAll(UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_OverlapCircleAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_OverlapCircleAll();

		//UnityEngine.Collider2D[] UnityEngine.Physics2D::INTERNAL_CALL_Internal_OverlapPointAll(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_OverlapPointAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_OverlapPointAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_GetRayIntersectionAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_Internal_LinecastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_LinecastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_LinecastAll();

		//UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&)
		void Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastAll();
		Register_UnityEngine_Physics2D_INTERNAL_CALL_Internal_RaycastAll();

		//UnityEngine.Rigidbody2D UnityEngine.Physics2D::GetRigidbodyFromInstanceID(System.Int32)
		void Register_UnityEngine_Physics2D_GetRigidbodyFromInstanceID();
		Register_UnityEngine_Physics2D_GetRigidbodyFromInstanceID();

	//End Registrations for type : UnityEngine.Physics2D

	//Start Registrations for type : UnityEngine.PlayerPrefs

		//System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
		void Register_UnityEngine_PlayerPrefs_HasKey();
		Register_UnityEngine_PlayerPrefs_HasKey();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_TrySetFloat();
		Register_UnityEngine_PlayerPrefs_TrySetFloat();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_TrySetInt();
		Register_UnityEngine_PlayerPrefs_TrySetInt();

		//System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_TrySetSetString();
		Register_UnityEngine_PlayerPrefs_TrySetSetString();

		//System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
		void Register_UnityEngine_PlayerPrefs_GetInt();
		Register_UnityEngine_PlayerPrefs_GetInt();

		//System.Single UnityEngine.PlayerPrefs::GetFloat(System.String,System.Single)
		void Register_UnityEngine_PlayerPrefs_GetFloat();
		Register_UnityEngine_PlayerPrefs_GetFloat();

		//System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
		void Register_UnityEngine_PlayerPrefs_GetString();
		Register_UnityEngine_PlayerPrefs_GetString();

		//System.Void UnityEngine.PlayerPrefs::DeleteAll()
		void Register_UnityEngine_PlayerPrefs_DeleteAll();
		Register_UnityEngine_PlayerPrefs_DeleteAll();

		//System.Void UnityEngine.PlayerPrefs::DeleteKey(System.String)
		void Register_UnityEngine_PlayerPrefs_DeleteKey();
		Register_UnityEngine_PlayerPrefs_DeleteKey();

		//System.Void UnityEngine.PlayerPrefs::Save()
		void Register_UnityEngine_PlayerPrefs_Save();
		Register_UnityEngine_PlayerPrefs_Save();

	//End Registrations for type : UnityEngine.PlayerPrefs

	//Start Registrations for type : UnityEngine.Profiling.Profiler

		//System.Boolean UnityEngine.Profiling.Profiler::get_enabled()
		void Register_UnityEngine_Profiling_Profiler_get_enabled();
		Register_UnityEngine_Profiling_Profiler_get_enabled();

		//System.Boolean UnityEngine.Profiling.Profiler::get_supported()
		void Register_UnityEngine_Profiling_Profiler_get_supported();
		Register_UnityEngine_Profiling_Profiler_get_supported();

		//System.Int64 UnityEngine.Profiling.Profiler::GetMonoHeapSizeLong()
		void Register_UnityEngine_Profiling_Profiler_GetMonoHeapSizeLong();
		Register_UnityEngine_Profiling_Profiler_GetMonoHeapSizeLong();

		//System.Int64 UnityEngine.Profiling.Profiler::GetMonoUsedSizeLong()
		void Register_UnityEngine_Profiling_Profiler_GetMonoUsedSizeLong();
		Register_UnityEngine_Profiling_Profiler_GetMonoUsedSizeLong();

		//System.Int64 UnityEngine.Profiling.Profiler::GetTotalAllocatedMemoryLong()
		void Register_UnityEngine_Profiling_Profiler_GetTotalAllocatedMemoryLong();
		Register_UnityEngine_Profiling_Profiler_GetTotalAllocatedMemoryLong();

		//System.Int64 UnityEngine.Profiling.Profiler::GetTotalReservedMemoryLong()
		void Register_UnityEngine_Profiling_Profiler_GetTotalReservedMemoryLong();
		Register_UnityEngine_Profiling_Profiler_GetTotalReservedMemoryLong();

		//System.Void UnityEngine.Profiling.Profiler::set_enabled(System.Boolean)
		void Register_UnityEngine_Profiling_Profiler_set_enabled();
		Register_UnityEngine_Profiling_Profiler_set_enabled();

	//End Registrations for type : UnityEngine.Profiling.Profiler

	//Start Registrations for type : UnityEngine.QualitySettings

		//System.Int32 UnityEngine.QualitySettings::GetQualityLevel()
		void Register_UnityEngine_QualitySettings_GetQualityLevel();
		Register_UnityEngine_QualitySettings_GetQualityLevel();

		//System.String[] UnityEngine.QualitySettings::get_names()
		void Register_UnityEngine_QualitySettings_get_names();
		Register_UnityEngine_QualitySettings_get_names();

	//End Registrations for type : UnityEngine.QualitySettings

	//Start Registrations for type : UnityEngine.Quaternion

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_AngleAxis();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_FromToRotation();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_FromEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Internal_ToEulerRad();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Inverse();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Lerp();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_LookRotation();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_Slerp();

		//System.Void UnityEngine.Quaternion::INTERNAL_CALL_SlerpUnclamped(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
		void Register_UnityEngine_Quaternion_INTERNAL_CALL_SlerpUnclamped();
		Register_UnityEngine_Quaternion_INTERNAL_CALL_SlerpUnclamped();

	//End Registrations for type : UnityEngine.Quaternion

	//Start Registrations for type : UnityEngine.Random

		//System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
		void Register_UnityEngine_Random_RandomRangeInt();
		Register_UnityEngine_Random_RandomRangeInt();

		//System.Single UnityEngine.Random::Range(System.Single,System.Single)
		void Register_UnityEngine_Random_Range();
		Register_UnityEngine_Random_Range();

	//End Registrations for type : UnityEngine.Random

	//Start Registrations for type : UnityEngine.RectOffset

		//System.Int32 UnityEngine.RectOffset::get_bottom()
		void Register_UnityEngine_RectOffset_get_bottom();
		Register_UnityEngine_RectOffset_get_bottom();

		//System.Int32 UnityEngine.RectOffset::get_horizontal()
		void Register_UnityEngine_RectOffset_get_horizontal();
		Register_UnityEngine_RectOffset_get_horizontal();

		//System.Int32 UnityEngine.RectOffset::get_left()
		void Register_UnityEngine_RectOffset_get_left();
		Register_UnityEngine_RectOffset_get_left();

		//System.Int32 UnityEngine.RectOffset::get_right()
		void Register_UnityEngine_RectOffset_get_right();
		Register_UnityEngine_RectOffset_get_right();

		//System.Int32 UnityEngine.RectOffset::get_top()
		void Register_UnityEngine_RectOffset_get_top();
		Register_UnityEngine_RectOffset_get_top();

		//System.Int32 UnityEngine.RectOffset::get_vertical()
		void Register_UnityEngine_RectOffset_get_vertical();
		Register_UnityEngine_RectOffset_get_vertical();

		//System.Void UnityEngine.RectOffset::Cleanup()
		void Register_UnityEngine_RectOffset_Cleanup();
		Register_UnityEngine_RectOffset_Cleanup();

		//System.Void UnityEngine.RectOffset::INTERNAL_CALL_Add(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
		void Register_UnityEngine_RectOffset_INTERNAL_CALL_Add();
		Register_UnityEngine_RectOffset_INTERNAL_CALL_Add();

		//System.Void UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&,UnityEngine.Rect&)
		void Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove();
		Register_UnityEngine_RectOffset_INTERNAL_CALL_Remove();

		//System.Void UnityEngine.RectOffset::Init()
		void Register_UnityEngine_RectOffset_Init();
		Register_UnityEngine_RectOffset_Init();

		//System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
		void Register_UnityEngine_RectOffset_set_bottom();
		Register_UnityEngine_RectOffset_set_bottom();

		//System.Void UnityEngine.RectOffset::set_left(System.Int32)
		void Register_UnityEngine_RectOffset_set_left();
		Register_UnityEngine_RectOffset_set_left();

		//System.Void UnityEngine.RectOffset::set_right(System.Int32)
		void Register_UnityEngine_RectOffset_set_right();
		Register_UnityEngine_RectOffset_set_right();

		//System.Void UnityEngine.RectOffset::set_top(System.Int32)
		void Register_UnityEngine_RectOffset_set_top();
		Register_UnityEngine_RectOffset_set_top();

	//End Registrations for type : UnityEngine.RectOffset

	//Start Registrations for type : UnityEngine.RectTransform

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_get_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_get_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_rect();
		Register_UnityEngine_RectTransform_INTERNAL_get_rect();

		//System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_get_sizeDelta();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMax();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchorMin();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();
		Register_UnityEngine_RectTransform_INTERNAL_set_anchoredPosition();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_pivot();
		Register_UnityEngine_RectTransform_INTERNAL_set_pivot();

		//System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();
		Register_UnityEngine_RectTransform_INTERNAL_set_sizeDelta();

	//End Registrations for type : UnityEngine.RectTransform

	//Start Registrations for type : UnityEngine.RectTransformUtility

		//System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint();

		//System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
		void Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();
		Register_UnityEngine_RectTransformUtility_INTERNAL_CALL_PixelAdjustRect();

	//End Registrations for type : UnityEngine.RectTransformUtility

	//Start Registrations for type : UnityEngine.Renderer

		//System.Boolean UnityEngine.Renderer::get_enabled()
		void Register_UnityEngine_Renderer_get_enabled();
		Register_UnityEngine_Renderer_get_enabled();

		//System.Boolean UnityEngine.Renderer::get_isVisible()
		void Register_UnityEngine_Renderer_get_isVisible();
		Register_UnityEngine_Renderer_get_isVisible();

		//System.Int32 UnityEngine.Renderer::get_sortingLayerID()
		void Register_UnityEngine_Renderer_get_sortingLayerID();
		Register_UnityEngine_Renderer_get_sortingLayerID();

		//System.Int32 UnityEngine.Renderer::get_sortingOrder()
		void Register_UnityEngine_Renderer_get_sortingOrder();
		Register_UnityEngine_Renderer_get_sortingOrder();

		//System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Renderer_INTERNAL_get_bounds();
		Register_UnityEngine_Renderer_INTERNAL_get_bounds();

		//System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
		void Register_UnityEngine_Renderer_set_enabled();
		Register_UnityEngine_Renderer_set_enabled();

		//System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
		void Register_UnityEngine_Renderer_set_material();
		Register_UnityEngine_Renderer_set_material();

		//System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
		void Register_UnityEngine_Renderer_set_materials();
		Register_UnityEngine_Renderer_set_materials();

		//System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
		void Register_UnityEngine_Renderer_set_sharedMaterials();
		Register_UnityEngine_Renderer_set_sharedMaterials();

		//UnityEngine.Material UnityEngine.Renderer::get_material()
		void Register_UnityEngine_Renderer_get_material();
		Register_UnityEngine_Renderer_get_material();

		//UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
		void Register_UnityEngine_Renderer_get_sharedMaterial();
		Register_UnityEngine_Renderer_get_sharedMaterial();

		//UnityEngine.Material[] UnityEngine.Renderer::get_materials()
		void Register_UnityEngine_Renderer_get_materials();
		Register_UnityEngine_Renderer_get_materials();

		//UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
		void Register_UnityEngine_Renderer_get_sharedMaterials();
		Register_UnityEngine_Renderer_get_sharedMaterials();

	//End Registrations for type : UnityEngine.Renderer

	//Start Registrations for type : UnityEngine.RenderSettings

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_ambientLight(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_ambientLight();
		Register_UnityEngine_RenderSettings_INTERNAL_set_ambientLight();

		//System.Void UnityEngine.RenderSettings::INTERNAL_set_fogColor(UnityEngine.Color&)
		void Register_UnityEngine_RenderSettings_INTERNAL_set_fogColor();
		Register_UnityEngine_RenderSettings_INTERNAL_set_fogColor();

		//System.Void UnityEngine.RenderSettings::set_flareStrength(System.Single)
		void Register_UnityEngine_RenderSettings_set_flareStrength();
		Register_UnityEngine_RenderSettings_set_flareStrength();

		//System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
		void Register_UnityEngine_RenderSettings_set_fog();
		Register_UnityEngine_RenderSettings_set_fog();

		//System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
		void Register_UnityEngine_RenderSettings_set_fogDensity();
		Register_UnityEngine_RenderSettings_set_fogDensity();

		//System.Void UnityEngine.RenderSettings::set_haloStrength(System.Single)
		void Register_UnityEngine_RenderSettings_set_haloStrength();
		Register_UnityEngine_RenderSettings_set_haloStrength();

		//System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
		void Register_UnityEngine_RenderSettings_set_skybox();
		Register_UnityEngine_RenderSettings_set_skybox();

	//End Registrations for type : UnityEngine.RenderSettings

	//Start Registrations for type : UnityEngine.RenderTexture

		//System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetHeight();
		Register_UnityEngine_RenderTexture_Internal_GetHeight();

		//System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
		void Register_UnityEngine_RenderTexture_Internal_GetWidth();
		Register_UnityEngine_RenderTexture_Internal_GetWidth();

	//End Registrations for type : UnityEngine.RenderTexture

	//Start Registrations for type : UnityEngine.Resources

		//UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
		void Register_UnityEngine_Resources_UnloadUnusedAssets();
		Register_UnityEngine_Resources_UnloadUnusedAssets();

		//UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
		void Register_UnityEngine_Resources_GetBuiltinResource();
		Register_UnityEngine_Resources_GetBuiltinResource();

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

		//UnityEngine.Object[] UnityEngine.Resources::LoadAll(System.String,System.Type)
		void Register_UnityEngine_Resources_LoadAll();
		Register_UnityEngine_Resources_LoadAll();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.Rigidbody

		//System.Boolean UnityEngine.Rigidbody::INTERNAL_CALL_IsSleeping(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_IsSleeping();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_IsSleeping();

		//System.Boolean UnityEngine.Rigidbody::get_isKinematic()
		void Register_UnityEngine_Rigidbody_get_isKinematic();
		Register_UnityEngine_Rigidbody_get_isKinematic();

		//System.Single UnityEngine.Rigidbody::get_mass()
		void Register_UnityEngine_Rigidbody_get_mass();
		Register_UnityEngine_Rigidbody_get_mass();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddExplosionForce(UnityEngine.Rigidbody,System.Single,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddExplosionForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddExplosionForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForceAtPosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddForceAtPosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeForce();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeForce();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddRelativeTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeTorque();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddRelativeTorque();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddTorque(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddTorque();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_AddTorque();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MovePosition();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_MoveRotation();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_Sleep(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_Sleep();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_Sleep();

		//System.Void UnityEngine.Rigidbody::INTERNAL_CALL_WakeUp(UnityEngine.Rigidbody)
		void Register_UnityEngine_Rigidbody_INTERNAL_CALL_WakeUp();
		Register_UnityEngine_Rigidbody_INTERNAL_CALL_WakeUp();

		//System.Void UnityEngine.Rigidbody::INTERNAL_get_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_get_velocity();
		Register_UnityEngine_Rigidbody_INTERNAL_get_velocity();

		//System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
		void Register_UnityEngine_Rigidbody_INTERNAL_set_velocity();
		Register_UnityEngine_Rigidbody_INTERNAL_set_velocity();

		//System.Void UnityEngine.Rigidbody::set_drag(System.Single)
		void Register_UnityEngine_Rigidbody_set_drag();
		Register_UnityEngine_Rigidbody_set_drag();

		//System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_freezeRotation();
		Register_UnityEngine_Rigidbody_set_freezeRotation();

		//System.Void UnityEngine.Rigidbody::set_isKinematic(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_isKinematic();
		Register_UnityEngine_Rigidbody_set_isKinematic();

		//System.Void UnityEngine.Rigidbody::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody_set_mass();
		Register_UnityEngine_Rigidbody_set_mass();

		//System.Void UnityEngine.Rigidbody::set_useGravity(System.Boolean)
		void Register_UnityEngine_Rigidbody_set_useGravity();
		Register_UnityEngine_Rigidbody_set_useGravity();

	//End Registrations for type : UnityEngine.Rigidbody

	//Start Registrations for type : UnityEngine.Rigidbody2D

		//System.Boolean UnityEngine.Rigidbody2D::IsSleeping()
		void Register_UnityEngine_Rigidbody2D_IsSleeping();
		Register_UnityEngine_Rigidbody2D_IsSleeping();

		//System.Single UnityEngine.Rigidbody2D::get_mass()
		void Register_UnityEngine_Rigidbody2D_get_mass();
		Register_UnityEngine_Rigidbody2D_get_mass();

		//System.Void UnityEngine.Rigidbody2D::AddTorque(System.Single,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_AddTorque();
		Register_UnityEngine_Rigidbody2D_AddTorque();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForce();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForceAtPosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForceAtPosition();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddForceAtPosition();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddRelativeForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddRelativeForce();
		Register_UnityEngine_Rigidbody2D_INTERNAL_CALL_AddRelativeForce();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_get_velocity(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_get_velocity();

		//System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
		void Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity();
		Register_UnityEngine_Rigidbody2D_INTERNAL_set_velocity();

		//System.Void UnityEngine.Rigidbody2D::Sleep()
		void Register_UnityEngine_Rigidbody2D_Sleep();
		Register_UnityEngine_Rigidbody2D_Sleep();

		//System.Void UnityEngine.Rigidbody2D::WakeUp()
		void Register_UnityEngine_Rigidbody2D_WakeUp();
		Register_UnityEngine_Rigidbody2D_WakeUp();

		//System.Void UnityEngine.Rigidbody2D::set_bodyType(UnityEngine.RigidbodyType2D)
		void Register_UnityEngine_Rigidbody2D_set_bodyType();
		Register_UnityEngine_Rigidbody2D_set_bodyType();

		//System.Void UnityEngine.Rigidbody2D::set_constraints(UnityEngine.RigidbodyConstraints2D)
		void Register_UnityEngine_Rigidbody2D_set_constraints();
		Register_UnityEngine_Rigidbody2D_set_constraints();

		//System.Void UnityEngine.Rigidbody2D::set_gravityScale(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_gravityScale();
		Register_UnityEngine_Rigidbody2D_set_gravityScale();

		//System.Void UnityEngine.Rigidbody2D::set_mass(System.Single)
		void Register_UnityEngine_Rigidbody2D_set_mass();
		Register_UnityEngine_Rigidbody2D_set_mass();

		//UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
		void Register_UnityEngine_Rigidbody2D_get_constraints();
		Register_UnityEngine_Rigidbody2D_get_constraints();

		//UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
		void Register_UnityEngine_Rigidbody2D_get_bodyType();
		Register_UnityEngine_Rigidbody2D_get_bodyType();

	//End Registrations for type : UnityEngine.Rigidbody2D

	//Start Registrations for type : UnityEngine.SceneManagement.Scene

		//System.Boolean UnityEngine.SceneManagement.Scene::GetIsDirtyInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetIsDirtyInternal();
		Register_UnityEngine_SceneManagement_Scene_GetIsDirtyInternal();

		//System.Boolean UnityEngine.SceneManagement.Scene::GetIsLoadedInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetIsLoadedInternal();
		Register_UnityEngine_SceneManagement_Scene_GetIsLoadedInternal();

		//System.Boolean UnityEngine.SceneManagement.Scene::IsValidInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_IsValidInternal();
		Register_UnityEngine_SceneManagement_Scene_IsValidInternal();

		//System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal();
		Register_UnityEngine_SceneManagement_Scene_GetBuildIndexInternal();

		//System.Int32 UnityEngine.SceneManagement.Scene::GetRootCountInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetRootCountInternal();
		Register_UnityEngine_SceneManagement_Scene_GetRootCountInternal();

		//System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetNameInternal();
		Register_UnityEngine_SceneManagement_Scene_GetNameInternal();

		//System.String UnityEngine.SceneManagement.Scene::GetPathInternal(System.Int32)
		void Register_UnityEngine_SceneManagement_Scene_GetPathInternal();
		Register_UnityEngine_SceneManagement_Scene_GetPathInternal();

		//System.Void UnityEngine.SceneManagement.Scene::GetRootGameObjectsInternal(System.Int32,System.Object)
		void Register_UnityEngine_SceneManagement_Scene_GetRootGameObjectsInternal();
		Register_UnityEngine_SceneManagement_Scene_GetRootGameObjectsInternal();

	//End Registrations for type : UnityEngine.SceneManagement.Scene

	//Start Registrations for type : UnityEngine.SceneManagement.SceneManager

		//System.Boolean UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_SetActiveScene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_SetActiveScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_SetActiveScene();

		//System.Boolean UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_UnloadSceneInternal(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_UnloadSceneInternal();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_UnloadSceneInternal();

		//System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCount()
		void Register_UnityEngine_SceneManagement_SceneManager_get_sceneCount();
		Register_UnityEngine_SceneManagement_SceneManager_get_sceneCount();

		//System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCountInBuildSettings()
		void Register_UnityEngine_SceneManagement_SceneManager_get_sceneCountInBuildSettings();
		Register_UnityEngine_SceneManagement_SceneManager_get_sceneCountInBuildSettings();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_CreateScene(System.String,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_CreateScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_CreateScene();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetActiveScene();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneAt(System.Int32,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneAt();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneAt();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneByName(System.String,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneByName();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneByName();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneByPath(System.String,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneByPath();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_GetSceneByPath();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_MergeScenes(UnityEngine.SceneManagement.Scene&,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_MergeScenes();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_MergeScenes();

		//System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_MoveGameObjectToScene(UnityEngine.GameObject,UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_MoveGameObjectToScene();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_MoveGameObjectToScene();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_UnloadSceneAsyncInternal(UnityEngine.SceneManagement.Scene&)
		void Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_UnloadSceneAsyncInternal();
		Register_UnityEngine_SceneManagement_SceneManager_INTERNAL_CALL_UnloadSceneAsyncInternal();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
		void Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_LoadSceneAsyncNameIndexInternal();

		//UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::UnloadSceneNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean&)
		void Register_UnityEngine_SceneManagement_SceneManager_UnloadSceneNameIndexInternal();
		Register_UnityEngine_SceneManagement_SceneManager_UnloadSceneNameIndexInternal();

	//End Registrations for type : UnityEngine.SceneManagement.SceneManager

	//Start Registrations for type : UnityEngine.Screen

		//System.Boolean UnityEngine.Screen::get_fullScreen()
		void Register_UnityEngine_Screen_get_fullScreen();
		Register_UnityEngine_Screen_get_fullScreen();

		//System.Int32 UnityEngine.Screen::get_height()
		void Register_UnityEngine_Screen_get_height();
		Register_UnityEngine_Screen_get_height();

		//System.Int32 UnityEngine.Screen::get_sleepTimeout()
		void Register_UnityEngine_Screen_get_sleepTimeout();
		Register_UnityEngine_Screen_get_sleepTimeout();

		//System.Int32 UnityEngine.Screen::get_width()
		void Register_UnityEngine_Screen_get_width();
		Register_UnityEngine_Screen_get_width();

		//System.Single UnityEngine.Screen::get_dpi()
		void Register_UnityEngine_Screen_get_dpi();
		Register_UnityEngine_Screen_get_dpi();

		//System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)
		void Register_UnityEngine_Screen_SetResolution();
		Register_UnityEngine_Screen_SetResolution();

		//UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
		void Register_UnityEngine_Screen_get_currentResolution();
		Register_UnityEngine_Screen_get_currentResolution();

		//UnityEngine.Resolution[] UnityEngine.Screen::get_resolutions()
		void Register_UnityEngine_Screen_get_resolutions();
		Register_UnityEngine_Screen_get_resolutions();

		//UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
		void Register_UnityEngine_Screen_get_orientation();
		Register_UnityEngine_Screen_get_orientation();

	//End Registrations for type : UnityEngine.Screen

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
		void Register_UnityEngine_ScriptableObject_CreateInstance();
		Register_UnityEngine_ScriptableObject_CreateInstance();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.Shader

		//System.Int32 UnityEngine.Shader::PropertyToID(System.String)
		void Register_UnityEngine_Shader_PropertyToID();
		Register_UnityEngine_Shader_PropertyToID();

	//End Registrations for type : UnityEngine.Shader

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends(System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[],System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.String[],System.Int32,System.Int32,System.Object)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.SortingLayer

		//System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
		void Register_UnityEngine_SortingLayer_GetLayerValueFromID();
		Register_UnityEngine_SortingLayer_GetLayerValueFromID();

	//End Registrations for type : UnityEngine.SortingLayer

	//Start Registrations for type : UnityEngine.Sprite

		//System.Boolean UnityEngine.Sprite::get_packed()
		void Register_UnityEngine_Sprite_get_packed();
		Register_UnityEngine_Sprite_get_packed();

		//System.Single UnityEngine.Sprite::get_pixelsPerUnit()
		void Register_UnityEngine_Sprite_get_pixelsPerUnit();
		Register_UnityEngine_Sprite_get_pixelsPerUnit();

		//System.Void UnityEngine.Sprite::INTERNAL_get_border(UnityEngine.Vector4&)
		void Register_UnityEngine_Sprite_INTERNAL_get_border();
		Register_UnityEngine_Sprite_INTERNAL_get_border();

		//System.Void UnityEngine.Sprite::INTERNAL_get_bounds(UnityEngine.Bounds&)
		void Register_UnityEngine_Sprite_INTERNAL_get_bounds();
		Register_UnityEngine_Sprite_INTERNAL_get_bounds();

		//System.Void UnityEngine.Sprite::INTERNAL_get_rect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_rect();
		Register_UnityEngine_Sprite_INTERNAL_get_rect();

		//System.Void UnityEngine.Sprite::INTERNAL_get_textureRect(UnityEngine.Rect&)
		void Register_UnityEngine_Sprite_INTERNAL_get_textureRect();
		Register_UnityEngine_Sprite_INTERNAL_get_textureRect();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
		void Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();
		Register_UnityEngine_Sprite_get_associatedAlphaSplitTexture();

		//UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
		void Register_UnityEngine_Sprite_get_texture();
		Register_UnityEngine_Sprite_get_texture();

	//End Registrations for type : UnityEngine.Sprite

	//Start Registrations for type : UnityEngine.Sprites.DataUtility

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetInnerUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetInnerUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetOuterUV(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetOuterUV();

		//System.Void UnityEngine.Sprites.DataUtility::INTERNAL_CALL_GetPadding(UnityEngine.Sprite,UnityEngine.Vector4&)
		void Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();
		Register_UnityEngine_Sprites_DataUtility_INTERNAL_CALL_GetPadding();

		//System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
		void Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();
		Register_UnityEngine_Sprites_DataUtility_Internal_GetMinSize();

	//End Registrations for type : UnityEngine.Sprites.DataUtility

	//Start Registrations for type : UnityEngine.SystemInfo

		//System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
		void Register_UnityEngine_SystemInfo_get_supports3DTextures();
		Register_UnityEngine_SystemInfo_get_supports3DTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsAccelerometer()
		void Register_UnityEngine_SystemInfo_get_supportsAccelerometer();
		Register_UnityEngine_SystemInfo_get_supportsAccelerometer();

		//System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
		void Register_UnityEngine_SystemInfo_get_supportsComputeShaders();
		Register_UnityEngine_SystemInfo_get_supportsComputeShaders();

		//System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
		void Register_UnityEngine_SystemInfo_get_supportsGyroscope();
		Register_UnityEngine_SystemInfo_get_supportsGyroscope();

		//System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
		void Register_UnityEngine_SystemInfo_get_supportsImageEffects();
		Register_UnityEngine_SystemInfo_get_supportsImageEffects();

		//System.Boolean UnityEngine.SystemInfo::get_supportsLocationService()
		void Register_UnityEngine_SystemInfo_get_supportsLocationService();
		Register_UnityEngine_SystemInfo_get_supportsLocationService();

		//System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
		void Register_UnityEngine_SystemInfo_get_supportsRenderTextures();
		Register_UnityEngine_SystemInfo_get_supportsRenderTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsRenderToCubemap()
		void Register_UnityEngine_SystemInfo_get_supportsRenderToCubemap();
		Register_UnityEngine_SystemInfo_get_supportsRenderToCubemap();

		//System.Boolean UnityEngine.SystemInfo::get_supportsShadows()
		void Register_UnityEngine_SystemInfo_get_supportsShadows();
		Register_UnityEngine_SystemInfo_get_supportsShadows();

		//System.Boolean UnityEngine.SystemInfo::get_supportsSparseTextures()
		void Register_UnityEngine_SystemInfo_get_supportsSparseTextures();
		Register_UnityEngine_SystemInfo_get_supportsSparseTextures();

		//System.Boolean UnityEngine.SystemInfo::get_supportsVibration()
		void Register_UnityEngine_SystemInfo_get_supportsVibration();
		Register_UnityEngine_SystemInfo_get_supportsVibration();

		//System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
		void Register_UnityEngine_SystemInfo_get_maxTextureSize();
		Register_UnityEngine_SystemInfo_get_maxTextureSize();

		//System.Int32 UnityEngine.SystemInfo::get_processorCount()
		void Register_UnityEngine_SystemInfo_get_processorCount();
		Register_UnityEngine_SystemInfo_get_processorCount();

		//System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
		void Register_UnityEngine_SystemInfo_get_supportedRenderTargetCount();
		Register_UnityEngine_SystemInfo_get_supportedRenderTargetCount();

		//System.Int32 UnityEngine.SystemInfo::get_supportsStencil()
		void Register_UnityEngine_SystemInfo_get_supportsStencil();
		Register_UnityEngine_SystemInfo_get_supportsStencil();

		//System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
		void Register_UnityEngine_SystemInfo_get_systemMemorySize();
		Register_UnityEngine_SystemInfo_get_systemMemorySize();

		//System.String UnityEngine.SystemInfo::get_deviceModel()
		void Register_UnityEngine_SystemInfo_get_deviceModel();
		Register_UnityEngine_SystemInfo_get_deviceModel();

		//System.String UnityEngine.SystemInfo::get_deviceName()
		void Register_UnityEngine_SystemInfo_get_deviceName();
		Register_UnityEngine_SystemInfo_get_deviceName();

		//System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
		void Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier();
		Register_UnityEngine_SystemInfo_get_deviceUniqueIdentifier();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceName();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceName();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceVendor()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceVendor();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceVendor();

		//System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
		void Register_UnityEngine_SystemInfo_get_graphicsDeviceVersion();
		Register_UnityEngine_SystemInfo_get_graphicsDeviceVersion();

		//System.String UnityEngine.SystemInfo::get_operatingSystem()
		void Register_UnityEngine_SystemInfo_get_operatingSystem();
		Register_UnityEngine_SystemInfo_get_operatingSystem();

		//System.String UnityEngine.SystemInfo::get_processorType()
		void Register_UnityEngine_SystemInfo_get_processorType();
		Register_UnityEngine_SystemInfo_get_processorType();

		//UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
		void Register_UnityEngine_SystemInfo_get_deviceType();
		Register_UnityEngine_SystemInfo_get_deviceType();

		//UnityEngine.NPOTSupport UnityEngine.SystemInfo::get_npotSupport()
		void Register_UnityEngine_SystemInfo_get_npotSupport();
		Register_UnityEngine_SystemInfo_get_npotSupport();

		//UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
		void Register_UnityEngine_SystemInfo_get_operatingSystemFamily();
		Register_UnityEngine_SystemInfo_get_operatingSystemFamily();

	//End Registrations for type : UnityEngine.SystemInfo

	//Start Registrations for type : UnityEngine.TextAsset

		//System.String UnityEngine.TextAsset::get_text()
		void Register_UnityEngine_TextAsset_get_text();
		Register_UnityEngine_TextAsset_get_text();

	//End Registrations for type : UnityEngine.TextAsset

	//Start Registrations for type : UnityEngine.TextGenerator

		//System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
		void Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();
		Register_UnityEngine_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp();

		//System.Int32 UnityEngine.TextGenerator::get_characterCount()
		void Register_UnityEngine_TextGenerator_get_characterCount();
		Register_UnityEngine_TextGenerator_get_characterCount();

		//System.Int32 UnityEngine.TextGenerator::get_lineCount()
		void Register_UnityEngine_TextGenerator_get_lineCount();
		Register_UnityEngine_TextGenerator_get_lineCount();

		//System.Void UnityEngine.TextGenerator::Dispose_cpp()
		void Register_UnityEngine_TextGenerator_Dispose_cpp();
		Register_UnityEngine_TextGenerator_Dispose_cpp();

		//System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetCharactersInternal();
		Register_UnityEngine_TextGenerator_GetCharactersInternal();

		//System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetLinesInternal();
		Register_UnityEngine_TextGenerator_GetLinesInternal();

		//System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
		void Register_UnityEngine_TextGenerator_GetVerticesInternal();
		Register_UnityEngine_TextGenerator_GetVerticesInternal();

		//System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
		void Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();
		Register_UnityEngine_TextGenerator_INTERNAL_get_rectExtents();

		//System.Void UnityEngine.TextGenerator::Init()
		void Register_UnityEngine_TextGenerator_Init();
		Register_UnityEngine_TextGenerator_Init();

	//End Registrations for type : UnityEngine.TextGenerator

	//Start Registrations for type : UnityEngine.TextMesh

		//System.Void UnityEngine.TextMesh::set_text(System.String)
		void Register_UnityEngine_TextMesh_set_text();
		Register_UnityEngine_TextMesh_set_text();

	//End Registrations for type : UnityEngine.TextMesh

	//Start Registrations for type : UnityEngine.Texture

		//System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetHeight();
		Register_UnityEngine_Texture_Internal_GetHeight();

		//System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
		void Register_UnityEngine_Texture_Internal_GetWidth();
		Register_UnityEngine_Texture_Internal_GetWidth();

		//System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
		void Register_UnityEngine_Texture_INTERNAL_get_texelSize();
		Register_UnityEngine_Texture_INTERNAL_get_texelSize();

		//UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
		void Register_UnityEngine_Texture_get_wrapMode();
		Register_UnityEngine_Texture_get_wrapMode();

	//End Registrations for type : UnityEngine.Texture

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Boolean UnityEngine.Texture2D::LoadImage(System.Byte[],System.Boolean)
		void Register_UnityEngine_Texture2D_LoadImage();
		Register_UnityEngine_Texture2D_LoadImage();

		//System.Byte[] UnityEngine.Texture2D::EncodeToPNG()
		void Register_UnityEngine_Texture2D_EncodeToPNG();
		Register_UnityEngine_Texture2D_EncodeToPNG();

		//System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
		void Register_UnityEngine_Texture2D_Apply();
		Register_UnityEngine_Texture2D_Apply();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_GetPixelBilinear();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_ReadPixels();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_ReadPixels();

		//System.Void UnityEngine.Texture2D::INTERNAL_CALL_SetPixel(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.Color&)
		void Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();
		Register_UnityEngine_Texture2D_INTERNAL_CALL_SetPixel();

		//System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_Create();
		Register_UnityEngine_Texture2D_Internal_Create();

		//System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
		void Register_UnityEngine_Texture2D_SetAllPixels32();
		Register_UnityEngine_Texture2D_SetAllPixels32();

		//System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
		void Register_UnityEngine_Texture2D_SetPixels();
		Register_UnityEngine_Texture2D_SetPixels();

		//UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
		void Register_UnityEngine_Texture2D_GetPixels32();
		Register_UnityEngine_Texture2D_GetPixels32();

		//UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
		void Register_UnityEngine_Texture2D_get_whiteTexture();
		Register_UnityEngine_Texture2D_get_whiteTexture();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Time

		//System.Int32 UnityEngine.Time::get_frameCount()
		void Register_UnityEngine_Time_get_frameCount();
		Register_UnityEngine_Time_get_frameCount();

		//System.Single UnityEngine.Time::get_deltaTime()
		void Register_UnityEngine_Time_get_deltaTime();
		Register_UnityEngine_Time_get_deltaTime();

		//System.Single UnityEngine.Time::get_fixedDeltaTime()
		void Register_UnityEngine_Time_get_fixedDeltaTime();
		Register_UnityEngine_Time_get_fixedDeltaTime();

		//System.Single UnityEngine.Time::get_realtimeSinceStartup()
		void Register_UnityEngine_Time_get_realtimeSinceStartup();
		Register_UnityEngine_Time_get_realtimeSinceStartup();

		//System.Single UnityEngine.Time::get_smoothDeltaTime()
		void Register_UnityEngine_Time_get_smoothDeltaTime();
		Register_UnityEngine_Time_get_smoothDeltaTime();

		//System.Single UnityEngine.Time::get_time()
		void Register_UnityEngine_Time_get_time();
		Register_UnityEngine_Time_get_time();

		//System.Single UnityEngine.Time::get_timeScale()
		void Register_UnityEngine_Time_get_timeScale();
		Register_UnityEngine_Time_get_timeScale();

		//System.Single UnityEngine.Time::get_timeSinceLevelLoad()
		void Register_UnityEngine_Time_get_timeSinceLevelLoad();
		Register_UnityEngine_Time_get_timeSinceLevelLoad();

		//System.Single UnityEngine.Time::get_unscaledDeltaTime()
		void Register_UnityEngine_Time_get_unscaledDeltaTime();
		Register_UnityEngine_Time_get_unscaledDeltaTime();

		//System.Single UnityEngine.Time::get_unscaledTime()
		void Register_UnityEngine_Time_get_unscaledTime();
		Register_UnityEngine_Time_get_unscaledTime();

		//System.Void UnityEngine.Time::set_fixedDeltaTime(System.Single)
		void Register_UnityEngine_Time_set_fixedDeltaTime();
		Register_UnityEngine_Time_set_fixedDeltaTime();

		//System.Void UnityEngine.Time::set_timeScale(System.Single)
		void Register_UnityEngine_Time_set_timeScale();
		Register_UnityEngine_Time_set_timeScale();

	//End Registrations for type : UnityEngine.Time

	//Start Registrations for type : UnityEngine.TouchScreenKeyboard

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
		void Register_UnityEngine_TouchScreenKeyboard_get_active();
		Register_UnityEngine_TouchScreenKeyboard_get_active();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
		void Register_UnityEngine_TouchScreenKeyboard_get_canGetSelection();
		Register_UnityEngine_TouchScreenKeyboard_get_canGetSelection();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
		void Register_UnityEngine_TouchScreenKeyboard_get_done();
		Register_UnityEngine_TouchScreenKeyboard_get_done();

		//System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
		void Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();
		Register_UnityEngine_TouchScreenKeyboard_get_wasCanceled();

		//System.String UnityEngine.TouchScreenKeyboard::get_text()
		void Register_UnityEngine_TouchScreenKeyboard_get_text();
		Register_UnityEngine_TouchScreenKeyboard_get_text();

		//System.Void UnityEngine.TouchScreenKeyboard::Destroy()
		void Register_UnityEngine_TouchScreenKeyboard_Destroy();
		Register_UnityEngine_TouchScreenKeyboard_Destroy();

		//System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
		void Register_UnityEngine_TouchScreenKeyboard_GetSelectionInternal();
		Register_UnityEngine_TouchScreenKeyboard_GetSelectionInternal();

		//System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
		void Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();
		Register_UnityEngine_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper();

		//System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_active();
		Register_UnityEngine_TouchScreenKeyboard_set_active();

		//System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
		void Register_UnityEngine_TouchScreenKeyboard_set_hideInput();
		Register_UnityEngine_TouchScreenKeyboard_set_hideInput();

		//System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
		void Register_UnityEngine_TouchScreenKeyboard_set_text();
		Register_UnityEngine_TouchScreenKeyboard_set_text();

	//End Registrations for type : UnityEngine.TouchScreenKeyboard

	//Start Registrations for type : UnityEngine.Transform

		//System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
		void Register_UnityEngine_Transform_IsChildOf();
		Register_UnityEngine_Transform_IsChildOf();

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//System.Void UnityEngine.Transform::DetachChildren()
		void Register_UnityEngine_Transform_DetachChildren();
		Register_UnityEngine_Transform_DetachChildren();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_InverseTransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_LookAt(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();
		Register_UnityEngine_Transform_INTERNAL_CALL_LookAt();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformDirection();

		//System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();
		Register_UnityEngine_Transform_INTERNAL_CALL_TransformPoint();

		//System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localPosition();
		Register_UnityEngine_Transform_INTERNAL_get_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_localRotation();
		Register_UnityEngine_Transform_INTERNAL_get_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_localScale();
		Register_UnityEngine_Transform_INTERNAL_get_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_lossyScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_lossyScale();
		Register_UnityEngine_Transform_INTERNAL_get_lossyScale();

		//System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_get_position();
		Register_UnityEngine_Transform_INTERNAL_get_position();

		//System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_get_rotation();
		Register_UnityEngine_Transform_INTERNAL_get_rotation();

		//System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();
		Register_UnityEngine_Transform_INTERNAL_get_worldToLocalMatrix();

		//System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localPosition();
		Register_UnityEngine_Transform_INTERNAL_set_localPosition();

		//System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_localRotation();
		Register_UnityEngine_Transform_INTERNAL_set_localRotation();

		//System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_localScale();
		Register_UnityEngine_Transform_INTERNAL_set_localScale();

		//System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
		void Register_UnityEngine_Transform_INTERNAL_set_position();
		Register_UnityEngine_Transform_INTERNAL_set_position();

		//System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
		void Register_UnityEngine_Transform_INTERNAL_set_rotation();
		Register_UnityEngine_Transform_INTERNAL_set_rotation();

		//System.Void UnityEngine.Transform::SetAsFirstSibling()
		void Register_UnityEngine_Transform_SetAsFirstSibling();
		Register_UnityEngine_Transform_SetAsFirstSibling();

		//System.Void UnityEngine.Transform::SetAsLastSibling()
		void Register_UnityEngine_Transform_SetAsLastSibling();
		Register_UnityEngine_Transform_SetAsLastSibling();

		//System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
		void Register_UnityEngine_Transform_SetParent();
		Register_UnityEngine_Transform_SetParent();

		//System.Void UnityEngine.Transform::SetSiblingIndex(System.Int32)
		void Register_UnityEngine_Transform_SetSiblingIndex();
		Register_UnityEngine_Transform_SetSiblingIndex();

		//System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
		void Register_UnityEngine_Transform_set_parentInternal();
		Register_UnityEngine_Transform_set_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::Find(System.String)
		void Register_UnityEngine_Transform_Find();
		Register_UnityEngine_Transform_Find();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

		//UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
		void Register_UnityEngine_Transform_get_parentInternal();
		Register_UnityEngine_Transform_get_parentInternal();

		//UnityEngine.Transform UnityEngine.Transform::get_root()
		void Register_UnityEngine_Transform_get_root();
		Register_UnityEngine_Transform_get_root();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.UnhandledExceptionHandler

		//System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
		void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();
		Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();

	//End Registrations for type : UnityEngine.UnhandledExceptionHandler

	//Start Registrations for type : UnityEngine.Vector3

		//System.Void UnityEngine.Vector3::INTERNAL_CALL_RotateTowards(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)
		void Register_UnityEngine_Vector3_INTERNAL_CALL_RotateTowards();
		Register_UnityEngine_Vector3_INTERNAL_CALL_RotateTowards();

	//End Registrations for type : UnityEngine.Vector3

	//Start Registrations for type : UnityEngine.WheelJoint2D

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_get_motor();
		Register_UnityEngine_WheelJoint2D_INTERNAL_get_motor();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_get_suspension(UnityEngine.JointSuspension2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_get_suspension();
		Register_UnityEngine_WheelJoint2D_INTERNAL_get_suspension();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_set_motor();
		Register_UnityEngine_WheelJoint2D_INTERNAL_set_motor();

		//System.Void UnityEngine.WheelJoint2D::INTERNAL_set_suspension(UnityEngine.JointSuspension2D&)
		void Register_UnityEngine_WheelJoint2D_INTERNAL_set_suspension();
		Register_UnityEngine_WheelJoint2D_INTERNAL_set_suspension();

		//System.Void UnityEngine.WheelJoint2D::set_useMotor(System.Boolean)
		void Register_UnityEngine_WheelJoint2D_set_useMotor();
		Register_UnityEngine_WheelJoint2D_set_useMotor();

	//End Registrations for type : UnityEngine.WheelJoint2D

	//Start Registrations for type : UnityEngine.WWW

		//System.Boolean UnityEngine.WWW::get_isDone()
		void Register_UnityEngine_WWW_get_isDone();
		Register_UnityEngine_WWW_get_isDone();

		//System.Byte[] UnityEngine.WWW::get_bytes()
		void Register_UnityEngine_WWW_get_bytes();
		Register_UnityEngine_WWW_get_bytes();

		//System.Single UnityEngine.WWW::get_progress()
		void Register_UnityEngine_WWW_get_progress();
		Register_UnityEngine_WWW_get_progress();

		//System.Single UnityEngine.WWW::get_uploadProgress()
		void Register_UnityEngine_WWW_get_uploadProgress();
		Register_UnityEngine_WWW_get_uploadProgress();

		//System.String UnityEngine.WWW::get_error()
		void Register_UnityEngine_WWW_get_error();
		Register_UnityEngine_WWW_get_error();

		//System.String UnityEngine.WWW::get_responseHeadersString()
		void Register_UnityEngine_WWW_get_responseHeadersString();
		Register_UnityEngine_WWW_get_responseHeadersString();

		//System.Void UnityEngine.WWW::DestroyWWW(System.Boolean)
		void Register_UnityEngine_WWW_DestroyWWW();
		Register_UnityEngine_WWW_DestroyWWW();

		//System.Void UnityEngine.WWW::InitWWW(System.String,System.Byte[],System.String[])
		void Register_UnityEngine_WWW_InitWWW();
		Register_UnityEngine_WWW_InitWWW();

	//End Registrations for type : UnityEngine.WWW

}
