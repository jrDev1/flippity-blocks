﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmRect
struct  FsmRect_t19023354  : public NamedVariable_t3026441313
{
public:
	// UnityEngine.Rect HutongGames.PlayMaker.FsmRect::value
	Rect_t3681755626  ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmRect_t19023354, ___value_5)); }
	inline Rect_t3681755626  get_value_5() const { return ___value_5; }
	inline Rect_t3681755626 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Rect_t3681755626  value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
