﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchIsOverRectTransform
struct  EasyTouchIsOverRectTransform_t3278465  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.EasyTouchIsOverRectTransform::rectTransformOwner
	FsmGameObject_t3097142863 * ___rectTransformOwner_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EasyTouchIsOverRectTransform::isOverRectTransform
	FsmBool_t664485696 * ___isOverRectTransform_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EasyTouchIsOverRectTransform::overEvent
	FsmEvent_t1258573736 * ___overEvent_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EasyTouchIsOverRectTransform::notOverEvent
	FsmEvent_t1258573736 * ___notOverEvent_14;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchIsOverRectTransform::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_15;

public:
	inline static int32_t get_offset_of_rectTransformOwner_11() { return static_cast<int32_t>(offsetof(EasyTouchIsOverRectTransform_t3278465, ___rectTransformOwner_11)); }
	inline FsmGameObject_t3097142863 * get_rectTransformOwner_11() const { return ___rectTransformOwner_11; }
	inline FsmGameObject_t3097142863 ** get_address_of_rectTransformOwner_11() { return &___rectTransformOwner_11; }
	inline void set_rectTransformOwner_11(FsmGameObject_t3097142863 * value)
	{
		___rectTransformOwner_11 = value;
		Il2CppCodeGenWriteBarrier(&___rectTransformOwner_11, value);
	}

	inline static int32_t get_offset_of_isOverRectTransform_12() { return static_cast<int32_t>(offsetof(EasyTouchIsOverRectTransform_t3278465, ___isOverRectTransform_12)); }
	inline FsmBool_t664485696 * get_isOverRectTransform_12() const { return ___isOverRectTransform_12; }
	inline FsmBool_t664485696 ** get_address_of_isOverRectTransform_12() { return &___isOverRectTransform_12; }
	inline void set_isOverRectTransform_12(FsmBool_t664485696 * value)
	{
		___isOverRectTransform_12 = value;
		Il2CppCodeGenWriteBarrier(&___isOverRectTransform_12, value);
	}

	inline static int32_t get_offset_of_overEvent_13() { return static_cast<int32_t>(offsetof(EasyTouchIsOverRectTransform_t3278465, ___overEvent_13)); }
	inline FsmEvent_t1258573736 * get_overEvent_13() const { return ___overEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_overEvent_13() { return &___overEvent_13; }
	inline void set_overEvent_13(FsmEvent_t1258573736 * value)
	{
		___overEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___overEvent_13, value);
	}

	inline static int32_t get_offset_of_notOverEvent_14() { return static_cast<int32_t>(offsetof(EasyTouchIsOverRectTransform_t3278465, ___notOverEvent_14)); }
	inline FsmEvent_t1258573736 * get_notOverEvent_14() const { return ___notOverEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_notOverEvent_14() { return &___notOverEvent_14; }
	inline void set_notOverEvent_14(FsmEvent_t1258573736 * value)
	{
		___notOverEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___notOverEvent_14, value);
	}

	inline static int32_t get_offset_of_proxy_15() { return static_cast<int32_t>(offsetof(EasyTouchIsOverRectTransform_t3278465, ___proxy_15)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_15() const { return ___proxy_15; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_15() { return &___proxy_15; }
	inline void set_proxy_15(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_15 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
