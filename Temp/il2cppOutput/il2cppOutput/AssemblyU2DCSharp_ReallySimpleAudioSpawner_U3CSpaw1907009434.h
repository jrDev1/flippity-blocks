﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// ReallySimpleAudioSpawner
struct ReallySimpleAudioSpawner_t1135031217;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReallySimpleAudioSpawner/<Spawner>c__Iterator1
struct  U3CSpawnerU3Ec__Iterator1_t1907009434  : public Il2CppObject
{
public:
	// UnityEngine.AudioSource ReallySimpleAudioSpawner/<Spawner>c__Iterator1::<current>__1
	AudioSource_t1135106623 * ___U3CcurrentU3E__1_0;
	// ReallySimpleAudioSpawner ReallySimpleAudioSpawner/<Spawner>c__Iterator1::$this
	ReallySimpleAudioSpawner_t1135031217 * ___U24this_1;
	// System.Object ReallySimpleAudioSpawner/<Spawner>c__Iterator1::$current
	Il2CppObject * ___U24current_2;
	// System.Boolean ReallySimpleAudioSpawner/<Spawner>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 ReallySimpleAudioSpawner/<Spawner>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcurrentU3E__1_0() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t1907009434, ___U3CcurrentU3E__1_0)); }
	inline AudioSource_t1135106623 * get_U3CcurrentU3E__1_0() const { return ___U3CcurrentU3E__1_0; }
	inline AudioSource_t1135106623 ** get_address_of_U3CcurrentU3E__1_0() { return &___U3CcurrentU3E__1_0; }
	inline void set_U3CcurrentU3E__1_0(AudioSource_t1135106623 * value)
	{
		___U3CcurrentU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentU3E__1_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t1907009434, ___U24this_1)); }
	inline ReallySimpleAudioSpawner_t1135031217 * get_U24this_1() const { return ___U24this_1; }
	inline ReallySimpleAudioSpawner_t1135031217 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ReallySimpleAudioSpawner_t1135031217 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t1907009434, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t1907009434, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CSpawnerU3Ec__Iterator1_t1907009434, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
