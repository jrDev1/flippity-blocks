﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Sprite
struct Sprite_t309593783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.Style
struct  Style_t1578998053  : public Il2CppObject
{
public:
	// UnityEngine.Color SRF.UI.Style::ActiveColor
	Color_t2020392075  ___ActiveColor_0;
	// UnityEngine.Color SRF.UI.Style::DisabledColor
	Color_t2020392075  ___DisabledColor_1;
	// UnityEngine.Color SRF.UI.Style::HoverColor
	Color_t2020392075  ___HoverColor_2;
	// UnityEngine.Sprite SRF.UI.Style::Image
	Sprite_t309593783 * ___Image_3;
	// UnityEngine.Color SRF.UI.Style::NormalColor
	Color_t2020392075  ___NormalColor_4;

public:
	inline static int32_t get_offset_of_ActiveColor_0() { return static_cast<int32_t>(offsetof(Style_t1578998053, ___ActiveColor_0)); }
	inline Color_t2020392075  get_ActiveColor_0() const { return ___ActiveColor_0; }
	inline Color_t2020392075 * get_address_of_ActiveColor_0() { return &___ActiveColor_0; }
	inline void set_ActiveColor_0(Color_t2020392075  value)
	{
		___ActiveColor_0 = value;
	}

	inline static int32_t get_offset_of_DisabledColor_1() { return static_cast<int32_t>(offsetof(Style_t1578998053, ___DisabledColor_1)); }
	inline Color_t2020392075  get_DisabledColor_1() const { return ___DisabledColor_1; }
	inline Color_t2020392075 * get_address_of_DisabledColor_1() { return &___DisabledColor_1; }
	inline void set_DisabledColor_1(Color_t2020392075  value)
	{
		___DisabledColor_1 = value;
	}

	inline static int32_t get_offset_of_HoverColor_2() { return static_cast<int32_t>(offsetof(Style_t1578998053, ___HoverColor_2)); }
	inline Color_t2020392075  get_HoverColor_2() const { return ___HoverColor_2; }
	inline Color_t2020392075 * get_address_of_HoverColor_2() { return &___HoverColor_2; }
	inline void set_HoverColor_2(Color_t2020392075  value)
	{
		___HoverColor_2 = value;
	}

	inline static int32_t get_offset_of_Image_3() { return static_cast<int32_t>(offsetof(Style_t1578998053, ___Image_3)); }
	inline Sprite_t309593783 * get_Image_3() const { return ___Image_3; }
	inline Sprite_t309593783 ** get_address_of_Image_3() { return &___Image_3; }
	inline void set_Image_3(Sprite_t309593783 * value)
	{
		___Image_3 = value;
		Il2CppCodeGenWriteBarrier(&___Image_3, value);
	}

	inline static int32_t get_offset_of_NormalColor_4() { return static_cast<int32_t>(offsetof(Style_t1578998053, ___NormalColor_4)); }
	inline Color_t2020392075  get_NormalColor_4() const { return ___NormalColor_4; }
	inline Color_t2020392075 * get_address_of_NormalColor_4() { return &___NormalColor_4; }
	inline void set_NormalColor_4(Color_t2020392075  value)
	{
		___NormalColor_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
