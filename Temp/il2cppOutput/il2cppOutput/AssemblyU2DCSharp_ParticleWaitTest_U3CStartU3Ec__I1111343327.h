﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// ParticleWaitTest
struct ParticleWaitTest_t1154004511;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleWaitTest/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1111343327  : public Il2CppObject
{
public:
	// PathologicalGames.SpawnPool ParticleWaitTest/<Start>c__Iterator0::<particlesPool>__0
	SpawnPool_t2419717525 * ___U3CparticlesPoolU3E__0_0;
	// UnityEngine.ParticleSystem ParticleWaitTest/<Start>c__Iterator0::<prefab>__0
	ParticleSystem_t3394631041 * ___U3CprefabU3E__0_1;
	// UnityEngine.Vector3 ParticleWaitTest/<Start>c__Iterator0::<prefabXform>__0
	Vector3_t2243707580  ___U3CprefabXformU3E__0_2;
	// UnityEngine.Quaternion ParticleWaitTest/<Start>c__Iterator0::<prefabRot>__0
	Quaternion_t4030073918  ___U3CprefabRotU3E__0_3;
	// UnityEngine.ParticleSystem ParticleWaitTest/<Start>c__Iterator0::<emitter>__1
	ParticleSystem_t3394631041 * ___U3CemitterU3E__1_4;
	// UnityEngine.ParticleSystem ParticleWaitTest/<Start>c__Iterator0::<inst>__1
	ParticleSystem_t3394631041 * ___U3CinstU3E__1_5;
	// ParticleWaitTest ParticleWaitTest/<Start>c__Iterator0::$this
	ParticleWaitTest_t1154004511 * ___U24this_6;
	// System.Object ParticleWaitTest/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean ParticleWaitTest/<Start>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 ParticleWaitTest/<Start>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CparticlesPoolU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U3CparticlesPoolU3E__0_0)); }
	inline SpawnPool_t2419717525 * get_U3CparticlesPoolU3E__0_0() const { return ___U3CparticlesPoolU3E__0_0; }
	inline SpawnPool_t2419717525 ** get_address_of_U3CparticlesPoolU3E__0_0() { return &___U3CparticlesPoolU3E__0_0; }
	inline void set_U3CparticlesPoolU3E__0_0(SpawnPool_t2419717525 * value)
	{
		___U3CparticlesPoolU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CparticlesPoolU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CprefabU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U3CprefabU3E__0_1)); }
	inline ParticleSystem_t3394631041 * get_U3CprefabU3E__0_1() const { return ___U3CprefabU3E__0_1; }
	inline ParticleSystem_t3394631041 ** get_address_of_U3CprefabU3E__0_1() { return &___U3CprefabU3E__0_1; }
	inline void set_U3CprefabU3E__0_1(ParticleSystem_t3394631041 * value)
	{
		___U3CprefabU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprefabU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CprefabXformU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U3CprefabXformU3E__0_2)); }
	inline Vector3_t2243707580  get_U3CprefabXformU3E__0_2() const { return ___U3CprefabXformU3E__0_2; }
	inline Vector3_t2243707580 * get_address_of_U3CprefabXformU3E__0_2() { return &___U3CprefabXformU3E__0_2; }
	inline void set_U3CprefabXformU3E__0_2(Vector3_t2243707580  value)
	{
		___U3CprefabXformU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CprefabRotU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U3CprefabRotU3E__0_3)); }
	inline Quaternion_t4030073918  get_U3CprefabRotU3E__0_3() const { return ___U3CprefabRotU3E__0_3; }
	inline Quaternion_t4030073918 * get_address_of_U3CprefabRotU3E__0_3() { return &___U3CprefabRotU3E__0_3; }
	inline void set_U3CprefabRotU3E__0_3(Quaternion_t4030073918  value)
	{
		___U3CprefabRotU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CemitterU3E__1_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U3CemitterU3E__1_4)); }
	inline ParticleSystem_t3394631041 * get_U3CemitterU3E__1_4() const { return ___U3CemitterU3E__1_4; }
	inline ParticleSystem_t3394631041 ** get_address_of_U3CemitterU3E__1_4() { return &___U3CemitterU3E__1_4; }
	inline void set_U3CemitterU3E__1_4(ParticleSystem_t3394631041 * value)
	{
		___U3CemitterU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CemitterU3E__1_4, value);
	}

	inline static int32_t get_offset_of_U3CinstU3E__1_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U3CinstU3E__1_5)); }
	inline ParticleSystem_t3394631041 * get_U3CinstU3E__1_5() const { return ___U3CinstU3E__1_5; }
	inline ParticleSystem_t3394631041 ** get_address_of_U3CinstU3E__1_5() { return &___U3CinstU3E__1_5; }
	inline void set_U3CinstU3E__1_5(ParticleSystem_t3394631041 * value)
	{
		___U3CinstU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CinstU3E__1_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U24this_6)); }
	inline ParticleWaitTest_t1154004511 * get_U24this_6() const { return ___U24this_6; }
	inline ParticleWaitTest_t1154004511 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(ParticleWaitTest_t1154004511 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1111343327, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
