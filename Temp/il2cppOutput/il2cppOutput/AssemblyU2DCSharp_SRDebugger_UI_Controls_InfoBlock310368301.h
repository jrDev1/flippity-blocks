﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.InfoBlock
struct  InfoBlock_t310368301  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.UI.Text SRDebugger.UI.Controls.InfoBlock::Content
	Text_t356221433 * ___Content_9;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.InfoBlock::Title
	Text_t356221433 * ___Title_10;

public:
	inline static int32_t get_offset_of_Content_9() { return static_cast<int32_t>(offsetof(InfoBlock_t310368301, ___Content_9)); }
	inline Text_t356221433 * get_Content_9() const { return ___Content_9; }
	inline Text_t356221433 ** get_address_of_Content_9() { return &___Content_9; }
	inline void set_Content_9(Text_t356221433 * value)
	{
		___Content_9 = value;
		Il2CppCodeGenWriteBarrier(&___Content_9, value);
	}

	inline static int32_t get_offset_of_Title_10() { return static_cast<int32_t>(offsetof(InfoBlock_t310368301, ___Title_10)); }
	inline Text_t356221433 * get_Title_10() const { return ___Title_10; }
	inline Text_t356221433 ** get_address_of_Title_10() { return &___Title_10; }
	inline void set_Title_10(Text_t356221433 * value)
	{
		___Title_10 = value;
		Il2CppCodeGenWriteBarrier(&___Title_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
