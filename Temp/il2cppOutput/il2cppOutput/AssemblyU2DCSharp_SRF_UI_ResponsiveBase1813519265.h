﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ResponsiveBase
struct  ResponsiveBase_t1813519265  : public SRMonoBehaviour_t2352136145
{
public:
	// System.Boolean SRF.UI.ResponsiveBase::_queueRefresh
	bool ____queueRefresh_8;

public:
	inline static int32_t get_offset_of__queueRefresh_8() { return static_cast<int32_t>(offsetof(ResponsiveBase_t1813519265, ____queueRefresh_8)); }
	inline bool get__queueRefresh_8() const { return ____queueRefresh_8; }
	inline bool* get_address_of__queueRefresh_8() { return &____queueRefresh_8; }
	inline void set__queueRefresh_8(bool value)
	{
		____queueRefresh_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
