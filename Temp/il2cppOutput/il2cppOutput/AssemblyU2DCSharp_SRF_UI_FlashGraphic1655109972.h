﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.UI.Graphic
struct Graphic_t2426225576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.FlashGraphic
struct  FlashGraphic_t1655109972  : public UIBehaviour_t3960014691
{
public:
	// System.Single SRF.UI.FlashGraphic::DecayTime
	float ___DecayTime_2;
	// UnityEngine.Color SRF.UI.FlashGraphic::DefaultColor
	Color_t2020392075  ___DefaultColor_3;
	// UnityEngine.Color SRF.UI.FlashGraphic::FlashColor
	Color_t2020392075  ___FlashColor_4;
	// UnityEngine.UI.Graphic SRF.UI.FlashGraphic::Target
	Graphic_t2426225576 * ___Target_5;

public:
	inline static int32_t get_offset_of_DecayTime_2() { return static_cast<int32_t>(offsetof(FlashGraphic_t1655109972, ___DecayTime_2)); }
	inline float get_DecayTime_2() const { return ___DecayTime_2; }
	inline float* get_address_of_DecayTime_2() { return &___DecayTime_2; }
	inline void set_DecayTime_2(float value)
	{
		___DecayTime_2 = value;
	}

	inline static int32_t get_offset_of_DefaultColor_3() { return static_cast<int32_t>(offsetof(FlashGraphic_t1655109972, ___DefaultColor_3)); }
	inline Color_t2020392075  get_DefaultColor_3() const { return ___DefaultColor_3; }
	inline Color_t2020392075 * get_address_of_DefaultColor_3() { return &___DefaultColor_3; }
	inline void set_DefaultColor_3(Color_t2020392075  value)
	{
		___DefaultColor_3 = value;
	}

	inline static int32_t get_offset_of_FlashColor_4() { return static_cast<int32_t>(offsetof(FlashGraphic_t1655109972, ___FlashColor_4)); }
	inline Color_t2020392075  get_FlashColor_4() const { return ___FlashColor_4; }
	inline Color_t2020392075 * get_address_of_FlashColor_4() { return &___FlashColor_4; }
	inline void set_FlashColor_4(Color_t2020392075  value)
	{
		___FlashColor_4 = value;
	}

	inline static int32_t get_offset_of_Target_5() { return static_cast<int32_t>(offsetof(FlashGraphic_t1655109972, ___Target_5)); }
	inline Graphic_t2426225576 * get_Target_5() const { return ___Target_5; }
	inline Graphic_t2426225576 ** get_address_of_Target_5() { return &___Target_5; }
	inline void set_Target_5(Graphic_t2426225576 * value)
	{
		___Target_5 = value;
		Il2CppCodeGenWriteBarrier(&___Target_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
