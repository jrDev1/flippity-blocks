﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1205937924.h"

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint
struct  EasyTouchGetTouchToWorldPoint_t4140574269  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint/DepthType HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint::depthType
	int32_t ___depthType_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint::z
	FsmFloat_t937133978 * ___z_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint::position
	FsmVector3_t3996534004 * ___position_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint::gameObjectReference
	FsmGameObject_t3097142863 * ___gameObjectReference_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint::worldPosition
	FsmVector3_t3996534004 * ___worldPosition_15;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchGetTouchToWorldPoint::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_16;

public:
	inline static int32_t get_offset_of_depthType_11() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchToWorldPoint_t4140574269, ___depthType_11)); }
	inline int32_t get_depthType_11() const { return ___depthType_11; }
	inline int32_t* get_address_of_depthType_11() { return &___depthType_11; }
	inline void set_depthType_11(int32_t value)
	{
		___depthType_11 = value;
	}

	inline static int32_t get_offset_of_z_12() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchToWorldPoint_t4140574269, ___z_12)); }
	inline FsmFloat_t937133978 * get_z_12() const { return ___z_12; }
	inline FsmFloat_t937133978 ** get_address_of_z_12() { return &___z_12; }
	inline void set_z_12(FsmFloat_t937133978 * value)
	{
		___z_12 = value;
		Il2CppCodeGenWriteBarrier(&___z_12, value);
	}

	inline static int32_t get_offset_of_position_13() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchToWorldPoint_t4140574269, ___position_13)); }
	inline FsmVector3_t3996534004 * get_position_13() const { return ___position_13; }
	inline FsmVector3_t3996534004 ** get_address_of_position_13() { return &___position_13; }
	inline void set_position_13(FsmVector3_t3996534004 * value)
	{
		___position_13 = value;
		Il2CppCodeGenWriteBarrier(&___position_13, value);
	}

	inline static int32_t get_offset_of_gameObjectReference_14() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchToWorldPoint_t4140574269, ___gameObjectReference_14)); }
	inline FsmGameObject_t3097142863 * get_gameObjectReference_14() const { return ___gameObjectReference_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObjectReference_14() { return &___gameObjectReference_14; }
	inline void set_gameObjectReference_14(FsmGameObject_t3097142863 * value)
	{
		___gameObjectReference_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectReference_14, value);
	}

	inline static int32_t get_offset_of_worldPosition_15() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchToWorldPoint_t4140574269, ___worldPosition_15)); }
	inline FsmVector3_t3996534004 * get_worldPosition_15() const { return ___worldPosition_15; }
	inline FsmVector3_t3996534004 ** get_address_of_worldPosition_15() { return &___worldPosition_15; }
	inline void set_worldPosition_15(FsmVector3_t3996534004 * value)
	{
		___worldPosition_15 = value;
		Il2CppCodeGenWriteBarrier(&___worldPosition_15, value);
	}

	inline static int32_t get_offset_of_proxy_16() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchToWorldPoint_t4140574269, ___proxy_16)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_16() const { return ___proxy_16; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_16() { return &___proxy_16; }
	inline void set_proxy_16(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_16 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
