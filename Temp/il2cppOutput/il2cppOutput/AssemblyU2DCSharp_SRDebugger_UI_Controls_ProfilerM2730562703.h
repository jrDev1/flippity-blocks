﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Slider
struct Slider_t297367283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ProfilerMemoryBlock
struct  ProfilerMemoryBlock_t2730562703  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Single SRDebugger.UI.Controls.ProfilerMemoryBlock::_lastRefresh
	float ____lastRefresh_9;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerMemoryBlock::CurrentUsedText
	Text_t356221433 * ___CurrentUsedText_10;
	// UnityEngine.UI.Slider SRDebugger.UI.Controls.ProfilerMemoryBlock::Slider
	Slider_t297367283 * ___Slider_11;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerMemoryBlock::TotalAllocatedText
	Text_t356221433 * ___TotalAllocatedText_12;

public:
	inline static int32_t get_offset_of__lastRefresh_9() { return static_cast<int32_t>(offsetof(ProfilerMemoryBlock_t2730562703, ____lastRefresh_9)); }
	inline float get__lastRefresh_9() const { return ____lastRefresh_9; }
	inline float* get_address_of__lastRefresh_9() { return &____lastRefresh_9; }
	inline void set__lastRefresh_9(float value)
	{
		____lastRefresh_9 = value;
	}

	inline static int32_t get_offset_of_CurrentUsedText_10() { return static_cast<int32_t>(offsetof(ProfilerMemoryBlock_t2730562703, ___CurrentUsedText_10)); }
	inline Text_t356221433 * get_CurrentUsedText_10() const { return ___CurrentUsedText_10; }
	inline Text_t356221433 ** get_address_of_CurrentUsedText_10() { return &___CurrentUsedText_10; }
	inline void set_CurrentUsedText_10(Text_t356221433 * value)
	{
		___CurrentUsedText_10 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentUsedText_10, value);
	}

	inline static int32_t get_offset_of_Slider_11() { return static_cast<int32_t>(offsetof(ProfilerMemoryBlock_t2730562703, ___Slider_11)); }
	inline Slider_t297367283 * get_Slider_11() const { return ___Slider_11; }
	inline Slider_t297367283 ** get_address_of_Slider_11() { return &___Slider_11; }
	inline void set_Slider_11(Slider_t297367283 * value)
	{
		___Slider_11 = value;
		Il2CppCodeGenWriteBarrier(&___Slider_11, value);
	}

	inline static int32_t get_offset_of_TotalAllocatedText_12() { return static_cast<int32_t>(offsetof(ProfilerMemoryBlock_t2730562703, ___TotalAllocatedText_12)); }
	inline Text_t356221433 * get_TotalAllocatedText_12() const { return ___TotalAllocatedText_12; }
	inline Text_t356221433 ** get_address_of_TotalAllocatedText_12() { return &___TotalAllocatedText_12; }
	inline void set_TotalAllocatedText_12(Text_t356221433 * value)
	{
		___TotalAllocatedText_12 = value;
		Il2CppCodeGenWriteBarrier(&___TotalAllocatedText_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
