﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// SRDebugger.UI.Controls.ConsoleLogControl
struct ConsoleLogControl_t2638108020;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.ConsoleTabController
struct  ConsoleTabController_t999068710  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.Canvas SRDebugger.UI.Tabs.ConsoleTabController::_consoleCanvas
	Canvas_t209405766 * ____consoleCanvas_10;
	// System.Boolean SRDebugger.UI.Tabs.ConsoleTabController::_isDirty
	bool ____isDirty_11;
	// SRDebugger.UI.Controls.ConsoleLogControl SRDebugger.UI.Tabs.ConsoleTabController::ConsoleLogControl
	ConsoleLogControl_t2638108020 * ___ConsoleLogControl_12;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.ConsoleTabController::PinToggle
	Toggle_t3976754468 * ___PinToggle_13;
	// UnityEngine.UI.ScrollRect SRDebugger.UI.Tabs.ConsoleTabController::StackTraceScrollRect
	ScrollRect_t1199013257 * ___StackTraceScrollRect_14;
	// UnityEngine.UI.Text SRDebugger.UI.Tabs.ConsoleTabController::StackTraceText
	Text_t356221433 * ___StackTraceText_15;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.ConsoleTabController::ToggleErrors
	Toggle_t3976754468 * ___ToggleErrors_16;
	// UnityEngine.UI.Text SRDebugger.UI.Tabs.ConsoleTabController::ToggleErrorsText
	Text_t356221433 * ___ToggleErrorsText_17;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.ConsoleTabController::ToggleInfo
	Toggle_t3976754468 * ___ToggleInfo_18;
	// UnityEngine.UI.Text SRDebugger.UI.Tabs.ConsoleTabController::ToggleInfoText
	Text_t356221433 * ___ToggleInfoText_19;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.ConsoleTabController::ToggleWarnings
	Toggle_t3976754468 * ___ToggleWarnings_20;
	// UnityEngine.UI.Text SRDebugger.UI.Tabs.ConsoleTabController::ToggleWarningsText
	Text_t356221433 * ___ToggleWarningsText_21;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.ConsoleTabController::FilterToggle
	Toggle_t3976754468 * ___FilterToggle_22;
	// UnityEngine.UI.InputField SRDebugger.UI.Tabs.ConsoleTabController::FilterField
	InputField_t1631627530 * ___FilterField_23;
	// UnityEngine.GameObject SRDebugger.UI.Tabs.ConsoleTabController::FilterBarContainer
	GameObject_t1756533147 * ___FilterBarContainer_24;

public:
	inline static int32_t get_offset_of__consoleCanvas_10() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ____consoleCanvas_10)); }
	inline Canvas_t209405766 * get__consoleCanvas_10() const { return ____consoleCanvas_10; }
	inline Canvas_t209405766 ** get_address_of__consoleCanvas_10() { return &____consoleCanvas_10; }
	inline void set__consoleCanvas_10(Canvas_t209405766 * value)
	{
		____consoleCanvas_10 = value;
		Il2CppCodeGenWriteBarrier(&____consoleCanvas_10, value);
	}

	inline static int32_t get_offset_of__isDirty_11() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ____isDirty_11)); }
	inline bool get__isDirty_11() const { return ____isDirty_11; }
	inline bool* get_address_of__isDirty_11() { return &____isDirty_11; }
	inline void set__isDirty_11(bool value)
	{
		____isDirty_11 = value;
	}

	inline static int32_t get_offset_of_ConsoleLogControl_12() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ConsoleLogControl_12)); }
	inline ConsoleLogControl_t2638108020 * get_ConsoleLogControl_12() const { return ___ConsoleLogControl_12; }
	inline ConsoleLogControl_t2638108020 ** get_address_of_ConsoleLogControl_12() { return &___ConsoleLogControl_12; }
	inline void set_ConsoleLogControl_12(ConsoleLogControl_t2638108020 * value)
	{
		___ConsoleLogControl_12 = value;
		Il2CppCodeGenWriteBarrier(&___ConsoleLogControl_12, value);
	}

	inline static int32_t get_offset_of_PinToggle_13() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___PinToggle_13)); }
	inline Toggle_t3976754468 * get_PinToggle_13() const { return ___PinToggle_13; }
	inline Toggle_t3976754468 ** get_address_of_PinToggle_13() { return &___PinToggle_13; }
	inline void set_PinToggle_13(Toggle_t3976754468 * value)
	{
		___PinToggle_13 = value;
		Il2CppCodeGenWriteBarrier(&___PinToggle_13, value);
	}

	inline static int32_t get_offset_of_StackTraceScrollRect_14() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___StackTraceScrollRect_14)); }
	inline ScrollRect_t1199013257 * get_StackTraceScrollRect_14() const { return ___StackTraceScrollRect_14; }
	inline ScrollRect_t1199013257 ** get_address_of_StackTraceScrollRect_14() { return &___StackTraceScrollRect_14; }
	inline void set_StackTraceScrollRect_14(ScrollRect_t1199013257 * value)
	{
		___StackTraceScrollRect_14 = value;
		Il2CppCodeGenWriteBarrier(&___StackTraceScrollRect_14, value);
	}

	inline static int32_t get_offset_of_StackTraceText_15() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___StackTraceText_15)); }
	inline Text_t356221433 * get_StackTraceText_15() const { return ___StackTraceText_15; }
	inline Text_t356221433 ** get_address_of_StackTraceText_15() { return &___StackTraceText_15; }
	inline void set_StackTraceText_15(Text_t356221433 * value)
	{
		___StackTraceText_15 = value;
		Il2CppCodeGenWriteBarrier(&___StackTraceText_15, value);
	}

	inline static int32_t get_offset_of_ToggleErrors_16() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ToggleErrors_16)); }
	inline Toggle_t3976754468 * get_ToggleErrors_16() const { return ___ToggleErrors_16; }
	inline Toggle_t3976754468 ** get_address_of_ToggleErrors_16() { return &___ToggleErrors_16; }
	inline void set_ToggleErrors_16(Toggle_t3976754468 * value)
	{
		___ToggleErrors_16 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleErrors_16, value);
	}

	inline static int32_t get_offset_of_ToggleErrorsText_17() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ToggleErrorsText_17)); }
	inline Text_t356221433 * get_ToggleErrorsText_17() const { return ___ToggleErrorsText_17; }
	inline Text_t356221433 ** get_address_of_ToggleErrorsText_17() { return &___ToggleErrorsText_17; }
	inline void set_ToggleErrorsText_17(Text_t356221433 * value)
	{
		___ToggleErrorsText_17 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleErrorsText_17, value);
	}

	inline static int32_t get_offset_of_ToggleInfo_18() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ToggleInfo_18)); }
	inline Toggle_t3976754468 * get_ToggleInfo_18() const { return ___ToggleInfo_18; }
	inline Toggle_t3976754468 ** get_address_of_ToggleInfo_18() { return &___ToggleInfo_18; }
	inline void set_ToggleInfo_18(Toggle_t3976754468 * value)
	{
		___ToggleInfo_18 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleInfo_18, value);
	}

	inline static int32_t get_offset_of_ToggleInfoText_19() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ToggleInfoText_19)); }
	inline Text_t356221433 * get_ToggleInfoText_19() const { return ___ToggleInfoText_19; }
	inline Text_t356221433 ** get_address_of_ToggleInfoText_19() { return &___ToggleInfoText_19; }
	inline void set_ToggleInfoText_19(Text_t356221433 * value)
	{
		___ToggleInfoText_19 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleInfoText_19, value);
	}

	inline static int32_t get_offset_of_ToggleWarnings_20() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ToggleWarnings_20)); }
	inline Toggle_t3976754468 * get_ToggleWarnings_20() const { return ___ToggleWarnings_20; }
	inline Toggle_t3976754468 ** get_address_of_ToggleWarnings_20() { return &___ToggleWarnings_20; }
	inline void set_ToggleWarnings_20(Toggle_t3976754468 * value)
	{
		___ToggleWarnings_20 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleWarnings_20, value);
	}

	inline static int32_t get_offset_of_ToggleWarningsText_21() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___ToggleWarningsText_21)); }
	inline Text_t356221433 * get_ToggleWarningsText_21() const { return ___ToggleWarningsText_21; }
	inline Text_t356221433 ** get_address_of_ToggleWarningsText_21() { return &___ToggleWarningsText_21; }
	inline void set_ToggleWarningsText_21(Text_t356221433 * value)
	{
		___ToggleWarningsText_21 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleWarningsText_21, value);
	}

	inline static int32_t get_offset_of_FilterToggle_22() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___FilterToggle_22)); }
	inline Toggle_t3976754468 * get_FilterToggle_22() const { return ___FilterToggle_22; }
	inline Toggle_t3976754468 ** get_address_of_FilterToggle_22() { return &___FilterToggle_22; }
	inline void set_FilterToggle_22(Toggle_t3976754468 * value)
	{
		___FilterToggle_22 = value;
		Il2CppCodeGenWriteBarrier(&___FilterToggle_22, value);
	}

	inline static int32_t get_offset_of_FilterField_23() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___FilterField_23)); }
	inline InputField_t1631627530 * get_FilterField_23() const { return ___FilterField_23; }
	inline InputField_t1631627530 ** get_address_of_FilterField_23() { return &___FilterField_23; }
	inline void set_FilterField_23(InputField_t1631627530 * value)
	{
		___FilterField_23 = value;
		Il2CppCodeGenWriteBarrier(&___FilterField_23, value);
	}

	inline static int32_t get_offset_of_FilterBarContainer_24() { return static_cast<int32_t>(offsetof(ConsoleTabController_t999068710, ___FilterBarContainer_24)); }
	inline GameObject_t1756533147 * get_FilterBarContainer_24() const { return ___FilterBarContainer_24; }
	inline GameObject_t1756533147 ** get_address_of_FilterBarContainer_24() { return &___FilterBarContainer_24; }
	inline void set_FilterBarContainer_24(GameObject_t1756533147 * value)
	{
		___FilterBarContainer_24 = value;
		Il2CppCodeGenWriteBarrier(&___FilterBarContainer_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
