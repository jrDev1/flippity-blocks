﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetSwipeAngle
struct  EasyTouchGetSwipeAngle_t1559143374  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetSwipeAngle::swipeAngle
	FsmFloat_t937133978 * ___swipeAngle_11;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchGetSwipeAngle::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_12;

public:
	inline static int32_t get_offset_of_swipeAngle_11() { return static_cast<int32_t>(offsetof(EasyTouchGetSwipeAngle_t1559143374, ___swipeAngle_11)); }
	inline FsmFloat_t937133978 * get_swipeAngle_11() const { return ___swipeAngle_11; }
	inline FsmFloat_t937133978 ** get_address_of_swipeAngle_11() { return &___swipeAngle_11; }
	inline void set_swipeAngle_11(FsmFloat_t937133978 * value)
	{
		___swipeAngle_11 = value;
		Il2CppCodeGenWriteBarrier(&___swipeAngle_11, value);
	}

	inline static int32_t get_offset_of_proxy_12() { return static_cast<int32_t>(offsetof(EasyTouchGetSwipeAngle_t1559143374, ___proxy_12)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_12() const { return ___proxy_12; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_12() { return &___proxy_12; }
	inline void set_proxy_12(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_12 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
