﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Components.SRSingleton`1<System.Object>
struct  SRSingleton_1_t4173139059  : public SRMonoBehaviour_t2352136145
{
public:

public:
};

struct SRSingleton_1_t4173139059_StaticFields
{
public:
	// T SRF.Components.SRSingleton`1::_instance
	Il2CppObject * ____instance_8;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(SRSingleton_1_t4173139059_StaticFields, ____instance_8)); }
	inline Il2CppObject * get__instance_8() const { return ____instance_8; }
	inline Il2CppObject ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(Il2CppObject * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier(&____instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
