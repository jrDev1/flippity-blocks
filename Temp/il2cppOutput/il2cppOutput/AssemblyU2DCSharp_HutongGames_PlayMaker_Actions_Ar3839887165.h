﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListCopyTo
struct  ArrayListCopyTo_t3839887165  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListCopyTo::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListCopyTo::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListCopyTo::gameObjectTarget
	FsmOwnerDefault_t2023674184 * ___gameObjectTarget_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListCopyTo::referenceTarget
	FsmString_t2414474701 * ___referenceTarget_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListCopyTo::startIndex
	FsmInt_t1273009179 * ___startIndex_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListCopyTo::count
	FsmInt_t1273009179 * ___count_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListCopyTo_t3839887165, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListCopyTo_t3839887165, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_gameObjectTarget_14() { return static_cast<int32_t>(offsetof(ArrayListCopyTo_t3839887165, ___gameObjectTarget_14)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObjectTarget_14() const { return ___gameObjectTarget_14; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObjectTarget_14() { return &___gameObjectTarget_14; }
	inline void set_gameObjectTarget_14(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObjectTarget_14 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectTarget_14, value);
	}

	inline static int32_t get_offset_of_referenceTarget_15() { return static_cast<int32_t>(offsetof(ArrayListCopyTo_t3839887165, ___referenceTarget_15)); }
	inline FsmString_t2414474701 * get_referenceTarget_15() const { return ___referenceTarget_15; }
	inline FsmString_t2414474701 ** get_address_of_referenceTarget_15() { return &___referenceTarget_15; }
	inline void set_referenceTarget_15(FsmString_t2414474701 * value)
	{
		___referenceTarget_15 = value;
		Il2CppCodeGenWriteBarrier(&___referenceTarget_15, value);
	}

	inline static int32_t get_offset_of_startIndex_16() { return static_cast<int32_t>(offsetof(ArrayListCopyTo_t3839887165, ___startIndex_16)); }
	inline FsmInt_t1273009179 * get_startIndex_16() const { return ___startIndex_16; }
	inline FsmInt_t1273009179 ** get_address_of_startIndex_16() { return &___startIndex_16; }
	inline void set_startIndex_16(FsmInt_t1273009179 * value)
	{
		___startIndex_16 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_16, value);
	}

	inline static int32_t get_offset_of_count_17() { return static_cast<int32_t>(offsetof(ArrayListCopyTo_t3839887165, ___count_17)); }
	inline FsmInt_t1273009179 * get_count_17() const { return ___count_17; }
	inline FsmInt_t1273009179 ** get_address_of_count_17() { return &___count_17; }
	inline void set_count_17(FsmInt_t1273009179 * value)
	{
		___count_17 = value;
		Il2CppCodeGenWriteBarrier(&___count_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
