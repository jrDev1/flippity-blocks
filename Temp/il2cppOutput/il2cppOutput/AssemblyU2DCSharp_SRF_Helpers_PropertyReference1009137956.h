﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Helpers.PropertyReference
struct  PropertyReference_t1009137956  : public Il2CppObject
{
public:
	// System.Reflection.PropertyInfo SRF.Helpers.PropertyReference::_property
	PropertyInfo_t * ____property_0;
	// System.Object SRF.Helpers.PropertyReference::_target
	Il2CppObject * ____target_1;

public:
	inline static int32_t get_offset_of__property_0() { return static_cast<int32_t>(offsetof(PropertyReference_t1009137956, ____property_0)); }
	inline PropertyInfo_t * get__property_0() const { return ____property_0; }
	inline PropertyInfo_t ** get_address_of__property_0() { return &____property_0; }
	inline void set__property_0(PropertyInfo_t * value)
	{
		____property_0 = value;
		Il2CppCodeGenWriteBarrier(&____property_0, value);
	}

	inline static int32_t get_offset_of__target_1() { return static_cast<int32_t>(offsetof(PropertyReference_t1009137956, ____target_1)); }
	inline Il2CppObject * get__target_1() const { return ____target_1; }
	inline Il2CppObject ** get_address_of__target_1() { return &____target_1; }
	inline void set__target_1(Il2CppObject * value)
	{
		____target_1 = value;
		Il2CppCodeGenWriteBarrier(&____target_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
