﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListIsEmpty
struct  ArrayListIsEmpty_t4274965434  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListIsEmpty::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListIsEmpty::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ArrayListIsEmpty::isEmpty
	FsmBool_t664485696 * ___isEmpty_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListIsEmpty::isEmptyEvent
	FsmEvent_t1258573736 * ___isEmptyEvent_15;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListIsEmpty::isNotEmptyEvent
	FsmEvent_t1258573736 * ___isNotEmptyEvent_16;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListIsEmpty_t4274965434, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListIsEmpty_t4274965434, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_isEmpty_14() { return static_cast<int32_t>(offsetof(ArrayListIsEmpty_t4274965434, ___isEmpty_14)); }
	inline FsmBool_t664485696 * get_isEmpty_14() const { return ___isEmpty_14; }
	inline FsmBool_t664485696 ** get_address_of_isEmpty_14() { return &___isEmpty_14; }
	inline void set_isEmpty_14(FsmBool_t664485696 * value)
	{
		___isEmpty_14 = value;
		Il2CppCodeGenWriteBarrier(&___isEmpty_14, value);
	}

	inline static int32_t get_offset_of_isEmptyEvent_15() { return static_cast<int32_t>(offsetof(ArrayListIsEmpty_t4274965434, ___isEmptyEvent_15)); }
	inline FsmEvent_t1258573736 * get_isEmptyEvent_15() const { return ___isEmptyEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_isEmptyEvent_15() { return &___isEmptyEvent_15; }
	inline void set_isEmptyEvent_15(FsmEvent_t1258573736 * value)
	{
		___isEmptyEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___isEmptyEvent_15, value);
	}

	inline static int32_t get_offset_of_isNotEmptyEvent_16() { return static_cast<int32_t>(offsetof(ArrayListIsEmpty_t4274965434, ___isNotEmptyEvent_16)); }
	inline FsmEvent_t1258573736 * get_isNotEmptyEvent_16() const { return ___isNotEmptyEvent_16; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotEmptyEvent_16() { return &___isNotEmptyEvent_16; }
	inline void set_isNotEmptyEvent_16(FsmEvent_t1258573736 * value)
	{
		___isNotEmptyEvent_16 = value;
		Il2CppCodeGenWriteBarrier(&___isNotEmptyEvent_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
