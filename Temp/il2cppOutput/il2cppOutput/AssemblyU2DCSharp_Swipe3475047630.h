﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Swipe
struct  Swipe_t3475047630  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Swipe::trail
	GameObject_t1756533147 * ___trail_2;
	// UnityEngine.UI.Text Swipe::swipeText
	Text_t356221433 * ___swipeText_3;

public:
	inline static int32_t get_offset_of_trail_2() { return static_cast<int32_t>(offsetof(Swipe_t3475047630, ___trail_2)); }
	inline GameObject_t1756533147 * get_trail_2() const { return ___trail_2; }
	inline GameObject_t1756533147 ** get_address_of_trail_2() { return &___trail_2; }
	inline void set_trail_2(GameObject_t1756533147 * value)
	{
		___trail_2 = value;
		Il2CppCodeGenWriteBarrier(&___trail_2, value);
	}

	inline static int32_t get_offset_of_swipeText_3() { return static_cast<int32_t>(offsetof(Swipe_t3475047630, ___swipeText_3)); }
	inline Text_t356221433 * get_swipeText_3() const { return ___swipeText_3; }
	inline Text_t356221433 ** get_address_of_swipeText_3() { return &___swipeText_3; }
	inline void set_swipeText_3(Text_t356221433 * value)
	{
		___swipeText_3 = value;
		Il2CppCodeGenWriteBarrier(&___swipeText_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
