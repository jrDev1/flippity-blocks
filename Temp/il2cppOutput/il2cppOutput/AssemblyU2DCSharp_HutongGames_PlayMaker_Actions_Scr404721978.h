﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ScreenSetResolution
struct  ScreenSetResolution_t404721978  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.ScreenSetResolution::fullScreen
	FsmBool_t664485696 * ___fullScreen_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ScreenSetResolution::width
	FsmInt_t1273009179 * ___width_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ScreenSetResolution::height
	FsmInt_t1273009179 * ___height_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ScreenSetResolution::preferedRefreshRate
	FsmInt_t1273009179 * ___preferedRefreshRate_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.ScreenSetResolution::orResolution
	FsmVector3_t3996534004 * ___orResolution_15;

public:
	inline static int32_t get_offset_of_fullScreen_11() { return static_cast<int32_t>(offsetof(ScreenSetResolution_t404721978, ___fullScreen_11)); }
	inline FsmBool_t664485696 * get_fullScreen_11() const { return ___fullScreen_11; }
	inline FsmBool_t664485696 ** get_address_of_fullScreen_11() { return &___fullScreen_11; }
	inline void set_fullScreen_11(FsmBool_t664485696 * value)
	{
		___fullScreen_11 = value;
		Il2CppCodeGenWriteBarrier(&___fullScreen_11, value);
	}

	inline static int32_t get_offset_of_width_12() { return static_cast<int32_t>(offsetof(ScreenSetResolution_t404721978, ___width_12)); }
	inline FsmInt_t1273009179 * get_width_12() const { return ___width_12; }
	inline FsmInt_t1273009179 ** get_address_of_width_12() { return &___width_12; }
	inline void set_width_12(FsmInt_t1273009179 * value)
	{
		___width_12 = value;
		Il2CppCodeGenWriteBarrier(&___width_12, value);
	}

	inline static int32_t get_offset_of_height_13() { return static_cast<int32_t>(offsetof(ScreenSetResolution_t404721978, ___height_13)); }
	inline FsmInt_t1273009179 * get_height_13() const { return ___height_13; }
	inline FsmInt_t1273009179 ** get_address_of_height_13() { return &___height_13; }
	inline void set_height_13(FsmInt_t1273009179 * value)
	{
		___height_13 = value;
		Il2CppCodeGenWriteBarrier(&___height_13, value);
	}

	inline static int32_t get_offset_of_preferedRefreshRate_14() { return static_cast<int32_t>(offsetof(ScreenSetResolution_t404721978, ___preferedRefreshRate_14)); }
	inline FsmInt_t1273009179 * get_preferedRefreshRate_14() const { return ___preferedRefreshRate_14; }
	inline FsmInt_t1273009179 ** get_address_of_preferedRefreshRate_14() { return &___preferedRefreshRate_14; }
	inline void set_preferedRefreshRate_14(FsmInt_t1273009179 * value)
	{
		___preferedRefreshRate_14 = value;
		Il2CppCodeGenWriteBarrier(&___preferedRefreshRate_14, value);
	}

	inline static int32_t get_offset_of_orResolution_15() { return static_cast<int32_t>(offsetof(ScreenSetResolution_t404721978, ___orResolution_15)); }
	inline FsmVector3_t3996534004 * get_orResolution_15() const { return ___orResolution_15; }
	inline FsmVector3_t3996534004 ** get_address_of_orResolution_15() { return &___orResolution_15; }
	inline void set_orResolution_15(FsmVector3_t3996534004 * value)
	{
		___orResolution_15 = value;
		Il2CppCodeGenWriteBarrier(&___orResolution_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
