﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct PlayMakerEvent_t1571596806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiPointerEventsProxy
struct  PlayMakerUGuiPointerEventsProxy_t2224244788  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerUGuiPointerEventsProxy::debug
	bool ___debug_2;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget PlayMakerUGuiPointerEventsProxy::eventTarget
	PlayMakerEventTarget_t2104288969 * ___eventTarget_3;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiPointerEventsProxy::onClickEvent
	PlayMakerEvent_t1571596806 * ___onClickEvent_4;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiPointerEventsProxy::onDownEvent
	PlayMakerEvent_t1571596806 * ___onDownEvent_5;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiPointerEventsProxy::onEnterEvent
	PlayMakerEvent_t1571596806 * ___onEnterEvent_6;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiPointerEventsProxy::onExitEvent
	PlayMakerEvent_t1571596806 * ___onExitEvent_7;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiPointerEventsProxy::onUpEvent
	PlayMakerEvent_t1571596806 * ___onUpEvent_8;

public:
	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_eventTarget_3() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___eventTarget_3)); }
	inline PlayMakerEventTarget_t2104288969 * get_eventTarget_3() const { return ___eventTarget_3; }
	inline PlayMakerEventTarget_t2104288969 ** get_address_of_eventTarget_3() { return &___eventTarget_3; }
	inline void set_eventTarget_3(PlayMakerEventTarget_t2104288969 * value)
	{
		___eventTarget_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_3, value);
	}

	inline static int32_t get_offset_of_onClickEvent_4() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___onClickEvent_4)); }
	inline PlayMakerEvent_t1571596806 * get_onClickEvent_4() const { return ___onClickEvent_4; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onClickEvent_4() { return &___onClickEvent_4; }
	inline void set_onClickEvent_4(PlayMakerEvent_t1571596806 * value)
	{
		___onClickEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___onClickEvent_4, value);
	}

	inline static int32_t get_offset_of_onDownEvent_5() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___onDownEvent_5)); }
	inline PlayMakerEvent_t1571596806 * get_onDownEvent_5() const { return ___onDownEvent_5; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onDownEvent_5() { return &___onDownEvent_5; }
	inline void set_onDownEvent_5(PlayMakerEvent_t1571596806 * value)
	{
		___onDownEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___onDownEvent_5, value);
	}

	inline static int32_t get_offset_of_onEnterEvent_6() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___onEnterEvent_6)); }
	inline PlayMakerEvent_t1571596806 * get_onEnterEvent_6() const { return ___onEnterEvent_6; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onEnterEvent_6() { return &___onEnterEvent_6; }
	inline void set_onEnterEvent_6(PlayMakerEvent_t1571596806 * value)
	{
		___onEnterEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___onEnterEvent_6, value);
	}

	inline static int32_t get_offset_of_onExitEvent_7() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___onExitEvent_7)); }
	inline PlayMakerEvent_t1571596806 * get_onExitEvent_7() const { return ___onExitEvent_7; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onExitEvent_7() { return &___onExitEvent_7; }
	inline void set_onExitEvent_7(PlayMakerEvent_t1571596806 * value)
	{
		___onExitEvent_7 = value;
		Il2CppCodeGenWriteBarrier(&___onExitEvent_7, value);
	}

	inline static int32_t get_offset_of_onUpEvent_8() { return static_cast<int32_t>(offsetof(PlayMakerUGuiPointerEventsProxy_t2224244788, ___onUpEvent_8)); }
	inline PlayMakerEvent_t1571596806 * get_onUpEvent_8() const { return ___onUpEvent_8; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onUpEvent_8() { return &___onUpEvent_8; }
	inline void set_onUpEvent_8(PlayMakerEvent_t1571596806 * value)
	{
		___onUpEvent_8 = value;
		Il2CppCodeGenWriteBarrier(&___onUpEvent_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
