﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIDrag
struct  UIDrag_t854096796  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 UIDrag::fingerId
	int32_t ___fingerId_2;
	// System.Boolean UIDrag::drag
	bool ___drag_3;

public:
	inline static int32_t get_offset_of_fingerId_2() { return static_cast<int32_t>(offsetof(UIDrag_t854096796, ___fingerId_2)); }
	inline int32_t get_fingerId_2() const { return ___fingerId_2; }
	inline int32_t* get_address_of_fingerId_2() { return &___fingerId_2; }
	inline void set_fingerId_2(int32_t value)
	{
		___fingerId_2 = value;
	}

	inline static int32_t get_offset_of_drag_3() { return static_cast<int32_t>(offsetof(UIDrag_t854096796, ___drag_3)); }
	inline bool get_drag_3() const { return ___drag_3; }
	inline bool* get_address_of_drag_3() { return &___drag_3; }
	inline void set_drag_3(bool value)
	{
		___drag_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
