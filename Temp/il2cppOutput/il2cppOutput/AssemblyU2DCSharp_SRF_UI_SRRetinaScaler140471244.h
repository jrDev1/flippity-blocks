﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.SRRetinaScaler
struct  SRRetinaScaler_t140471244  : public SRMonoBehaviour_t2352136145
{
public:
	// System.Single SRF.UI.SRRetinaScaler::_retinaScale
	float ____retinaScale_8;
	// System.Int32 SRF.UI.SRRetinaScaler::_thresholdDpi
	int32_t ____thresholdDpi_9;
	// System.Boolean SRF.UI.SRRetinaScaler::_disablePixelPerfect
	bool ____disablePixelPerfect_10;

public:
	inline static int32_t get_offset_of__retinaScale_8() { return static_cast<int32_t>(offsetof(SRRetinaScaler_t140471244, ____retinaScale_8)); }
	inline float get__retinaScale_8() const { return ____retinaScale_8; }
	inline float* get_address_of__retinaScale_8() { return &____retinaScale_8; }
	inline void set__retinaScale_8(float value)
	{
		____retinaScale_8 = value;
	}

	inline static int32_t get_offset_of__thresholdDpi_9() { return static_cast<int32_t>(offsetof(SRRetinaScaler_t140471244, ____thresholdDpi_9)); }
	inline int32_t get__thresholdDpi_9() const { return ____thresholdDpi_9; }
	inline int32_t* get_address_of__thresholdDpi_9() { return &____thresholdDpi_9; }
	inline void set__thresholdDpi_9(int32_t value)
	{
		____thresholdDpi_9 = value;
	}

	inline static int32_t get_offset_of__disablePixelPerfect_10() { return static_cast<int32_t>(offsetof(SRRetinaScaler_t140471244, ____disablePixelPerfect_10)); }
	inline bool get__disablePixelPerfect_10() const { return ____disablePixelPerfect_10; }
	inline bool* get_address_of__disablePixelPerfect_10() { return &____disablePixelPerfect_10; }
	inline void set__disablePixelPerfect_10(bool value)
	{
		____disablePixelPerfect_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
