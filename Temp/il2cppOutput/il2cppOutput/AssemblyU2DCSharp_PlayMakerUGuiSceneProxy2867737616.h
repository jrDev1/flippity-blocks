﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PlayMakerFSM
struct PlayMakerFSM_t437737208;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiSceneProxy
struct  PlayMakerUGuiSceneProxy_t2867737616  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct PlayMakerUGuiSceneProxy_t2867737616_StaticFields
{
public:
	// PlayMakerFSM PlayMakerUGuiSceneProxy::fsm
	PlayMakerFSM_t437737208 * ___fsm_2;

public:
	inline static int32_t get_offset_of_fsm_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiSceneProxy_t2867737616_StaticFields, ___fsm_2)); }
	inline PlayMakerFSM_t437737208 * get_fsm_2() const { return ___fsm_2; }
	inline PlayMakerFSM_t437737208 ** get_address_of_fsm_2() { return &___fsm_2; }
	inline void set_fsm_2(PlayMakerFSM_t437737208 * value)
	{
		___fsm_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
