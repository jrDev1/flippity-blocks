﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneCountInBuildSettings
struct  GetSceneCountInBuildSettings_t4187098583  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneCountInBuildSettings::sceneCountInBuildSettings
	FsmInt_t1273009179 * ___sceneCountInBuildSettings_11;

public:
	inline static int32_t get_offset_of_sceneCountInBuildSettings_11() { return static_cast<int32_t>(offsetof(GetSceneCountInBuildSettings_t4187098583, ___sceneCountInBuildSettings_11)); }
	inline FsmInt_t1273009179 * get_sceneCountInBuildSettings_11() const { return ___sceneCountInBuildSettings_11; }
	inline FsmInt_t1273009179 ** get_address_of_sceneCountInBuildSettings_11() { return &___sceneCountInBuildSettings_11; }
	inline void set_sceneCountInBuildSettings_11(FsmInt_t1273009179 * value)
	{
		___sceneCountInBuildSettings_11 = value;
		Il2CppCodeGenWriteBarrier(&___sceneCountInBuildSettings_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
