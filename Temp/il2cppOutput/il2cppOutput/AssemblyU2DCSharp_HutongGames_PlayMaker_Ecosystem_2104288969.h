﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_1036036659.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct  PlayMakerEventTarget_t2104288969  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.ProxyEventTarget HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::eventTarget
	int32_t ___eventTarget_0;
	// UnityEngine.GameObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::includeChildren
	bool ___includeChildren_2;
	// PlayMakerFSM HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget::fsmComponent
	PlayMakerFSM_t437737208 * ___fsmComponent_3;

public:
	inline static int32_t get_offset_of_eventTarget_0() { return static_cast<int32_t>(offsetof(PlayMakerEventTarget_t2104288969, ___eventTarget_0)); }
	inline int32_t get_eventTarget_0() const { return ___eventTarget_0; }
	inline int32_t* get_address_of_eventTarget_0() { return &___eventTarget_0; }
	inline void set_eventTarget_0(int32_t value)
	{
		___eventTarget_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(PlayMakerEventTarget_t2104288969, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}

	inline static int32_t get_offset_of_includeChildren_2() { return static_cast<int32_t>(offsetof(PlayMakerEventTarget_t2104288969, ___includeChildren_2)); }
	inline bool get_includeChildren_2() const { return ___includeChildren_2; }
	inline bool* get_address_of_includeChildren_2() { return &___includeChildren_2; }
	inline void set_includeChildren_2(bool value)
	{
		___includeChildren_2 = value;
	}

	inline static int32_t get_offset_of_fsmComponent_3() { return static_cast<int32_t>(offsetof(PlayMakerEventTarget_t2104288969, ___fsmComponent_3)); }
	inline PlayMakerFSM_t437737208 * get_fsmComponent_3() const { return ___fsmComponent_3; }
	inline PlayMakerFSM_t437737208 ** get_address_of_fsmComponent_3() { return &___fsmComponent_3; }
	inline void set_fsmComponent_3(PlayMakerFSM_t437737208 * value)
	{
		___fsmComponent_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsmComponent_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
