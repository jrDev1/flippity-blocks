﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TouchPadUIEvent
struct  TouchPadUIEvent_t997246418  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text TouchPadUIEvent::touchDownText
	Text_t356221433 * ___touchDownText_2;
	// UnityEngine.UI.Text TouchPadUIEvent::touchText
	Text_t356221433 * ___touchText_3;
	// UnityEngine.UI.Text TouchPadUIEvent::touchUpText
	Text_t356221433 * ___touchUpText_4;

public:
	inline static int32_t get_offset_of_touchDownText_2() { return static_cast<int32_t>(offsetof(TouchPadUIEvent_t997246418, ___touchDownText_2)); }
	inline Text_t356221433 * get_touchDownText_2() const { return ___touchDownText_2; }
	inline Text_t356221433 ** get_address_of_touchDownText_2() { return &___touchDownText_2; }
	inline void set_touchDownText_2(Text_t356221433 * value)
	{
		___touchDownText_2 = value;
		Il2CppCodeGenWriteBarrier(&___touchDownText_2, value);
	}

	inline static int32_t get_offset_of_touchText_3() { return static_cast<int32_t>(offsetof(TouchPadUIEvent_t997246418, ___touchText_3)); }
	inline Text_t356221433 * get_touchText_3() const { return ___touchText_3; }
	inline Text_t356221433 ** get_address_of_touchText_3() { return &___touchText_3; }
	inline void set_touchText_3(Text_t356221433 * value)
	{
		___touchText_3 = value;
		Il2CppCodeGenWriteBarrier(&___touchText_3, value);
	}

	inline static int32_t get_offset_of_touchUpText_4() { return static_cast<int32_t>(offsetof(TouchPadUIEvent_t997246418, ___touchUpText_4)); }
	inline Text_t356221433 * get_touchUpText_4() const { return ___touchUpText_4; }
	inline Text_t356221433 ** get_address_of_touchUpText_4() { return &___touchUpText_4; }
	inline void set_touchUpText_4(Text_t356221433 * value)
	{
		___touchUpText_4 = value;
		Il2CppCodeGenWriteBarrier(&___touchUpText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
