﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SROptions
struct SROptions_t4170704091;
// SROptionsPropertyChanged
struct SROptionsPropertyChanged_t1005294736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SROptions
struct  SROptions_t4170704091  : public Il2CppObject
{
public:
	// SROptionsPropertyChanged SROptions::PropertyChanged
	SROptionsPropertyChanged_t1005294736 * ___PropertyChanged_1;

public:
	inline static int32_t get_offset_of_PropertyChanged_1() { return static_cast<int32_t>(offsetof(SROptions_t4170704091, ___PropertyChanged_1)); }
	inline SROptionsPropertyChanged_t1005294736 * get_PropertyChanged_1() const { return ___PropertyChanged_1; }
	inline SROptionsPropertyChanged_t1005294736 ** get_address_of_PropertyChanged_1() { return &___PropertyChanged_1; }
	inline void set_PropertyChanged_1(SROptionsPropertyChanged_t1005294736 * value)
	{
		___PropertyChanged_1 = value;
		Il2CppCodeGenWriteBarrier(&___PropertyChanged_1, value);
	}
};

struct SROptions_t4170704091_StaticFields
{
public:
	// SROptions SROptions::_current
	SROptions_t4170704091 * ____current_0;

public:
	inline static int32_t get_offset_of__current_0() { return static_cast<int32_t>(offsetof(SROptions_t4170704091_StaticFields, ____current_0)); }
	inline SROptions_t4170704091 * get__current_0() const { return ____current_0; }
	inline SROptions_t4170704091 ** get_address_of__current_0() { return &____current_0; }
	inline void set__current_0(SROptions_t4170704091 * value)
	{
		____current_0 = value;
		Il2CppCodeGenWriteBarrier(&____current_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
