﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_316495900.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchSetGesturePriority
struct  EasyTouchSetGesturePriority_t3078528686  : public FsmStateAction_t2862378169
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/GesturePriority HutongGames.PlayMaker.Actions.EasyTouchSetGesturePriority::priority
	int32_t ___priority_11;

public:
	inline static int32_t get_offset_of_priority_11() { return static_cast<int32_t>(offsetof(EasyTouchSetGesturePriority_t3078528686, ___priority_11)); }
	inline int32_t get_priority_11() const { return ___priority_11; }
	inline int32_t* get_address_of_priority_11() { return &___priority_11; }
	inline void set_priority_11(int32_t value)
	{
		___priority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
