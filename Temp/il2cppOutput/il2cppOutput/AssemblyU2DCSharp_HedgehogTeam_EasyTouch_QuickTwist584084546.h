﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickTwis2978027614.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickTwist930913507.h"

// HedgehogTeam.EasyTouch.QuickTwist/OnTwistAction
struct OnTwistAction_t3811439132;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTwist
struct  QuickTwist_t584084546  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickTwist/OnTwistAction HedgehogTeam.EasyTouch.QuickTwist::onTwistAction
	OnTwistAction_t3811439132 * ___onTwistAction_19;
	// System.Boolean HedgehogTeam.EasyTouch.QuickTwist::isGestureOnMe
	bool ___isGestureOnMe_20;
	// HedgehogTeam.EasyTouch.QuickTwist/ActionTiggering HedgehogTeam.EasyTouch.QuickTwist::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.QuickTwist/ActionRotationDirection HedgehogTeam.EasyTouch.QuickTwist::rotationDirection
	int32_t ___rotationDirection_22;
	// System.Single HedgehogTeam.EasyTouch.QuickTwist::axisActionValue
	float ___axisActionValue_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickTwist::enableSimpleAction
	bool ___enableSimpleAction_24;

public:
	inline static int32_t get_offset_of_onTwistAction_19() { return static_cast<int32_t>(offsetof(QuickTwist_t584084546, ___onTwistAction_19)); }
	inline OnTwistAction_t3811439132 * get_onTwistAction_19() const { return ___onTwistAction_19; }
	inline OnTwistAction_t3811439132 ** get_address_of_onTwistAction_19() { return &___onTwistAction_19; }
	inline void set_onTwistAction_19(OnTwistAction_t3811439132 * value)
	{
		___onTwistAction_19 = value;
		Il2CppCodeGenWriteBarrier(&___onTwistAction_19, value);
	}

	inline static int32_t get_offset_of_isGestureOnMe_20() { return static_cast<int32_t>(offsetof(QuickTwist_t584084546, ___isGestureOnMe_20)); }
	inline bool get_isGestureOnMe_20() const { return ___isGestureOnMe_20; }
	inline bool* get_address_of_isGestureOnMe_20() { return &___isGestureOnMe_20; }
	inline void set_isGestureOnMe_20(bool value)
	{
		___isGestureOnMe_20 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickTwist_t584084546, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_rotationDirection_22() { return static_cast<int32_t>(offsetof(QuickTwist_t584084546, ___rotationDirection_22)); }
	inline int32_t get_rotationDirection_22() const { return ___rotationDirection_22; }
	inline int32_t* get_address_of_rotationDirection_22() { return &___rotationDirection_22; }
	inline void set_rotationDirection_22(int32_t value)
	{
		___rotationDirection_22 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_23() { return static_cast<int32_t>(offsetof(QuickTwist_t584084546, ___axisActionValue_23)); }
	inline float get_axisActionValue_23() const { return ___axisActionValue_23; }
	inline float* get_address_of_axisActionValue_23() { return &___axisActionValue_23; }
	inline void set_axisActionValue_23(float value)
	{
		___axisActionValue_23 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_24() { return static_cast<int32_t>(offsetof(QuickTwist_t584084546, ___enableSimpleAction_24)); }
	inline bool get_enableSimpleAction_24() const { return ___enableSimpleAction_24; }
	inline bool* get_address_of_enableSimpleAction_24() { return &___enableSimpleAction_24; }
	inline void set_enableSimpleAction_24(bool value)
	{
		___enableSimpleAction_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
