﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetEnable2DCollider
struct  EasyTouchGetEnable2DCollider_t773411822  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EasyTouchGetEnable2DCollider::enabled
	FsmBool_t664485696 * ___enabled_11;

public:
	inline static int32_t get_offset_of_enabled_11() { return static_cast<int32_t>(offsetof(EasyTouchGetEnable2DCollider_t773411822, ___enabled_11)); }
	inline FsmBool_t664485696 * get_enabled_11() const { return ___enabled_11; }
	inline FsmBool_t664485696 ** get_address_of_enabled_11() { return &___enabled_11; }
	inline void set_enabled_11(FsmBool_t664485696 * value)
	{
		___enabled_11 = value;
		Il2CppCodeGenWriteBarrier(&___enabled_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
