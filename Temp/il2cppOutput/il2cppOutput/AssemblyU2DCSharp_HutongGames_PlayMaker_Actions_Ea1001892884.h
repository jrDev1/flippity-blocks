﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedUIElement
struct  EasyTouchGetCurrentPickedUIElement_t1001892884  : public FsmStateAction_t2862378169
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedUIElement::isTwoFinger
	bool ___isTwoFinger_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedUIElement::currentUIElement
	FsmGameObject_t3097142863 * ___currentUIElement_12;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedUIElement::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_13;

public:
	inline static int32_t get_offset_of_isTwoFinger_11() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedUIElement_t1001892884, ___isTwoFinger_11)); }
	inline bool get_isTwoFinger_11() const { return ___isTwoFinger_11; }
	inline bool* get_address_of_isTwoFinger_11() { return &___isTwoFinger_11; }
	inline void set_isTwoFinger_11(bool value)
	{
		___isTwoFinger_11 = value;
	}

	inline static int32_t get_offset_of_currentUIElement_12() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedUIElement_t1001892884, ___currentUIElement_12)); }
	inline FsmGameObject_t3097142863 * get_currentUIElement_12() const { return ___currentUIElement_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_currentUIElement_12() { return &___currentUIElement_12; }
	inline void set_currentUIElement_12(FsmGameObject_t3097142863 * value)
	{
		___currentUIElement_12 = value;
		Il2CppCodeGenWriteBarrier(&___currentUIElement_12, value);
	}

	inline static int32_t get_offset_of_proxy_13() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedUIElement_t1001892884, ___proxy_13)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_13() const { return ___proxy_13; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_13() { return &___proxy_13; }
	inline void set_proxy_13(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_13 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
