﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_DataBound3384854171.h"

// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Array
struct Il2CppArray;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// SRF.UI.SRSpinner
struct SRSpinner_t2158684882;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.EnumControl
struct  EnumControl_t2277533208  : public DataBoundControl_t3384854171
{
public:
	// System.Object SRDebugger.UI.Controls.Data.EnumControl::_lastValue
	Il2CppObject * ____lastValue_17;
	// System.String[] SRDebugger.UI.Controls.Data.EnumControl::_names
	StringU5BU5D_t1642385972* ____names_18;
	// System.Array SRDebugger.UI.Controls.Data.EnumControl::_values
	Il2CppArray * ____values_19;
	// UnityEngine.UI.LayoutElement SRDebugger.UI.Controls.Data.EnumControl::ContentLayoutElement
	LayoutElement_t2808691390 * ___ContentLayoutElement_20;
	// UnityEngine.GameObject[] SRDebugger.UI.Controls.Data.EnumControl::DisableOnReadOnly
	GameObjectU5BU5D_t3057952154* ___DisableOnReadOnly_21;
	// SRF.UI.SRSpinner SRDebugger.UI.Controls.Data.EnumControl::Spinner
	SRSpinner_t2158684882 * ___Spinner_22;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.EnumControl::Title
	Text_t356221433 * ___Title_23;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.EnumControl::Value
	Text_t356221433 * ___Value_24;

public:
	inline static int32_t get_offset_of__lastValue_17() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ____lastValue_17)); }
	inline Il2CppObject * get__lastValue_17() const { return ____lastValue_17; }
	inline Il2CppObject ** get_address_of__lastValue_17() { return &____lastValue_17; }
	inline void set__lastValue_17(Il2CppObject * value)
	{
		____lastValue_17 = value;
		Il2CppCodeGenWriteBarrier(&____lastValue_17, value);
	}

	inline static int32_t get_offset_of__names_18() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ____names_18)); }
	inline StringU5BU5D_t1642385972* get__names_18() const { return ____names_18; }
	inline StringU5BU5D_t1642385972** get_address_of__names_18() { return &____names_18; }
	inline void set__names_18(StringU5BU5D_t1642385972* value)
	{
		____names_18 = value;
		Il2CppCodeGenWriteBarrier(&____names_18, value);
	}

	inline static int32_t get_offset_of__values_19() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ____values_19)); }
	inline Il2CppArray * get__values_19() const { return ____values_19; }
	inline Il2CppArray ** get_address_of__values_19() { return &____values_19; }
	inline void set__values_19(Il2CppArray * value)
	{
		____values_19 = value;
		Il2CppCodeGenWriteBarrier(&____values_19, value);
	}

	inline static int32_t get_offset_of_ContentLayoutElement_20() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ___ContentLayoutElement_20)); }
	inline LayoutElement_t2808691390 * get_ContentLayoutElement_20() const { return ___ContentLayoutElement_20; }
	inline LayoutElement_t2808691390 ** get_address_of_ContentLayoutElement_20() { return &___ContentLayoutElement_20; }
	inline void set_ContentLayoutElement_20(LayoutElement_t2808691390 * value)
	{
		___ContentLayoutElement_20 = value;
		Il2CppCodeGenWriteBarrier(&___ContentLayoutElement_20, value);
	}

	inline static int32_t get_offset_of_DisableOnReadOnly_21() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ___DisableOnReadOnly_21)); }
	inline GameObjectU5BU5D_t3057952154* get_DisableOnReadOnly_21() const { return ___DisableOnReadOnly_21; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_DisableOnReadOnly_21() { return &___DisableOnReadOnly_21; }
	inline void set_DisableOnReadOnly_21(GameObjectU5BU5D_t3057952154* value)
	{
		___DisableOnReadOnly_21 = value;
		Il2CppCodeGenWriteBarrier(&___DisableOnReadOnly_21, value);
	}

	inline static int32_t get_offset_of_Spinner_22() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ___Spinner_22)); }
	inline SRSpinner_t2158684882 * get_Spinner_22() const { return ___Spinner_22; }
	inline SRSpinner_t2158684882 ** get_address_of_Spinner_22() { return &___Spinner_22; }
	inline void set_Spinner_22(SRSpinner_t2158684882 * value)
	{
		___Spinner_22 = value;
		Il2CppCodeGenWriteBarrier(&___Spinner_22, value);
	}

	inline static int32_t get_offset_of_Title_23() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ___Title_23)); }
	inline Text_t356221433 * get_Title_23() const { return ___Title_23; }
	inline Text_t356221433 ** get_address_of_Title_23() { return &___Title_23; }
	inline void set_Title_23(Text_t356221433 * value)
	{
		___Title_23 = value;
		Il2CppCodeGenWriteBarrier(&___Title_23, value);
	}

	inline static int32_t get_offset_of_Value_24() { return static_cast<int32_t>(offsetof(EnumControl_t2277533208, ___Value_24)); }
	inline Text_t356221433 * get_Value_24() const { return ___Value_24; }
	inline Text_t356221433 ** get_address_of_Value_24() { return &___Value_24; }
	inline void set_Value_24(Text_t356221433 * value)
	{
		___Value_24 = value;
		Il2CppCodeGenWriteBarrier(&___Value_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
