﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// SRF.UI.SRText
struct SRText_t1495418442;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ContentFitText
struct  ContentFitText_t3144722753  : public UIBehaviour_t3960014691
{
public:
	// SRF.UI.SRText SRF.UI.ContentFitText::CopySource
	SRText_t1495418442 * ___CopySource_2;
	// UnityEngine.Vector2 SRF.UI.ContentFitText::Padding
	Vector2_t2243707579  ___Padding_3;

public:
	inline static int32_t get_offset_of_CopySource_2() { return static_cast<int32_t>(offsetof(ContentFitText_t3144722753, ___CopySource_2)); }
	inline SRText_t1495418442 * get_CopySource_2() const { return ___CopySource_2; }
	inline SRText_t1495418442 ** get_address_of_CopySource_2() { return &___CopySource_2; }
	inline void set_CopySource_2(SRText_t1495418442 * value)
	{
		___CopySource_2 = value;
		Il2CppCodeGenWriteBarrier(&___CopySource_2, value);
	}

	inline static int32_t get_offset_of_Padding_3() { return static_cast<int32_t>(offsetof(ContentFitText_t3144722753, ___Padding_3)); }
	inline Vector2_t2243707579  get_Padding_3() const { return ___Padding_3; }
	inline Vector2_t2243707579 * get_address_of_Padding_3() { return &___Padding_3; }
	inline void set_Padding_3(Vector2_t2243707579  value)
	{
		___Padding_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
