﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// SRF.SRList`1<SRDebugger.UI.Other.SRTab>
struct SRList_1_t1966669819;
// SRDebugger.UI.Other.SRTab
struct SRTab_t1974039238;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRDebugger.UI.Controls.SRTabButton
struct SRTabButton_t2557777178;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Action`2<SRDebugger.UI.Other.SRTabController,SRDebugger.UI.Other.SRTab>
struct Action_2_t1708790689;
// System.Comparison`1<SRDebugger.UI.Other.SRTab>
struct Comparison_1_t3235778089;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.SRTabController
struct  SRTabController_t1363238198  : public SRMonoBehaviourEx_t3120278648
{
public:
	// SRF.SRList`1<SRDebugger.UI.Other.SRTab> SRDebugger.UI.Other.SRTabController::_tabs
	SRList_1_t1966669819 * ____tabs_9;
	// SRDebugger.UI.Other.SRTab SRDebugger.UI.Other.SRTabController::_activeTab
	SRTab_t1974039238 * ____activeTab_10;
	// UnityEngine.RectTransform SRDebugger.UI.Other.SRTabController::TabButtonContainer
	RectTransform_t3349966182 * ___TabButtonContainer_11;
	// SRDebugger.UI.Controls.SRTabButton SRDebugger.UI.Other.SRTabController::TabButtonPrefab
	SRTabButton_t2557777178 * ___TabButtonPrefab_12;
	// UnityEngine.RectTransform SRDebugger.UI.Other.SRTabController::TabContentsContainer
	RectTransform_t3349966182 * ___TabContentsContainer_13;
	// UnityEngine.RectTransform SRDebugger.UI.Other.SRTabController::TabHeaderContentContainer
	RectTransform_t3349966182 * ___TabHeaderContentContainer_14;
	// UnityEngine.UI.Text SRDebugger.UI.Other.SRTabController::TabHeaderText
	Text_t356221433 * ___TabHeaderText_15;
	// System.Action`2<SRDebugger.UI.Other.SRTabController,SRDebugger.UI.Other.SRTab> SRDebugger.UI.Other.SRTabController::ActiveTabChanged
	Action_2_t1708790689 * ___ActiveTabChanged_16;

public:
	inline static int32_t get_offset_of__tabs_9() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ____tabs_9)); }
	inline SRList_1_t1966669819 * get__tabs_9() const { return ____tabs_9; }
	inline SRList_1_t1966669819 ** get_address_of__tabs_9() { return &____tabs_9; }
	inline void set__tabs_9(SRList_1_t1966669819 * value)
	{
		____tabs_9 = value;
		Il2CppCodeGenWriteBarrier(&____tabs_9, value);
	}

	inline static int32_t get_offset_of__activeTab_10() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ____activeTab_10)); }
	inline SRTab_t1974039238 * get__activeTab_10() const { return ____activeTab_10; }
	inline SRTab_t1974039238 ** get_address_of__activeTab_10() { return &____activeTab_10; }
	inline void set__activeTab_10(SRTab_t1974039238 * value)
	{
		____activeTab_10 = value;
		Il2CppCodeGenWriteBarrier(&____activeTab_10, value);
	}

	inline static int32_t get_offset_of_TabButtonContainer_11() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ___TabButtonContainer_11)); }
	inline RectTransform_t3349966182 * get_TabButtonContainer_11() const { return ___TabButtonContainer_11; }
	inline RectTransform_t3349966182 ** get_address_of_TabButtonContainer_11() { return &___TabButtonContainer_11; }
	inline void set_TabButtonContainer_11(RectTransform_t3349966182 * value)
	{
		___TabButtonContainer_11 = value;
		Il2CppCodeGenWriteBarrier(&___TabButtonContainer_11, value);
	}

	inline static int32_t get_offset_of_TabButtonPrefab_12() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ___TabButtonPrefab_12)); }
	inline SRTabButton_t2557777178 * get_TabButtonPrefab_12() const { return ___TabButtonPrefab_12; }
	inline SRTabButton_t2557777178 ** get_address_of_TabButtonPrefab_12() { return &___TabButtonPrefab_12; }
	inline void set_TabButtonPrefab_12(SRTabButton_t2557777178 * value)
	{
		___TabButtonPrefab_12 = value;
		Il2CppCodeGenWriteBarrier(&___TabButtonPrefab_12, value);
	}

	inline static int32_t get_offset_of_TabContentsContainer_13() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ___TabContentsContainer_13)); }
	inline RectTransform_t3349966182 * get_TabContentsContainer_13() const { return ___TabContentsContainer_13; }
	inline RectTransform_t3349966182 ** get_address_of_TabContentsContainer_13() { return &___TabContentsContainer_13; }
	inline void set_TabContentsContainer_13(RectTransform_t3349966182 * value)
	{
		___TabContentsContainer_13 = value;
		Il2CppCodeGenWriteBarrier(&___TabContentsContainer_13, value);
	}

	inline static int32_t get_offset_of_TabHeaderContentContainer_14() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ___TabHeaderContentContainer_14)); }
	inline RectTransform_t3349966182 * get_TabHeaderContentContainer_14() const { return ___TabHeaderContentContainer_14; }
	inline RectTransform_t3349966182 ** get_address_of_TabHeaderContentContainer_14() { return &___TabHeaderContentContainer_14; }
	inline void set_TabHeaderContentContainer_14(RectTransform_t3349966182 * value)
	{
		___TabHeaderContentContainer_14 = value;
		Il2CppCodeGenWriteBarrier(&___TabHeaderContentContainer_14, value);
	}

	inline static int32_t get_offset_of_TabHeaderText_15() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ___TabHeaderText_15)); }
	inline Text_t356221433 * get_TabHeaderText_15() const { return ___TabHeaderText_15; }
	inline Text_t356221433 ** get_address_of_TabHeaderText_15() { return &___TabHeaderText_15; }
	inline void set_TabHeaderText_15(Text_t356221433 * value)
	{
		___TabHeaderText_15 = value;
		Il2CppCodeGenWriteBarrier(&___TabHeaderText_15, value);
	}

	inline static int32_t get_offset_of_ActiveTabChanged_16() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198, ___ActiveTabChanged_16)); }
	inline Action_2_t1708790689 * get_ActiveTabChanged_16() const { return ___ActiveTabChanged_16; }
	inline Action_2_t1708790689 ** get_address_of_ActiveTabChanged_16() { return &___ActiveTabChanged_16; }
	inline void set_ActiveTabChanged_16(Action_2_t1708790689 * value)
	{
		___ActiveTabChanged_16 = value;
		Il2CppCodeGenWriteBarrier(&___ActiveTabChanged_16, value);
	}
};

struct SRTabController_t1363238198_StaticFields
{
public:
	// System.Comparison`1<SRDebugger.UI.Other.SRTab> SRDebugger.UI.Other.SRTabController::<>f__am$cache0
	Comparison_1_t3235778089 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(SRTabController_t1363238198_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Comparison_1_t3235778089 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Comparison_1_t3235778089 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Comparison_1_t3235778089 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
