﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ResponsiveResize/Element/SizeDefinition
struct  SizeDefinition_t1466074441 
{
public:
	// System.Single SRF.UI.ResponsiveResize/Element/SizeDefinition::ElementWidth
	float ___ElementWidth_0;
	// System.Single SRF.UI.ResponsiveResize/Element/SizeDefinition::ThresholdWidth
	float ___ThresholdWidth_1;

public:
	inline static int32_t get_offset_of_ElementWidth_0() { return static_cast<int32_t>(offsetof(SizeDefinition_t1466074441, ___ElementWidth_0)); }
	inline float get_ElementWidth_0() const { return ___ElementWidth_0; }
	inline float* get_address_of_ElementWidth_0() { return &___ElementWidth_0; }
	inline void set_ElementWidth_0(float value)
	{
		___ElementWidth_0 = value;
	}

	inline static int32_t get_offset_of_ThresholdWidth_1() { return static_cast<int32_t>(offsetof(SizeDefinition_t1466074441, ___ThresholdWidth_1)); }
	inline float get_ThresholdWidth_1() const { return ___ThresholdWidth_1; }
	inline float* get_address_of_ThresholdWidth_1() { return &___ThresholdWidth_1; }
	inline void set_ThresholdWidth_1(float value)
	{
		___ThresholdWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
