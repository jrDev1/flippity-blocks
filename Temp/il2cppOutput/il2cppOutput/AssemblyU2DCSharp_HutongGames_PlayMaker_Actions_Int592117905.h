﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.IntAddValue
struct  IntAddValue_t592117905  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAddValue::intVariable
	FsmInt_t1273009179 * ___intVariable_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAddValue::add
	FsmInt_t1273009179 * ___add_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.IntAddValue::intValue
	FsmInt_t1273009179 * ___intValue_13;
	// System.Boolean HutongGames.PlayMaker.Actions.IntAddValue::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_intVariable_11() { return static_cast<int32_t>(offsetof(IntAddValue_t592117905, ___intVariable_11)); }
	inline FsmInt_t1273009179 * get_intVariable_11() const { return ___intVariable_11; }
	inline FsmInt_t1273009179 ** get_address_of_intVariable_11() { return &___intVariable_11; }
	inline void set_intVariable_11(FsmInt_t1273009179 * value)
	{
		___intVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___intVariable_11, value);
	}

	inline static int32_t get_offset_of_add_12() { return static_cast<int32_t>(offsetof(IntAddValue_t592117905, ___add_12)); }
	inline FsmInt_t1273009179 * get_add_12() const { return ___add_12; }
	inline FsmInt_t1273009179 ** get_address_of_add_12() { return &___add_12; }
	inline void set_add_12(FsmInt_t1273009179 * value)
	{
		___add_12 = value;
		Il2CppCodeGenWriteBarrier(&___add_12, value);
	}

	inline static int32_t get_offset_of_intValue_13() { return static_cast<int32_t>(offsetof(IntAddValue_t592117905, ___intValue_13)); }
	inline FsmInt_t1273009179 * get_intValue_13() const { return ___intValue_13; }
	inline FsmInt_t1273009179 ** get_address_of_intValue_13() { return &___intValue_13; }
	inline void set_intValue_13(FsmInt_t1273009179 * value)
	{
		___intValue_13 = value;
		Il2CppCodeGenWriteBarrier(&___intValue_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(IntAddValue_t592117905, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
