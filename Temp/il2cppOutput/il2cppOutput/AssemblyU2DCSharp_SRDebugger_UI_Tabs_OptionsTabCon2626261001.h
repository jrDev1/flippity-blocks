﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.UI.Other.CategoryGroup
struct CategoryGroup_t4174071633;
// System.Collections.Generic.List`1<SRDebugger.UI.Controls.OptionsControlBase>
struct List_1_t2852422836;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance
struct  CategoryInstance_t2626261001  : public Il2CppObject
{
public:
	// SRDebugger.UI.Other.CategoryGroup SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance::<CategoryGroup>k__BackingField
	CategoryGroup_t4174071633 * ___U3CCategoryGroupU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<SRDebugger.UI.Controls.OptionsControlBase> SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance::Options
	List_1_t2852422836 * ___Options_1;

public:
	inline static int32_t get_offset_of_U3CCategoryGroupU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CategoryInstance_t2626261001, ___U3CCategoryGroupU3Ek__BackingField_0)); }
	inline CategoryGroup_t4174071633 * get_U3CCategoryGroupU3Ek__BackingField_0() const { return ___U3CCategoryGroupU3Ek__BackingField_0; }
	inline CategoryGroup_t4174071633 ** get_address_of_U3CCategoryGroupU3Ek__BackingField_0() { return &___U3CCategoryGroupU3Ek__BackingField_0; }
	inline void set_U3CCategoryGroupU3Ek__BackingField_0(CategoryGroup_t4174071633 * value)
	{
		___U3CCategoryGroupU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCategoryGroupU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_Options_1() { return static_cast<int32_t>(offsetof(CategoryInstance_t2626261001, ___Options_1)); }
	inline List_1_t2852422836 * get_Options_1() const { return ___Options_1; }
	inline List_1_t2852422836 ** get_address_of_Options_1() { return &___Options_1; }
	inline void set_Options_1(List_1_t2852422836 * value)
	{
		___Options_1 = value;
		Il2CppCodeGenWriteBarrier(&___Options_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
