﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen2068033852.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRDependencyServiceBase`1<System.Object>
struct  SRDependencyServiceBase_1_t1215971031  : public SRServiceBase_1_t2068033852
{
public:
	// System.Boolean SRF.Service.SRDependencyServiceBase`1::_isLoaded
	bool ____isLoaded_9;

public:
	inline static int32_t get_offset_of__isLoaded_9() { return static_cast<int32_t>(offsetof(SRDependencyServiceBase_1_t1215971031, ____isLoaded_9)); }
	inline bool get__isLoaded_9() const { return ____isLoaded_9; }
	inline bool* get_address_of__isLoaded_9() { return &____isLoaded_9; }
	inline void set__isLoaded_9(bool value)
	{
		____isLoaded_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
