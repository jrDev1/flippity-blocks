﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// HedgehogTeam.EasyTouch.QuickDrag/OnDragStart
struct OnDragStart_t3841907510;
// HedgehogTeam.EasyTouch.QuickDrag/OnDrag
struct OnDrag_t1578171758;
// HedgehogTeam.EasyTouch.QuickDrag/OnDragEnd
struct OnDragEnd_t2570412943;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickDrag
struct  QuickDrag_t2207902219  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickDrag/OnDragStart HedgehogTeam.EasyTouch.QuickDrag::onDragStart
	OnDragStart_t3841907510 * ___onDragStart_19;
	// HedgehogTeam.EasyTouch.QuickDrag/OnDrag HedgehogTeam.EasyTouch.QuickDrag::onDrag
	OnDrag_t1578171758 * ___onDrag_20;
	// HedgehogTeam.EasyTouch.QuickDrag/OnDragEnd HedgehogTeam.EasyTouch.QuickDrag::onDragEnd
	OnDragEnd_t2570412943 * ___onDragEnd_21;
	// System.Boolean HedgehogTeam.EasyTouch.QuickDrag::isStopOncollisionEnter
	bool ___isStopOncollisionEnter_22;
	// UnityEngine.Vector3 HedgehogTeam.EasyTouch.QuickDrag::deltaPosition
	Vector3_t2243707580  ___deltaPosition_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickDrag::isOnDrag
	bool ___isOnDrag_24;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickDrag::lastGesture
	Gesture_t367995397 * ___lastGesture_25;

public:
	inline static int32_t get_offset_of_onDragStart_19() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___onDragStart_19)); }
	inline OnDragStart_t3841907510 * get_onDragStart_19() const { return ___onDragStart_19; }
	inline OnDragStart_t3841907510 ** get_address_of_onDragStart_19() { return &___onDragStart_19; }
	inline void set_onDragStart_19(OnDragStart_t3841907510 * value)
	{
		___onDragStart_19 = value;
		Il2CppCodeGenWriteBarrier(&___onDragStart_19, value);
	}

	inline static int32_t get_offset_of_onDrag_20() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___onDrag_20)); }
	inline OnDrag_t1578171758 * get_onDrag_20() const { return ___onDrag_20; }
	inline OnDrag_t1578171758 ** get_address_of_onDrag_20() { return &___onDrag_20; }
	inline void set_onDrag_20(OnDrag_t1578171758 * value)
	{
		___onDrag_20 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_20, value);
	}

	inline static int32_t get_offset_of_onDragEnd_21() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___onDragEnd_21)); }
	inline OnDragEnd_t2570412943 * get_onDragEnd_21() const { return ___onDragEnd_21; }
	inline OnDragEnd_t2570412943 ** get_address_of_onDragEnd_21() { return &___onDragEnd_21; }
	inline void set_onDragEnd_21(OnDragEnd_t2570412943 * value)
	{
		___onDragEnd_21 = value;
		Il2CppCodeGenWriteBarrier(&___onDragEnd_21, value);
	}

	inline static int32_t get_offset_of_isStopOncollisionEnter_22() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___isStopOncollisionEnter_22)); }
	inline bool get_isStopOncollisionEnter_22() const { return ___isStopOncollisionEnter_22; }
	inline bool* get_address_of_isStopOncollisionEnter_22() { return &___isStopOncollisionEnter_22; }
	inline void set_isStopOncollisionEnter_22(bool value)
	{
		___isStopOncollisionEnter_22 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_23() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___deltaPosition_23)); }
	inline Vector3_t2243707580  get_deltaPosition_23() const { return ___deltaPosition_23; }
	inline Vector3_t2243707580 * get_address_of_deltaPosition_23() { return &___deltaPosition_23; }
	inline void set_deltaPosition_23(Vector3_t2243707580  value)
	{
		___deltaPosition_23 = value;
	}

	inline static int32_t get_offset_of_isOnDrag_24() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___isOnDrag_24)); }
	inline bool get_isOnDrag_24() const { return ___isOnDrag_24; }
	inline bool* get_address_of_isOnDrag_24() { return &___isOnDrag_24; }
	inline void set_isOnDrag_24(bool value)
	{
		___isOnDrag_24 = value;
	}

	inline static int32_t get_offset_of_lastGesture_25() { return static_cast<int32_t>(offsetof(QuickDrag_t2207902219, ___lastGesture_25)); }
	inline Gesture_t367995397 * get_lastGesture_25() const { return ___lastGesture_25; }
	inline Gesture_t367995397 ** get_address_of_lastGesture_25() { return &___lastGesture_25; }
	inline void set_lastGesture_25(Gesture_t367995397 * value)
	{
		___lastGesture_25 = value;
		Il2CppCodeGenWriteBarrier(&___lastGesture_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
