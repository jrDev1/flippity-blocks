﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"
#include "AssemblyU2DCSharp_SRDebugger_PinAlignment3550534810.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.HandleManager
struct  HandleManager_t305543651  : public SRMonoBehaviour_t2352136145
{
public:
	// System.Boolean SRDebugger.UI.Other.HandleManager::_hasSet
	bool ____hasSet_8;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::BottomHandle
	GameObject_t1756533147 * ___BottomHandle_9;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::BottomLeftHandle
	GameObject_t1756533147 * ___BottomLeftHandle_10;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::BottomRightHandle
	GameObject_t1756533147 * ___BottomRightHandle_11;
	// SRDebugger.PinAlignment SRDebugger.UI.Other.HandleManager::DefaultAlignment
	int32_t ___DefaultAlignment_12;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::LeftHandle
	GameObject_t1756533147 * ___LeftHandle_13;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::RightHandle
	GameObject_t1756533147 * ___RightHandle_14;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::TopHandle
	GameObject_t1756533147 * ___TopHandle_15;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::TopLeftHandle
	GameObject_t1756533147 * ___TopLeftHandle_16;
	// UnityEngine.GameObject SRDebugger.UI.Other.HandleManager::TopRightHandle
	GameObject_t1756533147 * ___TopRightHandle_17;

public:
	inline static int32_t get_offset_of__hasSet_8() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ____hasSet_8)); }
	inline bool get__hasSet_8() const { return ____hasSet_8; }
	inline bool* get_address_of__hasSet_8() { return &____hasSet_8; }
	inline void set__hasSet_8(bool value)
	{
		____hasSet_8 = value;
	}

	inline static int32_t get_offset_of_BottomHandle_9() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___BottomHandle_9)); }
	inline GameObject_t1756533147 * get_BottomHandle_9() const { return ___BottomHandle_9; }
	inline GameObject_t1756533147 ** get_address_of_BottomHandle_9() { return &___BottomHandle_9; }
	inline void set_BottomHandle_9(GameObject_t1756533147 * value)
	{
		___BottomHandle_9 = value;
		Il2CppCodeGenWriteBarrier(&___BottomHandle_9, value);
	}

	inline static int32_t get_offset_of_BottomLeftHandle_10() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___BottomLeftHandle_10)); }
	inline GameObject_t1756533147 * get_BottomLeftHandle_10() const { return ___BottomLeftHandle_10; }
	inline GameObject_t1756533147 ** get_address_of_BottomLeftHandle_10() { return &___BottomLeftHandle_10; }
	inline void set_BottomLeftHandle_10(GameObject_t1756533147 * value)
	{
		___BottomLeftHandle_10 = value;
		Il2CppCodeGenWriteBarrier(&___BottomLeftHandle_10, value);
	}

	inline static int32_t get_offset_of_BottomRightHandle_11() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___BottomRightHandle_11)); }
	inline GameObject_t1756533147 * get_BottomRightHandle_11() const { return ___BottomRightHandle_11; }
	inline GameObject_t1756533147 ** get_address_of_BottomRightHandle_11() { return &___BottomRightHandle_11; }
	inline void set_BottomRightHandle_11(GameObject_t1756533147 * value)
	{
		___BottomRightHandle_11 = value;
		Il2CppCodeGenWriteBarrier(&___BottomRightHandle_11, value);
	}

	inline static int32_t get_offset_of_DefaultAlignment_12() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___DefaultAlignment_12)); }
	inline int32_t get_DefaultAlignment_12() const { return ___DefaultAlignment_12; }
	inline int32_t* get_address_of_DefaultAlignment_12() { return &___DefaultAlignment_12; }
	inline void set_DefaultAlignment_12(int32_t value)
	{
		___DefaultAlignment_12 = value;
	}

	inline static int32_t get_offset_of_LeftHandle_13() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___LeftHandle_13)); }
	inline GameObject_t1756533147 * get_LeftHandle_13() const { return ___LeftHandle_13; }
	inline GameObject_t1756533147 ** get_address_of_LeftHandle_13() { return &___LeftHandle_13; }
	inline void set_LeftHandle_13(GameObject_t1756533147 * value)
	{
		___LeftHandle_13 = value;
		Il2CppCodeGenWriteBarrier(&___LeftHandle_13, value);
	}

	inline static int32_t get_offset_of_RightHandle_14() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___RightHandle_14)); }
	inline GameObject_t1756533147 * get_RightHandle_14() const { return ___RightHandle_14; }
	inline GameObject_t1756533147 ** get_address_of_RightHandle_14() { return &___RightHandle_14; }
	inline void set_RightHandle_14(GameObject_t1756533147 * value)
	{
		___RightHandle_14 = value;
		Il2CppCodeGenWriteBarrier(&___RightHandle_14, value);
	}

	inline static int32_t get_offset_of_TopHandle_15() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___TopHandle_15)); }
	inline GameObject_t1756533147 * get_TopHandle_15() const { return ___TopHandle_15; }
	inline GameObject_t1756533147 ** get_address_of_TopHandle_15() { return &___TopHandle_15; }
	inline void set_TopHandle_15(GameObject_t1756533147 * value)
	{
		___TopHandle_15 = value;
		Il2CppCodeGenWriteBarrier(&___TopHandle_15, value);
	}

	inline static int32_t get_offset_of_TopLeftHandle_16() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___TopLeftHandle_16)); }
	inline GameObject_t1756533147 * get_TopLeftHandle_16() const { return ___TopLeftHandle_16; }
	inline GameObject_t1756533147 ** get_address_of_TopLeftHandle_16() { return &___TopLeftHandle_16; }
	inline void set_TopLeftHandle_16(GameObject_t1756533147 * value)
	{
		___TopLeftHandle_16 = value;
		Il2CppCodeGenWriteBarrier(&___TopLeftHandle_16, value);
	}

	inline static int32_t get_offset_of_TopRightHandle_17() { return static_cast<int32_t>(offsetof(HandleManager_t305543651, ___TopRightHandle_17)); }
	inline GameObject_t1756533147 * get_TopRightHandle_17() const { return ___TopRightHandle_17; }
	inline GameObject_t1756533147 ** get_address_of_TopRightHandle_17() { return &___TopRightHandle_17; }
	inline void set_TopRightHandle_17(GameObject_t1756533147 * value)
	{
		___TopRightHandle_17 = value;
		Il2CppCodeGenWriteBarrier(&___TopRightHandle_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
