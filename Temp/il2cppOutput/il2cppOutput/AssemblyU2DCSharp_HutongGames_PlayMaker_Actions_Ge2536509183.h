﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData
struct  GetSceneActivateChangedEventData_t2536509183  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newName
	FsmString_t2414474701 * ___newName_11;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newPath
	FsmString_t2414474701 * ___newPath_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newIsValid
	FsmBool_t664485696 * ___newIsValid_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newBuildIndex
	FsmInt_t1273009179 * ___newBuildIndex_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newIsLoaded
	FsmBool_t664485696 * ___newIsLoaded_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newIsDirty
	FsmBool_t664485696 * ___newIsDirty_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newRootCount
	FsmInt_t1273009179 * ___newRootCount_17;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::newRootGameObjects
	FsmArray_t527459893 * ___newRootGameObjects_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousName
	FsmString_t2414474701 * ___previousName_19;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousPath
	FsmString_t2414474701 * ___previousPath_20;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousIsValid
	FsmBool_t664485696 * ___previousIsValid_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousBuildIndex
	FsmInt_t1273009179 * ___previousBuildIndex_22;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousIsLoaded
	FsmBool_t664485696 * ___previousIsLoaded_23;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousIsDirty
	FsmBool_t664485696 * ___previousIsDirty_24;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousRootCount
	FsmInt_t1273009179 * ___previousRootCount_25;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::previousRootGameObjects
	FsmArray_t527459893 * ___previousRootGameObjects_26;
	// UnityEngine.SceneManagement.Scene HutongGames.PlayMaker.Actions.GetSceneActivateChangedEventData::_scene
	Scene_t1684909666  ____scene_27;

public:
	inline static int32_t get_offset_of_newName_11() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newName_11)); }
	inline FsmString_t2414474701 * get_newName_11() const { return ___newName_11; }
	inline FsmString_t2414474701 ** get_address_of_newName_11() { return &___newName_11; }
	inline void set_newName_11(FsmString_t2414474701 * value)
	{
		___newName_11 = value;
		Il2CppCodeGenWriteBarrier(&___newName_11, value);
	}

	inline static int32_t get_offset_of_newPath_12() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newPath_12)); }
	inline FsmString_t2414474701 * get_newPath_12() const { return ___newPath_12; }
	inline FsmString_t2414474701 ** get_address_of_newPath_12() { return &___newPath_12; }
	inline void set_newPath_12(FsmString_t2414474701 * value)
	{
		___newPath_12 = value;
		Il2CppCodeGenWriteBarrier(&___newPath_12, value);
	}

	inline static int32_t get_offset_of_newIsValid_13() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newIsValid_13)); }
	inline FsmBool_t664485696 * get_newIsValid_13() const { return ___newIsValid_13; }
	inline FsmBool_t664485696 ** get_address_of_newIsValid_13() { return &___newIsValid_13; }
	inline void set_newIsValid_13(FsmBool_t664485696 * value)
	{
		___newIsValid_13 = value;
		Il2CppCodeGenWriteBarrier(&___newIsValid_13, value);
	}

	inline static int32_t get_offset_of_newBuildIndex_14() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newBuildIndex_14)); }
	inline FsmInt_t1273009179 * get_newBuildIndex_14() const { return ___newBuildIndex_14; }
	inline FsmInt_t1273009179 ** get_address_of_newBuildIndex_14() { return &___newBuildIndex_14; }
	inline void set_newBuildIndex_14(FsmInt_t1273009179 * value)
	{
		___newBuildIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___newBuildIndex_14, value);
	}

	inline static int32_t get_offset_of_newIsLoaded_15() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newIsLoaded_15)); }
	inline FsmBool_t664485696 * get_newIsLoaded_15() const { return ___newIsLoaded_15; }
	inline FsmBool_t664485696 ** get_address_of_newIsLoaded_15() { return &___newIsLoaded_15; }
	inline void set_newIsLoaded_15(FsmBool_t664485696 * value)
	{
		___newIsLoaded_15 = value;
		Il2CppCodeGenWriteBarrier(&___newIsLoaded_15, value);
	}

	inline static int32_t get_offset_of_newIsDirty_16() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newIsDirty_16)); }
	inline FsmBool_t664485696 * get_newIsDirty_16() const { return ___newIsDirty_16; }
	inline FsmBool_t664485696 ** get_address_of_newIsDirty_16() { return &___newIsDirty_16; }
	inline void set_newIsDirty_16(FsmBool_t664485696 * value)
	{
		___newIsDirty_16 = value;
		Il2CppCodeGenWriteBarrier(&___newIsDirty_16, value);
	}

	inline static int32_t get_offset_of_newRootCount_17() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newRootCount_17)); }
	inline FsmInt_t1273009179 * get_newRootCount_17() const { return ___newRootCount_17; }
	inline FsmInt_t1273009179 ** get_address_of_newRootCount_17() { return &___newRootCount_17; }
	inline void set_newRootCount_17(FsmInt_t1273009179 * value)
	{
		___newRootCount_17 = value;
		Il2CppCodeGenWriteBarrier(&___newRootCount_17, value);
	}

	inline static int32_t get_offset_of_newRootGameObjects_18() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___newRootGameObjects_18)); }
	inline FsmArray_t527459893 * get_newRootGameObjects_18() const { return ___newRootGameObjects_18; }
	inline FsmArray_t527459893 ** get_address_of_newRootGameObjects_18() { return &___newRootGameObjects_18; }
	inline void set_newRootGameObjects_18(FsmArray_t527459893 * value)
	{
		___newRootGameObjects_18 = value;
		Il2CppCodeGenWriteBarrier(&___newRootGameObjects_18, value);
	}

	inline static int32_t get_offset_of_previousName_19() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousName_19)); }
	inline FsmString_t2414474701 * get_previousName_19() const { return ___previousName_19; }
	inline FsmString_t2414474701 ** get_address_of_previousName_19() { return &___previousName_19; }
	inline void set_previousName_19(FsmString_t2414474701 * value)
	{
		___previousName_19 = value;
		Il2CppCodeGenWriteBarrier(&___previousName_19, value);
	}

	inline static int32_t get_offset_of_previousPath_20() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousPath_20)); }
	inline FsmString_t2414474701 * get_previousPath_20() const { return ___previousPath_20; }
	inline FsmString_t2414474701 ** get_address_of_previousPath_20() { return &___previousPath_20; }
	inline void set_previousPath_20(FsmString_t2414474701 * value)
	{
		___previousPath_20 = value;
		Il2CppCodeGenWriteBarrier(&___previousPath_20, value);
	}

	inline static int32_t get_offset_of_previousIsValid_21() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousIsValid_21)); }
	inline FsmBool_t664485696 * get_previousIsValid_21() const { return ___previousIsValid_21; }
	inline FsmBool_t664485696 ** get_address_of_previousIsValid_21() { return &___previousIsValid_21; }
	inline void set_previousIsValid_21(FsmBool_t664485696 * value)
	{
		___previousIsValid_21 = value;
		Il2CppCodeGenWriteBarrier(&___previousIsValid_21, value);
	}

	inline static int32_t get_offset_of_previousBuildIndex_22() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousBuildIndex_22)); }
	inline FsmInt_t1273009179 * get_previousBuildIndex_22() const { return ___previousBuildIndex_22; }
	inline FsmInt_t1273009179 ** get_address_of_previousBuildIndex_22() { return &___previousBuildIndex_22; }
	inline void set_previousBuildIndex_22(FsmInt_t1273009179 * value)
	{
		___previousBuildIndex_22 = value;
		Il2CppCodeGenWriteBarrier(&___previousBuildIndex_22, value);
	}

	inline static int32_t get_offset_of_previousIsLoaded_23() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousIsLoaded_23)); }
	inline FsmBool_t664485696 * get_previousIsLoaded_23() const { return ___previousIsLoaded_23; }
	inline FsmBool_t664485696 ** get_address_of_previousIsLoaded_23() { return &___previousIsLoaded_23; }
	inline void set_previousIsLoaded_23(FsmBool_t664485696 * value)
	{
		___previousIsLoaded_23 = value;
		Il2CppCodeGenWriteBarrier(&___previousIsLoaded_23, value);
	}

	inline static int32_t get_offset_of_previousIsDirty_24() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousIsDirty_24)); }
	inline FsmBool_t664485696 * get_previousIsDirty_24() const { return ___previousIsDirty_24; }
	inline FsmBool_t664485696 ** get_address_of_previousIsDirty_24() { return &___previousIsDirty_24; }
	inline void set_previousIsDirty_24(FsmBool_t664485696 * value)
	{
		___previousIsDirty_24 = value;
		Il2CppCodeGenWriteBarrier(&___previousIsDirty_24, value);
	}

	inline static int32_t get_offset_of_previousRootCount_25() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousRootCount_25)); }
	inline FsmInt_t1273009179 * get_previousRootCount_25() const { return ___previousRootCount_25; }
	inline FsmInt_t1273009179 ** get_address_of_previousRootCount_25() { return &___previousRootCount_25; }
	inline void set_previousRootCount_25(FsmInt_t1273009179 * value)
	{
		___previousRootCount_25 = value;
		Il2CppCodeGenWriteBarrier(&___previousRootCount_25, value);
	}

	inline static int32_t get_offset_of_previousRootGameObjects_26() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ___previousRootGameObjects_26)); }
	inline FsmArray_t527459893 * get_previousRootGameObjects_26() const { return ___previousRootGameObjects_26; }
	inline FsmArray_t527459893 ** get_address_of_previousRootGameObjects_26() { return &___previousRootGameObjects_26; }
	inline void set_previousRootGameObjects_26(FsmArray_t527459893 * value)
	{
		___previousRootGameObjects_26 = value;
		Il2CppCodeGenWriteBarrier(&___previousRootGameObjects_26, value);
	}

	inline static int32_t get_offset_of__scene_27() { return static_cast<int32_t>(offsetof(GetSceneActivateChangedEventData_t2536509183, ____scene_27)); }
	inline Scene_t1684909666  get__scene_27() const { return ____scene_27; }
	inline Scene_t1684909666 * get_address_of__scene_27() { return &____scene_27; }
	inline void set__scene_27(Scene_t1684909666  value)
	{
		____scene_27 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
