﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.UI.Controls.PinEntryControl
struct PinEntryControl_t1988638876;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.PinEntryControl/<Awake>c__AnonStorey0
struct  U3CAwakeU3Ec__AnonStorey0_t1450473563  : public Il2CppObject
{
public:
	// System.Int32 SRDebugger.UI.Controls.PinEntryControl/<Awake>c__AnonStorey0::number
	int32_t ___number_0;
	// SRDebugger.UI.Controls.PinEntryControl SRDebugger.UI.Controls.PinEntryControl/<Awake>c__AnonStorey0::$this
	PinEntryControl_t1988638876 * ___U24this_1;

public:
	inline static int32_t get_offset_of_number_0() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t1450473563, ___number_0)); }
	inline int32_t get_number_0() const { return ___number_0; }
	inline int32_t* get_address_of_number_0() { return &___number_0; }
	inline void set_number_0(int32_t value)
	{
		___number_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAwakeU3Ec__AnonStorey0_t1450473563, ___U24this_1)); }
	inline PinEntryControl_t1988638876 * get_U24this_1() const { return ___U24this_1; }
	inline PinEntryControl_t1988638876 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PinEntryControl_t1988638876 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
