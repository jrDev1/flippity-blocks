﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sp4171926443.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Spl442439406.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.SplitTextToArrayList
struct  SplitTextToArrayList_t355753733  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.SplitTextToArrayList::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SplitTextToArrayList::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SplitTextToArrayList::startIndex
	FsmInt_t1273009179 * ___startIndex_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.SplitTextToArrayList::parseRange
	FsmInt_t1273009179 * ___parseRange_15;
	// UnityEngine.TextAsset HutongGames.PlayMaker.Actions.SplitTextToArrayList::textAsset
	TextAsset_t3973159845 * ___textAsset_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SplitTextToArrayList::OrThisString
	FsmString_t2414474701 * ___OrThisString_17;
	// HutongGames.PlayMaker.Actions.SplitTextToArrayList/SplitSpecialChars HutongGames.PlayMaker.Actions.SplitTextToArrayList::split
	int32_t ___split_18;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.SplitTextToArrayList::OrThisChar
	FsmString_t2414474701 * ___OrThisChar_19;
	// HutongGames.PlayMaker.Actions.SplitTextToArrayList/ArrayMakerParseStringAs HutongGames.PlayMaker.Actions.SplitTextToArrayList::parseAsType
	int32_t ___parseAsType_20;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_startIndex_14() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___startIndex_14)); }
	inline FsmInt_t1273009179 * get_startIndex_14() const { return ___startIndex_14; }
	inline FsmInt_t1273009179 ** get_address_of_startIndex_14() { return &___startIndex_14; }
	inline void set_startIndex_14(FsmInt_t1273009179 * value)
	{
		___startIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_14, value);
	}

	inline static int32_t get_offset_of_parseRange_15() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___parseRange_15)); }
	inline FsmInt_t1273009179 * get_parseRange_15() const { return ___parseRange_15; }
	inline FsmInt_t1273009179 ** get_address_of_parseRange_15() { return &___parseRange_15; }
	inline void set_parseRange_15(FsmInt_t1273009179 * value)
	{
		___parseRange_15 = value;
		Il2CppCodeGenWriteBarrier(&___parseRange_15, value);
	}

	inline static int32_t get_offset_of_textAsset_16() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___textAsset_16)); }
	inline TextAsset_t3973159845 * get_textAsset_16() const { return ___textAsset_16; }
	inline TextAsset_t3973159845 ** get_address_of_textAsset_16() { return &___textAsset_16; }
	inline void set_textAsset_16(TextAsset_t3973159845 * value)
	{
		___textAsset_16 = value;
		Il2CppCodeGenWriteBarrier(&___textAsset_16, value);
	}

	inline static int32_t get_offset_of_OrThisString_17() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___OrThisString_17)); }
	inline FsmString_t2414474701 * get_OrThisString_17() const { return ___OrThisString_17; }
	inline FsmString_t2414474701 ** get_address_of_OrThisString_17() { return &___OrThisString_17; }
	inline void set_OrThisString_17(FsmString_t2414474701 * value)
	{
		___OrThisString_17 = value;
		Il2CppCodeGenWriteBarrier(&___OrThisString_17, value);
	}

	inline static int32_t get_offset_of_split_18() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___split_18)); }
	inline int32_t get_split_18() const { return ___split_18; }
	inline int32_t* get_address_of_split_18() { return &___split_18; }
	inline void set_split_18(int32_t value)
	{
		___split_18 = value;
	}

	inline static int32_t get_offset_of_OrThisChar_19() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___OrThisChar_19)); }
	inline FsmString_t2414474701 * get_OrThisChar_19() const { return ___OrThisChar_19; }
	inline FsmString_t2414474701 ** get_address_of_OrThisChar_19() { return &___OrThisChar_19; }
	inline void set_OrThisChar_19(FsmString_t2414474701 * value)
	{
		___OrThisChar_19 = value;
		Il2CppCodeGenWriteBarrier(&___OrThisChar_19, value);
	}

	inline static int32_t get_offset_of_parseAsType_20() { return static_cast<int32_t>(offsetof(SplitTextToArrayList_t355753733, ___parseAsType_20)); }
	inline int32_t get_parseAsType_20() const { return ___parseAsType_20; }
	inline int32_t* get_address_of_parseAsType_20() { return &___parseAsType_20; }
	inline void set_parseAsType_20(int32_t value)
	{
		___parseAsType_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
