﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedObject
struct  EasyTouchGetCurrentPickedObject_t3431736127  : public FsmStateAction_t2862378169
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedObject::isTwoFinger
	bool ___isTwoFinger_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedObject::currentObject
	FsmGameObject_t3097142863 * ___currentObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedObject::name
	FsmString_t2414474701 * ___name_13;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchGetCurrentPickedObject::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_14;

public:
	inline static int32_t get_offset_of_isTwoFinger_11() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedObject_t3431736127, ___isTwoFinger_11)); }
	inline bool get_isTwoFinger_11() const { return ___isTwoFinger_11; }
	inline bool* get_address_of_isTwoFinger_11() { return &___isTwoFinger_11; }
	inline void set_isTwoFinger_11(bool value)
	{
		___isTwoFinger_11 = value;
	}

	inline static int32_t get_offset_of_currentObject_12() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedObject_t3431736127, ___currentObject_12)); }
	inline FsmGameObject_t3097142863 * get_currentObject_12() const { return ___currentObject_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_currentObject_12() { return &___currentObject_12; }
	inline void set_currentObject_12(FsmGameObject_t3097142863 * value)
	{
		___currentObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___currentObject_12, value);
	}

	inline static int32_t get_offset_of_name_13() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedObject_t3431736127, ___name_13)); }
	inline FsmString_t2414474701 * get_name_13() const { return ___name_13; }
	inline FsmString_t2414474701 ** get_address_of_name_13() { return &___name_13; }
	inline void set_name_13(FsmString_t2414474701 * value)
	{
		___name_13 = value;
		Il2CppCodeGenWriteBarrier(&___name_13, value);
	}

	inline static int32_t get_offset_of_proxy_14() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentPickedObject_t3431736127, ___proxy_14)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_14() const { return ___proxy_14; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_14() { return &___proxy_14; }
	inline void set_proxy_14(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_14 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
