﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCSetDirectActionTransform
struct  ETCSetDirectActionTransform_t2169385377  : public MonoBehaviour_t1158329972
{
public:
	// System.String ETCSetDirectActionTransform::axisName1
	String_t* ___axisName1_2;
	// System.String ETCSetDirectActionTransform::axisName2
	String_t* ___axisName2_3;

public:
	inline static int32_t get_offset_of_axisName1_2() { return static_cast<int32_t>(offsetof(ETCSetDirectActionTransform_t2169385377, ___axisName1_2)); }
	inline String_t* get_axisName1_2() const { return ___axisName1_2; }
	inline String_t** get_address_of_axisName1_2() { return &___axisName1_2; }
	inline void set_axisName1_2(String_t* value)
	{
		___axisName1_2 = value;
		Il2CppCodeGenWriteBarrier(&___axisName1_2, value);
	}

	inline static int32_t get_offset_of_axisName2_3() { return static_cast<int32_t>(offsetof(ETCSetDirectActionTransform_t2169385377, ___axisName2_3)); }
	inline String_t* get_axisName2_3() const { return ___axisName2_3; }
	inline String_t** get_address_of_axisName2_3() { return &___axisName2_3; }
	inline void set_axisName2_3(String_t* value)
	{
		___axisName2_3 = value;
		Il2CppCodeGenWriteBarrier(&___axisName2_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
