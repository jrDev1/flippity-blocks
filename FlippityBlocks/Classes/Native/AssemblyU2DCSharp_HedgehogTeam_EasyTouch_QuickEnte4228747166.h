﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"

// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchEnter
struct OnTouchEnter_t3414327896;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchOver
struct OnTouchOver_t2504699996;
// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchExit
struct OnTouchExit_t3593593854;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickEnterOverExist
struct  QuickEnterOverExist_t4228747166  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchEnter HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchEnter
	OnTouchEnter_t3414327896 * ___onTouchEnter_19;
	// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchOver HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchOver
	OnTouchOver_t2504699996 * ___onTouchOver_20;
	// HedgehogTeam.EasyTouch.QuickEnterOverExist/OnTouchExit HedgehogTeam.EasyTouch.QuickEnterOverExist::onTouchExit
	OnTouchExit_t3593593854 * ___onTouchExit_21;
	// System.Boolean[] HedgehogTeam.EasyTouch.QuickEnterOverExist::fingerOver
	BooleanU5BU5D_t3568034315* ___fingerOver_22;

public:
	inline static int32_t get_offset_of_onTouchEnter_19() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t4228747166, ___onTouchEnter_19)); }
	inline OnTouchEnter_t3414327896 * get_onTouchEnter_19() const { return ___onTouchEnter_19; }
	inline OnTouchEnter_t3414327896 ** get_address_of_onTouchEnter_19() { return &___onTouchEnter_19; }
	inline void set_onTouchEnter_19(OnTouchEnter_t3414327896 * value)
	{
		___onTouchEnter_19 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchEnter_19, value);
	}

	inline static int32_t get_offset_of_onTouchOver_20() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t4228747166, ___onTouchOver_20)); }
	inline OnTouchOver_t2504699996 * get_onTouchOver_20() const { return ___onTouchOver_20; }
	inline OnTouchOver_t2504699996 ** get_address_of_onTouchOver_20() { return &___onTouchOver_20; }
	inline void set_onTouchOver_20(OnTouchOver_t2504699996 * value)
	{
		___onTouchOver_20 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchOver_20, value);
	}

	inline static int32_t get_offset_of_onTouchExit_21() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t4228747166, ___onTouchExit_21)); }
	inline OnTouchExit_t3593593854 * get_onTouchExit_21() const { return ___onTouchExit_21; }
	inline OnTouchExit_t3593593854 ** get_address_of_onTouchExit_21() { return &___onTouchExit_21; }
	inline void set_onTouchExit_21(OnTouchExit_t3593593854 * value)
	{
		___onTouchExit_21 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchExit_21, value);
	}

	inline static int32_t get_offset_of_fingerOver_22() { return static_cast<int32_t>(offsetof(QuickEnterOverExist_t4228747166, ___fingerOver_22)); }
	inline BooleanU5BU5D_t3568034315* get_fingerOver_22() const { return ___fingerOver_22; }
	inline BooleanU5BU5D_t3568034315** get_address_of_fingerOver_22() { return &___fingerOver_22; }
	inline void set_fingerOver_22(BooleanU5BU5D_t3568034315* value)
	{
		___fingerOver_22 = value;
		Il2CppCodeGenWriteBarrier(&___fingerOver_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
