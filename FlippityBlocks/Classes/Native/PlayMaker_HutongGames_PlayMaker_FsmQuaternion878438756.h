﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmQuaternion
struct  FsmQuaternion_t878438756  : public NamedVariable_t3026441313
{
public:
	// UnityEngine.Quaternion HutongGames.PlayMaker.FsmQuaternion::value
	Quaternion_t4030073918  ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmQuaternion_t878438756, ___value_5)); }
	inline Quaternion_t4030073918  get_value_5() const { return ___value_5; }
	inline Quaternion_t4030073918 * get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(Quaternion_t4030073918  value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
