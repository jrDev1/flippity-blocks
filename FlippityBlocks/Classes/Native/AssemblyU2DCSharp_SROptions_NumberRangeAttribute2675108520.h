﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SROptions/NumberRangeAttribute
struct  NumberRangeAttribute_t2675108520  : public Attribute_t542643598
{
public:
	// System.Double SROptions/NumberRangeAttribute::Max
	double ___Max_0;
	// System.Double SROptions/NumberRangeAttribute::Min
	double ___Min_1;

public:
	inline static int32_t get_offset_of_Max_0() { return static_cast<int32_t>(offsetof(NumberRangeAttribute_t2675108520, ___Max_0)); }
	inline double get_Max_0() const { return ___Max_0; }
	inline double* get_address_of_Max_0() { return &___Max_0; }
	inline void set_Max_0(double value)
	{
		___Max_0 = value;
	}

	inline static int32_t get_offset_of_Min_1() { return static_cast<int32_t>(offsetof(NumberRangeAttribute_t2675108520, ___Min_1)); }
	inline double get_Min_1() const { return ___Min_1; }
	inline double* get_address_of_Min_1() { return &___Min_1; }
	inline void set_Min_1(double value)
	{
		___Min_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
