﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"

// SRF.UI.ResponsiveResize/Element/SizeDefinition[]
struct SizeDefinitionU5BU5D_t1055537012;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
struct SizeDefinition_t1466074441 ;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ResponsiveResize/Element
struct  Element_t1410679598 
{
public:
	// SRF.UI.ResponsiveResize/Element/SizeDefinition[] SRF.UI.ResponsiveResize/Element::SizeDefinitions
	SizeDefinitionU5BU5D_t1055537012* ___SizeDefinitions_0;
	// UnityEngine.RectTransform SRF.UI.ResponsiveResize/Element::Target
	RectTransform_t3349966182 * ___Target_1;

public:
	inline static int32_t get_offset_of_SizeDefinitions_0() { return static_cast<int32_t>(offsetof(Element_t1410679598, ___SizeDefinitions_0)); }
	inline SizeDefinitionU5BU5D_t1055537012* get_SizeDefinitions_0() const { return ___SizeDefinitions_0; }
	inline SizeDefinitionU5BU5D_t1055537012** get_address_of_SizeDefinitions_0() { return &___SizeDefinitions_0; }
	inline void set_SizeDefinitions_0(SizeDefinitionU5BU5D_t1055537012* value)
	{
		___SizeDefinitions_0 = value;
		Il2CppCodeGenWriteBarrier(&___SizeDefinitions_0, value);
	}

	inline static int32_t get_offset_of_Target_1() { return static_cast<int32_t>(offsetof(Element_t1410679598, ___Target_1)); }
	inline RectTransform_t3349966182 * get_Target_1() const { return ___Target_1; }
	inline RectTransform_t3349966182 ** get_address_of_Target_1() { return &___Target_1; }
	inline void set_Target_1(RectTransform_t3349966182 * value)
	{
		___Target_1 = value;
		Il2CppCodeGenWriteBarrier(&___Target_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of SRF.UI.ResponsiveResize/Element
struct Element_t1410679598_marshaled_pinvoke
{
	SizeDefinition_t1466074441 * ___SizeDefinitions_0;
	RectTransform_t3349966182 * ___Target_1;
};
// Native definition for COM marshalling of SRF.UI.ResponsiveResize/Element
struct Element_t1410679598_marshaled_com
{
	SizeDefinition_t1466074441 * ___SizeDefinitions_0;
	RectTransform_t3349966182 * ___Target_1;
};
