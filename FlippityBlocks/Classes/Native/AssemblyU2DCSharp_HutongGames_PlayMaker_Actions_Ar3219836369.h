﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetRelative
struct  ArrayListGetRelative_t3219836369  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetRelative::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetRelative::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetRelative::baseIndex
	FsmInt_t1273009179 * ___baseIndex_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetRelative::increment
	FsmInt_t1273009179 * ___increment_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayListGetRelative::result
	FsmVar_t2872592513 * ___result_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetRelative::resultIndex
	FsmInt_t1273009179 * ___resultIndex_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetRelative_t3219836369, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetRelative_t3219836369, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_baseIndex_14() { return static_cast<int32_t>(offsetof(ArrayListGetRelative_t3219836369, ___baseIndex_14)); }
	inline FsmInt_t1273009179 * get_baseIndex_14() const { return ___baseIndex_14; }
	inline FsmInt_t1273009179 ** get_address_of_baseIndex_14() { return &___baseIndex_14; }
	inline void set_baseIndex_14(FsmInt_t1273009179 * value)
	{
		___baseIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___baseIndex_14, value);
	}

	inline static int32_t get_offset_of_increment_15() { return static_cast<int32_t>(offsetof(ArrayListGetRelative_t3219836369, ___increment_15)); }
	inline FsmInt_t1273009179 * get_increment_15() const { return ___increment_15; }
	inline FsmInt_t1273009179 ** get_address_of_increment_15() { return &___increment_15; }
	inline void set_increment_15(FsmInt_t1273009179 * value)
	{
		___increment_15 = value;
		Il2CppCodeGenWriteBarrier(&___increment_15, value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(ArrayListGetRelative_t3219836369, ___result_16)); }
	inline FsmVar_t2872592513 * get_result_16() const { return ___result_16; }
	inline FsmVar_t2872592513 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmVar_t2872592513 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier(&___result_16, value);
	}

	inline static int32_t get_offset_of_resultIndex_17() { return static_cast<int32_t>(offsetof(ArrayListGetRelative_t3219836369, ___resultIndex_17)); }
	inline FsmInt_t1273009179 * get_resultIndex_17() const { return ___resultIndex_17; }
	inline FsmInt_t1273009179 ** get_address_of_resultIndex_17() { return &___resultIndex_17; }
	inline void set_resultIndex_17(FsmInt_t1273009179 * value)
	{
		___resultIndex_17 = value;
		Il2CppCodeGenWriteBarrier(&___resultIndex_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
