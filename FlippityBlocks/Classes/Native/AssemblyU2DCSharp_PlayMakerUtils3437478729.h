﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// PlayMakerFSM
struct PlayMakerFSM_t437737208;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUtils
struct  PlayMakerUtils_t3437478729  : public Il2CppObject
{
public:

public:
};

struct PlayMakerUtils_t3437478729_StaticFields
{
public:
	// PlayMakerFSM PlayMakerUtils::FsmEventSender
	PlayMakerFSM_t437737208 * ___FsmEventSender_0;

public:
	inline static int32_t get_offset_of_FsmEventSender_0() { return static_cast<int32_t>(offsetof(PlayMakerUtils_t3437478729_StaticFields, ___FsmEventSender_0)); }
	inline PlayMakerFSM_t437737208 * get_FsmEventSender_0() const { return ___FsmEventSender_0; }
	inline PlayMakerFSM_t437737208 ** get_address_of_FsmEventSender_0() { return &___FsmEventSender_0; }
	inline void set_FsmEventSender_0(PlayMakerFSM_t437737208 * value)
	{
		___FsmEventSender_0 = value;
		Il2CppCodeGenWriteBarrier(&___FsmEventSender_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
