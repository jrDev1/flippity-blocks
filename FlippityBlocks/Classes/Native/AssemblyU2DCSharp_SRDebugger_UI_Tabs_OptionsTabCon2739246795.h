﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// System.Collections.Generic.List`1<SRDebugger.UI.Controls.OptionsControlBase>
struct List_1_t2852422836;
// System.Collections.Generic.List`1<SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance>
struct List_1_t1995382133;
// System.Collections.Generic.Dictionary`2<SRDebugger.Internal.OptionDefinition,SRDebugger.UI.Controls.OptionsControlBase>
struct Dictionary_2_t1438329295;
// UnityEngine.Canvas
struct Canvas_t209405766;
// SRDebugger.UI.Controls.Data.ActionControl
struct ActionControl_t3457065707;
// SRDebugger.UI.Other.CategoryGroup
struct CategoryGroup_t4174071633;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// System.Comparison`1<SRDebugger.Internal.OptionDefinition>
struct Comparison_1_t1176147159;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.OptionsTabController
struct  OptionsTabController_t2739246795  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Collections.Generic.List`1<SRDebugger.UI.Controls.OptionsControlBase> SRDebugger.UI.Tabs.OptionsTabController::_controls
	List_1_t2852422836 * ____controls_9;
	// System.Collections.Generic.List`1<SRDebugger.UI.Tabs.OptionsTabController/CategoryInstance> SRDebugger.UI.Tabs.OptionsTabController::_categories
	List_1_t1995382133 * ____categories_10;
	// System.Collections.Generic.Dictionary`2<SRDebugger.Internal.OptionDefinition,SRDebugger.UI.Controls.OptionsControlBase> SRDebugger.UI.Tabs.OptionsTabController::_options
	Dictionary_2_t1438329295 * ____options_11;
	// System.Boolean SRDebugger.UI.Tabs.OptionsTabController::_queueRefresh
	bool ____queueRefresh_12;
	// System.Boolean SRDebugger.UI.Tabs.OptionsTabController::_selectionModeEnabled
	bool ____selectionModeEnabled_13;
	// UnityEngine.Canvas SRDebugger.UI.Tabs.OptionsTabController::_optionCanvas
	Canvas_t209405766 * ____optionCanvas_14;
	// SRDebugger.UI.Controls.Data.ActionControl SRDebugger.UI.Tabs.OptionsTabController::ActionControlPrefab
	ActionControl_t3457065707 * ___ActionControlPrefab_15;
	// SRDebugger.UI.Other.CategoryGroup SRDebugger.UI.Tabs.OptionsTabController::CategoryGroupPrefab
	CategoryGroup_t4174071633 * ___CategoryGroupPrefab_16;
	// UnityEngine.RectTransform SRDebugger.UI.Tabs.OptionsTabController::ContentContainer
	RectTransform_t3349966182 * ___ContentContainer_17;
	// UnityEngine.GameObject SRDebugger.UI.Tabs.OptionsTabController::NoOptionsNotice
	GameObject_t1756533147 * ___NoOptionsNotice_18;
	// UnityEngine.UI.Toggle SRDebugger.UI.Tabs.OptionsTabController::PinButton
	Toggle_t3976754468 * ___PinButton_19;
	// UnityEngine.GameObject SRDebugger.UI.Tabs.OptionsTabController::PinPromptSpacer
	GameObject_t1756533147 * ___PinPromptSpacer_20;
	// UnityEngine.GameObject SRDebugger.UI.Tabs.OptionsTabController::PinPromptText
	GameObject_t1756533147 * ___PinPromptText_21;
	// System.Boolean SRDebugger.UI.Tabs.OptionsTabController::_isTogglingCategory
	bool ____isTogglingCategory_22;

public:
	inline static int32_t get_offset_of__controls_9() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____controls_9)); }
	inline List_1_t2852422836 * get__controls_9() const { return ____controls_9; }
	inline List_1_t2852422836 ** get_address_of__controls_9() { return &____controls_9; }
	inline void set__controls_9(List_1_t2852422836 * value)
	{
		____controls_9 = value;
		Il2CppCodeGenWriteBarrier(&____controls_9, value);
	}

	inline static int32_t get_offset_of__categories_10() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____categories_10)); }
	inline List_1_t1995382133 * get__categories_10() const { return ____categories_10; }
	inline List_1_t1995382133 ** get_address_of__categories_10() { return &____categories_10; }
	inline void set__categories_10(List_1_t1995382133 * value)
	{
		____categories_10 = value;
		Il2CppCodeGenWriteBarrier(&____categories_10, value);
	}

	inline static int32_t get_offset_of__options_11() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____options_11)); }
	inline Dictionary_2_t1438329295 * get__options_11() const { return ____options_11; }
	inline Dictionary_2_t1438329295 ** get_address_of__options_11() { return &____options_11; }
	inline void set__options_11(Dictionary_2_t1438329295 * value)
	{
		____options_11 = value;
		Il2CppCodeGenWriteBarrier(&____options_11, value);
	}

	inline static int32_t get_offset_of__queueRefresh_12() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____queueRefresh_12)); }
	inline bool get__queueRefresh_12() const { return ____queueRefresh_12; }
	inline bool* get_address_of__queueRefresh_12() { return &____queueRefresh_12; }
	inline void set__queueRefresh_12(bool value)
	{
		____queueRefresh_12 = value;
	}

	inline static int32_t get_offset_of__selectionModeEnabled_13() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____selectionModeEnabled_13)); }
	inline bool get__selectionModeEnabled_13() const { return ____selectionModeEnabled_13; }
	inline bool* get_address_of__selectionModeEnabled_13() { return &____selectionModeEnabled_13; }
	inline void set__selectionModeEnabled_13(bool value)
	{
		____selectionModeEnabled_13 = value;
	}

	inline static int32_t get_offset_of__optionCanvas_14() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____optionCanvas_14)); }
	inline Canvas_t209405766 * get__optionCanvas_14() const { return ____optionCanvas_14; }
	inline Canvas_t209405766 ** get_address_of__optionCanvas_14() { return &____optionCanvas_14; }
	inline void set__optionCanvas_14(Canvas_t209405766 * value)
	{
		____optionCanvas_14 = value;
		Il2CppCodeGenWriteBarrier(&____optionCanvas_14, value);
	}

	inline static int32_t get_offset_of_ActionControlPrefab_15() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___ActionControlPrefab_15)); }
	inline ActionControl_t3457065707 * get_ActionControlPrefab_15() const { return ___ActionControlPrefab_15; }
	inline ActionControl_t3457065707 ** get_address_of_ActionControlPrefab_15() { return &___ActionControlPrefab_15; }
	inline void set_ActionControlPrefab_15(ActionControl_t3457065707 * value)
	{
		___ActionControlPrefab_15 = value;
		Il2CppCodeGenWriteBarrier(&___ActionControlPrefab_15, value);
	}

	inline static int32_t get_offset_of_CategoryGroupPrefab_16() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___CategoryGroupPrefab_16)); }
	inline CategoryGroup_t4174071633 * get_CategoryGroupPrefab_16() const { return ___CategoryGroupPrefab_16; }
	inline CategoryGroup_t4174071633 ** get_address_of_CategoryGroupPrefab_16() { return &___CategoryGroupPrefab_16; }
	inline void set_CategoryGroupPrefab_16(CategoryGroup_t4174071633 * value)
	{
		___CategoryGroupPrefab_16 = value;
		Il2CppCodeGenWriteBarrier(&___CategoryGroupPrefab_16, value);
	}

	inline static int32_t get_offset_of_ContentContainer_17() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___ContentContainer_17)); }
	inline RectTransform_t3349966182 * get_ContentContainer_17() const { return ___ContentContainer_17; }
	inline RectTransform_t3349966182 ** get_address_of_ContentContainer_17() { return &___ContentContainer_17; }
	inline void set_ContentContainer_17(RectTransform_t3349966182 * value)
	{
		___ContentContainer_17 = value;
		Il2CppCodeGenWriteBarrier(&___ContentContainer_17, value);
	}

	inline static int32_t get_offset_of_NoOptionsNotice_18() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___NoOptionsNotice_18)); }
	inline GameObject_t1756533147 * get_NoOptionsNotice_18() const { return ___NoOptionsNotice_18; }
	inline GameObject_t1756533147 ** get_address_of_NoOptionsNotice_18() { return &___NoOptionsNotice_18; }
	inline void set_NoOptionsNotice_18(GameObject_t1756533147 * value)
	{
		___NoOptionsNotice_18 = value;
		Il2CppCodeGenWriteBarrier(&___NoOptionsNotice_18, value);
	}

	inline static int32_t get_offset_of_PinButton_19() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___PinButton_19)); }
	inline Toggle_t3976754468 * get_PinButton_19() const { return ___PinButton_19; }
	inline Toggle_t3976754468 ** get_address_of_PinButton_19() { return &___PinButton_19; }
	inline void set_PinButton_19(Toggle_t3976754468 * value)
	{
		___PinButton_19 = value;
		Il2CppCodeGenWriteBarrier(&___PinButton_19, value);
	}

	inline static int32_t get_offset_of_PinPromptSpacer_20() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___PinPromptSpacer_20)); }
	inline GameObject_t1756533147 * get_PinPromptSpacer_20() const { return ___PinPromptSpacer_20; }
	inline GameObject_t1756533147 ** get_address_of_PinPromptSpacer_20() { return &___PinPromptSpacer_20; }
	inline void set_PinPromptSpacer_20(GameObject_t1756533147 * value)
	{
		___PinPromptSpacer_20 = value;
		Il2CppCodeGenWriteBarrier(&___PinPromptSpacer_20, value);
	}

	inline static int32_t get_offset_of_PinPromptText_21() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ___PinPromptText_21)); }
	inline GameObject_t1756533147 * get_PinPromptText_21() const { return ___PinPromptText_21; }
	inline GameObject_t1756533147 ** get_address_of_PinPromptText_21() { return &___PinPromptText_21; }
	inline void set_PinPromptText_21(GameObject_t1756533147 * value)
	{
		___PinPromptText_21 = value;
		Il2CppCodeGenWriteBarrier(&___PinPromptText_21, value);
	}

	inline static int32_t get_offset_of__isTogglingCategory_22() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795, ____isTogglingCategory_22)); }
	inline bool get__isTogglingCategory_22() const { return ____isTogglingCategory_22; }
	inline bool* get_address_of__isTogglingCategory_22() { return &____isTogglingCategory_22; }
	inline void set__isTogglingCategory_22(bool value)
	{
		____isTogglingCategory_22 = value;
	}
};

struct OptionsTabController_t2739246795_StaticFields
{
public:
	// System.Comparison`1<SRDebugger.Internal.OptionDefinition> SRDebugger.UI.Tabs.OptionsTabController::<>f__am$cache0
	Comparison_1_t1176147159 * ___U3CU3Ef__amU24cache0_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_23() { return static_cast<int32_t>(offsetof(OptionsTabController_t2739246795_StaticFields, ___U3CU3Ef__amU24cache0_23)); }
	inline Comparison_1_t1176147159 * get_U3CU3Ef__amU24cache0_23() const { return ___U3CU3Ef__amU24cache0_23; }
	inline Comparison_1_t1176147159 ** get_address_of_U3CU3Ef__amU24cache0_23() { return &___U3CU3Ef__amU24cache0_23; }
	inline void set_U3CU3Ef__amU24cache0_23(Comparison_1_t1176147159 * value)
	{
		___U3CU3Ef__amU24cache0_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
