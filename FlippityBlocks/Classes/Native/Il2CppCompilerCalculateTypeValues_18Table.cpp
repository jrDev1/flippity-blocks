﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm_EditorFlags125951225.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLog3672513366.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogEntry865277298.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmLogType1161667734.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust3741871820.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition_Cust2686433720.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility3735501412.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmUtility_BitConv3908935168.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableType930978778.h"
#include "PlayMaker_HutongGames_PlayMaker_VariableTypeNicifie799256485.h"
#include "PlayMaker_HutongGames_PlayMaker_ArrayVariableTypes1783921275.h"
#include "PlayMaker_HutongGames_PlayMaker_Actions_MissingActio51374616.h"
#include "PlayMaker_PlayMakerPrefs1833055544.h"
#include "PlayMaker_HutongGames_PlayMaker_ReflectionUtils1643754852.h"
#include "PlayMaker_PlayMakerProxyBase3141506643.h"
#include "PlayMaker_PlayMakerAnimatorIK1460122143.h"
#include "PlayMaker_PlayMakerAnimatorMove3213404038.h"
#include "PlayMaker_PlayMakerApplicationEvents3759624703.h"
#include "PlayMaker_PlayMakerCollisionEnter204200476.h"
#include "PlayMaker_PlayMakerCollisionEnter2D422695966.h"
#include "PlayMaker_PlayMakerCollisionExit3953531962.h"
#include "PlayMaker_PlayMakerCollisionExit2D2560737500.h"
#include "PlayMaker_PlayMakerCollisionStay2306635791.h"
#include "PlayMaker_PlayMakerCollisionStay2D4047738741.h"
#include "PlayMaker_PlayMakerControllerColliderHit182605873.h"
#include "PlayMaker_PlayMakerFixedUpdate960084629.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "PlayMaker_PlayMakerFSM_U3CDoCoroutineU3Ed__14237467219.h"
#include "PlayMaker_PlayMakerGlobals2120229426.h"
#include "PlayMaker_PlayMakerGUI2662579489.h"
#include "PlayMaker_PlayMakerMouseEvents3625748060.h"
#include "PlayMaker_PlayMakerOnGUI2031863694.h"
#include "PlayMaker_PlayMakerTriggerEnter2991464208.h"
#include "PlayMaker_PlayMakerTriggerEnter2D2208085914.h"
#include "PlayMaker_PlayMakerTriggerExit2606889942.h"
#include "PlayMaker_PlayMakerTriggerExit2D1071223868.h"
#include "PlayMaker_PlayMakerTriggerStay2768254945.h"
#include "PlayMaker_PlayMakerTriggerStay2D3506020163.h"
#include "PlayMaker_PlayMakerParticleCollision1230092432.h"
#include "PlayMaker_PlayMakerJointBreak1658177799.h"
#include "PlayMaker_PlayMakerJointBreak2D3128675709.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput621514313.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3709210170.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2680906638.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput2561058385.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3236822917.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc249603239.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1328781136.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3293839588.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (EditorFlags_t125951225)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[6] = 
{
	EditorFlags_t125951225::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (FsmEvent_t1258573736), -1, sizeof(FsmEvent_t1258573736_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[45] = 
{
	FsmEvent_t1258573736_StaticFields::get_offset_of_eventList_0(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_syncObj_1(),
	FsmEvent_t1258573736::get_offset_of_name_2(),
	FsmEvent_t1258573736::get_offset_of_isSystemEvent_3(),
	FsmEvent_t1258573736::get_offset_of_isGlobal_4(),
	FsmEvent_t1258573736::get_offset_of_U3CPathU3Ek__BackingField_5(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CBecameInvisibleU3Ek__BackingField_6(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CBecameVisibleU3Ek__BackingField_7(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CCollisionEnterU3Ek__BackingField_8(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CCollisionExitU3Ek__BackingField_9(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CCollisionStayU3Ek__BackingField_10(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CCollisionEnter2DU3Ek__BackingField_11(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CCollisionExit2DU3Ek__BackingField_12(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CCollisionStay2DU3Ek__BackingField_13(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CControllerColliderHitU3Ek__BackingField_14(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CFinishedU3Ek__BackingField_15(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CLevelLoadedU3Ek__BackingField_16(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseDownU3Ek__BackingField_17(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseDragU3Ek__BackingField_18(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseEnterU3Ek__BackingField_19(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseExitU3Ek__BackingField_20(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseOverU3Ek__BackingField_21(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseUpU3Ek__BackingField_22(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMouseUpAsButtonU3Ek__BackingField_23(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CTriggerEnterU3Ek__BackingField_24(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CTriggerExitU3Ek__BackingField_25(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CTriggerStayU3Ek__BackingField_26(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CTriggerEnter2DU3Ek__BackingField_27(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CTriggerExit2DU3Ek__BackingField_28(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CTriggerStay2DU3Ek__BackingField_29(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CApplicationFocusU3Ek__BackingField_30(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CApplicationPauseU3Ek__BackingField_31(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CApplicationQuitU3Ek__BackingField_32(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CParticleCollisionU3Ek__BackingField_33(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CJointBreakU3Ek__BackingField_34(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CJointBreak2DU3Ek__BackingField_35(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CPlayerConnectedU3Ek__BackingField_36(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CServerInitializedU3Ek__BackingField_37(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CConnectedToServerU3Ek__BackingField_38(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CPlayerDisconnectedU3Ek__BackingField_39(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CDisconnectedFromServerU3Ek__BackingField_40(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CFailedToConnectU3Ek__BackingField_41(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CFailedToConnectToMasterServerU3Ek__BackingField_42(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CMasterServerEventU3Ek__BackingField_43(),
	FsmEvent_t1258573736_StaticFields::get_offset_of_U3CNetworkInstantiateU3Ek__BackingField_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (FsmLog_t3672513366), -1, sizeof(FsmLog_t3672513366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1802[9] = 
{
	FsmLog_t3672513366_StaticFields::get_offset_of_MaxSize_0(),
	FsmLog_t3672513366_StaticFields::get_offset_of_Logs_1(),
	FsmLog_t3672513366_StaticFields::get_offset_of_logEntryPool_2(),
	FsmLog_t3672513366_StaticFields::get_offset_of_nextLogEntryPoolIndex_3(),
	FsmLog_t3672513366::get_offset_of_entries_4(),
	FsmLog_t3672513366_StaticFields::get_offset_of_U3CLoggingEnabledU3Ek__BackingField_5(),
	FsmLog_t3672513366_StaticFields::get_offset_of_U3CMirrorDebugLogU3Ek__BackingField_6(),
	FsmLog_t3672513366_StaticFields::get_offset_of_U3CEnableDebugFlowU3Ek__BackingField_7(),
	FsmLog_t3672513366::get_offset_of_U3CFsmU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (FsmLogEntry_t865277298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[19] = 
{
	FsmLogEntry_t865277298::get_offset_of_text_0(),
	FsmLogEntry_t865277298::get_offset_of_textWithTimecode_1(),
	FsmLogEntry_t865277298::get_offset_of_U3CLogU3Ek__BackingField_2(),
	FsmLogEntry_t865277298::get_offset_of_U3CLogTypeU3Ek__BackingField_3(),
	FsmLogEntry_t865277298::get_offset_of_U3CStateU3Ek__BackingField_4(),
	FsmLogEntry_t865277298::get_offset_of_U3CSentByStateU3Ek__BackingField_5(),
	FsmLogEntry_t865277298::get_offset_of_U3CActionU3Ek__BackingField_6(),
	FsmLogEntry_t865277298::get_offset_of_U3CEventU3Ek__BackingField_7(),
	FsmLogEntry_t865277298::get_offset_of_U3CTransitionU3Ek__BackingField_8(),
	FsmLogEntry_t865277298::get_offset_of_U3CEventTargetU3Ek__BackingField_9(),
	FsmLogEntry_t865277298::get_offset_of_U3CTimeU3Ek__BackingField_10(),
	FsmLogEntry_t865277298::get_offset_of_U3CStateTimeU3Ek__BackingField_11(),
	FsmLogEntry_t865277298::get_offset_of_U3CFrameCountU3Ek__BackingField_12(),
	FsmLogEntry_t865277298::get_offset_of_U3CFsmVariablesCopyU3Ek__BackingField_13(),
	FsmLogEntry_t865277298::get_offset_of_U3CGlobalVariablesCopyU3Ek__BackingField_14(),
	FsmLogEntry_t865277298::get_offset_of_U3CGameObjectU3Ek__BackingField_15(),
	FsmLogEntry_t865277298::get_offset_of_U3CGameObjectNameU3Ek__BackingField_16(),
	FsmLogEntry_t865277298::get_offset_of_U3CGameObjectIconU3Ek__BackingField_17(),
	FsmLogEntry_t865277298::get_offset_of_U3CText2U3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (FsmLogType_t1161667734)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[12] = 
{
	FsmLogType_t1161667734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (FsmState_t1643911659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[21] = 
{
	FsmState_t1643911659::get_offset_of_active_0(),
	FsmState_t1643911659::get_offset_of_finished_1(),
	FsmState_t1643911659::get_offset_of_activeAction_2(),
	FsmState_t1643911659::get_offset_of_activeActionIndex_3(),
	FsmState_t1643911659::get_offset_of_fsm_4(),
	FsmState_t1643911659::get_offset_of_name_5(),
	FsmState_t1643911659::get_offset_of_description_6(),
	FsmState_t1643911659::get_offset_of_colorIndex_7(),
	FsmState_t1643911659::get_offset_of_position_8(),
	FsmState_t1643911659::get_offset_of_isBreakpoint_9(),
	FsmState_t1643911659::get_offset_of_isSequence_10(),
	FsmState_t1643911659::get_offset_of_hideUnused_11(),
	FsmState_t1643911659::get_offset_of_transitions_12(),
	FsmState_t1643911659::get_offset_of_actions_13(),
	FsmState_t1643911659::get_offset_of_actionData_14(),
	FsmState_t1643911659::get_offset_of_activeActions_15(),
	FsmState_t1643911659::get_offset_of__finishedActions_16(),
	FsmState_t1643911659::get_offset_of_U3CStateTimeU3Ek__BackingField_17(),
	FsmState_t1643911659::get_offset_of_U3CRealStartTimeU3Ek__BackingField_18(),
	FsmState_t1643911659::get_offset_of_U3CloopCountU3Ek__BackingField_19(),
	FsmState_t1643911659::get_offset_of_U3CmaxLoopCountU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (FsmStateAction_t2862378169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[11] = 
{
	FsmStateAction_t2862378169::get_offset_of_name_0(),
	FsmStateAction_t2862378169::get_offset_of_enabled_1(),
	FsmStateAction_t2862378169::get_offset_of_isOpen_2(),
	FsmStateAction_t2862378169::get_offset_of_active_3(),
	FsmStateAction_t2862378169::get_offset_of_finished_4(),
	FsmStateAction_t2862378169::get_offset_of_autoName_5(),
	FsmStateAction_t2862378169::get_offset_of_owner_6(),
	FsmStateAction_t2862378169::get_offset_of_fsmState_7(),
	FsmStateAction_t2862378169::get_offset_of_fsm_8(),
	FsmStateAction_t2862378169::get_offset_of_fsmComponent_9(),
	FsmStateAction_t2862378169::get_offset_of_U3CEnteredU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (FsmTransition_t1534990431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[5] = 
{
	FsmTransition_t1534990431::get_offset_of_fsmEvent_0(),
	FsmTransition_t1534990431::get_offset_of_toState_1(),
	FsmTransition_t1534990431::get_offset_of_linkStyle_2(),
	FsmTransition_t1534990431::get_offset_of_linkConstraint_3(),
	FsmTransition_t1534990431::get_offset_of_colorIndex_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (CustomLinkStyle_t3741871820)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	CustomLinkStyle_t3741871820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (CustomLinkConstraint_t2686433720)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	CustomLinkConstraint_t2686433720::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (FsmUtility_t3735501412), -1, sizeof(FsmUtility_t3735501412_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	FsmUtility_t3735501412_StaticFields::get_offset_of_encoding_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (BitConverter_t3908935168), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (FsmVariables_t630687169), -1, sizeof(FsmVariables_t630687169_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[18] = 
{
	FsmVariables_t630687169::get_offset_of_floatVariables_0(),
	FsmVariables_t630687169::get_offset_of_intVariables_1(),
	FsmVariables_t630687169::get_offset_of_boolVariables_2(),
	FsmVariables_t630687169::get_offset_of_stringVariables_3(),
	FsmVariables_t630687169::get_offset_of_vector2Variables_4(),
	FsmVariables_t630687169::get_offset_of_vector3Variables_5(),
	FsmVariables_t630687169::get_offset_of_colorVariables_6(),
	FsmVariables_t630687169::get_offset_of_rectVariables_7(),
	FsmVariables_t630687169::get_offset_of_quaternionVariables_8(),
	FsmVariables_t630687169::get_offset_of_gameObjectVariables_9(),
	FsmVariables_t630687169::get_offset_of_objectVariables_10(),
	FsmVariables_t630687169::get_offset_of_materialVariables_11(),
	FsmVariables_t630687169::get_offset_of_textureVariables_12(),
	FsmVariables_t630687169::get_offset_of_arrayVariables_13(),
	FsmVariables_t630687169::get_offset_of_enumVariables_14(),
	FsmVariables_t630687169::get_offset_of_categories_15(),
	FsmVariables_t630687169::get_offset_of_variableCategoryIDs_16(),
	FsmVariables_t630687169_StaticFields::get_offset_of_U3CGlobalVariablesSyncedU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (VariableType_t930978778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1814[17] = 
{
	VariableType_t930978778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (VariableTypeNicified_t799256485)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[16] = 
{
	VariableTypeNicified_t799256485::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (ArrayVariableTypesNicified_t1783921275)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[15] = 
{
	ArrayVariableTypesNicified_t1783921275::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (MissingAction_t51374616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[1] = 
{
	MissingAction_t51374616::get_offset_of_actionName_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (PlayMakerPrefs_t1833055544), -1, sizeof(PlayMakerPrefs_t1833055544_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1818[6] = 
{
	PlayMakerPrefs_t1833055544_StaticFields::get_offset_of_instance_2(),
	PlayMakerPrefs_t1833055544_StaticFields::get_offset_of_defaultColors_3(),
	PlayMakerPrefs_t1833055544_StaticFields::get_offset_of_defaultColorNames_4(),
	PlayMakerPrefs_t1833055544::get_offset_of_colors_5(),
	PlayMakerPrefs_t1833055544::get_offset_of_colorNames_6(),
	PlayMakerPrefs_t1833055544_StaticFields::get_offset_of_minimapColors_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (ReflectionUtils_t1643754852), -1, sizeof(ReflectionUtils_t1643754852_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	ReflectionUtils_t1643754852_StaticFields::get_offset_of_assemblyNames_0(),
	ReflectionUtils_t1643754852_StaticFields::get_offset_of_loadedAssemblies_1(),
	ReflectionUtils_t1643754852_StaticFields::get_offset_of_typeLookup_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (PlayMakerProxyBase_t3141506643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	PlayMakerProxyBase_t3141506643::get_offset_of_playMakerFSMs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (PlayMakerAnimatorIK_t1460122143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (PlayMakerAnimatorMove_t3213404038), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (PlayMakerApplicationEvents_t3759624703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (PlayMakerCollisionEnter_t204200476), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (PlayMakerCollisionEnter2D_t422695966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (PlayMakerCollisionExit_t3953531962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (PlayMakerCollisionExit2D_t2560737500), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (PlayMakerCollisionStay_t2306635791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (PlayMakerCollisionStay2D_t4047738741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (PlayMakerControllerColliderHit_t182605873), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (PlayMakerFixedUpdate_t960084629), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (PlayMakerFSM_t437737208), -1, sizeof(PlayMakerFSM_t437737208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[10] = 
{
	PlayMakerFSM_t437737208_StaticFields::get_offset_of_fsmList_2(),
	PlayMakerFSM_t437737208_StaticFields::get_offset_of_MaximizeFileCompatibility_3(),
	PlayMakerFSM_t437737208_StaticFields::get_offset_of_ApplicationIsQuitting_4(),
	PlayMakerFSM_t437737208_StaticFields::get_offset_of_NotMainThread_5(),
	PlayMakerFSM_t437737208::get_offset_of_fsm_6(),
	PlayMakerFSM_t437737208::get_offset_of_fsmTemplate_7(),
	PlayMakerFSM_t437737208::get_offset_of_eventHandlerComponentsAdded_8(),
	PlayMakerFSM_t437737208::get_offset_of_U3CGuiTextureU3Ek__BackingField_9(),
	PlayMakerFSM_t437737208::get_offset_of_U3CGuiTextU3Ek__BackingField_10(),
	PlayMakerFSM_t437737208_StaticFields::get_offset_of_U3CDrawGizmosU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (U3CDoCoroutineU3Ed__1_t4237467219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[4] = 
{
	U3CDoCoroutineU3Ed__1_t4237467219::get_offset_of_U3CU3E2__current_0(),
	U3CDoCoroutineU3Ed__1_t4237467219::get_offset_of_U3CU3E1__state_1(),
	U3CDoCoroutineU3Ed__1_t4237467219::get_offset_of_U3CU3E4__this_2(),
	U3CDoCoroutineU3Ed__1_t4237467219::get_offset_of_routine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (PlayMakerGlobals_t2120229426), -1, sizeof(PlayMakerGlobals_t2120229426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[8] = 
{
	PlayMakerGlobals_t2120229426_StaticFields::get_offset_of_instance_2(),
	PlayMakerGlobals_t2120229426::get_offset_of_variables_3(),
	PlayMakerGlobals_t2120229426::get_offset_of_events_4(),
	PlayMakerGlobals_t2120229426_StaticFields::get_offset_of_U3CInitializedU3Ek__BackingField_5(),
	PlayMakerGlobals_t2120229426_StaticFields::get_offset_of_U3CIsPlayingInEditorU3Ek__BackingField_6(),
	PlayMakerGlobals_t2120229426_StaticFields::get_offset_of_U3CIsPlayingU3Ek__BackingField_7(),
	PlayMakerGlobals_t2120229426_StaticFields::get_offset_of_U3CIsEditorU3Ek__BackingField_8(),
	PlayMakerGlobals_t2120229426_StaticFields::get_offset_of_U3CIsBuildingU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (PlayMakerGUI_t2662579489), -1, sizeof(PlayMakerGUI_t2662579489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1835[30] = 
{
	0,
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_fsmList_3(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_SelectedFSM_4(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_labelContent_5(),
	PlayMakerGUI_t2662579489::get_offset_of_previewOnGUI_6(),
	PlayMakerGUI_t2662579489::get_offset_of_enableGUILayout_7(),
	PlayMakerGUI_t2662579489::get_offset_of_drawStateLabels_8(),
	PlayMakerGUI_t2662579489::get_offset_of_enableStateLabelsInBuilds_9(),
	PlayMakerGUI_t2662579489::get_offset_of_GUITextureStateLabels_10(),
	PlayMakerGUI_t2662579489::get_offset_of_GUITextStateLabels_11(),
	PlayMakerGUI_t2662579489::get_offset_of_filterLabelsWithDistance_12(),
	PlayMakerGUI_t2662579489::get_offset_of_maxLabelDistance_13(),
	PlayMakerGUI_t2662579489::get_offset_of_controlMouseCursor_14(),
	PlayMakerGUI_t2662579489::get_offset_of_labelScale_15(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_SortedFsmList_16(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_labelGameObject_17(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_fsmLabelIndex_18(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_instance_19(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_guiSkin_20(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_guiColor_21(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_guiBackgroundColor_22(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_guiContentColor_23(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_guiMatrix_24(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_stateLabelStyle_25(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_stateLabelBackground_26(),
	PlayMakerGUI_t2662579489::get_offset_of_initLabelScale_27(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_U3CMouseCursorU3Ek__BackingField_28(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_U3CLockCursorU3Ek__BackingField_29(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_U3CHideCursorU3Ek__BackingField_30(),
	PlayMakerGUI_t2662579489_StaticFields::get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (PlayMakerMouseEvents_t3625748060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (PlayMakerOnGUI_t2031863694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[2] = 
{
	PlayMakerOnGUI_t2031863694::get_offset_of_playMakerFSM_2(),
	PlayMakerOnGUI_t2031863694::get_offset_of_previewInEditMode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (PlayMakerTriggerEnter_t2991464208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (PlayMakerTriggerEnter2D_t2208085914), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (PlayMakerTriggerExit_t2606889942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (PlayMakerTriggerExit2D_t1071223868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (PlayMakerTriggerStay_t2768254945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (PlayMakerTriggerStay2D_t3506020163), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (PlayMakerParticleCollision_t1230092432), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (PlayMakerJointBreak_t1658177799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (PlayMakerJointBreak2D_t3128675709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1848[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1867[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1871[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1872[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1874[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[21] = 
{
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_3(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_hovered_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1882[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1883[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (BaseInput_t621514313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[6] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t1295781545::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t1295781545::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_12(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (MouseButtonEventData_t3709210170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	MouseButtonEventData_t3709210170::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3709210170::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (StandaloneInputModule_t70867863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[13] = 
{
	StandaloneInputModule_t70867863::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t70867863::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t70867863::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t70867863::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t70867863::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t70867863::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t70867863::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t70867863::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t70867863::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t70867863::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t70867863::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (InputMode_t2680906638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1891[3] = 
{
	InputMode_t2680906638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (TouchInputModule_t2561058385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[3] = 
{
	TouchInputModule_t2561058385::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t2561058385::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t2561058385::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (BaseRaycaster_t2336171397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (Physics2DRaycaster_t3236822917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (PhysicsRaycaster_t249603239), -1, sizeof(PhysicsRaycaster_t249603239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1895[4] = 
{
	0,
	PhysicsRaycaster_t249603239::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t249603239::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t249603239_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (ColorTween_t3438117476)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[6] = 
{
	ColorTween_t3438117476::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ColorTweenMode_t1328781136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[4] = 
{
	ColorTweenMode_t1328781136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (ColorTweenCallback_t3293839588), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
