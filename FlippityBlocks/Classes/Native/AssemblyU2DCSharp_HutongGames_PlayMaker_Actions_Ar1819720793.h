﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListRemove
struct  ArrayListRemove_t1819720793  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListRemove::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListRemove::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayListRemove::variable
	FsmVar_t2872592513 * ___variable_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListRemove::notFoundEvent
	FsmEvent_t1258573736 * ___notFoundEvent_15;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListRemove_t1819720793, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListRemove_t1819720793, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_variable_14() { return static_cast<int32_t>(offsetof(ArrayListRemove_t1819720793, ___variable_14)); }
	inline FsmVar_t2872592513 * get_variable_14() const { return ___variable_14; }
	inline FsmVar_t2872592513 ** get_address_of_variable_14() { return &___variable_14; }
	inline void set_variable_14(FsmVar_t2872592513 * value)
	{
		___variable_14 = value;
		Il2CppCodeGenWriteBarrier(&___variable_14, value);
	}

	inline static int32_t get_offset_of_notFoundEvent_15() { return static_cast<int32_t>(offsetof(ArrayListRemove_t1819720793, ___notFoundEvent_15)); }
	inline FsmEvent_t1258573736 * get_notFoundEvent_15() const { return ___notFoundEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_notFoundEvent_15() { return &___notFoundEvent_15; }
	inline void set_notFoundEvent_15(FsmEvent_t1258573736 * value)
	{
		___notFoundEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___notFoundEvent_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
