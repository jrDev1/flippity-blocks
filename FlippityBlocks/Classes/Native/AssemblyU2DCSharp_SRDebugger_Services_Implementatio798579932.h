﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen466153640.h"

// SRDebugger.Services.PinEntryCompleteCallback
struct PinEntryCompleteCallback_t4175557387;
// SRDebugger.UI.Controls.PinEntryControl
struct PinEntryControl_t1988638876;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.PinEntryServiceImpl
struct  PinEntryServiceImpl_t798579932  : public SRServiceBase_1_t466153640
{
public:
	// SRDebugger.Services.PinEntryCompleteCallback SRDebugger.Services.Implementation.PinEntryServiceImpl::_callback
	PinEntryCompleteCallback_t4175557387 * ____callback_9;
	// System.Boolean SRDebugger.Services.Implementation.PinEntryServiceImpl::_isVisible
	bool ____isVisible_10;
	// SRDebugger.UI.Controls.PinEntryControl SRDebugger.Services.Implementation.PinEntryServiceImpl::_pinControl
	PinEntryControl_t1988638876 * ____pinControl_11;
	// System.Collections.Generic.List`1<System.Int32> SRDebugger.Services.Implementation.PinEntryServiceImpl::_requiredPin
	List_1_t1440998580 * ____requiredPin_12;

public:
	inline static int32_t get_offset_of__callback_9() { return static_cast<int32_t>(offsetof(PinEntryServiceImpl_t798579932, ____callback_9)); }
	inline PinEntryCompleteCallback_t4175557387 * get__callback_9() const { return ____callback_9; }
	inline PinEntryCompleteCallback_t4175557387 ** get_address_of__callback_9() { return &____callback_9; }
	inline void set__callback_9(PinEntryCompleteCallback_t4175557387 * value)
	{
		____callback_9 = value;
		Il2CppCodeGenWriteBarrier(&____callback_9, value);
	}

	inline static int32_t get_offset_of__isVisible_10() { return static_cast<int32_t>(offsetof(PinEntryServiceImpl_t798579932, ____isVisible_10)); }
	inline bool get__isVisible_10() const { return ____isVisible_10; }
	inline bool* get_address_of__isVisible_10() { return &____isVisible_10; }
	inline void set__isVisible_10(bool value)
	{
		____isVisible_10 = value;
	}

	inline static int32_t get_offset_of__pinControl_11() { return static_cast<int32_t>(offsetof(PinEntryServiceImpl_t798579932, ____pinControl_11)); }
	inline PinEntryControl_t1988638876 * get__pinControl_11() const { return ____pinControl_11; }
	inline PinEntryControl_t1988638876 ** get_address_of__pinControl_11() { return &____pinControl_11; }
	inline void set__pinControl_11(PinEntryControl_t1988638876 * value)
	{
		____pinControl_11 = value;
		Il2CppCodeGenWriteBarrier(&____pinControl_11, value);
	}

	inline static int32_t get_offset_of__requiredPin_12() { return static_cast<int32_t>(offsetof(PinEntryServiceImpl_t798579932, ____requiredPin_12)); }
	inline List_1_t1440998580 * get__requiredPin_12() const { return ____requiredPin_12; }
	inline List_1_t1440998580 ** get_address_of__requiredPin_12() { return &____requiredPin_12; }
	inline void set__requiredPin_12(List_1_t1440998580 * value)
	{
		____requiredPin_12 = value;
		Il2CppCodeGenWriteBarrier(&____requiredPin_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
