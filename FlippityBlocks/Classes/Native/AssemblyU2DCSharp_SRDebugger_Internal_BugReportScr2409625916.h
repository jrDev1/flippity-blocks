﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.BugReportScreenshotUtil
struct  BugReportScreenshotUtil_t2409625916  : public Il2CppObject
{
public:

public:
};

struct BugReportScreenshotUtil_t2409625916_StaticFields
{
public:
	// System.Byte[] SRDebugger.Internal.BugReportScreenshotUtil::ScreenshotData
	ByteU5BU5D_t3397334013* ___ScreenshotData_0;

public:
	inline static int32_t get_offset_of_ScreenshotData_0() { return static_cast<int32_t>(offsetof(BugReportScreenshotUtil_t2409625916_StaticFields, ___ScreenshotData_0)); }
	inline ByteU5BU5D_t3397334013* get_ScreenshotData_0() const { return ___ScreenshotData_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_ScreenshotData_0() { return &___ScreenshotData_0; }
	inline void set_ScreenshotData_0(ByteU5BU5D_t3397334013* value)
	{
		___ScreenshotData_0 = value;
		Il2CppCodeGenWriteBarrier(&___ScreenshotData_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
