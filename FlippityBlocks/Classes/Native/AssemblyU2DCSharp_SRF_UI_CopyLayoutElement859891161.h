﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.CopyLayoutElement
struct  CopyLayoutElement_t859891161  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean SRF.UI.CopyLayoutElement::CopyMinHeight
	bool ___CopyMinHeight_2;
	// System.Boolean SRF.UI.CopyLayoutElement::CopyMinWidth
	bool ___CopyMinWidth_3;
	// System.Boolean SRF.UI.CopyLayoutElement::CopyPreferredHeight
	bool ___CopyPreferredHeight_4;
	// System.Boolean SRF.UI.CopyLayoutElement::CopyPreferredWidth
	bool ___CopyPreferredWidth_5;
	// UnityEngine.RectTransform SRF.UI.CopyLayoutElement::CopySource
	RectTransform_t3349966182 * ___CopySource_6;
	// System.Single SRF.UI.CopyLayoutElement::PaddingMinHeight
	float ___PaddingMinHeight_7;
	// System.Single SRF.UI.CopyLayoutElement::PaddingMinWidth
	float ___PaddingMinWidth_8;
	// System.Single SRF.UI.CopyLayoutElement::PaddingPreferredHeight
	float ___PaddingPreferredHeight_9;
	// System.Single SRF.UI.CopyLayoutElement::PaddingPreferredWidth
	float ___PaddingPreferredWidth_10;

public:
	inline static int32_t get_offset_of_CopyMinHeight_2() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___CopyMinHeight_2)); }
	inline bool get_CopyMinHeight_2() const { return ___CopyMinHeight_2; }
	inline bool* get_address_of_CopyMinHeight_2() { return &___CopyMinHeight_2; }
	inline void set_CopyMinHeight_2(bool value)
	{
		___CopyMinHeight_2 = value;
	}

	inline static int32_t get_offset_of_CopyMinWidth_3() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___CopyMinWidth_3)); }
	inline bool get_CopyMinWidth_3() const { return ___CopyMinWidth_3; }
	inline bool* get_address_of_CopyMinWidth_3() { return &___CopyMinWidth_3; }
	inline void set_CopyMinWidth_3(bool value)
	{
		___CopyMinWidth_3 = value;
	}

	inline static int32_t get_offset_of_CopyPreferredHeight_4() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___CopyPreferredHeight_4)); }
	inline bool get_CopyPreferredHeight_4() const { return ___CopyPreferredHeight_4; }
	inline bool* get_address_of_CopyPreferredHeight_4() { return &___CopyPreferredHeight_4; }
	inline void set_CopyPreferredHeight_4(bool value)
	{
		___CopyPreferredHeight_4 = value;
	}

	inline static int32_t get_offset_of_CopyPreferredWidth_5() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___CopyPreferredWidth_5)); }
	inline bool get_CopyPreferredWidth_5() const { return ___CopyPreferredWidth_5; }
	inline bool* get_address_of_CopyPreferredWidth_5() { return &___CopyPreferredWidth_5; }
	inline void set_CopyPreferredWidth_5(bool value)
	{
		___CopyPreferredWidth_5 = value;
	}

	inline static int32_t get_offset_of_CopySource_6() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___CopySource_6)); }
	inline RectTransform_t3349966182 * get_CopySource_6() const { return ___CopySource_6; }
	inline RectTransform_t3349966182 ** get_address_of_CopySource_6() { return &___CopySource_6; }
	inline void set_CopySource_6(RectTransform_t3349966182 * value)
	{
		___CopySource_6 = value;
		Il2CppCodeGenWriteBarrier(&___CopySource_6, value);
	}

	inline static int32_t get_offset_of_PaddingMinHeight_7() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___PaddingMinHeight_7)); }
	inline float get_PaddingMinHeight_7() const { return ___PaddingMinHeight_7; }
	inline float* get_address_of_PaddingMinHeight_7() { return &___PaddingMinHeight_7; }
	inline void set_PaddingMinHeight_7(float value)
	{
		___PaddingMinHeight_7 = value;
	}

	inline static int32_t get_offset_of_PaddingMinWidth_8() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___PaddingMinWidth_8)); }
	inline float get_PaddingMinWidth_8() const { return ___PaddingMinWidth_8; }
	inline float* get_address_of_PaddingMinWidth_8() { return &___PaddingMinWidth_8; }
	inline void set_PaddingMinWidth_8(float value)
	{
		___PaddingMinWidth_8 = value;
	}

	inline static int32_t get_offset_of_PaddingPreferredHeight_9() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___PaddingPreferredHeight_9)); }
	inline float get_PaddingPreferredHeight_9() const { return ___PaddingPreferredHeight_9; }
	inline float* get_address_of_PaddingPreferredHeight_9() { return &___PaddingPreferredHeight_9; }
	inline void set_PaddingPreferredHeight_9(float value)
	{
		___PaddingPreferredHeight_9 = value;
	}

	inline static int32_t get_offset_of_PaddingPreferredWidth_10() { return static_cast<int32_t>(offsetof(CopyLayoutElement_t859891161, ___PaddingPreferredWidth_10)); }
	inline float get_PaddingPreferredWidth_10() const { return ___PaddingPreferredWidth_10; }
	inline float* get_address_of_PaddingPreferredWidth_10() { return &___PaddingPreferredWidth_10; }
	inline void set_PaddingPreferredWidth_10(float value)
	{
		___PaddingPreferredWidth_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
