﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen335453255.h"

// SRDebugger.Services.BugReportCompleteCallback
struct BugReportCompleteCallback_t3977973366;
// SRDebugger.UI.Other.BugReportPopoverRoot
struct BugReportPopoverRoot_t3609447367;
// SRDebugger.UI.Other.BugReportSheetController
struct BugReportSheetController_t1864182511;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.BugReportPopoverService
struct  BugReportPopoverService_t956868698  : public SRServiceBase_1_t335453255
{
public:
	// SRDebugger.Services.BugReportCompleteCallback SRDebugger.Services.Implementation.BugReportPopoverService::_callback
	BugReportCompleteCallback_t3977973366 * ____callback_9;
	// System.Boolean SRDebugger.Services.Implementation.BugReportPopoverService::_isVisible
	bool ____isVisible_10;
	// SRDebugger.UI.Other.BugReportPopoverRoot SRDebugger.Services.Implementation.BugReportPopoverService::_popover
	BugReportPopoverRoot_t3609447367 * ____popover_11;
	// SRDebugger.UI.Other.BugReportSheetController SRDebugger.Services.Implementation.BugReportPopoverService::_sheet
	BugReportSheetController_t1864182511 * ____sheet_12;

public:
	inline static int32_t get_offset_of__callback_9() { return static_cast<int32_t>(offsetof(BugReportPopoverService_t956868698, ____callback_9)); }
	inline BugReportCompleteCallback_t3977973366 * get__callback_9() const { return ____callback_9; }
	inline BugReportCompleteCallback_t3977973366 ** get_address_of__callback_9() { return &____callback_9; }
	inline void set__callback_9(BugReportCompleteCallback_t3977973366 * value)
	{
		____callback_9 = value;
		Il2CppCodeGenWriteBarrier(&____callback_9, value);
	}

	inline static int32_t get_offset_of__isVisible_10() { return static_cast<int32_t>(offsetof(BugReportPopoverService_t956868698, ____isVisible_10)); }
	inline bool get__isVisible_10() const { return ____isVisible_10; }
	inline bool* get_address_of__isVisible_10() { return &____isVisible_10; }
	inline void set__isVisible_10(bool value)
	{
		____isVisible_10 = value;
	}

	inline static int32_t get_offset_of__popover_11() { return static_cast<int32_t>(offsetof(BugReportPopoverService_t956868698, ____popover_11)); }
	inline BugReportPopoverRoot_t3609447367 * get__popover_11() const { return ____popover_11; }
	inline BugReportPopoverRoot_t3609447367 ** get_address_of__popover_11() { return &____popover_11; }
	inline void set__popover_11(BugReportPopoverRoot_t3609447367 * value)
	{
		____popover_11 = value;
		Il2CppCodeGenWriteBarrier(&____popover_11, value);
	}

	inline static int32_t get_offset_of__sheet_12() { return static_cast<int32_t>(offsetof(BugReportPopoverService_t956868698, ____sheet_12)); }
	inline BugReportSheetController_t1864182511 * get__sheet_12() const { return ____sheet_12; }
	inline BugReportSheetController_t1864182511 ** get_address_of__sheet_12() { return &____sheet_12; }
	inline void set__sheet_12(BugReportSheetController_t1864182511 * value)
	{
		____sheet_12 = value;
		Il2CppCodeGenWriteBarrier(&____sheet_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
