﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"

// System.Collections.Generic.IList`1<UnityEngine.RectTransform>
struct IList_1_t3890906783;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.Layout.FlowLayoutGroup
struct  FlowLayoutGroup_t719127507  : public LayoutGroup_t3962498969
{
public:
	// System.Collections.Generic.IList`1<UnityEngine.RectTransform> SRF.UI.Layout.FlowLayoutGroup::_rowList
	Il2CppObject* ____rowList_10;
	// System.Single SRF.UI.Layout.FlowLayoutGroup::_layoutHeight
	float ____layoutHeight_11;
	// System.Boolean SRF.UI.Layout.FlowLayoutGroup::ChildForceExpandHeight
	bool ___ChildForceExpandHeight_12;
	// System.Boolean SRF.UI.Layout.FlowLayoutGroup::ChildForceExpandWidth
	bool ___ChildForceExpandWidth_13;
	// System.Single SRF.UI.Layout.FlowLayoutGroup::Spacing
	float ___Spacing_14;

public:
	inline static int32_t get_offset_of__rowList_10() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t719127507, ____rowList_10)); }
	inline Il2CppObject* get__rowList_10() const { return ____rowList_10; }
	inline Il2CppObject** get_address_of__rowList_10() { return &____rowList_10; }
	inline void set__rowList_10(Il2CppObject* value)
	{
		____rowList_10 = value;
		Il2CppCodeGenWriteBarrier(&____rowList_10, value);
	}

	inline static int32_t get_offset_of__layoutHeight_11() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t719127507, ____layoutHeight_11)); }
	inline float get__layoutHeight_11() const { return ____layoutHeight_11; }
	inline float* get_address_of__layoutHeight_11() { return &____layoutHeight_11; }
	inline void set__layoutHeight_11(float value)
	{
		____layoutHeight_11 = value;
	}

	inline static int32_t get_offset_of_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t719127507, ___ChildForceExpandHeight_12)); }
	inline bool get_ChildForceExpandHeight_12() const { return ___ChildForceExpandHeight_12; }
	inline bool* get_address_of_ChildForceExpandHeight_12() { return &___ChildForceExpandHeight_12; }
	inline void set_ChildForceExpandHeight_12(bool value)
	{
		___ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t719127507, ___ChildForceExpandWidth_13)); }
	inline bool get_ChildForceExpandWidth_13() const { return ___ChildForceExpandWidth_13; }
	inline bool* get_address_of_ChildForceExpandWidth_13() { return &___ChildForceExpandWidth_13; }
	inline void set_ChildForceExpandWidth_13(bool value)
	{
		___ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_Spacing_14() { return static_cast<int32_t>(offsetof(FlowLayoutGroup_t719127507, ___Spacing_14)); }
	inline float get_Spacing_14() const { return ___Spacing_14; }
	inline float* get_address_of_Spacing_14() { return &___Spacing_14; }
	inline void set_Spacing_14(float value)
	{
		___Spacing_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
