﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen1635681411.h"

// SRDebugger.UI.DebugPanelRoot
struct DebugPanelRoot_t2681537317;
// System.Action`2<SRDebugger.Services.IDebugPanelService,System.Boolean>
struct Action_2_t410303404;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.DebugPanelServiceImpl
struct  DebugPanelServiceImpl_t106306942  : public ScriptableObject_t1975622470
{
public:
	// SRDebugger.UI.DebugPanelRoot SRDebugger.Services.Implementation.DebugPanelServiceImpl::_debugPanelRootObject
	DebugPanelRoot_t2681537317 * ____debugPanelRootObject_2;
	// System.Action`2<SRDebugger.Services.IDebugPanelService,System.Boolean> SRDebugger.Services.Implementation.DebugPanelServiceImpl::VisibilityChanged
	Action_2_t410303404 * ___VisibilityChanged_3;
	// System.Boolean SRDebugger.Services.Implementation.DebugPanelServiceImpl::_isVisible
	bool ____isVisible_4;
	// System.Nullable`1<System.Boolean> SRDebugger.Services.Implementation.DebugPanelServiceImpl::_cursorWasVisible
	Nullable_1_t2088641033  ____cursorWasVisible_5;
	// System.Nullable`1<UnityEngine.CursorLockMode> SRDebugger.Services.Implementation.DebugPanelServiceImpl::_cursorLockMode
	Nullable_1_t1635681411  ____cursorLockMode_6;

public:
	inline static int32_t get_offset_of__debugPanelRootObject_2() { return static_cast<int32_t>(offsetof(DebugPanelServiceImpl_t106306942, ____debugPanelRootObject_2)); }
	inline DebugPanelRoot_t2681537317 * get__debugPanelRootObject_2() const { return ____debugPanelRootObject_2; }
	inline DebugPanelRoot_t2681537317 ** get_address_of__debugPanelRootObject_2() { return &____debugPanelRootObject_2; }
	inline void set__debugPanelRootObject_2(DebugPanelRoot_t2681537317 * value)
	{
		____debugPanelRootObject_2 = value;
		Il2CppCodeGenWriteBarrier(&____debugPanelRootObject_2, value);
	}

	inline static int32_t get_offset_of_VisibilityChanged_3() { return static_cast<int32_t>(offsetof(DebugPanelServiceImpl_t106306942, ___VisibilityChanged_3)); }
	inline Action_2_t410303404 * get_VisibilityChanged_3() const { return ___VisibilityChanged_3; }
	inline Action_2_t410303404 ** get_address_of_VisibilityChanged_3() { return &___VisibilityChanged_3; }
	inline void set_VisibilityChanged_3(Action_2_t410303404 * value)
	{
		___VisibilityChanged_3 = value;
		Il2CppCodeGenWriteBarrier(&___VisibilityChanged_3, value);
	}

	inline static int32_t get_offset_of__isVisible_4() { return static_cast<int32_t>(offsetof(DebugPanelServiceImpl_t106306942, ____isVisible_4)); }
	inline bool get__isVisible_4() const { return ____isVisible_4; }
	inline bool* get_address_of__isVisible_4() { return &____isVisible_4; }
	inline void set__isVisible_4(bool value)
	{
		____isVisible_4 = value;
	}

	inline static int32_t get_offset_of__cursorWasVisible_5() { return static_cast<int32_t>(offsetof(DebugPanelServiceImpl_t106306942, ____cursorWasVisible_5)); }
	inline Nullable_1_t2088641033  get__cursorWasVisible_5() const { return ____cursorWasVisible_5; }
	inline Nullable_1_t2088641033 * get_address_of__cursorWasVisible_5() { return &____cursorWasVisible_5; }
	inline void set__cursorWasVisible_5(Nullable_1_t2088641033  value)
	{
		____cursorWasVisible_5 = value;
	}

	inline static int32_t get_offset_of__cursorLockMode_6() { return static_cast<int32_t>(offsetof(DebugPanelServiceImpl_t106306942, ____cursorLockMode_6)); }
	inline Nullable_1_t1635681411  get__cursorLockMode_6() const { return ____cursorLockMode_6; }
	inline Nullable_1_t1635681411 * get_address_of__cursorLockMode_6() { return &____cursorLockMode_6; }
	inline void set__cursorLockMode_6(Nullable_1_t1635681411  value)
	{
		____cursorLockMode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
