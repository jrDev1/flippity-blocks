﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SRF_Json_Parser3601429971.h"
#include "AssemblyU2DCSharp_SRF_Json_Parser_TOKEN587396723.h"
#include "AssemblyU2DCSharp_SRF_Json_Serializer2905244580.h"
#include "AssemblyU2DCSharp_SRF_Internal_ComponentMenuPaths525022418.h"
#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"
#include "AssemblyU2DCSharp_SRF_RequiredFieldAttribute2121822791.h"
#include "AssemblyU2DCSharp_SRF_ImportAttribute2021143331.h"
#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"
#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx_FieldInfo2603195560.h"
#include "AssemblyU2DCSharp_SRF_Coroutines3967614489.h"
#include "AssemblyU2DCSharp_SRF_Coroutines_U3CWaitForSecondsR296318494.h"
#include "AssemblyU2DCSharp_SRF_SRFFloatExtensions1393548501.h"
#include "AssemblyU2DCSharp_SRF_SRFGameObjectExtensions4088930220.h"
#include "AssemblyU2DCSharp_SRF_SRFIListExtensions1425110228.h"
#include "AssemblyU2DCSharp_SRF_SRFStringExtensions3506610346.h"
#include "AssemblyU2DCSharp_SRF_SRFTransformExtensions3185796567.h"
#include "AssemblyU2DCSharp_SRF_SRFTransformExtensions_U3CGe2448828195.h"
#include "AssemblyU2DCSharp_SRF_Helpers_AssetUtil831232134.h"
#include "AssemblyU2DCSharp_SRF_Hierarchy2267531791.h"
#include "AssemblyU2DCSharp_SRF_Helpers_MethodReference3498339100.h"
#include "AssemblyU2DCSharp_SRF_Helpers_PropertyReference1009137956.h"
#include "AssemblyU2DCSharp_SRDebugUtil1529540768.h"
#include "AssemblyU2DCSharp_SRFileUtil1468275449.h"
#include "AssemblyU2DCSharp_SRInstantiate1251403549.h"
#include "AssemblyU2DCSharp_SRMath52963407.h"
#include "AssemblyU2DCSharp_SRMath_TweenFunctions2356057452.h"
#include "AssemblyU2DCSharp_SRMath_EaseType892987806.h"
#include "AssemblyU2DCSharp_SRF_Helpers_SRReflection1802876238.h"
#include "AssemblyU2DCSharp_SRF_Service_ServiceAttribute1825329269.h"
#include "AssemblyU2DCSharp_SRF_Service_ServiceSelectorAttri1476084060.h"
#include "AssemblyU2DCSharp_SRF_Service_ServiceConstructorAttr57773005.h"
#include "AssemblyU2DCSharp_SRF_Service_SRServiceManager2267823955.h"
#include "AssemblyU2DCSharp_SRF_Service_SRServiceManager_Ser4023205305.h"
#include "AssemblyU2DCSharp_SRF_Service_SRServiceManager_Serv376495697.h"
#include "AssemblyU2DCSharp_SRF_Service_SRServiceManager_U3C4068173263.h"
#include "AssemblyU2DCSharp_SRF_Service_SRServiceManager_U3C3510309839.h"
#include "AssemblyU2DCSharp_SRF_UI_ContentFitText3144722753.h"
#include "AssemblyU2DCSharp_SRF_UI_CopyLayoutElement859891161.h"
#include "AssemblyU2DCSharp_SRF_UI_CopyPreferredSize2307038875.h"
#include "AssemblyU2DCSharp_SRF_UI_CopySizeIntoLayoutElement2725109818.h"
#include "AssemblyU2DCSharp_SRF_UI_DragHandle2261255472.h"
#include "AssemblyU2DCSharp_SRF_UI_FlashGraphic1655109972.h"
#include "AssemblyU2DCSharp_SRF_UI_InheritColour1066420259.h"
#include "AssemblyU2DCSharp_SRF_UI_Layout_FlowLayoutGroup719127507.h"
#include "AssemblyU2DCSharp_SRF_UI_Layout_VirtualVerticalLay4077716920.h"
#include "AssemblyU2DCSharp_SRF_UI_Layout_VirtualVerticalLay4048256138.h"
#include "AssemblyU2DCSharp_SRF_UI_Layout_VirtualVerticalLayo725569592.h"
#include "AssemblyU2DCSharp_SRF_UI_LongPressButton1525827641.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveBase1813519265.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveEnable2173267019.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveEnable_Modes1535981351.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveEnable_Entry16942375.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveResize1089670572.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveResize_Element1410679598.h"
#include "AssemblyU2DCSharp_SRF_UI_ResponsiveResize_Element_1466074441.h"
#include "AssemblyU2DCSharp_SRF_UI_SRNumberButton836980342.h"
#include "AssemblyU2DCSharp_SRF_UI_SRNumberSpinner2685847661.h"
#include "AssemblyU2DCSharp_SRF_UI_SRRetinaScaler140471244.h"
#include "AssemblyU2DCSharp_SRF_UI_SRSpinner2158684882.h"
#include "AssemblyU2DCSharp_SRF_UI_SRSpinner_SpinEvent3474549388.h"
#include "AssemblyU2DCSharp_SRF_UI_SRText1495418442.h"
#include "AssemblyU2DCSharp_SRF_UI_ScrollToBottomBehaviour2402064726.h"
#include "AssemblyU2DCSharp_SRF_UI_StyleComponent3036492378.h"
#include "AssemblyU2DCSharp_SRF_UI_StyleRoot1691968825.h"
#include "AssemblyU2DCSharp_SRF_UI_Style1578998053.h"
#include "AssemblyU2DCSharp_SRF_UI_StyleSheet2708161410.h"
#include "AssemblyU2DCSharp_SRF_UI_Unselectable2595244145.h"
#include "AssemblyU2DCSharp_UnfinityGames_U2DEX_u2dexGrid3875635992.h"
#include "AssemblyU2DCSharp_UnfinityGames_U2DEX_u2dexGrid_Gr3851649610.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr954317994.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Arr509737366.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Co1896095612.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Fl3788524128.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Flo515012733.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Int592117905.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_In3364028834.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Sc3519220847.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ve2559142310.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Vec269491953.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ve1113646055.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Wa2317729974.h"
#include "AssemblyU2DCSharp_iTween488923914.h"
#include "AssemblyU2DCSharp_iTween_EasingFunction3676968174.h"
#include "AssemblyU2DCSharp_iTween_ApplyTween747394300.h"
#include "AssemblyU2DCSharp_iTween_EaseType818674011.h"
#include "AssemblyU2DCSharp_iTween_LoopType1490651981.h"
#include "AssemblyU2DCSharp_iTween_NamedValueColor2874784184.h"
#include "AssemblyU2DCSharp_iTween_Defaults4204852305.h"
#include "AssemblyU2DCSharp_iTween_CRSpline4177960625.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3400 = { sizeof (Parser_t3601429971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3400[2] = 
{
	0,
	Parser_t3601429971::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3401 = { sizeof (TOKEN_t587396723)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3401[13] = 
{
	TOKEN_t587396723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3402 = { sizeof (Serializer_t2905244580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3402[1] = 
{
	Serializer_t2905244580::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3403 = { sizeof (ComponentMenuPaths_t525022418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3403[48] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3404 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3404[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3405 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3405[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3406 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3406[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3407 = { sizeof (SRMonoBehaviour_t2352136145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3407[6] = 
{
	SRMonoBehaviour_t2352136145::get_offset_of__collider_2(),
	SRMonoBehaviour_t2352136145::get_offset_of__transform_3(),
	SRMonoBehaviour_t2352136145::get_offset_of__rigidBody_4(),
	SRMonoBehaviour_t2352136145::get_offset_of__gameObject_5(),
	SRMonoBehaviour_t2352136145::get_offset_of__rigidbody2D_6(),
	SRMonoBehaviour_t2352136145::get_offset_of__collider2D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3408 = { sizeof (RequiredFieldAttribute_t2121822791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3408[3] = 
{
	RequiredFieldAttribute_t2121822791::get_offset_of__autoCreate_0(),
	RequiredFieldAttribute_t2121822791::get_offset_of__autoSearch_1(),
	RequiredFieldAttribute_t2121822791::get_offset_of__editorOnly_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3409 = { sizeof (ImportAttribute_t2021143331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3409[1] = 
{
	ImportAttribute_t2021143331::get_offset_of_Service_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3410 = { sizeof (SRMonoBehaviourEx_t3120278648), -1, sizeof(SRMonoBehaviourEx_t3120278648_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3410[1] = 
{
	SRMonoBehaviourEx_t3120278648_StaticFields::get_offset_of__checkedFields_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3411 = { sizeof (FieldInfo_t2603195560)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3411[5] = 
{
	FieldInfo_t2603195560::get_offset_of_AutoCreate_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FieldInfo_t2603195560::get_offset_of_AutoSet_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FieldInfo_t2603195560::get_offset_of_Field_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FieldInfo_t2603195560::get_offset_of_Import_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FieldInfo_t2603195560::get_offset_of_ImportType_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3412 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3412[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3413 = { sizeof (Coroutines_t3967614489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3414 = { sizeof (U3CWaitForSecondsRealTimeU3Ec__Iterator0_t296318494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3414[5] = 
{
	U3CWaitForSecondsRealTimeU3Ec__Iterator0_t296318494::get_offset_of_time_0(),
	U3CWaitForSecondsRealTimeU3Ec__Iterator0_t296318494::get_offset_of_U3CendTimeU3E__0_1(),
	U3CWaitForSecondsRealTimeU3Ec__Iterator0_t296318494::get_offset_of_U24current_2(),
	U3CWaitForSecondsRealTimeU3Ec__Iterator0_t296318494::get_offset_of_U24disposing_3(),
	U3CWaitForSecondsRealTimeU3Ec__Iterator0_t296318494::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3415 = { sizeof (SRFFloatExtensions_t1393548501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3416 = { sizeof (SRFGameObjectExtensions_t4088930220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3417 = { sizeof (SRFIListExtensions_t1425110228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3418 = { sizeof (SRFStringExtensions_t3506610346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3419 = { sizeof (SRFTransformExtensions_t3185796567), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3420 = { sizeof (U3CGetChildrenU3Ec__Iterator0_t2448828195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3420[5] = 
{
	U3CGetChildrenU3Ec__Iterator0_t2448828195::get_offset_of_U3CiU3E__0_0(),
	U3CGetChildrenU3Ec__Iterator0_t2448828195::get_offset_of_t_1(),
	U3CGetChildrenU3Ec__Iterator0_t2448828195::get_offset_of_U24current_2(),
	U3CGetChildrenU3Ec__Iterator0_t2448828195::get_offset_of_U24disposing_3(),
	U3CGetChildrenU3Ec__Iterator0_t2448828195::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3421 = { sizeof (AssetUtil_t831232134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3422 = { sizeof (Hierarchy_t2267531791), -1, sizeof(Hierarchy_t2267531791_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3422[2] = 
{
	Hierarchy_t2267531791_StaticFields::get_offset_of_Seperator_0(),
	Hierarchy_t2267531791_StaticFields::get_offset_of_Cache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3423 = { sizeof (MethodReference_t3498339100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3423[2] = 
{
	MethodReference_t3498339100::get_offset_of__method_0(),
	MethodReference_t3498339100::get_offset_of__target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3424 = { sizeof (PropertyReference_t1009137956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3424[2] = 
{
	PropertyReference_t1009137956::get_offset_of__property_0(),
	PropertyReference_t1009137956::get_offset_of__target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3425 = { sizeof (SRDebugUtil_t1529540768), -1, sizeof(SRDebugUtil_t1529540768_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3425[2] = 
{
	0,
	SRDebugUtil_t1529540768_StaticFields::get_offset_of_U3CIsFixedUpdateU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3426 = { sizeof (SRFileUtil_t1468275449), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3427 = { sizeof (SRInstantiate_t1251403549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3428 = { sizeof (SRMath_t52963407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3429 = { sizeof (TweenFunctions_t2356057452), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3430 = { sizeof (EaseType_t892987806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3430[42] = 
{
	EaseType_t892987806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3431 = { sizeof (SRReflection_t1802876238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3432 = { sizeof (ServiceAttribute_t1825329269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3432[1] = 
{
	ServiceAttribute_t1825329269::get_offset_of_U3CServiceTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3433 = { sizeof (ServiceSelectorAttribute_t1476084060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3433[1] = 
{
	ServiceSelectorAttribute_t1476084060::get_offset_of_U3CServiceTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3434 = { sizeof (ServiceConstructorAttribute_t57773005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3434[1] = 
{
	ServiceConstructorAttribute_t57773005::get_offset_of_U3CServiceTypeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3435 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3436 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3436[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3437 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3437[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3438 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3438[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3439 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3439[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3440 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3441 = { sizeof (SRServiceManager_t2267823955), -1, sizeof(SRServiceManager_t2267823955_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3441[5] = 
{
	0,
	SRServiceManager_t2267823955_StaticFields::get_offset_of_LoadingCount_10(),
	SRServiceManager_t2267823955::get_offset_of__services_11(),
	SRServiceManager_t2267823955::get_offset_of__serviceStubs_12(),
	SRServiceManager_t2267823955_StaticFields::get_offset_of__hasQuit_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3442 = { sizeof (Service_t4023205305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3442[2] = 
{
	Service_t4023205305::get_offset_of_Object_0(),
	Service_t4023205305::get_offset_of_Type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3443 = { sizeof (ServiceStub_t376495697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3443[4] = 
{
	ServiceStub_t376495697::get_offset_of_Constructor_0(),
	ServiceStub_t376495697::get_offset_of_InterfaceType_1(),
	ServiceStub_t376495697::get_offset_of_Selector_2(),
	ServiceStub_t376495697::get_offset_of_Type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3444 = { sizeof (U3CScanTypeForSelectorsU3Ec__AnonStorey0_t4068173263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3444[1] = 
{
	U3CScanTypeForSelectorsU3Ec__AnonStorey0_t4068173263::get_offset_of_attrib_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3445 = { sizeof (U3CScanTypeForConstructorsU3Ec__AnonStorey1_t3510309839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3445[2] = 
{
	U3CScanTypeForConstructorsU3Ec__AnonStorey1_t3510309839::get_offset_of_attrib_0(),
	U3CScanTypeForConstructorsU3Ec__AnonStorey1_t3510309839::get_offset_of_m_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3446 = { sizeof (ContentFitText_t3144722753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3446[2] = 
{
	ContentFitText_t3144722753::get_offset_of_CopySource_2(),
	ContentFitText_t3144722753::get_offset_of_Padding_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3447 = { sizeof (CopyLayoutElement_t859891161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3447[9] = 
{
	CopyLayoutElement_t859891161::get_offset_of_CopyMinHeight_2(),
	CopyLayoutElement_t859891161::get_offset_of_CopyMinWidth_3(),
	CopyLayoutElement_t859891161::get_offset_of_CopyPreferredHeight_4(),
	CopyLayoutElement_t859891161::get_offset_of_CopyPreferredWidth_5(),
	CopyLayoutElement_t859891161::get_offset_of_CopySource_6(),
	CopyLayoutElement_t859891161::get_offset_of_PaddingMinHeight_7(),
	CopyLayoutElement_t859891161::get_offset_of_PaddingMinWidth_8(),
	CopyLayoutElement_t859891161::get_offset_of_PaddingPreferredHeight_9(),
	CopyLayoutElement_t859891161::get_offset_of_PaddingPreferredWidth_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3448 = { sizeof (CopyPreferredSize_t2307038875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3448[3] = 
{
	CopyPreferredSize_t2307038875::get_offset_of_CopySource_9(),
	CopyPreferredSize_t2307038875::get_offset_of_PaddingHeight_10(),
	CopyPreferredSize_t2307038875::get_offset_of_PaddingWidth_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3449 = { sizeof (CopySizeIntoLayoutElement_t2725109818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3449[5] = 
{
	CopySizeIntoLayoutElement_t2725109818::get_offset_of_CopySource_9(),
	CopySizeIntoLayoutElement_t2725109818::get_offset_of_PaddingHeight_10(),
	CopySizeIntoLayoutElement_t2725109818::get_offset_of_PaddingWidth_11(),
	CopySizeIntoLayoutElement_t2725109818::get_offset_of_SetPreferredSize_12(),
	CopySizeIntoLayoutElement_t2725109818::get_offset_of_SetMinimumSize_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3450 = { sizeof (DragHandle_t2261255472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3450[8] = 
{
	DragHandle_t2261255472::get_offset_of__canvasScaler_2(),
	DragHandle_t2261255472::get_offset_of__delta_3(),
	DragHandle_t2261255472::get_offset_of__startValue_4(),
	DragHandle_t2261255472::get_offset_of_Axis_5(),
	DragHandle_t2261255472::get_offset_of_Invert_6(),
	DragHandle_t2261255472::get_offset_of_MaxSize_7(),
	DragHandle_t2261255472::get_offset_of_TargetLayoutElement_8(),
	DragHandle_t2261255472::get_offset_of_TargetRectTransform_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3451 = { sizeof (FlashGraphic_t1655109972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3451[4] = 
{
	FlashGraphic_t1655109972::get_offset_of_DecayTime_2(),
	FlashGraphic_t1655109972::get_offset_of_DefaultColor_3(),
	FlashGraphic_t1655109972::get_offset_of_FlashColor_4(),
	FlashGraphic_t1655109972::get_offset_of_Target_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3452 = { sizeof (InheritColour_t1066420259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3452[2] = 
{
	InheritColour_t1066420259::get_offset_of__graphic_8(),
	InheritColour_t1066420259::get_offset_of_From_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3453 = { sizeof (FlowLayoutGroup_t719127507), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3453[5] = 
{
	FlowLayoutGroup_t719127507::get_offset_of__rowList_10(),
	FlowLayoutGroup_t719127507::get_offset_of__layoutHeight_11(),
	FlowLayoutGroup_t719127507::get_offset_of_ChildForceExpandHeight_12(),
	FlowLayoutGroup_t719127507::get_offset_of_ChildForceExpandWidth_13(),
	FlowLayoutGroup_t719127507::get_offset_of_Spacing_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3454 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3455 = { sizeof (VirtualVerticalLayoutGroup_t4077716920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3455[19] = 
{
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__itemList_10(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__visibleItemList_11(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__isDirty_12(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__rowCache_13(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__scrollRect_14(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__selectedIndex_15(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__selectedItem_16(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__selectedItemChanged_17(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__visibleItemCount_18(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__visibleRows_19(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_AltRowStyleSheet_20(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_EnableSelection_21(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_ItemPrefab_22(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_RowPadding_23(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_RowStyleSheet_24(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_SelectedRowStyleSheet_25(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_Spacing_26(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of_StickToBottom_27(),
	VirtualVerticalLayoutGroup_t4077716920::get_offset_of__itemHeight_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3456 = { sizeof (SelectedItemChangedEvent_t4048256138), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3457 = { sizeof (Row_t725569592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3457[5] = 
{
	Row_t725569592::get_offset_of_Data_0(),
	Row_t725569592::get_offset_of_Index_1(),
	Row_t725569592::get_offset_of_Rect_2(),
	Row_t725569592::get_offset_of_Root_3(),
	Row_t725569592::get_offset_of_View_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3458 = { sizeof (LongPressButton_t1525827641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3458[5] = 
{
	LongPressButton_t1525827641::get_offset_of__handled_17(),
	LongPressButton_t1525827641::get_offset_of__onLongPress_18(),
	LongPressButton_t1525827641::get_offset_of__pressed_19(),
	LongPressButton_t1525827641::get_offset_of__pressedTime_20(),
	LongPressButton_t1525827641::get_offset_of_LongPressDuration_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3459 = { sizeof (ResponsiveBase_t1813519265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3459[1] = 
{
	ResponsiveBase_t1813519265::get_offset_of__queueRefresh_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3460 = { sizeof (ResponsiveEnable_t2173267019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3460[1] = 
{
	ResponsiveEnable_t2173267019::get_offset_of_Entries_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3461 = { sizeof (Modes_t1535981351)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3461[3] = 
{
	Modes_t1535981351::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3462 = { sizeof (Entry_t16942375)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3462[5] = 
{
	Entry_t16942375::get_offset_of_Components_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t16942375::get_offset_of_GameObjects_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t16942375::get_offset_of_Mode_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t16942375::get_offset_of_ThresholdHeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Entry_t16942375::get_offset_of_ThresholdWidth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3463 = { sizeof (ResponsiveResize_t1089670572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3463[1] = 
{
	ResponsiveResize_t1089670572::get_offset_of_Elements_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3464 = { sizeof (Element_t1410679598)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3464[2] = 
{
	Element_t1410679598::get_offset_of_SizeDefinitions_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Element_t1410679598::get_offset_of_Target_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3465 = { sizeof (SizeDefinition_t1466074441)+ sizeof (Il2CppObject), sizeof(SizeDefinition_t1466074441 ), 0, 0 };
extern const int32_t g_FieldOffsetTable3465[2] = 
{
	SizeDefinition_t1466074441::get_offset_of_ElementWidth_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SizeDefinition_t1466074441::get_offset_of_ThresholdWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3466 = { sizeof (SRNumberButton_t836980342), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3466[7] = 
{
	0,
	0,
	SRNumberButton_t836980342::get_offset_of__delayTime_19(),
	SRNumberButton_t836980342::get_offset_of__downTime_20(),
	SRNumberButton_t836980342::get_offset_of__isDown_21(),
	SRNumberButton_t836980342::get_offset_of_Amount_22(),
	SRNumberButton_t836980342::get_offset_of_TargetField_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3467 = { sizeof (SRNumberSpinner_t2685847661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3467[6] = 
{
	SRNumberSpinner_t2685847661::get_offset_of__currentValue_63(),
	SRNumberSpinner_t2685847661::get_offset_of__dragStartAmount_64(),
	SRNumberSpinner_t2685847661::get_offset_of__dragStep_65(),
	SRNumberSpinner_t2685847661::get_offset_of_DragSensitivity_66(),
	SRNumberSpinner_t2685847661::get_offset_of_MaxValue_67(),
	SRNumberSpinner_t2685847661::get_offset_of_MinValue_68(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3468 = { sizeof (SRRetinaScaler_t140471244), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3468[3] = 
{
	SRRetinaScaler_t140471244::get_offset_of__retinaScale_8(),
	SRRetinaScaler_t140471244::get_offset_of__thresholdDpi_9(),
	SRRetinaScaler_t140471244::get_offset_of__disablePixelPerfect_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3469 = { sizeof (SRSpinner_t2158684882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3469[4] = 
{
	SRSpinner_t2158684882::get_offset_of__dragDelta_16(),
	SRSpinner_t2158684882::get_offset_of__onSpinDecrement_17(),
	SRSpinner_t2158684882::get_offset_of__onSpinIncrement_18(),
	SRSpinner_t2158684882::get_offset_of_DragThreshold_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3470 = { sizeof (SpinEvent_t3474549388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3471 = { sizeof (SRText_t1495418442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3471[1] = 
{
	SRText_t1495418442::get_offset_of_LayoutDirty_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3472 = { sizeof (ScrollToBottomBehaviour_t2402064726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3472[2] = 
{
	ScrollToBottomBehaviour_t2402064726::get_offset_of__scrollRect_2(),
	ScrollToBottomBehaviour_t2402064726::get_offset_of__canvasGroup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3473 = { sizeof (StyleComponent_t3036492378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3473[8] = 
{
	StyleComponent_t3036492378::get_offset_of__activeStyle_8(),
	StyleComponent_t3036492378::get_offset_of__cachedRoot_9(),
	StyleComponent_t3036492378::get_offset_of__graphic_10(),
	StyleComponent_t3036492378::get_offset_of__hasStarted_11(),
	StyleComponent_t3036492378::get_offset_of__image_12(),
	StyleComponent_t3036492378::get_offset_of__selectable_13(),
	StyleComponent_t3036492378::get_offset_of__styleKey_14(),
	StyleComponent_t3036492378::get_offset_of_IgnoreImage_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3474 = { sizeof (StyleRoot_t1691968825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3474[2] = 
{
	StyleRoot_t1691968825::get_offset_of__activeStyleSheet_8(),
	StyleRoot_t1691968825::get_offset_of_StyleSheet_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3475 = { sizeof (Style_t1578998053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3475[5] = 
{
	Style_t1578998053::get_offset_of_ActiveColor_0(),
	Style_t1578998053::get_offset_of_DisabledColor_1(),
	Style_t1578998053::get_offset_of_HoverColor_2(),
	Style_t1578998053::get_offset_of_Image_3(),
	Style_t1578998053::get_offset_of_NormalColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3476 = { sizeof (StyleSheet_t2708161410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3476[3] = 
{
	StyleSheet_t2708161410::get_offset_of__keys_2(),
	StyleSheet_t2708161410::get_offset_of__styles_3(),
	StyleSheet_t2708161410::get_offset_of_Parent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3477 = { sizeof (Unselectable_t2595244145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3477[1] = 
{
	Unselectable_t2595244145::get_offset_of__suspectedSelected_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3478 = { sizeof (u2dexGrid_t3875635992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3478[6] = 
{
	u2dexGrid_t3875635992::get_offset_of_gridSize_2(),
	u2dexGrid_t3875635992::get_offset_of_color_3(),
	u2dexGrid_t3875635992::get_offset_of_gridScale_4(),
	u2dexGrid_t3875635992::get_offset_of_lineLength_5(),
	0,
	u2dexGrid_t3875635992::get_offset_of_maximumDistance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3479 = { sizeof (GridScale_t3851649610)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3479[8] = 
{
	GridScale_t3851649610::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3480 = { sizeof (ArrayBoolAllTrue_t954317994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3480[4] = 
{
	ArrayBoolAllTrue_t954317994::get_offset_of_boolArray_11(),
	ArrayBoolAllTrue_t954317994::get_offset_of_sendEvent_12(),
	ArrayBoolAllTrue_t954317994::get_offset_of_storeResult_13(),
	ArrayBoolAllTrue_t954317994::get_offset_of_everyFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3481 = { sizeof (ArrayCopy_t509737366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3481[2] = 
{
	ArrayCopy_t509737366::get_offset_of_array_11(),
	ArrayCopy_t509737366::get_offset_of_storeArray_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3482 = { sizeof (Collider2DInstersect_t1896095612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3482[5] = 
{
	Collider2DInstersect_t1896095612::get_offset_of_gameObject_11(),
	Collider2DInstersect_t1896095612::get_offset_of_target_12(),
	Collider2DInstersect_t1896095612::get_offset_of_insideCollider_13(),
	Collider2DInstersect_t1896095612::get_offset_of_insideEvent_14(),
	Collider2DInstersect_t1896095612::get_offset_of_outsideEvent_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3483 = { sizeof (FloatAddValue_t3788524128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3483[5] = 
{
	FloatAddValue_t3788524128::get_offset_of_floatVariable_11(),
	FloatAddValue_t3788524128::get_offset_of_add_12(),
	FloatAddValue_t3788524128::get_offset_of_addValue_13(),
	FloatAddValue_t3788524128::get_offset_of_everyFrame_14(),
	FloatAddValue_t3788524128::get_offset_of_perSecond_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3484 = { sizeof (FloatSubtractValue_t515012733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3484[5] = 
{
	FloatSubtractValue_t515012733::get_offset_of_floatVariable_11(),
	FloatSubtractValue_t515012733::get_offset_of_subtract_12(),
	FloatSubtractValue_t515012733::get_offset_of_subtractValue_13(),
	FloatSubtractValue_t515012733::get_offset_of_everyFrame_14(),
	FloatSubtractValue_t515012733::get_offset_of_perSecond_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3485 = { sizeof (IntAddValue_t592117905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3485[4] = 
{
	IntAddValue_t592117905::get_offset_of_intVariable_11(),
	IntAddValue_t592117905::get_offset_of_add_12(),
	IntAddValue_t592117905::get_offset_of_intValue_13(),
	IntAddValue_t592117905::get_offset_of_everyFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3486 = { sizeof (IntSubtractValue_t3364028834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3486[6] = 
{
	IntSubtractValue_t3364028834::get_offset_of_intVariable_11(),
	IntSubtractValue_t3364028834::get_offset_of_subtract_12(),
	IntSubtractValue_t3364028834::get_offset_of_subtractValue_13(),
	IntSubtractValue_t3364028834::get_offset_of_everyFrame_14(),
	IntSubtractValue_t3364028834::get_offset_of_perSecond_15(),
	IntSubtractValue_t3364028834::get_offset_of__acc_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3487 = { sizeof (ScreenPointToRay_t3519220847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3487[10] = 
{
	ScreenPointToRay_t3519220847::get_offset_of_screenVector_11(),
	ScreenPointToRay_t3519220847::get_offset_of_screenX_12(),
	ScreenPointToRay_t3519220847::get_offset_of_screenY_13(),
	ScreenPointToRay_t3519220847::get_offset_of_screenZ_14(),
	ScreenPointToRay_t3519220847::get_offset_of_normalized_15(),
	ScreenPointToRay_t3519220847::get_offset_of_storeWorldVector_16(),
	ScreenPointToRay_t3519220847::get_offset_of_storeWorldX_17(),
	ScreenPointToRay_t3519220847::get_offset_of_storeWorldY_18(),
	ScreenPointToRay_t3519220847::get_offset_of_storeWorldZ_19(),
	ScreenPointToRay_t3519220847::get_offset_of_everyFrame_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3488 = { sizeof (Vector2SubtractValue_t2559142310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3488[4] = 
{
	Vector2SubtractValue_t2559142310::get_offset_of_vector2Variable_11(),
	Vector2SubtractValue_t2559142310::get_offset_of_subtractVector_12(),
	Vector2SubtractValue_t2559142310::get_offset_of_storeVector_13(),
	Vector2SubtractValue_t2559142310::get_offset_of_everyFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3489 = { sizeof (Vector3RoundToNearest_t269491953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3489[4] = 
{
	Vector3RoundToNearest_t269491953::get_offset_of_vector3Variable_11(),
	Vector3RoundToNearest_t269491953::get_offset_of_nearest_12(),
	Vector3RoundToNearest_t269491953::get_offset_of_resultAsVector3_13(),
	Vector3RoundToNearest_t269491953::get_offset_of_everyFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3490 = { sizeof (Vector3SubtractValue_t1113646055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3490[4] = 
{
	Vector3SubtractValue_t1113646055::get_offset_of_vector3Variable_11(),
	Vector3SubtractValue_t1113646055::get_offset_of_subtractVector_12(),
	Vector3SubtractValue_t1113646055::get_offset_of_storeVector_13(),
	Vector3SubtractValue_t1113646055::get_offset_of_everyFrame_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3491 = { sizeof (WaitValue_t2317729974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3491[5] = 
{
	WaitValue_t2317729974::get_offset_of_time_11(),
	WaitValue_t2317729974::get_offset_of_finishEvent_12(),
	WaitValue_t2317729974::get_offset_of_realTime_13(),
	WaitValue_t2317729974::get_offset_of_startTime_14(),
	WaitValue_t2317729974::get_offset_of_timer_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3492 = { sizeof (iTween_t488923914), -1, sizeof(iTween_t488923914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3492[38] = 
{
	iTween_t488923914_StaticFields::get_offset_of_tweens_2(),
	iTween_t488923914_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t488923914::get_offset_of_id_4(),
	iTween_t488923914::get_offset_of_type_5(),
	iTween_t488923914::get_offset_of_method_6(),
	iTween_t488923914::get_offset_of_easeType_7(),
	iTween_t488923914::get_offset_of_time_8(),
	iTween_t488923914::get_offset_of_delay_9(),
	iTween_t488923914::get_offset_of_loopType_10(),
	iTween_t488923914::get_offset_of_isRunning_11(),
	iTween_t488923914::get_offset_of_isPaused_12(),
	iTween_t488923914::get_offset_of__name_13(),
	iTween_t488923914::get_offset_of_runningTime_14(),
	iTween_t488923914::get_offset_of_percentage_15(),
	iTween_t488923914::get_offset_of_delayStarted_16(),
	iTween_t488923914::get_offset_of_kinematic_17(),
	iTween_t488923914::get_offset_of_isLocal_18(),
	iTween_t488923914::get_offset_of_loop_19(),
	iTween_t488923914::get_offset_of_reverse_20(),
	iTween_t488923914::get_offset_of_wasPaused_21(),
	iTween_t488923914::get_offset_of_physics_22(),
	iTween_t488923914::get_offset_of_tweenArguments_23(),
	iTween_t488923914::get_offset_of_space_24(),
	iTween_t488923914::get_offset_of_ease_25(),
	iTween_t488923914::get_offset_of_apply_26(),
	iTween_t488923914::get_offset_of_audioSource_27(),
	iTween_t488923914::get_offset_of_vector3s_28(),
	iTween_t488923914::get_offset_of_vector2s_29(),
	iTween_t488923914::get_offset_of_colors_30(),
	iTween_t488923914::get_offset_of_floats_31(),
	iTween_t488923914::get_offset_of_rects_32(),
	iTween_t488923914::get_offset_of_path_33(),
	iTween_t488923914::get_offset_of_preUpdate_34(),
	iTween_t488923914::get_offset_of_postUpdate_35(),
	iTween_t488923914::get_offset_of_namedcolorvalue_36(),
	iTween_t488923914::get_offset_of_lastRealTime_37(),
	iTween_t488923914::get_offset_of_useRealTime_38(),
	iTween_t488923914_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3493 = { sizeof (EasingFunction_t3676968174), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3494 = { sizeof (ApplyTween_t747394300), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3495 = { sizeof (EaseType_t818674011)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3495[34] = 
{
	EaseType_t818674011::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3496 = { sizeof (LoopType_t1490651981)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3496[4] = 
{
	LoopType_t1490651981::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3497 = { sizeof (NamedValueColor_t2874784184)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3497[5] = 
{
	NamedValueColor_t2874784184::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3498 = { sizeof (Defaults_t4204852305), -1, sizeof(Defaults_t4204852305_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3498[16] = 
{
	Defaults_t4204852305_StaticFields::get_offset_of_time_0(),
	Defaults_t4204852305_StaticFields::get_offset_of_delay_1(),
	Defaults_t4204852305_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t4204852305_StaticFields::get_offset_of_loopType_3(),
	Defaults_t4204852305_StaticFields::get_offset_of_easeType_4(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t4204852305_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t4204852305_StaticFields::get_offset_of_space_7(),
	Defaults_t4204852305_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t4204852305_StaticFields::get_offset_of_color_9(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t4204852305_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t4204852305_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t4204852305_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t4204852305_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t4204852305_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3499 = { sizeof (CRSpline_t4177960625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3499[1] = 
{
	CRSpline_t4177960625::get_offset_of_pts_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
