﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ControlUIEvent
struct  ControlUIEvent_t1830266849  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ControlUIEvent::moveStartText
	Text_t356221433 * ___moveStartText_2;
	// UnityEngine.UI.Text ControlUIEvent::moveText
	Text_t356221433 * ___moveText_3;
	// UnityEngine.UI.Text ControlUIEvent::moveSpeedText
	Text_t356221433 * ___moveSpeedText_4;
	// UnityEngine.UI.Text ControlUIEvent::moveEndText
	Text_t356221433 * ___moveEndText_5;
	// UnityEngine.UI.Text ControlUIEvent::touchStartText
	Text_t356221433 * ___touchStartText_6;
	// UnityEngine.UI.Text ControlUIEvent::touchUpText
	Text_t356221433 * ___touchUpText_7;
	// UnityEngine.UI.Text ControlUIEvent::downRightText
	Text_t356221433 * ___downRightText_8;
	// UnityEngine.UI.Text ControlUIEvent::downDownText
	Text_t356221433 * ___downDownText_9;
	// UnityEngine.UI.Text ControlUIEvent::downLeftText
	Text_t356221433 * ___downLeftText_10;
	// UnityEngine.UI.Text ControlUIEvent::downUpText
	Text_t356221433 * ___downUpText_11;
	// UnityEngine.UI.Text ControlUIEvent::rightText
	Text_t356221433 * ___rightText_12;
	// UnityEngine.UI.Text ControlUIEvent::downText
	Text_t356221433 * ___downText_13;
	// UnityEngine.UI.Text ControlUIEvent::leftText
	Text_t356221433 * ___leftText_14;
	// UnityEngine.UI.Text ControlUIEvent::upText
	Text_t356221433 * ___upText_15;
	// System.Boolean ControlUIEvent::isDown
	bool ___isDown_16;
	// System.Boolean ControlUIEvent::isLeft
	bool ___isLeft_17;
	// System.Boolean ControlUIEvent::isUp
	bool ___isUp_18;
	// System.Boolean ControlUIEvent::isRight
	bool ___isRight_19;

public:
	inline static int32_t get_offset_of_moveStartText_2() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___moveStartText_2)); }
	inline Text_t356221433 * get_moveStartText_2() const { return ___moveStartText_2; }
	inline Text_t356221433 ** get_address_of_moveStartText_2() { return &___moveStartText_2; }
	inline void set_moveStartText_2(Text_t356221433 * value)
	{
		___moveStartText_2 = value;
		Il2CppCodeGenWriteBarrier(&___moveStartText_2, value);
	}

	inline static int32_t get_offset_of_moveText_3() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___moveText_3)); }
	inline Text_t356221433 * get_moveText_3() const { return ___moveText_3; }
	inline Text_t356221433 ** get_address_of_moveText_3() { return &___moveText_3; }
	inline void set_moveText_3(Text_t356221433 * value)
	{
		___moveText_3 = value;
		Il2CppCodeGenWriteBarrier(&___moveText_3, value);
	}

	inline static int32_t get_offset_of_moveSpeedText_4() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___moveSpeedText_4)); }
	inline Text_t356221433 * get_moveSpeedText_4() const { return ___moveSpeedText_4; }
	inline Text_t356221433 ** get_address_of_moveSpeedText_4() { return &___moveSpeedText_4; }
	inline void set_moveSpeedText_4(Text_t356221433 * value)
	{
		___moveSpeedText_4 = value;
		Il2CppCodeGenWriteBarrier(&___moveSpeedText_4, value);
	}

	inline static int32_t get_offset_of_moveEndText_5() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___moveEndText_5)); }
	inline Text_t356221433 * get_moveEndText_5() const { return ___moveEndText_5; }
	inline Text_t356221433 ** get_address_of_moveEndText_5() { return &___moveEndText_5; }
	inline void set_moveEndText_5(Text_t356221433 * value)
	{
		___moveEndText_5 = value;
		Il2CppCodeGenWriteBarrier(&___moveEndText_5, value);
	}

	inline static int32_t get_offset_of_touchStartText_6() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___touchStartText_6)); }
	inline Text_t356221433 * get_touchStartText_6() const { return ___touchStartText_6; }
	inline Text_t356221433 ** get_address_of_touchStartText_6() { return &___touchStartText_6; }
	inline void set_touchStartText_6(Text_t356221433 * value)
	{
		___touchStartText_6 = value;
		Il2CppCodeGenWriteBarrier(&___touchStartText_6, value);
	}

	inline static int32_t get_offset_of_touchUpText_7() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___touchUpText_7)); }
	inline Text_t356221433 * get_touchUpText_7() const { return ___touchUpText_7; }
	inline Text_t356221433 ** get_address_of_touchUpText_7() { return &___touchUpText_7; }
	inline void set_touchUpText_7(Text_t356221433 * value)
	{
		___touchUpText_7 = value;
		Il2CppCodeGenWriteBarrier(&___touchUpText_7, value);
	}

	inline static int32_t get_offset_of_downRightText_8() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___downRightText_8)); }
	inline Text_t356221433 * get_downRightText_8() const { return ___downRightText_8; }
	inline Text_t356221433 ** get_address_of_downRightText_8() { return &___downRightText_8; }
	inline void set_downRightText_8(Text_t356221433 * value)
	{
		___downRightText_8 = value;
		Il2CppCodeGenWriteBarrier(&___downRightText_8, value);
	}

	inline static int32_t get_offset_of_downDownText_9() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___downDownText_9)); }
	inline Text_t356221433 * get_downDownText_9() const { return ___downDownText_9; }
	inline Text_t356221433 ** get_address_of_downDownText_9() { return &___downDownText_9; }
	inline void set_downDownText_9(Text_t356221433 * value)
	{
		___downDownText_9 = value;
		Il2CppCodeGenWriteBarrier(&___downDownText_9, value);
	}

	inline static int32_t get_offset_of_downLeftText_10() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___downLeftText_10)); }
	inline Text_t356221433 * get_downLeftText_10() const { return ___downLeftText_10; }
	inline Text_t356221433 ** get_address_of_downLeftText_10() { return &___downLeftText_10; }
	inline void set_downLeftText_10(Text_t356221433 * value)
	{
		___downLeftText_10 = value;
		Il2CppCodeGenWriteBarrier(&___downLeftText_10, value);
	}

	inline static int32_t get_offset_of_downUpText_11() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___downUpText_11)); }
	inline Text_t356221433 * get_downUpText_11() const { return ___downUpText_11; }
	inline Text_t356221433 ** get_address_of_downUpText_11() { return &___downUpText_11; }
	inline void set_downUpText_11(Text_t356221433 * value)
	{
		___downUpText_11 = value;
		Il2CppCodeGenWriteBarrier(&___downUpText_11, value);
	}

	inline static int32_t get_offset_of_rightText_12() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___rightText_12)); }
	inline Text_t356221433 * get_rightText_12() const { return ___rightText_12; }
	inline Text_t356221433 ** get_address_of_rightText_12() { return &___rightText_12; }
	inline void set_rightText_12(Text_t356221433 * value)
	{
		___rightText_12 = value;
		Il2CppCodeGenWriteBarrier(&___rightText_12, value);
	}

	inline static int32_t get_offset_of_downText_13() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___downText_13)); }
	inline Text_t356221433 * get_downText_13() const { return ___downText_13; }
	inline Text_t356221433 ** get_address_of_downText_13() { return &___downText_13; }
	inline void set_downText_13(Text_t356221433 * value)
	{
		___downText_13 = value;
		Il2CppCodeGenWriteBarrier(&___downText_13, value);
	}

	inline static int32_t get_offset_of_leftText_14() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___leftText_14)); }
	inline Text_t356221433 * get_leftText_14() const { return ___leftText_14; }
	inline Text_t356221433 ** get_address_of_leftText_14() { return &___leftText_14; }
	inline void set_leftText_14(Text_t356221433 * value)
	{
		___leftText_14 = value;
		Il2CppCodeGenWriteBarrier(&___leftText_14, value);
	}

	inline static int32_t get_offset_of_upText_15() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___upText_15)); }
	inline Text_t356221433 * get_upText_15() const { return ___upText_15; }
	inline Text_t356221433 ** get_address_of_upText_15() { return &___upText_15; }
	inline void set_upText_15(Text_t356221433 * value)
	{
		___upText_15 = value;
		Il2CppCodeGenWriteBarrier(&___upText_15, value);
	}

	inline static int32_t get_offset_of_isDown_16() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___isDown_16)); }
	inline bool get_isDown_16() const { return ___isDown_16; }
	inline bool* get_address_of_isDown_16() { return &___isDown_16; }
	inline void set_isDown_16(bool value)
	{
		___isDown_16 = value;
	}

	inline static int32_t get_offset_of_isLeft_17() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___isLeft_17)); }
	inline bool get_isLeft_17() const { return ___isLeft_17; }
	inline bool* get_address_of_isLeft_17() { return &___isLeft_17; }
	inline void set_isLeft_17(bool value)
	{
		___isLeft_17 = value;
	}

	inline static int32_t get_offset_of_isUp_18() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___isUp_18)); }
	inline bool get_isUp_18() const { return ___isUp_18; }
	inline bool* get_address_of_isUp_18() { return &___isUp_18; }
	inline void set_isUp_18(bool value)
	{
		___isUp_18 = value;
	}

	inline static int32_t get_offset_of_isRight_19() { return static_cast<int32_t>(offsetof(ControlUIEvent_t1830266849, ___isRight_19)); }
	inline bool get_isRight_19() const { return ___isRight_19; }
	inline bool* get_address_of_isRight_19() { return &___isRight_19; }
	inline void set_isRight_19(bool value)
	{
		___isRight_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
