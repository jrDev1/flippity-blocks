﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmObject
struct FsmObject_t2785794313;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtCreatePool
struct  PmtCreatePool_t809695595  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtCreatePool::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PmtCreatePool::dontDestroyOnLoad
	FsmBool_t664485696 * ___dontDestroyOnLoad_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PmtCreatePool::logMessages
	FsmBool_t664485696 * ___logMessages_13;
	// HutongGames.PlayMaker.FsmObject HutongGames.PlayMaker.Actions.PmtCreatePool::pool
	FsmObject_t2785794313 * ___pool_14;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtCreatePool::poolGameObject
	FsmGameObject_t3097142863 * ___poolGameObject_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PmtCreatePool::poolExistsAlready
	FsmBool_t664485696 * ___poolExistsAlready_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCreatePool::poolExistsAlreadyEvent
	FsmEvent_t1258573736 * ___poolExistsAlreadyEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCreatePool::poolCreatedEvent
	FsmEvent_t1258573736 * ___poolCreatedEvent_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCreatePool::poolCreationFailedEvent
	FsmEvent_t1258573736 * ___poolCreationFailedEvent_19;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_dontDestroyOnLoad_12() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___dontDestroyOnLoad_12)); }
	inline FsmBool_t664485696 * get_dontDestroyOnLoad_12() const { return ___dontDestroyOnLoad_12; }
	inline FsmBool_t664485696 ** get_address_of_dontDestroyOnLoad_12() { return &___dontDestroyOnLoad_12; }
	inline void set_dontDestroyOnLoad_12(FsmBool_t664485696 * value)
	{
		___dontDestroyOnLoad_12 = value;
		Il2CppCodeGenWriteBarrier(&___dontDestroyOnLoad_12, value);
	}

	inline static int32_t get_offset_of_logMessages_13() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___logMessages_13)); }
	inline FsmBool_t664485696 * get_logMessages_13() const { return ___logMessages_13; }
	inline FsmBool_t664485696 ** get_address_of_logMessages_13() { return &___logMessages_13; }
	inline void set_logMessages_13(FsmBool_t664485696 * value)
	{
		___logMessages_13 = value;
		Il2CppCodeGenWriteBarrier(&___logMessages_13, value);
	}

	inline static int32_t get_offset_of_pool_14() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___pool_14)); }
	inline FsmObject_t2785794313 * get_pool_14() const { return ___pool_14; }
	inline FsmObject_t2785794313 ** get_address_of_pool_14() { return &___pool_14; }
	inline void set_pool_14(FsmObject_t2785794313 * value)
	{
		___pool_14 = value;
		Il2CppCodeGenWriteBarrier(&___pool_14, value);
	}

	inline static int32_t get_offset_of_poolGameObject_15() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___poolGameObject_15)); }
	inline FsmGameObject_t3097142863 * get_poolGameObject_15() const { return ___poolGameObject_15; }
	inline FsmGameObject_t3097142863 ** get_address_of_poolGameObject_15() { return &___poolGameObject_15; }
	inline void set_poolGameObject_15(FsmGameObject_t3097142863 * value)
	{
		___poolGameObject_15 = value;
		Il2CppCodeGenWriteBarrier(&___poolGameObject_15, value);
	}

	inline static int32_t get_offset_of_poolExistsAlready_16() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___poolExistsAlready_16)); }
	inline FsmBool_t664485696 * get_poolExistsAlready_16() const { return ___poolExistsAlready_16; }
	inline FsmBool_t664485696 ** get_address_of_poolExistsAlready_16() { return &___poolExistsAlready_16; }
	inline void set_poolExistsAlready_16(FsmBool_t664485696 * value)
	{
		___poolExistsAlready_16 = value;
		Il2CppCodeGenWriteBarrier(&___poolExistsAlready_16, value);
	}

	inline static int32_t get_offset_of_poolExistsAlreadyEvent_17() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___poolExistsAlreadyEvent_17)); }
	inline FsmEvent_t1258573736 * get_poolExistsAlreadyEvent_17() const { return ___poolExistsAlreadyEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_poolExistsAlreadyEvent_17() { return &___poolExistsAlreadyEvent_17; }
	inline void set_poolExistsAlreadyEvent_17(FsmEvent_t1258573736 * value)
	{
		___poolExistsAlreadyEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___poolExistsAlreadyEvent_17, value);
	}

	inline static int32_t get_offset_of_poolCreatedEvent_18() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___poolCreatedEvent_18)); }
	inline FsmEvent_t1258573736 * get_poolCreatedEvent_18() const { return ___poolCreatedEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_poolCreatedEvent_18() { return &___poolCreatedEvent_18; }
	inline void set_poolCreatedEvent_18(FsmEvent_t1258573736 * value)
	{
		___poolCreatedEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___poolCreatedEvent_18, value);
	}

	inline static int32_t get_offset_of_poolCreationFailedEvent_19() { return static_cast<int32_t>(offsetof(PmtCreatePool_t809695595, ___poolCreationFailedEvent_19)); }
	inline FsmEvent_t1258573736 * get_poolCreationFailedEvent_19() const { return ___poolCreationFailedEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_poolCreationFailedEvent_19() { return &___poolCreationFailedEvent_19; }
	inline void set_poolCreationFailedEvent_19(FsmEvent_t1258573736 * value)
	{
		___poolCreationFailedEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___poolCreationFailedEvent_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
