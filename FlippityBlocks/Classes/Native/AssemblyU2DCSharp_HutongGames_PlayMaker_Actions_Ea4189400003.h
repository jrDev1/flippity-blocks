﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchNormalizedPosition
struct  EasyTouchNormalizedPosition_t4189400003  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchNormalizedPosition::normalizedPosition
	FsmVector3_t3996534004 * ___normalizedPosition_11;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchNormalizedPosition::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_12;

public:
	inline static int32_t get_offset_of_normalizedPosition_11() { return static_cast<int32_t>(offsetof(EasyTouchNormalizedPosition_t4189400003, ___normalizedPosition_11)); }
	inline FsmVector3_t3996534004 * get_normalizedPosition_11() const { return ___normalizedPosition_11; }
	inline FsmVector3_t3996534004 ** get_address_of_normalizedPosition_11() { return &___normalizedPosition_11; }
	inline void set_normalizedPosition_11(FsmVector3_t3996534004 * value)
	{
		___normalizedPosition_11 = value;
		Il2CppCodeGenWriteBarrier(&___normalizedPosition_11, value);
	}

	inline static int32_t get_offset_of_proxy_12() { return static_cast<int32_t>(offsetof(EasyTouchNormalizedPosition_t4189400003, ___proxy_12)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_12() const { return ___proxy_12; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_12() { return &___proxy_12; }
	inline void set_proxy_12(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_12 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
