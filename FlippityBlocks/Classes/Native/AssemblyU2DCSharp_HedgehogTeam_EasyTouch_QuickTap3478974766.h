﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickTap_3459534638.h"

// HedgehogTeam.EasyTouch.QuickTap/OnTap
struct OnTap_t533982918;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickTap
struct  QuickTap_t3478974766  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickTap/OnTap HedgehogTeam.EasyTouch.QuickTap::onTap
	OnTap_t533982918 * ___onTap_19;
	// HedgehogTeam.EasyTouch.QuickTap/ActionTriggering HedgehogTeam.EasyTouch.QuickTap::actionTriggering
	int32_t ___actionTriggering_20;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickTap::currentGesture
	Gesture_t367995397 * ___currentGesture_21;

public:
	inline static int32_t get_offset_of_onTap_19() { return static_cast<int32_t>(offsetof(QuickTap_t3478974766, ___onTap_19)); }
	inline OnTap_t533982918 * get_onTap_19() const { return ___onTap_19; }
	inline OnTap_t533982918 ** get_address_of_onTap_19() { return &___onTap_19; }
	inline void set_onTap_19(OnTap_t533982918 * value)
	{
		___onTap_19 = value;
		Il2CppCodeGenWriteBarrier(&___onTap_19, value);
	}

	inline static int32_t get_offset_of_actionTriggering_20() { return static_cast<int32_t>(offsetof(QuickTap_t3478974766, ___actionTriggering_20)); }
	inline int32_t get_actionTriggering_20() const { return ___actionTriggering_20; }
	inline int32_t* get_address_of_actionTriggering_20() { return &___actionTriggering_20; }
	inline void set_actionTriggering_20(int32_t value)
	{
		___actionTriggering_20 = value;
	}

	inline static int32_t get_offset_of_currentGesture_21() { return static_cast<int32_t>(offsetof(QuickTap_t3478974766, ___currentGesture_21)); }
	inline Gesture_t367995397 * get_currentGesture_21() const { return ___currentGesture_21; }
	inline Gesture_t367995397 ** get_address_of_currentGesture_21() { return &___currentGesture_21; }
	inline void set_currentGesture_21(Gesture_t367995397 * value)
	{
		___currentGesture_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentGesture_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
