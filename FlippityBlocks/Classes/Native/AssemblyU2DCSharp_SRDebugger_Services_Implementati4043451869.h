﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen3119360721.h"

// System.Collections.Generic.List`1<SRDebugger.UI.Controls.OptionsControlBase>
struct List_1_t2852422836;
// System.Collections.Generic.Dictionary`2<SRDebugger.Internal.OptionDefinition,SRDebugger.UI.Controls.OptionsControlBase>
struct Dictionary_2_t1438329295;
// SRDebugger.UI.Other.PinnedUIRoot
struct PinnedUIRoot_t3479234366;
// System.Action`2<SRDebugger.Internal.OptionDefinition,System.Boolean>
struct Action_2_t2071144739;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.PinnedUIServiceImpl
struct  PinnedUIServiceImpl_t4043451869  : public SRServiceBase_1_t3119360721
{
public:
	// System.Collections.Generic.List`1<SRDebugger.UI.Controls.OptionsControlBase> SRDebugger.Services.Implementation.PinnedUIServiceImpl::_controlList
	List_1_t2852422836 * ____controlList_9;
	// System.Collections.Generic.Dictionary`2<SRDebugger.Internal.OptionDefinition,SRDebugger.UI.Controls.OptionsControlBase> SRDebugger.Services.Implementation.PinnedUIServiceImpl::_pinnedObjects
	Dictionary_2_t1438329295 * ____pinnedObjects_10;
	// System.Boolean SRDebugger.Services.Implementation.PinnedUIServiceImpl::_queueRefresh
	bool ____queueRefresh_11;
	// SRDebugger.UI.Other.PinnedUIRoot SRDebugger.Services.Implementation.PinnedUIServiceImpl::_uiRoot
	PinnedUIRoot_t3479234366 * ____uiRoot_12;
	// System.Action`2<SRDebugger.Internal.OptionDefinition,System.Boolean> SRDebugger.Services.Implementation.PinnedUIServiceImpl::OptionPinStateChanged
	Action_2_t2071144739 * ___OptionPinStateChanged_13;

public:
	inline static int32_t get_offset_of__controlList_9() { return static_cast<int32_t>(offsetof(PinnedUIServiceImpl_t4043451869, ____controlList_9)); }
	inline List_1_t2852422836 * get__controlList_9() const { return ____controlList_9; }
	inline List_1_t2852422836 ** get_address_of__controlList_9() { return &____controlList_9; }
	inline void set__controlList_9(List_1_t2852422836 * value)
	{
		____controlList_9 = value;
		Il2CppCodeGenWriteBarrier(&____controlList_9, value);
	}

	inline static int32_t get_offset_of__pinnedObjects_10() { return static_cast<int32_t>(offsetof(PinnedUIServiceImpl_t4043451869, ____pinnedObjects_10)); }
	inline Dictionary_2_t1438329295 * get__pinnedObjects_10() const { return ____pinnedObjects_10; }
	inline Dictionary_2_t1438329295 ** get_address_of__pinnedObjects_10() { return &____pinnedObjects_10; }
	inline void set__pinnedObjects_10(Dictionary_2_t1438329295 * value)
	{
		____pinnedObjects_10 = value;
		Il2CppCodeGenWriteBarrier(&____pinnedObjects_10, value);
	}

	inline static int32_t get_offset_of__queueRefresh_11() { return static_cast<int32_t>(offsetof(PinnedUIServiceImpl_t4043451869, ____queueRefresh_11)); }
	inline bool get__queueRefresh_11() const { return ____queueRefresh_11; }
	inline bool* get_address_of__queueRefresh_11() { return &____queueRefresh_11; }
	inline void set__queueRefresh_11(bool value)
	{
		____queueRefresh_11 = value;
	}

	inline static int32_t get_offset_of__uiRoot_12() { return static_cast<int32_t>(offsetof(PinnedUIServiceImpl_t4043451869, ____uiRoot_12)); }
	inline PinnedUIRoot_t3479234366 * get__uiRoot_12() const { return ____uiRoot_12; }
	inline PinnedUIRoot_t3479234366 ** get_address_of__uiRoot_12() { return &____uiRoot_12; }
	inline void set__uiRoot_12(PinnedUIRoot_t3479234366 * value)
	{
		____uiRoot_12 = value;
		Il2CppCodeGenWriteBarrier(&____uiRoot_12, value);
	}

	inline static int32_t get_offset_of_OptionPinStateChanged_13() { return static_cast<int32_t>(offsetof(PinnedUIServiceImpl_t4043451869, ___OptionPinStateChanged_13)); }
	inline Action_2_t2071144739 * get_OptionPinStateChanged_13() const { return ___OptionPinStateChanged_13; }
	inline Action_2_t2071144739 ** get_address_of_OptionPinStateChanged_13() { return &___OptionPinStateChanged_13; }
	inline void set_OptionPinStateChanged_13(Action_2_t2071144739 * value)
	{
		___OptionPinStateChanged_13 = value;
		Il2CppCodeGenWriteBarrier(&___OptionPinStateChanged_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
