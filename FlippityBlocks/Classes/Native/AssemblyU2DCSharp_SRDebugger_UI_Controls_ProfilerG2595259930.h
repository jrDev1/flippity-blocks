﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ProfilerGraphAxisLabel
struct  ProfilerGraphAxisLabel_t2595259930  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Single SRDebugger.UI.Controls.ProfilerGraphAxisLabel::_prevFrameTime
	float ____prevFrameTime_9;
	// System.Nullable`1<System.Single> SRDebugger.UI.Controls.ProfilerGraphAxisLabel::_queuedFrameTime
	Nullable_1_t339576247  ____queuedFrameTime_10;
	// System.Single SRDebugger.UI.Controls.ProfilerGraphAxisLabel::_yPosition
	float ____yPosition_11;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ProfilerGraphAxisLabel::Text
	Text_t356221433 * ___Text_12;

public:
	inline static int32_t get_offset_of__prevFrameTime_9() { return static_cast<int32_t>(offsetof(ProfilerGraphAxisLabel_t2595259930, ____prevFrameTime_9)); }
	inline float get__prevFrameTime_9() const { return ____prevFrameTime_9; }
	inline float* get_address_of__prevFrameTime_9() { return &____prevFrameTime_9; }
	inline void set__prevFrameTime_9(float value)
	{
		____prevFrameTime_9 = value;
	}

	inline static int32_t get_offset_of__queuedFrameTime_10() { return static_cast<int32_t>(offsetof(ProfilerGraphAxisLabel_t2595259930, ____queuedFrameTime_10)); }
	inline Nullable_1_t339576247  get__queuedFrameTime_10() const { return ____queuedFrameTime_10; }
	inline Nullable_1_t339576247 * get_address_of__queuedFrameTime_10() { return &____queuedFrameTime_10; }
	inline void set__queuedFrameTime_10(Nullable_1_t339576247  value)
	{
		____queuedFrameTime_10 = value;
	}

	inline static int32_t get_offset_of__yPosition_11() { return static_cast<int32_t>(offsetof(ProfilerGraphAxisLabel_t2595259930, ____yPosition_11)); }
	inline float get__yPosition_11() const { return ____yPosition_11; }
	inline float* get_address_of__yPosition_11() { return &____yPosition_11; }
	inline void set__yPosition_11(float value)
	{
		____yPosition_11 = value;
	}

	inline static int32_t get_offset_of_Text_12() { return static_cast<int32_t>(offsetof(ProfilerGraphAxisLabel_t2595259930, ___Text_12)); }
	inline Text_t356221433 * get_Text_12() const { return ___Text_12; }
	inline Text_t356221433 ** get_address_of_Text_12() { return &___Text_12; }
	inline void set_Text_12(Text_t356221433 * value)
	{
		___Text_12 = value;
		Il2CppCodeGenWriteBarrier(&___Text_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
