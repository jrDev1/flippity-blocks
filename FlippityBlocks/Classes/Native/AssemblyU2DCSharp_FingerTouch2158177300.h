﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.TextMesh
struct TextMesh_t1641806576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FingerTouch
struct  FingerTouch_t2158177300  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh FingerTouch::textMesh
	TextMesh_t1641806576 * ___textMesh_2;
	// UnityEngine.Vector3 FingerTouch::deltaPosition
	Vector3_t2243707580  ___deltaPosition_3;
	// System.Int32 FingerTouch::fingerId
	int32_t ___fingerId_4;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(FingerTouch_t2158177300, ___textMesh_2)); }
	inline TextMesh_t1641806576 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1641806576 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_2, value);
	}

	inline static int32_t get_offset_of_deltaPosition_3() { return static_cast<int32_t>(offsetof(FingerTouch_t2158177300, ___deltaPosition_3)); }
	inline Vector3_t2243707580  get_deltaPosition_3() const { return ___deltaPosition_3; }
	inline Vector3_t2243707580 * get_address_of_deltaPosition_3() { return &___deltaPosition_3; }
	inline void set_deltaPosition_3(Vector3_t2243707580  value)
	{
		___deltaPosition_3 = value;
	}

	inline static int32_t get_offset_of_fingerId_4() { return static_cast<int32_t>(offsetof(FingerTouch_t2158177300, ___fingerId_4)); }
	inline int32_t get_fingerId_4() const { return ___fingerId_4; }
	inline int32_t* get_address_of_fingerId_4() { return &___fingerId_4; }
	inline void set_fingerId_4(int32_t value)
	{
		___fingerId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
