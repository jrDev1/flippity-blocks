﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_iTween_U3CTweenDelayU3Ec__Iterat1889752800.h"
#include "AssemblyU2DCSharp_iTween_U3CTweenRestartU3Ec__Iter3903815285.h"
#include "AssemblyU2DCSharp_iTween_U3CStartU3Ec__Iterator21700299370.h"
#include "AssemblyU2DCSharp_xARMProxy3550954754.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236547.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864769.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3500 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t1889752800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3500[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t1889752800::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3501 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t3903815285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3501[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t3903815285::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3502 = { sizeof (U3CStartU3Ec__Iterator2_t1700299370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3502[4] = 
{
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t1700299370::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3503 = { sizeof (xARMProxy_t3550954754), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3504 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3504[3] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1(),
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3505 = { sizeof (U24ArrayTypeU3D28_t2375206772)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D28_t2375206772 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3506 = { sizeof (U24ArrayTypeU3D36_t3894236547)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D36_t3894236547 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3507 = { sizeof (U24ArrayTypeU3D124_t2306864769)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D124_t2306864769 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
