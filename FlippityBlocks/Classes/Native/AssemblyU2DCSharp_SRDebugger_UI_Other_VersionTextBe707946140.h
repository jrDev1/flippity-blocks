﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.VersionTextBehaviour
struct  VersionTextBehaviour_t707946140  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.String SRDebugger.UI.Other.VersionTextBehaviour::Format
	String_t* ___Format_9;
	// UnityEngine.UI.Text SRDebugger.UI.Other.VersionTextBehaviour::Text
	Text_t356221433 * ___Text_10;

public:
	inline static int32_t get_offset_of_Format_9() { return static_cast<int32_t>(offsetof(VersionTextBehaviour_t707946140, ___Format_9)); }
	inline String_t* get_Format_9() const { return ___Format_9; }
	inline String_t** get_address_of_Format_9() { return &___Format_9; }
	inline void set_Format_9(String_t* value)
	{
		___Format_9 = value;
		Il2CppCodeGenWriteBarrier(&___Format_9, value);
	}

	inline static int32_t get_offset_of_Text_10() { return static_cast<int32_t>(offsetof(VersionTextBehaviour_t707946140, ___Text_10)); }
	inline Text_t356221433 * get_Text_10() const { return ___Text_10; }
	inline Text_t356221433 ** get_address_of_Text_10() { return &___Text_10; }
	inline void set_Text_10(Text_t356221433 * value)
	{
		___Text_10 = value;
		Il2CppCodeGenWriteBarrier(&___Text_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
