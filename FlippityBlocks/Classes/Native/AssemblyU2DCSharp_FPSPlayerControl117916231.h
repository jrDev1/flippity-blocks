﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSPlayerControl
struct  FPSPlayerControl_t117916231  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip FPSPlayerControl::gunSound
	AudioClip_t1932558630 * ___gunSound_2;
	// UnityEngine.AudioClip FPSPlayerControl::reload
	AudioClip_t1932558630 * ___reload_3;
	// UnityEngine.AudioClip FPSPlayerControl::needReload
	AudioClip_t1932558630 * ___needReload_4;
	// UnityEngine.ParticleSystem FPSPlayerControl::shellParticle
	ParticleSystem_t3394631041 * ___shellParticle_5;
	// UnityEngine.GameObject FPSPlayerControl::muzzleEffect
	GameObject_t1756533147 * ___muzzleEffect_6;
	// UnityEngine.GameObject FPSPlayerControl::impactEffect
	GameObject_t1756533147 * ___impactEffect_7;
	// UnityEngine.UI.Text FPSPlayerControl::armoText
	Text_t356221433 * ___armoText_8;
	// System.Boolean FPSPlayerControl::inFire
	bool ___inFire_9;
	// System.Boolean FPSPlayerControl::inReload
	bool ___inReload_10;
	// UnityEngine.Animator FPSPlayerControl::anim
	Animator_t69676727 * ___anim_11;
	// System.Int32 FPSPlayerControl::armoCount
	int32_t ___armoCount_12;
	// UnityEngine.AudioSource FPSPlayerControl::audioSource
	AudioSource_t1135106623 * ___audioSource_13;

public:
	inline static int32_t get_offset_of_gunSound_2() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___gunSound_2)); }
	inline AudioClip_t1932558630 * get_gunSound_2() const { return ___gunSound_2; }
	inline AudioClip_t1932558630 ** get_address_of_gunSound_2() { return &___gunSound_2; }
	inline void set_gunSound_2(AudioClip_t1932558630 * value)
	{
		___gunSound_2 = value;
		Il2CppCodeGenWriteBarrier(&___gunSound_2, value);
	}

	inline static int32_t get_offset_of_reload_3() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___reload_3)); }
	inline AudioClip_t1932558630 * get_reload_3() const { return ___reload_3; }
	inline AudioClip_t1932558630 ** get_address_of_reload_3() { return &___reload_3; }
	inline void set_reload_3(AudioClip_t1932558630 * value)
	{
		___reload_3 = value;
		Il2CppCodeGenWriteBarrier(&___reload_3, value);
	}

	inline static int32_t get_offset_of_needReload_4() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___needReload_4)); }
	inline AudioClip_t1932558630 * get_needReload_4() const { return ___needReload_4; }
	inline AudioClip_t1932558630 ** get_address_of_needReload_4() { return &___needReload_4; }
	inline void set_needReload_4(AudioClip_t1932558630 * value)
	{
		___needReload_4 = value;
		Il2CppCodeGenWriteBarrier(&___needReload_4, value);
	}

	inline static int32_t get_offset_of_shellParticle_5() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___shellParticle_5)); }
	inline ParticleSystem_t3394631041 * get_shellParticle_5() const { return ___shellParticle_5; }
	inline ParticleSystem_t3394631041 ** get_address_of_shellParticle_5() { return &___shellParticle_5; }
	inline void set_shellParticle_5(ParticleSystem_t3394631041 * value)
	{
		___shellParticle_5 = value;
		Il2CppCodeGenWriteBarrier(&___shellParticle_5, value);
	}

	inline static int32_t get_offset_of_muzzleEffect_6() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___muzzleEffect_6)); }
	inline GameObject_t1756533147 * get_muzzleEffect_6() const { return ___muzzleEffect_6; }
	inline GameObject_t1756533147 ** get_address_of_muzzleEffect_6() { return &___muzzleEffect_6; }
	inline void set_muzzleEffect_6(GameObject_t1756533147 * value)
	{
		___muzzleEffect_6 = value;
		Il2CppCodeGenWriteBarrier(&___muzzleEffect_6, value);
	}

	inline static int32_t get_offset_of_impactEffect_7() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___impactEffect_7)); }
	inline GameObject_t1756533147 * get_impactEffect_7() const { return ___impactEffect_7; }
	inline GameObject_t1756533147 ** get_address_of_impactEffect_7() { return &___impactEffect_7; }
	inline void set_impactEffect_7(GameObject_t1756533147 * value)
	{
		___impactEffect_7 = value;
		Il2CppCodeGenWriteBarrier(&___impactEffect_7, value);
	}

	inline static int32_t get_offset_of_armoText_8() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___armoText_8)); }
	inline Text_t356221433 * get_armoText_8() const { return ___armoText_8; }
	inline Text_t356221433 ** get_address_of_armoText_8() { return &___armoText_8; }
	inline void set_armoText_8(Text_t356221433 * value)
	{
		___armoText_8 = value;
		Il2CppCodeGenWriteBarrier(&___armoText_8, value);
	}

	inline static int32_t get_offset_of_inFire_9() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___inFire_9)); }
	inline bool get_inFire_9() const { return ___inFire_9; }
	inline bool* get_address_of_inFire_9() { return &___inFire_9; }
	inline void set_inFire_9(bool value)
	{
		___inFire_9 = value;
	}

	inline static int32_t get_offset_of_inReload_10() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___inReload_10)); }
	inline bool get_inReload_10() const { return ___inReload_10; }
	inline bool* get_address_of_inReload_10() { return &___inReload_10; }
	inline void set_inReload_10(bool value)
	{
		___inReload_10 = value;
	}

	inline static int32_t get_offset_of_anim_11() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___anim_11)); }
	inline Animator_t69676727 * get_anim_11() const { return ___anim_11; }
	inline Animator_t69676727 ** get_address_of_anim_11() { return &___anim_11; }
	inline void set_anim_11(Animator_t69676727 * value)
	{
		___anim_11 = value;
		Il2CppCodeGenWriteBarrier(&___anim_11, value);
	}

	inline static int32_t get_offset_of_armoCount_12() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___armoCount_12)); }
	inline int32_t get_armoCount_12() const { return ___armoCount_12; }
	inline int32_t* get_address_of_armoCount_12() { return &___armoCount_12; }
	inline void set_armoCount_12(int32_t value)
	{
		___armoCount_12 = value;
	}

	inline static int32_t get_offset_of_audioSource_13() { return static_cast<int32_t>(offsetof(FPSPlayerControl_t117916231, ___audioSource_13)); }
	inline AudioSource_t1135106623 * get_audioSource_13() const { return ___audioSource_13; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_13() { return &___audioSource_13; }
	inline void set_audioSource_13(AudioSource_t1135106623 * value)
	{
		___audioSource_13 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
