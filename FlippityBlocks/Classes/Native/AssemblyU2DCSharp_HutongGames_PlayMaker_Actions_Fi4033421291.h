﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FindGameObjectsWithTag
struct  FindGameObjectsWithTag_t4033421291  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.FindGameObjectsWithTag::withTag
	FsmString_t2414474701 * ___withTag_11;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.FindGameObjectsWithTag::store
	FsmArray_t527459893 * ___store_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.FindGameObjectsWithTag::TagDoesntExistEvent
	FsmEvent_t1258573736 * ___TagDoesntExistEvent_13;
	// UnityEngine.GameObject[] HutongGames.PlayMaker.Actions.FindGameObjectsWithTag::_result
	GameObjectU5BU5D_t3057952154* ____result_14;

public:
	inline static int32_t get_offset_of_withTag_11() { return static_cast<int32_t>(offsetof(FindGameObjectsWithTag_t4033421291, ___withTag_11)); }
	inline FsmString_t2414474701 * get_withTag_11() const { return ___withTag_11; }
	inline FsmString_t2414474701 ** get_address_of_withTag_11() { return &___withTag_11; }
	inline void set_withTag_11(FsmString_t2414474701 * value)
	{
		___withTag_11 = value;
		Il2CppCodeGenWriteBarrier(&___withTag_11, value);
	}

	inline static int32_t get_offset_of_store_12() { return static_cast<int32_t>(offsetof(FindGameObjectsWithTag_t4033421291, ___store_12)); }
	inline FsmArray_t527459893 * get_store_12() const { return ___store_12; }
	inline FsmArray_t527459893 ** get_address_of_store_12() { return &___store_12; }
	inline void set_store_12(FsmArray_t527459893 * value)
	{
		___store_12 = value;
		Il2CppCodeGenWriteBarrier(&___store_12, value);
	}

	inline static int32_t get_offset_of_TagDoesntExistEvent_13() { return static_cast<int32_t>(offsetof(FindGameObjectsWithTag_t4033421291, ___TagDoesntExistEvent_13)); }
	inline FsmEvent_t1258573736 * get_TagDoesntExistEvent_13() const { return ___TagDoesntExistEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_TagDoesntExistEvent_13() { return &___TagDoesntExistEvent_13; }
	inline void set_TagDoesntExistEvent_13(FsmEvent_t1258573736 * value)
	{
		___TagDoesntExistEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___TagDoesntExistEvent_13, value);
	}

	inline static int32_t get_offset_of__result_14() { return static_cast<int32_t>(offsetof(FindGameObjectsWithTag_t4033421291, ____result_14)); }
	inline GameObjectU5BU5D_t3057952154* get__result_14() const { return ____result_14; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__result_14() { return &____result_14; }
	inline void set__result_14(GameObjectU5BU5D_t3057952154* value)
	{
		____result_14 = value;
		Il2CppCodeGenWriteBarrier(&____result_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
