﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtDestroyPool
struct  PmtDestroyPool_t540127429  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtDestroyPool::poolName
	FsmString_t2414474701 * ___poolName_11;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtDestroyPool_t540127429, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
