﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t4101825636;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// UnityEngine.GUIContent
struct GUIContent_t4210063000;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayMakerGUI
struct PlayMakerGUI_t2662579489;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Comparison`1<PlayMakerFSM>
struct Comparison_1_t1699476059;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerGUI
struct  PlayMakerGUI_t2662579489  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerGUI::previewOnGUI
	bool ___previewOnGUI_6;
	// System.Boolean PlayMakerGUI::enableGUILayout
	bool ___enableGUILayout_7;
	// System.Boolean PlayMakerGUI::drawStateLabels
	bool ___drawStateLabels_8;
	// System.Boolean PlayMakerGUI::enableStateLabelsInBuilds
	bool ___enableStateLabelsInBuilds_9;
	// System.Boolean PlayMakerGUI::GUITextureStateLabels
	bool ___GUITextureStateLabels_10;
	// System.Boolean PlayMakerGUI::GUITextStateLabels
	bool ___GUITextStateLabels_11;
	// System.Boolean PlayMakerGUI::filterLabelsWithDistance
	bool ___filterLabelsWithDistance_12;
	// System.Single PlayMakerGUI::maxLabelDistance
	float ___maxLabelDistance_13;
	// System.Boolean PlayMakerGUI::controlMouseCursor
	bool ___controlMouseCursor_14;
	// System.Single PlayMakerGUI::labelScale
	float ___labelScale_15;
	// System.Single PlayMakerGUI::initLabelScale
	float ___initLabelScale_27;

public:
	inline static int32_t get_offset_of_previewOnGUI_6() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___previewOnGUI_6)); }
	inline bool get_previewOnGUI_6() const { return ___previewOnGUI_6; }
	inline bool* get_address_of_previewOnGUI_6() { return &___previewOnGUI_6; }
	inline void set_previewOnGUI_6(bool value)
	{
		___previewOnGUI_6 = value;
	}

	inline static int32_t get_offset_of_enableGUILayout_7() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___enableGUILayout_7)); }
	inline bool get_enableGUILayout_7() const { return ___enableGUILayout_7; }
	inline bool* get_address_of_enableGUILayout_7() { return &___enableGUILayout_7; }
	inline void set_enableGUILayout_7(bool value)
	{
		___enableGUILayout_7 = value;
	}

	inline static int32_t get_offset_of_drawStateLabels_8() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___drawStateLabels_8)); }
	inline bool get_drawStateLabels_8() const { return ___drawStateLabels_8; }
	inline bool* get_address_of_drawStateLabels_8() { return &___drawStateLabels_8; }
	inline void set_drawStateLabels_8(bool value)
	{
		___drawStateLabels_8 = value;
	}

	inline static int32_t get_offset_of_enableStateLabelsInBuilds_9() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___enableStateLabelsInBuilds_9)); }
	inline bool get_enableStateLabelsInBuilds_9() const { return ___enableStateLabelsInBuilds_9; }
	inline bool* get_address_of_enableStateLabelsInBuilds_9() { return &___enableStateLabelsInBuilds_9; }
	inline void set_enableStateLabelsInBuilds_9(bool value)
	{
		___enableStateLabelsInBuilds_9 = value;
	}

	inline static int32_t get_offset_of_GUITextureStateLabels_10() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___GUITextureStateLabels_10)); }
	inline bool get_GUITextureStateLabels_10() const { return ___GUITextureStateLabels_10; }
	inline bool* get_address_of_GUITextureStateLabels_10() { return &___GUITextureStateLabels_10; }
	inline void set_GUITextureStateLabels_10(bool value)
	{
		___GUITextureStateLabels_10 = value;
	}

	inline static int32_t get_offset_of_GUITextStateLabels_11() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___GUITextStateLabels_11)); }
	inline bool get_GUITextStateLabels_11() const { return ___GUITextStateLabels_11; }
	inline bool* get_address_of_GUITextStateLabels_11() { return &___GUITextStateLabels_11; }
	inline void set_GUITextStateLabels_11(bool value)
	{
		___GUITextStateLabels_11 = value;
	}

	inline static int32_t get_offset_of_filterLabelsWithDistance_12() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___filterLabelsWithDistance_12)); }
	inline bool get_filterLabelsWithDistance_12() const { return ___filterLabelsWithDistance_12; }
	inline bool* get_address_of_filterLabelsWithDistance_12() { return &___filterLabelsWithDistance_12; }
	inline void set_filterLabelsWithDistance_12(bool value)
	{
		___filterLabelsWithDistance_12 = value;
	}

	inline static int32_t get_offset_of_maxLabelDistance_13() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___maxLabelDistance_13)); }
	inline float get_maxLabelDistance_13() const { return ___maxLabelDistance_13; }
	inline float* get_address_of_maxLabelDistance_13() { return &___maxLabelDistance_13; }
	inline void set_maxLabelDistance_13(float value)
	{
		___maxLabelDistance_13 = value;
	}

	inline static int32_t get_offset_of_controlMouseCursor_14() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___controlMouseCursor_14)); }
	inline bool get_controlMouseCursor_14() const { return ___controlMouseCursor_14; }
	inline bool* get_address_of_controlMouseCursor_14() { return &___controlMouseCursor_14; }
	inline void set_controlMouseCursor_14(bool value)
	{
		___controlMouseCursor_14 = value;
	}

	inline static int32_t get_offset_of_labelScale_15() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___labelScale_15)); }
	inline float get_labelScale_15() const { return ___labelScale_15; }
	inline float* get_address_of_labelScale_15() { return &___labelScale_15; }
	inline void set_labelScale_15(float value)
	{
		___labelScale_15 = value;
	}

	inline static int32_t get_offset_of_initLabelScale_27() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489, ___initLabelScale_27)); }
	inline float get_initLabelScale_27() const { return ___initLabelScale_27; }
	inline float* get_address_of_initLabelScale_27() { return &___initLabelScale_27; }
	inline void set_initLabelScale_27(float value)
	{
		___initLabelScale_27 = value;
	}
};

struct PlayMakerGUI_t2662579489_StaticFields
{
public:
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::fsmList
	List_1_t4101825636 * ___fsmList_3;
	// HutongGames.PlayMaker.Fsm PlayMakerGUI::SelectedFSM
	Fsm_t917886356 * ___SelectedFSM_4;
	// UnityEngine.GUIContent PlayMakerGUI::labelContent
	GUIContent_t4210063000 * ___labelContent_5;
	// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerGUI::SortedFsmList
	List_1_t4101825636 * ___SortedFsmList_16;
	// UnityEngine.GameObject PlayMakerGUI::labelGameObject
	GameObject_t1756533147 * ___labelGameObject_17;
	// System.Single PlayMakerGUI::fsmLabelIndex
	float ___fsmLabelIndex_18;
	// PlayMakerGUI PlayMakerGUI::instance
	PlayMakerGUI_t2662579489 * ___instance_19;
	// UnityEngine.GUISkin PlayMakerGUI::guiSkin
	GUISkin_t1436893342 * ___guiSkin_20;
	// UnityEngine.Color PlayMakerGUI::guiColor
	Color_t2020392075  ___guiColor_21;
	// UnityEngine.Color PlayMakerGUI::guiBackgroundColor
	Color_t2020392075  ___guiBackgroundColor_22;
	// UnityEngine.Color PlayMakerGUI::guiContentColor
	Color_t2020392075  ___guiContentColor_23;
	// UnityEngine.Matrix4x4 PlayMakerGUI::guiMatrix
	Matrix4x4_t2933234003  ___guiMatrix_24;
	// UnityEngine.GUIStyle PlayMakerGUI::stateLabelStyle
	GUIStyle_t1799908754 * ___stateLabelStyle_25;
	// UnityEngine.Texture2D PlayMakerGUI::stateLabelBackground
	Texture2D_t3542995729 * ___stateLabelBackground_26;
	// UnityEngine.Texture PlayMakerGUI::<MouseCursor>k__BackingField
	Texture_t2243626319 * ___U3CMouseCursorU3Ek__BackingField_28;
	// System.Boolean PlayMakerGUI::<LockCursor>k__BackingField
	bool ___U3CLockCursorU3Ek__BackingField_29;
	// System.Boolean PlayMakerGUI::<HideCursor>k__BackingField
	bool ___U3CHideCursorU3Ek__BackingField_30;
	// System.Comparison`1<PlayMakerFSM> PlayMakerGUI::CS$<>9__CachedAnonymousMethodDelegate2
	Comparison_1_t1699476059 * ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31;

public:
	inline static int32_t get_offset_of_fsmList_3() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___fsmList_3)); }
	inline List_1_t4101825636 * get_fsmList_3() const { return ___fsmList_3; }
	inline List_1_t4101825636 ** get_address_of_fsmList_3() { return &___fsmList_3; }
	inline void set_fsmList_3(List_1_t4101825636 * value)
	{
		___fsmList_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsmList_3, value);
	}

	inline static int32_t get_offset_of_SelectedFSM_4() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___SelectedFSM_4)); }
	inline Fsm_t917886356 * get_SelectedFSM_4() const { return ___SelectedFSM_4; }
	inline Fsm_t917886356 ** get_address_of_SelectedFSM_4() { return &___SelectedFSM_4; }
	inline void set_SelectedFSM_4(Fsm_t917886356 * value)
	{
		___SelectedFSM_4 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedFSM_4, value);
	}

	inline static int32_t get_offset_of_labelContent_5() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___labelContent_5)); }
	inline GUIContent_t4210063000 * get_labelContent_5() const { return ___labelContent_5; }
	inline GUIContent_t4210063000 ** get_address_of_labelContent_5() { return &___labelContent_5; }
	inline void set_labelContent_5(GUIContent_t4210063000 * value)
	{
		___labelContent_5 = value;
		Il2CppCodeGenWriteBarrier(&___labelContent_5, value);
	}

	inline static int32_t get_offset_of_SortedFsmList_16() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___SortedFsmList_16)); }
	inline List_1_t4101825636 * get_SortedFsmList_16() const { return ___SortedFsmList_16; }
	inline List_1_t4101825636 ** get_address_of_SortedFsmList_16() { return &___SortedFsmList_16; }
	inline void set_SortedFsmList_16(List_1_t4101825636 * value)
	{
		___SortedFsmList_16 = value;
		Il2CppCodeGenWriteBarrier(&___SortedFsmList_16, value);
	}

	inline static int32_t get_offset_of_labelGameObject_17() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___labelGameObject_17)); }
	inline GameObject_t1756533147 * get_labelGameObject_17() const { return ___labelGameObject_17; }
	inline GameObject_t1756533147 ** get_address_of_labelGameObject_17() { return &___labelGameObject_17; }
	inline void set_labelGameObject_17(GameObject_t1756533147 * value)
	{
		___labelGameObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___labelGameObject_17, value);
	}

	inline static int32_t get_offset_of_fsmLabelIndex_18() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___fsmLabelIndex_18)); }
	inline float get_fsmLabelIndex_18() const { return ___fsmLabelIndex_18; }
	inline float* get_address_of_fsmLabelIndex_18() { return &___fsmLabelIndex_18; }
	inline void set_fsmLabelIndex_18(float value)
	{
		___fsmLabelIndex_18 = value;
	}

	inline static int32_t get_offset_of_instance_19() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___instance_19)); }
	inline PlayMakerGUI_t2662579489 * get_instance_19() const { return ___instance_19; }
	inline PlayMakerGUI_t2662579489 ** get_address_of_instance_19() { return &___instance_19; }
	inline void set_instance_19(PlayMakerGUI_t2662579489 * value)
	{
		___instance_19 = value;
		Il2CppCodeGenWriteBarrier(&___instance_19, value);
	}

	inline static int32_t get_offset_of_guiSkin_20() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiSkin_20)); }
	inline GUISkin_t1436893342 * get_guiSkin_20() const { return ___guiSkin_20; }
	inline GUISkin_t1436893342 ** get_address_of_guiSkin_20() { return &___guiSkin_20; }
	inline void set_guiSkin_20(GUISkin_t1436893342 * value)
	{
		___guiSkin_20 = value;
		Il2CppCodeGenWriteBarrier(&___guiSkin_20, value);
	}

	inline static int32_t get_offset_of_guiColor_21() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiColor_21)); }
	inline Color_t2020392075  get_guiColor_21() const { return ___guiColor_21; }
	inline Color_t2020392075 * get_address_of_guiColor_21() { return &___guiColor_21; }
	inline void set_guiColor_21(Color_t2020392075  value)
	{
		___guiColor_21 = value;
	}

	inline static int32_t get_offset_of_guiBackgroundColor_22() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiBackgroundColor_22)); }
	inline Color_t2020392075  get_guiBackgroundColor_22() const { return ___guiBackgroundColor_22; }
	inline Color_t2020392075 * get_address_of_guiBackgroundColor_22() { return &___guiBackgroundColor_22; }
	inline void set_guiBackgroundColor_22(Color_t2020392075  value)
	{
		___guiBackgroundColor_22 = value;
	}

	inline static int32_t get_offset_of_guiContentColor_23() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiContentColor_23)); }
	inline Color_t2020392075  get_guiContentColor_23() const { return ___guiContentColor_23; }
	inline Color_t2020392075 * get_address_of_guiContentColor_23() { return &___guiContentColor_23; }
	inline void set_guiContentColor_23(Color_t2020392075  value)
	{
		___guiContentColor_23 = value;
	}

	inline static int32_t get_offset_of_guiMatrix_24() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___guiMatrix_24)); }
	inline Matrix4x4_t2933234003  get_guiMatrix_24() const { return ___guiMatrix_24; }
	inline Matrix4x4_t2933234003 * get_address_of_guiMatrix_24() { return &___guiMatrix_24; }
	inline void set_guiMatrix_24(Matrix4x4_t2933234003  value)
	{
		___guiMatrix_24 = value;
	}

	inline static int32_t get_offset_of_stateLabelStyle_25() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___stateLabelStyle_25)); }
	inline GUIStyle_t1799908754 * get_stateLabelStyle_25() const { return ___stateLabelStyle_25; }
	inline GUIStyle_t1799908754 ** get_address_of_stateLabelStyle_25() { return &___stateLabelStyle_25; }
	inline void set_stateLabelStyle_25(GUIStyle_t1799908754 * value)
	{
		___stateLabelStyle_25 = value;
		Il2CppCodeGenWriteBarrier(&___stateLabelStyle_25, value);
	}

	inline static int32_t get_offset_of_stateLabelBackground_26() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___stateLabelBackground_26)); }
	inline Texture2D_t3542995729 * get_stateLabelBackground_26() const { return ___stateLabelBackground_26; }
	inline Texture2D_t3542995729 ** get_address_of_stateLabelBackground_26() { return &___stateLabelBackground_26; }
	inline void set_stateLabelBackground_26(Texture2D_t3542995729 * value)
	{
		___stateLabelBackground_26 = value;
		Il2CppCodeGenWriteBarrier(&___stateLabelBackground_26, value);
	}

	inline static int32_t get_offset_of_U3CMouseCursorU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___U3CMouseCursorU3Ek__BackingField_28)); }
	inline Texture_t2243626319 * get_U3CMouseCursorU3Ek__BackingField_28() const { return ___U3CMouseCursorU3Ek__BackingField_28; }
	inline Texture_t2243626319 ** get_address_of_U3CMouseCursorU3Ek__BackingField_28() { return &___U3CMouseCursorU3Ek__BackingField_28; }
	inline void set_U3CMouseCursorU3Ek__BackingField_28(Texture_t2243626319 * value)
	{
		___U3CMouseCursorU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CMouseCursorU3Ek__BackingField_28, value);
	}

	inline static int32_t get_offset_of_U3CLockCursorU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___U3CLockCursorU3Ek__BackingField_29)); }
	inline bool get_U3CLockCursorU3Ek__BackingField_29() const { return ___U3CLockCursorU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CLockCursorU3Ek__BackingField_29() { return &___U3CLockCursorU3Ek__BackingField_29; }
	inline void set_U3CLockCursorU3Ek__BackingField_29(bool value)
	{
		___U3CLockCursorU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CHideCursorU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___U3CHideCursorU3Ek__BackingField_30)); }
	inline bool get_U3CHideCursorU3Ek__BackingField_30() const { return ___U3CHideCursorU3Ek__BackingField_30; }
	inline bool* get_address_of_U3CHideCursorU3Ek__BackingField_30() { return &___U3CHideCursorU3Ek__BackingField_30; }
	inline void set_U3CHideCursorU3Ek__BackingField_30(bool value)
	{
		___U3CHideCursorU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31() { return static_cast<int32_t>(offsetof(PlayMakerGUI_t2662579489_StaticFields, ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31)); }
	inline Comparison_1_t1699476059 * get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31() const { return ___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31; }
	inline Comparison_1_t1699476059 ** get_address_of_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31() { return &___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31; }
	inline void set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31(Comparison_1_t1699476059 * value)
	{
		___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31 = value;
		Il2CppCodeGenWriteBarrier(&___CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
