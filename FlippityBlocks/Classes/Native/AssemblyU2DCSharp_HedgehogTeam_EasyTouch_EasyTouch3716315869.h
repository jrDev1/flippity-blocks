﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2268380264.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_110068694.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch4166573859.h"

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver
struct  EasyTouchReceiver_t3716315869  : public Il2CppObject
{
public:
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::enable
	bool ___enable_0;
	// HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTType HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::triggerType
	int32_t ___triggerType_1;
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::name
	String_t* ___name_2;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::restricted
	bool ___restricted_3;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::gameObject
	GameObject_t1756533147 * ___gameObject_4;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::otherReceiver
	bool ___otherReceiver_5;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::gameObjectReceiver
	GameObject_t1756533147 * ___gameObjectReceiver_6;
	// HedgehogTeam.EasyTouch.EasyTouch/EvtType HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::eventName
	int32_t ___eventName_7;
	// System.String HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::methodName
	String_t* ___methodName_8;
	// HedgehogTeam.EasyTouch.EasyTouchTrigger/ETTParameter HedgehogTeam.EasyTouch.EasyTouchTrigger/EasyTouchReceiver::parameter
	int32_t ___parameter_9;

public:
	inline static int32_t get_offset_of_enable_0() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___enable_0)); }
	inline bool get_enable_0() const { return ___enable_0; }
	inline bool* get_address_of_enable_0() { return &___enable_0; }
	inline void set_enable_0(bool value)
	{
		___enable_0 = value;
	}

	inline static int32_t get_offset_of_triggerType_1() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___triggerType_1)); }
	inline int32_t get_triggerType_1() const { return ___triggerType_1; }
	inline int32_t* get_address_of_triggerType_1() { return &___triggerType_1; }
	inline void set_triggerType_1(int32_t value)
	{
		___triggerType_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_restricted_3() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___restricted_3)); }
	inline bool get_restricted_3() const { return ___restricted_3; }
	inline bool* get_address_of_restricted_3() { return &___restricted_3; }
	inline void set_restricted_3(bool value)
	{
		___restricted_3 = value;
	}

	inline static int32_t get_offset_of_gameObject_4() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___gameObject_4)); }
	inline GameObject_t1756533147 * get_gameObject_4() const { return ___gameObject_4; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_4() { return &___gameObject_4; }
	inline void set_gameObject_4(GameObject_t1756533147 * value)
	{
		___gameObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_4, value);
	}

	inline static int32_t get_offset_of_otherReceiver_5() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___otherReceiver_5)); }
	inline bool get_otherReceiver_5() const { return ___otherReceiver_5; }
	inline bool* get_address_of_otherReceiver_5() { return &___otherReceiver_5; }
	inline void set_otherReceiver_5(bool value)
	{
		___otherReceiver_5 = value;
	}

	inline static int32_t get_offset_of_gameObjectReceiver_6() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___gameObjectReceiver_6)); }
	inline GameObject_t1756533147 * get_gameObjectReceiver_6() const { return ___gameObjectReceiver_6; }
	inline GameObject_t1756533147 ** get_address_of_gameObjectReceiver_6() { return &___gameObjectReceiver_6; }
	inline void set_gameObjectReceiver_6(GameObject_t1756533147 * value)
	{
		___gameObjectReceiver_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectReceiver_6, value);
	}

	inline static int32_t get_offset_of_eventName_7() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___eventName_7)); }
	inline int32_t get_eventName_7() const { return ___eventName_7; }
	inline int32_t* get_address_of_eventName_7() { return &___eventName_7; }
	inline void set_eventName_7(int32_t value)
	{
		___eventName_7 = value;
	}

	inline static int32_t get_offset_of_methodName_8() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___methodName_8)); }
	inline String_t* get_methodName_8() const { return ___methodName_8; }
	inline String_t** get_address_of_methodName_8() { return &___methodName_8; }
	inline void set_methodName_8(String_t* value)
	{
		___methodName_8 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_8, value);
	}

	inline static int32_t get_offset_of_parameter_9() { return static_cast<int32_t>(offsetof(EasyTouchReceiver_t3716315869, ___parameter_9)); }
	inline int32_t get_parameter_9() const { return ___parameter_9; }
	inline int32_t* get_address_of_parameter_9() { return &___parameter_9; }
	inline void set_parameter_9(int32_t value)
	{
		___parameter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
