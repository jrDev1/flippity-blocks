﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2
struct  U3CU3Ec__DisplayClass2_t841294727  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.NamedVariable HutongGames.PlayMaker.FsmTemplateControl/<>c__DisplayClass2::namedVariable
	NamedVariable_t3026441313 * ___namedVariable_0;

public:
	inline static int32_t get_offset_of_namedVariable_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_t841294727, ___namedVariable_0)); }
	inline NamedVariable_t3026441313 * get_namedVariable_0() const { return ___namedVariable_0; }
	inline NamedVariable_t3026441313 ** get_address_of_namedVariable_0() { return &___namedVariable_0; }
	inline void set_namedVariable_0(NamedVariable_t3026441313 * value)
	{
		___namedVariable_0 = value;
		Il2CppCodeGenWriteBarrier(&___namedVariable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
