﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.DebugCameraServiceImpl
struct  DebugCameraServiceImpl_t2487113893  : public Il2CppObject
{
public:
	// UnityEngine.Camera SRDebugger.Services.Implementation.DebugCameraServiceImpl::_debugCamera
	Camera_t189460977 * ____debugCamera_0;

public:
	inline static int32_t get_offset_of__debugCamera_0() { return static_cast<int32_t>(offsetof(DebugCameraServiceImpl_t2487113893, ____debugCamera_0)); }
	inline Camera_t189460977 * get__debugCamera_0() const { return ____debugCamera_0; }
	inline Camera_t189460977 ** get_address_of__debugCamera_0() { return &____debugCamera_0; }
	inline void set__debugCamera_0(Camera_t189460977 * value)
	{
		____debugCamera_0 = value;
		Il2CppCodeGenWriteBarrier(&____debugCamera_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
