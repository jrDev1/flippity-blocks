﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// System.Collections.ArrayList
struct ArrayList_t4252133567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableGetRandom
struct  HashTableGetRandom_t3451311881  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableGetRandom::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableGetRandom::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableGetRandom::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableGetRandom::key
	FsmString_t2414474701 * ___key_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.HashTableGetRandom::result
	FsmVar_t2872592513 * ___result_16;
	// System.Collections.ArrayList HutongGames.PlayMaker.Actions.HashTableGetRandom::_keys
	ArrayList_t4252133567 * ____keys_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableGetRandom_t3451311881, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableGetRandom_t3451311881, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_failureEvent_14() { return static_cast<int32_t>(offsetof(HashTableGetRandom_t3451311881, ___failureEvent_14)); }
	inline FsmEvent_t1258573736 * get_failureEvent_14() const { return ___failureEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_14() { return &___failureEvent_14; }
	inline void set_failureEvent_14(FsmEvent_t1258573736 * value)
	{
		___failureEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_14, value);
	}

	inline static int32_t get_offset_of_key_15() { return static_cast<int32_t>(offsetof(HashTableGetRandom_t3451311881, ___key_15)); }
	inline FsmString_t2414474701 * get_key_15() const { return ___key_15; }
	inline FsmString_t2414474701 ** get_address_of_key_15() { return &___key_15; }
	inline void set_key_15(FsmString_t2414474701 * value)
	{
		___key_15 = value;
		Il2CppCodeGenWriteBarrier(&___key_15, value);
	}

	inline static int32_t get_offset_of_result_16() { return static_cast<int32_t>(offsetof(HashTableGetRandom_t3451311881, ___result_16)); }
	inline FsmVar_t2872592513 * get_result_16() const { return ___result_16; }
	inline FsmVar_t2872592513 ** get_address_of_result_16() { return &___result_16; }
	inline void set_result_16(FsmVar_t2872592513 * value)
	{
		___result_16 = value;
		Il2CppCodeGenWriteBarrier(&___result_16, value);
	}

	inline static int32_t get_offset_of__keys_17() { return static_cast<int32_t>(offsetof(HashTableGetRandom_t3451311881, ____keys_17)); }
	inline ArrayList_t4252133567 * get__keys_17() const { return ____keys_17; }
	inline ArrayList_t4252133567 ** get_address_of__keys_17() { return &____keys_17; }
	inline void set__keys_17(ArrayList_t4252133567 * value)
	{
		____keys_17 = value;
		Il2CppCodeGenWriteBarrier(&____keys_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
