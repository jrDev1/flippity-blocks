﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy
struct  GameObjectIsActiveInHierarchy_t221971743  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy::gameObject
	FsmGameObject_t3097142863 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy::isActive
	FsmEvent_t1258573736 * ___isActive_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy::isNotActive
	FsmEvent_t1258573736 * ___isNotActive_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy::storeResult
	FsmBool_t664485696 * ___storeResult_14;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy::debugging
	bool ___debugging_15;
	// System.Boolean HutongGames.PlayMaker.Actions.GameObjectIsActiveInHierarchy::everyFrame
	bool ___everyFrame_16;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GameObjectIsActiveInHierarchy_t221971743, ___gameObject_11)); }
	inline FsmGameObject_t3097142863 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t3097142863 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_isActive_12() { return static_cast<int32_t>(offsetof(GameObjectIsActiveInHierarchy_t221971743, ___isActive_12)); }
	inline FsmEvent_t1258573736 * get_isActive_12() const { return ___isActive_12; }
	inline FsmEvent_t1258573736 ** get_address_of_isActive_12() { return &___isActive_12; }
	inline void set_isActive_12(FsmEvent_t1258573736 * value)
	{
		___isActive_12 = value;
		Il2CppCodeGenWriteBarrier(&___isActive_12, value);
	}

	inline static int32_t get_offset_of_isNotActive_13() { return static_cast<int32_t>(offsetof(GameObjectIsActiveInHierarchy_t221971743, ___isNotActive_13)); }
	inline FsmEvent_t1258573736 * get_isNotActive_13() const { return ___isNotActive_13; }
	inline FsmEvent_t1258573736 ** get_address_of_isNotActive_13() { return &___isNotActive_13; }
	inline void set_isNotActive_13(FsmEvent_t1258573736 * value)
	{
		___isNotActive_13 = value;
		Il2CppCodeGenWriteBarrier(&___isNotActive_13, value);
	}

	inline static int32_t get_offset_of_storeResult_14() { return static_cast<int32_t>(offsetof(GameObjectIsActiveInHierarchy_t221971743, ___storeResult_14)); }
	inline FsmBool_t664485696 * get_storeResult_14() const { return ___storeResult_14; }
	inline FsmBool_t664485696 ** get_address_of_storeResult_14() { return &___storeResult_14; }
	inline void set_storeResult_14(FsmBool_t664485696 * value)
	{
		___storeResult_14 = value;
		Il2CppCodeGenWriteBarrier(&___storeResult_14, value);
	}

	inline static int32_t get_offset_of_debugging_15() { return static_cast<int32_t>(offsetof(GameObjectIsActiveInHierarchy_t221971743, ___debugging_15)); }
	inline bool get_debugging_15() const { return ___debugging_15; }
	inline bool* get_address_of_debugging_15() { return &___debugging_15; }
	inline void set_debugging_15(bool value)
	{
		___debugging_15 = value;
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(GameObjectIsActiveInHierarchy_t221971743, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
