﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>
struct Dictionary_2_t894930024;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Hierarchy
struct  Hierarchy_t2267531791  : public Il2CppObject
{
public:

public:
};

struct Hierarchy_t2267531791_StaticFields
{
public:
	// System.Char[] SRF.Hierarchy::Seperator
	CharU5BU5D_t1328083999* ___Seperator_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform> SRF.Hierarchy::Cache
	Dictionary_2_t894930024 * ___Cache_1;

public:
	inline static int32_t get_offset_of_Seperator_0() { return static_cast<int32_t>(offsetof(Hierarchy_t2267531791_StaticFields, ___Seperator_0)); }
	inline CharU5BU5D_t1328083999* get_Seperator_0() const { return ___Seperator_0; }
	inline CharU5BU5D_t1328083999** get_address_of_Seperator_0() { return &___Seperator_0; }
	inline void set_Seperator_0(CharU5BU5D_t1328083999* value)
	{
		___Seperator_0 = value;
		Il2CppCodeGenWriteBarrier(&___Seperator_0, value);
	}

	inline static int32_t get_offset_of_Cache_1() { return static_cast<int32_t>(offsetof(Hierarchy_t2267531791_StaticFields, ___Cache_1)); }
	inline Dictionary_2_t894930024 * get_Cache_1() const { return ___Cache_1; }
	inline Dictionary_2_t894930024 ** get_address_of_Cache_1() { return &___Cache_1; }
	inline void set_Cache_1(Dictionary_2_t894930024 * value)
	{
		___Cache_1 = value;
		Il2CppCodeGenWriteBarrier(&___Cache_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
