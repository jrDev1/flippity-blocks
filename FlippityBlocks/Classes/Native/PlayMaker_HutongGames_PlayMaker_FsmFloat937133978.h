﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.FsmFloat
struct  FsmFloat_t937133978  : public NamedVariable_t3026441313
{
public:
	// System.Single HutongGames.PlayMaker.FsmFloat::value
	float ___value_5;

public:
	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(FsmFloat_t937133978, ___value_5)); }
	inline float get_value_5() const { return ___value_5; }
	inline float* get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(float value)
	{
		___value_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
