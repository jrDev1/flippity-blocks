﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// System.Func`1<System.Object>
struct Func_1_t348874681;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.InfoEntry
struct  InfoEntry_t2565792942  : public Il2CppObject
{
public:
	// System.String SRDebugger.InfoEntry::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_0;
	// System.Boolean SRDebugger.InfoEntry::<IsPrivate>k__BackingField
	bool ___U3CIsPrivateU3Ek__BackingField_1;
	// System.Func`1<System.Object> SRDebugger.InfoEntry::_valueGetter
	Func_1_t348874681 * ____valueGetter_2;

public:
	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InfoEntry_t2565792942, ___U3CTitleU3Ek__BackingField_0)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_0() const { return ___U3CTitleU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_0() { return &___U3CTitleU3Ek__BackingField_0; }
	inline void set_U3CTitleU3Ek__BackingField_0(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTitleU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CIsPrivateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InfoEntry_t2565792942, ___U3CIsPrivateU3Ek__BackingField_1)); }
	inline bool get_U3CIsPrivateU3Ek__BackingField_1() const { return ___U3CIsPrivateU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsPrivateU3Ek__BackingField_1() { return &___U3CIsPrivateU3Ek__BackingField_1; }
	inline void set_U3CIsPrivateU3Ek__BackingField_1(bool value)
	{
		___U3CIsPrivateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__valueGetter_2() { return static_cast<int32_t>(offsetof(InfoEntry_t2565792942, ____valueGetter_2)); }
	inline Func_1_t348874681 * get__valueGetter_2() const { return ____valueGetter_2; }
	inline Func_1_t348874681 ** get_address_of__valueGetter_2() { return &____valueGetter_2; }
	inline void set__valueGetter_2(Func_1_t348874681 * value)
	{
		____valueGetter_2 = value;
		Il2CppCodeGenWriteBarrier(&____valueGetter_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
