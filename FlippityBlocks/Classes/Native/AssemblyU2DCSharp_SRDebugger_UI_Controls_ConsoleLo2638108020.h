﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"
#include "mscorlib_System_Nullable_1_gen506773894.h"

// SRF.UI.Layout.VirtualVerticalLayoutGroup
struct VirtualVerticalLayoutGroup_t4077716920;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// System.Action`1<SRDebugger.Services.ConsoleEntry>
struct Action_1_t2810766981;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ConsoleLogControl
struct  ConsoleLogControl_t2638108020  : public SRMonoBehaviourEx_t3120278648
{
public:
	// SRF.UI.Layout.VirtualVerticalLayoutGroup SRDebugger.UI.Controls.ConsoleLogControl::_consoleScrollLayoutGroup
	VirtualVerticalLayoutGroup_t4077716920 * ____consoleScrollLayoutGroup_9;
	// UnityEngine.UI.ScrollRect SRDebugger.UI.Controls.ConsoleLogControl::_consoleScrollRect
	ScrollRect_t1199013257 * ____consoleScrollRect_10;
	// System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::_isDirty
	bool ____isDirty_11;
	// System.Nullable`1<UnityEngine.Vector2> SRDebugger.UI.Controls.ConsoleLogControl::_scrollPosition
	Nullable_1_t506773894  ____scrollPosition_12;
	// System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::_showErrors
	bool ____showErrors_13;
	// System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::_showInfo
	bool ____showInfo_14;
	// System.Boolean SRDebugger.UI.Controls.ConsoleLogControl::_showWarnings
	bool ____showWarnings_15;
	// System.Action`1<SRDebugger.Services.ConsoleEntry> SRDebugger.UI.Controls.ConsoleLogControl::SelectedItemChanged
	Action_1_t2810766981 * ___SelectedItemChanged_16;
	// System.String SRDebugger.UI.Controls.ConsoleLogControl::_filter
	String_t* ____filter_17;

public:
	inline static int32_t get_offset_of__consoleScrollLayoutGroup_9() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____consoleScrollLayoutGroup_9)); }
	inline VirtualVerticalLayoutGroup_t4077716920 * get__consoleScrollLayoutGroup_9() const { return ____consoleScrollLayoutGroup_9; }
	inline VirtualVerticalLayoutGroup_t4077716920 ** get_address_of__consoleScrollLayoutGroup_9() { return &____consoleScrollLayoutGroup_9; }
	inline void set__consoleScrollLayoutGroup_9(VirtualVerticalLayoutGroup_t4077716920 * value)
	{
		____consoleScrollLayoutGroup_9 = value;
		Il2CppCodeGenWriteBarrier(&____consoleScrollLayoutGroup_9, value);
	}

	inline static int32_t get_offset_of__consoleScrollRect_10() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____consoleScrollRect_10)); }
	inline ScrollRect_t1199013257 * get__consoleScrollRect_10() const { return ____consoleScrollRect_10; }
	inline ScrollRect_t1199013257 ** get_address_of__consoleScrollRect_10() { return &____consoleScrollRect_10; }
	inline void set__consoleScrollRect_10(ScrollRect_t1199013257 * value)
	{
		____consoleScrollRect_10 = value;
		Il2CppCodeGenWriteBarrier(&____consoleScrollRect_10, value);
	}

	inline static int32_t get_offset_of__isDirty_11() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____isDirty_11)); }
	inline bool get__isDirty_11() const { return ____isDirty_11; }
	inline bool* get_address_of__isDirty_11() { return &____isDirty_11; }
	inline void set__isDirty_11(bool value)
	{
		____isDirty_11 = value;
	}

	inline static int32_t get_offset_of__scrollPosition_12() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____scrollPosition_12)); }
	inline Nullable_1_t506773894  get__scrollPosition_12() const { return ____scrollPosition_12; }
	inline Nullable_1_t506773894 * get_address_of__scrollPosition_12() { return &____scrollPosition_12; }
	inline void set__scrollPosition_12(Nullable_1_t506773894  value)
	{
		____scrollPosition_12 = value;
	}

	inline static int32_t get_offset_of__showErrors_13() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____showErrors_13)); }
	inline bool get__showErrors_13() const { return ____showErrors_13; }
	inline bool* get_address_of__showErrors_13() { return &____showErrors_13; }
	inline void set__showErrors_13(bool value)
	{
		____showErrors_13 = value;
	}

	inline static int32_t get_offset_of__showInfo_14() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____showInfo_14)); }
	inline bool get__showInfo_14() const { return ____showInfo_14; }
	inline bool* get_address_of__showInfo_14() { return &____showInfo_14; }
	inline void set__showInfo_14(bool value)
	{
		____showInfo_14 = value;
	}

	inline static int32_t get_offset_of__showWarnings_15() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____showWarnings_15)); }
	inline bool get__showWarnings_15() const { return ____showWarnings_15; }
	inline bool* get_address_of__showWarnings_15() { return &____showWarnings_15; }
	inline void set__showWarnings_15(bool value)
	{
		____showWarnings_15 = value;
	}

	inline static int32_t get_offset_of_SelectedItemChanged_16() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ___SelectedItemChanged_16)); }
	inline Action_1_t2810766981 * get_SelectedItemChanged_16() const { return ___SelectedItemChanged_16; }
	inline Action_1_t2810766981 ** get_address_of_SelectedItemChanged_16() { return &___SelectedItemChanged_16; }
	inline void set_SelectedItemChanged_16(Action_1_t2810766981 * value)
	{
		___SelectedItemChanged_16 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedItemChanged_16, value);
	}

	inline static int32_t get_offset_of__filter_17() { return static_cast<int32_t>(offsetof(ConsoleLogControl_t2638108020, ____filter_17)); }
	inline String_t* get__filter_17() const { return ____filter_17; }
	inline String_t** get_address_of__filter_17() { return &____filter_17; }
	inline void set__filter_17(String_t* value)
	{
		____filter_17 = value;
		Il2CppCodeGenWriteBarrier(&____filter_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
