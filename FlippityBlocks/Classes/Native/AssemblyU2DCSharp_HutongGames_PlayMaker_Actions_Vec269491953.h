﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Vector3RoundToNearest
struct  Vector3RoundToNearest_t269491953  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3RoundToNearest::vector3Variable
	FsmVector3_t3996534004 * ___vector3Variable_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.Vector3RoundToNearest::nearest
	FsmFloat_t937133978 * ___nearest_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.Vector3RoundToNearest::resultAsVector3
	FsmVector3_t3996534004 * ___resultAsVector3_13;
	// System.Boolean HutongGames.PlayMaker.Actions.Vector3RoundToNearest::everyFrame
	bool ___everyFrame_14;

public:
	inline static int32_t get_offset_of_vector3Variable_11() { return static_cast<int32_t>(offsetof(Vector3RoundToNearest_t269491953, ___vector3Variable_11)); }
	inline FsmVector3_t3996534004 * get_vector3Variable_11() const { return ___vector3Variable_11; }
	inline FsmVector3_t3996534004 ** get_address_of_vector3Variable_11() { return &___vector3Variable_11; }
	inline void set_vector3Variable_11(FsmVector3_t3996534004 * value)
	{
		___vector3Variable_11 = value;
		Il2CppCodeGenWriteBarrier(&___vector3Variable_11, value);
	}

	inline static int32_t get_offset_of_nearest_12() { return static_cast<int32_t>(offsetof(Vector3RoundToNearest_t269491953, ___nearest_12)); }
	inline FsmFloat_t937133978 * get_nearest_12() const { return ___nearest_12; }
	inline FsmFloat_t937133978 ** get_address_of_nearest_12() { return &___nearest_12; }
	inline void set_nearest_12(FsmFloat_t937133978 * value)
	{
		___nearest_12 = value;
		Il2CppCodeGenWriteBarrier(&___nearest_12, value);
	}

	inline static int32_t get_offset_of_resultAsVector3_13() { return static_cast<int32_t>(offsetof(Vector3RoundToNearest_t269491953, ___resultAsVector3_13)); }
	inline FsmVector3_t3996534004 * get_resultAsVector3_13() const { return ___resultAsVector3_13; }
	inline FsmVector3_t3996534004 ** get_address_of_resultAsVector3_13() { return &___resultAsVector3_13; }
	inline void set_resultAsVector3_13(FsmVector3_t3996534004 * value)
	{
		___resultAsVector3_13 = value;
		Il2CppCodeGenWriteBarrier(&___resultAsVector3_13, value);
	}

	inline static int32_t get_offset_of_everyFrame_14() { return static_cast<int32_t>(offsetof(Vector3RoundToNearest_t269491953, ___everyFrame_14)); }
	inline bool get_everyFrame_14() const { return ___everyFrame_14; }
	inline bool* get_address_of_everyFrame_14() { return &___everyFrame_14; }
	inline void set_everyFrame_14(bool value)
	{
		___everyFrame_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
