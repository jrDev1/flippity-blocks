﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListIndexOf
struct  ArrayListIndexOf_t1849268766  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListIndexOf::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListIndexOf::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListIndexOf::startIndex
	FsmInt_t1273009179 * ___startIndex_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListIndexOf::count
	FsmInt_t1273009179 * ___count_15;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayListIndexOf::variable
	FsmVar_t2872592513 * ___variable_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListIndexOf::indexOf
	FsmInt_t1273009179 * ___indexOf_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListIndexOf::itemFound
	FsmEvent_t1258573736 * ___itemFound_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListIndexOf::itemNotFound
	FsmEvent_t1258573736 * ___itemNotFound_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.ArrayListIndexOf::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_20;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_startIndex_14() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___startIndex_14)); }
	inline FsmInt_t1273009179 * get_startIndex_14() const { return ___startIndex_14; }
	inline FsmInt_t1273009179 ** get_address_of_startIndex_14() { return &___startIndex_14; }
	inline void set_startIndex_14(FsmInt_t1273009179 * value)
	{
		___startIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_14, value);
	}

	inline static int32_t get_offset_of_count_15() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___count_15)); }
	inline FsmInt_t1273009179 * get_count_15() const { return ___count_15; }
	inline FsmInt_t1273009179 ** get_address_of_count_15() { return &___count_15; }
	inline void set_count_15(FsmInt_t1273009179 * value)
	{
		___count_15 = value;
		Il2CppCodeGenWriteBarrier(&___count_15, value);
	}

	inline static int32_t get_offset_of_variable_16() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___variable_16)); }
	inline FsmVar_t2872592513 * get_variable_16() const { return ___variable_16; }
	inline FsmVar_t2872592513 ** get_address_of_variable_16() { return &___variable_16; }
	inline void set_variable_16(FsmVar_t2872592513 * value)
	{
		___variable_16 = value;
		Il2CppCodeGenWriteBarrier(&___variable_16, value);
	}

	inline static int32_t get_offset_of_indexOf_17() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___indexOf_17)); }
	inline FsmInt_t1273009179 * get_indexOf_17() const { return ___indexOf_17; }
	inline FsmInt_t1273009179 ** get_address_of_indexOf_17() { return &___indexOf_17; }
	inline void set_indexOf_17(FsmInt_t1273009179 * value)
	{
		___indexOf_17 = value;
		Il2CppCodeGenWriteBarrier(&___indexOf_17, value);
	}

	inline static int32_t get_offset_of_itemFound_18() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___itemFound_18)); }
	inline FsmEvent_t1258573736 * get_itemFound_18() const { return ___itemFound_18; }
	inline FsmEvent_t1258573736 ** get_address_of_itemFound_18() { return &___itemFound_18; }
	inline void set_itemFound_18(FsmEvent_t1258573736 * value)
	{
		___itemFound_18 = value;
		Il2CppCodeGenWriteBarrier(&___itemFound_18, value);
	}

	inline static int32_t get_offset_of_itemNotFound_19() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___itemNotFound_19)); }
	inline FsmEvent_t1258573736 * get_itemNotFound_19() const { return ___itemNotFound_19; }
	inline FsmEvent_t1258573736 ** get_address_of_itemNotFound_19() { return &___itemNotFound_19; }
	inline void set_itemNotFound_19(FsmEvent_t1258573736 * value)
	{
		___itemNotFound_19 = value;
		Il2CppCodeGenWriteBarrier(&___itemNotFound_19, value);
	}

	inline static int32_t get_offset_of_failureEvent_20() { return static_cast<int32_t>(offsetof(ArrayListIndexOf_t1849268766, ___failureEvent_20)); }
	inline FsmEvent_t1258573736 * get_failureEvent_20() const { return ___failureEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_20() { return &___failureEvent_20; }
	inline void set_failureEvent_20(FsmEvent_t1258573736 * value)
	{
		___failureEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
