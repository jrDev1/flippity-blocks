﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// SRDebugger.Services.IProfilerService
struct IProfilerService_t1347272725;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.ProfilerFPSLabel
struct  ProfilerFPSLabel_t3584209780  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Single SRDebugger.UI.ProfilerFPSLabel::_nextUpdate
	float ____nextUpdate_9;
	// SRDebugger.Services.IProfilerService SRDebugger.UI.ProfilerFPSLabel::_profilerService
	Il2CppObject * ____profilerService_10;
	// System.Single SRDebugger.UI.ProfilerFPSLabel::UpdateFrequency
	float ___UpdateFrequency_11;
	// UnityEngine.UI.Text SRDebugger.UI.ProfilerFPSLabel::_text
	Text_t356221433 * ____text_12;

public:
	inline static int32_t get_offset_of__nextUpdate_9() { return static_cast<int32_t>(offsetof(ProfilerFPSLabel_t3584209780, ____nextUpdate_9)); }
	inline float get__nextUpdate_9() const { return ____nextUpdate_9; }
	inline float* get_address_of__nextUpdate_9() { return &____nextUpdate_9; }
	inline void set__nextUpdate_9(float value)
	{
		____nextUpdate_9 = value;
	}

	inline static int32_t get_offset_of__profilerService_10() { return static_cast<int32_t>(offsetof(ProfilerFPSLabel_t3584209780, ____profilerService_10)); }
	inline Il2CppObject * get__profilerService_10() const { return ____profilerService_10; }
	inline Il2CppObject ** get_address_of__profilerService_10() { return &____profilerService_10; }
	inline void set__profilerService_10(Il2CppObject * value)
	{
		____profilerService_10 = value;
		Il2CppCodeGenWriteBarrier(&____profilerService_10, value);
	}

	inline static int32_t get_offset_of_UpdateFrequency_11() { return static_cast<int32_t>(offsetof(ProfilerFPSLabel_t3584209780, ___UpdateFrequency_11)); }
	inline float get_UpdateFrequency_11() const { return ___UpdateFrequency_11; }
	inline float* get_address_of_UpdateFrequency_11() { return &___UpdateFrequency_11; }
	inline void set_UpdateFrequency_11(float value)
	{
		___UpdateFrequency_11 = value;
	}

	inline static int32_t get_offset_of__text_12() { return static_cast<int32_t>(offsetof(ProfilerFPSLabel_t3584209780, ____text_12)); }
	inline Text_t356221433 * get__text_12() const { return ____text_12; }
	inline Text_t356221433 ** get_address_of__text_12() { return &____text_12; }
	inline void set__text_12(Text_t356221433 * value)
	{
		____text_12 = value;
		Il2CppCodeGenWriteBarrier(&____text_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
