﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen3786579381.h"

// System.Collections.Generic.List`1<SRDebugger.Settings/KeyboardShortcut>
struct List_1_t1881242303;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.KeyboardShortcutListenerService
struct  KeyboardShortcutListenerService_t113027528  : public SRServiceBase_1_t3786579381
{
public:
	// System.Collections.Generic.List`1<SRDebugger.Settings/KeyboardShortcut> SRDebugger.Services.Implementation.KeyboardShortcutListenerService::_shortcuts
	List_1_t1881242303 * ____shortcuts_9;

public:
	inline static int32_t get_offset_of__shortcuts_9() { return static_cast<int32_t>(offsetof(KeyboardShortcutListenerService_t113027528, ____shortcuts_9)); }
	inline List_1_t1881242303 * get__shortcuts_9() const { return ____shortcuts_9; }
	inline List_1_t1881242303 ** get_address_of__shortcuts_9() { return &____shortcuts_9; }
	inline void set__shortcuts_9(List_1_t1881242303 * value)
	{
		____shortcuts_9 = value;
		Il2CppCodeGenWriteBarrier(&____shortcuts_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
