﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// UnityEngine.UI.Graphic
struct Graphic_t2426225576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.InheritColour
struct  InheritColour_t1066420259  : public SRMonoBehaviour_t2352136145
{
public:
	// UnityEngine.UI.Graphic SRF.UI.InheritColour::_graphic
	Graphic_t2426225576 * ____graphic_8;
	// UnityEngine.UI.Graphic SRF.UI.InheritColour::From
	Graphic_t2426225576 * ___From_9;

public:
	inline static int32_t get_offset_of__graphic_8() { return static_cast<int32_t>(offsetof(InheritColour_t1066420259, ____graphic_8)); }
	inline Graphic_t2426225576 * get__graphic_8() const { return ____graphic_8; }
	inline Graphic_t2426225576 ** get_address_of__graphic_8() { return &____graphic_8; }
	inline void set__graphic_8(Graphic_t2426225576 * value)
	{
		____graphic_8 = value;
		Il2CppCodeGenWriteBarrier(&____graphic_8, value);
	}

	inline static int32_t get_offset_of_From_9() { return static_cast<int32_t>(offsetof(InheritColour_t1066420259, ___From_9)); }
	inline Graphic_t2426225576 * get_From_9() const { return ___From_9; }
	inline Graphic_t2426225576 ** get_address_of_From_9() { return &___From_9; }
	inline void set_From_9(Graphic_t2426225576 * value)
	{
		___From_9 = value;
		Il2CppCodeGenWriteBarrier(&___From_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
