﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_OptionsCo3483301704.h"

// System.Object
struct Il2CppObject;
// SRF.Helpers.PropertyReference
struct PropertyReference_t1009137956;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.DataBoundControl
struct  DataBoundControl_t3384854171  : public OptionsControlBase_t3483301704
{
public:
	// System.Boolean SRDebugger.UI.Controls.DataBoundControl::_hasStarted
	bool ____hasStarted_12;
	// System.Boolean SRDebugger.UI.Controls.DataBoundControl::_isReadOnly
	bool ____isReadOnly_13;
	// System.Object SRDebugger.UI.Controls.DataBoundControl::_prevValue
	Il2CppObject * ____prevValue_14;
	// SRF.Helpers.PropertyReference SRDebugger.UI.Controls.DataBoundControl::_prop
	PropertyReference_t1009137956 * ____prop_15;
	// System.String SRDebugger.UI.Controls.DataBoundControl::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of__hasStarted_12() { return static_cast<int32_t>(offsetof(DataBoundControl_t3384854171, ____hasStarted_12)); }
	inline bool get__hasStarted_12() const { return ____hasStarted_12; }
	inline bool* get_address_of__hasStarted_12() { return &____hasStarted_12; }
	inline void set__hasStarted_12(bool value)
	{
		____hasStarted_12 = value;
	}

	inline static int32_t get_offset_of__isReadOnly_13() { return static_cast<int32_t>(offsetof(DataBoundControl_t3384854171, ____isReadOnly_13)); }
	inline bool get__isReadOnly_13() const { return ____isReadOnly_13; }
	inline bool* get_address_of__isReadOnly_13() { return &____isReadOnly_13; }
	inline void set__isReadOnly_13(bool value)
	{
		____isReadOnly_13 = value;
	}

	inline static int32_t get_offset_of__prevValue_14() { return static_cast<int32_t>(offsetof(DataBoundControl_t3384854171, ____prevValue_14)); }
	inline Il2CppObject * get__prevValue_14() const { return ____prevValue_14; }
	inline Il2CppObject ** get_address_of__prevValue_14() { return &____prevValue_14; }
	inline void set__prevValue_14(Il2CppObject * value)
	{
		____prevValue_14 = value;
		Il2CppCodeGenWriteBarrier(&____prevValue_14, value);
	}

	inline static int32_t get_offset_of__prop_15() { return static_cast<int32_t>(offsetof(DataBoundControl_t3384854171, ____prop_15)); }
	inline PropertyReference_t1009137956 * get__prop_15() const { return ____prop_15; }
	inline PropertyReference_t1009137956 ** get_address_of__prop_15() { return &____prop_15; }
	inline void set__prop_15(PropertyReference_t1009137956 * value)
	{
		____prop_15 = value;
		Il2CppCodeGenWriteBarrier(&____prop_15, value);
	}

	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DataBoundControl_t3384854171, ___U3CPropertyNameU3Ek__BackingField_16)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_16() const { return ___U3CPropertyNameU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_16() { return &___U3CPropertyNameU3Ek__BackingField_16; }
	inline void set_U3CPropertyNameU3Ek__BackingField_16(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertyNameU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
