﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnCreatedDelegateExample
struct  OnCreatedDelegateExample_t2429582238  : public MonoBehaviour_t1158329972
{
public:
	// System.String OnCreatedDelegateExample::poolName
	String_t* ___poolName_2;

public:
	inline static int32_t get_offset_of_poolName_2() { return static_cast<int32_t>(offsetof(OnCreatedDelegateExample_t2429582238, ___poolName_2)); }
	inline String_t* get_poolName_2() const { return ___poolName_2; }
	inline String_t** get_address_of_poolName_2() { return &___poolName_2; }
	inline void set_poolName_2(String_t* value)
	{
		___poolName_2 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
