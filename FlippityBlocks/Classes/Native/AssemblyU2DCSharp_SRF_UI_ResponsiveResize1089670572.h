﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_UI_ResponsiveBase1813519265.h"

// SRF.UI.ResponsiveResize/Element[]
struct ElementU5BU5D_t94344283;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ResponsiveResize
struct  ResponsiveResize_t1089670572  : public ResponsiveBase_t1813519265
{
public:
	// SRF.UI.ResponsiveResize/Element[] SRF.UI.ResponsiveResize::Elements
	ElementU5BU5D_t94344283* ___Elements_9;

public:
	inline static int32_t get_offset_of_Elements_9() { return static_cast<int32_t>(offsetof(ResponsiveResize_t1089670572, ___Elements_9)); }
	inline ElementU5BU5D_t94344283* get_Elements_9() const { return ___Elements_9; }
	inline ElementU5BU5D_t94344283** get_address_of_Elements_9() { return &___Elements_9; }
	inline void set_Elements_9(ElementU5BU5D_t94344283* value)
	{
		___Elements_9 = value;
		Il2CppCodeGenWriteBarrier(&___Elements_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
