﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRF.Service.ServiceSelectorAttribute
struct ServiceSelectorAttribute_t1476084060;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRServiceManager/<ScanTypeForSelectors>c__AnonStorey0
struct  U3CScanTypeForSelectorsU3Ec__AnonStorey0_t4068173263  : public Il2CppObject
{
public:
	// SRF.Service.ServiceSelectorAttribute SRF.Service.SRServiceManager/<ScanTypeForSelectors>c__AnonStorey0::attrib
	ServiceSelectorAttribute_t1476084060 * ___attrib_0;

public:
	inline static int32_t get_offset_of_attrib_0() { return static_cast<int32_t>(offsetof(U3CScanTypeForSelectorsU3Ec__AnonStorey0_t4068173263, ___attrib_0)); }
	inline ServiceSelectorAttribute_t1476084060 * get_attrib_0() const { return ___attrib_0; }
	inline ServiceSelectorAttribute_t1476084060 ** get_address_of_attrib_0() { return &___attrib_0; }
	inline void set_attrib_0(ServiceSelectorAttribute_t1476084060 * value)
	{
		___attrib_0 = value;
		Il2CppCodeGenWriteBarrier(&___attrib_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
