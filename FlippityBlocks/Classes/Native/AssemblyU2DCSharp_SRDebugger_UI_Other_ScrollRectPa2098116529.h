﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Mask
struct Mask_t2977958238;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.ScrollRectPatch
struct  ScrollRectPatch_t2098116529  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform SRDebugger.UI.Other.ScrollRectPatch::Content
	RectTransform_t3349966182 * ___Content_2;
	// UnityEngine.UI.Mask SRDebugger.UI.Other.ScrollRectPatch::ReplaceMask
	Mask_t2977958238 * ___ReplaceMask_3;
	// UnityEngine.RectTransform SRDebugger.UI.Other.ScrollRectPatch::Viewport
	RectTransform_t3349966182 * ___Viewport_4;

public:
	inline static int32_t get_offset_of_Content_2() { return static_cast<int32_t>(offsetof(ScrollRectPatch_t2098116529, ___Content_2)); }
	inline RectTransform_t3349966182 * get_Content_2() const { return ___Content_2; }
	inline RectTransform_t3349966182 ** get_address_of_Content_2() { return &___Content_2; }
	inline void set_Content_2(RectTransform_t3349966182 * value)
	{
		___Content_2 = value;
		Il2CppCodeGenWriteBarrier(&___Content_2, value);
	}

	inline static int32_t get_offset_of_ReplaceMask_3() { return static_cast<int32_t>(offsetof(ScrollRectPatch_t2098116529, ___ReplaceMask_3)); }
	inline Mask_t2977958238 * get_ReplaceMask_3() const { return ___ReplaceMask_3; }
	inline Mask_t2977958238 ** get_address_of_ReplaceMask_3() { return &___ReplaceMask_3; }
	inline void set_ReplaceMask_3(Mask_t2977958238 * value)
	{
		___ReplaceMask_3 = value;
		Il2CppCodeGenWriteBarrier(&___ReplaceMask_3, value);
	}

	inline static int32_t get_offset_of_Viewport_4() { return static_cast<int32_t>(offsetof(ScrollRectPatch_t2098116529, ___Viewport_4)); }
	inline RectTransform_t3349966182 * get_Viewport_4() const { return ___Viewport_4; }
	inline RectTransform_t3349966182 ** get_address_of_Viewport_4() { return &___Viewport_4; }
	inline void set_Viewport_4(RectTransform_t3349966182 * value)
	{
		___Viewport_4 = value;
		Il2CppCodeGenWriteBarrier(&___Viewport_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
