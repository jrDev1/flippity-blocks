﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;
// HutongGames.PlayMaker.FsmVar[]
struct FsmVarU5BU5D_t16885852;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableGetMany
struct  HashTableGetMany_t220573833  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableGetMany::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableGetMany::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.Actions.HashTableGetMany::keys
	FsmStringU5BU5D_t2699231328* ___keys_14;
	// HutongGames.PlayMaker.FsmVar[] HutongGames.PlayMaker.Actions.HashTableGetMany::results
	FsmVarU5BU5D_t16885852* ___results_15;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableGetMany_t220573833, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableGetMany_t220573833, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_keys_14() { return static_cast<int32_t>(offsetof(HashTableGetMany_t220573833, ___keys_14)); }
	inline FsmStringU5BU5D_t2699231328* get_keys_14() const { return ___keys_14; }
	inline FsmStringU5BU5D_t2699231328** get_address_of_keys_14() { return &___keys_14; }
	inline void set_keys_14(FsmStringU5BU5D_t2699231328* value)
	{
		___keys_14 = value;
		Il2CppCodeGenWriteBarrier(&___keys_14, value);
	}

	inline static int32_t get_offset_of_results_15() { return static_cast<int32_t>(offsetof(HashTableGetMany_t220573833, ___results_15)); }
	inline FsmVarU5BU5D_t16885852* get_results_15() const { return ___results_15; }
	inline FsmVarU5BU5D_t16885852** get_address_of_results_15() { return &___results_15; }
	inline void set_results_15(FsmVarU5BU5D_t16885852* value)
	{
		___results_15 = value;
		Il2CppCodeGenWriteBarrier(&___results_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
