﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.EventHandler
struct EventHandler_t277755526;
// System.EventHandler`1<System.ComponentModel.PropertyChangedEventArgs>
struct EventHandler_1_t280753604;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.ICollection`1<SRDebugger.Internal.OptionDefinition>>
struct Dictionary_2_t458543741;
// System.Collections.Generic.List`1<SRDebugger.Internal.OptionDefinition>
struct List_1_t3578496736;
// System.Collections.Generic.IList`1<SRDebugger.Internal.OptionDefinition>
struct IList_1_t455348909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.OptionsServiceImpl
struct  OptionsServiceImpl_t3177763977  : public Il2CppObject
{
public:
	// System.EventHandler SRDebugger.Services.Implementation.OptionsServiceImpl::OptionsUpdated
	EventHandler_t277755526 * ___OptionsUpdated_0;
	// System.EventHandler`1<System.ComponentModel.PropertyChangedEventArgs> SRDebugger.Services.Implementation.OptionsServiceImpl::OptionsValueUpdated
	EventHandler_1_t280753604 * ___OptionsValueUpdated_1;
	// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.ICollection`1<SRDebugger.Internal.OptionDefinition>> SRDebugger.Services.Implementation.OptionsServiceImpl::_optionContainerLookup
	Dictionary_2_t458543741 * ____optionContainerLookup_2;
	// System.Collections.Generic.List`1<SRDebugger.Internal.OptionDefinition> SRDebugger.Services.Implementation.OptionsServiceImpl::_options
	List_1_t3578496736 * ____options_3;
	// System.Collections.Generic.IList`1<SRDebugger.Internal.OptionDefinition> SRDebugger.Services.Implementation.OptionsServiceImpl::_optionsReadonly
	Il2CppObject* ____optionsReadonly_4;

public:
	inline static int32_t get_offset_of_OptionsUpdated_0() { return static_cast<int32_t>(offsetof(OptionsServiceImpl_t3177763977, ___OptionsUpdated_0)); }
	inline EventHandler_t277755526 * get_OptionsUpdated_0() const { return ___OptionsUpdated_0; }
	inline EventHandler_t277755526 ** get_address_of_OptionsUpdated_0() { return &___OptionsUpdated_0; }
	inline void set_OptionsUpdated_0(EventHandler_t277755526 * value)
	{
		___OptionsUpdated_0 = value;
		Il2CppCodeGenWriteBarrier(&___OptionsUpdated_0, value);
	}

	inline static int32_t get_offset_of_OptionsValueUpdated_1() { return static_cast<int32_t>(offsetof(OptionsServiceImpl_t3177763977, ___OptionsValueUpdated_1)); }
	inline EventHandler_1_t280753604 * get_OptionsValueUpdated_1() const { return ___OptionsValueUpdated_1; }
	inline EventHandler_1_t280753604 ** get_address_of_OptionsValueUpdated_1() { return &___OptionsValueUpdated_1; }
	inline void set_OptionsValueUpdated_1(EventHandler_1_t280753604 * value)
	{
		___OptionsValueUpdated_1 = value;
		Il2CppCodeGenWriteBarrier(&___OptionsValueUpdated_1, value);
	}

	inline static int32_t get_offset_of__optionContainerLookup_2() { return static_cast<int32_t>(offsetof(OptionsServiceImpl_t3177763977, ____optionContainerLookup_2)); }
	inline Dictionary_2_t458543741 * get__optionContainerLookup_2() const { return ____optionContainerLookup_2; }
	inline Dictionary_2_t458543741 ** get_address_of__optionContainerLookup_2() { return &____optionContainerLookup_2; }
	inline void set__optionContainerLookup_2(Dictionary_2_t458543741 * value)
	{
		____optionContainerLookup_2 = value;
		Il2CppCodeGenWriteBarrier(&____optionContainerLookup_2, value);
	}

	inline static int32_t get_offset_of__options_3() { return static_cast<int32_t>(offsetof(OptionsServiceImpl_t3177763977, ____options_3)); }
	inline List_1_t3578496736 * get__options_3() const { return ____options_3; }
	inline List_1_t3578496736 ** get_address_of__options_3() { return &____options_3; }
	inline void set__options_3(List_1_t3578496736 * value)
	{
		____options_3 = value;
		Il2CppCodeGenWriteBarrier(&____options_3, value);
	}

	inline static int32_t get_offset_of__optionsReadonly_4() { return static_cast<int32_t>(offsetof(OptionsServiceImpl_t3177763977, ____optionsReadonly_4)); }
	inline Il2CppObject* get__optionsReadonly_4() const { return ____optionsReadonly_4; }
	inline Il2CppObject** get_address_of__optionsReadonly_4() { return &____optionsReadonly_4; }
	inline void set__optionsReadonly_4(Il2CppObject* value)
	{
		____optionsReadonly_4 = value;
		Il2CppCodeGenWriteBarrier(&____optionsReadonly_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
