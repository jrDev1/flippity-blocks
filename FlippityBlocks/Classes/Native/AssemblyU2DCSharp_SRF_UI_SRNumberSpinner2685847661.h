﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_InputField1631627530.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.SRNumberSpinner
struct  SRNumberSpinner_t2685847661  : public InputField_t1631627530
{
public:
	// System.Double SRF.UI.SRNumberSpinner::_currentValue
	double ____currentValue_63;
	// System.Double SRF.UI.SRNumberSpinner::_dragStartAmount
	double ____dragStartAmount_64;
	// System.Double SRF.UI.SRNumberSpinner::_dragStep
	double ____dragStep_65;
	// System.Single SRF.UI.SRNumberSpinner::DragSensitivity
	float ___DragSensitivity_66;
	// System.Double SRF.UI.SRNumberSpinner::MaxValue
	double ___MaxValue_67;
	// System.Double SRF.UI.SRNumberSpinner::MinValue
	double ___MinValue_68;

public:
	inline static int32_t get_offset_of__currentValue_63() { return static_cast<int32_t>(offsetof(SRNumberSpinner_t2685847661, ____currentValue_63)); }
	inline double get__currentValue_63() const { return ____currentValue_63; }
	inline double* get_address_of__currentValue_63() { return &____currentValue_63; }
	inline void set__currentValue_63(double value)
	{
		____currentValue_63 = value;
	}

	inline static int32_t get_offset_of__dragStartAmount_64() { return static_cast<int32_t>(offsetof(SRNumberSpinner_t2685847661, ____dragStartAmount_64)); }
	inline double get__dragStartAmount_64() const { return ____dragStartAmount_64; }
	inline double* get_address_of__dragStartAmount_64() { return &____dragStartAmount_64; }
	inline void set__dragStartAmount_64(double value)
	{
		____dragStartAmount_64 = value;
	}

	inline static int32_t get_offset_of__dragStep_65() { return static_cast<int32_t>(offsetof(SRNumberSpinner_t2685847661, ____dragStep_65)); }
	inline double get__dragStep_65() const { return ____dragStep_65; }
	inline double* get_address_of__dragStep_65() { return &____dragStep_65; }
	inline void set__dragStep_65(double value)
	{
		____dragStep_65 = value;
	}

	inline static int32_t get_offset_of_DragSensitivity_66() { return static_cast<int32_t>(offsetof(SRNumberSpinner_t2685847661, ___DragSensitivity_66)); }
	inline float get_DragSensitivity_66() const { return ___DragSensitivity_66; }
	inline float* get_address_of_DragSensitivity_66() { return &___DragSensitivity_66; }
	inline void set_DragSensitivity_66(float value)
	{
		___DragSensitivity_66 = value;
	}

	inline static int32_t get_offset_of_MaxValue_67() { return static_cast<int32_t>(offsetof(SRNumberSpinner_t2685847661, ___MaxValue_67)); }
	inline double get_MaxValue_67() const { return ___MaxValue_67; }
	inline double* get_address_of_MaxValue_67() { return &___MaxValue_67; }
	inline void set_MaxValue_67(double value)
	{
		___MaxValue_67 = value;
	}

	inline static int32_t get_offset_of_MinValue_68() { return static_cast<int32_t>(offsetof(SRNumberSpinner_t2685847661, ___MinValue_68)); }
	inline double get_MinValue_68() const { return ___MinValue_68; }
	inline double* get_address_of_MinValue_68() { return &___MinValue_68; }
	inline void set_MinValue_68(double value)
	{
		___MinValue_68 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
