﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.NumberControl/ValueRange
struct  ValueRange_t1714395645 
{
public:
	// System.Double SRDebugger.UI.Controls.Data.NumberControl/ValueRange::MaxValue
	double ___MaxValue_0;
	// System.Double SRDebugger.UI.Controls.Data.NumberControl/ValueRange::MinValue
	double ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(ValueRange_t1714395645, ___MaxValue_0)); }
	inline double get_MaxValue_0() const { return ___MaxValue_0; }
	inline double* get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(double value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(ValueRange_t1714395645, ___MinValue_1)); }
	inline double get_MinValue_1() const { return ___MinValue_1; }
	inline double* get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(double value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
