﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Collider2D
struct Collider2D_t646061738;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRMonoBehaviour
struct  SRMonoBehaviour_t2352136145  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Collider SRF.SRMonoBehaviour::_collider
	Collider_t3497673348 * ____collider_2;
	// UnityEngine.Transform SRF.SRMonoBehaviour::_transform
	Transform_t3275118058 * ____transform_3;
	// UnityEngine.Rigidbody SRF.SRMonoBehaviour::_rigidBody
	Rigidbody_t4233889191 * ____rigidBody_4;
	// UnityEngine.GameObject SRF.SRMonoBehaviour::_gameObject
	GameObject_t1756533147 * ____gameObject_5;
	// UnityEngine.Rigidbody2D SRF.SRMonoBehaviour::_rigidbody2D
	Rigidbody2D_t502193897 * ____rigidbody2D_6;
	// UnityEngine.Collider2D SRF.SRMonoBehaviour::_collider2D
	Collider2D_t646061738 * ____collider2D_7;

public:
	inline static int32_t get_offset_of__collider_2() { return static_cast<int32_t>(offsetof(SRMonoBehaviour_t2352136145, ____collider_2)); }
	inline Collider_t3497673348 * get__collider_2() const { return ____collider_2; }
	inline Collider_t3497673348 ** get_address_of__collider_2() { return &____collider_2; }
	inline void set__collider_2(Collider_t3497673348 * value)
	{
		____collider_2 = value;
		Il2CppCodeGenWriteBarrier(&____collider_2, value);
	}

	inline static int32_t get_offset_of__transform_3() { return static_cast<int32_t>(offsetof(SRMonoBehaviour_t2352136145, ____transform_3)); }
	inline Transform_t3275118058 * get__transform_3() const { return ____transform_3; }
	inline Transform_t3275118058 ** get_address_of__transform_3() { return &____transform_3; }
	inline void set__transform_3(Transform_t3275118058 * value)
	{
		____transform_3 = value;
		Il2CppCodeGenWriteBarrier(&____transform_3, value);
	}

	inline static int32_t get_offset_of__rigidBody_4() { return static_cast<int32_t>(offsetof(SRMonoBehaviour_t2352136145, ____rigidBody_4)); }
	inline Rigidbody_t4233889191 * get__rigidBody_4() const { return ____rigidBody_4; }
	inline Rigidbody_t4233889191 ** get_address_of__rigidBody_4() { return &____rigidBody_4; }
	inline void set__rigidBody_4(Rigidbody_t4233889191 * value)
	{
		____rigidBody_4 = value;
		Il2CppCodeGenWriteBarrier(&____rigidBody_4, value);
	}

	inline static int32_t get_offset_of__gameObject_5() { return static_cast<int32_t>(offsetof(SRMonoBehaviour_t2352136145, ____gameObject_5)); }
	inline GameObject_t1756533147 * get__gameObject_5() const { return ____gameObject_5; }
	inline GameObject_t1756533147 ** get_address_of__gameObject_5() { return &____gameObject_5; }
	inline void set__gameObject_5(GameObject_t1756533147 * value)
	{
		____gameObject_5 = value;
		Il2CppCodeGenWriteBarrier(&____gameObject_5, value);
	}

	inline static int32_t get_offset_of__rigidbody2D_6() { return static_cast<int32_t>(offsetof(SRMonoBehaviour_t2352136145, ____rigidbody2D_6)); }
	inline Rigidbody2D_t502193897 * get__rigidbody2D_6() const { return ____rigidbody2D_6; }
	inline Rigidbody2D_t502193897 ** get_address_of__rigidbody2D_6() { return &____rigidbody2D_6; }
	inline void set__rigidbody2D_6(Rigidbody2D_t502193897 * value)
	{
		____rigidbody2D_6 = value;
		Il2CppCodeGenWriteBarrier(&____rigidbody2D_6, value);
	}

	inline static int32_t get_offset_of__collider2D_7() { return static_cast<int32_t>(offsetof(SRMonoBehaviour_t2352136145, ____collider2D_7)); }
	inline Collider2D_t646061738 * get__collider2D_7() const { return ____collider2D_7; }
	inline Collider2D_t646061738 ** get_address_of__collider2D_7() { return &____collider2D_7; }
	inline void set__collider2D_7(Collider2D_t646061738 * value)
	{
		____collider2D_7 = value;
		Il2CppCodeGenWriteBarrier(&____collider2D_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
