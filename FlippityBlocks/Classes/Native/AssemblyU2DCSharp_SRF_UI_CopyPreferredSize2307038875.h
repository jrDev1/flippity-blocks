﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.CopyPreferredSize
struct  CopyPreferredSize_t2307038875  : public LayoutElement_t2808691390
{
public:
	// UnityEngine.RectTransform SRF.UI.CopyPreferredSize::CopySource
	RectTransform_t3349966182 * ___CopySource_9;
	// System.Single SRF.UI.CopyPreferredSize::PaddingHeight
	float ___PaddingHeight_10;
	// System.Single SRF.UI.CopyPreferredSize::PaddingWidth
	float ___PaddingWidth_11;

public:
	inline static int32_t get_offset_of_CopySource_9() { return static_cast<int32_t>(offsetof(CopyPreferredSize_t2307038875, ___CopySource_9)); }
	inline RectTransform_t3349966182 * get_CopySource_9() const { return ___CopySource_9; }
	inline RectTransform_t3349966182 ** get_address_of_CopySource_9() { return &___CopySource_9; }
	inline void set_CopySource_9(RectTransform_t3349966182 * value)
	{
		___CopySource_9 = value;
		Il2CppCodeGenWriteBarrier(&___CopySource_9, value);
	}

	inline static int32_t get_offset_of_PaddingHeight_10() { return static_cast<int32_t>(offsetof(CopyPreferredSize_t2307038875, ___PaddingHeight_10)); }
	inline float get_PaddingHeight_10() const { return ___PaddingHeight_10; }
	inline float* get_address_of_PaddingHeight_10() { return &___PaddingHeight_10; }
	inline void set_PaddingHeight_10(float value)
	{
		___PaddingHeight_10 = value;
	}

	inline static int32_t get_offset_of_PaddingWidth_11() { return static_cast<int32_t>(offsetof(CopyPreferredSize_t2307038875, ___PaddingWidth_11)); }
	inline float get_PaddingWidth_11() const { return ___PaddingWidth_11; }
	inline float* get_address_of_PaddingWidth_11() { return &___PaddingWidth_11; }
	inline void set_PaddingWidth_11(float value)
	{
		___PaddingWidth_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
