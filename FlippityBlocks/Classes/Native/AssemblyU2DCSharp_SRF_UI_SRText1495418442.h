﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"

// System.Action`1<SRF.UI.SRText>
struct Action_1_t1297217824;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.SRText
struct  SRText_t1495418442  : public Text_t356221433
{
public:
	// System.Action`1<SRF.UI.SRText> SRF.UI.SRText::LayoutDirty
	Action_1_t1297217824 * ___LayoutDirty_35;

public:
	inline static int32_t get_offset_of_LayoutDirty_35() { return static_cast<int32_t>(offsetof(SRText_t1495418442, ___LayoutDirty_35)); }
	inline Action_1_t1297217824 * get_LayoutDirty_35() const { return ___LayoutDirty_35; }
	inline Action_1_t1297217824 ** get_address_of_LayoutDirty_35() { return &___LayoutDirty_35; }
	inline void set_LayoutDirty_35(Action_1_t1297217824 * value)
	{
		___LayoutDirty_35 = value;
		Il2CppCodeGenWriteBarrier(&___LayoutDirty_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
