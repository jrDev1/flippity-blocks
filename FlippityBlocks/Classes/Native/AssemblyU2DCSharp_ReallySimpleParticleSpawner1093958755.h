﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReallySimpleParticleSpawner
struct  ReallySimpleParticleSpawner_t1093958755  : public MonoBehaviour_t1158329972
{
public:
	// System.String ReallySimpleParticleSpawner::poolName
	String_t* ___poolName_2;
	// UnityEngine.ParticleSystem ReallySimpleParticleSpawner::prefab
	ParticleSystem_t3394631041 * ___prefab_3;
	// System.Single ReallySimpleParticleSpawner::spawnInterval
	float ___spawnInterval_4;

public:
	inline static int32_t get_offset_of_poolName_2() { return static_cast<int32_t>(offsetof(ReallySimpleParticleSpawner_t1093958755, ___poolName_2)); }
	inline String_t* get_poolName_2() const { return ___poolName_2; }
	inline String_t** get_address_of_poolName_2() { return &___poolName_2; }
	inline void set_poolName_2(String_t* value)
	{
		___poolName_2 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_2, value);
	}

	inline static int32_t get_offset_of_prefab_3() { return static_cast<int32_t>(offsetof(ReallySimpleParticleSpawner_t1093958755, ___prefab_3)); }
	inline ParticleSystem_t3394631041 * get_prefab_3() const { return ___prefab_3; }
	inline ParticleSystem_t3394631041 ** get_address_of_prefab_3() { return &___prefab_3; }
	inline void set_prefab_3(ParticleSystem_t3394631041 * value)
	{
		___prefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_3, value);
	}

	inline static int32_t get_offset_of_spawnInterval_4() { return static_cast<int32_t>(offsetof(ReallySimpleParticleSpawner_t1093958755, ___spawnInterval_4)); }
	inline float get_spawnInterval_4() const { return ___spawnInterval_4; }
	inline float* get_address_of_spawnInterval_4() { return &___spawnInterval_4; }
	inline void set_spawnInterval_4(float value)
	{
		___spawnInterval_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
