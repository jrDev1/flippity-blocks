﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReallySimpleAudioSpawner
struct  ReallySimpleAudioSpawner_t1135031217  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource ReallySimpleAudioSpawner::prefab
	AudioSource_t1135106623 * ___prefab_2;
	// UnityEngine.AudioSource ReallySimpleAudioSpawner::musicPrefab
	AudioSource_t1135106623 * ___musicPrefab_3;
	// System.Single ReallySimpleAudioSpawner::spawnInterval
	float ___spawnInterval_4;
	// PathologicalGames.SpawnPool ReallySimpleAudioSpawner::pool
	SpawnPool_t2419717525 * ___pool_5;

public:
	inline static int32_t get_offset_of_prefab_2() { return static_cast<int32_t>(offsetof(ReallySimpleAudioSpawner_t1135031217, ___prefab_2)); }
	inline AudioSource_t1135106623 * get_prefab_2() const { return ___prefab_2; }
	inline AudioSource_t1135106623 ** get_address_of_prefab_2() { return &___prefab_2; }
	inline void set_prefab_2(AudioSource_t1135106623 * value)
	{
		___prefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_2, value);
	}

	inline static int32_t get_offset_of_musicPrefab_3() { return static_cast<int32_t>(offsetof(ReallySimpleAudioSpawner_t1135031217, ___musicPrefab_3)); }
	inline AudioSource_t1135106623 * get_musicPrefab_3() const { return ___musicPrefab_3; }
	inline AudioSource_t1135106623 ** get_address_of_musicPrefab_3() { return &___musicPrefab_3; }
	inline void set_musicPrefab_3(AudioSource_t1135106623 * value)
	{
		___musicPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___musicPrefab_3, value);
	}

	inline static int32_t get_offset_of_spawnInterval_4() { return static_cast<int32_t>(offsetof(ReallySimpleAudioSpawner_t1135031217, ___spawnInterval_4)); }
	inline float get_spawnInterval_4() const { return ___spawnInterval_4; }
	inline float* get_address_of_spawnInterval_4() { return &___spawnInterval_4; }
	inline void set_spawnInterval_4(float value)
	{
		___spawnInterval_4 = value;
	}

	inline static int32_t get_offset_of_pool_5() { return static_cast<int32_t>(offsetof(ReallySimpleAudioSpawner_t1135031217, ___pool_5)); }
	inline SpawnPool_t2419717525 * get_pool_5() const { return ___pool_5; }
	inline SpawnPool_t2419717525 ** get_address_of_pool_5() { return &___pool_5; }
	inline void set_pool_5(SpawnPool_t2419717525 * value)
	{
		___pool_5 = value;
		Il2CppCodeGenWriteBarrier(&___pool_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
