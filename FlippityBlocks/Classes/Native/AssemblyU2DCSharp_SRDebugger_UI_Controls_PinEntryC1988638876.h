﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t1971649997;
// SRDebugger.UI.Controls.PinEntryControlCallback
struct PinEntryControlCallback_t496076559;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.PinEntryControl
struct  PinEntryControl_t1988638876  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Boolean SRDebugger.UI.Controls.PinEntryControl::_isVisible
	bool ____isVisible_9;
	// System.Collections.Generic.List`1<System.Int32> SRDebugger.UI.Controls.PinEntryControl::_numbers
	List_1_t1440998580 * ____numbers_10;
	// UnityEngine.UI.Image SRDebugger.UI.Controls.PinEntryControl::Background
	Image_t2042527209 * ___Background_11;
	// System.Boolean SRDebugger.UI.Controls.PinEntryControl::CanCancel
	bool ___CanCancel_12;
	// UnityEngine.UI.Button SRDebugger.UI.Controls.PinEntryControl::CancelButton
	Button_t2872111280 * ___CancelButton_13;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.PinEntryControl::CancelButtonText
	Text_t356221433 * ___CancelButtonText_14;
	// UnityEngine.CanvasGroup SRDebugger.UI.Controls.PinEntryControl::CanvasGroup
	CanvasGroup_t3296560743 * ___CanvasGroup_15;
	// UnityEngine.Animator SRDebugger.UI.Controls.PinEntryControl::DotAnimator
	Animator_t69676727 * ___DotAnimator_16;
	// UnityEngine.UI.Button[] SRDebugger.UI.Controls.PinEntryControl::NumberButtons
	ButtonU5BU5D_t3071100561* ___NumberButtons_17;
	// UnityEngine.UI.Toggle[] SRDebugger.UI.Controls.PinEntryControl::NumberDots
	ToggleU5BU5D_t1971649997* ___NumberDots_18;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.PinEntryControl::PromptText
	Text_t356221433 * ___PromptText_19;
	// SRDebugger.UI.Controls.PinEntryControlCallback SRDebugger.UI.Controls.PinEntryControl::Complete
	PinEntryControlCallback_t496076559 * ___Complete_20;

public:
	inline static int32_t get_offset_of__isVisible_9() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ____isVisible_9)); }
	inline bool get__isVisible_9() const { return ____isVisible_9; }
	inline bool* get_address_of__isVisible_9() { return &____isVisible_9; }
	inline void set__isVisible_9(bool value)
	{
		____isVisible_9 = value;
	}

	inline static int32_t get_offset_of__numbers_10() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ____numbers_10)); }
	inline List_1_t1440998580 * get__numbers_10() const { return ____numbers_10; }
	inline List_1_t1440998580 ** get_address_of__numbers_10() { return &____numbers_10; }
	inline void set__numbers_10(List_1_t1440998580 * value)
	{
		____numbers_10 = value;
		Il2CppCodeGenWriteBarrier(&____numbers_10, value);
	}

	inline static int32_t get_offset_of_Background_11() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___Background_11)); }
	inline Image_t2042527209 * get_Background_11() const { return ___Background_11; }
	inline Image_t2042527209 ** get_address_of_Background_11() { return &___Background_11; }
	inline void set_Background_11(Image_t2042527209 * value)
	{
		___Background_11 = value;
		Il2CppCodeGenWriteBarrier(&___Background_11, value);
	}

	inline static int32_t get_offset_of_CanCancel_12() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___CanCancel_12)); }
	inline bool get_CanCancel_12() const { return ___CanCancel_12; }
	inline bool* get_address_of_CanCancel_12() { return &___CanCancel_12; }
	inline void set_CanCancel_12(bool value)
	{
		___CanCancel_12 = value;
	}

	inline static int32_t get_offset_of_CancelButton_13() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___CancelButton_13)); }
	inline Button_t2872111280 * get_CancelButton_13() const { return ___CancelButton_13; }
	inline Button_t2872111280 ** get_address_of_CancelButton_13() { return &___CancelButton_13; }
	inline void set_CancelButton_13(Button_t2872111280 * value)
	{
		___CancelButton_13 = value;
		Il2CppCodeGenWriteBarrier(&___CancelButton_13, value);
	}

	inline static int32_t get_offset_of_CancelButtonText_14() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___CancelButtonText_14)); }
	inline Text_t356221433 * get_CancelButtonText_14() const { return ___CancelButtonText_14; }
	inline Text_t356221433 ** get_address_of_CancelButtonText_14() { return &___CancelButtonText_14; }
	inline void set_CancelButtonText_14(Text_t356221433 * value)
	{
		___CancelButtonText_14 = value;
		Il2CppCodeGenWriteBarrier(&___CancelButtonText_14, value);
	}

	inline static int32_t get_offset_of_CanvasGroup_15() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___CanvasGroup_15)); }
	inline CanvasGroup_t3296560743 * get_CanvasGroup_15() const { return ___CanvasGroup_15; }
	inline CanvasGroup_t3296560743 ** get_address_of_CanvasGroup_15() { return &___CanvasGroup_15; }
	inline void set_CanvasGroup_15(CanvasGroup_t3296560743 * value)
	{
		___CanvasGroup_15 = value;
		Il2CppCodeGenWriteBarrier(&___CanvasGroup_15, value);
	}

	inline static int32_t get_offset_of_DotAnimator_16() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___DotAnimator_16)); }
	inline Animator_t69676727 * get_DotAnimator_16() const { return ___DotAnimator_16; }
	inline Animator_t69676727 ** get_address_of_DotAnimator_16() { return &___DotAnimator_16; }
	inline void set_DotAnimator_16(Animator_t69676727 * value)
	{
		___DotAnimator_16 = value;
		Il2CppCodeGenWriteBarrier(&___DotAnimator_16, value);
	}

	inline static int32_t get_offset_of_NumberButtons_17() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___NumberButtons_17)); }
	inline ButtonU5BU5D_t3071100561* get_NumberButtons_17() const { return ___NumberButtons_17; }
	inline ButtonU5BU5D_t3071100561** get_address_of_NumberButtons_17() { return &___NumberButtons_17; }
	inline void set_NumberButtons_17(ButtonU5BU5D_t3071100561* value)
	{
		___NumberButtons_17 = value;
		Il2CppCodeGenWriteBarrier(&___NumberButtons_17, value);
	}

	inline static int32_t get_offset_of_NumberDots_18() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___NumberDots_18)); }
	inline ToggleU5BU5D_t1971649997* get_NumberDots_18() const { return ___NumberDots_18; }
	inline ToggleU5BU5D_t1971649997** get_address_of_NumberDots_18() { return &___NumberDots_18; }
	inline void set_NumberDots_18(ToggleU5BU5D_t1971649997* value)
	{
		___NumberDots_18 = value;
		Il2CppCodeGenWriteBarrier(&___NumberDots_18, value);
	}

	inline static int32_t get_offset_of_PromptText_19() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___PromptText_19)); }
	inline Text_t356221433 * get_PromptText_19() const { return ___PromptText_19; }
	inline Text_t356221433 ** get_address_of_PromptText_19() { return &___PromptText_19; }
	inline void set_PromptText_19(Text_t356221433 * value)
	{
		___PromptText_19 = value;
		Il2CppCodeGenWriteBarrier(&___PromptText_19, value);
	}

	inline static int32_t get_offset_of_Complete_20() { return static_cast<int32_t>(offsetof(PinEntryControl_t1988638876, ___Complete_20)); }
	inline PinEntryControlCallback_t496076559 * get_Complete_20() const { return ___Complete_20; }
	inline PinEntryControlCallback_t496076559 ** get_address_of_Complete_20() { return &___Complete_20; }
	inline void set_Complete_20(PinEntryControlCallback_t496076559 * value)
	{
		___Complete_20 = value;
		Il2CppCodeGenWriteBarrier(&___Complete_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
