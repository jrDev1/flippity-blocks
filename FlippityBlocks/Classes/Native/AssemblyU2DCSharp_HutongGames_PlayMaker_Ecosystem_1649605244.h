﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_PropertyAttribute2606999759.h"
#include "mscorlib_System_Reflection_BindingFlags1082350898.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute
struct  ButtonAttribute_t1649605244  : public PropertyAttribute_t2606999759
{
public:
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::methodName
	String_t* ___methodName_0;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::buttonName
	String_t* ___buttonName_1;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::useValue
	bool ___useValue_2;
	// System.Reflection.BindingFlags HutongGames.PlayMaker.Ecosystem.Utils.ButtonAttribute::flags
	int32_t ___flags_3;

public:
	inline static int32_t get_offset_of_methodName_0() { return static_cast<int32_t>(offsetof(ButtonAttribute_t1649605244, ___methodName_0)); }
	inline String_t* get_methodName_0() const { return ___methodName_0; }
	inline String_t** get_address_of_methodName_0() { return &___methodName_0; }
	inline void set_methodName_0(String_t* value)
	{
		___methodName_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodName_0, value);
	}

	inline static int32_t get_offset_of_buttonName_1() { return static_cast<int32_t>(offsetof(ButtonAttribute_t1649605244, ___buttonName_1)); }
	inline String_t* get_buttonName_1() const { return ___buttonName_1; }
	inline String_t** get_address_of_buttonName_1() { return &___buttonName_1; }
	inline void set_buttonName_1(String_t* value)
	{
		___buttonName_1 = value;
		Il2CppCodeGenWriteBarrier(&___buttonName_1, value);
	}

	inline static int32_t get_offset_of_useValue_2() { return static_cast<int32_t>(offsetof(ButtonAttribute_t1649605244, ___useValue_2)); }
	inline bool get_useValue_2() const { return ___useValue_2; }
	inline bool* get_address_of_useValue_2() { return &___useValue_2; }
	inline void set_useValue_2(bool value)
	{
		___useValue_2 = value;
	}

	inline static int32_t get_offset_of_flags_3() { return static_cast<int32_t>(offsetof(ButtonAttribute_t1649605244, ___flags_3)); }
	inline int32_t get_flags_3() const { return ___flags_3; }
	inline int32_t* get_address_of_flags_3() { return &___flags_3; }
	inline void set_flags_3(int32_t value)
	{
		___flags_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
