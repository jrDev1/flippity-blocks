﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.Internal.SRDebugStrings
struct SRDebugStrings_t511296532;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.SRDebugStrings
struct  SRDebugStrings_t511296532  : public Il2CppObject
{
public:
	// System.String SRDebugger.Internal.SRDebugStrings::Console_MessageTruncated
	String_t* ___Console_MessageTruncated_1;
	// System.String SRDebugger.Internal.SRDebugStrings::Console_NoStackTrace
	String_t* ___Console_NoStackTrace_2;
	// System.String SRDebugger.Internal.SRDebugStrings::PinEntryPrompt
	String_t* ___PinEntryPrompt_3;
	// System.String SRDebugger.Internal.SRDebugStrings::Profiler_DisableProfilerInfo
	String_t* ___Profiler_DisableProfilerInfo_4;
	// System.String SRDebugger.Internal.SRDebugStrings::Profiler_EnableProfilerInfo
	String_t* ___Profiler_EnableProfilerInfo_5;
	// System.String SRDebugger.Internal.SRDebugStrings::Profiler_NoProInfo
	String_t* ___Profiler_NoProInfo_6;
	// System.String SRDebugger.Internal.SRDebugStrings::Profiler_NotSupported
	String_t* ___Profiler_NotSupported_7;
	// System.String SRDebugger.Internal.SRDebugStrings::ProfilerCameraListenerHelp
	String_t* ___ProfilerCameraListenerHelp_8;

public:
	inline static int32_t get_offset_of_Console_MessageTruncated_1() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___Console_MessageTruncated_1)); }
	inline String_t* get_Console_MessageTruncated_1() const { return ___Console_MessageTruncated_1; }
	inline String_t** get_address_of_Console_MessageTruncated_1() { return &___Console_MessageTruncated_1; }
	inline void set_Console_MessageTruncated_1(String_t* value)
	{
		___Console_MessageTruncated_1 = value;
		Il2CppCodeGenWriteBarrier(&___Console_MessageTruncated_1, value);
	}

	inline static int32_t get_offset_of_Console_NoStackTrace_2() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___Console_NoStackTrace_2)); }
	inline String_t* get_Console_NoStackTrace_2() const { return ___Console_NoStackTrace_2; }
	inline String_t** get_address_of_Console_NoStackTrace_2() { return &___Console_NoStackTrace_2; }
	inline void set_Console_NoStackTrace_2(String_t* value)
	{
		___Console_NoStackTrace_2 = value;
		Il2CppCodeGenWriteBarrier(&___Console_NoStackTrace_2, value);
	}

	inline static int32_t get_offset_of_PinEntryPrompt_3() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___PinEntryPrompt_3)); }
	inline String_t* get_PinEntryPrompt_3() const { return ___PinEntryPrompt_3; }
	inline String_t** get_address_of_PinEntryPrompt_3() { return &___PinEntryPrompt_3; }
	inline void set_PinEntryPrompt_3(String_t* value)
	{
		___PinEntryPrompt_3 = value;
		Il2CppCodeGenWriteBarrier(&___PinEntryPrompt_3, value);
	}

	inline static int32_t get_offset_of_Profiler_DisableProfilerInfo_4() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___Profiler_DisableProfilerInfo_4)); }
	inline String_t* get_Profiler_DisableProfilerInfo_4() const { return ___Profiler_DisableProfilerInfo_4; }
	inline String_t** get_address_of_Profiler_DisableProfilerInfo_4() { return &___Profiler_DisableProfilerInfo_4; }
	inline void set_Profiler_DisableProfilerInfo_4(String_t* value)
	{
		___Profiler_DisableProfilerInfo_4 = value;
		Il2CppCodeGenWriteBarrier(&___Profiler_DisableProfilerInfo_4, value);
	}

	inline static int32_t get_offset_of_Profiler_EnableProfilerInfo_5() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___Profiler_EnableProfilerInfo_5)); }
	inline String_t* get_Profiler_EnableProfilerInfo_5() const { return ___Profiler_EnableProfilerInfo_5; }
	inline String_t** get_address_of_Profiler_EnableProfilerInfo_5() { return &___Profiler_EnableProfilerInfo_5; }
	inline void set_Profiler_EnableProfilerInfo_5(String_t* value)
	{
		___Profiler_EnableProfilerInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___Profiler_EnableProfilerInfo_5, value);
	}

	inline static int32_t get_offset_of_Profiler_NoProInfo_6() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___Profiler_NoProInfo_6)); }
	inline String_t* get_Profiler_NoProInfo_6() const { return ___Profiler_NoProInfo_6; }
	inline String_t** get_address_of_Profiler_NoProInfo_6() { return &___Profiler_NoProInfo_6; }
	inline void set_Profiler_NoProInfo_6(String_t* value)
	{
		___Profiler_NoProInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___Profiler_NoProInfo_6, value);
	}

	inline static int32_t get_offset_of_Profiler_NotSupported_7() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___Profiler_NotSupported_7)); }
	inline String_t* get_Profiler_NotSupported_7() const { return ___Profiler_NotSupported_7; }
	inline String_t** get_address_of_Profiler_NotSupported_7() { return &___Profiler_NotSupported_7; }
	inline void set_Profiler_NotSupported_7(String_t* value)
	{
		___Profiler_NotSupported_7 = value;
		Il2CppCodeGenWriteBarrier(&___Profiler_NotSupported_7, value);
	}

	inline static int32_t get_offset_of_ProfilerCameraListenerHelp_8() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532, ___ProfilerCameraListenerHelp_8)); }
	inline String_t* get_ProfilerCameraListenerHelp_8() const { return ___ProfilerCameraListenerHelp_8; }
	inline String_t** get_address_of_ProfilerCameraListenerHelp_8() { return &___ProfilerCameraListenerHelp_8; }
	inline void set_ProfilerCameraListenerHelp_8(String_t* value)
	{
		___ProfilerCameraListenerHelp_8 = value;
		Il2CppCodeGenWriteBarrier(&___ProfilerCameraListenerHelp_8, value);
	}
};

struct SRDebugStrings_t511296532_StaticFields
{
public:
	// SRDebugger.Internal.SRDebugStrings SRDebugger.Internal.SRDebugStrings::Current
	SRDebugStrings_t511296532 * ___Current_0;

public:
	inline static int32_t get_offset_of_Current_0() { return static_cast<int32_t>(offsetof(SRDebugStrings_t511296532_StaticFields, ___Current_0)); }
	inline SRDebugStrings_t511296532 * get_Current_0() const { return ___Current_0; }
	inline SRDebugStrings_t511296532 ** get_address_of_Current_0() { return &___Current_0; }
	inline void set_Current_0(SRDebugStrings_t511296532 * value)
	{
		___Current_0 = value;
		Il2CppCodeGenWriteBarrier(&___Current_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
