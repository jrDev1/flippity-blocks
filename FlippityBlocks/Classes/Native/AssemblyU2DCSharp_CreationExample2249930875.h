﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreationExample
struct  CreationExample_t2249930875  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform CreationExample::testPrefab
	Transform_t3275118058 * ___testPrefab_2;
	// System.String CreationExample::poolName
	String_t* ___poolName_3;
	// System.Int32 CreationExample::spawnAmount
	int32_t ___spawnAmount_4;
	// System.Single CreationExample::spawnInterval
	float ___spawnInterval_5;
	// PathologicalGames.SpawnPool CreationExample::pool
	SpawnPool_t2419717525 * ___pool_6;

public:
	inline static int32_t get_offset_of_testPrefab_2() { return static_cast<int32_t>(offsetof(CreationExample_t2249930875, ___testPrefab_2)); }
	inline Transform_t3275118058 * get_testPrefab_2() const { return ___testPrefab_2; }
	inline Transform_t3275118058 ** get_address_of_testPrefab_2() { return &___testPrefab_2; }
	inline void set_testPrefab_2(Transform_t3275118058 * value)
	{
		___testPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___testPrefab_2, value);
	}

	inline static int32_t get_offset_of_poolName_3() { return static_cast<int32_t>(offsetof(CreationExample_t2249930875, ___poolName_3)); }
	inline String_t* get_poolName_3() const { return ___poolName_3; }
	inline String_t** get_address_of_poolName_3() { return &___poolName_3; }
	inline void set_poolName_3(String_t* value)
	{
		___poolName_3 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_3, value);
	}

	inline static int32_t get_offset_of_spawnAmount_4() { return static_cast<int32_t>(offsetof(CreationExample_t2249930875, ___spawnAmount_4)); }
	inline int32_t get_spawnAmount_4() const { return ___spawnAmount_4; }
	inline int32_t* get_address_of_spawnAmount_4() { return &___spawnAmount_4; }
	inline void set_spawnAmount_4(int32_t value)
	{
		___spawnAmount_4 = value;
	}

	inline static int32_t get_offset_of_spawnInterval_5() { return static_cast<int32_t>(offsetof(CreationExample_t2249930875, ___spawnInterval_5)); }
	inline float get_spawnInterval_5() const { return ___spawnInterval_5; }
	inline float* get_address_of_spawnInterval_5() { return &___spawnInterval_5; }
	inline void set_spawnInterval_5(float value)
	{
		___spawnInterval_5 = value;
	}

	inline static int32_t get_offset_of_pool_6() { return static_cast<int32_t>(offsetof(CreationExample_t2249930875, ___pool_6)); }
	inline SpawnPool_t2419717525 * get_pool_6() const { return ___pool_6; }
	inline SpawnPool_t2419717525 ** get_address_of_pool_6() { return &___pool_6; }
	inline void set_pool_6(SpawnPool_t2419717525 * value)
	{
		___pool_6 = value;
		Il2CppCodeGenWriteBarrier(&___pool_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
