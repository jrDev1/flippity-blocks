﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions
struct  ArrayListSetVertexPositions_t432243395  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::mesh
	FsmGameObject_t3097142863 * ___mesh_14;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::everyFrame
	bool ___everyFrame_15;
	// UnityEngine.Mesh HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::_mesh
	Mesh_t1356156583 * ____mesh_16;
	// UnityEngine.Vector3[] HutongGames.PlayMaker.Actions.ArrayListSetVertexPositions::_vertices
	Vector3U5BU5D_t1172311765* ____vertices_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListSetVertexPositions_t432243395, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListSetVertexPositions_t432243395, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_mesh_14() { return static_cast<int32_t>(offsetof(ArrayListSetVertexPositions_t432243395, ___mesh_14)); }
	inline FsmGameObject_t3097142863 * get_mesh_14() const { return ___mesh_14; }
	inline FsmGameObject_t3097142863 ** get_address_of_mesh_14() { return &___mesh_14; }
	inline void set_mesh_14(FsmGameObject_t3097142863 * value)
	{
		___mesh_14 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(ArrayListSetVertexPositions_t432243395, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}

	inline static int32_t get_offset_of__mesh_16() { return static_cast<int32_t>(offsetof(ArrayListSetVertexPositions_t432243395, ____mesh_16)); }
	inline Mesh_t1356156583 * get__mesh_16() const { return ____mesh_16; }
	inline Mesh_t1356156583 ** get_address_of__mesh_16() { return &____mesh_16; }
	inline void set__mesh_16(Mesh_t1356156583 * value)
	{
		____mesh_16 = value;
		Il2CppCodeGenWriteBarrier(&____mesh_16, value);
	}

	inline static int32_t get_offset_of__vertices_17() { return static_cast<int32_t>(offsetof(ArrayListSetVertexPositions_t432243395, ____vertices_17)); }
	inline Vector3U5BU5D_t1172311765* get__vertices_17() const { return ____vertices_17; }
	inline Vector3U5BU5D_t1172311765** get_address_of__vertices_17() { return &____vertices_17; }
	inline void set__vertices_17(Vector3U5BU5D_t1172311765* value)
	{
		____vertices_17 = value;
		Il2CppCodeGenWriteBarrier(&____vertices_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
