﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.FloatRoundToNearest
struct  FloatRoundToNearest_t539851699  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatRoundToNearest::floatVariable
	FsmFloat_t937133978 * ___floatVariable_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatRoundToNearest::nearest
	FsmFloat_t937133978 * ___nearest_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.FloatRoundToNearest::resultAsInt
	FsmInt_t1273009179 * ___resultAsInt_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.FloatRoundToNearest::resultAsFloat
	FsmFloat_t937133978 * ___resultAsFloat_14;
	// System.Boolean HutongGames.PlayMaker.Actions.FloatRoundToNearest::everyFrame
	bool ___everyFrame_15;

public:
	inline static int32_t get_offset_of_floatVariable_11() { return static_cast<int32_t>(offsetof(FloatRoundToNearest_t539851699, ___floatVariable_11)); }
	inline FsmFloat_t937133978 * get_floatVariable_11() const { return ___floatVariable_11; }
	inline FsmFloat_t937133978 ** get_address_of_floatVariable_11() { return &___floatVariable_11; }
	inline void set_floatVariable_11(FsmFloat_t937133978 * value)
	{
		___floatVariable_11 = value;
		Il2CppCodeGenWriteBarrier(&___floatVariable_11, value);
	}

	inline static int32_t get_offset_of_nearest_12() { return static_cast<int32_t>(offsetof(FloatRoundToNearest_t539851699, ___nearest_12)); }
	inline FsmFloat_t937133978 * get_nearest_12() const { return ___nearest_12; }
	inline FsmFloat_t937133978 ** get_address_of_nearest_12() { return &___nearest_12; }
	inline void set_nearest_12(FsmFloat_t937133978 * value)
	{
		___nearest_12 = value;
		Il2CppCodeGenWriteBarrier(&___nearest_12, value);
	}

	inline static int32_t get_offset_of_resultAsInt_13() { return static_cast<int32_t>(offsetof(FloatRoundToNearest_t539851699, ___resultAsInt_13)); }
	inline FsmInt_t1273009179 * get_resultAsInt_13() const { return ___resultAsInt_13; }
	inline FsmInt_t1273009179 ** get_address_of_resultAsInt_13() { return &___resultAsInt_13; }
	inline void set_resultAsInt_13(FsmInt_t1273009179 * value)
	{
		___resultAsInt_13 = value;
		Il2CppCodeGenWriteBarrier(&___resultAsInt_13, value);
	}

	inline static int32_t get_offset_of_resultAsFloat_14() { return static_cast<int32_t>(offsetof(FloatRoundToNearest_t539851699, ___resultAsFloat_14)); }
	inline FsmFloat_t937133978 * get_resultAsFloat_14() const { return ___resultAsFloat_14; }
	inline FsmFloat_t937133978 ** get_address_of_resultAsFloat_14() { return &___resultAsFloat_14; }
	inline void set_resultAsFloat_14(FsmFloat_t937133978 * value)
	{
		___resultAsFloat_14 = value;
		Il2CppCodeGenWriteBarrier(&___resultAsFloat_14, value);
	}

	inline static int32_t get_offset_of_everyFrame_15() { return static_cast<int32_t>(offsetof(FloatRoundToNearest_t539851699, ___everyFrame_15)); }
	inline bool get_everyFrame_15() const { return ___everyFrame_15; }
	inline bool* get_address_of_everyFrame_15() { return &___everyFrame_15; }
	inline void set_everyFrame_15(bool value)
	{
		___everyFrame_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
