﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct PlayMakerEvent_t1571596806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiDragEventsProxy
struct  PlayMakerUGuiDragEventsProxy_t2387283859  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerUGuiDragEventsProxy::debug
	bool ___debug_2;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget PlayMakerUGuiDragEventsProxy::eventTarget
	PlayMakerEventTarget_t2104288969 * ___eventTarget_3;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiDragEventsProxy::onBeginDragEvent
	PlayMakerEvent_t1571596806 * ___onBeginDragEvent_4;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiDragEventsProxy::onDragEvent
	PlayMakerEvent_t1571596806 * ___onDragEvent_5;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiDragEventsProxy::onEndDragEvent
	PlayMakerEvent_t1571596806 * ___onEndDragEvent_6;

public:
	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsProxy_t2387283859, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_eventTarget_3() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsProxy_t2387283859, ___eventTarget_3)); }
	inline PlayMakerEventTarget_t2104288969 * get_eventTarget_3() const { return ___eventTarget_3; }
	inline PlayMakerEventTarget_t2104288969 ** get_address_of_eventTarget_3() { return &___eventTarget_3; }
	inline void set_eventTarget_3(PlayMakerEventTarget_t2104288969 * value)
	{
		___eventTarget_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_3, value);
	}

	inline static int32_t get_offset_of_onBeginDragEvent_4() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsProxy_t2387283859, ___onBeginDragEvent_4)); }
	inline PlayMakerEvent_t1571596806 * get_onBeginDragEvent_4() const { return ___onBeginDragEvent_4; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onBeginDragEvent_4() { return &___onBeginDragEvent_4; }
	inline void set_onBeginDragEvent_4(PlayMakerEvent_t1571596806 * value)
	{
		___onBeginDragEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___onBeginDragEvent_4, value);
	}

	inline static int32_t get_offset_of_onDragEvent_5() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsProxy_t2387283859, ___onDragEvent_5)); }
	inline PlayMakerEvent_t1571596806 * get_onDragEvent_5() const { return ___onDragEvent_5; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onDragEvent_5() { return &___onDragEvent_5; }
	inline void set_onDragEvent_5(PlayMakerEvent_t1571596806 * value)
	{
		___onDragEvent_5 = value;
		Il2CppCodeGenWriteBarrier(&___onDragEvent_5, value);
	}

	inline static int32_t get_offset_of_onEndDragEvent_6() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDragEventsProxy_t2387283859, ___onEndDragEvent_6)); }
	inline PlayMakerEvent_t1571596806 * get_onEndDragEvent_6() const { return ___onEndDragEvent_6; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onEndDragEvent_6() { return &___onEndDragEvent_6; }
	inline void set_onEndDragEvent_6(PlayMakerEvent_t1571596806 * value)
	{
		___onEndDragEvent_6 = value;
		Il2CppCodeGenWriteBarrier(&___onEndDragEvent_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
