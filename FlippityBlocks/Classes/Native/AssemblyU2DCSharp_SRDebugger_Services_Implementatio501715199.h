﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_Service_SRServiceBase_1_gen3950185291.h"

// SRDebugger.Services.BugReportCompleteCallback
struct BugReportCompleteCallback_t3977973366;
// System.String
struct String_t;
// SRDebugger.Services.BugReportProgressCallback
struct BugReportProgressCallback_t3296505394;
// SRDebugger.Internal.BugReportApi
struct BugReportApi_t1345885876;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.BugReportApiService
struct  BugReportApiService_t501715199  : public SRServiceBase_1_t3950185291
{
public:
	// SRDebugger.Services.BugReportCompleteCallback SRDebugger.Services.Implementation.BugReportApiService::_completeCallback
	BugReportCompleteCallback_t3977973366 * ____completeCallback_10;
	// System.String SRDebugger.Services.Implementation.BugReportApiService::_errorMessage
	String_t* ____errorMessage_11;
	// System.Boolean SRDebugger.Services.Implementation.BugReportApiService::_isBusy
	bool ____isBusy_12;
	// System.Single SRDebugger.Services.Implementation.BugReportApiService::_previousProgress
	float ____previousProgress_13;
	// SRDebugger.Services.BugReportProgressCallback SRDebugger.Services.Implementation.BugReportApiService::_progressCallback
	BugReportProgressCallback_t3296505394 * ____progressCallback_14;
	// SRDebugger.Internal.BugReportApi SRDebugger.Services.Implementation.BugReportApiService::_reportApi
	BugReportApi_t1345885876 * ____reportApi_15;

public:
	inline static int32_t get_offset_of__completeCallback_10() { return static_cast<int32_t>(offsetof(BugReportApiService_t501715199, ____completeCallback_10)); }
	inline BugReportCompleteCallback_t3977973366 * get__completeCallback_10() const { return ____completeCallback_10; }
	inline BugReportCompleteCallback_t3977973366 ** get_address_of__completeCallback_10() { return &____completeCallback_10; }
	inline void set__completeCallback_10(BugReportCompleteCallback_t3977973366 * value)
	{
		____completeCallback_10 = value;
		Il2CppCodeGenWriteBarrier(&____completeCallback_10, value);
	}

	inline static int32_t get_offset_of__errorMessage_11() { return static_cast<int32_t>(offsetof(BugReportApiService_t501715199, ____errorMessage_11)); }
	inline String_t* get__errorMessage_11() const { return ____errorMessage_11; }
	inline String_t** get_address_of__errorMessage_11() { return &____errorMessage_11; }
	inline void set__errorMessage_11(String_t* value)
	{
		____errorMessage_11 = value;
		Il2CppCodeGenWriteBarrier(&____errorMessage_11, value);
	}

	inline static int32_t get_offset_of__isBusy_12() { return static_cast<int32_t>(offsetof(BugReportApiService_t501715199, ____isBusy_12)); }
	inline bool get__isBusy_12() const { return ____isBusy_12; }
	inline bool* get_address_of__isBusy_12() { return &____isBusy_12; }
	inline void set__isBusy_12(bool value)
	{
		____isBusy_12 = value;
	}

	inline static int32_t get_offset_of__previousProgress_13() { return static_cast<int32_t>(offsetof(BugReportApiService_t501715199, ____previousProgress_13)); }
	inline float get__previousProgress_13() const { return ____previousProgress_13; }
	inline float* get_address_of__previousProgress_13() { return &____previousProgress_13; }
	inline void set__previousProgress_13(float value)
	{
		____previousProgress_13 = value;
	}

	inline static int32_t get_offset_of__progressCallback_14() { return static_cast<int32_t>(offsetof(BugReportApiService_t501715199, ____progressCallback_14)); }
	inline BugReportProgressCallback_t3296505394 * get__progressCallback_14() const { return ____progressCallback_14; }
	inline BugReportProgressCallback_t3296505394 ** get_address_of__progressCallback_14() { return &____progressCallback_14; }
	inline void set__progressCallback_14(BugReportProgressCallback_t3296505394 * value)
	{
		____progressCallback_14 = value;
		Il2CppCodeGenWriteBarrier(&____progressCallback_14, value);
	}

	inline static int32_t get_offset_of__reportApi_15() { return static_cast<int32_t>(offsetof(BugReportApiService_t501715199, ____reportApi_15)); }
	inline BugReportApi_t1345885876 * get__reportApi_15() const { return ____reportApi_15; }
	inline BugReportApi_t1345885876 ** get_address_of__reportApi_15() { return &____reportApi_15; }
	inline void set__reportApi_15(BugReportApi_t1345885876 * value)
	{
		____reportApi_15 = value;
		Il2CppCodeGenWriteBarrier(&____reportApi_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
