﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// System.String
struct String_t;
// SRDebugger.Services.ConsoleEntry
struct ConsoleEntry_t3008967599;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// SRF.UI.StyleComponent
struct StyleComponent_t3036492378;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.ConsoleEntryView
struct  ConsoleEntryView_t2517313264  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Int32 SRDebugger.UI.Controls.ConsoleEntryView::_count
	int32_t ____count_12;
	// System.Boolean SRDebugger.UI.Controls.ConsoleEntryView::_hasCount
	bool ____hasCount_13;
	// SRDebugger.Services.ConsoleEntry SRDebugger.UI.Controls.ConsoleEntryView::_prevData
	ConsoleEntry_t3008967599 * ____prevData_14;
	// UnityEngine.RectTransform SRDebugger.UI.Controls.ConsoleEntryView::_rectTransform
	RectTransform_t3349966182 * ____rectTransform_15;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ConsoleEntryView::Count
	Text_t356221433 * ___Count_16;
	// UnityEngine.CanvasGroup SRDebugger.UI.Controls.ConsoleEntryView::CountContainer
	CanvasGroup_t3296560743 * ___CountContainer_17;
	// SRF.UI.StyleComponent SRDebugger.UI.Controls.ConsoleEntryView::ImageStyle
	StyleComponent_t3036492378 * ___ImageStyle_18;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ConsoleEntryView::Message
	Text_t356221433 * ___Message_19;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.ConsoleEntryView::StackTrace
	Text_t356221433 * ___StackTrace_20;

public:
	inline static int32_t get_offset_of__count_12() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ____count_12)); }
	inline int32_t get__count_12() const { return ____count_12; }
	inline int32_t* get_address_of__count_12() { return &____count_12; }
	inline void set__count_12(int32_t value)
	{
		____count_12 = value;
	}

	inline static int32_t get_offset_of__hasCount_13() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ____hasCount_13)); }
	inline bool get__hasCount_13() const { return ____hasCount_13; }
	inline bool* get_address_of__hasCount_13() { return &____hasCount_13; }
	inline void set__hasCount_13(bool value)
	{
		____hasCount_13 = value;
	}

	inline static int32_t get_offset_of__prevData_14() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ____prevData_14)); }
	inline ConsoleEntry_t3008967599 * get__prevData_14() const { return ____prevData_14; }
	inline ConsoleEntry_t3008967599 ** get_address_of__prevData_14() { return &____prevData_14; }
	inline void set__prevData_14(ConsoleEntry_t3008967599 * value)
	{
		____prevData_14 = value;
		Il2CppCodeGenWriteBarrier(&____prevData_14, value);
	}

	inline static int32_t get_offset_of__rectTransform_15() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ____rectTransform_15)); }
	inline RectTransform_t3349966182 * get__rectTransform_15() const { return ____rectTransform_15; }
	inline RectTransform_t3349966182 ** get_address_of__rectTransform_15() { return &____rectTransform_15; }
	inline void set__rectTransform_15(RectTransform_t3349966182 * value)
	{
		____rectTransform_15 = value;
		Il2CppCodeGenWriteBarrier(&____rectTransform_15, value);
	}

	inline static int32_t get_offset_of_Count_16() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ___Count_16)); }
	inline Text_t356221433 * get_Count_16() const { return ___Count_16; }
	inline Text_t356221433 ** get_address_of_Count_16() { return &___Count_16; }
	inline void set_Count_16(Text_t356221433 * value)
	{
		___Count_16 = value;
		Il2CppCodeGenWriteBarrier(&___Count_16, value);
	}

	inline static int32_t get_offset_of_CountContainer_17() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ___CountContainer_17)); }
	inline CanvasGroup_t3296560743 * get_CountContainer_17() const { return ___CountContainer_17; }
	inline CanvasGroup_t3296560743 ** get_address_of_CountContainer_17() { return &___CountContainer_17; }
	inline void set_CountContainer_17(CanvasGroup_t3296560743 * value)
	{
		___CountContainer_17 = value;
		Il2CppCodeGenWriteBarrier(&___CountContainer_17, value);
	}

	inline static int32_t get_offset_of_ImageStyle_18() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ___ImageStyle_18)); }
	inline StyleComponent_t3036492378 * get_ImageStyle_18() const { return ___ImageStyle_18; }
	inline StyleComponent_t3036492378 ** get_address_of_ImageStyle_18() { return &___ImageStyle_18; }
	inline void set_ImageStyle_18(StyleComponent_t3036492378 * value)
	{
		___ImageStyle_18 = value;
		Il2CppCodeGenWriteBarrier(&___ImageStyle_18, value);
	}

	inline static int32_t get_offset_of_Message_19() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ___Message_19)); }
	inline Text_t356221433 * get_Message_19() const { return ___Message_19; }
	inline Text_t356221433 ** get_address_of_Message_19() { return &___Message_19; }
	inline void set_Message_19(Text_t356221433 * value)
	{
		___Message_19 = value;
		Il2CppCodeGenWriteBarrier(&___Message_19, value);
	}

	inline static int32_t get_offset_of_StackTrace_20() { return static_cast<int32_t>(offsetof(ConsoleEntryView_t2517313264, ___StackTrace_20)); }
	inline Text_t356221433 * get_StackTrace_20() const { return ___StackTrace_20; }
	inline Text_t356221433 ** get_address_of_StackTrace_20() { return &___StackTrace_20; }
	inline void set_StackTrace_20(Text_t356221433 * value)
	{
		___StackTrace_20 = value;
		Il2CppCodeGenWriteBarrier(&___StackTrace_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
