﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.String
struct String_t;
// SRDebugger.Services.Implementation.BugReportPopoverService
struct BugReportPopoverService_t956868698;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0
struct  U3COpenCoU3Ec__Iterator0_t2244406016  : public Il2CppObject
{
public:
	// System.Boolean SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0::takeScreenshot
	bool ___takeScreenshot_0;
	// System.String SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0::descriptionText
	String_t* ___descriptionText_1;
	// SRDebugger.Services.Implementation.BugReportPopoverService SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0::$this
	BugReportPopoverService_t956868698 * ___U24this_2;
	// System.Object SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 SRDebugger.Services.Implementation.BugReportPopoverService/<OpenCo>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_takeScreenshot_0() { return static_cast<int32_t>(offsetof(U3COpenCoU3Ec__Iterator0_t2244406016, ___takeScreenshot_0)); }
	inline bool get_takeScreenshot_0() const { return ___takeScreenshot_0; }
	inline bool* get_address_of_takeScreenshot_0() { return &___takeScreenshot_0; }
	inline void set_takeScreenshot_0(bool value)
	{
		___takeScreenshot_0 = value;
	}

	inline static int32_t get_offset_of_descriptionText_1() { return static_cast<int32_t>(offsetof(U3COpenCoU3Ec__Iterator0_t2244406016, ___descriptionText_1)); }
	inline String_t* get_descriptionText_1() const { return ___descriptionText_1; }
	inline String_t** get_address_of_descriptionText_1() { return &___descriptionText_1; }
	inline void set_descriptionText_1(String_t* value)
	{
		___descriptionText_1 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionText_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3COpenCoU3Ec__Iterator0_t2244406016, ___U24this_2)); }
	inline BugReportPopoverService_t956868698 * get_U24this_2() const { return ___U24this_2; }
	inline BugReportPopoverService_t956868698 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BugReportPopoverService_t956868698 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3COpenCoU3Ec__Iterator0_t2244406016, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3COpenCoU3Ec__Iterator0_t2244406016, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3COpenCoU3Ec__Iterator0_t2244406016, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
