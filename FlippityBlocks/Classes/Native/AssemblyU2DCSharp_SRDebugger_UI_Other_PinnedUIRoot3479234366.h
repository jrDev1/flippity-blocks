﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// SRDebugger.UI.Other.DockConsoleController
struct DockConsoleController_t2913316086;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// SRF.UI.Layout.FlowLayoutGroup
struct FlowLayoutGroup_t719127507;
// SRDebugger.UI.Other.HandleManager
struct HandleManager_t305543651;
// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t2468316403;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.PinnedUIRoot
struct  PinnedUIRoot_t3479234366  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.Canvas SRDebugger.UI.Other.PinnedUIRoot::Canvas
	Canvas_t209405766 * ___Canvas_9;
	// UnityEngine.RectTransform SRDebugger.UI.Other.PinnedUIRoot::Container
	RectTransform_t3349966182 * ___Container_10;
	// SRDebugger.UI.Other.DockConsoleController SRDebugger.UI.Other.PinnedUIRoot::DockConsoleController
	DockConsoleController_t2913316086 * ___DockConsoleController_11;
	// UnityEngine.GameObject SRDebugger.UI.Other.PinnedUIRoot::Options
	GameObject_t1756533147 * ___Options_12;
	// SRF.UI.Layout.FlowLayoutGroup SRDebugger.UI.Other.PinnedUIRoot::OptionsLayoutGroup
	FlowLayoutGroup_t719127507 * ___OptionsLayoutGroup_13;
	// UnityEngine.GameObject SRDebugger.UI.Other.PinnedUIRoot::Profiler
	GameObject_t1756533147 * ___Profiler_14;
	// SRDebugger.UI.Other.HandleManager SRDebugger.UI.Other.PinnedUIRoot::ProfilerHandleManager
	HandleManager_t305543651 * ___ProfilerHandleManager_15;
	// UnityEngine.UI.VerticalLayoutGroup SRDebugger.UI.Other.PinnedUIRoot::ProfilerVerticalLayoutGroup
	VerticalLayoutGroup_t2468316403 * ___ProfilerVerticalLayoutGroup_16;

public:
	inline static int32_t get_offset_of_Canvas_9() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___Canvas_9)); }
	inline Canvas_t209405766 * get_Canvas_9() const { return ___Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_Canvas_9() { return &___Canvas_9; }
	inline void set_Canvas_9(Canvas_t209405766 * value)
	{
		___Canvas_9 = value;
		Il2CppCodeGenWriteBarrier(&___Canvas_9, value);
	}

	inline static int32_t get_offset_of_Container_10() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___Container_10)); }
	inline RectTransform_t3349966182 * get_Container_10() const { return ___Container_10; }
	inline RectTransform_t3349966182 ** get_address_of_Container_10() { return &___Container_10; }
	inline void set_Container_10(RectTransform_t3349966182 * value)
	{
		___Container_10 = value;
		Il2CppCodeGenWriteBarrier(&___Container_10, value);
	}

	inline static int32_t get_offset_of_DockConsoleController_11() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___DockConsoleController_11)); }
	inline DockConsoleController_t2913316086 * get_DockConsoleController_11() const { return ___DockConsoleController_11; }
	inline DockConsoleController_t2913316086 ** get_address_of_DockConsoleController_11() { return &___DockConsoleController_11; }
	inline void set_DockConsoleController_11(DockConsoleController_t2913316086 * value)
	{
		___DockConsoleController_11 = value;
		Il2CppCodeGenWriteBarrier(&___DockConsoleController_11, value);
	}

	inline static int32_t get_offset_of_Options_12() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___Options_12)); }
	inline GameObject_t1756533147 * get_Options_12() const { return ___Options_12; }
	inline GameObject_t1756533147 ** get_address_of_Options_12() { return &___Options_12; }
	inline void set_Options_12(GameObject_t1756533147 * value)
	{
		___Options_12 = value;
		Il2CppCodeGenWriteBarrier(&___Options_12, value);
	}

	inline static int32_t get_offset_of_OptionsLayoutGroup_13() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___OptionsLayoutGroup_13)); }
	inline FlowLayoutGroup_t719127507 * get_OptionsLayoutGroup_13() const { return ___OptionsLayoutGroup_13; }
	inline FlowLayoutGroup_t719127507 ** get_address_of_OptionsLayoutGroup_13() { return &___OptionsLayoutGroup_13; }
	inline void set_OptionsLayoutGroup_13(FlowLayoutGroup_t719127507 * value)
	{
		___OptionsLayoutGroup_13 = value;
		Il2CppCodeGenWriteBarrier(&___OptionsLayoutGroup_13, value);
	}

	inline static int32_t get_offset_of_Profiler_14() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___Profiler_14)); }
	inline GameObject_t1756533147 * get_Profiler_14() const { return ___Profiler_14; }
	inline GameObject_t1756533147 ** get_address_of_Profiler_14() { return &___Profiler_14; }
	inline void set_Profiler_14(GameObject_t1756533147 * value)
	{
		___Profiler_14 = value;
		Il2CppCodeGenWriteBarrier(&___Profiler_14, value);
	}

	inline static int32_t get_offset_of_ProfilerHandleManager_15() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___ProfilerHandleManager_15)); }
	inline HandleManager_t305543651 * get_ProfilerHandleManager_15() const { return ___ProfilerHandleManager_15; }
	inline HandleManager_t305543651 ** get_address_of_ProfilerHandleManager_15() { return &___ProfilerHandleManager_15; }
	inline void set_ProfilerHandleManager_15(HandleManager_t305543651 * value)
	{
		___ProfilerHandleManager_15 = value;
		Il2CppCodeGenWriteBarrier(&___ProfilerHandleManager_15, value);
	}

	inline static int32_t get_offset_of_ProfilerVerticalLayoutGroup_16() { return static_cast<int32_t>(offsetof(PinnedUIRoot_t3479234366, ___ProfilerVerticalLayoutGroup_16)); }
	inline VerticalLayoutGroup_t2468316403 * get_ProfilerVerticalLayoutGroup_16() const { return ___ProfilerVerticalLayoutGroup_16; }
	inline VerticalLayoutGroup_t2468316403 ** get_address_of_ProfilerVerticalLayoutGroup_16() { return &___ProfilerVerticalLayoutGroup_16; }
	inline void set_ProfilerVerticalLayoutGroup_16(VerticalLayoutGroup_t2468316403 * value)
	{
		___ProfilerVerticalLayoutGroup_16 = value;
		Il2CppCodeGenWriteBarrier(&___ProfilerVerticalLayoutGroup_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
