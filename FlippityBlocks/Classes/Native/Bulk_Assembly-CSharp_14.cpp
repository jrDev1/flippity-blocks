﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SROptions_IncrementAttribute339343029.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Attribute542643598.h"
#include "AssemblyU2DCSharp_SROptions_NumberRangeAttribute2675108520.h"
#include "AssemblyU2DCSharp_SROptions_SortAttribute3684079898.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_SROptionsPropertyChanged1005294736.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_Swipe3475047630.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch1813931207.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch4041355147.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_567777398.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_Gesture367995397.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2889416990.h"
#include "AssemblyU2DCSharp_TapMe2856432827.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2041759604.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_BaseFinge1402521766.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "AssemblyU2DCSharp_ThirdPersonCamera2751132817.h"
#include "AssemblyU2DCSharp_TooglePickMethodUI2222958494.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3272313708.h"
#include "AssemblyU2DCSharp_TouchMe1950908167.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3045666224.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_T16499120.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3694099103.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "AssemblyU2DCSharp_TouchPadUIEvent997246418.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_TouchPadUIEvent_U3CClearTextU3Ec3345759406.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_TrackMouseMovement1471877027.h"
#include "AssemblyU2DCSharp_TwistMe1887030447.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3096813332.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_117269532.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch1042340097.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2820220009.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_TwoDoubleTapMe376817258.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_313188403.h"
#include "AssemblyU2DCSharp_TwoDragMe2032870650.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch1996108375.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_599932411.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch1069006048.h"
#include "AssemblyU2DCSharp_TwoLongTapMe2356062841.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_L59253952.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2742766376.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3611206799.h"
#include "AssemblyU2DCSharp_TwoSwipe373754724.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3085603867.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2386328279.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2863096110.h"
#include "AssemblyU2DCSharp_TwoTapMe3705696635.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3725456324.h"
#include "AssemblyU2DCSharp_TwoTouchMe4294184479.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3403539080.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_384986787.h"
#include "AssemblyU2DCSharp_UICompatibility2032272458.h"
#include "AssemblyU2DCSharp_UIDrag854096796.h"
#include "AssemblyU2DCSharp_UIPinch1472354382.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch3939026435.h"
#include "AssemblyU2DCSharp_UITwist430776843.h"
#include "AssemblyU2DCSharp_UIWindow2837557726.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "AssemblyU2DCSharp_UnfinityGames_U2DEX_u2dexGrid3875635992.h"
#include "AssemblyU2DCSharp_UnfinityGames_U2DEX_u2dexGrid_Gr3851649610.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "AssemblyU2DCSharp_xARMProxy3550954754.h"

// SROptions/IncrementAttribute
struct IncrementAttribute_t339343029;
// System.Attribute
struct Attribute_t542643598;
// SROptions/NumberRangeAttribute
struct NumberRangeAttribute_t2675108520;
// SROptions/SortAttribute
struct SortAttribute_t3684079898;
// SROptionsPropertyChanged
struct SROptionsPropertyChanged_t1005294736;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Swipe
struct Swipe_t3475047630;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler
struct SwipeStartHandler_t1813931207;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler
struct SwipeHandler_t4041355147;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler
struct SwipeEndHandler_t567777398;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// TapMe
struct TapMe_t2856432827;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler
struct SimpleTapHandler_t2041759604;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material
struct Material_t193706927;
// ThirdPersonCamera
struct ThirdPersonCamera_t2751132817;
// TooglePickMethodUI
struct TooglePickMethodUI_t2222958494;
// TouchMe
struct TouchMe_t1950908167;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler
struct TouchStartHandler_t3045666224;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler
struct TouchDownHandler_t16499120;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler
struct TouchUpHandler_t3694099103;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// TouchPadUIEvent
struct TouchPadUIEvent_t997246418;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// TouchPadUIEvent/<ClearText>c__Iterator0
struct U3CClearTextU3Ec__Iterator0_t3345759406;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// TrackMouseMovement
struct TrackMouseMovement_t1471877027;
// TwistMe
struct TwistMe_t1887030447;
// HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler
struct TouchStart2FingersHandler_t3096813332;
// HedgehogTeam.EasyTouch.EasyTouch/TwistHandler
struct TwistHandler_t117269532;
// HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler
struct TwistEndHandler_t1042340097;
// HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler
struct Cancel2FingersHandler_t2820220009;
// TwoDoubleTapMe
struct TwoDoubleTapMe_t376817258;
// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler
struct DoubleTap2FingersHandler_t313188403;
// TwoDragMe
struct TwoDragMe_t2032870650;
// HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler
struct DragStart2FingersHandler_t1996108375;
// HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler
struct Drag2FingersHandler_t599932411;
// HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler
struct DragEnd2FingersHandler_t1069006048;
// TwoLongTapMe
struct TwoLongTapMe_t2356062841;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler
struct LongTapStart2FingersHandler_t59253952;
// HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler
struct LongTap2FingersHandler_t2742766376;
// HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler
struct LongTapEnd2FingersHandler_t3611206799;
// TwoSwipe
struct TwoSwipe_t373754724;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler
struct SwipeStart2FingersHandler_t3085603867;
// HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler
struct Swipe2FingersHandler_t2386328279;
// HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler
struct SwipeEnd2FingersHandler_t2863096110;
// TwoTapMe
struct TwoTapMe_t3705696635;
// HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler
struct SimpleTap2FingersHandler_t3725456324;
// TwoTouchMe
struct TwoTouchMe_t4294184479;
// HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler
struct TouchDown2FingersHandler_t3403539080;
// HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler
struct TouchUp2FingersHandler_t384986787;
// UICompatibility
struct UICompatibility_t2032272458;
// UIDrag
struct UIDrag_t854096796;
// UIPinch
struct UIPinch_t1472354382;
// HedgehogTeam.EasyTouch.EasyTouch/PinchHandler
struct PinchHandler_t3939026435;
// UITwist
struct UITwist_t430776843;
// UIWindow
struct UIWindow_t2837557726;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnfinityGames.U2DEX.u2dexGrid
struct u2dexGrid_t3875635992;
// UnityEngine.Camera
struct Camera_t189460977;
// xARMProxy
struct xARMProxy_t3550954754;
extern Il2CppClass* SwipeStartHandler_t1813931207_il2cpp_TypeInfo_var;
extern Il2CppClass* SwipeHandler_t4041355147_il2cpp_TypeInfo_var;
extern Il2CppClass* SwipeEndHandler_t567777398_il2cpp_TypeInfo_var;
extern const MethodInfo* Swipe_On_SwipeStart_m2210836367_MethodInfo_var;
extern const MethodInfo* Swipe_On_Swipe_m2206798675_MethodInfo_var;
extern const MethodInfo* Swipe_On_SwipeEnd_m693766790_MethodInfo_var;
extern const uint32_t Swipe_OnEnable_m4119303775_MetadataUsageId;
extern const uint32_t Swipe_UnsubscribeEvent_m1132877722_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1754277768;
extern const uint32_t Swipe_On_SwipeStart_m2210836367_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* SwipeDirection_t2889416990_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2694093798;
extern Il2CppCodeGenString* _stringLiteral3871193470;
extern Il2CppCodeGenString* _stringLiteral1868748486;
extern Il2CppCodeGenString* _stringLiteral3231012694;
extern const uint32_t Swipe_On_SwipeEnd_m693766790_MetadataUsageId;
extern Il2CppClass* SimpleTapHandler_t2041759604_il2cpp_TypeInfo_var;
extern const MethodInfo* TapMe_On_SimpleTap_m150421749_MethodInfo_var;
extern const uint32_t TapMe_OnEnable_m3398285078_MetadataUsageId;
extern const uint32_t TapMe_UnsubscribeEvent_m1008893349_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var;
extern const uint32_t TapMe_On_SimpleTap_m150421749_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern const uint32_t ThirdPersonCamera_Start_m1961687042_MetadataUsageId;
extern Il2CppClass* TouchStartHandler_t3045666224_il2cpp_TypeInfo_var;
extern Il2CppClass* TouchDownHandler_t16499120_il2cpp_TypeInfo_var;
extern Il2CppClass* TouchUpHandler_t3694099103_il2cpp_TypeInfo_var;
extern const MethodInfo* TouchMe_On_TouchStart_m3040594667_MethodInfo_var;
extern const MethodInfo* TouchMe_On_TouchDown_m2481547457_MethodInfo_var;
extern const MethodInfo* TouchMe_On_TouchUp_m1897545936_MethodInfo_var;
extern const uint32_t TouchMe_OnEnable_m511725484_MetadataUsageId;
extern const uint32_t TouchMe_UnsubscribeEvent_m754540737_MetadataUsageId;
extern const MethodInfo* Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506_MethodInfo_var;
extern const uint32_t TouchMe_Start_m2308516364_MetadataUsageId;
extern const uint32_t TouchMe_On_TouchStart_m3040594667_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral587809818;
extern const uint32_t TouchMe_On_TouchDown_m2481547457_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2386153277;
extern const uint32_t TouchMe_On_TouchUp_m1897545936_MetadataUsageId;
extern const uint32_t TouchMe_RandomColor_m1557085842_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1596708495;
extern const uint32_t TouchPadUIEvent_TouchDown_m3798008410_MetadataUsageId;
extern const uint32_t TouchPadUIEvent_TouchUp_m1433547427_MetadataUsageId;
extern Il2CppClass* U3CClearTextU3Ec__Iterator0_t3345759406_il2cpp_TypeInfo_var;
extern const uint32_t TouchPadUIEvent_ClearText_m1473369353_MetadataUsageId;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CClearTextU3Ec__Iterator0_MoveNext_m670795441_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CClearTextU3Ec__Iterator0_Reset_m4055416822_MetadataUsageId;
extern Il2CppClass* TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var;
extern Il2CppClass* TwistHandler_t117269532_il2cpp_TypeInfo_var;
extern Il2CppClass* TwistEndHandler_t1042340097_il2cpp_TypeInfo_var;
extern Il2CppClass* Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var;
extern const MethodInfo* TwistMe_On_TouchStart2Fingers_m200446251_MethodInfo_var;
extern const MethodInfo* TwistMe_On_Twist_m647447831_MethodInfo_var;
extern const MethodInfo* TwistMe_On_TwistEnd_m525922928_MethodInfo_var;
extern const MethodInfo* TwistMe_On_Cancel2Fingers_m948298906_MethodInfo_var;
extern const uint32_t TwistMe_OnEnable_m3712938068_MetadataUsageId;
extern const uint32_t TwistMe_UnsubscribeEvent_m1613131113_MetadataUsageId;
extern const uint32_t TwistMe_Start_m3006910900_MetadataUsageId;
extern const uint32_t TwistMe_On_TouchStart2Fingers_m200446251_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2303590499;
extern const uint32_t TwistMe_On_Twist_m647447831_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2313879957;
extern const uint32_t TwistMe_On_TwistEnd_m525922928_MetadataUsageId;
extern const uint32_t TwistMe_On_Cancel2Fingers_m948298906_MetadataUsageId;
extern Il2CppClass* DoubleTap2FingersHandler_t313188403_il2cpp_TypeInfo_var;
extern const MethodInfo* TwoDoubleTapMe_On_DoubleTap2Fingers_m426934483_MethodInfo_var;
extern const uint32_t TwoDoubleTapMe_OnEnable_m455871831_MetadataUsageId;
extern const uint32_t TwoDoubleTapMe_UnsubscribeEvent_m596587122_MetadataUsageId;
extern const uint32_t TwoDoubleTapMe_On_DoubleTap2Fingers_m426934483_MetadataUsageId;
extern Il2CppClass* DragStart2FingersHandler_t1996108375_il2cpp_TypeInfo_var;
extern Il2CppClass* Drag2FingersHandler_t599932411_il2cpp_TypeInfo_var;
extern Il2CppClass* DragEnd2FingersHandler_t1069006048_il2cpp_TypeInfo_var;
extern const MethodInfo* TwoDragMe_On_DragStart2Fingers_m3319697183_MethodInfo_var;
extern const MethodInfo* TwoDragMe_On_Drag2Fingers_m195202675_MethodInfo_var;
extern const MethodInfo* TwoDragMe_On_DragEnd2Fingers_m967453298_MethodInfo_var;
extern const MethodInfo* TwoDragMe_On_Cancel2Fingers_m2222755653_MethodInfo_var;
extern const uint32_t TwoDragMe_OnEnable_m3878514699_MetadataUsageId;
extern const uint32_t TwoDragMe_UnsubscribeEvent_m3871665702_MetadataUsageId;
extern const uint32_t TwoDragMe_Start_m1968061035_MetadataUsageId;
extern const uint32_t TwoDragMe_On_DragStart2Fingers_m3319697183_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral57471863;
extern const uint32_t TwoDragMe_On_Drag2Fingers_m195202675_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4215660982;
extern const uint32_t TwoDragMe_On_DragEnd2Fingers_m967453298_MetadataUsageId;
extern const uint32_t TwoDragMe_RandomColor_m2832589557_MetadataUsageId;
extern Il2CppClass* LongTapStart2FingersHandler_t59253952_il2cpp_TypeInfo_var;
extern Il2CppClass* LongTap2FingersHandler_t2742766376_il2cpp_TypeInfo_var;
extern Il2CppClass* LongTapEnd2FingersHandler_t3611206799_il2cpp_TypeInfo_var;
extern const MethodInfo* TwoLongTapMe_On_LongTapStart2Fingers_m930757295_MethodInfo_var;
extern const MethodInfo* TwoLongTapMe_On_LongTap2Fingers_m2992644723_MethodInfo_var;
extern const MethodInfo* TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974_MethodInfo_var;
extern const MethodInfo* TwoLongTapMe_On_Cancel2Fingers_m2591473512_MethodInfo_var;
extern const uint32_t TwoLongTapMe_OnEnable_m1179355642_MetadataUsageId;
extern const uint32_t TwoLongTapMe_UnsubscribeEvent_m2447274439_MetadataUsageId;
extern const uint32_t TwoLongTapMe_Start_m3819980634_MetadataUsageId;
extern const uint32_t TwoLongTapMe_On_LongTapStart2Fingers_m930757295_MetadataUsageId;
extern const uint32_t TwoLongTapMe_On_LongTap2Fingers_m2992644723_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1375686743;
extern const uint32_t TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974_MetadataUsageId;
extern const uint32_t TwoLongTapMe_RandomColor_m312821092_MetadataUsageId;
extern Il2CppClass* SwipeStart2FingersHandler_t3085603867_il2cpp_TypeInfo_var;
extern Il2CppClass* Swipe2FingersHandler_t2386328279_il2cpp_TypeInfo_var;
extern Il2CppClass* SwipeEnd2FingersHandler_t2863096110_il2cpp_TypeInfo_var;
extern const MethodInfo* TwoSwipe_On_SwipeStart2Fingers_m2895296955_MethodInfo_var;
extern const MethodInfo* TwoSwipe_On_Swipe2Fingers_m2117361735_MethodInfo_var;
extern const MethodInfo* TwoSwipe_On_SwipeEnd2Fingers_m1793330128_MethodInfo_var;
extern const uint32_t TwoSwipe_OnEnable_m2048578019_MetadataUsageId;
extern const uint32_t TwoSwipe_UnsubscribeEvent_m369687584_MetadataUsageId;
extern const uint32_t TwoSwipe_On_SwipeStart2Fingers_m2895296955_MetadataUsageId;
extern const uint32_t TwoSwipe_On_SwipeEnd2Fingers_m1793330128_MetadataUsageId;
extern Il2CppClass* SimpleTap2FingersHandler_t3725456324_il2cpp_TypeInfo_var;
extern const MethodInfo* TwoTapMe_On_SimpleTap2Fingers_m1985585293_MethodInfo_var;
extern const uint32_t TwoTapMe_OnEnable_m3137535336_MetadataUsageId;
extern const uint32_t TwoTapMe_UnsubscribeEvent_m4100987849_MetadataUsageId;
extern const uint32_t TwoTapMe_On_SimpleTap2Fingers_m1985585293_MetadataUsageId;
extern const uint32_t TwoTapMe_RandomColor_m3901042166_MetadataUsageId;
extern Il2CppClass* TouchDown2FingersHandler_t3403539080_il2cpp_TypeInfo_var;
extern Il2CppClass* TouchUp2FingersHandler_t384986787_il2cpp_TypeInfo_var;
extern const MethodInfo* TwoTouchMe_On_TouchStart2Fingers_m583160655_MethodInfo_var;
extern const MethodInfo* TwoTouchMe_On_TouchDown2Fingers_m4170579177_MethodInfo_var;
extern const MethodInfo* TwoTouchMe_On_TouchUp2Fingers_m286272622_MethodInfo_var;
extern const MethodInfo* TwoTouchMe_On_Cancel2Fingers_m936211736_MethodInfo_var;
extern const uint32_t TwoTouchMe_OnEnable_m3939713802_MetadataUsageId;
extern const uint32_t TwoTouchMe_UnsubscribeEvent_m3432234013_MetadataUsageId;
extern const uint32_t TwoTouchMe_Start_m1530771530_MetadataUsageId;
extern const uint32_t TwoTouchMe_On_TouchStart2Fingers_m583160655_MetadataUsageId;
extern const uint32_t TwoTouchMe_On_TouchDown2Fingers_m4170579177_MetadataUsageId;
extern const uint32_t TwoTouchMe_On_TouchUp2Fingers_m286272622_MetadataUsageId;
extern const uint32_t TwoTouchMe_RandomColor_m3906756720_MetadataUsageId;
extern const MethodInfo* UIDrag_On_TouchDown_m3549100664_MethodInfo_var;
extern const MethodInfo* UIDrag_On_TouchStart_m3437169910_MethodInfo_var;
extern const MethodInfo* UIDrag_On_TouchUp_m2370286479_MethodInfo_var;
extern const MethodInfo* UIDrag_On_TouchStart2Fingers_m4065818318_MethodInfo_var;
extern const MethodInfo* UIDrag_On_TouchDown2Fingers_m2110802572_MethodInfo_var;
extern const MethodInfo* UIDrag_On_TouchUp2Fingers_m2775525951_MethodInfo_var;
extern const uint32_t UIDrag_OnEnable_m2279416155_MetadataUsageId;
extern const uint32_t UIDrag_OnDestroy_m135766882_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchStart_m3437169910_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchDown_m3549100664_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchStart2Fingers_m4065818318_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchDown2Fingers_m2110802572_MetadataUsageId;
extern const uint32_t UIDrag_On_TouchUp2Fingers_m2775525951_MetadataUsageId;
extern Il2CppClass* PinchHandler_t3939026435_il2cpp_TypeInfo_var;
extern const MethodInfo* UIPinch_On_Pinch_m1190717819_MethodInfo_var;
extern const uint32_t UIPinch_OnEnable_m1416134943_MetadataUsageId;
extern const uint32_t UIPinch_OnDestroy_m744302208_MetadataUsageId;
extern const uint32_t UIPinch_On_Pinch_m1190717819_MetadataUsageId;
extern const MethodInfo* UITwist_On_Twist_m2372298779_MethodInfo_var;
extern const uint32_t UITwist_OnEnable_m943932592_MetadataUsageId;
extern const uint32_t UITwist_OnDestroy_m3205865637_MetadataUsageId;
extern const uint32_t UITwist_On_Twist_m2372298779_MetadataUsageId;
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3107983927;
extern const uint32_t u2dexGrid_OnDrawGizmos_m1308842860_MetadataUsageId;
extern const uint32_t u2dexGrid_RoundInteger_m2792359982_MetadataUsageId;
extern const uint32_t u2dexGrid_Round_m158254686_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1730479323 (Attribute_t542643598 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SROptionsPropertyChanged::Invoke(System.Object,System.String)
extern "C"  void SROptionsPropertyChanged_Invoke_m3497097451 (SROptionsPropertyChanged_t1005294736 * __this, Il2CppObject * ___sender0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SwipeStartHandler__ctor_m2637179440 (SwipeStartHandler_t1813931207 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeStart(HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler)
extern "C"  void EasyTouch_add_On_SwipeStart_m2665075732 (Il2CppObject * __this /* static, unused */, SwipeStartHandler_t1813931207 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SwipeHandler__ctor_m3769459354 (SwipeHandler_t4041355147 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Swipe(HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler)
extern "C"  void EasyTouch_add_On_Swipe_m1394036586 (Il2CppObject * __this /* static, unused */, SwipeHandler_t4041355147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SwipeEndHandler__ctor_m3272965725 (SwipeEndHandler_t567777398 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeEnd(HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler)
extern "C"  void EasyTouch_add_On_SwipeEnd_m2228232980 (Il2CppObject * __this /* static, unused */, SwipeEndHandler_t567777398 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Swipe::UnsubscribeEvent()
extern "C"  void Swipe_UnsubscribeEvent_m1132877722 (Swipe_t3475047630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeStart(HedgehogTeam.EasyTouch.EasyTouch/SwipeStartHandler)
extern "C"  void EasyTouch_remove_On_SwipeStart_m1460797779 (Il2CppObject * __this /* static, unused */, SwipeStartHandler_t1813931207 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Swipe(HedgehogTeam.EasyTouch.EasyTouch/SwipeHandler)
extern "C"  void EasyTouch_remove_On_Swipe_m2588095719 (Il2CppObject * __this /* static, unused */, SwipeHandler_t4041355147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeEnd(HedgehogTeam.EasyTouch.EasyTouch/SwipeEndHandler)
extern "C"  void EasyTouch_remove_On_SwipeEnd_m3851242387 (Il2CppObject * __this /* static, unused */, SwipeEndHandler_t567777398 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HedgehogTeam.EasyTouch.Gesture::GetTouchToWorldPoint(System.Single)
extern "C"  Vector3_t2243707580  Gesture_GetTouchToWorldPoint_m3593534480 (Gesture_t367995397 * __this, float ___z0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HedgehogTeam.EasyTouch.Gesture::GetSwipeOrDragAngle()
extern "C"  float Gesture_GetSwipeOrDragAngle_m2164539425 (Gesture_t367995397 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t2243707579  Vector2_get_normalized_m2985402409 (Vector2_t2243707579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m2359963436 (float* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m3881798623 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SimpleTapHandler__ctor_m1181745501 (SimpleTapHandler_t2041759604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SimpleTap(HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler)
extern "C"  void EasyTouch_add_On_SimpleTap_m550386160 (Il2CppObject * __this /* static, unused */, SimpleTapHandler_t2041759604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TapMe::UnsubscribeEvent()
extern "C"  void TapMe_UnsubscribeEvent_m1008893349 (TapMe_t2856432827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SimpleTap(HedgehogTeam.EasyTouch.EasyTouch/SimpleTapHandler)
extern "C"  void EasyTouch_remove_On_SimpleTap_m1700056411 (Il2CppObject * __this /* static, unused */, SimpleTapHandler_t2041759604 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t257310565_m226639069(__this, method) ((  Renderer_t257310565 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2884721203 (Il2CppObject * __this /* static, unused */, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3811852957 (Color_t2020392075 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m577844242 (Material_t193706927 * __this, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindWithTag_m1929006324 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2243707580  Vector3_get_up_m2725403797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2243707580  Transform_get_forward_m1833488937 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m2407545601 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_Lerp_m2935648359 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform)
extern "C"  void Transform_LookAt_m2514033256 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetTwoFingerPickMethod(HedgehogTeam.EasyTouch.EasyTouch/TwoFingerPickMethod)
extern "C"  void EasyTouch_SetTwoFingerPickMethod_m2643975084 (Il2CppObject * __this /* static, unused */, int32_t ___pickMethod0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchStartHandler__ctor_m37798995 (TouchStartHandler_t3045666224 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchStart(HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler)
extern "C"  void EasyTouch_add_On_TouchStart_m2583984596 (Il2CppObject * __this /* static, unused */, TouchStartHandler_t3045666224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchDownHandler__ctor_m929731709 (TouchDownHandler_t16499120 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchDown(HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler)
extern "C"  void EasyTouch_add_On_TouchDown_m625431120 (Il2CppObject * __this /* static, unused */, TouchDownHandler_t16499120 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchUpHandler__ctor_m1021741058 (TouchUpHandler_t3694099103 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchUp(HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler)
extern "C"  void EasyTouch_add_On_TouchUp_m3031495162 (Il2CppObject * __this /* static, unused */, TouchUpHandler_t3694099103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchMe::UnsubscribeEvent()
extern "C"  void TouchMe_UnsubscribeEvent_m754540737 (TouchMe_t1950908167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchStart(HedgehogTeam.EasyTouch.EasyTouch/TouchStartHandler)
extern "C"  void EasyTouch_remove_On_TouchStart_m1488478227 (Il2CppObject * __this /* static, unused */, TouchStartHandler_t3045666224 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchDown(HedgehogTeam.EasyTouch.EasyTouch/TouchDownHandler)
extern "C"  void EasyTouch_remove_On_TouchDown_m1427941651 (Il2CppObject * __this /* static, unused */, TouchDownHandler_t16499120 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchUp(HedgehogTeam.EasyTouch.EasyTouch/TouchUpHandler)
extern "C"  void EasyTouch_remove_On_TouchUp_m1724410775 (Il2CppObject * __this /* static, unused */, TouchUpHandler_t3694099103 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.TextMesh>()
#define Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506(__this, method) ((  TextMesh_t1641806576 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// UnityEngine.Color UnityEngine.Material::get_color()
extern "C"  Color_t2020392075  Material_get_color_m668215843 (Material_t193706927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchMe::RandomColor()
extern "C"  void TouchMe_RandomColor_m1557085842 (TouchMe_t1950908167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextMesh::set_text(System.String)
extern "C"  void TextMesh_set_text_m3390063817 (TextMesh_t1641806576 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TouchPadUIEvent::ClearText(UnityEngine.UI.Text)
extern "C"  Il2CppObject * TouchPadUIEvent_ClearText_m1473369353 (TouchPadUIEvent_t997246418 * __this, Text_t356221433 * ___textToCLead0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m775491729 (Vector2_t2243707579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchPadUIEvent/<ClearText>c__Iterator0::.ctor()
extern "C"  void U3CClearTextU3Ec__Iterator0__ctor_m3571755835 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchStart2FingersHandler__ctor_m3302552411 (TouchStart2FingersHandler_t3096813332 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler)
extern "C"  void EasyTouch_add_On_TouchStart2Fingers_m1024224148 (Il2CppObject * __this /* static, unused */, TouchStart2FingersHandler_t3096813332 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TwistHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TwistHandler__ctor_m448382751 (TwistHandler_t117269532 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Twist(HedgehogTeam.EasyTouch.EasyTouch/TwistHandler)
extern "C"  void EasyTouch_add_On_Twist_m3882482804 (Il2CppObject * __this /* static, unused */, TwistHandler_t117269532 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TwistEndHandler__ctor_m870850134 (TwistEndHandler_t1042340097 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TwistEnd(HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler)
extern "C"  void EasyTouch_add_On_TwistEnd_m2544505780 (Il2CppObject * __this /* static, unused */, TwistEndHandler_t1042340097 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void Cancel2FingersHandler__ctor_m933193436 (Cancel2FingersHandler_t2820220009 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Cancel2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler)
extern "C"  void EasyTouch_add_On_Cancel2Fingers_m646354580 (Il2CppObject * __this /* static, unused */, Cancel2FingersHandler_t2820220009 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwistMe::UnsubscribeEvent()
extern "C"  void TwistMe_UnsubscribeEvent_m1613131113 (TwistMe_t1887030447 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchStart2FingersHandler)
extern "C"  void EasyTouch_remove_On_TouchStart2Fingers_m3416141907 (Il2CppObject * __this /* static, unused */, TouchStart2FingersHandler_t3096813332 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Twist(HedgehogTeam.EasyTouch.EasyTouch/TwistHandler)
extern "C"  void EasyTouch_remove_On_Twist_m1411584747 (Il2CppObject * __this /* static, unused */, TwistHandler_t117269532 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TwistEnd(HedgehogTeam.EasyTouch.EasyTouch/TwistEndHandler)
extern "C"  void EasyTouch_remove_On_TwistEnd_m2089157299 (Il2CppObject * __this /* static, unused */, TwistEndHandler_t1042340097 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Cancel2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Cancel2FingersHandler)
extern "C"  void EasyTouch_remove_On_Cancel2Fingers_m3690314195 (Il2CppObject * __this /* static, unused */, Cancel2FingersHandler_t2820220009 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnableTwist(System.Boolean)
extern "C"  void EasyTouch_SetEnableTwist_m199134308 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetEnablePinch(System.Boolean)
extern "C"  void EasyTouch_SetEnablePinch_m590621477 (Il2CppObject * __this /* static, unused */, bool ___enable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m1743927093 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m1813392066 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DoubleTap2FingersHandler__ctor_m1622909298 (DoubleTap2FingersHandler_t313188403 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler)
extern "C"  void EasyTouch_add_On_DoubleTap2Fingers_m3474334234 (Il2CppObject * __this /* static, unused */, DoubleTap2FingersHandler_t313188403 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoDoubleTapMe::UnsubscribeEvent()
extern "C"  void TwoDoubleTapMe_UnsubscribeEvent_m596587122 (TwoDoubleTapMe_t376817258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DoubleTap2FingersHandler)
extern "C"  void EasyTouch_remove_On_DoubleTap2Fingers_m4218316407 (Il2CppObject * __this /* static, unused */, DoubleTap2FingersHandler_t313188403 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DragStart2FingersHandler__ctor_m3803132154 (DragStart2FingersHandler_t1996108375 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler)
extern "C"  void EasyTouch_add_On_DragStart2Fingers_m1162086938 (Il2CppObject * __this /* static, unused */, DragStart2FingersHandler_t1996108375 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void Drag2FingersHandler__ctor_m4221981472 (Drag2FingersHandler_t599932411 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Drag2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler)
extern "C"  void EasyTouch_add_On_Drag2Fingers_m2386181684 (Il2CppObject * __this /* static, unused */, Drag2FingersHandler_t599932411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void DragEnd2FingersHandler__ctor_m2199226361 (DragEnd2FingersHandler_t1069006048 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_DragEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler)
extern "C"  void EasyTouch_add_On_DragEnd2Fingers_m101490336 (Il2CppObject * __this /* static, unused */, DragEnd2FingersHandler_t1069006048 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoDragMe::UnsubscribeEvent()
extern "C"  void TwoDragMe_UnsubscribeEvent_m3871665702 (TwoDragMe_t2032870650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragStart2FingersHandler)
extern "C"  void EasyTouch_remove_On_DragStart2Fingers_m86474871 (Il2CppObject * __this /* static, unused */, DragStart2FingersHandler_t1996108375 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Drag2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Drag2FingersHandler)
extern "C"  void EasyTouch_remove_On_Drag2Fingers_m3828850227 (Il2CppObject * __this /* static, unused */, Drag2FingersHandler_t599932411 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_DragEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/DragEnd2FingersHandler)
extern "C"  void EasyTouch_remove_On_DragEnd2Fingers_m1103201475 (Il2CppObject * __this /* static, unused */, DragEnd2FingersHandler_t1069006048 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoDragMe::RandomColor()
extern "C"  void TwoDragMe_RandomColor_m2832589557 (TwoDragMe_t2032870650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HedgehogTeam.EasyTouch.Gesture::GetTouchToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Gesture_GetTouchToWorldPoint_m2105441024 (Gesture_t367995397 * __this, Vector3_t2243707580  ___position3D0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoDragMe::On_DragEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoDragMe_On_DragEnd2Fingers_m967453298 (TwoDragMe_t2032870650 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void LongTapStart2FingersHandler__ctor_m4029944885 (LongTapStart2FingersHandler_t59253952 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler)
extern "C"  void EasyTouch_add_On_LongTapStart2Fingers_m2902511508 (Il2CppObject * __this /* static, unused */, LongTapStart2FingersHandler_t59253952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void LongTap2FingersHandler__ctor_m2664185749 (LongTap2FingersHandler_t2742766376 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler)
extern "C"  void EasyTouch_add_On_LongTap2Fingers_m2932670112 (Il2CppObject * __this /* static, unused */, LongTap2FingersHandler_t2742766376 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void LongTapEnd2FingersHandler__ctor_m2802920322 (LongTapEnd2FingersHandler_t3611206799 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler)
extern "C"  void EasyTouch_add_On_LongTapEnd2Fingers_m228344724 (Il2CppObject * __this /* static, unused */, LongTapEnd2FingersHandler_t3611206799 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoLongTapMe::UnsubscribeEvent()
extern "C"  void TwoLongTapMe_UnsubscribeEvent_m2447274439 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapStart2FingersHandler)
extern "C"  void EasyTouch_remove_On_LongTapStart2Fingers_m4268548115 (Il2CppObject * __this /* static, unused */, LongTapStart2FingersHandler_t59253952 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTap2FingersHandler)
extern "C"  void EasyTouch_remove_On_LongTap2Fingers_m1992347267 (Il2CppObject * __this /* static, unused */, LongTap2FingersHandler_t2742766376 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/LongTapEnd2FingersHandler)
extern "C"  void EasyTouch_remove_On_LongTapEnd2Fingers_m3457310035 (Il2CppObject * __this /* static, unused */, LongTapEnd2FingersHandler_t3611206799 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoLongTapMe::RandomColor()
extern "C"  void TwoLongTapMe_RandomColor_m312821092 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoLongTapMe::On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974 (TwoLongTapMe_t2356062841 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SwipeStart2FingersHandler__ctor_m647530272 (SwipeStart2FingersHandler_t3085603867 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler)
extern "C"  void EasyTouch_add_On_SwipeStart2Fingers_m4057703828 (Il2CppObject * __this /* static, unused */, SwipeStart2FingersHandler_t3085603867 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void Swipe2FingersHandler__ctor_m1389004394 (Swipe2FingersHandler_t2386328279 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Swipe2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler)
extern "C"  void EasyTouch_add_On_Swipe2Fingers_m2192986026 (Il2CppObject * __this /* static, unused */, Swipe2FingersHandler_t2386328279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SwipeEnd2FingersHandler__ctor_m3456079153 (SwipeEnd2FingersHandler_t2863096110 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler)
extern "C"  void EasyTouch_add_On_SwipeEnd2Fingers_m1320382740 (Il2CppObject * __this /* static, unused */, SwipeEnd2FingersHandler_t2863096110 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoSwipe::UnsubscribeEvent()
extern "C"  void TwoSwipe_UnsubscribeEvent_m369687584 (TwoSwipe_t373754724 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeStart2FingersHandler)
extern "C"  void EasyTouch_remove_On_SwipeStart2Fingers_m804622931 (Il2CppObject * __this /* static, unused */, SwipeStart2FingersHandler_t3085603867 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Swipe2Fingers(HedgehogTeam.EasyTouch.EasyTouch/Swipe2FingersHandler)
extern "C"  void EasyTouch_remove_On_Swipe2Fingers_m3559413287 (Il2CppObject * __this /* static, unused */, Swipe2FingersHandler_t2386328279 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SwipeEnd2FingersHandler)
extern "C"  void EasyTouch_remove_On_SwipeEnd2Fingers_m635268243 (Il2CppObject * __this /* static, unused */, SwipeEnd2FingersHandler_t2863096110 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void SimpleTap2FingersHandler__ctor_m1866037649 (SimpleTap2FingersHandler_t3725456324 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler)
extern "C"  void EasyTouch_add_On_SimpleTap2Fingers_m2286937328 (Il2CppObject * __this /* static, unused */, SimpleTap2FingersHandler_t3725456324 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoTapMe::UnsubscribeEvent()
extern "C"  void TwoTapMe_UnsubscribeEvent_m4100987849 (TwoTapMe_t3705696635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.EasyTouch/SimpleTap2FingersHandler)
extern "C"  void EasyTouch_remove_On_SimpleTap2Fingers_m3825049051 (Il2CppObject * __this /* static, unused */, SimpleTap2FingersHandler_t3725456324 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoTapMe::RandomColor()
extern "C"  void TwoTapMe_RandomColor_m3901042166 (TwoTapMe_t3705696635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchDown2FingersHandler__ctor_m1028756337 (TouchDown2FingersHandler_t3403539080 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchDown2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler)
extern "C"  void EasyTouch_add_On_TouchDown2Fingers_m1064519056 (Il2CppObject * __this /* static, unused */, TouchDown2FingersHandler_t3403539080 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void TouchUp2FingersHandler__ctor_m1257200738 (TouchUp2FingersHandler_t384986787 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_TouchUp2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler)
extern "C"  void EasyTouch_add_On_TouchUp2Fingers_m1044638522 (Il2CppObject * __this /* static, unused */, TouchUp2FingersHandler_t384986787 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoTouchMe::UnsubscribeEvent()
extern "C"  void TwoTouchMe_UnsubscribeEvent_m3432234013 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchDown2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchDown2FingersHandler)
extern "C"  void EasyTouch_remove_On_TouchDown2Fingers_m966509139 (Il2CppObject * __this /* static, unused */, TouchDown2FingersHandler_t3403539080 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_TouchUp2Fingers(HedgehogTeam.EasyTouch.EasyTouch/TouchUp2FingersHandler)
extern "C"  void EasyTouch_remove_On_TouchUp2Fingers_m2322502551 (Il2CppObject * __this /* static, unused */, TouchUp2FingersHandler_t384986787 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoTouchMe::RandomColor()
extern "C"  void TwoTouchMe_RandomColor_m3906756720 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TwoTouchMe::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoTouchMe_On_TouchUp2Fingers_m286272622 (TwoTouchMe_t4294184479 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::SetUICompatibily(System.Boolean)
extern "C"  void EasyTouch_SetUICompatibily_m3534348079 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m10844547 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetAsLastSibling()
extern "C"  void Transform_SetAsLastSibling_m1528402907 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch/PinchHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PinchHandler__ctor_m194359596 (PinchHandler_t3939026435 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::add_On_Pinch(HedgehogTeam.EasyTouch.EasyTouch/PinchHandler)
extern "C"  void EasyTouch_add_On_Pinch_m4248559610 (Il2CppObject * __this /* static, unused */, PinchHandler_t3939026435 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HedgehogTeam.EasyTouch.EasyTouch::remove_On_Pinch(HedgehogTeam.EasyTouch.EasyTouch/PinchHandler)
extern "C"  void EasyTouch_remove_On_Pinch_m171636591 (Il2CppObject * __this /* static, unused */, PinchHandler_t3939026435 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m3074381503 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_delta()
extern "C"  Vector2_t2243707579  PointerEventData_get_delta_m1072163964 (PointerEventData_t1599784723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m3987539815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Gizmos::get_color()
extern "C"  Color_t2020392075  Gizmos_get_color_m837177613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m494992840 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::.ctor()
extern "C"  void Camera__ctor_m2367274244 (Camera_t189460977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnfinityGames.U2DEX.u2dexGrid::RoundInteger(System.Int32,System.Single)
extern "C"  int32_t u2dexGrid_RoundInteger_m2792359982 (u2dexGrid_t3875635992 * __this, int32_t ___integer0, float ___multipleOf1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t189460977_m4200645945(__this, method) ((  Camera_t189460977 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2650145732_gshared)(__this, method)
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m3384007405 (Camera_t189460977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2354025655 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnfinityGames.U2DEX.u2dexGrid::Round(System.Single,System.Single)
extern "C"  float u2dexGrid_Round_m158254686 (u2dexGrid_t3875635992 * __this, float ___input0, float ___SnapTo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnfinityGames.U2DEX.u2dexGrid::SnapToGridSize(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  u2dexGrid_SnapToGridSize_m778086205 (u2dexGrid_t3875635992 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m1315654064 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern "C"  int32_t Mathf_FloorToInt_m4005035722 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern "C"  int32_t Mathf_CeilToInt_m2672598779 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnfinityGames.U2DEX.u2dexGrid::FixIfNaN(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  u2dexGrid_FixIfNaN_m505747756 (u2dexGrid_t3875635992 * __this, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::IsNaN(System.Single)
extern "C"  bool Single_IsNaN_m2349591895 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SROptions/IncrementAttribute::.ctor(System.Double)
extern "C"  void IncrementAttribute__ctor_m3592969914 (IncrementAttribute_t339343029 * __this, double ___increment0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		double L_0 = ___increment0;
		__this->set_Increment_0(L_0);
		return;
	}
}
// System.Void SROptions/NumberRangeAttribute::.ctor(System.Double,System.Double)
extern "C"  void NumberRangeAttribute__ctor_m2388738841 (NumberRangeAttribute_t2675108520 * __this, double ___min0, double ___max1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		double L_0 = ___min0;
		__this->set_Min_1(L_0);
		double L_1 = ___max1;
		__this->set_Max_0(L_1);
		return;
	}
}
// System.Void SROptions/SortAttribute::.ctor(System.Int32)
extern "C"  void SortAttribute__ctor_m4280728884 (SortAttribute_t3684079898 * __this, int32_t ___priority0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1730479323(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___priority0;
		__this->set_SortPriority_0(L_0);
		return;
	}
}
// System.Void SROptionsPropertyChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void SROptionsPropertyChanged__ctor_m2997645027 (SROptionsPropertyChanged_t1005294736 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void SROptionsPropertyChanged::Invoke(System.Object,System.String)
extern "C"  void SROptionsPropertyChanged_Invoke_m3497097451 (SROptionsPropertyChanged_t1005294736 * __this, Il2CppObject * ___sender0, String_t* ___propertyName1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		SROptionsPropertyChanged_Invoke_m3497097451((SROptionsPropertyChanged_t1005294736 *)__this->get_prev_9(),___sender0, ___propertyName1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___sender0, String_t* ___propertyName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sender0, ___propertyName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___sender0, String_t* ___propertyName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sender0, ___propertyName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___propertyName1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___sender0, ___propertyName1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SROptionsPropertyChanged::BeginInvoke(System.Object,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SROptionsPropertyChanged_BeginInvoke_m2219448084 (SROptionsPropertyChanged_t1005294736 * __this, Il2CppObject * ___sender0, String_t* ___propertyName1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender0;
	__d_args[1] = ___propertyName1;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void SROptionsPropertyChanged::EndInvoke(System.IAsyncResult)
extern "C"  void SROptionsPropertyChanged_EndInvoke_m873567689 (SROptionsPropertyChanged_t1005294736 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Swipe::.ctor()
extern "C"  void Swipe__ctor_m4106242907 (Swipe_t3475047630 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Swipe::OnEnable()
extern "C"  void Swipe_OnEnable_m4119303775 (Swipe_t3475047630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Swipe_OnEnable_m4119303775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)Swipe_On_SwipeStart_m2210836367_MethodInfo_var);
		SwipeStartHandler_t1813931207 * L_1 = (SwipeStartHandler_t1813931207 *)il2cpp_codegen_object_new(SwipeStartHandler_t1813931207_il2cpp_TypeInfo_var);
		SwipeStartHandler__ctor_m2637179440(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_SwipeStart_m2665075732(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Swipe_On_Swipe_m2206798675_MethodInfo_var);
		SwipeHandler_t4041355147 * L_3 = (SwipeHandler_t4041355147 *)il2cpp_codegen_object_new(SwipeHandler_t4041355147_il2cpp_TypeInfo_var);
		SwipeHandler__ctor_m3769459354(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_Swipe_m1394036586(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)Swipe_On_SwipeEnd_m693766790_MethodInfo_var);
		SwipeEndHandler_t567777398 * L_5 = (SwipeEndHandler_t567777398 *)il2cpp_codegen_object_new(SwipeEndHandler_t567777398_il2cpp_TypeInfo_var);
		SwipeEndHandler__ctor_m3272965725(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_SwipeEnd_m2228232980(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Swipe::OnDisable()
extern "C"  void Swipe_OnDisable_m550881870 (Swipe_t3475047630 * __this, const MethodInfo* method)
{
	{
		Swipe_UnsubscribeEvent_m1132877722(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Swipe::OnDestroy()
extern "C"  void Swipe_OnDestroy_m3696952336 (Swipe_t3475047630 * __this, const MethodInfo* method)
{
	{
		Swipe_UnsubscribeEvent_m1132877722(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Swipe::UnsubscribeEvent()
extern "C"  void Swipe_UnsubscribeEvent_m1132877722 (Swipe_t3475047630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Swipe_UnsubscribeEvent_m1132877722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)Swipe_On_SwipeStart_m2210836367_MethodInfo_var);
		SwipeStartHandler_t1813931207 * L_1 = (SwipeStartHandler_t1813931207 *)il2cpp_codegen_object_new(SwipeStartHandler_t1813931207_il2cpp_TypeInfo_var);
		SwipeStartHandler__ctor_m2637179440(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_SwipeStart_m1460797779(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Swipe_On_Swipe_m2206798675_MethodInfo_var);
		SwipeHandler_t4041355147 * L_3 = (SwipeHandler_t4041355147 *)il2cpp_codegen_object_new(SwipeHandler_t4041355147_il2cpp_TypeInfo_var);
		SwipeHandler__ctor_m3769459354(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_Swipe_m2588095719(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)Swipe_On_SwipeEnd_m693766790_MethodInfo_var);
		SwipeEndHandler_t567777398 * L_5 = (SwipeEndHandler_t567777398 *)il2cpp_codegen_object_new(SwipeEndHandler_t567777398_il2cpp_TypeInfo_var);
		SwipeEndHandler__ctor_m3272965725(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_SwipeEnd_m3851242387(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Swipe::On_SwipeStart(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void Swipe_On_SwipeStart_m2210836367 (Swipe_t3475047630 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Swipe_On_SwipeStart_m2210836367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_swipeText_3();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral1754277768);
		return;
	}
}
// System.Void Swipe::On_Swipe(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void Swipe_On_Swipe_m2206798675 (Swipe_t3475047630 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Gesture_GetTouchToWorldPoint_m3593534480(L_0, (5.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = __this->get_trail_2();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = V_0;
		NullCheck(L_3);
		Transform_set_position_m2469242620(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Swipe::On_SwipeEnd(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void Swipe_On_SwipeEnd_m693766790 (Swipe_t3475047630 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Swipe_On_SwipeEnd_m693766790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		float L_1 = Gesture_GetSwipeOrDragAngle_m2164539425(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Text_t356221433 * L_2 = __this->get_swipeText_3();
		ObjectU5BU5D_t3614634134* L_3 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral2694093798);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2694093798);
		ObjectU5BU5D_t3614634134* L_4 = L_3;
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		int32_t* L_6 = L_5->get_address_of_swipe_19();
		Il2CppObject * L_7 = Box(SwipeDirection_t2889416990_il2cpp_TypeInfo_var, L_6);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_8);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_4;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral3871193470);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3871193470);
		ObjectU5BU5D_t3614634134* L_10 = L_9;
		Gesture_t367995397 * L_11 = ___gesture0;
		NullCheck(L_11);
		Vector2_t2243707579 * L_12 = L_11->get_address_of_swipeVector_21();
		Vector2_t2243707579  L_13 = Vector2_get_normalized_m2985402409(L_12, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_15);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_10;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral1868748486);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1868748486);
		ObjectU5BU5D_t3614634134* L_17 = L_16;
		String_t* L_18 = Single_ToString_m2359963436((&V_0), _stringLiteral3231012694, /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3881798623(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_19);
		return;
	}
}
// System.Void TapMe::.ctor()
extern "C"  void TapMe__ctor_m1032983586 (TapMe_t2856432827 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TapMe::OnEnable()
extern "C"  void TapMe_OnEnable_m3398285078 (TapMe_t2856432827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapMe_OnEnable_m3398285078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TapMe_On_SimpleTap_m150421749_MethodInfo_var);
		SimpleTapHandler_t2041759604 * L_1 = (SimpleTapHandler_t2041759604 *)il2cpp_codegen_object_new(SimpleTapHandler_t2041759604_il2cpp_TypeInfo_var);
		SimpleTapHandler__ctor_m1181745501(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_SimpleTap_m550386160(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TapMe::OnDisable()
extern "C"  void TapMe_OnDisable_m636996455 (TapMe_t2856432827 * __this, const MethodInfo* method)
{
	{
		TapMe_UnsubscribeEvent_m1008893349(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TapMe::OnDestroy()
extern "C"  void TapMe_OnDestroy_m98719373 (TapMe_t2856432827 * __this, const MethodInfo* method)
{
	{
		TapMe_UnsubscribeEvent_m1008893349(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TapMe::UnsubscribeEvent()
extern "C"  void TapMe_UnsubscribeEvent_m1008893349 (TapMe_t2856432827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapMe_UnsubscribeEvent_m1008893349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TapMe_On_SimpleTap_m150421749_MethodInfo_var);
		SimpleTapHandler_t2041759604 * L_1 = (SimpleTapHandler_t2041759604 *)il2cpp_codegen_object_new(SimpleTapHandler_t2041759604_il2cpp_TypeInfo_var);
		SimpleTapHandler__ctor_m1181745501(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_SimpleTap_m1700056411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TapMe::On_SimpleTap(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TapMe_On_SimpleTap_m150421749 (TapMe_t2856432827 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TapMe_On_SimpleTap_m150421749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005d;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		float L_7 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_8 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_9 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m3811852957(&L_10, L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_10, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void ThirdPersonCamera::.ctor()
extern "C"  void ThirdPersonCamera__ctor_m2805428026 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ThirdPersonCamera::Start()
extern "C"  void ThirdPersonCamera_Start_m1961687042 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ThirdPersonCamera_Start_m1961687042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindWithTag_m1929006324(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		__this->set_follow_7(L_1);
		return;
	}
}
// System.Void ThirdPersonCamera::LateUpdate()
extern "C"  void ThirdPersonCamera_LateUpdate_m466960169 (ThirdPersonCamera_t2751132817 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_follow_7();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_distanceUp_3();
		Vector3_t2243707580  L_4 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_follow_7();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_forward_m1833488937(L_6, /*hidden argument*/NULL);
		float L_8 = __this->get_distanceAway_2();
		Vector3_t2243707580  L_9 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t2243707580  L_10 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_5, L_9, /*hidden argument*/NULL);
		__this->set_targetPosition_6(L_10);
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = __this->get_targetPosition_6();
		float L_15 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = __this->get_smooth_4();
		Vector3_t2243707580  L_17 = Vector3_Lerp_m2935648359(NULL /*static, unused*/, L_13, L_14, ((float)((float)L_15*(float)L_16)), /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_position_m2469242620(L_11, L_17, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_19 = __this->get_follow_7();
		NullCheck(L_18);
		Transform_LookAt_m2514033256(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TooglePickMethodUI::.ctor()
extern "C"  void TooglePickMethodUI__ctor_m713541623 (TooglePickMethodUI_t2222958494 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TooglePickMethodUI::SetPickMethod2Finger(System.Boolean)
extern "C"  void TooglePickMethodUI_SetPickMethod2Finger_m2303705195 (TooglePickMethodUI_t2222958494 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		EasyTouch_SetTwoFingerPickMethod_m2643975084(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
	}

IL_000c:
	{
		return;
	}
}
// System.Void TooglePickMethodUI::SetPickMethod2Averager(System.Boolean)
extern "C"  void TooglePickMethodUI_SetPickMethod2Averager_m640324167 (TooglePickMethodUI_t2222958494 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		EasyTouch_SetTwoFingerPickMethod_m2643975084(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_000c:
	{
		return;
	}
}
// System.Void TouchMe::.ctor()
extern "C"  void TouchMe__ctor_m2563529456 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchMe::OnEnable()
extern "C"  void TouchMe_OnEnable_m511725484 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_OnEnable_m511725484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TouchMe_On_TouchStart_m3040594667_MethodInfo_var);
		TouchStartHandler_t3045666224 * L_1 = (TouchStartHandler_t3045666224 *)il2cpp_codegen_object_new(TouchStartHandler_t3045666224_il2cpp_TypeInfo_var);
		TouchStartHandler__ctor_m37798995(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart_m2583984596(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TouchMe_On_TouchDown_m2481547457_MethodInfo_var);
		TouchDownHandler_t16499120 * L_3 = (TouchDownHandler_t16499120 *)il2cpp_codegen_object_new(TouchDownHandler_t16499120_il2cpp_TypeInfo_var);
		TouchDownHandler__ctor_m929731709(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown_m625431120(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TouchMe_On_TouchUp_m1897545936_MethodInfo_var);
		TouchUpHandler_t3694099103 * L_5 = (TouchUpHandler_t3694099103 *)il2cpp_codegen_object_new(TouchUpHandler_t3694099103_il2cpp_TypeInfo_var);
		TouchUpHandler__ctor_m1021741058(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp_m3031495162(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchMe::OnDisable()
extern "C"  void TouchMe_OnDisable_m1072065819 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	{
		TouchMe_UnsubscribeEvent_m754540737(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchMe::OnDestroy()
extern "C"  void TouchMe_OnDestroy_m3088865049 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	{
		TouchMe_UnsubscribeEvent_m754540737(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchMe::UnsubscribeEvent()
extern "C"  void TouchMe_UnsubscribeEvent_m754540737 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_UnsubscribeEvent_m754540737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TouchMe_On_TouchStart_m3040594667_MethodInfo_var);
		TouchStartHandler_t3045666224 * L_1 = (TouchStartHandler_t3045666224 *)il2cpp_codegen_object_new(TouchStartHandler_t3045666224_il2cpp_TypeInfo_var);
		TouchStartHandler__ctor_m37798995(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart_m1488478227(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TouchMe_On_TouchDown_m2481547457_MethodInfo_var);
		TouchDownHandler_t16499120 * L_3 = (TouchDownHandler_t16499120 *)il2cpp_codegen_object_new(TouchDownHandler_t16499120_il2cpp_TypeInfo_var);
		TouchDownHandler__ctor_m929731709(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown_m1427941651(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TouchMe_On_TouchUp_m1897545936_MethodInfo_var);
		TouchUpHandler_t3694099103 * L_5 = (TouchUpHandler_t3694099103 *)il2cpp_codegen_object_new(TouchUpHandler_t3694099103_il2cpp_TypeInfo_var);
		TouchUpHandler__ctor_m1021741058(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp_m1724410775(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchMe::Start()
extern "C"  void TouchMe_Start_m2308516364 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_Start_m2308516364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506_MethodInfo_var);
		__this->set_textMesh_2(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t257310565 * L_2 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_2);
		Material_t193706927 * L_3 = Renderer_get_material_m2553789785(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t2020392075  L_4 = Material_get_color_m668215843(L_3, /*hidden argument*/NULL);
		__this->set_startColor_3(L_4);
		return;
	}
}
// System.Void TouchMe::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TouchMe_On_TouchStart_m3040594667 (TouchMe_t1950908167 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_On_TouchStart_m3040594667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		TouchMe_RandomColor_m1557085842(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void TouchMe::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TouchMe_On_TouchDown_m2481547457 (TouchMe_t1950908167 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_On_TouchDown_m2481547457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		TextMesh_t1641806576 * L_4 = __this->get_textMesh_2();
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		float* L_6 = ((BaseFinger_t1402521766 *)L_5)->get_address_of_actionTime_5();
		String_t* L_7 = Single_ToString_m2359963436(L_6, _stringLiteral3231012694, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral587809818, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		TextMesh_set_text_m3390063817(L_4, L_8, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void TouchMe::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TouchMe_On_TouchUp_m1897545936 (TouchMe_t1950908167 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_On_TouchUp_m1897545936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = __this->get_startColor_3();
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_8 = __this->get_textMesh_2();
		NullCheck(L_8);
		TextMesh_set_text_m3390063817(L_8, _stringLiteral2386153277, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void TouchMe::RandomColor()
extern "C"  void TouchMe_RandomColor_m1557085842 (TouchMe_t1950908167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchMe_RandomColor_m1557085842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_1);
		Material_t193706927 * L_2 = Renderer_get_material_m2553789785(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m3811852957(&L_6, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m577844242(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchPadUIEvent::.ctor()
extern "C"  void TouchPadUIEvent__ctor_m810352619 (TouchPadUIEvent_t997246418 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchPadUIEvent::TouchDown()
extern "C"  void TouchPadUIEvent_TouchDown_m3798008410 (TouchPadUIEvent_t997246418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchPadUIEvent_TouchDown_m3798008410_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_touchDownText_2();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral1596708495);
		Text_t356221433 * L_1 = __this->get_touchDownText_2();
		Il2CppObject * L_2 = TouchPadUIEvent_ClearText_m1473369353(__this, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TouchPadUIEvent::TouchEvt(UnityEngine.Vector2)
extern "C"  void TouchPadUIEvent_TouchEvt_m3030614889 (TouchPadUIEvent_t997246418 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_touchText_3();
		String_t* L_1 = Vector2_ToString_m775491729((&___value0), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void TouchPadUIEvent::TouchUp()
extern "C"  void TouchPadUIEvent_TouchUp_m1433547427 (TouchPadUIEvent_t997246418 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchPadUIEvent_TouchUp_m1433547427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_touchUpText_4();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral1596708495);
		Text_t356221433 * L_1 = __this->get_touchUpText_4();
		Il2CppObject * L_2 = TouchPadUIEvent_ClearText_m1473369353(__this, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = __this->get_touchText_3();
		Il2CppObject * L_4 = TouchPadUIEvent_ClearText_m1473369353(__this, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator TouchPadUIEvent::ClearText(UnityEngine.UI.Text)
extern "C"  Il2CppObject * TouchPadUIEvent_ClearText_m1473369353 (TouchPadUIEvent_t997246418 * __this, Text_t356221433 * ___textToCLead0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchPadUIEvent_ClearText_m1473369353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CClearTextU3Ec__Iterator0_t3345759406 * V_0 = NULL;
	{
		U3CClearTextU3Ec__Iterator0_t3345759406 * L_0 = (U3CClearTextU3Ec__Iterator0_t3345759406 *)il2cpp_codegen_object_new(U3CClearTextU3Ec__Iterator0_t3345759406_il2cpp_TypeInfo_var);
		U3CClearTextU3Ec__Iterator0__ctor_m3571755835(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CClearTextU3Ec__Iterator0_t3345759406 * L_1 = V_0;
		Text_t356221433 * L_2 = ___textToCLead0;
		NullCheck(L_1);
		L_1->set_textToCLead_0(L_2);
		U3CClearTextU3Ec__Iterator0_t3345759406 * L_3 = V_0;
		return L_3;
	}
}
// System.Void TouchPadUIEvent/<ClearText>c__Iterator0::.ctor()
extern "C"  void U3CClearTextU3Ec__Iterator0__ctor_m3571755835 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean TouchPadUIEvent/<ClearText>c__Iterator0::MoveNext()
extern "C"  bool U3CClearTextU3Ec__Iterator0_MoveNext_m670795441 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CClearTextU3Ec__Iterator0_MoveNext_m670795441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0045;
		}
	}
	{
		goto IL_005c;
	}

IL_0021:
	{
		WaitForSeconds_t3839502067 * L_2 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_2, (0.3f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_2);
		bool L_3 = __this->get_U24disposing_2();
		if (L_3)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_0040:
	{
		goto IL_005e;
	}

IL_0045:
	{
		Text_t356221433 * L_4 = __this->get_textToCLead_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_5);
		__this->set_U24PC_3((-1));
	}

IL_005c:
	{
		return (bool)0;
	}

IL_005e:
	{
		return (bool)1;
	}
}
// System.Object TouchPadUIEvent/<ClearText>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CClearTextU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m544951081 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object TouchPadUIEvent/<ClearText>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CClearTextU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3862127489 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void TouchPadUIEvent/<ClearText>c__Iterator0::Dispose()
extern "C"  void U3CClearTextU3Ec__Iterator0_Dispose_m2167506168 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void TouchPadUIEvent/<ClearText>c__Iterator0::Reset()
extern "C"  void U3CClearTextU3Ec__Iterator0_Reset_m4055416822 (U3CClearTextU3Ec__Iterator0_t3345759406 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CClearTextU3Ec__Iterator0_Reset_m4055416822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void TrackMouseMovement::.ctor()
extern "C"  void TrackMouseMovement__ctor_m4255909330 (TrackMouseMovement_t1471877027 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TrackMouseMovement::Start()
extern "C"  void TrackMouseMovement_Start_m2591269326 (TrackMouseMovement_t1471877027 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TrackMouseMovement::Update()
extern "C"  void TrackMouseMovement_Update_m1904018219 (TrackMouseMovement_t1471877027 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TwistMe::.ctor()
extern "C"  void TwistMe__ctor_m3286002248 (TwistMe_t1887030447 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::OnEnable()
extern "C"  void TwistMe_OnEnable_m3712938068 (TwistMe_t1887030447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_OnEnable_m3712938068_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwistMe_On_TouchStart2Fingers_m200446251_MethodInfo_var);
		TouchStart2FingersHandler_t3096813332 * L_1 = (TouchStart2FingersHandler_t3096813332 *)il2cpp_codegen_object_new(TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m3302552411(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart2Fingers_m1024224148(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwistMe_On_Twist_m647447831_MethodInfo_var);
		TwistHandler_t117269532 * L_3 = (TwistHandler_t117269532 *)il2cpp_codegen_object_new(TwistHandler_t117269532_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m448382751(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_Twist_m3882482804(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwistMe_On_TwistEnd_m525922928_MethodInfo_var);
		TwistEndHandler_t1042340097 * L_5 = (TwistEndHandler_t1042340097 *)il2cpp_codegen_object_new(TwistEndHandler_t1042340097_il2cpp_TypeInfo_var);
		TwistEndHandler__ctor_m870850134(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_TwistEnd_m2544505780(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwistMe_On_Cancel2Fingers_m948298906_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_m646354580(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::OnDisable()
extern "C"  void TwistMe_OnDisable_m4097738355 (TwistMe_t1887030447 * __this, const MethodInfo* method)
{
	{
		TwistMe_UnsubscribeEvent_m1613131113(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::OnDestroy()
extern "C"  void TwistMe_OnDestroy_m2431016129 (TwistMe_t1887030447 * __this, const MethodInfo* method)
{
	{
		TwistMe_UnsubscribeEvent_m1613131113(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::UnsubscribeEvent()
extern "C"  void TwistMe_UnsubscribeEvent_m1613131113 (TwistMe_t1887030447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_UnsubscribeEvent_m1613131113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwistMe_On_TouchStart2Fingers_m200446251_MethodInfo_var);
		TouchStart2FingersHandler_t3096813332 * L_1 = (TouchStart2FingersHandler_t3096813332 *)il2cpp_codegen_object_new(TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m3302552411(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart2Fingers_m3416141907(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwistMe_On_Twist_m647447831_MethodInfo_var);
		TwistHandler_t117269532 * L_3 = (TwistHandler_t117269532 *)il2cpp_codegen_object_new(TwistHandler_t117269532_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m448382751(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_Twist_m1411584747(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwistMe_On_TwistEnd_m525922928_MethodInfo_var);
		TwistEndHandler_t1042340097 * L_5 = (TwistEndHandler_t1042340097 *)il2cpp_codegen_object_new(TwistEndHandler_t1042340097_il2cpp_TypeInfo_var);
		TwistEndHandler__ctor_m870850134(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_TwistEnd_m2089157299(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwistMe_On_Cancel2Fingers_m948298906_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m3690314195(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwistMe::Start()
extern "C"  void TwistMe_Start_m3006910900 (TwistMe_t1887030447 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_Start_m3006910900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506_MethodInfo_var);
		__this->set_textMesh_2(L_0);
		return;
	}
}
// System.Void TwistMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwistMe_On_TouchStart2Fingers_m200446251 (TwistMe_t1887030447 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_TouchStart2Fingers_m200446251_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		EasyTouch_SetEnableTwist_m199134308(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		EasyTouch_SetEnablePinch_m590621477(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void TwistMe::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwistMe_On_Twist_m647447831 (TwistMe_t1887030447 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_Twist_m647447831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005c;
		}
	}
	{
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		float L_6 = L_5->get_twistAngle_23();
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, (0.0f), (0.0f), L_6, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_Rotate_m1743927093(L_4, L_7, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_8 = __this->get_textMesh_2();
		Gesture_t367995397 * L_9 = ___gesture0;
		NullCheck(L_9);
		float* L_10 = L_9->get_address_of_twistAngle_23();
		String_t* L_11 = Single_ToString_m1813392066(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2303590499, L_11, /*hidden argument*/NULL);
		NullCheck(L_8);
		TextMesh_set_text_m3390063817(L_8, L_12, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void TwistMe::On_TwistEnd(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwistMe_On_TwistEnd_m525922928 (TwistMe_t1887030447 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_TwistEnd_m525922928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		EasyTouch_SetEnablePinch_m590621477(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_5 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_rotation_m3411284563(L_4, L_5, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_6 = __this->get_textMesh_2();
		NullCheck(L_6);
		TextMesh_set_text_m3390063817(L_6, _stringLiteral2313879957, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
// System.Void TwistMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwistMe_On_Cancel2Fingers_m948298906 (TwistMe_t1887030447 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwistMe_On_Cancel2Fingers_m948298906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		EasyTouch_SetEnablePinch_m590621477(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_rotation_m3411284563(L_0, L_1, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_2 = __this->get_textMesh_2();
		NullCheck(L_2);
		TextMesh_set_text_m3390063817(L_2, _stringLiteral2313879957, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::.ctor()
extern "C"  void TwoDoubleTapMe__ctor_m1402339187 (TwoDoubleTapMe_t376817258 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::OnEnable()
extern "C"  void TwoDoubleTapMe_OnEnable_m455871831 (TwoDoubleTapMe_t376817258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDoubleTapMe_OnEnable_m455871831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoDoubleTapMe_On_DoubleTap2Fingers_m426934483_MethodInfo_var);
		DoubleTap2FingersHandler_t313188403 * L_1 = (DoubleTap2FingersHandler_t313188403 *)il2cpp_codegen_object_new(DoubleTap2FingersHandler_t313188403_il2cpp_TypeInfo_var);
		DoubleTap2FingersHandler__ctor_m1622909298(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_DoubleTap2Fingers_m3474334234(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::OnDisable()
extern "C"  void TwoDoubleTapMe_OnDisable_m4073149666 (TwoDoubleTapMe_t376817258 * __this, const MethodInfo* method)
{
	{
		TwoDoubleTapMe_UnsubscribeEvent_m596587122(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::OnDestroy()
extern "C"  void TwoDoubleTapMe_OnDestroy_m3413542032 (TwoDoubleTapMe_t376817258 * __this, const MethodInfo* method)
{
	{
		TwoDoubleTapMe_UnsubscribeEvent_m596587122(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::UnsubscribeEvent()
extern "C"  void TwoDoubleTapMe_UnsubscribeEvent_m596587122 (TwoDoubleTapMe_t376817258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDoubleTapMe_UnsubscribeEvent_m596587122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoDoubleTapMe_On_DoubleTap2Fingers_m426934483_MethodInfo_var);
		DoubleTap2FingersHandler_t313188403 * L_1 = (DoubleTap2FingersHandler_t313188403 *)il2cpp_codegen_object_new(DoubleTap2FingersHandler_t313188403_il2cpp_TypeInfo_var);
		DoubleTap2FingersHandler__ctor_m1622909298(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_DoubleTap2Fingers_m4218316407(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDoubleTapMe::On_DoubleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoDoubleTapMe_On_DoubleTap2Fingers_m426934483 (TwoDoubleTapMe_t376817258 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDoubleTapMe_On_DoubleTap2Fingers_m426934483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_005d;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		float L_7 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_8 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_9 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m3811852957(&L_10, L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_10, /*hidden argument*/NULL);
	}

IL_005d:
	{
		return;
	}
}
// System.Void TwoDragMe::.ctor()
extern "C"  void TwoDragMe__ctor_m3855772151 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::OnEnable()
extern "C"  void TwoDragMe_OnEnable_m3878514699 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_OnEnable_m3878514699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoDragMe_On_DragStart2Fingers_m3319697183_MethodInfo_var);
		DragStart2FingersHandler_t1996108375 * L_1 = (DragStart2FingersHandler_t1996108375 *)il2cpp_codegen_object_new(DragStart2FingersHandler_t1996108375_il2cpp_TypeInfo_var);
		DragStart2FingersHandler__ctor_m3803132154(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_DragStart2Fingers_m1162086938(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoDragMe_On_Drag2Fingers_m195202675_MethodInfo_var);
		Drag2FingersHandler_t599932411 * L_3 = (Drag2FingersHandler_t599932411 *)il2cpp_codegen_object_new(Drag2FingersHandler_t599932411_il2cpp_TypeInfo_var);
		Drag2FingersHandler__ctor_m4221981472(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_Drag2Fingers_m2386181684(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoDragMe_On_DragEnd2Fingers_m967453298_MethodInfo_var);
		DragEnd2FingersHandler_t1069006048 * L_5 = (DragEnd2FingersHandler_t1069006048 *)il2cpp_codegen_object_new(DragEnd2FingersHandler_t1069006048_il2cpp_TypeInfo_var);
		DragEnd2FingersHandler__ctor_m2199226361(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_DragEnd2Fingers_m101490336(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwoDragMe_On_Cancel2Fingers_m2222755653_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_m646354580(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::OnDisable()
extern "C"  void TwoDragMe_OnDisable_m3298764226 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	{
		TwoDragMe_UnsubscribeEvent_m3871665702(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::OnDestroy()
extern "C"  void TwoDragMe_OnDestroy_m1282573940 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	{
		TwoDragMe_UnsubscribeEvent_m3871665702(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::UnsubscribeEvent()
extern "C"  void TwoDragMe_UnsubscribeEvent_m3871665702 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_UnsubscribeEvent_m3871665702_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoDragMe_On_DragStart2Fingers_m3319697183_MethodInfo_var);
		DragStart2FingersHandler_t1996108375 * L_1 = (DragStart2FingersHandler_t1996108375 *)il2cpp_codegen_object_new(DragStart2FingersHandler_t1996108375_il2cpp_TypeInfo_var);
		DragStart2FingersHandler__ctor_m3803132154(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_DragStart2Fingers_m86474871(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoDragMe_On_Drag2Fingers_m195202675_MethodInfo_var);
		Drag2FingersHandler_t599932411 * L_3 = (Drag2FingersHandler_t599932411 *)il2cpp_codegen_object_new(Drag2FingersHandler_t599932411_il2cpp_TypeInfo_var);
		Drag2FingersHandler__ctor_m4221981472(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_Drag2Fingers_m3828850227(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoDragMe_On_DragEnd2Fingers_m967453298_MethodInfo_var);
		DragEnd2FingersHandler_t1069006048 * L_5 = (DragEnd2FingersHandler_t1069006048 *)il2cpp_codegen_object_new(DragEnd2FingersHandler_t1069006048_il2cpp_TypeInfo_var);
		DragEnd2FingersHandler__ctor_m2199226361(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_DragEnd2Fingers_m1103201475(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwoDragMe_On_Cancel2Fingers_m2222755653_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m3690314195(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::Start()
extern "C"  void TwoDragMe_Start_m1968061035 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_Start_m1968061035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506_MethodInfo_var);
		__this->set_textMesh_2(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t257310565 * L_2 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_2);
		Material_t193706927 * L_3 = Renderer_get_material_m2553789785(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t2020392075  L_4 = Material_get_color_m668215843(L_3, /*hidden argument*/NULL);
		__this->set_startColor_4(L_4);
		return;
	}
}
// System.Void TwoDragMe::On_DragStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoDragMe_On_DragStart2Fingers_m3319697183 (TwoDragMe_t2032870650 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_On_DragStart2Fingers_m3319697183_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004a;
		}
	}
	{
		TwoDragMe_RandomColor_m2832589557(__this, /*hidden argument*/NULL);
		Gesture_t367995397 * L_4 = ___gesture0;
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = ((BaseFinger_t1402521766 *)L_5)->get_pickedObject_8();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_9 = Gesture_GetTouchToWorldPoint_m2105441024(L_4, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t2243707580  L_10 = V_0;
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t2243707580  L_12 = Transform_get_position_m1104419803(L_11, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		__this->set_deltaPosition_3(L_13);
	}

IL_004a:
	{
		return;
	}
}
// System.Void TwoDragMe::On_Drag2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoDragMe_On_Drag2Fingers_m195202675 (TwoDragMe_t2032870650 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_On_Drag2Fingers_m195202675_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_007d;
		}
	}
	{
		Gesture_t367995397 * L_4 = ___gesture0;
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = ((BaseFinger_t1402521766 *)L_5)->get_pickedObject_8();
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_9 = Gesture_GetTouchToWorldPoint_m2105441024(L_4, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = V_0;
		Vector3_t2243707580  L_12 = __this->get_deltaPosition_3();
		Vector3_t2243707580  L_13 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m2469242620(L_10, L_13, /*hidden argument*/NULL);
		Gesture_t367995397 * L_14 = ___gesture0;
		NullCheck(L_14);
		float L_15 = Gesture_GetSwipeOrDragAngle_m2164539425(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		TextMesh_t1641806576 * L_16 = __this->get_textMesh_2();
		String_t* L_17 = Single_ToString_m2359963436((&V_1), _stringLiteral3231012694, /*hidden argument*/NULL);
		Gesture_t367995397 * L_18 = ___gesture0;
		NullCheck(L_18);
		int32_t* L_19 = L_18->get_address_of_swipe_19();
		Il2CppObject * L_20 = Box(SwipeDirection_t2889416990_il2cpp_TypeInfo_var, L_19);
		NullCheck(L_20);
		String_t* L_21 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m612901809(NULL /*static, unused*/, L_17, _stringLiteral57471863, L_21, /*hidden argument*/NULL);
		NullCheck(L_16);
		TextMesh_set_text_m3390063817(L_16, L_22, /*hidden argument*/NULL);
	}

IL_007d:
	{
		return;
	}
}
// System.Void TwoDragMe::On_DragEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoDragMe_On_DragEnd2Fingers_m967453298 (TwoDragMe_t2032870650 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_On_DragEnd2Fingers_m967453298_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = __this->get_startColor_4();
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_8 = __this->get_textMesh_2();
		NullCheck(L_8);
		TextMesh_set_text_m3390063817(L_8, _stringLiteral4215660982, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void TwoDragMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoDragMe_On_Cancel2Fingers_m2222755653 (TwoDragMe_t2032870650 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		TwoDragMe_On_DragEnd2Fingers_m967453298(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoDragMe::RandomColor()
extern "C"  void TwoDragMe_RandomColor_m2832589557 (TwoDragMe_t2032870650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoDragMe_RandomColor_m2832589557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_1);
		Material_t193706927 * L_2 = Renderer_get_material_m2553789785(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m3811852957(&L_6, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m577844242(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::.ctor()
extern "C"  void TwoLongTapMe__ctor_m175446098 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::OnEnable()
extern "C"  void TwoLongTapMe_OnEnable_m1179355642 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_OnEnable_m1179355642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoLongTapMe_On_LongTapStart2Fingers_m930757295_MethodInfo_var);
		LongTapStart2FingersHandler_t59253952 * L_1 = (LongTapStart2FingersHandler_t59253952 *)il2cpp_codegen_object_new(LongTapStart2FingersHandler_t59253952_il2cpp_TypeInfo_var);
		LongTapStart2FingersHandler__ctor_m4029944885(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_LongTapStart2Fingers_m2902511508(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoLongTapMe_On_LongTap2Fingers_m2992644723_MethodInfo_var);
		LongTap2FingersHandler_t2742766376 * L_3 = (LongTap2FingersHandler_t2742766376 *)il2cpp_codegen_object_new(LongTap2FingersHandler_t2742766376_il2cpp_TypeInfo_var);
		LongTap2FingersHandler__ctor_m2664185749(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_LongTap2Fingers_m2932670112(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974_MethodInfo_var);
		LongTapEnd2FingersHandler_t3611206799 * L_5 = (LongTapEnd2FingersHandler_t3611206799 *)il2cpp_codegen_object_new(LongTapEnd2FingersHandler_t3611206799_il2cpp_TypeInfo_var);
		LongTapEnd2FingersHandler__ctor_m2802920322(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_LongTapEnd2Fingers_m228344724(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwoLongTapMe_On_Cancel2Fingers_m2591473512_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_m646354580(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::OnDisable()
extern "C"  void TwoLongTapMe_OnDisable_m3985797501 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	{
		TwoLongTapMe_UnsubscribeEvent_m2447274439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::OnDestroy()
extern "C"  void TwoLongTapMe_OnDestroy_m350471399 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	{
		TwoLongTapMe_UnsubscribeEvent_m2447274439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::UnsubscribeEvent()
extern "C"  void TwoLongTapMe_UnsubscribeEvent_m2447274439 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_UnsubscribeEvent_m2447274439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoLongTapMe_On_LongTapStart2Fingers_m930757295_MethodInfo_var);
		LongTapStart2FingersHandler_t59253952 * L_1 = (LongTapStart2FingersHandler_t59253952 *)il2cpp_codegen_object_new(LongTapStart2FingersHandler_t59253952_il2cpp_TypeInfo_var);
		LongTapStart2FingersHandler__ctor_m4029944885(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_LongTapStart2Fingers_m4268548115(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoLongTapMe_On_LongTap2Fingers_m2992644723_MethodInfo_var);
		LongTap2FingersHandler_t2742766376 * L_3 = (LongTap2FingersHandler_t2742766376 *)il2cpp_codegen_object_new(LongTap2FingersHandler_t2742766376_il2cpp_TypeInfo_var);
		LongTap2FingersHandler__ctor_m2664185749(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_LongTap2Fingers_m1992347267(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974_MethodInfo_var);
		LongTapEnd2FingersHandler_t3611206799 * L_5 = (LongTapEnd2FingersHandler_t3611206799 *)il2cpp_codegen_object_new(LongTapEnd2FingersHandler_t3611206799_il2cpp_TypeInfo_var);
		LongTapEnd2FingersHandler__ctor_m2802920322(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_LongTapEnd2Fingers_m3457310035(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwoLongTapMe_On_Cancel2Fingers_m2591473512_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m3690314195(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::Start()
extern "C"  void TwoLongTapMe_Start_m3819980634 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_Start_m3819980634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506_MethodInfo_var);
		__this->set_textMesh_2(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t257310565 * L_2 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_2);
		Material_t193706927 * L_3 = Renderer_get_material_m2553789785(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t2020392075  L_4 = Material_get_color_m668215843(L_3, /*hidden argument*/NULL);
		__this->set_startColor_3(L_4);
		return;
	}
}
// System.Void TwoLongTapMe::On_LongTapStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoLongTapMe_On_LongTapStart2Fingers_m930757295 (TwoLongTapMe_t2356062841 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_On_LongTapStart2Fingers_m930757295_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		TwoLongTapMe_RandomColor_m312821092(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void TwoLongTapMe::On_LongTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoLongTapMe_On_LongTap2Fingers_m2992644723 (TwoLongTapMe_t2356062841 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_On_LongTap2Fingers_m2992644723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		TextMesh_t1641806576 * L_4 = __this->get_textMesh_2();
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		float* L_6 = ((BaseFinger_t1402521766 *)L_5)->get_address_of_actionTime_5();
		String_t* L_7 = Single_ToString_m2359963436(L_6, _stringLiteral3231012694, /*hidden argument*/NULL);
		NullCheck(L_4);
		TextMesh_set_text_m3390063817(L_4, L_7, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void TwoLongTapMe::On_LongTapEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974 (TwoLongTapMe_t2356062841 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = __this->get_startColor_3();
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_8 = __this->get_textMesh_2();
		NullCheck(L_8);
		TextMesh_set_text_m3390063817(L_8, _stringLiteral1375686743, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void TwoLongTapMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoLongTapMe_On_Cancel2Fingers_m2591473512 (TwoLongTapMe_t2356062841 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		TwoLongTapMe_On_LongTapEnd2Fingers_m1851680974(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoLongTapMe::RandomColor()
extern "C"  void TwoLongTapMe_RandomColor_m312821092 (TwoLongTapMe_t2356062841 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoLongTapMe_RandomColor_m312821092_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_1);
		Material_t193706927 * L_2 = Renderer_get_material_m2553789785(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m3811852957(&L_6, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m577844242(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::.ctor()
extern "C"  void TwoSwipe__ctor_m3950690447 (TwoSwipe_t373754724 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::OnEnable()
extern "C"  void TwoSwipe_OnEnable_m2048578019 (TwoSwipe_t373754724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_OnEnable_m2048578019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoSwipe_On_SwipeStart2Fingers_m2895296955_MethodInfo_var);
		SwipeStart2FingersHandler_t3085603867 * L_1 = (SwipeStart2FingersHandler_t3085603867 *)il2cpp_codegen_object_new(SwipeStart2FingersHandler_t3085603867_il2cpp_TypeInfo_var);
		SwipeStart2FingersHandler__ctor_m647530272(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_SwipeStart2Fingers_m4057703828(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoSwipe_On_Swipe2Fingers_m2117361735_MethodInfo_var);
		Swipe2FingersHandler_t2386328279 * L_3 = (Swipe2FingersHandler_t2386328279 *)il2cpp_codegen_object_new(Swipe2FingersHandler_t2386328279_il2cpp_TypeInfo_var);
		Swipe2FingersHandler__ctor_m1389004394(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_Swipe2Fingers_m2192986026(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoSwipe_On_SwipeEnd2Fingers_m1793330128_MethodInfo_var);
		SwipeEnd2FingersHandler_t2863096110 * L_5 = (SwipeEnd2FingersHandler_t2863096110 *)il2cpp_codegen_object_new(SwipeEnd2FingersHandler_t2863096110_il2cpp_TypeInfo_var);
		SwipeEnd2FingersHandler__ctor_m3456079153(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_SwipeEnd2Fingers_m1320382740(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::OnDisable()
extern "C"  void TwoSwipe_OnDisable_m1244840976 (TwoSwipe_t373754724 * __this, const MethodInfo* method)
{
	{
		TwoSwipe_UnsubscribeEvent_m369687584(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::OnDestroy()
extern "C"  void TwoSwipe_OnDestroy_m3355297642 (TwoSwipe_t373754724 * __this, const MethodInfo* method)
{
	{
		TwoSwipe_UnsubscribeEvent_m369687584(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::UnsubscribeEvent()
extern "C"  void TwoSwipe_UnsubscribeEvent_m369687584 (TwoSwipe_t373754724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_UnsubscribeEvent_m369687584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoSwipe_On_SwipeStart2Fingers_m2895296955_MethodInfo_var);
		SwipeStart2FingersHandler_t3085603867 * L_1 = (SwipeStart2FingersHandler_t3085603867 *)il2cpp_codegen_object_new(SwipeStart2FingersHandler_t3085603867_il2cpp_TypeInfo_var);
		SwipeStart2FingersHandler__ctor_m647530272(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_SwipeStart2Fingers_m804622931(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoSwipe_On_Swipe2Fingers_m2117361735_MethodInfo_var);
		Swipe2FingersHandler_t2386328279 * L_3 = (Swipe2FingersHandler_t2386328279 *)il2cpp_codegen_object_new(Swipe2FingersHandler_t2386328279_il2cpp_TypeInfo_var);
		Swipe2FingersHandler__ctor_m1389004394(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_Swipe2Fingers_m3559413287(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoSwipe_On_SwipeEnd2Fingers_m1793330128_MethodInfo_var);
		SwipeEnd2FingersHandler_t2863096110 * L_5 = (SwipeEnd2FingersHandler_t2863096110 *)il2cpp_codegen_object_new(SwipeEnd2FingersHandler_t2863096110_il2cpp_TypeInfo_var);
		SwipeEnd2FingersHandler__ctor_m3456079153(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_SwipeEnd2Fingers_m635268243(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::On_SwipeStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoSwipe_On_SwipeStart2Fingers_m2895296955 (TwoSwipe_t373754724 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_On_SwipeStart2Fingers_m2895296955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_swipeData_3();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral1754277768);
		return;
	}
}
// System.Void TwoSwipe::On_Swipe2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoSwipe_On_Swipe2Fingers_m2117361735 (TwoSwipe_t373754724 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Gesture_GetTouchToWorldPoint_m3593534480(L_0, (5.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = __this->get_trail_2();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m909382139(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = V_0;
		NullCheck(L_3);
		Transform_set_position_m2469242620(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoSwipe::On_SwipeEnd2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoSwipe_On_SwipeEnd2Fingers_m1793330128 (TwoSwipe_t373754724 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoSwipe_On_SwipeEnd2Fingers_m1793330128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		float L_1 = Gesture_GetSwipeOrDragAngle_m2164539425(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Text_t356221433 * L_2 = __this->get_swipeData_3();
		ObjectU5BU5D_t3614634134* L_3 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral2694093798);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2694093798);
		ObjectU5BU5D_t3614634134* L_4 = L_3;
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		int32_t* L_6 = L_5->get_address_of_swipe_19();
		Il2CppObject * L_7 = Box(SwipeDirection_t2889416990_il2cpp_TypeInfo_var, L_6);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_7);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_8);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_8);
		ObjectU5BU5D_t3614634134* L_9 = L_4;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral3871193470);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3871193470);
		ObjectU5BU5D_t3614634134* L_10 = L_9;
		Gesture_t367995397 * L_11 = ___gesture0;
		NullCheck(L_11);
		Vector2_t2243707579 * L_12 = L_11->get_address_of_swipeVector_21();
		Vector2_t2243707579  L_13 = Vector2_get_normalized_m2985402409(L_12, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_15);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		ObjectU5BU5D_t3614634134* L_16 = L_10;
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, _stringLiteral1868748486);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral1868748486);
		ObjectU5BU5D_t3614634134* L_17 = L_16;
		String_t* L_18 = Single_ToString_m2359963436((&V_0), _stringLiteral3231012694, /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m3881798623(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_19);
		return;
	}
}
// System.Void TwoTapMe::.ctor()
extern "C"  void TwoTapMe__ctor_m3357163812 (TwoTapMe_t3705696635 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::OnEnable()
extern "C"  void TwoTapMe_OnEnable_m3137535336 (TwoTapMe_t3705696635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_OnEnable_m3137535336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoTapMe_On_SimpleTap2Fingers_m1985585293_MethodInfo_var);
		SimpleTap2FingersHandler_t3725456324 * L_1 = (SimpleTap2FingersHandler_t3725456324 *)il2cpp_codegen_object_new(SimpleTap2FingersHandler_t3725456324_il2cpp_TypeInfo_var);
		SimpleTap2FingersHandler__ctor_m1866037649(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_SimpleTap2Fingers_m2286937328(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::OnDisable()
extern "C"  void TwoTapMe_OnDisable_m1726855483 (TwoTapMe_t3705696635 * __this, const MethodInfo* method)
{
	{
		TwoTapMe_UnsubscribeEvent_m4100987849(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::OnDestroy()
extern "C"  void TwoTapMe_OnDestroy_m60898345 (TwoTapMe_t3705696635 * __this, const MethodInfo* method)
{
	{
		TwoTapMe_UnsubscribeEvent_m4100987849(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::UnsubscribeEvent()
extern "C"  void TwoTapMe_UnsubscribeEvent_m4100987849 (TwoTapMe_t3705696635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_UnsubscribeEvent_m4100987849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoTapMe_On_SimpleTap2Fingers_m1985585293_MethodInfo_var);
		SimpleTap2FingersHandler_t3725456324 * L_1 = (SimpleTap2FingersHandler_t3725456324 *)il2cpp_codegen_object_new(SimpleTap2FingersHandler_t3725456324_il2cpp_TypeInfo_var);
		SimpleTap2FingersHandler__ctor_m1866037649(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_SimpleTap2Fingers_m3825049051(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTapMe::On_SimpleTap2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoTapMe_On_SimpleTap2Fingers_m1985585293 (TwoTapMe_t3705696635 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_On_SimpleTap2Fingers_m1985585293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		TwoTapMe_RandomColor_m3901042166(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void TwoTapMe::RandomColor()
extern "C"  void TwoTapMe_RandomColor_m3901042166 (TwoTapMe_t3705696635 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTapMe_RandomColor_m3901042166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_1);
		Material_t193706927 * L_2 = Renderer_get_material_m2553789785(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m3811852957(&L_6, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m577844242(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::.ctor()
extern "C"  void TwoTouchMe__ctor_m806103806 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::OnEnable()
extern "C"  void TwoTouchMe_OnEnable_m3939713802 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_OnEnable_m3939713802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoTouchMe_On_TouchStart2Fingers_m583160655_MethodInfo_var);
		TouchStart2FingersHandler_t3096813332 * L_1 = (TouchStart2FingersHandler_t3096813332 *)il2cpp_codegen_object_new(TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m3302552411(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart2Fingers_m1024224148(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoTouchMe_On_TouchDown2Fingers_m4170579177_MethodInfo_var);
		TouchDown2FingersHandler_t3403539080 * L_3 = (TouchDown2FingersHandler_t3403539080 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t3403539080_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m1028756337(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown2Fingers_m1064519056(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoTouchMe_On_TouchUp2Fingers_m286272622_MethodInfo_var);
		TouchUp2FingersHandler_t384986787 * L_5 = (TouchUp2FingersHandler_t384986787 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_t384986787_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m1257200738(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp2Fingers_m1044638522(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwoTouchMe_On_Cancel2Fingers_m936211736_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_add_On_Cancel2Fingers_m646354580(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::OnDisable()
extern "C"  void TwoTouchMe_OnDisable_m758837751 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	{
		TwoTouchMe_UnsubscribeEvent_m3432234013(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::OnDestroy()
extern "C"  void TwoTouchMe_OnDestroy_m4245732989 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	{
		TwoTouchMe_UnsubscribeEvent_m3432234013(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::UnsubscribeEvent()
extern "C"  void TwoTouchMe_UnsubscribeEvent_m3432234013 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_UnsubscribeEvent_m3432234013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)TwoTouchMe_On_TouchStart2Fingers_m583160655_MethodInfo_var);
		TouchStart2FingersHandler_t3096813332 * L_1 = (TouchStart2FingersHandler_t3096813332 *)il2cpp_codegen_object_new(TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m3302552411(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart2Fingers_m3416141907(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)TwoTouchMe_On_TouchDown2Fingers_m4170579177_MethodInfo_var);
		TouchDown2FingersHandler_t3403539080 * L_3 = (TouchDown2FingersHandler_t3403539080 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t3403539080_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m1028756337(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown2Fingers_m966509139(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)TwoTouchMe_On_TouchUp2Fingers_m286272622_MethodInfo_var);
		TouchUp2FingersHandler_t384986787 * L_5 = (TouchUp2FingersHandler_t384986787 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_t384986787_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m1257200738(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp2Fingers_m2322502551(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)TwoTouchMe_On_Cancel2Fingers_m936211736_MethodInfo_var);
		Cancel2FingersHandler_t2820220009 * L_7 = (Cancel2FingersHandler_t2820220009 *)il2cpp_codegen_object_new(Cancel2FingersHandler_t2820220009_il2cpp_TypeInfo_var);
		Cancel2FingersHandler__ctor_m933193436(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_remove_On_Cancel2Fingers_m3690314195(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::Start()
extern "C"  void TwoTouchMe_Start_m1530771530 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_Start_m1530771530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TextMesh_t1641806576 * L_0 = Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506(__this, /*hidden argument*/Component_GetComponentInChildren_TisTextMesh_t1641806576_m1078439506_MethodInfo_var);
		__this->set_textMesh_2(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Renderer_t257310565 * L_2 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_2);
		Material_t193706927 * L_3 = Renderer_get_material_m2553789785(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t2020392075  L_4 = Material_get_color_m668215843(L_3, /*hidden argument*/NULL);
		__this->set_startColor_3(L_4);
		return;
	}
}
// System.Void TwoTouchMe::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoTouchMe_On_TouchStart2Fingers_m583160655 (TwoTouchMe_t4294184479 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_On_TouchStart2Fingers_m583160655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		TwoTouchMe_RandomColor_m3906756720(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void TwoTouchMe::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoTouchMe_On_TouchDown2Fingers_m4170579177 (TwoTouchMe_t4294184479 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_On_TouchDown2Fingers_m4170579177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		TextMesh_t1641806576 * L_4 = __this->get_textMesh_2();
		Gesture_t367995397 * L_5 = ___gesture0;
		NullCheck(L_5);
		float* L_6 = ((BaseFinger_t1402521766 *)L_5)->get_address_of_actionTime_5();
		String_t* L_7 = Single_ToString_m2359963436(L_6, _stringLiteral3231012694, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral587809818, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		TextMesh_set_text_m3390063817(L_4, L_8, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void TwoTouchMe::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoTouchMe_On_TouchUp2Fingers_m286272622 (TwoTouchMe_t4294184479 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_On_TouchUp2Fingers_m286272622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = ((BaseFinger_t1402521766 *)L_0)->get_pickedObject_8();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Renderer_t257310565 * L_5 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_5);
		Material_t193706927 * L_6 = Renderer_get_material_m2553789785(L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_7 = __this->get_startColor_3();
		NullCheck(L_6);
		Material_set_color_m577844242(L_6, L_7, /*hidden argument*/NULL);
		TextMesh_t1641806576 * L_8 = __this->get_textMesh_2();
		NullCheck(L_8);
		TextMesh_set_text_m3390063817(L_8, _stringLiteral2386153277, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void TwoTouchMe::On_Cancel2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void TwoTouchMe_On_Cancel2Fingers_m936211736 (TwoTouchMe_t4294184479 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		TwoTouchMe_On_TouchUp2Fingers_m286272622(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TwoTouchMe::RandomColor()
extern "C"  void TwoTouchMe_RandomColor_m3906756720 (TwoTouchMe_t4294184479 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TwoTouchMe_RandomColor_m3906756720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_t257310565 * L_1 = GameObject_GetComponent_TisRenderer_t257310565_m226639069(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t257310565_m226639069_MethodInfo_var);
		NullCheck(L_1);
		Material_t193706927 * L_2 = Renderer_get_material_m2553789785(L_1, /*hidden argument*/NULL);
		float L_3 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_4 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_5 = Random_Range_m2884721203(NULL /*static, unused*/, (0.0f), (1.0f), /*hidden argument*/NULL);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m3811852957(&L_6, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_color_m577844242(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UICompatibility::.ctor()
extern "C"  void UICompatibility__ctor_m3558450677 (UICompatibility_t2032272458 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UICompatibility::SetCompatibility(System.Boolean)
extern "C"  void UICompatibility_SetCompatibility_m2855251400 (UICompatibility_t2032272458 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		EasyTouch_SetUICompatibily_m3534348079(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDrag::.ctor()
extern "C"  void UIDrag__ctor_m273814159 (UIDrag_t854096796 * __this, const MethodInfo* method)
{
	{
		__this->set_fingerId_2((-1));
		__this->set_drag_3((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDrag::OnEnable()
extern "C"  void UIDrag_OnEnable_m2279416155 (UIDrag_t854096796 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_OnEnable_m2279416155_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UIDrag_On_TouchDown_m3549100664_MethodInfo_var);
		TouchDownHandler_t16499120 * L_1 = (TouchDownHandler_t16499120 *)il2cpp_codegen_object_new(TouchDownHandler_t16499120_il2cpp_TypeInfo_var);
		TouchDownHandler__ctor_m929731709(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown_m625431120(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UIDrag_On_TouchStart_m3437169910_MethodInfo_var);
		TouchStartHandler_t3045666224 * L_3 = (TouchStartHandler_t3045666224 *)il2cpp_codegen_object_new(TouchStartHandler_t3045666224_il2cpp_TypeInfo_var);
		TouchStartHandler__ctor_m37798995(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart_m2583984596(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)UIDrag_On_TouchUp_m2370286479_MethodInfo_var);
		TouchUpHandler_t3694099103 * L_5 = (TouchUpHandler_t3694099103 *)il2cpp_codegen_object_new(TouchUpHandler_t3694099103_il2cpp_TypeInfo_var);
		TouchUpHandler__ctor_m1021741058(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp_m3031495162(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UIDrag_On_TouchStart2Fingers_m4065818318_MethodInfo_var);
		TouchStart2FingersHandler_t3096813332 * L_7 = (TouchStart2FingersHandler_t3096813332 *)il2cpp_codegen_object_new(TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m3302552411(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchStart2Fingers_m1024224148(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)UIDrag_On_TouchDown2Fingers_m2110802572_MethodInfo_var);
		TouchDown2FingersHandler_t3403539080 * L_9 = (TouchDown2FingersHandler_t3403539080 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t3403539080_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m1028756337(L_9, __this, L_8, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchDown2Fingers_m1064519056(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)UIDrag_On_TouchUp2Fingers_m2775525951_MethodInfo_var);
		TouchUp2FingersHandler_t384986787 * L_11 = (TouchUp2FingersHandler_t384986787 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_t384986787_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m1257200738(L_11, __this, L_10, /*hidden argument*/NULL);
		EasyTouch_add_On_TouchUp2Fingers_m1044638522(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDrag::OnDestroy()
extern "C"  void UIDrag_OnDestroy_m135766882 (UIDrag_t854096796 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_OnDestroy_m135766882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UIDrag_On_TouchDown_m3549100664_MethodInfo_var);
		TouchDownHandler_t16499120 * L_1 = (TouchDownHandler_t16499120 *)il2cpp_codegen_object_new(TouchDownHandler_t16499120_il2cpp_TypeInfo_var);
		TouchDownHandler__ctor_m929731709(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown_m1427941651(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UIDrag_On_TouchStart_m3437169910_MethodInfo_var);
		TouchStartHandler_t3045666224 * L_3 = (TouchStartHandler_t3045666224 *)il2cpp_codegen_object_new(TouchStartHandler_t3045666224_il2cpp_TypeInfo_var);
		TouchStartHandler__ctor_m37798995(L_3, __this, L_2, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart_m1488478227(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)UIDrag_On_TouchUp_m2370286479_MethodInfo_var);
		TouchUpHandler_t3694099103 * L_5 = (TouchUpHandler_t3694099103 *)il2cpp_codegen_object_new(TouchUpHandler_t3694099103_il2cpp_TypeInfo_var);
		TouchUpHandler__ctor_m1021741058(L_5, __this, L_4, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp_m1724410775(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)UIDrag_On_TouchStart2Fingers_m4065818318_MethodInfo_var);
		TouchStart2FingersHandler_t3096813332 * L_7 = (TouchStart2FingersHandler_t3096813332 *)il2cpp_codegen_object_new(TouchStart2FingersHandler_t3096813332_il2cpp_TypeInfo_var);
		TouchStart2FingersHandler__ctor_m3302552411(L_7, __this, L_6, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchStart2Fingers_m3416141907(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)UIDrag_On_TouchDown2Fingers_m2110802572_MethodInfo_var);
		TouchDown2FingersHandler_t3403539080 * L_9 = (TouchDown2FingersHandler_t3403539080 *)il2cpp_codegen_object_new(TouchDown2FingersHandler_t3403539080_il2cpp_TypeInfo_var);
		TouchDown2FingersHandler__ctor_m1028756337(L_9, __this, L_8, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchDown2Fingers_m966509139(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)UIDrag_On_TouchUp2Fingers_m2775525951_MethodInfo_var);
		TouchUp2FingersHandler_t384986787 * L_11 = (TouchUp2FingersHandler_t384986787 *)il2cpp_codegen_object_new(TouchUp2FingersHandler_t384986787_il2cpp_TypeInfo_var);
		TouchUp2FingersHandler__ctor_m1257200738(L_11, __this, L_10, /*hidden argument*/NULL);
		EasyTouch_remove_On_TouchUp2Fingers_m2322502551(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIDrag::On_TouchStart(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIDrag_On_TouchStart_m3437169910 (UIDrag_t854096796 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchStart_m3437169910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t1402521766 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_006a;
		}
	}
	{
		bool L_2 = __this->get_drag_3();
		if (!L_2)
		{
			goto IL_006a;
		}
	}
	{
		Gesture_t367995397 * L_3 = ___gesture0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = ((BaseFinger_t1402521766 *)L_3)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0047;
		}
	}
	{
		Gesture_t367995397 * L_7 = ___gesture0;
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = ((BaseFinger_t1402521766 *)L_7)->get_pickedUIElement_11();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_11 = Transform_IsChildOf_m10844547(L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006a;
		}
	}

IL_0047:
	{
		int32_t L_12 = __this->get_fingerId_2();
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_006a;
		}
	}
	{
		Gesture_t367995397 * L_13 = ___gesture0;
		NullCheck(L_13);
		int32_t L_14 = ((BaseFinger_t1402521766 *)L_13)->get_fingerIndex_0();
		__this->set_fingerId_2(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_SetAsLastSibling_m1528402907(L_15, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchDown(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIDrag_On_TouchDown_m3549100664 (UIDrag_t854096796 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchDown_m3549100664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_fingerId_2();
		Gesture_t367995397 * L_1 = ___gesture0;
		NullCheck(L_1);
		int32_t L_2 = ((BaseFinger_t1402521766 *)L_1)->get_fingerIndex_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0079;
		}
	}
	{
		bool L_3 = __this->get_drag_3();
		if (!L_3)
		{
			goto IL_0079;
		}
	}
	{
		Gesture_t367995397 * L_4 = ___gesture0;
		NullCheck(L_4);
		bool L_5 = ((BaseFinger_t1402521766 *)L_4)->get_isOverGui_10();
		if (!L_5)
		{
			goto IL_0079;
		}
	}
	{
		Gesture_t367995397 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = ((BaseFinger_t1402521766 *)L_6)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0058;
		}
	}
	{
		Gesture_t367995397 * L_10 = ___gesture0;
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = ((BaseFinger_t1402521766 *)L_10)->get_pickedUIElement_11();
		NullCheck(L_11);
		Transform_t3275118058 * L_12 = GameObject_get_transform_m909382139(L_11, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		bool L_14 = Transform_IsChildOf_m10844547(L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0079;
		}
	}

IL_0058:
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = L_15;
		NullCheck(L_16);
		Vector3_t2243707580  L_17 = Transform_get_position_m1104419803(L_16, /*hidden argument*/NULL);
		Gesture_t367995397 * L_18 = ___gesture0;
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = ((BaseFinger_t1402521766 *)L_18)->get_deltaPosition_4();
		Vector3_t2243707580  L_20 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_17, L_20, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_position_m2469242620(L_16, L_21, /*hidden argument*/NULL);
	}

IL_0079:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchUp(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIDrag_On_TouchUp_m2370286479 (UIDrag_t854096796 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_fingerId_2();
		Gesture_t367995397 * L_1 = ___gesture0;
		NullCheck(L_1);
		int32_t L_2 = ((BaseFinger_t1402521766 *)L_1)->get_fingerIndex_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0018;
		}
	}
	{
		__this->set_fingerId_2((-1));
	}

IL_0018:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchStart2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIDrag_On_TouchStart2Fingers_m4065818318 (UIDrag_t854096796 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchStart2Fingers_m4065818318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t1402521766 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_005e;
		}
	}
	{
		bool L_2 = __this->get_drag_3();
		if (!L_2)
		{
			goto IL_005e;
		}
	}
	{
		Gesture_t367995397 * L_3 = ___gesture0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = ((BaseFinger_t1402521766 *)L_3)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0047;
		}
	}
	{
		Gesture_t367995397 * L_7 = ___gesture0;
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = ((BaseFinger_t1402521766 *)L_7)->get_pickedUIElement_11();
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_11 = Transform_IsChildOf_m10844547(L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_005e;
		}
	}

IL_0047:
	{
		int32_t L_12 = __this->get_fingerId_2();
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_005e;
		}
	}
	{
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_SetAsLastSibling_m1528402907(L_13, /*hidden argument*/NULL);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchDown2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIDrag_On_TouchDown2Fingers_m2110802572 (UIDrag_t854096796 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchDown2Fingers_m2110802572_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t1402521766 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_0095;
		}
	}
	{
		Gesture_t367995397 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = ((BaseFinger_t1402521766 *)L_2)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003c;
		}
	}
	{
		Gesture_t367995397 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = ((BaseFinger_t1402521766 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_m10844547(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0095;
		}
	}

IL_003c:
	{
		Gesture_t367995397 * L_11 = ___gesture0;
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = ((BaseFinger_t1402521766 *)L_11)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006d;
		}
	}
	{
		Gesture_t367995397 * L_15 = ___gesture0;
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = ((BaseFinger_t1402521766 *)L_15)->get_pickedUIElement_11();
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = GameObject_get_transform_m909382139(L_16, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		bool L_19 = Transform_IsChildOf_m10844547(L_17, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_008e;
		}
	}

IL_006d:
	{
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = L_20;
		NullCheck(L_21);
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		Gesture_t367995397 * L_23 = ___gesture0;
		NullCheck(L_23);
		Vector2_t2243707579  L_24 = ((BaseFinger_t1402521766 *)L_23)->get_deltaPosition_4();
		Vector3_t2243707580  L_25 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Vector3_t2243707580  L_26 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_22, L_25, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_position_m2469242620(L_21, L_26, /*hidden argument*/NULL);
	}

IL_008e:
	{
		__this->set_drag_3((bool)0);
	}

IL_0095:
	{
		return;
	}
}
// System.Void UIDrag::On_TouchUp2Fingers(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIDrag_On_TouchUp2Fingers_m2775525951 (UIDrag_t854096796 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIDrag_On_TouchUp2Fingers_m2775525951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t1402521766 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		Gesture_t367995397 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = ((BaseFinger_t1402521766 *)L_2)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003c;
		}
	}
	{
		Gesture_t367995397 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = ((BaseFinger_t1402521766 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_m10844547(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0043;
		}
	}

IL_003c:
	{
		__this->set_drag_3((bool)1);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UIPinch::.ctor()
extern "C"  void UIPinch__ctor_m306113219 (UIPinch_t1472354382 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPinch::OnEnable()
extern "C"  void UIPinch_OnEnable_m1416134943 (UIPinch_t1472354382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPinch_OnEnable_m1416134943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UIPinch_On_Pinch_m1190717819_MethodInfo_var);
		PinchHandler_t3939026435 * L_1 = (PinchHandler_t3939026435 *)il2cpp_codegen_object_new(PinchHandler_t3939026435_il2cpp_TypeInfo_var);
		PinchHandler__ctor_m194359596(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_Pinch_m4248559610(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPinch::OnDestroy()
extern "C"  void UIPinch_OnDestroy_m744302208 (UIPinch_t1472354382 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPinch_OnDestroy_m744302208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UIPinch_On_Pinch_m1190717819_MethodInfo_var);
		PinchHandler_t3939026435 * L_1 = (PinchHandler_t3939026435 *)il2cpp_codegen_object_new(PinchHandler_t3939026435_il2cpp_TypeInfo_var);
		PinchHandler__ctor_m194359596(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_Pinch_m171636591(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIPinch::On_Pinch(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UIPinch_On_Pinch_m1190717819 (UIPinch_t1472354382 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIPinch_On_Pinch_m1190717819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t1402521766 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_009f;
		}
	}
	{
		Gesture_t367995397 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = ((BaseFinger_t1402521766 *)L_2)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003c;
		}
	}
	{
		Gesture_t367995397 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = ((BaseFinger_t1402521766 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_m10844547(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_009f;
		}
	}

IL_003c:
	{
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_localScale_m3074381503(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		float L_14 = (&V_0)->get_x_1();
		Gesture_t367995397 * L_15 = ___gesture0;
		NullCheck(L_15);
		float L_16 = L_15->get_deltaPinch_22();
		float L_17 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t2243707580  L_19 = Transform_get_localScale_m3074381503(L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		float L_20 = (&V_1)->get_y_2();
		Gesture_t367995397 * L_21 = ___gesture0;
		NullCheck(L_21);
		float L_22 = L_21->get_deltaPinch_22();
		float L_23 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_localScale_m3074381503(L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = (&V_2)->get_z_3();
		Vector3_t2243707580  L_27;
		memset(&L_27, 0, sizeof(L_27));
		Vector3__ctor_m2638739322(&L_27, ((float)((float)L_14+(float)((float)((float)L_16*(float)L_17)))), ((float)((float)L_20+(float)((float)((float)L_22*(float)L_23)))), L_26, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m2325460848(L_11, L_27, /*hidden argument*/NULL);
	}

IL_009f:
	{
		return;
	}
}
// System.Void UITwist::.ctor()
extern "C"  void UITwist__ctor_m3450545740 (UITwist_t430776843 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITwist::OnEnable()
extern "C"  void UITwist_OnEnable_m943932592 (UITwist_t430776843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITwist_OnEnable_m943932592_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UITwist_On_Twist_m2372298779_MethodInfo_var);
		TwistHandler_t117269532 * L_1 = (TwistHandler_t117269532 *)il2cpp_codegen_object_new(TwistHandler_t117269532_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m448382751(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_add_On_Twist_m3882482804(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITwist::OnDestroy()
extern "C"  void UITwist_OnDestroy_m3205865637 (UITwist_t430776843 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITwist_OnDestroy_m3205865637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)UITwist_On_Twist_m2372298779_MethodInfo_var);
		TwistHandler_t117269532 * L_1 = (TwistHandler_t117269532 *)il2cpp_codegen_object_new(TwistHandler_t117269532_il2cpp_TypeInfo_var);
		TwistHandler__ctor_m448382751(L_1, __this, L_0, /*hidden argument*/NULL);
		EasyTouch_remove_On_Twist_m1411584747(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITwist::On_Twist(HedgehogTeam.EasyTouch.Gesture)
extern "C"  void UITwist_On_Twist_m2372298779 (UITwist_t430776843 * __this, Gesture_t367995397 * ___gesture0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UITwist_On_Twist_m2372298779_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Gesture_t367995397 * L_0 = ___gesture0;
		NullCheck(L_0);
		bool L_1 = ((BaseFinger_t1402521766 *)L_0)->get_isOverGui_10();
		if (!L_1)
		{
			goto IL_005c;
		}
	}
	{
		Gesture_t367995397 * L_2 = ___gesture0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = ((BaseFinger_t1402521766 *)L_2)->get_pickedUIElement_11();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003c;
		}
	}
	{
		Gesture_t367995397 * L_6 = ___gesture0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = ((BaseFinger_t1402521766 *)L_6)->get_pickedUIElement_11();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_10 = Transform_IsChildOf_m10844547(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005c;
		}
	}

IL_003c:
	{
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Gesture_t367995397 * L_12 = ___gesture0;
		NullCheck(L_12);
		float L_13 = L_12->get_twistAngle_23();
		Vector3_t2243707580  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m2638739322(&L_14, (0.0f), (0.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_Rotate_m1743927093(L_11, L_14, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void UIWindow::.ctor()
extern "C"  void UIWindow__ctor_m143397439 (UIWindow_t2837557726 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWindow::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void UIWindow_OnDrag_m2640352158 (UIWindow_t2837557726 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = L_0;
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m1104419803(L_1, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_3 = ___eventData0;
		NullCheck(L_3);
		Vector2_t2243707579  L_4 = PointerEventData_get_delta_m1072163964(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m2469242620(L_1, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWindow::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void UIWindow_OnPointerDown_m3655928323 (UIWindow_t2837557726 * __this, PointerEventData_t1599784723 * ___eventData0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_SetAsLastSibling_m1528402907(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnfinityGames.U2DEX.u2dexGrid::.ctor()
extern "C"  void u2dexGrid__ctor_m2565121970 (u2dexGrid_t3875635992 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m3067419446(&L_0, (64.0f), (64.0f), /*hidden argument*/NULL);
		__this->set_gridSize_2(L_0);
		Color_t2020392075  L_1 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_color_3(L_1);
		__this->set_gridScale_4(((int32_t)100000));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnfinityGames.U2DEX.u2dexGrid::OnDrawGizmos()
extern "C"  void u2dexGrid_OnDrawGizmos_m1308842860 (u2dexGrid_t3875635992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (u2dexGrid_OnDrawGizmos_m1308842860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t1756533147 * V_1 = NULL;
	Camera_t189460977 * V_2 = NULL;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	{
		Color_t2020392075  L_0 = Gizmos_get_color_m837177613(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Color_t2020392075  L_1 = __this->get_color_3();
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3107983927, /*hidden argument*/NULL);
		V_1 = L_2;
		Camera_t189460977 * L_3 = (Camera_t189460977 *)il2cpp_codegen_object_new(Camera_t189460977_il2cpp_TypeInfo_var);
		Camera__ctor_m2367274244(L_3, /*hidden argument*/NULL);
		V_2 = L_3;
		Vector3_t2243707580  L_4 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_4;
		V_4 = (0.0f);
		V_5 = (0.0f);
		Vector2_t2243707579 * L_5 = __this->get_address_of_gridSize_2();
		float L_6 = L_5->get_x_0();
		float L_7 = V_5;
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_0055;
		}
	}
	{
		Vector2_t2243707579 * L_8 = __this->get_address_of_gridSize_2();
		float L_9 = L_8->get_x_0();
		V_5 = L_9;
	}

IL_0055:
	{
		Vector2_t2243707579 * L_10 = __this->get_address_of_gridSize_2();
		float L_11 = L_10->get_y_1();
		float L_12 = V_5;
		if ((!(((float)L_11) > ((float)L_12))))
		{
			goto IL_0074;
		}
	}
	{
		Vector2_t2243707579 * L_13 = __this->get_address_of_gridSize_2();
		float L_14 = L_13->get_y_1();
		V_5 = L_14;
	}

IL_0074:
	{
		__this->set_lineLength_5((100000.0f));
		int32_t L_15 = __this->get_gridScale_4();
		float L_16 = V_5;
		int32_t L_17 = u2dexGrid_RoundInteger_m2792359982(__this, L_15, ((float)((float)L_16+(float)(0.25f))), /*hidden argument*/NULL);
		__this->set_maximumDistance_7((((float)((float)L_17))));
		GameObject_t1756533147 * L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_18, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0106;
		}
	}
	{
		GameObject_t1756533147 * L_20 = V_1;
		NullCheck(L_20);
		Camera_t189460977 * L_21 = GameObject_GetComponent_TisCamera_t189460977_m4200645945(L_20, /*hidden argument*/GameObject_GetComponent_TisCamera_t189460977_m4200645945_MethodInfo_var);
		V_2 = L_21;
		Camera_t189460977 * L_22 = V_2;
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Component_get_transform_m2697483695(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t2243707580  L_24 = Transform_get_position_m1104419803(L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		Camera_t189460977 * L_25 = V_2;
		NullCheck(L_25);
		float L_26 = Camera_get_fieldOfView_m3384007405(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_27 = tanf(((float)((float)((float)((float)L_26*(float)(0.5f)))*(float)(0.0174532924f))));
		V_6 = ((float)((float)(50000.0f)/(float)L_27));
		float L_28 = V_6;
		float L_29 = __this->get_maximumDistance_7();
		float L_30 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_28, (1.0f), L_29, /*hidden argument*/NULL);
		V_6 = L_30;
		float L_31 = V_6;
		float L_32 = V_5;
		float L_33 = u2dexGrid_Round_m158254686(__this, L_31, ((float)((float)L_32+(float)(0.25f))), /*hidden argument*/NULL);
		V_4 = L_33;
		float L_34 = V_4;
		__this->set_lineLength_5(L_34);
	}

IL_0106:
	{
		Vector2_t2243707579  L_35 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_35;
		Vector3_t2243707580  L_36 = V_3;
		Vector3_t2243707580  L_37 = u2dexGrid_SnapToGridSize_m778086205(__this, L_36, /*hidden argument*/NULL);
		V_3 = L_37;
		float L_38 = V_4;
		Vector2_t2243707579 * L_39 = __this->get_address_of_gridSize_2();
		float L_40 = L_39->get_x_0();
		V_8 = ((float)((float)L_38/(float)L_40));
		float L_41 = V_8;
		V_9 = ((float)((float)((-L_41))+(float)(1.0f)));
		goto IL_01da;
	}

IL_0135:
	{
		float L_42 = (&V_3)->get_y_2();
		Vector2_t2243707579 * L_43 = __this->get_address_of_gridSize_2();
		float L_44 = L_43->get_y_1();
		float L_45 = V_9;
		V_10 = ((float)((float)L_42+(float)((float)((float)L_44*(float)L_45))));
		float L_46 = (&V_3)->get_x_1();
		float L_47 = __this->get_lineLength_5();
		float L_48 = V_10;
		Vector2_t2243707579 * L_49 = __this->get_address_of_gridSize_2();
		float L_50 = L_49->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_51 = floorf(((float)((float)L_48/(float)L_50)));
		Vector2_t2243707579 * L_52 = __this->get_address_of_gridSize_2();
		float L_53 = L_52->get_y_1();
		float L_54 = (&V_7)->get_y_1();
		Vector3_t2243707580  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m2638739322(&L_55, ((float)((float)L_46-(float)L_47)), ((float)((float)((float)((float)L_51*(float)L_53))+(float)L_54)), (0.0f), /*hidden argument*/NULL);
		float L_56 = (&V_3)->get_x_1();
		float L_57 = __this->get_lineLength_5();
		float L_58 = V_10;
		Vector2_t2243707579 * L_59 = __this->get_address_of_gridSize_2();
		float L_60 = L_59->get_y_1();
		float L_61 = floorf(((float)((float)L_58/(float)L_60)));
		Vector2_t2243707579 * L_62 = __this->get_address_of_gridSize_2();
		float L_63 = L_62->get_y_1();
		float L_64 = (&V_7)->get_y_1();
		Vector3_t2243707580  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Vector3__ctor_m2638739322(&L_65, ((float)((float)L_56+(float)L_57)), ((float)((float)((float)((float)L_61*(float)L_63))+(float)L_64)), (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_55, L_65, /*hidden argument*/NULL);
		float L_66 = V_9;
		V_9 = ((float)((float)L_66+(float)(1.0f)));
	}

IL_01da:
	{
		float L_67 = V_9;
		float L_68 = V_8;
		if ((((float)L_67) < ((float)((float)((float)L_68+(float)(1.0f))))))
		{
			goto IL_0135;
		}
	}
	{
		float L_69 = V_8;
		V_11 = ((float)((float)((-L_69))+(float)(1.0f)));
		goto IL_029e;
	}

IL_01f9:
	{
		float L_70 = (&V_3)->get_x_1();
		Vector2_t2243707579 * L_71 = __this->get_address_of_gridSize_2();
		float L_72 = L_71->get_x_0();
		float L_73 = V_11;
		V_12 = ((float)((float)L_70+(float)((float)((float)L_72*(float)L_73))));
		float L_74 = V_12;
		Vector2_t2243707579 * L_75 = __this->get_address_of_gridSize_2();
		float L_76 = L_75->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_77 = floorf(((float)((float)L_74/(float)L_76)));
		Vector2_t2243707579 * L_78 = __this->get_address_of_gridSize_2();
		float L_79 = L_78->get_x_0();
		float L_80 = (&V_7)->get_x_0();
		float L_81 = (&V_3)->get_y_2();
		float L_82 = __this->get_lineLength_5();
		Vector3_t2243707580  L_83;
		memset(&L_83, 0, sizeof(L_83));
		Vector3__ctor_m2638739322(&L_83, ((float)((float)((float)((float)L_77*(float)L_79))+(float)L_80)), ((float)((float)L_81-(float)L_82)), (0.0f), /*hidden argument*/NULL);
		float L_84 = V_12;
		Vector2_t2243707579 * L_85 = __this->get_address_of_gridSize_2();
		float L_86 = L_85->get_x_0();
		float L_87 = floorf(((float)((float)L_84/(float)L_86)));
		Vector2_t2243707579 * L_88 = __this->get_address_of_gridSize_2();
		float L_89 = L_88->get_x_0();
		float L_90 = (&V_7)->get_x_0();
		float L_91 = (&V_3)->get_y_2();
		float L_92 = __this->get_lineLength_5();
		Vector3_t2243707580  L_93;
		memset(&L_93, 0, sizeof(L_93));
		Vector3__ctor_m2638739322(&L_93, ((float)((float)((float)((float)L_87*(float)L_89))+(float)L_90)), ((float)((float)L_91+(float)L_92)), (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawLine_m1315654064(NULL /*static, unused*/, L_83, L_93, /*hidden argument*/NULL);
		float L_94 = V_11;
		V_11 = ((float)((float)L_94+(float)(1.0f)));
	}

IL_029e:
	{
		float L_95 = V_11;
		float L_96 = V_8;
		if ((((float)L_95) < ((float)((float)((float)L_96+(float)(1.0f))))))
		{
			goto IL_01f9;
		}
	}
	{
		Color_t2020392075  L_97 = V_0;
		Gizmos_set_color_m494992840(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnfinityGames.U2DEX.u2dexGrid::RoundInteger(System.Int32,System.Single)
extern "C"  int32_t u2dexGrid_RoundInteger_m2792359982 (u2dexGrid_t3875635992 * __this, int32_t ___integer0, float ___multipleOf1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (u2dexGrid_RoundInteger_m2792359982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___integer0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = ___integer0;
		float L_2 = ___multipleOf1;
		float L_3 = ___multipleOf1;
		float L_4 = ___multipleOf1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)(((float)((float)L_1)))+(float)((float)((float)L_2/(float)(2.0f)))))/(float)L_3))*(float)L_4)), /*hidden argument*/NULL);
		return L_5;
	}

IL_001b:
	{
		int32_t L_6 = ___integer0;
		float L_7 = ___multipleOf1;
		float L_8 = ___multipleOf1;
		float L_9 = ___multipleOf1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_CeilToInt_m2672598779(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)(((float)((float)L_6)))-(float)((float)((float)L_7/(float)(2.0f)))))/(float)L_8))*(float)L_9)), /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector3 UnfinityGames.U2DEX.u2dexGrid::SnapToGridSize(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  u2dexGrid_SnapToGridSize_m778086205 (u2dexGrid_t3875635992 * __this, Vector3_t2243707580  ___position0, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t2243707580  L_0 = ___position0;
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		Vector2_t2243707579 * L_2 = __this->get_address_of_gridSize_2();
		float L_3 = L_2->get_x_0();
		float L_4 = u2dexGrid_Round_m158254686(__this, L_1, L_3, /*hidden argument*/NULL);
		(&V_0)->set_x_1(L_4);
		float L_5 = (&V_0)->get_y_2();
		Vector2_t2243707579 * L_6 = __this->get_address_of_gridSize_2();
		float L_7 = L_6->get_y_1();
		float L_8 = u2dexGrid_Round_m158254686(__this, L_5, L_7, /*hidden argument*/NULL);
		(&V_0)->set_y_2(L_8);
		Vector3_t2243707580  L_9 = V_0;
		Vector3_t2243707580  L_10 = u2dexGrid_FixIfNaN_m505747756(__this, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single UnfinityGames.U2DEX.u2dexGrid::Round(System.Single,System.Single)
extern "C"  float u2dexGrid_Round_m158254686 (u2dexGrid_t3875635992 * __this, float ___input0, float ___SnapTo1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (u2dexGrid_Round_m158254686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___SnapTo1;
		float L_1 = ___input0;
		float L_2 = ___SnapTo1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(((float)((float)((float)((float)L_1/(float)L_2))*(float)(100.0f))));
		return ((float)((float)((float)((float)L_0*(float)L_3))/(float)(100.0f)));
	}
}
// UnityEngine.Vector3 UnfinityGames.U2DEX.u2dexGrid::FixIfNaN(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  u2dexGrid_FixIfNaN_m505747756 (u2dexGrid_t3875635992 * __this, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	{
		float L_0 = (&___v0)->get_x_1();
		bool L_1 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		(&___v0)->set_x_1((0.0f));
	}

IL_001d:
	{
		float L_2 = (&___v0)->get_y_2();
		bool L_3 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		(&___v0)->set_y_2((0.0f));
	}

IL_003a:
	{
		float L_4 = (&___v0)->get_z_3();
		bool L_5 = Single_IsNaN_m2349591895(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		(&___v0)->set_z_3((0.0f));
	}

IL_0057:
	{
		Vector3_t2243707580  L_6 = ___v0;
		return L_6;
	}
}
// System.Void xARMProxy::.ctor()
extern "C"  void xARMProxy__ctor_m2588067561 (xARMProxy_t3550954754 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
