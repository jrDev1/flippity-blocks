﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"

// SRF.UI.SRNumberSpinner
struct SRNumberSpinner_t2685847661;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.SRNumberButton
struct  SRNumberButton_t836980342  : public Button_t2872111280
{
public:
	// System.Single SRF.UI.SRNumberButton::_delayTime
	float ____delayTime_19;
	// System.Single SRF.UI.SRNumberButton::_downTime
	float ____downTime_20;
	// System.Boolean SRF.UI.SRNumberButton::_isDown
	bool ____isDown_21;
	// System.Double SRF.UI.SRNumberButton::Amount
	double ___Amount_22;
	// SRF.UI.SRNumberSpinner SRF.UI.SRNumberButton::TargetField
	SRNumberSpinner_t2685847661 * ___TargetField_23;

public:
	inline static int32_t get_offset_of__delayTime_19() { return static_cast<int32_t>(offsetof(SRNumberButton_t836980342, ____delayTime_19)); }
	inline float get__delayTime_19() const { return ____delayTime_19; }
	inline float* get_address_of__delayTime_19() { return &____delayTime_19; }
	inline void set__delayTime_19(float value)
	{
		____delayTime_19 = value;
	}

	inline static int32_t get_offset_of__downTime_20() { return static_cast<int32_t>(offsetof(SRNumberButton_t836980342, ____downTime_20)); }
	inline float get__downTime_20() const { return ____downTime_20; }
	inline float* get_address_of__downTime_20() { return &____downTime_20; }
	inline void set__downTime_20(float value)
	{
		____downTime_20 = value;
	}

	inline static int32_t get_offset_of__isDown_21() { return static_cast<int32_t>(offsetof(SRNumberButton_t836980342, ____isDown_21)); }
	inline bool get__isDown_21() const { return ____isDown_21; }
	inline bool* get_address_of__isDown_21() { return &____isDown_21; }
	inline void set__isDown_21(bool value)
	{
		____isDown_21 = value;
	}

	inline static int32_t get_offset_of_Amount_22() { return static_cast<int32_t>(offsetof(SRNumberButton_t836980342, ___Amount_22)); }
	inline double get_Amount_22() const { return ___Amount_22; }
	inline double* get_address_of_Amount_22() { return &___Amount_22; }
	inline void set_Amount_22(double value)
	{
		___Amount_22 = value;
	}

	inline static int32_t get_offset_of_TargetField_23() { return static_cast<int32_t>(offsetof(SRNumberButton_t836980342, ___TargetField_23)); }
	inline SRNumberSpinner_t2685847661 * get_TargetField_23() const { return ___TargetField_23; }
	inline SRNumberSpinner_t2685847661 ** get_address_of_TargetField_23() { return &___TargetField_23; }
	inline void set_TargetField_23(SRNumberSpinner_t2685847661 * value)
	{
		___TargetField_23 = value;
		Il2CppCodeGenWriteBarrier(&___TargetField_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
