﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.Iterate
struct  Iterate_t1664451336  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Iterate::startIndex
	FsmInt_t1273009179 * ___startIndex_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Iterate::endIndex
	FsmInt_t1273009179 * ___endIndex_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Iterate::increment
	FsmInt_t1273009179 * ___increment_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Iterate::loopEvent
	FsmEvent_t1258573736 * ___loopEvent_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.Iterate::finishedEvent
	FsmEvent_t1258573736 * ___finishedEvent_15;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.Iterate::currentIndex
	FsmInt_t1273009179 * ___currentIndex_16;
	// System.Boolean HutongGames.PlayMaker.Actions.Iterate::started
	bool ___started_17;
	// System.Boolean HutongGames.PlayMaker.Actions.Iterate::_up
	bool ____up_18;

public:
	inline static int32_t get_offset_of_startIndex_11() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___startIndex_11)); }
	inline FsmInt_t1273009179 * get_startIndex_11() const { return ___startIndex_11; }
	inline FsmInt_t1273009179 ** get_address_of_startIndex_11() { return &___startIndex_11; }
	inline void set_startIndex_11(FsmInt_t1273009179 * value)
	{
		___startIndex_11 = value;
		Il2CppCodeGenWriteBarrier(&___startIndex_11, value);
	}

	inline static int32_t get_offset_of_endIndex_12() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___endIndex_12)); }
	inline FsmInt_t1273009179 * get_endIndex_12() const { return ___endIndex_12; }
	inline FsmInt_t1273009179 ** get_address_of_endIndex_12() { return &___endIndex_12; }
	inline void set_endIndex_12(FsmInt_t1273009179 * value)
	{
		___endIndex_12 = value;
		Il2CppCodeGenWriteBarrier(&___endIndex_12, value);
	}

	inline static int32_t get_offset_of_increment_13() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___increment_13)); }
	inline FsmInt_t1273009179 * get_increment_13() const { return ___increment_13; }
	inline FsmInt_t1273009179 ** get_address_of_increment_13() { return &___increment_13; }
	inline void set_increment_13(FsmInt_t1273009179 * value)
	{
		___increment_13 = value;
		Il2CppCodeGenWriteBarrier(&___increment_13, value);
	}

	inline static int32_t get_offset_of_loopEvent_14() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___loopEvent_14)); }
	inline FsmEvent_t1258573736 * get_loopEvent_14() const { return ___loopEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_loopEvent_14() { return &___loopEvent_14; }
	inline void set_loopEvent_14(FsmEvent_t1258573736 * value)
	{
		___loopEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___loopEvent_14, value);
	}

	inline static int32_t get_offset_of_finishedEvent_15() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___finishedEvent_15)); }
	inline FsmEvent_t1258573736 * get_finishedEvent_15() const { return ___finishedEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_finishedEvent_15() { return &___finishedEvent_15; }
	inline void set_finishedEvent_15(FsmEvent_t1258573736 * value)
	{
		___finishedEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___finishedEvent_15, value);
	}

	inline static int32_t get_offset_of_currentIndex_16() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___currentIndex_16)); }
	inline FsmInt_t1273009179 * get_currentIndex_16() const { return ___currentIndex_16; }
	inline FsmInt_t1273009179 ** get_address_of_currentIndex_16() { return &___currentIndex_16; }
	inline void set_currentIndex_16(FsmInt_t1273009179 * value)
	{
		___currentIndex_16 = value;
		Il2CppCodeGenWriteBarrier(&___currentIndex_16, value);
	}

	inline static int32_t get_offset_of_started_17() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ___started_17)); }
	inline bool get_started_17() const { return ___started_17; }
	inline bool* get_address_of_started_17() { return &___started_17; }
	inline void set_started_17(bool value)
	{
		___started_17 = value;
	}

	inline static int32_t get_offset_of__up_18() { return static_cast<int32_t>(offsetof(Iterate_t1664451336, ____up_18)); }
	inline bool get__up_18() const { return ____up_18; }
	inline bool* get_address_of__up_18() { return &____up_18; }
	inline void set__up_18(bool value)
	{
		____up_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
