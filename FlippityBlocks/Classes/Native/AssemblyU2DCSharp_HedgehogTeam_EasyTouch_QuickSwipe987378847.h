﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickSwip1197768855.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickSwip2097550818.h"

// HedgehogTeam.EasyTouch.QuickSwipe/OnSwipeAction
struct OnSwipeAction_t1378047388;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickSwipe
struct  QuickSwipe_t987378847  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickSwipe/OnSwipeAction HedgehogTeam.EasyTouch.QuickSwipe::onSwipeAction
	OnSwipeAction_t1378047388 * ___onSwipeAction_19;
	// System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::allowSwipeStartOverMe
	bool ___allowSwipeStartOverMe_20;
	// HedgehogTeam.EasyTouch.QuickSwipe/ActionTriggering HedgehogTeam.EasyTouch.QuickSwipe::actionTriggering
	int32_t ___actionTriggering_21;
	// HedgehogTeam.EasyTouch.QuickSwipe/SwipeDirection HedgehogTeam.EasyTouch.QuickSwipe::swipeDirection
	int32_t ___swipeDirection_22;
	// System.Single HedgehogTeam.EasyTouch.QuickSwipe::axisActionValue
	float ___axisActionValue_23;
	// System.Boolean HedgehogTeam.EasyTouch.QuickSwipe::enableSimpleAction
	bool ___enableSimpleAction_24;

public:
	inline static int32_t get_offset_of_onSwipeAction_19() { return static_cast<int32_t>(offsetof(QuickSwipe_t987378847, ___onSwipeAction_19)); }
	inline OnSwipeAction_t1378047388 * get_onSwipeAction_19() const { return ___onSwipeAction_19; }
	inline OnSwipeAction_t1378047388 ** get_address_of_onSwipeAction_19() { return &___onSwipeAction_19; }
	inline void set_onSwipeAction_19(OnSwipeAction_t1378047388 * value)
	{
		___onSwipeAction_19 = value;
		Il2CppCodeGenWriteBarrier(&___onSwipeAction_19, value);
	}

	inline static int32_t get_offset_of_allowSwipeStartOverMe_20() { return static_cast<int32_t>(offsetof(QuickSwipe_t987378847, ___allowSwipeStartOverMe_20)); }
	inline bool get_allowSwipeStartOverMe_20() const { return ___allowSwipeStartOverMe_20; }
	inline bool* get_address_of_allowSwipeStartOverMe_20() { return &___allowSwipeStartOverMe_20; }
	inline void set_allowSwipeStartOverMe_20(bool value)
	{
		___allowSwipeStartOverMe_20 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_21() { return static_cast<int32_t>(offsetof(QuickSwipe_t987378847, ___actionTriggering_21)); }
	inline int32_t get_actionTriggering_21() const { return ___actionTriggering_21; }
	inline int32_t* get_address_of_actionTriggering_21() { return &___actionTriggering_21; }
	inline void set_actionTriggering_21(int32_t value)
	{
		___actionTriggering_21 = value;
	}

	inline static int32_t get_offset_of_swipeDirection_22() { return static_cast<int32_t>(offsetof(QuickSwipe_t987378847, ___swipeDirection_22)); }
	inline int32_t get_swipeDirection_22() const { return ___swipeDirection_22; }
	inline int32_t* get_address_of_swipeDirection_22() { return &___swipeDirection_22; }
	inline void set_swipeDirection_22(int32_t value)
	{
		___swipeDirection_22 = value;
	}

	inline static int32_t get_offset_of_axisActionValue_23() { return static_cast<int32_t>(offsetof(QuickSwipe_t987378847, ___axisActionValue_23)); }
	inline float get_axisActionValue_23() const { return ___axisActionValue_23; }
	inline float* get_address_of_axisActionValue_23() { return &___axisActionValue_23; }
	inline void set_axisActionValue_23(float value)
	{
		___axisActionValue_23 = value;
	}

	inline static int32_t get_offset_of_enableSimpleAction_24() { return static_cast<int32_t>(offsetof(QuickSwipe_t987378847, ___enableSimpleAction_24)); }
	inline bool get_enableSimpleAction_24() const { return ___enableSimpleAction_24; }
	inline bool* get_address_of_enableSimpleAction_24() { return &___enableSimpleAction_24; }
	inline void set_enableSimpleAction_24(bool value)
	{
		___enableSimpleAction_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
