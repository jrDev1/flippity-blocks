﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_835798112.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.TwoFingerGesture
struct  TwoFingerGesture_t2848072384  : public Il2CppObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/GestureType HedgehogTeam.EasyTouch.TwoFingerGesture::currentGesture
	int32_t ___currentGesture_0;
	// HedgehogTeam.EasyTouch.EasyTouch/GestureType HedgehogTeam.EasyTouch.TwoFingerGesture::oldGesture
	int32_t ___oldGesture_1;
	// System.Int32 HedgehogTeam.EasyTouch.TwoFingerGesture::finger0
	int32_t ___finger0_2;
	// System.Int32 HedgehogTeam.EasyTouch.TwoFingerGesture::finger1
	int32_t ___finger1_3;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::startTimeAction
	float ___startTimeAction_4;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::timeSinceStartAction
	float ___timeSinceStartAction_5;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::startPosition
	Vector2_t2243707579  ___startPosition_6;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::position
	Vector2_t2243707579  ___position_7;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::deltaPosition
	Vector2_t2243707579  ___deltaPosition_8;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.TwoFingerGesture::oldStartPosition
	Vector2_t2243707579  ___oldStartPosition_9;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::startDistance
	float ___startDistance_10;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::fingerDistance
	float ___fingerDistance_11;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::oldFingerDistance
	float ___oldFingerDistance_12;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::lockPinch
	bool ___lockPinch_13;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::lockTwist
	bool ___lockTwist_14;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::lastPinch
	float ___lastPinch_15;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::lastTwistAngle
	float ___lastTwistAngle_16;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::pickedObject
	GameObject_t1756533147 * ___pickedObject_17;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::oldPickedObject
	GameObject_t1756533147 * ___oldPickedObject_18;
	// UnityEngine.Camera HedgehogTeam.EasyTouch.TwoFingerGesture::pickedCamera
	Camera_t189460977 * ___pickedCamera_19;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::isGuiCamera
	bool ___isGuiCamera_20;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::isOverGui
	bool ___isOverGui_21;
	// UnityEngine.GameObject HedgehogTeam.EasyTouch.TwoFingerGesture::pickedUIElement
	GameObject_t1756533147 * ___pickedUIElement_22;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::dragStart
	bool ___dragStart_23;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::swipeStart
	bool ___swipeStart_24;
	// System.Boolean HedgehogTeam.EasyTouch.TwoFingerGesture::inSingleDoubleTaps
	bool ___inSingleDoubleTaps_25;
	// System.Single HedgehogTeam.EasyTouch.TwoFingerGesture::tapCurentTime
	float ___tapCurentTime_26;

public:
	inline static int32_t get_offset_of_currentGesture_0() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___currentGesture_0)); }
	inline int32_t get_currentGesture_0() const { return ___currentGesture_0; }
	inline int32_t* get_address_of_currentGesture_0() { return &___currentGesture_0; }
	inline void set_currentGesture_0(int32_t value)
	{
		___currentGesture_0 = value;
	}

	inline static int32_t get_offset_of_oldGesture_1() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___oldGesture_1)); }
	inline int32_t get_oldGesture_1() const { return ___oldGesture_1; }
	inline int32_t* get_address_of_oldGesture_1() { return &___oldGesture_1; }
	inline void set_oldGesture_1(int32_t value)
	{
		___oldGesture_1 = value;
	}

	inline static int32_t get_offset_of_finger0_2() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___finger0_2)); }
	inline int32_t get_finger0_2() const { return ___finger0_2; }
	inline int32_t* get_address_of_finger0_2() { return &___finger0_2; }
	inline void set_finger0_2(int32_t value)
	{
		___finger0_2 = value;
	}

	inline static int32_t get_offset_of_finger1_3() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___finger1_3)); }
	inline int32_t get_finger1_3() const { return ___finger1_3; }
	inline int32_t* get_address_of_finger1_3() { return &___finger1_3; }
	inline void set_finger1_3(int32_t value)
	{
		___finger1_3 = value;
	}

	inline static int32_t get_offset_of_startTimeAction_4() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___startTimeAction_4)); }
	inline float get_startTimeAction_4() const { return ___startTimeAction_4; }
	inline float* get_address_of_startTimeAction_4() { return &___startTimeAction_4; }
	inline void set_startTimeAction_4(float value)
	{
		___startTimeAction_4 = value;
	}

	inline static int32_t get_offset_of_timeSinceStartAction_5() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___timeSinceStartAction_5)); }
	inline float get_timeSinceStartAction_5() const { return ___timeSinceStartAction_5; }
	inline float* get_address_of_timeSinceStartAction_5() { return &___timeSinceStartAction_5; }
	inline void set_timeSinceStartAction_5(float value)
	{
		___timeSinceStartAction_5 = value;
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___startPosition_6)); }
	inline Vector2_t2243707579  get_startPosition_6() const { return ___startPosition_6; }
	inline Vector2_t2243707579 * get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(Vector2_t2243707579  value)
	{
		___startPosition_6 = value;
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___position_7)); }
	inline Vector2_t2243707579  get_position_7() const { return ___position_7; }
	inline Vector2_t2243707579 * get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(Vector2_t2243707579  value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_8() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___deltaPosition_8)); }
	inline Vector2_t2243707579  get_deltaPosition_8() const { return ___deltaPosition_8; }
	inline Vector2_t2243707579 * get_address_of_deltaPosition_8() { return &___deltaPosition_8; }
	inline void set_deltaPosition_8(Vector2_t2243707579  value)
	{
		___deltaPosition_8 = value;
	}

	inline static int32_t get_offset_of_oldStartPosition_9() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___oldStartPosition_9)); }
	inline Vector2_t2243707579  get_oldStartPosition_9() const { return ___oldStartPosition_9; }
	inline Vector2_t2243707579 * get_address_of_oldStartPosition_9() { return &___oldStartPosition_9; }
	inline void set_oldStartPosition_9(Vector2_t2243707579  value)
	{
		___oldStartPosition_9 = value;
	}

	inline static int32_t get_offset_of_startDistance_10() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___startDistance_10)); }
	inline float get_startDistance_10() const { return ___startDistance_10; }
	inline float* get_address_of_startDistance_10() { return &___startDistance_10; }
	inline void set_startDistance_10(float value)
	{
		___startDistance_10 = value;
	}

	inline static int32_t get_offset_of_fingerDistance_11() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___fingerDistance_11)); }
	inline float get_fingerDistance_11() const { return ___fingerDistance_11; }
	inline float* get_address_of_fingerDistance_11() { return &___fingerDistance_11; }
	inline void set_fingerDistance_11(float value)
	{
		___fingerDistance_11 = value;
	}

	inline static int32_t get_offset_of_oldFingerDistance_12() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___oldFingerDistance_12)); }
	inline float get_oldFingerDistance_12() const { return ___oldFingerDistance_12; }
	inline float* get_address_of_oldFingerDistance_12() { return &___oldFingerDistance_12; }
	inline void set_oldFingerDistance_12(float value)
	{
		___oldFingerDistance_12 = value;
	}

	inline static int32_t get_offset_of_lockPinch_13() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___lockPinch_13)); }
	inline bool get_lockPinch_13() const { return ___lockPinch_13; }
	inline bool* get_address_of_lockPinch_13() { return &___lockPinch_13; }
	inline void set_lockPinch_13(bool value)
	{
		___lockPinch_13 = value;
	}

	inline static int32_t get_offset_of_lockTwist_14() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___lockTwist_14)); }
	inline bool get_lockTwist_14() const { return ___lockTwist_14; }
	inline bool* get_address_of_lockTwist_14() { return &___lockTwist_14; }
	inline void set_lockTwist_14(bool value)
	{
		___lockTwist_14 = value;
	}

	inline static int32_t get_offset_of_lastPinch_15() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___lastPinch_15)); }
	inline float get_lastPinch_15() const { return ___lastPinch_15; }
	inline float* get_address_of_lastPinch_15() { return &___lastPinch_15; }
	inline void set_lastPinch_15(float value)
	{
		___lastPinch_15 = value;
	}

	inline static int32_t get_offset_of_lastTwistAngle_16() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___lastTwistAngle_16)); }
	inline float get_lastTwistAngle_16() const { return ___lastTwistAngle_16; }
	inline float* get_address_of_lastTwistAngle_16() { return &___lastTwistAngle_16; }
	inline void set_lastTwistAngle_16(float value)
	{
		___lastTwistAngle_16 = value;
	}

	inline static int32_t get_offset_of_pickedObject_17() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___pickedObject_17)); }
	inline GameObject_t1756533147 * get_pickedObject_17() const { return ___pickedObject_17; }
	inline GameObject_t1756533147 ** get_address_of_pickedObject_17() { return &___pickedObject_17; }
	inline void set_pickedObject_17(GameObject_t1756533147 * value)
	{
		___pickedObject_17 = value;
		Il2CppCodeGenWriteBarrier(&___pickedObject_17, value);
	}

	inline static int32_t get_offset_of_oldPickedObject_18() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___oldPickedObject_18)); }
	inline GameObject_t1756533147 * get_oldPickedObject_18() const { return ___oldPickedObject_18; }
	inline GameObject_t1756533147 ** get_address_of_oldPickedObject_18() { return &___oldPickedObject_18; }
	inline void set_oldPickedObject_18(GameObject_t1756533147 * value)
	{
		___oldPickedObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___oldPickedObject_18, value);
	}

	inline static int32_t get_offset_of_pickedCamera_19() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___pickedCamera_19)); }
	inline Camera_t189460977 * get_pickedCamera_19() const { return ___pickedCamera_19; }
	inline Camera_t189460977 ** get_address_of_pickedCamera_19() { return &___pickedCamera_19; }
	inline void set_pickedCamera_19(Camera_t189460977 * value)
	{
		___pickedCamera_19 = value;
		Il2CppCodeGenWriteBarrier(&___pickedCamera_19, value);
	}

	inline static int32_t get_offset_of_isGuiCamera_20() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___isGuiCamera_20)); }
	inline bool get_isGuiCamera_20() const { return ___isGuiCamera_20; }
	inline bool* get_address_of_isGuiCamera_20() { return &___isGuiCamera_20; }
	inline void set_isGuiCamera_20(bool value)
	{
		___isGuiCamera_20 = value;
	}

	inline static int32_t get_offset_of_isOverGui_21() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___isOverGui_21)); }
	inline bool get_isOverGui_21() const { return ___isOverGui_21; }
	inline bool* get_address_of_isOverGui_21() { return &___isOverGui_21; }
	inline void set_isOverGui_21(bool value)
	{
		___isOverGui_21 = value;
	}

	inline static int32_t get_offset_of_pickedUIElement_22() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___pickedUIElement_22)); }
	inline GameObject_t1756533147 * get_pickedUIElement_22() const { return ___pickedUIElement_22; }
	inline GameObject_t1756533147 ** get_address_of_pickedUIElement_22() { return &___pickedUIElement_22; }
	inline void set_pickedUIElement_22(GameObject_t1756533147 * value)
	{
		___pickedUIElement_22 = value;
		Il2CppCodeGenWriteBarrier(&___pickedUIElement_22, value);
	}

	inline static int32_t get_offset_of_dragStart_23() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___dragStart_23)); }
	inline bool get_dragStart_23() const { return ___dragStart_23; }
	inline bool* get_address_of_dragStart_23() { return &___dragStart_23; }
	inline void set_dragStart_23(bool value)
	{
		___dragStart_23 = value;
	}

	inline static int32_t get_offset_of_swipeStart_24() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___swipeStart_24)); }
	inline bool get_swipeStart_24() const { return ___swipeStart_24; }
	inline bool* get_address_of_swipeStart_24() { return &___swipeStart_24; }
	inline void set_swipeStart_24(bool value)
	{
		___swipeStart_24 = value;
	}

	inline static int32_t get_offset_of_inSingleDoubleTaps_25() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___inSingleDoubleTaps_25)); }
	inline bool get_inSingleDoubleTaps_25() const { return ___inSingleDoubleTaps_25; }
	inline bool* get_address_of_inSingleDoubleTaps_25() { return &___inSingleDoubleTaps_25; }
	inline void set_inSingleDoubleTaps_25(bool value)
	{
		___inSingleDoubleTaps_25 = value;
	}

	inline static int32_t get_offset_of_tapCurentTime_26() { return static_cast<int32_t>(offsetof(TwoFingerGesture_t2848072384, ___tapCurentTime_26)); }
	inline float get_tapCurentTime_26() const { return ___tapCurentTime_26; }
	inline float* get_address_of_tapCurentTime_26() { return &___tapCurentTime_26; }
	inline void set_tapCurentTime_26(float value)
	{
		___tapCurentTime_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
