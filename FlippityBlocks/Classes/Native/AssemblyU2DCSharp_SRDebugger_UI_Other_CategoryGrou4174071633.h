﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.CategoryGroup
struct  CategoryGroup_t4174071633  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.RectTransform SRDebugger.UI.Other.CategoryGroup::Container
	RectTransform_t3349966182 * ___Container_9;
	// UnityEngine.UI.Text SRDebugger.UI.Other.CategoryGroup::Header
	Text_t356221433 * ___Header_10;
	// UnityEngine.GameObject SRDebugger.UI.Other.CategoryGroup::Background
	GameObject_t1756533147 * ___Background_11;
	// UnityEngine.UI.Toggle SRDebugger.UI.Other.CategoryGroup::SelectionToggle
	Toggle_t3976754468 * ___SelectionToggle_12;
	// UnityEngine.GameObject[] SRDebugger.UI.Other.CategoryGroup::EnabledDuringSelectionMode
	GameObjectU5BU5D_t3057952154* ___EnabledDuringSelectionMode_13;
	// System.Boolean SRDebugger.UI.Other.CategoryGroup::_selectionModeEnabled
	bool ____selectionModeEnabled_14;

public:
	inline static int32_t get_offset_of_Container_9() { return static_cast<int32_t>(offsetof(CategoryGroup_t4174071633, ___Container_9)); }
	inline RectTransform_t3349966182 * get_Container_9() const { return ___Container_9; }
	inline RectTransform_t3349966182 ** get_address_of_Container_9() { return &___Container_9; }
	inline void set_Container_9(RectTransform_t3349966182 * value)
	{
		___Container_9 = value;
		Il2CppCodeGenWriteBarrier(&___Container_9, value);
	}

	inline static int32_t get_offset_of_Header_10() { return static_cast<int32_t>(offsetof(CategoryGroup_t4174071633, ___Header_10)); }
	inline Text_t356221433 * get_Header_10() const { return ___Header_10; }
	inline Text_t356221433 ** get_address_of_Header_10() { return &___Header_10; }
	inline void set_Header_10(Text_t356221433 * value)
	{
		___Header_10 = value;
		Il2CppCodeGenWriteBarrier(&___Header_10, value);
	}

	inline static int32_t get_offset_of_Background_11() { return static_cast<int32_t>(offsetof(CategoryGroup_t4174071633, ___Background_11)); }
	inline GameObject_t1756533147 * get_Background_11() const { return ___Background_11; }
	inline GameObject_t1756533147 ** get_address_of_Background_11() { return &___Background_11; }
	inline void set_Background_11(GameObject_t1756533147 * value)
	{
		___Background_11 = value;
		Il2CppCodeGenWriteBarrier(&___Background_11, value);
	}

	inline static int32_t get_offset_of_SelectionToggle_12() { return static_cast<int32_t>(offsetof(CategoryGroup_t4174071633, ___SelectionToggle_12)); }
	inline Toggle_t3976754468 * get_SelectionToggle_12() const { return ___SelectionToggle_12; }
	inline Toggle_t3976754468 ** get_address_of_SelectionToggle_12() { return &___SelectionToggle_12; }
	inline void set_SelectionToggle_12(Toggle_t3976754468 * value)
	{
		___SelectionToggle_12 = value;
		Il2CppCodeGenWriteBarrier(&___SelectionToggle_12, value);
	}

	inline static int32_t get_offset_of_EnabledDuringSelectionMode_13() { return static_cast<int32_t>(offsetof(CategoryGroup_t4174071633, ___EnabledDuringSelectionMode_13)); }
	inline GameObjectU5BU5D_t3057952154* get_EnabledDuringSelectionMode_13() const { return ___EnabledDuringSelectionMode_13; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_EnabledDuringSelectionMode_13() { return &___EnabledDuringSelectionMode_13; }
	inline void set_EnabledDuringSelectionMode_13(GameObjectU5BU5D_t3057952154* value)
	{
		___EnabledDuringSelectionMode_13 = value;
		Il2CppCodeGenWriteBarrier(&___EnabledDuringSelectionMode_13, value);
	}

	inline static int32_t get_offset_of__selectionModeEnabled_14() { return static_cast<int32_t>(offsetof(CategoryGroup_t4174071633, ____selectionModeEnabled_14)); }
	inline bool get__selectionModeEnabled_14() const { return ____selectionModeEnabled_14; }
	inline bool* get_address_of__selectionModeEnabled_14() { return &____selectionModeEnabled_14; }
	inline void set__selectionModeEnabled_14(bool value)
	{
		____selectionModeEnabled_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
