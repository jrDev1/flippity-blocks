﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206772.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236547.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2306864769.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-F3688CBC204DF55BFD18894A5DCDFA4E1697DA18
	U24ArrayTypeU3D28_t2375206772  ___U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0;
	// <PrivateImplementationDetails>/$ArrayType=36 <PrivateImplementationDetails>::$field-37590A927D860D71B3B6C6A3F987E759520FB780
	U24ArrayTypeU3D36_t3894236547  ___U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1;
	// <PrivateImplementationDetails>/$ArrayType=124 <PrivateImplementationDetails>::$field-5AB7D59C6B235DDF1049955538B64DC36FC29B8D
	U24ArrayTypeU3D124_t2306864769  ___U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2;

public:
	inline static int32_t get_offset_of_U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0)); }
	inline U24ArrayTypeU3D28_t2375206772  get_U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0() const { return ___U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0; }
	inline U24ArrayTypeU3D28_t2375206772 * get_address_of_U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0() { return &___U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0; }
	inline void set_U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0(U24ArrayTypeU3D28_t2375206772  value)
	{
		___U24fieldU2DF3688CBC204DF55BFD18894A5DCDFA4E1697DA18_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1)); }
	inline U24ArrayTypeU3D36_t3894236547  get_U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1() const { return ___U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1; }
	inline U24ArrayTypeU3D36_t3894236547 * get_address_of_U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1() { return &___U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1; }
	inline void set_U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1(U24ArrayTypeU3D36_t3894236547  value)
	{
		___U24fieldU2D37590A927D860D71B3B6C6A3F987E759520FB780_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2)); }
	inline U24ArrayTypeU3D124_t2306864769  get_U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2() const { return ___U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2; }
	inline U24ArrayTypeU3D124_t2306864769 * get_address_of_U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2() { return &___U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2; }
	inline void set_U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2(U24ArrayTypeU3D124_t2306864769  value)
	{
		___U24fieldU2D5AB7D59C6B235DDF1049955538B64DC36FC29B8D_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
