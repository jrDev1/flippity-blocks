﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// SRDebugger.Internal.OptionDefinition
struct OptionDefinition_t4209375604;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.OptionsControlBase
struct  OptionsControlBase_t3483301704  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Boolean SRDebugger.UI.Controls.OptionsControlBase::_selectionModeEnabled
	bool ____selectionModeEnabled_9;
	// UnityEngine.UI.Toggle SRDebugger.UI.Controls.OptionsControlBase::SelectionModeToggle
	Toggle_t3976754468 * ___SelectionModeToggle_10;
	// SRDebugger.Internal.OptionDefinition SRDebugger.UI.Controls.OptionsControlBase::Option
	OptionDefinition_t4209375604 * ___Option_11;

public:
	inline static int32_t get_offset_of__selectionModeEnabled_9() { return static_cast<int32_t>(offsetof(OptionsControlBase_t3483301704, ____selectionModeEnabled_9)); }
	inline bool get__selectionModeEnabled_9() const { return ____selectionModeEnabled_9; }
	inline bool* get_address_of__selectionModeEnabled_9() { return &____selectionModeEnabled_9; }
	inline void set__selectionModeEnabled_9(bool value)
	{
		____selectionModeEnabled_9 = value;
	}

	inline static int32_t get_offset_of_SelectionModeToggle_10() { return static_cast<int32_t>(offsetof(OptionsControlBase_t3483301704, ___SelectionModeToggle_10)); }
	inline Toggle_t3976754468 * get_SelectionModeToggle_10() const { return ___SelectionModeToggle_10; }
	inline Toggle_t3976754468 ** get_address_of_SelectionModeToggle_10() { return &___SelectionModeToggle_10; }
	inline void set_SelectionModeToggle_10(Toggle_t3976754468 * value)
	{
		___SelectionModeToggle_10 = value;
		Il2CppCodeGenWriteBarrier(&___SelectionModeToggle_10, value);
	}

	inline static int32_t get_offset_of_Option_11() { return static_cast<int32_t>(offsetof(OptionsControlBase_t3483301704, ___Option_11)); }
	inline OptionDefinition_t4209375604 * get_Option_11() const { return ___Option_11; }
	inline OptionDefinition_t4209375604 ** get_address_of_Option_11() { return &___Option_11; }
	inline void set_Option_11(OptionDefinition_t4209375604 * value)
	{
		___Option_11 = value;
		Il2CppCodeGenWriteBarrier(&___Option_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
