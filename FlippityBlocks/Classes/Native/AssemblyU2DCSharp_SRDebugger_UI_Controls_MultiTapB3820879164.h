﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.MultiTapButton
struct  MultiTapButton_t3820879164  : public Button_t2872111280
{
public:
	// System.Single SRDebugger.UI.Controls.MultiTapButton::_lastTap
	float ____lastTap_17;
	// System.Int32 SRDebugger.UI.Controls.MultiTapButton::_tapCount
	int32_t ____tapCount_18;
	// System.Int32 SRDebugger.UI.Controls.MultiTapButton::RequiredTapCount
	int32_t ___RequiredTapCount_19;
	// System.Single SRDebugger.UI.Controls.MultiTapButton::ResetTime
	float ___ResetTime_20;

public:
	inline static int32_t get_offset_of__lastTap_17() { return static_cast<int32_t>(offsetof(MultiTapButton_t3820879164, ____lastTap_17)); }
	inline float get__lastTap_17() const { return ____lastTap_17; }
	inline float* get_address_of__lastTap_17() { return &____lastTap_17; }
	inline void set__lastTap_17(float value)
	{
		____lastTap_17 = value;
	}

	inline static int32_t get_offset_of__tapCount_18() { return static_cast<int32_t>(offsetof(MultiTapButton_t3820879164, ____tapCount_18)); }
	inline int32_t get__tapCount_18() const { return ____tapCount_18; }
	inline int32_t* get_address_of__tapCount_18() { return &____tapCount_18; }
	inline void set__tapCount_18(int32_t value)
	{
		____tapCount_18 = value;
	}

	inline static int32_t get_offset_of_RequiredTapCount_19() { return static_cast<int32_t>(offsetof(MultiTapButton_t3820879164, ___RequiredTapCount_19)); }
	inline int32_t get_RequiredTapCount_19() const { return ___RequiredTapCount_19; }
	inline int32_t* get_address_of_RequiredTapCount_19() { return &___RequiredTapCount_19; }
	inline void set_RequiredTapCount_19(int32_t value)
	{
		___RequiredTapCount_19 = value;
	}

	inline static int32_t get_offset_of_ResetTime_20() { return static_cast<int32_t>(offsetof(MultiTapButton_t3820879164, ___ResetTime_20)); }
	inline float get_ResetTime_20() const { return ___ResetTime_20; }
	inline float* get_address_of_ResetTime_20() { return &___ResetTime_20; }
	inline void set_ResetTime_20(float value)
	{
		___ResetTime_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
