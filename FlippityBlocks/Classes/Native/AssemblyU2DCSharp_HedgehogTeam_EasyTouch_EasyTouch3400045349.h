﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_110068694.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouchTrigger/<IsRecevier4>c__AnonStorey0
struct  U3CIsRecevier4U3Ec__AnonStorey0_t3400045349  : public Il2CppObject
{
public:
	// HedgehogTeam.EasyTouch.EasyTouch/EvtType HedgehogTeam.EasyTouch.EasyTouchTrigger/<IsRecevier4>c__AnonStorey0::evnt
	int32_t ___evnt_0;

public:
	inline static int32_t get_offset_of_evnt_0() { return static_cast<int32_t>(offsetof(U3CIsRecevier4U3Ec__AnonStorey0_t3400045349, ___evnt_0)); }
	inline int32_t get_evnt_0() const { return ___evnt_0; }
	inline int32_t* get_address_of_evnt_0() { return &___evnt_0; }
	inline void set_evnt_0(int32_t value)
	{
		___evnt_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
