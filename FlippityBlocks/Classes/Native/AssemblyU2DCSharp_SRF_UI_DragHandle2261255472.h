﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis3420330537.h"

// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2574720772;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.DragHandle
struct  DragHandle_t2261255472  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.CanvasScaler SRF.UI.DragHandle::_canvasScaler
	CanvasScaler_t2574720772 * ____canvasScaler_2;
	// System.Single SRF.UI.DragHandle::_delta
	float ____delta_3;
	// System.Single SRF.UI.DragHandle::_startValue
	float ____startValue_4;
	// UnityEngine.RectTransform/Axis SRF.UI.DragHandle::Axis
	int32_t ___Axis_5;
	// System.Boolean SRF.UI.DragHandle::Invert
	bool ___Invert_6;
	// System.Single SRF.UI.DragHandle::MaxSize
	float ___MaxSize_7;
	// UnityEngine.UI.LayoutElement SRF.UI.DragHandle::TargetLayoutElement
	LayoutElement_t2808691390 * ___TargetLayoutElement_8;
	// UnityEngine.RectTransform SRF.UI.DragHandle::TargetRectTransform
	RectTransform_t3349966182 * ___TargetRectTransform_9;

public:
	inline static int32_t get_offset_of__canvasScaler_2() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ____canvasScaler_2)); }
	inline CanvasScaler_t2574720772 * get__canvasScaler_2() const { return ____canvasScaler_2; }
	inline CanvasScaler_t2574720772 ** get_address_of__canvasScaler_2() { return &____canvasScaler_2; }
	inline void set__canvasScaler_2(CanvasScaler_t2574720772 * value)
	{
		____canvasScaler_2 = value;
		Il2CppCodeGenWriteBarrier(&____canvasScaler_2, value);
	}

	inline static int32_t get_offset_of__delta_3() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ____delta_3)); }
	inline float get__delta_3() const { return ____delta_3; }
	inline float* get_address_of__delta_3() { return &____delta_3; }
	inline void set__delta_3(float value)
	{
		____delta_3 = value;
	}

	inline static int32_t get_offset_of__startValue_4() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ____startValue_4)); }
	inline float get__startValue_4() const { return ____startValue_4; }
	inline float* get_address_of__startValue_4() { return &____startValue_4; }
	inline void set__startValue_4(float value)
	{
		____startValue_4 = value;
	}

	inline static int32_t get_offset_of_Axis_5() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ___Axis_5)); }
	inline int32_t get_Axis_5() const { return ___Axis_5; }
	inline int32_t* get_address_of_Axis_5() { return &___Axis_5; }
	inline void set_Axis_5(int32_t value)
	{
		___Axis_5 = value;
	}

	inline static int32_t get_offset_of_Invert_6() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ___Invert_6)); }
	inline bool get_Invert_6() const { return ___Invert_6; }
	inline bool* get_address_of_Invert_6() { return &___Invert_6; }
	inline void set_Invert_6(bool value)
	{
		___Invert_6 = value;
	}

	inline static int32_t get_offset_of_MaxSize_7() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ___MaxSize_7)); }
	inline float get_MaxSize_7() const { return ___MaxSize_7; }
	inline float* get_address_of_MaxSize_7() { return &___MaxSize_7; }
	inline void set_MaxSize_7(float value)
	{
		___MaxSize_7 = value;
	}

	inline static int32_t get_offset_of_TargetLayoutElement_8() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ___TargetLayoutElement_8)); }
	inline LayoutElement_t2808691390 * get_TargetLayoutElement_8() const { return ___TargetLayoutElement_8; }
	inline LayoutElement_t2808691390 ** get_address_of_TargetLayoutElement_8() { return &___TargetLayoutElement_8; }
	inline void set_TargetLayoutElement_8(LayoutElement_t2808691390 * value)
	{
		___TargetLayoutElement_8 = value;
		Il2CppCodeGenWriteBarrier(&___TargetLayoutElement_8, value);
	}

	inline static int32_t get_offset_of_TargetRectTransform_9() { return static_cast<int32_t>(offsetof(DragHandle_t2261255472, ___TargetRectTransform_9)); }
	inline RectTransform_t3349966182 * get_TargetRectTransform_9() const { return ___TargetRectTransform_9; }
	inline RectTransform_t3349966182 ** get_address_of_TargetRectTransform_9() { return &___TargetRectTransform_9; }
	inline void set_TargetRectTransform_9(RectTransform_t3349966182 * value)
	{
		___TargetRectTransform_9 = value;
		Il2CppCodeGenWriteBarrier(&___TargetRectTransform_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
