﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<RemoveCamera>c__AnonStorey3
struct  U3CRemoveCameraU3Ec__AnonStorey3_t3139311996  : public Il2CppObject
{
public:
	// UnityEngine.Camera HedgehogTeam.EasyTouch.EasyTouch/<RemoveCamera>c__AnonStorey3::cam
	Camera_t189460977 * ___cam_0;

public:
	inline static int32_t get_offset_of_cam_0() { return static_cast<int32_t>(offsetof(U3CRemoveCameraU3Ec__AnonStorey3_t3139311996, ___cam_0)); }
	inline Camera_t189460977 * get_cam_0() const { return ___cam_0; }
	inline Camera_t189460977 ** get_address_of_cam_0() { return &___cam_0; }
	inline void set_cam_0(Camera_t189460977 * value)
	{
		___cam_0 = value;
		Il2CppCodeGenWriteBarrier(&___cam_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
