﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_UI_ResponsiveBase1813519265.h"

// SRF.UI.ResponsiveEnable/Entry[]
struct EntryU5BU5D_t3246429790;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ResponsiveEnable
struct  ResponsiveEnable_t2173267019  : public ResponsiveBase_t1813519265
{
public:
	// SRF.UI.ResponsiveEnable/Entry[] SRF.UI.ResponsiveEnable::Entries
	EntryU5BU5D_t3246429790* ___Entries_9;

public:
	inline static int32_t get_offset_of_Entries_9() { return static_cast<int32_t>(offsetof(ResponsiveEnable_t2173267019, ___Entries_9)); }
	inline EntryU5BU5D_t3246429790* get_Entries_9() const { return ___Entries_9; }
	inline EntryU5BU5D_t3246429790** get_address_of_Entries_9() { return &___Entries_9; }
	inline void set_Entries_9(EntryU5BU5D_t3246429790* value)
	{
		___Entries_9 = value;
		Il2CppCodeGenWriteBarrier(&___Entries_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
