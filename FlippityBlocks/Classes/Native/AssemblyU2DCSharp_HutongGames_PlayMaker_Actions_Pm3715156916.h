﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtDeSpawnAll
struct  PmtDeSpawnAll_t3715156916  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtDeSpawnAll::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtDeSpawnAll::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_12;
	// PathologicalGames.SpawnPool HutongGames.PlayMaker.Actions.PmtDeSpawnAll::_pool
	SpawnPool_t2419717525 * ____pool_13;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtDeSpawnAll_t3715156916, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_failureEvent_12() { return static_cast<int32_t>(offsetof(PmtDeSpawnAll_t3715156916, ___failureEvent_12)); }
	inline FsmEvent_t1258573736 * get_failureEvent_12() const { return ___failureEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_12() { return &___failureEvent_12; }
	inline void set_failureEvent_12(FsmEvent_t1258573736 * value)
	{
		___failureEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_12, value);
	}

	inline static int32_t get_offset_of__pool_13() { return static_cast<int32_t>(offsetof(PmtDeSpawnAll_t3715156916, ____pool_13)); }
	inline SpawnPool_t2419717525 * get__pool_13() const { return ____pool_13; }
	inline SpawnPool_t2419717525 ** get_address_of__pool_13() { return &____pool_13; }
	inline void set__pool_13(SpawnPool_t2419717525 * value)
	{
		____pool_13 = value;
		Il2CppCodeGenWriteBarrier(&____pool_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
