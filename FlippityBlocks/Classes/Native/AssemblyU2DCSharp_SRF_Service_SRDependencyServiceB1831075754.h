﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// SRF.Service.IAsyncService
struct IAsyncService_t2895191406;
// SRF.Service.SRDependencyServiceBase`1<System.Object>
struct SRDependencyServiceBase_1_t1215971031;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0<System.Object>
struct  U3CLoadDependenciesU3Ec__Iterator0_t1831075754  : public Il2CppObject
{
public:
	// System.Type[] SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::$locvar0
	TypeU5BU5D_t1664964607* ___U24locvar0_0;
	// System.Int32 SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Type SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::<d>__1
	Type_t * ___U3CdU3E__1_2;
	// System.Boolean SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::<hasService>__2
	bool ___U3ChasServiceU3E__2_3;
	// System.Object SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::<service>__2
	Il2CppObject * ___U3CserviceU3E__2_4;
	// SRF.Service.IAsyncService SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::<a>__2
	Il2CppObject * ___U3CaU3E__2_5;
	// SRF.Service.SRDependencyServiceBase`1<T> SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::$this
	SRDependencyServiceBase_1_t1215971031 * ___U24this_6;
	// System.Object SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::$current
	Il2CppObject * ___U24current_7;
	// System.Boolean SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 SRF.Service.SRDependencyServiceBase`1/<LoadDependencies>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U24locvar0_0)); }
	inline TypeU5BU5D_t1664964607* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline TypeU5BU5D_t1664964607** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(TypeU5BU5D_t1664964607* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_0, value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U3CdU3E__1_2)); }
	inline Type_t * get_U3CdU3E__1_2() const { return ___U3CdU3E__1_2; }
	inline Type_t ** get_address_of_U3CdU3E__1_2() { return &___U3CdU3E__1_2; }
	inline void set_U3CdU3E__1_2(Type_t * value)
	{
		___U3CdU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3ChasServiceU3E__2_3() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U3ChasServiceU3E__2_3)); }
	inline bool get_U3ChasServiceU3E__2_3() const { return ___U3ChasServiceU3E__2_3; }
	inline bool* get_address_of_U3ChasServiceU3E__2_3() { return &___U3ChasServiceU3E__2_3; }
	inline void set_U3ChasServiceU3E__2_3(bool value)
	{
		___U3ChasServiceU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U3CserviceU3E__2_4() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U3CserviceU3E__2_4)); }
	inline Il2CppObject * get_U3CserviceU3E__2_4() const { return ___U3CserviceU3E__2_4; }
	inline Il2CppObject ** get_address_of_U3CserviceU3E__2_4() { return &___U3CserviceU3E__2_4; }
	inline void set_U3CserviceU3E__2_4(Il2CppObject * value)
	{
		___U3CserviceU3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CserviceU3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CaU3E__2_5() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U3CaU3E__2_5)); }
	inline Il2CppObject * get_U3CaU3E__2_5() const { return ___U3CaU3E__2_5; }
	inline Il2CppObject ** get_address_of_U3CaU3E__2_5() { return &___U3CaU3E__2_5; }
	inline void set_U3CaU3E__2_5(Il2CppObject * value)
	{
		___U3CaU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaU3E__2_5, value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U24this_6)); }
	inline SRDependencyServiceBase_1_t1215971031 * get_U24this_6() const { return ___U24this_6; }
	inline SRDependencyServiceBase_1_t1215971031 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(SRDependencyServiceBase_1_t1215971031 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_6, value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CLoadDependenciesU3Ec__Iterator0_t1831075754, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
