﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture
struct  EasyTouchGetCurrentGesture_t3122094385  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::fingerIndex
	FsmInt_t1273009179 * ___fingerIndex_11;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::touchCount
	FsmInt_t1273009179 * ___touchCount_12;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::startPosition
	FsmVector3_t3996534004 * ___startPosition_13;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::position
	FsmVector3_t3996534004 * ___position_14;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::deltaPosition
	FsmVector3_t3996534004 * ___deltaPosition_15;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::deltaPositionX
	FsmFloat_t937133978 * ___deltaPositionX_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::deltaPositionY
	FsmFloat_t937133978 * ___deltaPositionY_17;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::actionTime
	FsmFloat_t937133978 * ___actionTime_18;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::deltaTime
	FsmFloat_t937133978 * ___deltaTime_19;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::pickedGameObject
	FsmGameObject_t3097142863 * ___pickedGameObject_20;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::pickedUIElement
	FsmGameObject_t3097142863 * ___pickedUIElement_21;
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::swipe
	FsmEnum_t2808516103 * ___swipe_22;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::swipeLength
	FsmFloat_t937133978 * ___swipeLength_23;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::swipeVector
	FsmVector3_t3996534004 * ___swipeVector_24;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::deltaPinch
	FsmFloat_t937133978 * ___deltaPinch_25;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::twistAngle
	FsmFloat_t937133978 * ___twistAngle_26;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::isOverGUI
	FsmBool_t664485696 * ___isOverGUI_27;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::twoFingerDistance
	FsmFloat_t937133978 * ___twoFingerDistance_28;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchGetCurrentGesture::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_29;

public:
	inline static int32_t get_offset_of_fingerIndex_11() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___fingerIndex_11)); }
	inline FsmInt_t1273009179 * get_fingerIndex_11() const { return ___fingerIndex_11; }
	inline FsmInt_t1273009179 ** get_address_of_fingerIndex_11() { return &___fingerIndex_11; }
	inline void set_fingerIndex_11(FsmInt_t1273009179 * value)
	{
		___fingerIndex_11 = value;
		Il2CppCodeGenWriteBarrier(&___fingerIndex_11, value);
	}

	inline static int32_t get_offset_of_touchCount_12() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___touchCount_12)); }
	inline FsmInt_t1273009179 * get_touchCount_12() const { return ___touchCount_12; }
	inline FsmInt_t1273009179 ** get_address_of_touchCount_12() { return &___touchCount_12; }
	inline void set_touchCount_12(FsmInt_t1273009179 * value)
	{
		___touchCount_12 = value;
		Il2CppCodeGenWriteBarrier(&___touchCount_12, value);
	}

	inline static int32_t get_offset_of_startPosition_13() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___startPosition_13)); }
	inline FsmVector3_t3996534004 * get_startPosition_13() const { return ___startPosition_13; }
	inline FsmVector3_t3996534004 ** get_address_of_startPosition_13() { return &___startPosition_13; }
	inline void set_startPosition_13(FsmVector3_t3996534004 * value)
	{
		___startPosition_13 = value;
		Il2CppCodeGenWriteBarrier(&___startPosition_13, value);
	}

	inline static int32_t get_offset_of_position_14() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___position_14)); }
	inline FsmVector3_t3996534004 * get_position_14() const { return ___position_14; }
	inline FsmVector3_t3996534004 ** get_address_of_position_14() { return &___position_14; }
	inline void set_position_14(FsmVector3_t3996534004 * value)
	{
		___position_14 = value;
		Il2CppCodeGenWriteBarrier(&___position_14, value);
	}

	inline static int32_t get_offset_of_deltaPosition_15() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___deltaPosition_15)); }
	inline FsmVector3_t3996534004 * get_deltaPosition_15() const { return ___deltaPosition_15; }
	inline FsmVector3_t3996534004 ** get_address_of_deltaPosition_15() { return &___deltaPosition_15; }
	inline void set_deltaPosition_15(FsmVector3_t3996534004 * value)
	{
		___deltaPosition_15 = value;
		Il2CppCodeGenWriteBarrier(&___deltaPosition_15, value);
	}

	inline static int32_t get_offset_of_deltaPositionX_16() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___deltaPositionX_16)); }
	inline FsmFloat_t937133978 * get_deltaPositionX_16() const { return ___deltaPositionX_16; }
	inline FsmFloat_t937133978 ** get_address_of_deltaPositionX_16() { return &___deltaPositionX_16; }
	inline void set_deltaPositionX_16(FsmFloat_t937133978 * value)
	{
		___deltaPositionX_16 = value;
		Il2CppCodeGenWriteBarrier(&___deltaPositionX_16, value);
	}

	inline static int32_t get_offset_of_deltaPositionY_17() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___deltaPositionY_17)); }
	inline FsmFloat_t937133978 * get_deltaPositionY_17() const { return ___deltaPositionY_17; }
	inline FsmFloat_t937133978 ** get_address_of_deltaPositionY_17() { return &___deltaPositionY_17; }
	inline void set_deltaPositionY_17(FsmFloat_t937133978 * value)
	{
		___deltaPositionY_17 = value;
		Il2CppCodeGenWriteBarrier(&___deltaPositionY_17, value);
	}

	inline static int32_t get_offset_of_actionTime_18() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___actionTime_18)); }
	inline FsmFloat_t937133978 * get_actionTime_18() const { return ___actionTime_18; }
	inline FsmFloat_t937133978 ** get_address_of_actionTime_18() { return &___actionTime_18; }
	inline void set_actionTime_18(FsmFloat_t937133978 * value)
	{
		___actionTime_18 = value;
		Il2CppCodeGenWriteBarrier(&___actionTime_18, value);
	}

	inline static int32_t get_offset_of_deltaTime_19() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___deltaTime_19)); }
	inline FsmFloat_t937133978 * get_deltaTime_19() const { return ___deltaTime_19; }
	inline FsmFloat_t937133978 ** get_address_of_deltaTime_19() { return &___deltaTime_19; }
	inline void set_deltaTime_19(FsmFloat_t937133978 * value)
	{
		___deltaTime_19 = value;
		Il2CppCodeGenWriteBarrier(&___deltaTime_19, value);
	}

	inline static int32_t get_offset_of_pickedGameObject_20() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___pickedGameObject_20)); }
	inline FsmGameObject_t3097142863 * get_pickedGameObject_20() const { return ___pickedGameObject_20; }
	inline FsmGameObject_t3097142863 ** get_address_of_pickedGameObject_20() { return &___pickedGameObject_20; }
	inline void set_pickedGameObject_20(FsmGameObject_t3097142863 * value)
	{
		___pickedGameObject_20 = value;
		Il2CppCodeGenWriteBarrier(&___pickedGameObject_20, value);
	}

	inline static int32_t get_offset_of_pickedUIElement_21() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___pickedUIElement_21)); }
	inline FsmGameObject_t3097142863 * get_pickedUIElement_21() const { return ___pickedUIElement_21; }
	inline FsmGameObject_t3097142863 ** get_address_of_pickedUIElement_21() { return &___pickedUIElement_21; }
	inline void set_pickedUIElement_21(FsmGameObject_t3097142863 * value)
	{
		___pickedUIElement_21 = value;
		Il2CppCodeGenWriteBarrier(&___pickedUIElement_21, value);
	}

	inline static int32_t get_offset_of_swipe_22() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___swipe_22)); }
	inline FsmEnum_t2808516103 * get_swipe_22() const { return ___swipe_22; }
	inline FsmEnum_t2808516103 ** get_address_of_swipe_22() { return &___swipe_22; }
	inline void set_swipe_22(FsmEnum_t2808516103 * value)
	{
		___swipe_22 = value;
		Il2CppCodeGenWriteBarrier(&___swipe_22, value);
	}

	inline static int32_t get_offset_of_swipeLength_23() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___swipeLength_23)); }
	inline FsmFloat_t937133978 * get_swipeLength_23() const { return ___swipeLength_23; }
	inline FsmFloat_t937133978 ** get_address_of_swipeLength_23() { return &___swipeLength_23; }
	inline void set_swipeLength_23(FsmFloat_t937133978 * value)
	{
		___swipeLength_23 = value;
		Il2CppCodeGenWriteBarrier(&___swipeLength_23, value);
	}

	inline static int32_t get_offset_of_swipeVector_24() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___swipeVector_24)); }
	inline FsmVector3_t3996534004 * get_swipeVector_24() const { return ___swipeVector_24; }
	inline FsmVector3_t3996534004 ** get_address_of_swipeVector_24() { return &___swipeVector_24; }
	inline void set_swipeVector_24(FsmVector3_t3996534004 * value)
	{
		___swipeVector_24 = value;
		Il2CppCodeGenWriteBarrier(&___swipeVector_24, value);
	}

	inline static int32_t get_offset_of_deltaPinch_25() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___deltaPinch_25)); }
	inline FsmFloat_t937133978 * get_deltaPinch_25() const { return ___deltaPinch_25; }
	inline FsmFloat_t937133978 ** get_address_of_deltaPinch_25() { return &___deltaPinch_25; }
	inline void set_deltaPinch_25(FsmFloat_t937133978 * value)
	{
		___deltaPinch_25 = value;
		Il2CppCodeGenWriteBarrier(&___deltaPinch_25, value);
	}

	inline static int32_t get_offset_of_twistAngle_26() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___twistAngle_26)); }
	inline FsmFloat_t937133978 * get_twistAngle_26() const { return ___twistAngle_26; }
	inline FsmFloat_t937133978 ** get_address_of_twistAngle_26() { return &___twistAngle_26; }
	inline void set_twistAngle_26(FsmFloat_t937133978 * value)
	{
		___twistAngle_26 = value;
		Il2CppCodeGenWriteBarrier(&___twistAngle_26, value);
	}

	inline static int32_t get_offset_of_isOverGUI_27() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___isOverGUI_27)); }
	inline FsmBool_t664485696 * get_isOverGUI_27() const { return ___isOverGUI_27; }
	inline FsmBool_t664485696 ** get_address_of_isOverGUI_27() { return &___isOverGUI_27; }
	inline void set_isOverGUI_27(FsmBool_t664485696 * value)
	{
		___isOverGUI_27 = value;
		Il2CppCodeGenWriteBarrier(&___isOverGUI_27, value);
	}

	inline static int32_t get_offset_of_twoFingerDistance_28() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___twoFingerDistance_28)); }
	inline FsmFloat_t937133978 * get_twoFingerDistance_28() const { return ___twoFingerDistance_28; }
	inline FsmFloat_t937133978 ** get_address_of_twoFingerDistance_28() { return &___twoFingerDistance_28; }
	inline void set_twoFingerDistance_28(FsmFloat_t937133978 * value)
	{
		___twoFingerDistance_28 = value;
		Il2CppCodeGenWriteBarrier(&___twoFingerDistance_28, value);
	}

	inline static int32_t get_offset_of_proxy_29() { return static_cast<int32_t>(offsetof(EasyTouchGetCurrentGesture_t3122094385, ___proxy_29)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_29() const { return ___proxy_29; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_29() { return &___proxy_29; }
	inline void set_proxy_29(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_29 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
