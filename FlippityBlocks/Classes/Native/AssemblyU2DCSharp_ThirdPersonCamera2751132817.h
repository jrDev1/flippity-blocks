﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Transform
struct Transform_t3275118058;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdPersonCamera
struct  ThirdPersonCamera_t2751132817  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ThirdPersonCamera::distanceAway
	float ___distanceAway_2;
	// System.Single ThirdPersonCamera::distanceUp
	float ___distanceUp_3;
	// System.Single ThirdPersonCamera::smooth
	float ___smooth_4;
	// UnityEngine.GameObject ThirdPersonCamera::hovercraft
	GameObject_t1756533147 * ___hovercraft_5;
	// UnityEngine.Vector3 ThirdPersonCamera::targetPosition
	Vector3_t2243707580  ___targetPosition_6;
	// UnityEngine.Transform ThirdPersonCamera::follow
	Transform_t3275118058 * ___follow_7;

public:
	inline static int32_t get_offset_of_distanceAway_2() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2751132817, ___distanceAway_2)); }
	inline float get_distanceAway_2() const { return ___distanceAway_2; }
	inline float* get_address_of_distanceAway_2() { return &___distanceAway_2; }
	inline void set_distanceAway_2(float value)
	{
		___distanceAway_2 = value;
	}

	inline static int32_t get_offset_of_distanceUp_3() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2751132817, ___distanceUp_3)); }
	inline float get_distanceUp_3() const { return ___distanceUp_3; }
	inline float* get_address_of_distanceUp_3() { return &___distanceUp_3; }
	inline void set_distanceUp_3(float value)
	{
		___distanceUp_3 = value;
	}

	inline static int32_t get_offset_of_smooth_4() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2751132817, ___smooth_4)); }
	inline float get_smooth_4() const { return ___smooth_4; }
	inline float* get_address_of_smooth_4() { return &___smooth_4; }
	inline void set_smooth_4(float value)
	{
		___smooth_4 = value;
	}

	inline static int32_t get_offset_of_hovercraft_5() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2751132817, ___hovercraft_5)); }
	inline GameObject_t1756533147 * get_hovercraft_5() const { return ___hovercraft_5; }
	inline GameObject_t1756533147 ** get_address_of_hovercraft_5() { return &___hovercraft_5; }
	inline void set_hovercraft_5(GameObject_t1756533147 * value)
	{
		___hovercraft_5 = value;
		Il2CppCodeGenWriteBarrier(&___hovercraft_5, value);
	}

	inline static int32_t get_offset_of_targetPosition_6() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2751132817, ___targetPosition_6)); }
	inline Vector3_t2243707580  get_targetPosition_6() const { return ___targetPosition_6; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_6() { return &___targetPosition_6; }
	inline void set_targetPosition_6(Vector3_t2243707580  value)
	{
		___targetPosition_6 = value;
	}

	inline static int32_t get_offset_of_follow_7() { return static_cast<int32_t>(offsetof(ThirdPersonCamera_t2751132817, ___follow_7)); }
	inline Transform_t3275118058 * get_follow_7() const { return ___follow_7; }
	inline Transform_t3275118058 ** get_address_of_follow_7() { return &___follow_7; }
	inline void set_follow_7(Transform_t3275118058 * value)
	{
		___follow_7 = value;
		Il2CppCodeGenWriteBarrier(&___follow_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
