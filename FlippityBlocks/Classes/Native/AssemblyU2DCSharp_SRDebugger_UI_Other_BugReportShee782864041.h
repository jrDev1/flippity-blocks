﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.Services.IBugReportService
struct IBugReportService_t276633438;
// SRDebugger.Services.BugReport
struct BugReport_t3548726662;
// SRDebugger.UI.Other.BugReportSheetController
struct BugReportSheetController_t1864182511;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0
struct  U3CSubmitCoU3Ec__Iterator0_t782864041  : public Il2CppObject
{
public:
	// SRDebugger.Services.IBugReportService SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0::<s>__0
	Il2CppObject * ___U3CsU3E__0_0;
	// SRDebugger.Services.BugReport SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0::<r>__0
	BugReport_t3548726662 * ___U3CrU3E__0_1;
	// SRDebugger.UI.Other.BugReportSheetController SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0::$this
	BugReportSheetController_t1864182511 * ___U24this_2;
	// System.Object SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 SRDebugger.UI.Other.BugReportSheetController/<SubmitCo>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSubmitCoU3Ec__Iterator0_t782864041, ___U3CsU3E__0_0)); }
	inline Il2CppObject * get_U3CsU3E__0_0() const { return ___U3CsU3E__0_0; }
	inline Il2CppObject ** get_address_of_U3CsU3E__0_0() { return &___U3CsU3E__0_0; }
	inline void set_U3CsU3E__0_0(Il2CppObject * value)
	{
		___U3CsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CsU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CrU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSubmitCoU3Ec__Iterator0_t782864041, ___U3CrU3E__0_1)); }
	inline BugReport_t3548726662 * get_U3CrU3E__0_1() const { return ___U3CrU3E__0_1; }
	inline BugReport_t3548726662 ** get_address_of_U3CrU3E__0_1() { return &___U3CrU3E__0_1; }
	inline void set_U3CrU3E__0_1(BugReport_t3548726662 * value)
	{
		___U3CrU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CrU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSubmitCoU3Ec__Iterator0_t782864041, ___U24this_2)); }
	inline BugReportSheetController_t1864182511 * get_U24this_2() const { return ___U24this_2; }
	inline BugReportSheetController_t1864182511 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(BugReportSheetController_t1864182511 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSubmitCoU3Ec__Iterator0_t782864041, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSubmitCoU3Ec__Iterator0_t782864041, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSubmitCoU3Ec__Iterator0_t782864041, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
