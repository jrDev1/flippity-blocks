﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"

// SRF.Service.SRServiceManager
struct SRServiceManager_t2267823955;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Components.SRAutoSingleton`1<SRF.Service.SRServiceManager>
struct  SRAutoSingleton_1_t4224086282  : public SRMonoBehaviour_t2352136145
{
public:

public:
};

struct SRAutoSingleton_1_t4224086282_StaticFields
{
public:
	// T SRF.Components.SRAutoSingleton`1::_instance
	SRServiceManager_t2267823955 * ____instance_8;

public:
	inline static int32_t get_offset_of__instance_8() { return static_cast<int32_t>(offsetof(SRAutoSingleton_1_t4224086282_StaticFields, ____instance_8)); }
	inline SRServiceManager_t2267823955 * get__instance_8() const { return ____instance_8; }
	inline SRServiceManager_t2267823955 ** get_address_of__instance_8() { return &____instance_8; }
	inline void set__instance_8(SRServiceManager_t2267823955 * value)
	{
		____instance_8 = value;
		Il2CppCodeGenWriteBarrier(&____instance_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
