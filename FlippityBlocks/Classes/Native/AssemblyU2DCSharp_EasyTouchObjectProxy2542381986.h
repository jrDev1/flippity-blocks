﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EasyTouchObjectProxy
struct  EasyTouchObjectProxy_t2542381986  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean EasyTouchObjectProxy::allowSceneEvent
	bool ___allowSceneEvent_2;
	// PlayMakerFSM EasyTouchObjectProxy::cachedFsm
	PlayMakerFSM_t437737208 * ___cachedFsm_3;
	// HedgehogTeam.EasyTouch.Gesture EasyTouchObjectProxy::currentGesture
	Gesture_t367995397 * ___currentGesture_4;

public:
	inline static int32_t get_offset_of_allowSceneEvent_2() { return static_cast<int32_t>(offsetof(EasyTouchObjectProxy_t2542381986, ___allowSceneEvent_2)); }
	inline bool get_allowSceneEvent_2() const { return ___allowSceneEvent_2; }
	inline bool* get_address_of_allowSceneEvent_2() { return &___allowSceneEvent_2; }
	inline void set_allowSceneEvent_2(bool value)
	{
		___allowSceneEvent_2 = value;
	}

	inline static int32_t get_offset_of_cachedFsm_3() { return static_cast<int32_t>(offsetof(EasyTouchObjectProxy_t2542381986, ___cachedFsm_3)); }
	inline PlayMakerFSM_t437737208 * get_cachedFsm_3() const { return ___cachedFsm_3; }
	inline PlayMakerFSM_t437737208 ** get_address_of_cachedFsm_3() { return &___cachedFsm_3; }
	inline void set_cachedFsm_3(PlayMakerFSM_t437737208 * value)
	{
		___cachedFsm_3 = value;
		Il2CppCodeGenWriteBarrier(&___cachedFsm_3, value);
	}

	inline static int32_t get_offset_of_currentGesture_4() { return static_cast<int32_t>(offsetof(EasyTouchObjectProxy_t2542381986, ___currentGesture_4)); }
	inline Gesture_t367995397 * get_currentGesture_4() const { return ___currentGesture_4; }
	inline Gesture_t367995397 ** get_address_of_currentGesture_4() { return &___currentGesture_4; }
	inline void set_currentGesture_4(Gesture_t367995397 * value)
	{
		___currentGesture_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentGesture_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
