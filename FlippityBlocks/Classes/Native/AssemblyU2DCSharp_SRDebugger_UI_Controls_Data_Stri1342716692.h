﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_DataBound3384854171.h"

// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.StringControl
struct  StringControl_t1342716692  : public DataBoundControl_t3384854171
{
public:
	// UnityEngine.UI.InputField SRDebugger.UI.Controls.Data.StringControl::InputField
	InputField_t1631627530 * ___InputField_17;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.StringControl::Title
	Text_t356221433 * ___Title_18;

public:
	inline static int32_t get_offset_of_InputField_17() { return static_cast<int32_t>(offsetof(StringControl_t1342716692, ___InputField_17)); }
	inline InputField_t1631627530 * get_InputField_17() const { return ___InputField_17; }
	inline InputField_t1631627530 ** get_address_of_InputField_17() { return &___InputField_17; }
	inline void set_InputField_17(InputField_t1631627530 * value)
	{
		___InputField_17 = value;
		Il2CppCodeGenWriteBarrier(&___InputField_17, value);
	}

	inline static int32_t get_offset_of_Title_18() { return static_cast<int32_t>(offsetof(StringControl_t1342716692, ___Title_18)); }
	inline Text_t356221433 * get_Title_18() const { return ___Title_18; }
	inline Text_t356221433 ** get_address_of_Title_18() { return &___Title_18; }
	inline void set_Title_18(Text_t356221433 * value)
	{
		___Title_18 = value;
		Il2CppCodeGenWriteBarrier(&___Title_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
