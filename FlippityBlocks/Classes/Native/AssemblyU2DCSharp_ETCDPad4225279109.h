﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ETCBase118528977.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// ETCDPad/OnMoveStartHandler
struct OnMoveStartHandler_t4250282484;
// ETCDPad/OnMoveHandler
struct OnMoveHandler_t3373990688;
// ETCDPad/OnMoveSpeedHandler
struct OnMoveSpeedHandler_t684365173;
// ETCDPad/OnMoveEndHandler
struct OnMoveEndHandler_t630021049;
// ETCDPad/OnTouchStartHandler
struct OnTouchStartHandler_t1271022964;
// ETCDPad/OnTouchUPHandler
struct OnTouchUPHandler_t595775759;
// ETCDPad/OnDownUpHandler
struct OnDownUpHandler_t864821988;
// ETCDPad/OnDownDownHandler
struct OnDownDownHandler_t715706099;
// ETCDPad/OnDownLeftHandler
struct OnDownLeftHandler_t1079412668;
// ETCDPad/OnDownRightHandler
struct OnDownRightHandler_t41790613;
// ETCAxis
struct ETCAxis_t3445562479;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCDPad
struct  ETCDPad_t4225279109  : public ETCBase_t118528977
{
public:
	// ETCDPad/OnMoveStartHandler ETCDPad::onMoveStart
	OnMoveStartHandler_t4250282484 * ___onMoveStart_48;
	// ETCDPad/OnMoveHandler ETCDPad::onMove
	OnMoveHandler_t3373990688 * ___onMove_49;
	// ETCDPad/OnMoveSpeedHandler ETCDPad::onMoveSpeed
	OnMoveSpeedHandler_t684365173 * ___onMoveSpeed_50;
	// ETCDPad/OnMoveEndHandler ETCDPad::onMoveEnd
	OnMoveEndHandler_t630021049 * ___onMoveEnd_51;
	// ETCDPad/OnTouchStartHandler ETCDPad::onTouchStart
	OnTouchStartHandler_t1271022964 * ___onTouchStart_52;
	// ETCDPad/OnTouchUPHandler ETCDPad::onTouchUp
	OnTouchUPHandler_t595775759 * ___onTouchUp_53;
	// ETCDPad/OnDownUpHandler ETCDPad::OnDownUp
	OnDownUpHandler_t864821988 * ___OnDownUp_54;
	// ETCDPad/OnDownDownHandler ETCDPad::OnDownDown
	OnDownDownHandler_t715706099 * ___OnDownDown_55;
	// ETCDPad/OnDownLeftHandler ETCDPad::OnDownLeft
	OnDownLeftHandler_t1079412668 * ___OnDownLeft_56;
	// ETCDPad/OnDownRightHandler ETCDPad::OnDownRight
	OnDownRightHandler_t41790613 * ___OnDownRight_57;
	// ETCDPad/OnDownUpHandler ETCDPad::OnPressUp
	OnDownUpHandler_t864821988 * ___OnPressUp_58;
	// ETCDPad/OnDownDownHandler ETCDPad::OnPressDown
	OnDownDownHandler_t715706099 * ___OnPressDown_59;
	// ETCDPad/OnDownLeftHandler ETCDPad::OnPressLeft
	OnDownLeftHandler_t1079412668 * ___OnPressLeft_60;
	// ETCDPad/OnDownRightHandler ETCDPad::OnPressRight
	OnDownRightHandler_t41790613 * ___OnPressRight_61;
	// ETCAxis ETCDPad::axisX
	ETCAxis_t3445562479 * ___axisX_62;
	// ETCAxis ETCDPad::axisY
	ETCAxis_t3445562479 * ___axisY_63;
	// UnityEngine.Sprite ETCDPad::normalSprite
	Sprite_t309593783 * ___normalSprite_64;
	// UnityEngine.Color ETCDPad::normalColor
	Color_t2020392075  ___normalColor_65;
	// UnityEngine.Sprite ETCDPad::pressedSprite
	Sprite_t309593783 * ___pressedSprite_66;
	// UnityEngine.Color ETCDPad::pressedColor
	Color_t2020392075  ___pressedColor_67;
	// UnityEngine.Vector2 ETCDPad::tmpAxis
	Vector2_t2243707579  ___tmpAxis_68;
	// UnityEngine.Vector2 ETCDPad::OldTmpAxis
	Vector2_t2243707579  ___OldTmpAxis_69;
	// System.Boolean ETCDPad::isOnTouch
	bool ___isOnTouch_70;
	// UnityEngine.UI.Image ETCDPad::cachedImage
	Image_t2042527209 * ___cachedImage_71;
	// System.Single ETCDPad::buttonSizeCoef
	float ___buttonSizeCoef_72;

public:
	inline static int32_t get_offset_of_onMoveStart_48() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___onMoveStart_48)); }
	inline OnMoveStartHandler_t4250282484 * get_onMoveStart_48() const { return ___onMoveStart_48; }
	inline OnMoveStartHandler_t4250282484 ** get_address_of_onMoveStart_48() { return &___onMoveStart_48; }
	inline void set_onMoveStart_48(OnMoveStartHandler_t4250282484 * value)
	{
		___onMoveStart_48 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveStart_48, value);
	}

	inline static int32_t get_offset_of_onMove_49() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___onMove_49)); }
	inline OnMoveHandler_t3373990688 * get_onMove_49() const { return ___onMove_49; }
	inline OnMoveHandler_t3373990688 ** get_address_of_onMove_49() { return &___onMove_49; }
	inline void set_onMove_49(OnMoveHandler_t3373990688 * value)
	{
		___onMove_49 = value;
		Il2CppCodeGenWriteBarrier(&___onMove_49, value);
	}

	inline static int32_t get_offset_of_onMoveSpeed_50() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___onMoveSpeed_50)); }
	inline OnMoveSpeedHandler_t684365173 * get_onMoveSpeed_50() const { return ___onMoveSpeed_50; }
	inline OnMoveSpeedHandler_t684365173 ** get_address_of_onMoveSpeed_50() { return &___onMoveSpeed_50; }
	inline void set_onMoveSpeed_50(OnMoveSpeedHandler_t684365173 * value)
	{
		___onMoveSpeed_50 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveSpeed_50, value);
	}

	inline static int32_t get_offset_of_onMoveEnd_51() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___onMoveEnd_51)); }
	inline OnMoveEndHandler_t630021049 * get_onMoveEnd_51() const { return ___onMoveEnd_51; }
	inline OnMoveEndHandler_t630021049 ** get_address_of_onMoveEnd_51() { return &___onMoveEnd_51; }
	inline void set_onMoveEnd_51(OnMoveEndHandler_t630021049 * value)
	{
		___onMoveEnd_51 = value;
		Il2CppCodeGenWriteBarrier(&___onMoveEnd_51, value);
	}

	inline static int32_t get_offset_of_onTouchStart_52() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___onTouchStart_52)); }
	inline OnTouchStartHandler_t1271022964 * get_onTouchStart_52() const { return ___onTouchStart_52; }
	inline OnTouchStartHandler_t1271022964 ** get_address_of_onTouchStart_52() { return &___onTouchStart_52; }
	inline void set_onTouchStart_52(OnTouchStartHandler_t1271022964 * value)
	{
		___onTouchStart_52 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchStart_52, value);
	}

	inline static int32_t get_offset_of_onTouchUp_53() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___onTouchUp_53)); }
	inline OnTouchUPHandler_t595775759 * get_onTouchUp_53() const { return ___onTouchUp_53; }
	inline OnTouchUPHandler_t595775759 ** get_address_of_onTouchUp_53() { return &___onTouchUp_53; }
	inline void set_onTouchUp_53(OnTouchUPHandler_t595775759 * value)
	{
		___onTouchUp_53 = value;
		Il2CppCodeGenWriteBarrier(&___onTouchUp_53, value);
	}

	inline static int32_t get_offset_of_OnDownUp_54() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnDownUp_54)); }
	inline OnDownUpHandler_t864821988 * get_OnDownUp_54() const { return ___OnDownUp_54; }
	inline OnDownUpHandler_t864821988 ** get_address_of_OnDownUp_54() { return &___OnDownUp_54; }
	inline void set_OnDownUp_54(OnDownUpHandler_t864821988 * value)
	{
		___OnDownUp_54 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownUp_54, value);
	}

	inline static int32_t get_offset_of_OnDownDown_55() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnDownDown_55)); }
	inline OnDownDownHandler_t715706099 * get_OnDownDown_55() const { return ___OnDownDown_55; }
	inline OnDownDownHandler_t715706099 ** get_address_of_OnDownDown_55() { return &___OnDownDown_55; }
	inline void set_OnDownDown_55(OnDownDownHandler_t715706099 * value)
	{
		___OnDownDown_55 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownDown_55, value);
	}

	inline static int32_t get_offset_of_OnDownLeft_56() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnDownLeft_56)); }
	inline OnDownLeftHandler_t1079412668 * get_OnDownLeft_56() const { return ___OnDownLeft_56; }
	inline OnDownLeftHandler_t1079412668 ** get_address_of_OnDownLeft_56() { return &___OnDownLeft_56; }
	inline void set_OnDownLeft_56(OnDownLeftHandler_t1079412668 * value)
	{
		___OnDownLeft_56 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownLeft_56, value);
	}

	inline static int32_t get_offset_of_OnDownRight_57() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnDownRight_57)); }
	inline OnDownRightHandler_t41790613 * get_OnDownRight_57() const { return ___OnDownRight_57; }
	inline OnDownRightHandler_t41790613 ** get_address_of_OnDownRight_57() { return &___OnDownRight_57; }
	inline void set_OnDownRight_57(OnDownRightHandler_t41790613 * value)
	{
		___OnDownRight_57 = value;
		Il2CppCodeGenWriteBarrier(&___OnDownRight_57, value);
	}

	inline static int32_t get_offset_of_OnPressUp_58() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnPressUp_58)); }
	inline OnDownUpHandler_t864821988 * get_OnPressUp_58() const { return ___OnPressUp_58; }
	inline OnDownUpHandler_t864821988 ** get_address_of_OnPressUp_58() { return &___OnPressUp_58; }
	inline void set_OnPressUp_58(OnDownUpHandler_t864821988 * value)
	{
		___OnPressUp_58 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressUp_58, value);
	}

	inline static int32_t get_offset_of_OnPressDown_59() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnPressDown_59)); }
	inline OnDownDownHandler_t715706099 * get_OnPressDown_59() const { return ___OnPressDown_59; }
	inline OnDownDownHandler_t715706099 ** get_address_of_OnPressDown_59() { return &___OnPressDown_59; }
	inline void set_OnPressDown_59(OnDownDownHandler_t715706099 * value)
	{
		___OnPressDown_59 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressDown_59, value);
	}

	inline static int32_t get_offset_of_OnPressLeft_60() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnPressLeft_60)); }
	inline OnDownLeftHandler_t1079412668 * get_OnPressLeft_60() const { return ___OnPressLeft_60; }
	inline OnDownLeftHandler_t1079412668 ** get_address_of_OnPressLeft_60() { return &___OnPressLeft_60; }
	inline void set_OnPressLeft_60(OnDownLeftHandler_t1079412668 * value)
	{
		___OnPressLeft_60 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressLeft_60, value);
	}

	inline static int32_t get_offset_of_OnPressRight_61() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OnPressRight_61)); }
	inline OnDownRightHandler_t41790613 * get_OnPressRight_61() const { return ___OnPressRight_61; }
	inline OnDownRightHandler_t41790613 ** get_address_of_OnPressRight_61() { return &___OnPressRight_61; }
	inline void set_OnPressRight_61(OnDownRightHandler_t41790613 * value)
	{
		___OnPressRight_61 = value;
		Il2CppCodeGenWriteBarrier(&___OnPressRight_61, value);
	}

	inline static int32_t get_offset_of_axisX_62() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___axisX_62)); }
	inline ETCAxis_t3445562479 * get_axisX_62() const { return ___axisX_62; }
	inline ETCAxis_t3445562479 ** get_address_of_axisX_62() { return &___axisX_62; }
	inline void set_axisX_62(ETCAxis_t3445562479 * value)
	{
		___axisX_62 = value;
		Il2CppCodeGenWriteBarrier(&___axisX_62, value);
	}

	inline static int32_t get_offset_of_axisY_63() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___axisY_63)); }
	inline ETCAxis_t3445562479 * get_axisY_63() const { return ___axisY_63; }
	inline ETCAxis_t3445562479 ** get_address_of_axisY_63() { return &___axisY_63; }
	inline void set_axisY_63(ETCAxis_t3445562479 * value)
	{
		___axisY_63 = value;
		Il2CppCodeGenWriteBarrier(&___axisY_63, value);
	}

	inline static int32_t get_offset_of_normalSprite_64() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___normalSprite_64)); }
	inline Sprite_t309593783 * get_normalSprite_64() const { return ___normalSprite_64; }
	inline Sprite_t309593783 ** get_address_of_normalSprite_64() { return &___normalSprite_64; }
	inline void set_normalSprite_64(Sprite_t309593783 * value)
	{
		___normalSprite_64 = value;
		Il2CppCodeGenWriteBarrier(&___normalSprite_64, value);
	}

	inline static int32_t get_offset_of_normalColor_65() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___normalColor_65)); }
	inline Color_t2020392075  get_normalColor_65() const { return ___normalColor_65; }
	inline Color_t2020392075 * get_address_of_normalColor_65() { return &___normalColor_65; }
	inline void set_normalColor_65(Color_t2020392075  value)
	{
		___normalColor_65 = value;
	}

	inline static int32_t get_offset_of_pressedSprite_66() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___pressedSprite_66)); }
	inline Sprite_t309593783 * get_pressedSprite_66() const { return ___pressedSprite_66; }
	inline Sprite_t309593783 ** get_address_of_pressedSprite_66() { return &___pressedSprite_66; }
	inline void set_pressedSprite_66(Sprite_t309593783 * value)
	{
		___pressedSprite_66 = value;
		Il2CppCodeGenWriteBarrier(&___pressedSprite_66, value);
	}

	inline static int32_t get_offset_of_pressedColor_67() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___pressedColor_67)); }
	inline Color_t2020392075  get_pressedColor_67() const { return ___pressedColor_67; }
	inline Color_t2020392075 * get_address_of_pressedColor_67() { return &___pressedColor_67; }
	inline void set_pressedColor_67(Color_t2020392075  value)
	{
		___pressedColor_67 = value;
	}

	inline static int32_t get_offset_of_tmpAxis_68() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___tmpAxis_68)); }
	inline Vector2_t2243707579  get_tmpAxis_68() const { return ___tmpAxis_68; }
	inline Vector2_t2243707579 * get_address_of_tmpAxis_68() { return &___tmpAxis_68; }
	inline void set_tmpAxis_68(Vector2_t2243707579  value)
	{
		___tmpAxis_68 = value;
	}

	inline static int32_t get_offset_of_OldTmpAxis_69() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___OldTmpAxis_69)); }
	inline Vector2_t2243707579  get_OldTmpAxis_69() const { return ___OldTmpAxis_69; }
	inline Vector2_t2243707579 * get_address_of_OldTmpAxis_69() { return &___OldTmpAxis_69; }
	inline void set_OldTmpAxis_69(Vector2_t2243707579  value)
	{
		___OldTmpAxis_69 = value;
	}

	inline static int32_t get_offset_of_isOnTouch_70() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___isOnTouch_70)); }
	inline bool get_isOnTouch_70() const { return ___isOnTouch_70; }
	inline bool* get_address_of_isOnTouch_70() { return &___isOnTouch_70; }
	inline void set_isOnTouch_70(bool value)
	{
		___isOnTouch_70 = value;
	}

	inline static int32_t get_offset_of_cachedImage_71() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___cachedImage_71)); }
	inline Image_t2042527209 * get_cachedImage_71() const { return ___cachedImage_71; }
	inline Image_t2042527209 ** get_address_of_cachedImage_71() { return &___cachedImage_71; }
	inline void set_cachedImage_71(Image_t2042527209 * value)
	{
		___cachedImage_71 = value;
		Il2CppCodeGenWriteBarrier(&___cachedImage_71, value);
	}

	inline static int32_t get_offset_of_buttonSizeCoef_72() { return static_cast<int32_t>(offsetof(ETCDPad_t4225279109, ___buttonSizeCoef_72)); }
	inline float get_buttonSizeCoef_72() const { return ___buttonSizeCoef_72; }
	inline float* get_address_of_buttonSizeCoef_72() { return &___buttonSizeCoef_72; }
	inline void set_buttonSizeCoef_72(float value)
	{
		___buttonSizeCoef_72 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
