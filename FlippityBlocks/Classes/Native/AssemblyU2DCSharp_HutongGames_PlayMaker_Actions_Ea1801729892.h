﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1629559380.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Eas239496294.h"

// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchQuickPinch
struct  EasyTouchQuickPinch_t1801729892  : public EasyTouchQuickFSM_t1629559380
{
public:
	// System.Boolean HutongGames.PlayMaker.Actions.EasyTouchQuickPinch::onOwner
	bool ___onOwner_19;
	// HutongGames.PlayMaker.Actions.EasyTouchQuickPinch/ActionTriggering HutongGames.PlayMaker.Actions.EasyTouchQuickPinch::actionTriggering
	int32_t ___actionTriggering_20;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchQuickPinch::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_21;

public:
	inline static int32_t get_offset_of_onOwner_19() { return static_cast<int32_t>(offsetof(EasyTouchQuickPinch_t1801729892, ___onOwner_19)); }
	inline bool get_onOwner_19() const { return ___onOwner_19; }
	inline bool* get_address_of_onOwner_19() { return &___onOwner_19; }
	inline void set_onOwner_19(bool value)
	{
		___onOwner_19 = value;
	}

	inline static int32_t get_offset_of_actionTriggering_20() { return static_cast<int32_t>(offsetof(EasyTouchQuickPinch_t1801729892, ___actionTriggering_20)); }
	inline int32_t get_actionTriggering_20() const { return ___actionTriggering_20; }
	inline int32_t* get_address_of_actionTriggering_20() { return &___actionTriggering_20; }
	inline void set_actionTriggering_20(int32_t value)
	{
		___actionTriggering_20 = value;
	}

	inline static int32_t get_offset_of_proxy_21() { return static_cast<int32_t>(offsetof(EasyTouchQuickPinch_t1801729892, ___proxy_21)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_21() const { return ___proxy_21; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_21() { return &___proxy_21; }
	inline void set_proxy_21(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_21 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
