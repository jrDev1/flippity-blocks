﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"
#include "mscorlib_System_Nullable_1_gen2467698368.h"

// SRDebugger.UI.Other.SRTab
struct SRTab_t1974039238;
// SRDebugger.UI.Other.SRTabController
struct SRTabController_t1363238198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Scripts.DebuggerTabController
struct  DebuggerTabController_t1358735436  : public SRMonoBehaviourEx_t3120278648
{
public:
	// SRDebugger.UI.Other.SRTab SRDebugger.Scripts.DebuggerTabController::_aboutTabInstance
	SRTab_t1974039238 * ____aboutTabInstance_9;
	// System.Nullable`1<SRDebugger.DefaultTabs> SRDebugger.Scripts.DebuggerTabController::_activeTab
	Nullable_1_t2467698368  ____activeTab_10;
	// System.Boolean SRDebugger.Scripts.DebuggerTabController::_hasStarted
	bool ____hasStarted_11;
	// SRDebugger.UI.Other.SRTab SRDebugger.Scripts.DebuggerTabController::AboutTab
	SRTab_t1974039238 * ___AboutTab_12;
	// SRDebugger.UI.Other.SRTabController SRDebugger.Scripts.DebuggerTabController::TabController
	SRTabController_t1363238198 * ___TabController_13;

public:
	inline static int32_t get_offset_of__aboutTabInstance_9() { return static_cast<int32_t>(offsetof(DebuggerTabController_t1358735436, ____aboutTabInstance_9)); }
	inline SRTab_t1974039238 * get__aboutTabInstance_9() const { return ____aboutTabInstance_9; }
	inline SRTab_t1974039238 ** get_address_of__aboutTabInstance_9() { return &____aboutTabInstance_9; }
	inline void set__aboutTabInstance_9(SRTab_t1974039238 * value)
	{
		____aboutTabInstance_9 = value;
		Il2CppCodeGenWriteBarrier(&____aboutTabInstance_9, value);
	}

	inline static int32_t get_offset_of__activeTab_10() { return static_cast<int32_t>(offsetof(DebuggerTabController_t1358735436, ____activeTab_10)); }
	inline Nullable_1_t2467698368  get__activeTab_10() const { return ____activeTab_10; }
	inline Nullable_1_t2467698368 * get_address_of__activeTab_10() { return &____activeTab_10; }
	inline void set__activeTab_10(Nullable_1_t2467698368  value)
	{
		____activeTab_10 = value;
	}

	inline static int32_t get_offset_of__hasStarted_11() { return static_cast<int32_t>(offsetof(DebuggerTabController_t1358735436, ____hasStarted_11)); }
	inline bool get__hasStarted_11() const { return ____hasStarted_11; }
	inline bool* get_address_of__hasStarted_11() { return &____hasStarted_11; }
	inline void set__hasStarted_11(bool value)
	{
		____hasStarted_11 = value;
	}

	inline static int32_t get_offset_of_AboutTab_12() { return static_cast<int32_t>(offsetof(DebuggerTabController_t1358735436, ___AboutTab_12)); }
	inline SRTab_t1974039238 * get_AboutTab_12() const { return ___AboutTab_12; }
	inline SRTab_t1974039238 ** get_address_of_AboutTab_12() { return &___AboutTab_12; }
	inline void set_AboutTab_12(SRTab_t1974039238 * value)
	{
		___AboutTab_12 = value;
		Il2CppCodeGenWriteBarrier(&___AboutTab_12, value);
	}

	inline static int32_t get_offset_of_TabController_13() { return static_cast<int32_t>(offsetof(DebuggerTabController_t1358735436, ___TabController_13)); }
	inline SRTabController_t1363238198 * get_TabController_13() const { return ___TabController_13; }
	inline SRTabController_t1363238198 ** get_address_of_TabController_13() { return &___TabController_13; }
	inline void set_TabController_13(SRTabController_t1363238198 * value)
	{
		___TabController_13 = value;
		Il2CppCodeGenWriteBarrier(&___TabController_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
