﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t2455055323;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.LongPressButton
struct  LongPressButton_t1525827641  : public Button_t2872111280
{
public:
	// System.Boolean SRF.UI.LongPressButton::_handled
	bool ____handled_17;
	// UnityEngine.UI.Button/ButtonClickedEvent SRF.UI.LongPressButton::_onLongPress
	ButtonClickedEvent_t2455055323 * ____onLongPress_18;
	// System.Boolean SRF.UI.LongPressButton::_pressed
	bool ____pressed_19;
	// System.Single SRF.UI.LongPressButton::_pressedTime
	float ____pressedTime_20;
	// System.Single SRF.UI.LongPressButton::LongPressDuration
	float ___LongPressDuration_21;

public:
	inline static int32_t get_offset_of__handled_17() { return static_cast<int32_t>(offsetof(LongPressButton_t1525827641, ____handled_17)); }
	inline bool get__handled_17() const { return ____handled_17; }
	inline bool* get_address_of__handled_17() { return &____handled_17; }
	inline void set__handled_17(bool value)
	{
		____handled_17 = value;
	}

	inline static int32_t get_offset_of__onLongPress_18() { return static_cast<int32_t>(offsetof(LongPressButton_t1525827641, ____onLongPress_18)); }
	inline ButtonClickedEvent_t2455055323 * get__onLongPress_18() const { return ____onLongPress_18; }
	inline ButtonClickedEvent_t2455055323 ** get_address_of__onLongPress_18() { return &____onLongPress_18; }
	inline void set__onLongPress_18(ButtonClickedEvent_t2455055323 * value)
	{
		____onLongPress_18 = value;
		Il2CppCodeGenWriteBarrier(&____onLongPress_18, value);
	}

	inline static int32_t get_offset_of__pressed_19() { return static_cast<int32_t>(offsetof(LongPressButton_t1525827641, ____pressed_19)); }
	inline bool get__pressed_19() const { return ____pressed_19; }
	inline bool* get_address_of__pressed_19() { return &____pressed_19; }
	inline void set__pressed_19(bool value)
	{
		____pressed_19 = value;
	}

	inline static int32_t get_offset_of__pressedTime_20() { return static_cast<int32_t>(offsetof(LongPressButton_t1525827641, ____pressedTime_20)); }
	inline float get__pressedTime_20() const { return ____pressedTime_20; }
	inline float* get_address_of__pressedTime_20() { return &____pressedTime_20; }
	inline void set__pressedTime_20(float value)
	{
		____pressedTime_20 = value;
	}

	inline static int32_t get_offset_of_LongPressDuration_21() { return static_cast<int32_t>(offsetof(LongPressButton_t1525827641, ___LongPressDuration_21)); }
	inline float get_LongPressDuration_21() const { return ___LongPressDuration_21; }
	inline float* get_address_of_LongPressDuration_21() { return &___LongPressDuration_21; }
	inline void set_LongPressDuration_21(float value)
	{
		___LongPressDuration_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
