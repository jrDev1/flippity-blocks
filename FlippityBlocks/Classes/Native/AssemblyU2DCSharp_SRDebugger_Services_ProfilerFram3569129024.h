﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.ProfilerFrame
struct  ProfilerFrame_t3569129024 
{
public:
	// System.Double SRDebugger.Services.ProfilerFrame::FrameTime
	double ___FrameTime_0;
	// System.Double SRDebugger.Services.ProfilerFrame::OtherTime
	double ___OtherTime_1;
	// System.Double SRDebugger.Services.ProfilerFrame::RenderTime
	double ___RenderTime_2;
	// System.Double SRDebugger.Services.ProfilerFrame::UpdateTime
	double ___UpdateTime_3;

public:
	inline static int32_t get_offset_of_FrameTime_0() { return static_cast<int32_t>(offsetof(ProfilerFrame_t3569129024, ___FrameTime_0)); }
	inline double get_FrameTime_0() const { return ___FrameTime_0; }
	inline double* get_address_of_FrameTime_0() { return &___FrameTime_0; }
	inline void set_FrameTime_0(double value)
	{
		___FrameTime_0 = value;
	}

	inline static int32_t get_offset_of_OtherTime_1() { return static_cast<int32_t>(offsetof(ProfilerFrame_t3569129024, ___OtherTime_1)); }
	inline double get_OtherTime_1() const { return ___OtherTime_1; }
	inline double* get_address_of_OtherTime_1() { return &___OtherTime_1; }
	inline void set_OtherTime_1(double value)
	{
		___OtherTime_1 = value;
	}

	inline static int32_t get_offset_of_RenderTime_2() { return static_cast<int32_t>(offsetof(ProfilerFrame_t3569129024, ___RenderTime_2)); }
	inline double get_RenderTime_2() const { return ___RenderTime_2; }
	inline double* get_address_of_RenderTime_2() { return &___RenderTime_2; }
	inline void set_RenderTime_2(double value)
	{
		___RenderTime_2 = value;
	}

	inline static int32_t get_offset_of_UpdateTime_3() { return static_cast<int32_t>(offsetof(ProfilerFrame_t3569129024, ___UpdateTime_3)); }
	inline double get_UpdateTime_3() const { return ___UpdateTime_3; }
	inline double* get_address_of_UpdateTime_3() { return &___UpdateTime_3; }
	inline void set_UpdateTime_3(double value)
	{
		___UpdateTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
