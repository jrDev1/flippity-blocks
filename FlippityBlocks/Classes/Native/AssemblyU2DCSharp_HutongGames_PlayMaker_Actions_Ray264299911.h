﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802;
// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.RaycastFromScreen
struct  RaycastFromScreen_t264299911  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.RaycastFromScreen::camera
	FsmOwnerDefault_t2023674184 * ___camera_11;
	// HutongGames.PlayMaker.FsmVector3 HutongGames.PlayMaker.Actions.RaycastFromScreen::fromScreenPosition
	FsmVector3_t3996534004 * ___fromScreenPosition_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.RaycastFromScreen::distance
	FsmFloat_t937133978 * ___distance_13;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.RaycastFromScreen::hitEvent
	FsmEvent_t1258573736 * ___hitEvent_14;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RaycastFromScreen::storeDidHit
	FsmBool_t664485696 * ___storeDidHit_15;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.RaycastFromScreen::storeHitObject
	FsmGameObject_t3097142863 * ___storeHitObject_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.RaycastFromScreen::repeatInterval
	FsmInt_t1273009179 * ___repeatInterval_17;
	// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.Actions.RaycastFromScreen::layerMask
	FsmIntU5BU5D_t2637547802* ___layerMask_18;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.RaycastFromScreen::invertMask
	FsmBool_t664485696 * ___invertMask_19;
	// System.Int32 HutongGames.PlayMaker.Actions.RaycastFromScreen::repeat
	int32_t ___repeat_20;
	// UnityEngine.Camera HutongGames.PlayMaker.Actions.RaycastFromScreen::_cam
	Camera_t189460977 * ____cam_21;

public:
	inline static int32_t get_offset_of_camera_11() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___camera_11)); }
	inline FsmOwnerDefault_t2023674184 * get_camera_11() const { return ___camera_11; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_camera_11() { return &___camera_11; }
	inline void set_camera_11(FsmOwnerDefault_t2023674184 * value)
	{
		___camera_11 = value;
		Il2CppCodeGenWriteBarrier(&___camera_11, value);
	}

	inline static int32_t get_offset_of_fromScreenPosition_12() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___fromScreenPosition_12)); }
	inline FsmVector3_t3996534004 * get_fromScreenPosition_12() const { return ___fromScreenPosition_12; }
	inline FsmVector3_t3996534004 ** get_address_of_fromScreenPosition_12() { return &___fromScreenPosition_12; }
	inline void set_fromScreenPosition_12(FsmVector3_t3996534004 * value)
	{
		___fromScreenPosition_12 = value;
		Il2CppCodeGenWriteBarrier(&___fromScreenPosition_12, value);
	}

	inline static int32_t get_offset_of_distance_13() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___distance_13)); }
	inline FsmFloat_t937133978 * get_distance_13() const { return ___distance_13; }
	inline FsmFloat_t937133978 ** get_address_of_distance_13() { return &___distance_13; }
	inline void set_distance_13(FsmFloat_t937133978 * value)
	{
		___distance_13 = value;
		Il2CppCodeGenWriteBarrier(&___distance_13, value);
	}

	inline static int32_t get_offset_of_hitEvent_14() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___hitEvent_14)); }
	inline FsmEvent_t1258573736 * get_hitEvent_14() const { return ___hitEvent_14; }
	inline FsmEvent_t1258573736 ** get_address_of_hitEvent_14() { return &___hitEvent_14; }
	inline void set_hitEvent_14(FsmEvent_t1258573736 * value)
	{
		___hitEvent_14 = value;
		Il2CppCodeGenWriteBarrier(&___hitEvent_14, value);
	}

	inline static int32_t get_offset_of_storeDidHit_15() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___storeDidHit_15)); }
	inline FsmBool_t664485696 * get_storeDidHit_15() const { return ___storeDidHit_15; }
	inline FsmBool_t664485696 ** get_address_of_storeDidHit_15() { return &___storeDidHit_15; }
	inline void set_storeDidHit_15(FsmBool_t664485696 * value)
	{
		___storeDidHit_15 = value;
		Il2CppCodeGenWriteBarrier(&___storeDidHit_15, value);
	}

	inline static int32_t get_offset_of_storeHitObject_16() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___storeHitObject_16)); }
	inline FsmGameObject_t3097142863 * get_storeHitObject_16() const { return ___storeHitObject_16; }
	inline FsmGameObject_t3097142863 ** get_address_of_storeHitObject_16() { return &___storeHitObject_16; }
	inline void set_storeHitObject_16(FsmGameObject_t3097142863 * value)
	{
		___storeHitObject_16 = value;
		Il2CppCodeGenWriteBarrier(&___storeHitObject_16, value);
	}

	inline static int32_t get_offset_of_repeatInterval_17() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___repeatInterval_17)); }
	inline FsmInt_t1273009179 * get_repeatInterval_17() const { return ___repeatInterval_17; }
	inline FsmInt_t1273009179 ** get_address_of_repeatInterval_17() { return &___repeatInterval_17; }
	inline void set_repeatInterval_17(FsmInt_t1273009179 * value)
	{
		___repeatInterval_17 = value;
		Il2CppCodeGenWriteBarrier(&___repeatInterval_17, value);
	}

	inline static int32_t get_offset_of_layerMask_18() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___layerMask_18)); }
	inline FsmIntU5BU5D_t2637547802* get_layerMask_18() const { return ___layerMask_18; }
	inline FsmIntU5BU5D_t2637547802** get_address_of_layerMask_18() { return &___layerMask_18; }
	inline void set_layerMask_18(FsmIntU5BU5D_t2637547802* value)
	{
		___layerMask_18 = value;
		Il2CppCodeGenWriteBarrier(&___layerMask_18, value);
	}

	inline static int32_t get_offset_of_invertMask_19() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___invertMask_19)); }
	inline FsmBool_t664485696 * get_invertMask_19() const { return ___invertMask_19; }
	inline FsmBool_t664485696 ** get_address_of_invertMask_19() { return &___invertMask_19; }
	inline void set_invertMask_19(FsmBool_t664485696 * value)
	{
		___invertMask_19 = value;
		Il2CppCodeGenWriteBarrier(&___invertMask_19, value);
	}

	inline static int32_t get_offset_of_repeat_20() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ___repeat_20)); }
	inline int32_t get_repeat_20() const { return ___repeat_20; }
	inline int32_t* get_address_of_repeat_20() { return &___repeat_20; }
	inline void set_repeat_20(int32_t value)
	{
		___repeat_20 = value;
	}

	inline static int32_t get_offset_of__cam_21() { return static_cast<int32_t>(offsetof(RaycastFromScreen_t264299911, ____cam_21)); }
	inline Camera_t189460977 * get__cam_21() const { return ____cam_21; }
	inline Camera_t189460977 ** get_address_of__cam_21() { return &____cam_21; }
	inline void set__cam_21(Camera_t189460977 * value)
	{
		____cam_21 = value;
		Il2CppCodeGenWriteBarrier(&____cam_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
