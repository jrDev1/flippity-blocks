﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_ETCBase118528977.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// ETCButton/OnDownHandler
struct OnDownHandler_t1939343930;
// ETCButton/OnPressedHandler
struct OnPressedHandler_t379047892;
// ETCButton/OnPressedValueandler
struct OnPressedValueandler_t3041517609;
// ETCButton/OnUPHandler
struct OnUPHandler_t687066581;
// ETCAxis
struct ETCAxis_t3445562479;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCButton
struct  ETCButton_t2569486110  : public ETCBase_t118528977
{
public:
	// ETCButton/OnDownHandler ETCButton::onDown
	OnDownHandler_t1939343930 * ___onDown_48;
	// ETCButton/OnPressedHandler ETCButton::onPressed
	OnPressedHandler_t379047892 * ___onPressed_49;
	// ETCButton/OnPressedValueandler ETCButton::onPressedValue
	OnPressedValueandler_t3041517609 * ___onPressedValue_50;
	// ETCButton/OnUPHandler ETCButton::onUp
	OnUPHandler_t687066581 * ___onUp_51;
	// ETCAxis ETCButton::axis
	ETCAxis_t3445562479 * ___axis_52;
	// UnityEngine.Sprite ETCButton::normalSprite
	Sprite_t309593783 * ___normalSprite_53;
	// UnityEngine.Color ETCButton::normalColor
	Color_t2020392075  ___normalColor_54;
	// UnityEngine.Sprite ETCButton::pressedSprite
	Sprite_t309593783 * ___pressedSprite_55;
	// UnityEngine.Color ETCButton::pressedColor
	Color_t2020392075  ___pressedColor_56;
	// UnityEngine.UI.Image ETCButton::cachedImage
	Image_t2042527209 * ___cachedImage_57;
	// System.Boolean ETCButton::isOnPress
	bool ___isOnPress_58;
	// UnityEngine.GameObject ETCButton::previousDargObject
	GameObject_t1756533147 * ___previousDargObject_59;
	// System.Boolean ETCButton::isOnTouch
	bool ___isOnTouch_60;

public:
	inline static int32_t get_offset_of_onDown_48() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___onDown_48)); }
	inline OnDownHandler_t1939343930 * get_onDown_48() const { return ___onDown_48; }
	inline OnDownHandler_t1939343930 ** get_address_of_onDown_48() { return &___onDown_48; }
	inline void set_onDown_48(OnDownHandler_t1939343930 * value)
	{
		___onDown_48 = value;
		Il2CppCodeGenWriteBarrier(&___onDown_48, value);
	}

	inline static int32_t get_offset_of_onPressed_49() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___onPressed_49)); }
	inline OnPressedHandler_t379047892 * get_onPressed_49() const { return ___onPressed_49; }
	inline OnPressedHandler_t379047892 ** get_address_of_onPressed_49() { return &___onPressed_49; }
	inline void set_onPressed_49(OnPressedHandler_t379047892 * value)
	{
		___onPressed_49 = value;
		Il2CppCodeGenWriteBarrier(&___onPressed_49, value);
	}

	inline static int32_t get_offset_of_onPressedValue_50() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___onPressedValue_50)); }
	inline OnPressedValueandler_t3041517609 * get_onPressedValue_50() const { return ___onPressedValue_50; }
	inline OnPressedValueandler_t3041517609 ** get_address_of_onPressedValue_50() { return &___onPressedValue_50; }
	inline void set_onPressedValue_50(OnPressedValueandler_t3041517609 * value)
	{
		___onPressedValue_50 = value;
		Il2CppCodeGenWriteBarrier(&___onPressedValue_50, value);
	}

	inline static int32_t get_offset_of_onUp_51() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___onUp_51)); }
	inline OnUPHandler_t687066581 * get_onUp_51() const { return ___onUp_51; }
	inline OnUPHandler_t687066581 ** get_address_of_onUp_51() { return &___onUp_51; }
	inline void set_onUp_51(OnUPHandler_t687066581 * value)
	{
		___onUp_51 = value;
		Il2CppCodeGenWriteBarrier(&___onUp_51, value);
	}

	inline static int32_t get_offset_of_axis_52() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___axis_52)); }
	inline ETCAxis_t3445562479 * get_axis_52() const { return ___axis_52; }
	inline ETCAxis_t3445562479 ** get_address_of_axis_52() { return &___axis_52; }
	inline void set_axis_52(ETCAxis_t3445562479 * value)
	{
		___axis_52 = value;
		Il2CppCodeGenWriteBarrier(&___axis_52, value);
	}

	inline static int32_t get_offset_of_normalSprite_53() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___normalSprite_53)); }
	inline Sprite_t309593783 * get_normalSprite_53() const { return ___normalSprite_53; }
	inline Sprite_t309593783 ** get_address_of_normalSprite_53() { return &___normalSprite_53; }
	inline void set_normalSprite_53(Sprite_t309593783 * value)
	{
		___normalSprite_53 = value;
		Il2CppCodeGenWriteBarrier(&___normalSprite_53, value);
	}

	inline static int32_t get_offset_of_normalColor_54() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___normalColor_54)); }
	inline Color_t2020392075  get_normalColor_54() const { return ___normalColor_54; }
	inline Color_t2020392075 * get_address_of_normalColor_54() { return &___normalColor_54; }
	inline void set_normalColor_54(Color_t2020392075  value)
	{
		___normalColor_54 = value;
	}

	inline static int32_t get_offset_of_pressedSprite_55() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___pressedSprite_55)); }
	inline Sprite_t309593783 * get_pressedSprite_55() const { return ___pressedSprite_55; }
	inline Sprite_t309593783 ** get_address_of_pressedSprite_55() { return &___pressedSprite_55; }
	inline void set_pressedSprite_55(Sprite_t309593783 * value)
	{
		___pressedSprite_55 = value;
		Il2CppCodeGenWriteBarrier(&___pressedSprite_55, value);
	}

	inline static int32_t get_offset_of_pressedColor_56() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___pressedColor_56)); }
	inline Color_t2020392075  get_pressedColor_56() const { return ___pressedColor_56; }
	inline Color_t2020392075 * get_address_of_pressedColor_56() { return &___pressedColor_56; }
	inline void set_pressedColor_56(Color_t2020392075  value)
	{
		___pressedColor_56 = value;
	}

	inline static int32_t get_offset_of_cachedImage_57() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___cachedImage_57)); }
	inline Image_t2042527209 * get_cachedImage_57() const { return ___cachedImage_57; }
	inline Image_t2042527209 ** get_address_of_cachedImage_57() { return &___cachedImage_57; }
	inline void set_cachedImage_57(Image_t2042527209 * value)
	{
		___cachedImage_57 = value;
		Il2CppCodeGenWriteBarrier(&___cachedImage_57, value);
	}

	inline static int32_t get_offset_of_isOnPress_58() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___isOnPress_58)); }
	inline bool get_isOnPress_58() const { return ___isOnPress_58; }
	inline bool* get_address_of_isOnPress_58() { return &___isOnPress_58; }
	inline void set_isOnPress_58(bool value)
	{
		___isOnPress_58 = value;
	}

	inline static int32_t get_offset_of_previousDargObject_59() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___previousDargObject_59)); }
	inline GameObject_t1756533147 * get_previousDargObject_59() const { return ___previousDargObject_59; }
	inline GameObject_t1756533147 ** get_address_of_previousDargObject_59() { return &___previousDargObject_59; }
	inline void set_previousDargObject_59(GameObject_t1756533147 * value)
	{
		___previousDargObject_59 = value;
		Il2CppCodeGenWriteBarrier(&___previousDargObject_59, value);
	}

	inline static int32_t get_offset_of_isOnTouch_60() { return static_cast<int32_t>(offsetof(ETCButton_t2569486110, ___isOnTouch_60)); }
	inline bool get_isOnTouch_60() const { return ___isOnTouch_60; }
	inline bool* get_address_of_isOnTouch_60() { return &___isOnTouch_60; }
	inline void set_isOnTouch_60(bool value)
	{
		___isOnTouch_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
