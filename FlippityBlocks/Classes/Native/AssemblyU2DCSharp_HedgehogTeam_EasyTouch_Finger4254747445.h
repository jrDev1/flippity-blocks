﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_BaseFinge1402521766.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_TouchPhase2458120420.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch_835798112.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_EasyTouch2889416990.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.Finger
struct  Finger_t4254747445  : public BaseFinger_t1402521766
{
public:
	// System.Single HedgehogTeam.EasyTouch.Finger::startTimeAction
	float ___startTimeAction_19;
	// UnityEngine.Vector2 HedgehogTeam.EasyTouch.Finger::oldPosition
	Vector2_t2243707579  ___oldPosition_20;
	// System.Int32 HedgehogTeam.EasyTouch.Finger::tapCount
	int32_t ___tapCount_21;
	// UnityEngine.TouchPhase HedgehogTeam.EasyTouch.Finger::phase
	int32_t ___phase_22;
	// HedgehogTeam.EasyTouch.EasyTouch/GestureType HedgehogTeam.EasyTouch.Finger::gesture
	int32_t ___gesture_23;
	// HedgehogTeam.EasyTouch.EasyTouch/SwipeDirection HedgehogTeam.EasyTouch.Finger::oldSwipeType
	int32_t ___oldSwipeType_24;

public:
	inline static int32_t get_offset_of_startTimeAction_19() { return static_cast<int32_t>(offsetof(Finger_t4254747445, ___startTimeAction_19)); }
	inline float get_startTimeAction_19() const { return ___startTimeAction_19; }
	inline float* get_address_of_startTimeAction_19() { return &___startTimeAction_19; }
	inline void set_startTimeAction_19(float value)
	{
		___startTimeAction_19 = value;
	}

	inline static int32_t get_offset_of_oldPosition_20() { return static_cast<int32_t>(offsetof(Finger_t4254747445, ___oldPosition_20)); }
	inline Vector2_t2243707579  get_oldPosition_20() const { return ___oldPosition_20; }
	inline Vector2_t2243707579 * get_address_of_oldPosition_20() { return &___oldPosition_20; }
	inline void set_oldPosition_20(Vector2_t2243707579  value)
	{
		___oldPosition_20 = value;
	}

	inline static int32_t get_offset_of_tapCount_21() { return static_cast<int32_t>(offsetof(Finger_t4254747445, ___tapCount_21)); }
	inline int32_t get_tapCount_21() const { return ___tapCount_21; }
	inline int32_t* get_address_of_tapCount_21() { return &___tapCount_21; }
	inline void set_tapCount_21(int32_t value)
	{
		___tapCount_21 = value;
	}

	inline static int32_t get_offset_of_phase_22() { return static_cast<int32_t>(offsetof(Finger_t4254747445, ___phase_22)); }
	inline int32_t get_phase_22() const { return ___phase_22; }
	inline int32_t* get_address_of_phase_22() { return &___phase_22; }
	inline void set_phase_22(int32_t value)
	{
		___phase_22 = value;
	}

	inline static int32_t get_offset_of_gesture_23() { return static_cast<int32_t>(offsetof(Finger_t4254747445, ___gesture_23)); }
	inline int32_t get_gesture_23() const { return ___gesture_23; }
	inline int32_t* get_address_of_gesture_23() { return &___gesture_23; }
	inline void set_gesture_23(int32_t value)
	{
		___gesture_23 = value;
	}

	inline static int32_t get_offset_of_oldSwipeType_24() { return static_cast<int32_t>(offsetof(Finger_t4254747445, ___oldSwipeType_24)); }
	inline int32_t get_oldSwipeType_24() const { return ___oldSwipeType_24; }
	inline int32_t* get_address_of_oldSwipeType_24() { return &___oldSwipeType_24; }
	inline void set_oldSwipeType_24(int32_t value)
	{
		___oldSwipeType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
