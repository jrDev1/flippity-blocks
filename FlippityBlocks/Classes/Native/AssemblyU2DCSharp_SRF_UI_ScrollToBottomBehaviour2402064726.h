﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.ScrollToBottomBehaviour
struct  ScrollToBottomBehaviour_t2402064726  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.ScrollRect SRF.UI.ScrollToBottomBehaviour::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_2;
	// UnityEngine.CanvasGroup SRF.UI.ScrollToBottomBehaviour::_canvasGroup
	CanvasGroup_t3296560743 * ____canvasGroup_3;

public:
	inline static int32_t get_offset_of__scrollRect_2() { return static_cast<int32_t>(offsetof(ScrollToBottomBehaviour_t2402064726, ____scrollRect_2)); }
	inline ScrollRect_t1199013257 * get__scrollRect_2() const { return ____scrollRect_2; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_2() { return &____scrollRect_2; }
	inline void set__scrollRect_2(ScrollRect_t1199013257 * value)
	{
		____scrollRect_2 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRect_2, value);
	}

	inline static int32_t get_offset_of__canvasGroup_3() { return static_cast<int32_t>(offsetof(ScrollToBottomBehaviour_t2402064726, ____canvasGroup_3)); }
	inline CanvasGroup_t3296560743 * get__canvasGroup_3() const { return ____canvasGroup_3; }
	inline CanvasGroup_t3296560743 ** get_address_of__canvasGroup_3() { return &____canvasGroup_3; }
	inline void set__canvasGroup_3(CanvasGroup_t3296560743 * value)
	{
		____canvasGroup_3 = value;
		Il2CppCodeGenWriteBarrier(&____canvasGroup_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
