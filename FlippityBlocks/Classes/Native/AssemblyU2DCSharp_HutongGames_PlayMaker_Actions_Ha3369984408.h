﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableSet
struct  HashTableSet_t3369984408  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableSet::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableSet::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableSet::key
	FsmString_t2414474701 * ___key_14;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.HashTableSet::variable
	FsmVar_t2872592513 * ___variable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.HashTableSet::convertIntToByte
	bool ___convertIntToByte_16;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableSet_t3369984408, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableSet_t3369984408, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(HashTableSet_t3369984408, ___key_14)); }
	inline FsmString_t2414474701 * get_key_14() const { return ___key_14; }
	inline FsmString_t2414474701 ** get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(FsmString_t2414474701 * value)
	{
		___key_14 = value;
		Il2CppCodeGenWriteBarrier(&___key_14, value);
	}

	inline static int32_t get_offset_of_variable_15() { return static_cast<int32_t>(offsetof(HashTableSet_t3369984408, ___variable_15)); }
	inline FsmVar_t2872592513 * get_variable_15() const { return ___variable_15; }
	inline FsmVar_t2872592513 ** get_address_of_variable_15() { return &___variable_15; }
	inline void set_variable_15(FsmVar_t2872592513 * value)
	{
		___variable_15 = value;
		Il2CppCodeGenWriteBarrier(&___variable_15, value);
	}

	inline static int32_t get_offset_of_convertIntToByte_16() { return static_cast<int32_t>(offsetof(HashTableSet_t3369984408, ___convertIntToByte_16)); }
	inline bool get_convertIntToByte_16() const { return ___convertIntToByte_16; }
	inline bool* get_address_of_convertIntToByte_16() { return &___convertIntToByte_16; }
	inline void set_convertIntToByte_16(bool value)
	{
		___convertIntToByte_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
