﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.UI.Other.SRTab
struct SRTab_t1974039238;
// SRDebugger.UI.Other.SRTabController
struct SRTabController_t1363238198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.SRTabController/<AddTab>c__AnonStorey0
struct  U3CAddTabU3Ec__AnonStorey0_t986591490  : public Il2CppObject
{
public:
	// SRDebugger.UI.Other.SRTab SRDebugger.UI.Other.SRTabController/<AddTab>c__AnonStorey0::tab
	SRTab_t1974039238 * ___tab_0;
	// SRDebugger.UI.Other.SRTabController SRDebugger.UI.Other.SRTabController/<AddTab>c__AnonStorey0::$this
	SRTabController_t1363238198 * ___U24this_1;

public:
	inline static int32_t get_offset_of_tab_0() { return static_cast<int32_t>(offsetof(U3CAddTabU3Ec__AnonStorey0_t986591490, ___tab_0)); }
	inline SRTab_t1974039238 * get_tab_0() const { return ___tab_0; }
	inline SRTab_t1974039238 ** get_address_of_tab_0() { return &___tab_0; }
	inline void set_tab_0(SRTab_t1974039238 * value)
	{
		___tab_0 = value;
		Il2CppCodeGenWriteBarrier(&___tab_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAddTabU3Ec__AnonStorey0_t986591490, ___U24this_1)); }
	inline SRTabController_t1363238198 * get_U24this_1() const { return ___U24this_1; }
	inline SRTabController_t1363238198 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SRTabController_t1363238198 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
