﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// ETCInput
struct ETCInput_t659679246;
// System.Collections.Generic.Dictionary`2<System.String,ETCAxis>
struct Dictionary_2_t1065374445;
// System.Collections.Generic.Dictionary`2<System.String,ETCBase>
struct Dictionary_2_t2033308239;
// ETCBase
struct ETCBase_t118528977;
// ETCAxis
struct ETCAxis_t3445562479;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ETCInput
struct  ETCInput_t659679246  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ETCAxis> ETCInput::axes
	Dictionary_2_t1065374445 * ___axes_3;
	// System.Collections.Generic.Dictionary`2<System.String,ETCBase> ETCInput::controls
	Dictionary_2_t2033308239 * ___controls_4;

public:
	inline static int32_t get_offset_of_axes_3() { return static_cast<int32_t>(offsetof(ETCInput_t659679246, ___axes_3)); }
	inline Dictionary_2_t1065374445 * get_axes_3() const { return ___axes_3; }
	inline Dictionary_2_t1065374445 ** get_address_of_axes_3() { return &___axes_3; }
	inline void set_axes_3(Dictionary_2_t1065374445 * value)
	{
		___axes_3 = value;
		Il2CppCodeGenWriteBarrier(&___axes_3, value);
	}

	inline static int32_t get_offset_of_controls_4() { return static_cast<int32_t>(offsetof(ETCInput_t659679246, ___controls_4)); }
	inline Dictionary_2_t2033308239 * get_controls_4() const { return ___controls_4; }
	inline Dictionary_2_t2033308239 ** get_address_of_controls_4() { return &___controls_4; }
	inline void set_controls_4(Dictionary_2_t2033308239 * value)
	{
		___controls_4 = value;
		Il2CppCodeGenWriteBarrier(&___controls_4, value);
	}
};

struct ETCInput_t659679246_StaticFields
{
public:
	// ETCInput ETCInput::_instance
	ETCInput_t659679246 * ____instance_2;
	// ETCBase ETCInput::control
	ETCBase_t118528977 * ___control_5;
	// ETCAxis ETCInput::axis
	ETCAxis_t3445562479 * ___axis_6;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(ETCInput_t659679246_StaticFields, ____instance_2)); }
	inline ETCInput_t659679246 * get__instance_2() const { return ____instance_2; }
	inline ETCInput_t659679246 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(ETCInput_t659679246 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier(&____instance_2, value);
	}

	inline static int32_t get_offset_of_control_5() { return static_cast<int32_t>(offsetof(ETCInput_t659679246_StaticFields, ___control_5)); }
	inline ETCBase_t118528977 * get_control_5() const { return ___control_5; }
	inline ETCBase_t118528977 ** get_address_of_control_5() { return &___control_5; }
	inline void set_control_5(ETCBase_t118528977 * value)
	{
		___control_5 = value;
		Il2CppCodeGenWriteBarrier(&___control_5, value);
	}

	inline static int32_t get_offset_of_axis_6() { return static_cast<int32_t>(offsetof(ETCInput_t659679246_StaticFields, ___axis_6)); }
	inline ETCAxis_t3445562479 * get_axis_6() const { return ___axis_6; }
	inline ETCAxis_t3445562479 ** get_address_of_axis_6() { return &___axis_6; }
	inline void set_axis_6(ETCAxis_t3445562479 * value)
	{
		___axis_6 = value;
		Il2CppCodeGenWriteBarrier(&___axis_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
