﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeleteAndCreateInSameFrame
struct  DeleteAndCreateInSameFrame_t604968438  : public MonoBehaviour_t1158329972
{
public:
	// PathologicalGames.SpawnPool DeleteAndCreateInSameFrame::poolPrefab
	SpawnPool_t2419717525 * ___poolPrefab_2;

public:
	inline static int32_t get_offset_of_poolPrefab_2() { return static_cast<int32_t>(offsetof(DeleteAndCreateInSameFrame_t604968438, ___poolPrefab_2)); }
	inline SpawnPool_t2419717525 * get_poolPrefab_2() const { return ___poolPrefab_2; }
	inline SpawnPool_t2419717525 ** get_address_of_poolPrefab_2() { return &___poolPrefab_2; }
	inline void set_poolPrefab_2(SpawnPool_t2419717525 * value)
	{
		___poolPrefab_2 = value;
		Il2CppCodeGenWriteBarrier(&___poolPrefab_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
