﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex
struct  ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::fsmName
	FsmString_t2414474701 * ___fsmName_14;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::variableName
	FsmString_t2414474701 * ___variableName_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::everyframe
	bool ___everyframe_16;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::storeMaxValue
	FsmFloat_t937133978 * ___storeMaxValue_17;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::maxGameObject
	FsmGameObject_t3097142863 * ___maxGameObject_18;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::maxIndex
	FsmInt_t1273009179 * ___maxIndex_19;
	// UnityEngine.GameObject HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::goLastFrame
	GameObject_t1756533147 * ___goLastFrame_20;
	// PlayMakerFSM HutongGames.PlayMaker.Actions.ArrayListGetGameobjectMaxFsmFloatIndex::fsm
	PlayMakerFSM_t437737208 * ___fsm_21;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_fsmName_14() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___fsmName_14)); }
	inline FsmString_t2414474701 * get_fsmName_14() const { return ___fsmName_14; }
	inline FsmString_t2414474701 ** get_address_of_fsmName_14() { return &___fsmName_14; }
	inline void set_fsmName_14(FsmString_t2414474701 * value)
	{
		___fsmName_14 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_14, value);
	}

	inline static int32_t get_offset_of_variableName_15() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___variableName_15)); }
	inline FsmString_t2414474701 * get_variableName_15() const { return ___variableName_15; }
	inline FsmString_t2414474701 ** get_address_of_variableName_15() { return &___variableName_15; }
	inline void set_variableName_15(FsmString_t2414474701 * value)
	{
		___variableName_15 = value;
		Il2CppCodeGenWriteBarrier(&___variableName_15, value);
	}

	inline static int32_t get_offset_of_everyframe_16() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___everyframe_16)); }
	inline bool get_everyframe_16() const { return ___everyframe_16; }
	inline bool* get_address_of_everyframe_16() { return &___everyframe_16; }
	inline void set_everyframe_16(bool value)
	{
		___everyframe_16 = value;
	}

	inline static int32_t get_offset_of_storeMaxValue_17() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___storeMaxValue_17)); }
	inline FsmFloat_t937133978 * get_storeMaxValue_17() const { return ___storeMaxValue_17; }
	inline FsmFloat_t937133978 ** get_address_of_storeMaxValue_17() { return &___storeMaxValue_17; }
	inline void set_storeMaxValue_17(FsmFloat_t937133978 * value)
	{
		___storeMaxValue_17 = value;
		Il2CppCodeGenWriteBarrier(&___storeMaxValue_17, value);
	}

	inline static int32_t get_offset_of_maxGameObject_18() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___maxGameObject_18)); }
	inline FsmGameObject_t3097142863 * get_maxGameObject_18() const { return ___maxGameObject_18; }
	inline FsmGameObject_t3097142863 ** get_address_of_maxGameObject_18() { return &___maxGameObject_18; }
	inline void set_maxGameObject_18(FsmGameObject_t3097142863 * value)
	{
		___maxGameObject_18 = value;
		Il2CppCodeGenWriteBarrier(&___maxGameObject_18, value);
	}

	inline static int32_t get_offset_of_maxIndex_19() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___maxIndex_19)); }
	inline FsmInt_t1273009179 * get_maxIndex_19() const { return ___maxIndex_19; }
	inline FsmInt_t1273009179 ** get_address_of_maxIndex_19() { return &___maxIndex_19; }
	inline void set_maxIndex_19(FsmInt_t1273009179 * value)
	{
		___maxIndex_19 = value;
		Il2CppCodeGenWriteBarrier(&___maxIndex_19, value);
	}

	inline static int32_t get_offset_of_goLastFrame_20() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___goLastFrame_20)); }
	inline GameObject_t1756533147 * get_goLastFrame_20() const { return ___goLastFrame_20; }
	inline GameObject_t1756533147 ** get_address_of_goLastFrame_20() { return &___goLastFrame_20; }
	inline void set_goLastFrame_20(GameObject_t1756533147 * value)
	{
		___goLastFrame_20 = value;
		Il2CppCodeGenWriteBarrier(&___goLastFrame_20, value);
	}

	inline static int32_t get_offset_of_fsm_21() { return static_cast<int32_t>(offsetof(ArrayListGetGameobjectMaxFsmFloatIndex_t3922221970, ___fsm_21)); }
	inline PlayMakerFSM_t437737208 * get_fsm_21() const { return ___fsm_21; }
	inline PlayMakerFSM_t437737208 ** get_address_of_fsm_21() { return &___fsm_21; }
	inline void set_fsm_21(PlayMakerFSM_t437737208 * value)
	{
		___fsm_21 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
