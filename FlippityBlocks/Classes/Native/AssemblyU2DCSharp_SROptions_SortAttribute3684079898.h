﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SROptions/SortAttribute
struct  SortAttribute_t3684079898  : public Attribute_t542643598
{
public:
	// System.Int32 SROptions/SortAttribute::SortPriority
	int32_t ___SortPriority_0;

public:
	inline static int32_t get_offset_of_SortPriority_0() { return static_cast<int32_t>(offsetof(SortAttribute_t3684079898, ___SortPriority_0)); }
	inline int32_t get_SortPriority_0() const { return ___SortPriority_0; }
	inline int32_t* get_address_of_SortPriority_0() { return &___SortPriority_0; }
	inline void set_SortPriority_0(int32_t value)
	{
		___SortPriority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
