﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Ecosystem_3345575873.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget
struct  PlayMakerFsmVariableTarget_t681224443  : public Il2CppObject
{
public:
	// HutongGames.PlayMaker.Ecosystem.Utils.ProxyFsmVariableTarget HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::variableTarget
	int32_t ___variableTarget_0;
	// UnityEngine.GameObject HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// System.String HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::fsmName
	String_t* ___fsmName_2;
	// PlayMakerFSM HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::_fsmComponent
	PlayMakerFSM_t437737208 * ____fsmComponent_3;
	// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::_fsmVariables
	FsmVariables_t630687169 * ____fsmVariables_4;
	// System.Boolean HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerFsmVariableTarget::_initialized
	bool ____initialized_5;

public:
	inline static int32_t get_offset_of_variableTarget_0() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariableTarget_t681224443, ___variableTarget_0)); }
	inline int32_t get_variableTarget_0() const { return ___variableTarget_0; }
	inline int32_t* get_address_of_variableTarget_0() { return &___variableTarget_0; }
	inline void set_variableTarget_0(int32_t value)
	{
		___variableTarget_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariableTarget_t681224443, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}

	inline static int32_t get_offset_of_fsmName_2() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariableTarget_t681224443, ___fsmName_2)); }
	inline String_t* get_fsmName_2() const { return ___fsmName_2; }
	inline String_t** get_address_of_fsmName_2() { return &___fsmName_2; }
	inline void set_fsmName_2(String_t* value)
	{
		___fsmName_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsmName_2, value);
	}

	inline static int32_t get_offset_of__fsmComponent_3() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariableTarget_t681224443, ____fsmComponent_3)); }
	inline PlayMakerFSM_t437737208 * get__fsmComponent_3() const { return ____fsmComponent_3; }
	inline PlayMakerFSM_t437737208 ** get_address_of__fsmComponent_3() { return &____fsmComponent_3; }
	inline void set__fsmComponent_3(PlayMakerFSM_t437737208 * value)
	{
		____fsmComponent_3 = value;
		Il2CppCodeGenWriteBarrier(&____fsmComponent_3, value);
	}

	inline static int32_t get_offset_of__fsmVariables_4() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariableTarget_t681224443, ____fsmVariables_4)); }
	inline FsmVariables_t630687169 * get__fsmVariables_4() const { return ____fsmVariables_4; }
	inline FsmVariables_t630687169 ** get_address_of__fsmVariables_4() { return &____fsmVariables_4; }
	inline void set__fsmVariables_4(FsmVariables_t630687169 * value)
	{
		____fsmVariables_4 = value;
		Il2CppCodeGenWriteBarrier(&____fsmVariables_4, value);
	}

	inline static int32_t get_offset_of__initialized_5() { return static_cast<int32_t>(offsetof(PlayMakerFsmVariableTarget_t681224443, ____initialized_5)); }
	inline bool get__initialized_5() const { return ____initialized_5; }
	inline bool* get_address_of__initialized_5() { return &____initialized_5; }
	inline void set__initialized_5(bool value)
	{
		____initialized_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
