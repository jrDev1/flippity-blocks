﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,SRDebugger.UI.Controls.InfoBlock>
struct Dictionary_2_t2225147563;
// SRDebugger.UI.Controls.InfoBlock
struct InfoBlock_t310368301;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Tabs.InfoTabController
struct  InfoTabController_t2308368305  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SRDebugger.UI.Controls.InfoBlock> SRDebugger.UI.Tabs.InfoTabController::_infoBlocks
	Dictionary_2_t2225147563 * ____infoBlocks_12;
	// SRDebugger.UI.Controls.InfoBlock SRDebugger.UI.Tabs.InfoTabController::InfoBlockPrefab
	InfoBlock_t310368301 * ___InfoBlockPrefab_13;
	// UnityEngine.RectTransform SRDebugger.UI.Tabs.InfoTabController::LayoutContainer
	RectTransform_t3349966182 * ___LayoutContainer_14;

public:
	inline static int32_t get_offset_of__infoBlocks_12() { return static_cast<int32_t>(offsetof(InfoTabController_t2308368305, ____infoBlocks_12)); }
	inline Dictionary_2_t2225147563 * get__infoBlocks_12() const { return ____infoBlocks_12; }
	inline Dictionary_2_t2225147563 ** get_address_of__infoBlocks_12() { return &____infoBlocks_12; }
	inline void set__infoBlocks_12(Dictionary_2_t2225147563 * value)
	{
		____infoBlocks_12 = value;
		Il2CppCodeGenWriteBarrier(&____infoBlocks_12, value);
	}

	inline static int32_t get_offset_of_InfoBlockPrefab_13() { return static_cast<int32_t>(offsetof(InfoTabController_t2308368305, ___InfoBlockPrefab_13)); }
	inline InfoBlock_t310368301 * get_InfoBlockPrefab_13() const { return ___InfoBlockPrefab_13; }
	inline InfoBlock_t310368301 ** get_address_of_InfoBlockPrefab_13() { return &___InfoBlockPrefab_13; }
	inline void set_InfoBlockPrefab_13(InfoBlock_t310368301 * value)
	{
		___InfoBlockPrefab_13 = value;
		Il2CppCodeGenWriteBarrier(&___InfoBlockPrefab_13, value);
	}

	inline static int32_t get_offset_of_LayoutContainer_14() { return static_cast<int32_t>(offsetof(InfoTabController_t2308368305, ___LayoutContainer_14)); }
	inline RectTransform_t3349966182 * get_LayoutContainer_14() const { return ___LayoutContainer_14; }
	inline RectTransform_t3349966182 ** get_address_of_LayoutContainer_14() { return &___LayoutContainer_14; }
	inline void set_LayoutContainer_14(RectTransform_t3349966182 * value)
	{
		___LayoutContainer_14 = value;
		Il2CppCodeGenWriteBarrier(&___LayoutContainer_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
