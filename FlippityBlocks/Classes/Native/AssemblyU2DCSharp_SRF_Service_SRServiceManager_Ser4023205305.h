﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.SRServiceManager/Service
struct  Service_t4023205305  : public Il2CppObject
{
public:
	// System.Object SRF.Service.SRServiceManager/Service::Object
	Il2CppObject * ___Object_0;
	// System.Type SRF.Service.SRServiceManager/Service::Type
	Type_t * ___Type_1;

public:
	inline static int32_t get_offset_of_Object_0() { return static_cast<int32_t>(offsetof(Service_t4023205305, ___Object_0)); }
	inline Il2CppObject * get_Object_0() const { return ___Object_0; }
	inline Il2CppObject ** get_address_of_Object_0() { return &___Object_0; }
	inline void set_Object_0(Il2CppObject * value)
	{
		___Object_0 = value;
		Il2CppCodeGenWriteBarrier(&___Object_0, value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(Service_t4023205305, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier(&___Type_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
