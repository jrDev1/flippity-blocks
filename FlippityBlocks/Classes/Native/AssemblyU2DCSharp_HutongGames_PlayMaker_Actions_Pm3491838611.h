﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtDeSpawn
struct  PmtDeSpawn_t3491838611  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtDeSpawn::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtDeSpawn::gameObject
	FsmGameObject_t3097142863 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PmtDeSpawn::reParent
	FsmBool_t664485696 * ___reParent_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.PmtDeSpawn::delay
	FsmFloat_t937133978 * ___delay_14;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtDeSpawn::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_15;
	// PathologicalGames.SpawnPool HutongGames.PlayMaker.Actions.PmtDeSpawn::_pool
	SpawnPool_t2419717525 * ____pool_16;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtDeSpawn_t3491838611, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(PmtDeSpawn_t3491838611, ___gameObject_12)); }
	inline FsmGameObject_t3097142863 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmGameObject_t3097142863 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reParent_13() { return static_cast<int32_t>(offsetof(PmtDeSpawn_t3491838611, ___reParent_13)); }
	inline FsmBool_t664485696 * get_reParent_13() const { return ___reParent_13; }
	inline FsmBool_t664485696 ** get_address_of_reParent_13() { return &___reParent_13; }
	inline void set_reParent_13(FsmBool_t664485696 * value)
	{
		___reParent_13 = value;
		Il2CppCodeGenWriteBarrier(&___reParent_13, value);
	}

	inline static int32_t get_offset_of_delay_14() { return static_cast<int32_t>(offsetof(PmtDeSpawn_t3491838611, ___delay_14)); }
	inline FsmFloat_t937133978 * get_delay_14() const { return ___delay_14; }
	inline FsmFloat_t937133978 ** get_address_of_delay_14() { return &___delay_14; }
	inline void set_delay_14(FsmFloat_t937133978 * value)
	{
		___delay_14 = value;
		Il2CppCodeGenWriteBarrier(&___delay_14, value);
	}

	inline static int32_t get_offset_of_failureEvent_15() { return static_cast<int32_t>(offsetof(PmtDeSpawn_t3491838611, ___failureEvent_15)); }
	inline FsmEvent_t1258573736 * get_failureEvent_15() const { return ___failureEvent_15; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_15() { return &___failureEvent_15; }
	inline void set_failureEvent_15(FsmEvent_t1258573736 * value)
	{
		___failureEvent_15 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_15, value);
	}

	inline static int32_t get_offset_of__pool_16() { return static_cast<int32_t>(offsetof(PmtDeSpawn_t3491838611, ____pool_16)); }
	inline SpawnPool_t2419717525 * get__pool_16() const { return ____pool_16; }
	inline SpawnPool_t2419717525 ** get_address_of__pool_16() { return &____pool_16; }
	inline void set__pool_16(SpawnPool_t2419717525 * value)
	{
		____pool_16 = value;
		Il2CppCodeGenWriteBarrier(&____pool_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
