﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SRDebugger_Settings_ShortcutActio299253119.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Settings/KeyboardShortcut
struct  KeyboardShortcut_t2512121171  : public Il2CppObject
{
public:
	// SRDebugger.Settings/ShortcutActions SRDebugger.Settings/KeyboardShortcut::Action
	int32_t ___Action_0;
	// System.Boolean SRDebugger.Settings/KeyboardShortcut::Alt
	bool ___Alt_1;
	// System.Boolean SRDebugger.Settings/KeyboardShortcut::Control
	bool ___Control_2;
	// UnityEngine.KeyCode SRDebugger.Settings/KeyboardShortcut::Key
	int32_t ___Key_3;
	// System.Boolean SRDebugger.Settings/KeyboardShortcut::Shift
	bool ___Shift_4;

public:
	inline static int32_t get_offset_of_Action_0() { return static_cast<int32_t>(offsetof(KeyboardShortcut_t2512121171, ___Action_0)); }
	inline int32_t get_Action_0() const { return ___Action_0; }
	inline int32_t* get_address_of_Action_0() { return &___Action_0; }
	inline void set_Action_0(int32_t value)
	{
		___Action_0 = value;
	}

	inline static int32_t get_offset_of_Alt_1() { return static_cast<int32_t>(offsetof(KeyboardShortcut_t2512121171, ___Alt_1)); }
	inline bool get_Alt_1() const { return ___Alt_1; }
	inline bool* get_address_of_Alt_1() { return &___Alt_1; }
	inline void set_Alt_1(bool value)
	{
		___Alt_1 = value;
	}

	inline static int32_t get_offset_of_Control_2() { return static_cast<int32_t>(offsetof(KeyboardShortcut_t2512121171, ___Control_2)); }
	inline bool get_Control_2() const { return ___Control_2; }
	inline bool* get_address_of_Control_2() { return &___Control_2; }
	inline void set_Control_2(bool value)
	{
		___Control_2 = value;
	}

	inline static int32_t get_offset_of_Key_3() { return static_cast<int32_t>(offsetof(KeyboardShortcut_t2512121171, ___Key_3)); }
	inline int32_t get_Key_3() const { return ___Key_3; }
	inline int32_t* get_address_of_Key_3() { return &___Key_3; }
	inline void set_Key_3(int32_t value)
	{
		___Key_3 = value;
	}

	inline static int32_t get_offset_of_Shift_4() { return static_cast<int32_t>(offsetof(KeyboardShortcut_t2512121171, ___Shift_4)); }
	inline bool get_Shift_4() const { return ___Shift_4; }
	inline bool* get_address_of_Shift_4() { return &___Shift_4; }
	inline void set_Shift_4(bool value)
	{
		___Shift_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
