﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonInputUI
struct  ButtonInputUI_t3197132886  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ButtonInputUI::getButtonDownText
	Text_t356221433 * ___getButtonDownText_2;
	// UnityEngine.UI.Text ButtonInputUI::getButtonText
	Text_t356221433 * ___getButtonText_3;
	// UnityEngine.UI.Text ButtonInputUI::getButtonTimeText
	Text_t356221433 * ___getButtonTimeText_4;
	// UnityEngine.UI.Text ButtonInputUI::getButtonUpText
	Text_t356221433 * ___getButtonUpText_5;

public:
	inline static int32_t get_offset_of_getButtonDownText_2() { return static_cast<int32_t>(offsetof(ButtonInputUI_t3197132886, ___getButtonDownText_2)); }
	inline Text_t356221433 * get_getButtonDownText_2() const { return ___getButtonDownText_2; }
	inline Text_t356221433 ** get_address_of_getButtonDownText_2() { return &___getButtonDownText_2; }
	inline void set_getButtonDownText_2(Text_t356221433 * value)
	{
		___getButtonDownText_2 = value;
		Il2CppCodeGenWriteBarrier(&___getButtonDownText_2, value);
	}

	inline static int32_t get_offset_of_getButtonText_3() { return static_cast<int32_t>(offsetof(ButtonInputUI_t3197132886, ___getButtonText_3)); }
	inline Text_t356221433 * get_getButtonText_3() const { return ___getButtonText_3; }
	inline Text_t356221433 ** get_address_of_getButtonText_3() { return &___getButtonText_3; }
	inline void set_getButtonText_3(Text_t356221433 * value)
	{
		___getButtonText_3 = value;
		Il2CppCodeGenWriteBarrier(&___getButtonText_3, value);
	}

	inline static int32_t get_offset_of_getButtonTimeText_4() { return static_cast<int32_t>(offsetof(ButtonInputUI_t3197132886, ___getButtonTimeText_4)); }
	inline Text_t356221433 * get_getButtonTimeText_4() const { return ___getButtonTimeText_4; }
	inline Text_t356221433 ** get_address_of_getButtonTimeText_4() { return &___getButtonTimeText_4; }
	inline void set_getButtonTimeText_4(Text_t356221433 * value)
	{
		___getButtonTimeText_4 = value;
		Il2CppCodeGenWriteBarrier(&___getButtonTimeText_4, value);
	}

	inline static int32_t get_offset_of_getButtonUpText_5() { return static_cast<int32_t>(offsetof(ButtonInputUI_t3197132886, ___getButtonUpText_5)); }
	inline Text_t356221433 * get_getButtonUpText_5() const { return ___getButtonUpText_5; }
	inline Text_t356221433 ** get_address_of_getButtonUpText_5() { return &___getButtonUpText_5; }
	inline void set_getButtonUpText_5(Text_t356221433 * value)
	{
		___getButtonUpText_5 = value;
		Il2CppCodeGenWriteBarrier(&___getButtonUpText_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
