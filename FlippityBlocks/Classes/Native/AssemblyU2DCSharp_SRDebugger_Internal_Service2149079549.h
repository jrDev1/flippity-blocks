﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.Services.IConsoleService
struct IConsoleService_t2268660775;
// SRDebugger.Services.IDebugPanelService
struct IDebugPanelService_t763743455;
// SRDebugger.Services.IDebugTriggerService
struct IDebugTriggerService_t4294816349;
// SRDebugger.Services.IPinnedUIService
struct IPinnedUIService_t3740776164;
// SRDebugger.Services.IDebugCameraService
struct IDebugCameraService_t2503051566;
// SRDebugger.Services.IOptionsService
struct IOptionsService_t4289539304;
// SRDebugger.Services.IDockConsoleService
struct IDockConsoleService_t4027937554;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Internal.Service
struct  Service_t2149079549  : public Il2CppObject
{
public:

public:
};

struct Service_t2149079549_StaticFields
{
public:
	// SRDebugger.Services.IConsoleService SRDebugger.Internal.Service::_consoleService
	Il2CppObject * ____consoleService_0;
	// SRDebugger.Services.IDebugPanelService SRDebugger.Internal.Service::_debugPanelService
	Il2CppObject * ____debugPanelService_1;
	// SRDebugger.Services.IDebugTriggerService SRDebugger.Internal.Service::_debugTriggerService
	Il2CppObject * ____debugTriggerService_2;
	// SRDebugger.Services.IPinnedUIService SRDebugger.Internal.Service::_pinnedUiService
	Il2CppObject * ____pinnedUiService_3;
	// SRDebugger.Services.IDebugCameraService SRDebugger.Internal.Service::_debugCameraService
	Il2CppObject * ____debugCameraService_4;
	// SRDebugger.Services.IOptionsService SRDebugger.Internal.Service::_optionsService
	Il2CppObject * ____optionsService_5;
	// SRDebugger.Services.IDockConsoleService SRDebugger.Internal.Service::_dockConsoleService
	Il2CppObject * ____dockConsoleService_6;

public:
	inline static int32_t get_offset_of__consoleService_0() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____consoleService_0)); }
	inline Il2CppObject * get__consoleService_0() const { return ____consoleService_0; }
	inline Il2CppObject ** get_address_of__consoleService_0() { return &____consoleService_0; }
	inline void set__consoleService_0(Il2CppObject * value)
	{
		____consoleService_0 = value;
		Il2CppCodeGenWriteBarrier(&____consoleService_0, value);
	}

	inline static int32_t get_offset_of__debugPanelService_1() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____debugPanelService_1)); }
	inline Il2CppObject * get__debugPanelService_1() const { return ____debugPanelService_1; }
	inline Il2CppObject ** get_address_of__debugPanelService_1() { return &____debugPanelService_1; }
	inline void set__debugPanelService_1(Il2CppObject * value)
	{
		____debugPanelService_1 = value;
		Il2CppCodeGenWriteBarrier(&____debugPanelService_1, value);
	}

	inline static int32_t get_offset_of__debugTriggerService_2() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____debugTriggerService_2)); }
	inline Il2CppObject * get__debugTriggerService_2() const { return ____debugTriggerService_2; }
	inline Il2CppObject ** get_address_of__debugTriggerService_2() { return &____debugTriggerService_2; }
	inline void set__debugTriggerService_2(Il2CppObject * value)
	{
		____debugTriggerService_2 = value;
		Il2CppCodeGenWriteBarrier(&____debugTriggerService_2, value);
	}

	inline static int32_t get_offset_of__pinnedUiService_3() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____pinnedUiService_3)); }
	inline Il2CppObject * get__pinnedUiService_3() const { return ____pinnedUiService_3; }
	inline Il2CppObject ** get_address_of__pinnedUiService_3() { return &____pinnedUiService_3; }
	inline void set__pinnedUiService_3(Il2CppObject * value)
	{
		____pinnedUiService_3 = value;
		Il2CppCodeGenWriteBarrier(&____pinnedUiService_3, value);
	}

	inline static int32_t get_offset_of__debugCameraService_4() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____debugCameraService_4)); }
	inline Il2CppObject * get__debugCameraService_4() const { return ____debugCameraService_4; }
	inline Il2CppObject ** get_address_of__debugCameraService_4() { return &____debugCameraService_4; }
	inline void set__debugCameraService_4(Il2CppObject * value)
	{
		____debugCameraService_4 = value;
		Il2CppCodeGenWriteBarrier(&____debugCameraService_4, value);
	}

	inline static int32_t get_offset_of__optionsService_5() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____optionsService_5)); }
	inline Il2CppObject * get__optionsService_5() const { return ____optionsService_5; }
	inline Il2CppObject ** get_address_of__optionsService_5() { return &____optionsService_5; }
	inline void set__optionsService_5(Il2CppObject * value)
	{
		____optionsService_5 = value;
		Il2CppCodeGenWriteBarrier(&____optionsService_5, value);
	}

	inline static int32_t get_offset_of__dockConsoleService_6() { return static_cast<int32_t>(offsetof(Service_t2149079549_StaticFields, ____dockConsoleService_6)); }
	inline Il2CppObject * get__dockConsoleService_6() const { return ____dockConsoleService_6; }
	inline Il2CppObject ** get_address_of__dockConsoleService_6() { return &____dockConsoleService_6; }
	inline void set__dockConsoleService_6(Il2CppObject * value)
	{
		____dockConsoleService_6 = value;
		Il2CppCodeGenWriteBarrier(&____dockConsoleService_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
