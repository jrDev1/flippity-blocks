﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"

// SRF.SRList`1<System.Object>
struct SRList_1_t2682079876;
// SRF.SRList`1<System.Int32>
struct SRList_1_t2064508029;
// SRF.SRList`1<SRF.UI.Layout.VirtualVerticalLayoutGroup/Row>
struct SRList_1_t718200173;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// System.Object
struct Il2CppObject;
// SRF.UI.Layout.VirtualVerticalLayoutGroup/SelectedItemChangedEvent
struct SelectedItemChangedEvent_t4048256138;
// SRF.UI.StyleSheet
struct StyleSheet_t2708161410;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.Layout.VirtualVerticalLayoutGroup
struct  VirtualVerticalLayoutGroup_t4077716920  : public LayoutGroup_t3962498969
{
public:
	// SRF.SRList`1<System.Object> SRF.UI.Layout.VirtualVerticalLayoutGroup::_itemList
	SRList_1_t2682079876 * ____itemList_10;
	// SRF.SRList`1<System.Int32> SRF.UI.Layout.VirtualVerticalLayoutGroup::_visibleItemList
	SRList_1_t2064508029 * ____visibleItemList_11;
	// System.Boolean SRF.UI.Layout.VirtualVerticalLayoutGroup::_isDirty
	bool ____isDirty_12;
	// SRF.SRList`1<SRF.UI.Layout.VirtualVerticalLayoutGroup/Row> SRF.UI.Layout.VirtualVerticalLayoutGroup::_rowCache
	SRList_1_t718200173 * ____rowCache_13;
	// UnityEngine.UI.ScrollRect SRF.UI.Layout.VirtualVerticalLayoutGroup::_scrollRect
	ScrollRect_t1199013257 * ____scrollRect_14;
	// System.Int32 SRF.UI.Layout.VirtualVerticalLayoutGroup::_selectedIndex
	int32_t ____selectedIndex_15;
	// System.Object SRF.UI.Layout.VirtualVerticalLayoutGroup::_selectedItem
	Il2CppObject * ____selectedItem_16;
	// SRF.UI.Layout.VirtualVerticalLayoutGroup/SelectedItemChangedEvent SRF.UI.Layout.VirtualVerticalLayoutGroup::_selectedItemChanged
	SelectedItemChangedEvent_t4048256138 * ____selectedItemChanged_17;
	// System.Int32 SRF.UI.Layout.VirtualVerticalLayoutGroup::_visibleItemCount
	int32_t ____visibleItemCount_18;
	// SRF.SRList`1<SRF.UI.Layout.VirtualVerticalLayoutGroup/Row> SRF.UI.Layout.VirtualVerticalLayoutGroup::_visibleRows
	SRList_1_t718200173 * ____visibleRows_19;
	// SRF.UI.StyleSheet SRF.UI.Layout.VirtualVerticalLayoutGroup::AltRowStyleSheet
	StyleSheet_t2708161410 * ___AltRowStyleSheet_20;
	// System.Boolean SRF.UI.Layout.VirtualVerticalLayoutGroup::EnableSelection
	bool ___EnableSelection_21;
	// UnityEngine.RectTransform SRF.UI.Layout.VirtualVerticalLayoutGroup::ItemPrefab
	RectTransform_t3349966182 * ___ItemPrefab_22;
	// System.Int32 SRF.UI.Layout.VirtualVerticalLayoutGroup::RowPadding
	int32_t ___RowPadding_23;
	// SRF.UI.StyleSheet SRF.UI.Layout.VirtualVerticalLayoutGroup::RowStyleSheet
	StyleSheet_t2708161410 * ___RowStyleSheet_24;
	// SRF.UI.StyleSheet SRF.UI.Layout.VirtualVerticalLayoutGroup::SelectedRowStyleSheet
	StyleSheet_t2708161410 * ___SelectedRowStyleSheet_25;
	// System.Single SRF.UI.Layout.VirtualVerticalLayoutGroup::Spacing
	float ___Spacing_26;
	// System.Boolean SRF.UI.Layout.VirtualVerticalLayoutGroup::StickToBottom
	bool ___StickToBottom_27;
	// System.Single SRF.UI.Layout.VirtualVerticalLayoutGroup::_itemHeight
	float ____itemHeight_28;

public:
	inline static int32_t get_offset_of__itemList_10() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____itemList_10)); }
	inline SRList_1_t2682079876 * get__itemList_10() const { return ____itemList_10; }
	inline SRList_1_t2682079876 ** get_address_of__itemList_10() { return &____itemList_10; }
	inline void set__itemList_10(SRList_1_t2682079876 * value)
	{
		____itemList_10 = value;
		Il2CppCodeGenWriteBarrier(&____itemList_10, value);
	}

	inline static int32_t get_offset_of__visibleItemList_11() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____visibleItemList_11)); }
	inline SRList_1_t2064508029 * get__visibleItemList_11() const { return ____visibleItemList_11; }
	inline SRList_1_t2064508029 ** get_address_of__visibleItemList_11() { return &____visibleItemList_11; }
	inline void set__visibleItemList_11(SRList_1_t2064508029 * value)
	{
		____visibleItemList_11 = value;
		Il2CppCodeGenWriteBarrier(&____visibleItemList_11, value);
	}

	inline static int32_t get_offset_of__isDirty_12() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____isDirty_12)); }
	inline bool get__isDirty_12() const { return ____isDirty_12; }
	inline bool* get_address_of__isDirty_12() { return &____isDirty_12; }
	inline void set__isDirty_12(bool value)
	{
		____isDirty_12 = value;
	}

	inline static int32_t get_offset_of__rowCache_13() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____rowCache_13)); }
	inline SRList_1_t718200173 * get__rowCache_13() const { return ____rowCache_13; }
	inline SRList_1_t718200173 ** get_address_of__rowCache_13() { return &____rowCache_13; }
	inline void set__rowCache_13(SRList_1_t718200173 * value)
	{
		____rowCache_13 = value;
		Il2CppCodeGenWriteBarrier(&____rowCache_13, value);
	}

	inline static int32_t get_offset_of__scrollRect_14() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____scrollRect_14)); }
	inline ScrollRect_t1199013257 * get__scrollRect_14() const { return ____scrollRect_14; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRect_14() { return &____scrollRect_14; }
	inline void set__scrollRect_14(ScrollRect_t1199013257 * value)
	{
		____scrollRect_14 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRect_14, value);
	}

	inline static int32_t get_offset_of__selectedIndex_15() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____selectedIndex_15)); }
	inline int32_t get__selectedIndex_15() const { return ____selectedIndex_15; }
	inline int32_t* get_address_of__selectedIndex_15() { return &____selectedIndex_15; }
	inline void set__selectedIndex_15(int32_t value)
	{
		____selectedIndex_15 = value;
	}

	inline static int32_t get_offset_of__selectedItem_16() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____selectedItem_16)); }
	inline Il2CppObject * get__selectedItem_16() const { return ____selectedItem_16; }
	inline Il2CppObject ** get_address_of__selectedItem_16() { return &____selectedItem_16; }
	inline void set__selectedItem_16(Il2CppObject * value)
	{
		____selectedItem_16 = value;
		Il2CppCodeGenWriteBarrier(&____selectedItem_16, value);
	}

	inline static int32_t get_offset_of__selectedItemChanged_17() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____selectedItemChanged_17)); }
	inline SelectedItemChangedEvent_t4048256138 * get__selectedItemChanged_17() const { return ____selectedItemChanged_17; }
	inline SelectedItemChangedEvent_t4048256138 ** get_address_of__selectedItemChanged_17() { return &____selectedItemChanged_17; }
	inline void set__selectedItemChanged_17(SelectedItemChangedEvent_t4048256138 * value)
	{
		____selectedItemChanged_17 = value;
		Il2CppCodeGenWriteBarrier(&____selectedItemChanged_17, value);
	}

	inline static int32_t get_offset_of__visibleItemCount_18() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____visibleItemCount_18)); }
	inline int32_t get__visibleItemCount_18() const { return ____visibleItemCount_18; }
	inline int32_t* get_address_of__visibleItemCount_18() { return &____visibleItemCount_18; }
	inline void set__visibleItemCount_18(int32_t value)
	{
		____visibleItemCount_18 = value;
	}

	inline static int32_t get_offset_of__visibleRows_19() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____visibleRows_19)); }
	inline SRList_1_t718200173 * get__visibleRows_19() const { return ____visibleRows_19; }
	inline SRList_1_t718200173 ** get_address_of__visibleRows_19() { return &____visibleRows_19; }
	inline void set__visibleRows_19(SRList_1_t718200173 * value)
	{
		____visibleRows_19 = value;
		Il2CppCodeGenWriteBarrier(&____visibleRows_19, value);
	}

	inline static int32_t get_offset_of_AltRowStyleSheet_20() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___AltRowStyleSheet_20)); }
	inline StyleSheet_t2708161410 * get_AltRowStyleSheet_20() const { return ___AltRowStyleSheet_20; }
	inline StyleSheet_t2708161410 ** get_address_of_AltRowStyleSheet_20() { return &___AltRowStyleSheet_20; }
	inline void set_AltRowStyleSheet_20(StyleSheet_t2708161410 * value)
	{
		___AltRowStyleSheet_20 = value;
		Il2CppCodeGenWriteBarrier(&___AltRowStyleSheet_20, value);
	}

	inline static int32_t get_offset_of_EnableSelection_21() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___EnableSelection_21)); }
	inline bool get_EnableSelection_21() const { return ___EnableSelection_21; }
	inline bool* get_address_of_EnableSelection_21() { return &___EnableSelection_21; }
	inline void set_EnableSelection_21(bool value)
	{
		___EnableSelection_21 = value;
	}

	inline static int32_t get_offset_of_ItemPrefab_22() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___ItemPrefab_22)); }
	inline RectTransform_t3349966182 * get_ItemPrefab_22() const { return ___ItemPrefab_22; }
	inline RectTransform_t3349966182 ** get_address_of_ItemPrefab_22() { return &___ItemPrefab_22; }
	inline void set_ItemPrefab_22(RectTransform_t3349966182 * value)
	{
		___ItemPrefab_22 = value;
		Il2CppCodeGenWriteBarrier(&___ItemPrefab_22, value);
	}

	inline static int32_t get_offset_of_RowPadding_23() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___RowPadding_23)); }
	inline int32_t get_RowPadding_23() const { return ___RowPadding_23; }
	inline int32_t* get_address_of_RowPadding_23() { return &___RowPadding_23; }
	inline void set_RowPadding_23(int32_t value)
	{
		___RowPadding_23 = value;
	}

	inline static int32_t get_offset_of_RowStyleSheet_24() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___RowStyleSheet_24)); }
	inline StyleSheet_t2708161410 * get_RowStyleSheet_24() const { return ___RowStyleSheet_24; }
	inline StyleSheet_t2708161410 ** get_address_of_RowStyleSheet_24() { return &___RowStyleSheet_24; }
	inline void set_RowStyleSheet_24(StyleSheet_t2708161410 * value)
	{
		___RowStyleSheet_24 = value;
		Il2CppCodeGenWriteBarrier(&___RowStyleSheet_24, value);
	}

	inline static int32_t get_offset_of_SelectedRowStyleSheet_25() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___SelectedRowStyleSheet_25)); }
	inline StyleSheet_t2708161410 * get_SelectedRowStyleSheet_25() const { return ___SelectedRowStyleSheet_25; }
	inline StyleSheet_t2708161410 ** get_address_of_SelectedRowStyleSheet_25() { return &___SelectedRowStyleSheet_25; }
	inline void set_SelectedRowStyleSheet_25(StyleSheet_t2708161410 * value)
	{
		___SelectedRowStyleSheet_25 = value;
		Il2CppCodeGenWriteBarrier(&___SelectedRowStyleSheet_25, value);
	}

	inline static int32_t get_offset_of_Spacing_26() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___Spacing_26)); }
	inline float get_Spacing_26() const { return ___Spacing_26; }
	inline float* get_address_of_Spacing_26() { return &___Spacing_26; }
	inline void set_Spacing_26(float value)
	{
		___Spacing_26 = value;
	}

	inline static int32_t get_offset_of_StickToBottom_27() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ___StickToBottom_27)); }
	inline bool get_StickToBottom_27() const { return ___StickToBottom_27; }
	inline bool* get_address_of_StickToBottom_27() { return &___StickToBottom_27; }
	inline void set_StickToBottom_27(bool value)
	{
		___StickToBottom_27 = value;
	}

	inline static int32_t get_offset_of__itemHeight_28() { return static_cast<int32_t>(offsetof(VirtualVerticalLayoutGroup_t4077716920, ____itemHeight_28)); }
	inline float get__itemHeight_28() const { return ____itemHeight_28; }
	inline float* get_address_of__itemHeight_28() { return &____itemHeight_28; }
	inline void set__itemHeight_28(float value)
	{
		____itemHeight_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
