﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleSpawner
struct  SimpleSpawner_t3347155754  : public MonoBehaviour_t1158329972
{
public:
	// System.String SimpleSpawner::poolName
	String_t* ___poolName_2;
	// UnityEngine.Transform SimpleSpawner::testPrefab
	Transform_t3275118058 * ___testPrefab_3;
	// System.Int32 SimpleSpawner::spawnAmount
	int32_t ___spawnAmount_4;
	// System.Single SimpleSpawner::spawnInterval
	float ___spawnInterval_5;
	// System.String SimpleSpawner::particlesPoolName
	String_t* ___particlesPoolName_6;
	// UnityEngine.ParticleSystem SimpleSpawner::particleSystemPrefab
	ParticleSystem_t3394631041 * ___particleSystemPrefab_7;

public:
	inline static int32_t get_offset_of_poolName_2() { return static_cast<int32_t>(offsetof(SimpleSpawner_t3347155754, ___poolName_2)); }
	inline String_t* get_poolName_2() const { return ___poolName_2; }
	inline String_t** get_address_of_poolName_2() { return &___poolName_2; }
	inline void set_poolName_2(String_t* value)
	{
		___poolName_2 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_2, value);
	}

	inline static int32_t get_offset_of_testPrefab_3() { return static_cast<int32_t>(offsetof(SimpleSpawner_t3347155754, ___testPrefab_3)); }
	inline Transform_t3275118058 * get_testPrefab_3() const { return ___testPrefab_3; }
	inline Transform_t3275118058 ** get_address_of_testPrefab_3() { return &___testPrefab_3; }
	inline void set_testPrefab_3(Transform_t3275118058 * value)
	{
		___testPrefab_3 = value;
		Il2CppCodeGenWriteBarrier(&___testPrefab_3, value);
	}

	inline static int32_t get_offset_of_spawnAmount_4() { return static_cast<int32_t>(offsetof(SimpleSpawner_t3347155754, ___spawnAmount_4)); }
	inline int32_t get_spawnAmount_4() const { return ___spawnAmount_4; }
	inline int32_t* get_address_of_spawnAmount_4() { return &___spawnAmount_4; }
	inline void set_spawnAmount_4(int32_t value)
	{
		___spawnAmount_4 = value;
	}

	inline static int32_t get_offset_of_spawnInterval_5() { return static_cast<int32_t>(offsetof(SimpleSpawner_t3347155754, ___spawnInterval_5)); }
	inline float get_spawnInterval_5() const { return ___spawnInterval_5; }
	inline float* get_address_of_spawnInterval_5() { return &___spawnInterval_5; }
	inline void set_spawnInterval_5(float value)
	{
		___spawnInterval_5 = value;
	}

	inline static int32_t get_offset_of_particlesPoolName_6() { return static_cast<int32_t>(offsetof(SimpleSpawner_t3347155754, ___particlesPoolName_6)); }
	inline String_t* get_particlesPoolName_6() const { return ___particlesPoolName_6; }
	inline String_t** get_address_of_particlesPoolName_6() { return &___particlesPoolName_6; }
	inline void set_particlesPoolName_6(String_t* value)
	{
		___particlesPoolName_6 = value;
		Il2CppCodeGenWriteBarrier(&___particlesPoolName_6, value);
	}

	inline static int32_t get_offset_of_particleSystemPrefab_7() { return static_cast<int32_t>(offsetof(SimpleSpawner_t3347155754, ___particleSystemPrefab_7)); }
	inline ParticleSystem_t3394631041 * get_particleSystemPrefab_7() const { return ___particleSystemPrefab_7; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystemPrefab_7() { return &___particleSystemPrefab_7; }
	inline void set_particleSystemPrefab_7(ParticleSystem_t3394631041 * value)
	{
		___particleSystemPrefab_7 = value;
		Il2CppCodeGenWriteBarrier(&___particleSystemPrefab_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
