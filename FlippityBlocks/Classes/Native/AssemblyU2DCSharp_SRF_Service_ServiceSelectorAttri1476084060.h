﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Attribute542643598.h"

// System.Type
struct Type_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.Service.ServiceSelectorAttribute
struct  ServiceSelectorAttribute_t1476084060  : public Attribute_t542643598
{
public:
	// System.Type SRF.Service.ServiceSelectorAttribute::<ServiceType>k__BackingField
	Type_t * ___U3CServiceTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CServiceTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ServiceSelectorAttribute_t1476084060, ___U3CServiceTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CServiceTypeU3Ek__BackingField_0() const { return ___U3CServiceTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CServiceTypeU3Ek__BackingField_0() { return &___U3CServiceTypeU3Ek__BackingField_0; }
	inline void set_U3CServiceTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CServiceTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CServiceTypeU3Ek__BackingField_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
