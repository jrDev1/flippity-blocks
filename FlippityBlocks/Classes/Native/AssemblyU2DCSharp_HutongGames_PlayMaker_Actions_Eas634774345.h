﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea1629559380.h"
#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ea2590907265.h"

// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchQuickTap
struct  EasyTouchQuickTap_t634774345  : public EasyTouchQuickFSM_t1629559380
{
public:
	// HutongGames.PlayMaker.Actions.EasyTouchQuickTap/ActionTriggering HutongGames.PlayMaker.Actions.EasyTouchQuickTap::actionTriggering
	int32_t ___actionTriggering_19;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchQuickTap::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_20;

public:
	inline static int32_t get_offset_of_actionTriggering_19() { return static_cast<int32_t>(offsetof(EasyTouchQuickTap_t634774345, ___actionTriggering_19)); }
	inline int32_t get_actionTriggering_19() const { return ___actionTriggering_19; }
	inline int32_t* get_address_of_actionTriggering_19() { return &___actionTriggering_19; }
	inline void set_actionTriggering_19(int32_t value)
	{
		___actionTriggering_19 = value;
	}

	inline static int32_t get_offset_of_proxy_20() { return static_cast<int32_t>(offsetof(EasyTouchQuickTap_t634774345, ___proxy_20)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_20() const { return ___proxy_20; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_20() { return &___proxy_20; }
	inline void set_proxy_20(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_20 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
