﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGetTouchCount
struct  EasyTouchGetTouchCount_t1024506187  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.EasyTouchGetTouchCount::touchCount
	FsmInt_t1273009179 * ___touchCount_11;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EasyTouchGetTouchCount::everyFrame
	FsmBool_t664485696 * ___everyFrame_12;

public:
	inline static int32_t get_offset_of_touchCount_11() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchCount_t1024506187, ___touchCount_11)); }
	inline FsmInt_t1273009179 * get_touchCount_11() const { return ___touchCount_11; }
	inline FsmInt_t1273009179 ** get_address_of_touchCount_11() { return &___touchCount_11; }
	inline void set_touchCount_11(FsmInt_t1273009179 * value)
	{
		___touchCount_11 = value;
		Il2CppCodeGenWriteBarrier(&___touchCount_11, value);
	}

	inline static int32_t get_offset_of_everyFrame_12() { return static_cast<int32_t>(offsetof(EasyTouchGetTouchCount_t1024506187, ___everyFrame_12)); }
	inline FsmBool_t664485696 * get_everyFrame_12() const { return ___everyFrame_12; }
	inline FsmBool_t664485696 ** get_address_of_everyFrame_12() { return &___everyFrame_12; }
	inline void set_everyFrame_12(FsmBool_t664485696 * value)
	{
		___everyFrame_12 = value;
		Il2CppCodeGenWriteBarrier(&___everyFrame_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
