﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.PmtCreatePrefabPool
struct  PmtCreatePrefabPool_t2999219353  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::poolName
	FsmString_t2414474701 * ___poolName_11;
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::prefab
	FsmGameObject_t3097142863 * ___prefab_12;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::preloadAmount
	FsmInt_t1273009179 * ___preloadAmount_13;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::limitInstances
	FsmBool_t664485696 * ___limitInstances_14;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::limitAmount
	FsmInt_t1273009179 * ___limitAmount_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::cullDespawned
	FsmBool_t664485696 * ___cullDespawned_16;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::cullAbove
	FsmInt_t1273009179 * ___cullAbove_17;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::cullDelay
	FsmInt_t1273009179 * ___cullDelay_18;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::successEvent
	FsmEvent_t1258573736 * ___successEvent_19;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::alreadyExistsEvent
	FsmEvent_t1258573736 * ___alreadyExistsEvent_20;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::failureEvent
	FsmEvent_t1258573736 * ___failureEvent_21;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::preloadedAmount
	FsmInt_t1273009179 * ___preloadedAmount_22;
	// PathologicalGames.SpawnPool HutongGames.PlayMaker.Actions.PmtCreatePrefabPool::_pool
	SpawnPool_t2419717525 * ____pool_23;

public:
	inline static int32_t get_offset_of_poolName_11() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___poolName_11)); }
	inline FsmString_t2414474701 * get_poolName_11() const { return ___poolName_11; }
	inline FsmString_t2414474701 ** get_address_of_poolName_11() { return &___poolName_11; }
	inline void set_poolName_11(FsmString_t2414474701 * value)
	{
		___poolName_11 = value;
		Il2CppCodeGenWriteBarrier(&___poolName_11, value);
	}

	inline static int32_t get_offset_of_prefab_12() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___prefab_12)); }
	inline FsmGameObject_t3097142863 * get_prefab_12() const { return ___prefab_12; }
	inline FsmGameObject_t3097142863 ** get_address_of_prefab_12() { return &___prefab_12; }
	inline void set_prefab_12(FsmGameObject_t3097142863 * value)
	{
		___prefab_12 = value;
		Il2CppCodeGenWriteBarrier(&___prefab_12, value);
	}

	inline static int32_t get_offset_of_preloadAmount_13() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___preloadAmount_13)); }
	inline FsmInt_t1273009179 * get_preloadAmount_13() const { return ___preloadAmount_13; }
	inline FsmInt_t1273009179 ** get_address_of_preloadAmount_13() { return &___preloadAmount_13; }
	inline void set_preloadAmount_13(FsmInt_t1273009179 * value)
	{
		___preloadAmount_13 = value;
		Il2CppCodeGenWriteBarrier(&___preloadAmount_13, value);
	}

	inline static int32_t get_offset_of_limitInstances_14() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___limitInstances_14)); }
	inline FsmBool_t664485696 * get_limitInstances_14() const { return ___limitInstances_14; }
	inline FsmBool_t664485696 ** get_address_of_limitInstances_14() { return &___limitInstances_14; }
	inline void set_limitInstances_14(FsmBool_t664485696 * value)
	{
		___limitInstances_14 = value;
		Il2CppCodeGenWriteBarrier(&___limitInstances_14, value);
	}

	inline static int32_t get_offset_of_limitAmount_15() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___limitAmount_15)); }
	inline FsmInt_t1273009179 * get_limitAmount_15() const { return ___limitAmount_15; }
	inline FsmInt_t1273009179 ** get_address_of_limitAmount_15() { return &___limitAmount_15; }
	inline void set_limitAmount_15(FsmInt_t1273009179 * value)
	{
		___limitAmount_15 = value;
		Il2CppCodeGenWriteBarrier(&___limitAmount_15, value);
	}

	inline static int32_t get_offset_of_cullDespawned_16() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___cullDespawned_16)); }
	inline FsmBool_t664485696 * get_cullDespawned_16() const { return ___cullDespawned_16; }
	inline FsmBool_t664485696 ** get_address_of_cullDespawned_16() { return &___cullDespawned_16; }
	inline void set_cullDespawned_16(FsmBool_t664485696 * value)
	{
		___cullDespawned_16 = value;
		Il2CppCodeGenWriteBarrier(&___cullDespawned_16, value);
	}

	inline static int32_t get_offset_of_cullAbove_17() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___cullAbove_17)); }
	inline FsmInt_t1273009179 * get_cullAbove_17() const { return ___cullAbove_17; }
	inline FsmInt_t1273009179 ** get_address_of_cullAbove_17() { return &___cullAbove_17; }
	inline void set_cullAbove_17(FsmInt_t1273009179 * value)
	{
		___cullAbove_17 = value;
		Il2CppCodeGenWriteBarrier(&___cullAbove_17, value);
	}

	inline static int32_t get_offset_of_cullDelay_18() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___cullDelay_18)); }
	inline FsmInt_t1273009179 * get_cullDelay_18() const { return ___cullDelay_18; }
	inline FsmInt_t1273009179 ** get_address_of_cullDelay_18() { return &___cullDelay_18; }
	inline void set_cullDelay_18(FsmInt_t1273009179 * value)
	{
		___cullDelay_18 = value;
		Il2CppCodeGenWriteBarrier(&___cullDelay_18, value);
	}

	inline static int32_t get_offset_of_successEvent_19() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___successEvent_19)); }
	inline FsmEvent_t1258573736 * get_successEvent_19() const { return ___successEvent_19; }
	inline FsmEvent_t1258573736 ** get_address_of_successEvent_19() { return &___successEvent_19; }
	inline void set_successEvent_19(FsmEvent_t1258573736 * value)
	{
		___successEvent_19 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_19, value);
	}

	inline static int32_t get_offset_of_alreadyExistsEvent_20() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___alreadyExistsEvent_20)); }
	inline FsmEvent_t1258573736 * get_alreadyExistsEvent_20() const { return ___alreadyExistsEvent_20; }
	inline FsmEvent_t1258573736 ** get_address_of_alreadyExistsEvent_20() { return &___alreadyExistsEvent_20; }
	inline void set_alreadyExistsEvent_20(FsmEvent_t1258573736 * value)
	{
		___alreadyExistsEvent_20 = value;
		Il2CppCodeGenWriteBarrier(&___alreadyExistsEvent_20, value);
	}

	inline static int32_t get_offset_of_failureEvent_21() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___failureEvent_21)); }
	inline FsmEvent_t1258573736 * get_failureEvent_21() const { return ___failureEvent_21; }
	inline FsmEvent_t1258573736 ** get_address_of_failureEvent_21() { return &___failureEvent_21; }
	inline void set_failureEvent_21(FsmEvent_t1258573736 * value)
	{
		___failureEvent_21 = value;
		Il2CppCodeGenWriteBarrier(&___failureEvent_21, value);
	}

	inline static int32_t get_offset_of_preloadedAmount_22() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ___preloadedAmount_22)); }
	inline FsmInt_t1273009179 * get_preloadedAmount_22() const { return ___preloadedAmount_22; }
	inline FsmInt_t1273009179 ** get_address_of_preloadedAmount_22() { return &___preloadedAmount_22; }
	inline void set_preloadedAmount_22(FsmInt_t1273009179 * value)
	{
		___preloadedAmount_22 = value;
		Il2CppCodeGenWriteBarrier(&___preloadedAmount_22, value);
	}

	inline static int32_t get_offset_of__pool_23() { return static_cast<int32_t>(offsetof(PmtCreatePrefabPool_t2999219353, ____pool_23)); }
	inline SpawnPool_t2419717525 * get__pool_23() const { return ____pool_23; }
	inline SpawnPool_t2419717525 ** get_address_of__pool_23() { return &____pool_23; }
	inline void set__pool_23(SpawnPool_t2419717525 * value)
	{
		____pool_23 = value;
		Il2CppCodeGenWriteBarrier(&____pool_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
