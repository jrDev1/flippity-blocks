﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmEnum
struct FsmEnum_t2808516103;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchGet2FingersPickMethod
struct  EasyTouchGet2FingersPickMethod_t621619313  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmEnum HutongGames.PlayMaker.Actions.EasyTouchGet2FingersPickMethod::method
	FsmEnum_t2808516103 * ___method_11;

public:
	inline static int32_t get_offset_of_method_11() { return static_cast<int32_t>(offsetof(EasyTouchGet2FingersPickMethod_t621619313, ___method_11)); }
	inline FsmEnum_t2808516103 * get_method_11() const { return ___method_11; }
	inline FsmEnum_t2808516103 ** get_address_of_method_11() { return &___method_11; }
	inline void set_method_11(FsmEnum_t2808516103 * value)
	{
		___method_11 = value;
		Il2CppCodeGenWriteBarrier(&___method_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
