﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRDebugger_UI_Controls_DataBound3384854171.h"

// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Collections.Generic.Dictionary`2<System.Type,SRDebugger.UI.Controls.Data.NumberControl/ValueRange>
struct Dictionary_2_t3651753542;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// SRF.UI.SRNumberButton
struct SRNumberButton_t836980342;
// SRF.UI.SRNumberSpinner
struct SRNumberSpinner_t2685847661;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Controls.Data.NumberControl
struct  NumberControl_t1939034700  : public DataBoundControl_t3384854171
{
public:
	// System.String SRDebugger.UI.Controls.Data.NumberControl::_lastValue
	String_t* ____lastValue_20;
	// System.Type SRDebugger.UI.Controls.Data.NumberControl::_type
	Type_t * ____type_21;
	// UnityEngine.GameObject[] SRDebugger.UI.Controls.Data.NumberControl::DisableOnReadOnly
	GameObjectU5BU5D_t3057952154* ___DisableOnReadOnly_22;
	// SRF.UI.SRNumberButton SRDebugger.UI.Controls.Data.NumberControl::DownNumberButton
	SRNumberButton_t836980342 * ___DownNumberButton_23;
	// SRF.UI.SRNumberSpinner SRDebugger.UI.Controls.Data.NumberControl::NumberSpinner
	SRNumberSpinner_t2685847661 * ___NumberSpinner_24;
	// UnityEngine.UI.Text SRDebugger.UI.Controls.Data.NumberControl::Title
	Text_t356221433 * ___Title_25;
	// SRF.UI.SRNumberButton SRDebugger.UI.Controls.Data.NumberControl::UpNumberButton
	SRNumberButton_t836980342 * ___UpNumberButton_26;

public:
	inline static int32_t get_offset_of__lastValue_20() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ____lastValue_20)); }
	inline String_t* get__lastValue_20() const { return ____lastValue_20; }
	inline String_t** get_address_of__lastValue_20() { return &____lastValue_20; }
	inline void set__lastValue_20(String_t* value)
	{
		____lastValue_20 = value;
		Il2CppCodeGenWriteBarrier(&____lastValue_20, value);
	}

	inline static int32_t get_offset_of__type_21() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ____type_21)); }
	inline Type_t * get__type_21() const { return ____type_21; }
	inline Type_t ** get_address_of__type_21() { return &____type_21; }
	inline void set__type_21(Type_t * value)
	{
		____type_21 = value;
		Il2CppCodeGenWriteBarrier(&____type_21, value);
	}

	inline static int32_t get_offset_of_DisableOnReadOnly_22() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ___DisableOnReadOnly_22)); }
	inline GameObjectU5BU5D_t3057952154* get_DisableOnReadOnly_22() const { return ___DisableOnReadOnly_22; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_DisableOnReadOnly_22() { return &___DisableOnReadOnly_22; }
	inline void set_DisableOnReadOnly_22(GameObjectU5BU5D_t3057952154* value)
	{
		___DisableOnReadOnly_22 = value;
		Il2CppCodeGenWriteBarrier(&___DisableOnReadOnly_22, value);
	}

	inline static int32_t get_offset_of_DownNumberButton_23() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ___DownNumberButton_23)); }
	inline SRNumberButton_t836980342 * get_DownNumberButton_23() const { return ___DownNumberButton_23; }
	inline SRNumberButton_t836980342 ** get_address_of_DownNumberButton_23() { return &___DownNumberButton_23; }
	inline void set_DownNumberButton_23(SRNumberButton_t836980342 * value)
	{
		___DownNumberButton_23 = value;
		Il2CppCodeGenWriteBarrier(&___DownNumberButton_23, value);
	}

	inline static int32_t get_offset_of_NumberSpinner_24() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ___NumberSpinner_24)); }
	inline SRNumberSpinner_t2685847661 * get_NumberSpinner_24() const { return ___NumberSpinner_24; }
	inline SRNumberSpinner_t2685847661 ** get_address_of_NumberSpinner_24() { return &___NumberSpinner_24; }
	inline void set_NumberSpinner_24(SRNumberSpinner_t2685847661 * value)
	{
		___NumberSpinner_24 = value;
		Il2CppCodeGenWriteBarrier(&___NumberSpinner_24, value);
	}

	inline static int32_t get_offset_of_Title_25() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ___Title_25)); }
	inline Text_t356221433 * get_Title_25() const { return ___Title_25; }
	inline Text_t356221433 ** get_address_of_Title_25() { return &___Title_25; }
	inline void set_Title_25(Text_t356221433 * value)
	{
		___Title_25 = value;
		Il2CppCodeGenWriteBarrier(&___Title_25, value);
	}

	inline static int32_t get_offset_of_UpNumberButton_26() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700, ___UpNumberButton_26)); }
	inline SRNumberButton_t836980342 * get_UpNumberButton_26() const { return ___UpNumberButton_26; }
	inline SRNumberButton_t836980342 ** get_address_of_UpNumberButton_26() { return &___UpNumberButton_26; }
	inline void set_UpNumberButton_26(SRNumberButton_t836980342 * value)
	{
		___UpNumberButton_26 = value;
		Il2CppCodeGenWriteBarrier(&___UpNumberButton_26, value);
	}
};

struct NumberControl_t1939034700_StaticFields
{
public:
	// System.Type[] SRDebugger.UI.Controls.Data.NumberControl::IntegerTypes
	TypeU5BU5D_t1664964607* ___IntegerTypes_17;
	// System.Type[] SRDebugger.UI.Controls.Data.NumberControl::DecimalTypes
	TypeU5BU5D_t1664964607* ___DecimalTypes_18;
	// System.Collections.Generic.Dictionary`2<System.Type,SRDebugger.UI.Controls.Data.NumberControl/ValueRange> SRDebugger.UI.Controls.Data.NumberControl::ValueRanges
	Dictionary_2_t3651753542 * ___ValueRanges_19;

public:
	inline static int32_t get_offset_of_IntegerTypes_17() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700_StaticFields, ___IntegerTypes_17)); }
	inline TypeU5BU5D_t1664964607* get_IntegerTypes_17() const { return ___IntegerTypes_17; }
	inline TypeU5BU5D_t1664964607** get_address_of_IntegerTypes_17() { return &___IntegerTypes_17; }
	inline void set_IntegerTypes_17(TypeU5BU5D_t1664964607* value)
	{
		___IntegerTypes_17 = value;
		Il2CppCodeGenWriteBarrier(&___IntegerTypes_17, value);
	}

	inline static int32_t get_offset_of_DecimalTypes_18() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700_StaticFields, ___DecimalTypes_18)); }
	inline TypeU5BU5D_t1664964607* get_DecimalTypes_18() const { return ___DecimalTypes_18; }
	inline TypeU5BU5D_t1664964607** get_address_of_DecimalTypes_18() { return &___DecimalTypes_18; }
	inline void set_DecimalTypes_18(TypeU5BU5D_t1664964607* value)
	{
		___DecimalTypes_18 = value;
		Il2CppCodeGenWriteBarrier(&___DecimalTypes_18, value);
	}

	inline static int32_t get_offset_of_ValueRanges_19() { return static_cast<int32_t>(offsetof(NumberControl_t1939034700_StaticFields, ___ValueRanges_19)); }
	inline Dictionary_2_t3651753542 * get_ValueRanges_19() const { return ___ValueRanges_19; }
	inline Dictionary_2_t3651753542 ** get_address_of_ValueRanges_19() { return &___ValueRanges_19; }
	inline void set_ValueRanges_19(Dictionary_2_t3651753542 * value)
	{
		___ValueRanges_19 = value;
		Il2CppCodeGenWriteBarrier(&___ValueRanges_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
