﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Action
struct Action_t3226471752;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1865222972;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.BugReportSheetController
struct  BugReportSheetController_t1864182511  : public SRMonoBehaviourEx_t3120278648
{
public:
	// UnityEngine.GameObject SRDebugger.UI.Other.BugReportSheetController::ButtonContainer
	GameObject_t1756533147 * ___ButtonContainer_9;
	// UnityEngine.UI.Text SRDebugger.UI.Other.BugReportSheetController::ButtonText
	Text_t356221433 * ___ButtonText_10;
	// UnityEngine.UI.Button SRDebugger.UI.Other.BugReportSheetController::CancelButton
	Button_t2872111280 * ___CancelButton_11;
	// System.Action SRDebugger.UI.Other.BugReportSheetController::CancelPressed
	Action_t3226471752 * ___CancelPressed_12;
	// UnityEngine.UI.InputField SRDebugger.UI.Other.BugReportSheetController::DescriptionField
	InputField_t1631627530 * ___DescriptionField_13;
	// UnityEngine.UI.InputField SRDebugger.UI.Other.BugReportSheetController::EmailField
	InputField_t1631627530 * ___EmailField_14;
	// UnityEngine.UI.Slider SRDebugger.UI.Other.BugReportSheetController::ProgressBar
	Slider_t297367283 * ___ProgressBar_15;
	// UnityEngine.UI.Text SRDebugger.UI.Other.BugReportSheetController::ResultMessageText
	Text_t356221433 * ___ResultMessageText_16;
	// System.Action SRDebugger.UI.Other.BugReportSheetController::ScreenshotComplete
	Action_t3226471752 * ___ScreenshotComplete_17;
	// UnityEngine.UI.Button SRDebugger.UI.Other.BugReportSheetController::SubmitButton
	Button_t2872111280 * ___SubmitButton_18;
	// System.Action`2<System.Boolean,System.String> SRDebugger.UI.Other.BugReportSheetController::SubmitComplete
	Action_2_t1865222972 * ___SubmitComplete_19;
	// System.Action SRDebugger.UI.Other.BugReportSheetController::TakingScreenshot
	Action_t3226471752 * ___TakingScreenshot_20;

public:
	inline static int32_t get_offset_of_ButtonContainer_9() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___ButtonContainer_9)); }
	inline GameObject_t1756533147 * get_ButtonContainer_9() const { return ___ButtonContainer_9; }
	inline GameObject_t1756533147 ** get_address_of_ButtonContainer_9() { return &___ButtonContainer_9; }
	inline void set_ButtonContainer_9(GameObject_t1756533147 * value)
	{
		___ButtonContainer_9 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonContainer_9, value);
	}

	inline static int32_t get_offset_of_ButtonText_10() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___ButtonText_10)); }
	inline Text_t356221433 * get_ButtonText_10() const { return ___ButtonText_10; }
	inline Text_t356221433 ** get_address_of_ButtonText_10() { return &___ButtonText_10; }
	inline void set_ButtonText_10(Text_t356221433 * value)
	{
		___ButtonText_10 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonText_10, value);
	}

	inline static int32_t get_offset_of_CancelButton_11() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___CancelButton_11)); }
	inline Button_t2872111280 * get_CancelButton_11() const { return ___CancelButton_11; }
	inline Button_t2872111280 ** get_address_of_CancelButton_11() { return &___CancelButton_11; }
	inline void set_CancelButton_11(Button_t2872111280 * value)
	{
		___CancelButton_11 = value;
		Il2CppCodeGenWriteBarrier(&___CancelButton_11, value);
	}

	inline static int32_t get_offset_of_CancelPressed_12() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___CancelPressed_12)); }
	inline Action_t3226471752 * get_CancelPressed_12() const { return ___CancelPressed_12; }
	inline Action_t3226471752 ** get_address_of_CancelPressed_12() { return &___CancelPressed_12; }
	inline void set_CancelPressed_12(Action_t3226471752 * value)
	{
		___CancelPressed_12 = value;
		Il2CppCodeGenWriteBarrier(&___CancelPressed_12, value);
	}

	inline static int32_t get_offset_of_DescriptionField_13() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___DescriptionField_13)); }
	inline InputField_t1631627530 * get_DescriptionField_13() const { return ___DescriptionField_13; }
	inline InputField_t1631627530 ** get_address_of_DescriptionField_13() { return &___DescriptionField_13; }
	inline void set_DescriptionField_13(InputField_t1631627530 * value)
	{
		___DescriptionField_13 = value;
		Il2CppCodeGenWriteBarrier(&___DescriptionField_13, value);
	}

	inline static int32_t get_offset_of_EmailField_14() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___EmailField_14)); }
	inline InputField_t1631627530 * get_EmailField_14() const { return ___EmailField_14; }
	inline InputField_t1631627530 ** get_address_of_EmailField_14() { return &___EmailField_14; }
	inline void set_EmailField_14(InputField_t1631627530 * value)
	{
		___EmailField_14 = value;
		Il2CppCodeGenWriteBarrier(&___EmailField_14, value);
	}

	inline static int32_t get_offset_of_ProgressBar_15() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___ProgressBar_15)); }
	inline Slider_t297367283 * get_ProgressBar_15() const { return ___ProgressBar_15; }
	inline Slider_t297367283 ** get_address_of_ProgressBar_15() { return &___ProgressBar_15; }
	inline void set_ProgressBar_15(Slider_t297367283 * value)
	{
		___ProgressBar_15 = value;
		Il2CppCodeGenWriteBarrier(&___ProgressBar_15, value);
	}

	inline static int32_t get_offset_of_ResultMessageText_16() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___ResultMessageText_16)); }
	inline Text_t356221433 * get_ResultMessageText_16() const { return ___ResultMessageText_16; }
	inline Text_t356221433 ** get_address_of_ResultMessageText_16() { return &___ResultMessageText_16; }
	inline void set_ResultMessageText_16(Text_t356221433 * value)
	{
		___ResultMessageText_16 = value;
		Il2CppCodeGenWriteBarrier(&___ResultMessageText_16, value);
	}

	inline static int32_t get_offset_of_ScreenshotComplete_17() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___ScreenshotComplete_17)); }
	inline Action_t3226471752 * get_ScreenshotComplete_17() const { return ___ScreenshotComplete_17; }
	inline Action_t3226471752 ** get_address_of_ScreenshotComplete_17() { return &___ScreenshotComplete_17; }
	inline void set_ScreenshotComplete_17(Action_t3226471752 * value)
	{
		___ScreenshotComplete_17 = value;
		Il2CppCodeGenWriteBarrier(&___ScreenshotComplete_17, value);
	}

	inline static int32_t get_offset_of_SubmitButton_18() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___SubmitButton_18)); }
	inline Button_t2872111280 * get_SubmitButton_18() const { return ___SubmitButton_18; }
	inline Button_t2872111280 ** get_address_of_SubmitButton_18() { return &___SubmitButton_18; }
	inline void set_SubmitButton_18(Button_t2872111280 * value)
	{
		___SubmitButton_18 = value;
		Il2CppCodeGenWriteBarrier(&___SubmitButton_18, value);
	}

	inline static int32_t get_offset_of_SubmitComplete_19() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___SubmitComplete_19)); }
	inline Action_2_t1865222972 * get_SubmitComplete_19() const { return ___SubmitComplete_19; }
	inline Action_2_t1865222972 ** get_address_of_SubmitComplete_19() { return &___SubmitComplete_19; }
	inline void set_SubmitComplete_19(Action_2_t1865222972 * value)
	{
		___SubmitComplete_19 = value;
		Il2CppCodeGenWriteBarrier(&___SubmitComplete_19, value);
	}

	inline static int32_t get_offset_of_TakingScreenshot_20() { return static_cast<int32_t>(offsetof(BugReportSheetController_t1864182511, ___TakingScreenshot_20)); }
	inline Action_t3226471752 * get_TakingScreenshot_20() const { return ___TakingScreenshot_20; }
	inline Action_t3226471752 ** get_address_of_TakingScreenshot_20() { return &___TakingScreenshot_20; }
	inline void set_TakingScreenshot_20(Action_t3226471752 * value)
	{
		___TakingScreenshot_20 = value;
		Il2CppCodeGenWriteBarrier(&___TakingScreenshot_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
