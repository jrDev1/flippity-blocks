﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.TextMesh
struct TextMesh_t1641806576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleActionExample
struct  SimpleActionExample_t1820476314  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh SimpleActionExample::textMesh
	TextMesh_t1641806576 * ___textMesh_2;
	// UnityEngine.Vector3 SimpleActionExample::startScale
	Vector3_t2243707580  ___startScale_3;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(SimpleActionExample_t1820476314, ___textMesh_2)); }
	inline TextMesh_t1641806576 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1641806576 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_2, value);
	}

	inline static int32_t get_offset_of_startScale_3() { return static_cast<int32_t>(offsetof(SimpleActionExample_t1820476314, ___startScale_3)); }
	inline Vector3_t2243707580  get_startScale_3() const { return ___startScale_3; }
	inline Vector3_t2243707580 * get_address_of_startScale_3() { return &___startScale_3; }
	inline void set_startScale_3(Vector3_t2243707580  value)
	{
		___startScale_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
