﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.ConsoleEntry
struct  ConsoleEntry_t3008967599  : public Il2CppObject
{
public:
	// System.String SRDebugger.Services.ConsoleEntry::_messagePreview
	String_t* ____messagePreview_2;
	// System.String SRDebugger.Services.ConsoleEntry::_stackTracePreview
	String_t* ____stackTracePreview_3;
	// System.Int32 SRDebugger.Services.ConsoleEntry::Count
	int32_t ___Count_4;
	// UnityEngine.LogType SRDebugger.Services.ConsoleEntry::LogType
	int32_t ___LogType_5;
	// System.String SRDebugger.Services.ConsoleEntry::Message
	String_t* ___Message_6;
	// System.String SRDebugger.Services.ConsoleEntry::StackTrace
	String_t* ___StackTrace_7;

public:
	inline static int32_t get_offset_of__messagePreview_2() { return static_cast<int32_t>(offsetof(ConsoleEntry_t3008967599, ____messagePreview_2)); }
	inline String_t* get__messagePreview_2() const { return ____messagePreview_2; }
	inline String_t** get_address_of__messagePreview_2() { return &____messagePreview_2; }
	inline void set__messagePreview_2(String_t* value)
	{
		____messagePreview_2 = value;
		Il2CppCodeGenWriteBarrier(&____messagePreview_2, value);
	}

	inline static int32_t get_offset_of__stackTracePreview_3() { return static_cast<int32_t>(offsetof(ConsoleEntry_t3008967599, ____stackTracePreview_3)); }
	inline String_t* get__stackTracePreview_3() const { return ____stackTracePreview_3; }
	inline String_t** get_address_of__stackTracePreview_3() { return &____stackTracePreview_3; }
	inline void set__stackTracePreview_3(String_t* value)
	{
		____stackTracePreview_3 = value;
		Il2CppCodeGenWriteBarrier(&____stackTracePreview_3, value);
	}

	inline static int32_t get_offset_of_Count_4() { return static_cast<int32_t>(offsetof(ConsoleEntry_t3008967599, ___Count_4)); }
	inline int32_t get_Count_4() const { return ___Count_4; }
	inline int32_t* get_address_of_Count_4() { return &___Count_4; }
	inline void set_Count_4(int32_t value)
	{
		___Count_4 = value;
	}

	inline static int32_t get_offset_of_LogType_5() { return static_cast<int32_t>(offsetof(ConsoleEntry_t3008967599, ___LogType_5)); }
	inline int32_t get_LogType_5() const { return ___LogType_5; }
	inline int32_t* get_address_of_LogType_5() { return &___LogType_5; }
	inline void set_LogType_5(int32_t value)
	{
		___LogType_5 = value;
	}

	inline static int32_t get_offset_of_Message_6() { return static_cast<int32_t>(offsetof(ConsoleEntry_t3008967599, ___Message_6)); }
	inline String_t* get_Message_6() const { return ___Message_6; }
	inline String_t** get_address_of_Message_6() { return &___Message_6; }
	inline void set_Message_6(String_t* value)
	{
		___Message_6 = value;
		Il2CppCodeGenWriteBarrier(&___Message_6, value);
	}

	inline static int32_t get_offset_of_StackTrace_7() { return static_cast<int32_t>(offsetof(ConsoleEntry_t3008967599, ___StackTrace_7)); }
	inline String_t* get_StackTrace_7() const { return ___StackTrace_7; }
	inline String_t** get_address_of_StackTrace_7() { return &___StackTrace_7; }
	inline void set_StackTrace_7(String_t* value)
	{
		___StackTrace_7 = value;
		Il2CppCodeGenWriteBarrier(&___StackTrace_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
