﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiCanvasRaycastFilterEventsProxy
struct  PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerUGuiCanvasRaycastFilterEventsProxy::RayCastingEnabled
	bool ___RayCastingEnabled_2;

public:
	inline static int32_t get_offset_of_RayCastingEnabled_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiCanvasRaycastFilterEventsProxy_t3483117240, ___RayCastingEnabled_2)); }
	inline bool get_RayCastingEnabled_2() const { return ___RayCastingEnabled_2; }
	inline bool* get_address_of_RayCastingEnabled_2() { return &___RayCastingEnabled_2; }
	inline void set_RayCastingEnabled_2(bool value)
	{
		___RayCastingEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
