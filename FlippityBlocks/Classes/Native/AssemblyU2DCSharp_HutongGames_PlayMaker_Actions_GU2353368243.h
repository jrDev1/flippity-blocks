﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmGameObject
struct FsmGameObject_t3097142863;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject
struct  GUILayoutBeginAreaFollowObject_t2353368243  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmGameObject HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::gameObject
	FsmGameObject_t3097142863 * ___gameObject_11;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::offsetLeft
	FsmFloat_t937133978 * ___offsetLeft_12;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::offsetTop
	FsmFloat_t937133978 * ___offsetTop_13;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::width
	FsmFloat_t937133978 * ___width_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::height
	FsmFloat_t937133978 * ___height_15;
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::normalized
	FsmBool_t664485696 * ___normalized_16;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.GUILayoutBeginAreaFollowObject::style
	FsmString_t2414474701 * ___style_17;

public:
	inline static int32_t get_offset_of_gameObject_11() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___gameObject_11)); }
	inline FsmGameObject_t3097142863 * get_gameObject_11() const { return ___gameObject_11; }
	inline FsmGameObject_t3097142863 ** get_address_of_gameObject_11() { return &___gameObject_11; }
	inline void set_gameObject_11(FsmGameObject_t3097142863 * value)
	{
		___gameObject_11 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_11, value);
	}

	inline static int32_t get_offset_of_offsetLeft_12() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___offsetLeft_12)); }
	inline FsmFloat_t937133978 * get_offsetLeft_12() const { return ___offsetLeft_12; }
	inline FsmFloat_t937133978 ** get_address_of_offsetLeft_12() { return &___offsetLeft_12; }
	inline void set_offsetLeft_12(FsmFloat_t937133978 * value)
	{
		___offsetLeft_12 = value;
		Il2CppCodeGenWriteBarrier(&___offsetLeft_12, value);
	}

	inline static int32_t get_offset_of_offsetTop_13() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___offsetTop_13)); }
	inline FsmFloat_t937133978 * get_offsetTop_13() const { return ___offsetTop_13; }
	inline FsmFloat_t937133978 ** get_address_of_offsetTop_13() { return &___offsetTop_13; }
	inline void set_offsetTop_13(FsmFloat_t937133978 * value)
	{
		___offsetTop_13 = value;
		Il2CppCodeGenWriteBarrier(&___offsetTop_13, value);
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___width_14)); }
	inline FsmFloat_t937133978 * get_width_14() const { return ___width_14; }
	inline FsmFloat_t937133978 ** get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(FsmFloat_t937133978 * value)
	{
		___width_14 = value;
		Il2CppCodeGenWriteBarrier(&___width_14, value);
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___height_15)); }
	inline FsmFloat_t937133978 * get_height_15() const { return ___height_15; }
	inline FsmFloat_t937133978 ** get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(FsmFloat_t937133978 * value)
	{
		___height_15 = value;
		Il2CppCodeGenWriteBarrier(&___height_15, value);
	}

	inline static int32_t get_offset_of_normalized_16() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___normalized_16)); }
	inline FsmBool_t664485696 * get_normalized_16() const { return ___normalized_16; }
	inline FsmBool_t664485696 ** get_address_of_normalized_16() { return &___normalized_16; }
	inline void set_normalized_16(FsmBool_t664485696 * value)
	{
		___normalized_16 = value;
		Il2CppCodeGenWriteBarrier(&___normalized_16, value);
	}

	inline static int32_t get_offset_of_style_17() { return static_cast<int32_t>(offsetof(GUILayoutBeginAreaFollowObject_t2353368243, ___style_17)); }
	inline FsmString_t2414474701 * get_style_17() const { return ___style_17; }
	inline FsmString_t2414474701 ** get_address_of_style_17() { return &___style_17; }
	inline void set_style_17(FsmString_t2414474701 * value)
	{
		___style_17 = value;
		Il2CppCodeGenWriteBarrier(&___style_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
