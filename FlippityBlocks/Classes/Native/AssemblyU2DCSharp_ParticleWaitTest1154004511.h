﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleWaitTest
struct  ParticleWaitTest_t1154004511  : public MonoBehaviour_t1158329972
{
public:
	// System.Single ParticleWaitTest::spawnInterval
	float ___spawnInterval_2;
	// System.String ParticleWaitTest::particlesPoolName
	String_t* ___particlesPoolName_3;
	// UnityEngine.ParticleSystem ParticleWaitTest::particleSystemPrefab
	ParticleSystem_t3394631041 * ___particleSystemPrefab_4;

public:
	inline static int32_t get_offset_of_spawnInterval_2() { return static_cast<int32_t>(offsetof(ParticleWaitTest_t1154004511, ___spawnInterval_2)); }
	inline float get_spawnInterval_2() const { return ___spawnInterval_2; }
	inline float* get_address_of_spawnInterval_2() { return &___spawnInterval_2; }
	inline void set_spawnInterval_2(float value)
	{
		___spawnInterval_2 = value;
	}

	inline static int32_t get_offset_of_particlesPoolName_3() { return static_cast<int32_t>(offsetof(ParticleWaitTest_t1154004511, ___particlesPoolName_3)); }
	inline String_t* get_particlesPoolName_3() const { return ___particlesPoolName_3; }
	inline String_t** get_address_of_particlesPoolName_3() { return &___particlesPoolName_3; }
	inline void set_particlesPoolName_3(String_t* value)
	{
		___particlesPoolName_3 = value;
		Il2CppCodeGenWriteBarrier(&___particlesPoolName_3, value);
	}

	inline static int32_t get_offset_of_particleSystemPrefab_4() { return static_cast<int32_t>(offsetof(ParticleWaitTest_t1154004511, ___particleSystemPrefab_4)); }
	inline ParticleSystem_t3394631041 * get_particleSystemPrefab_4() const { return ___particleSystemPrefab_4; }
	inline ParticleSystem_t3394631041 ** get_address_of_particleSystemPrefab_4() { return &___particleSystemPrefab_4; }
	inline void set_particleSystemPrefab_4(ParticleSystem_t3394631041 * value)
	{
		___particleSystemPrefab_4 = value;
		Il2CppCodeGenWriteBarrier(&___particleSystemPrefab_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
