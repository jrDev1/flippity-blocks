﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviourEx3120278648.h"

// System.String
struct String_t;
// SRDebugger.Services.IConsoleService
struct IConsoleService_t2268660775;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.UI.Other.ConsoleTabQuickViewControl
struct  ConsoleTabQuickViewControl_t3410852549  : public SRMonoBehaviourEx_t3120278648
{
public:
	// System.Int32 SRDebugger.UI.Other.ConsoleTabQuickViewControl::_prevErrorCount
	int32_t ____prevErrorCount_11;
	// System.Int32 SRDebugger.UI.Other.ConsoleTabQuickViewControl::_prevInfoCount
	int32_t ____prevInfoCount_12;
	// System.Int32 SRDebugger.UI.Other.ConsoleTabQuickViewControl::_prevWarningCount
	int32_t ____prevWarningCount_13;
	// SRDebugger.Services.IConsoleService SRDebugger.UI.Other.ConsoleTabQuickViewControl::ConsoleService
	Il2CppObject * ___ConsoleService_14;
	// UnityEngine.UI.Text SRDebugger.UI.Other.ConsoleTabQuickViewControl::ErrorCountText
	Text_t356221433 * ___ErrorCountText_15;
	// UnityEngine.UI.Text SRDebugger.UI.Other.ConsoleTabQuickViewControl::InfoCountText
	Text_t356221433 * ___InfoCountText_16;
	// UnityEngine.UI.Text SRDebugger.UI.Other.ConsoleTabQuickViewControl::WarningCountText
	Text_t356221433 * ___WarningCountText_17;

public:
	inline static int32_t get_offset_of__prevErrorCount_11() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ____prevErrorCount_11)); }
	inline int32_t get__prevErrorCount_11() const { return ____prevErrorCount_11; }
	inline int32_t* get_address_of__prevErrorCount_11() { return &____prevErrorCount_11; }
	inline void set__prevErrorCount_11(int32_t value)
	{
		____prevErrorCount_11 = value;
	}

	inline static int32_t get_offset_of__prevInfoCount_12() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ____prevInfoCount_12)); }
	inline int32_t get__prevInfoCount_12() const { return ____prevInfoCount_12; }
	inline int32_t* get_address_of__prevInfoCount_12() { return &____prevInfoCount_12; }
	inline void set__prevInfoCount_12(int32_t value)
	{
		____prevInfoCount_12 = value;
	}

	inline static int32_t get_offset_of__prevWarningCount_13() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ____prevWarningCount_13)); }
	inline int32_t get__prevWarningCount_13() const { return ____prevWarningCount_13; }
	inline int32_t* get_address_of__prevWarningCount_13() { return &____prevWarningCount_13; }
	inline void set__prevWarningCount_13(int32_t value)
	{
		____prevWarningCount_13 = value;
	}

	inline static int32_t get_offset_of_ConsoleService_14() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ___ConsoleService_14)); }
	inline Il2CppObject * get_ConsoleService_14() const { return ___ConsoleService_14; }
	inline Il2CppObject ** get_address_of_ConsoleService_14() { return &___ConsoleService_14; }
	inline void set_ConsoleService_14(Il2CppObject * value)
	{
		___ConsoleService_14 = value;
		Il2CppCodeGenWriteBarrier(&___ConsoleService_14, value);
	}

	inline static int32_t get_offset_of_ErrorCountText_15() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ___ErrorCountText_15)); }
	inline Text_t356221433 * get_ErrorCountText_15() const { return ___ErrorCountText_15; }
	inline Text_t356221433 ** get_address_of_ErrorCountText_15() { return &___ErrorCountText_15; }
	inline void set_ErrorCountText_15(Text_t356221433 * value)
	{
		___ErrorCountText_15 = value;
		Il2CppCodeGenWriteBarrier(&___ErrorCountText_15, value);
	}

	inline static int32_t get_offset_of_InfoCountText_16() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ___InfoCountText_16)); }
	inline Text_t356221433 * get_InfoCountText_16() const { return ___InfoCountText_16; }
	inline Text_t356221433 ** get_address_of_InfoCountText_16() { return &___InfoCountText_16; }
	inline void set_InfoCountText_16(Text_t356221433 * value)
	{
		___InfoCountText_16 = value;
		Il2CppCodeGenWriteBarrier(&___InfoCountText_16, value);
	}

	inline static int32_t get_offset_of_WarningCountText_17() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549, ___WarningCountText_17)); }
	inline Text_t356221433 * get_WarningCountText_17() const { return ___WarningCountText_17; }
	inline Text_t356221433 ** get_address_of_WarningCountText_17() { return &___WarningCountText_17; }
	inline void set_WarningCountText_17(Text_t356221433 * value)
	{
		___WarningCountText_17 = value;
		Il2CppCodeGenWriteBarrier(&___WarningCountText_17, value);
	}
};

struct ConsoleTabQuickViewControl_t3410852549_StaticFields
{
public:
	// System.String SRDebugger.UI.Other.ConsoleTabQuickViewControl::MaxString
	String_t* ___MaxString_10;

public:
	inline static int32_t get_offset_of_MaxString_10() { return static_cast<int32_t>(offsetof(ConsoleTabQuickViewControl_t3410852549_StaticFields, ___MaxString_10)); }
	inline String_t* get_MaxString_10() const { return ___MaxString_10; }
	inline String_t** get_address_of_MaxString_10() { return &___MaxString_10; }
	inline void set_MaxString_10(String_t* value)
	{
		___MaxString_10 = value;
		Il2CppCodeGenWriteBarrier(&___MaxString_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
