﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ha3659156725.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.HashTableAdd
struct  HashTableAdd_t3571497505  : public HashTableActions_t3659156725
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.HashTableAdd::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableAdd::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.HashTableAdd::key
	FsmString_t2414474701 * ___key_14;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.HashTableAdd::variable
	FsmVar_t2872592513 * ___variable_15;
	// System.Boolean HutongGames.PlayMaker.Actions.HashTableAdd::convertIntToByte
	bool ___convertIntToByte_16;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableAdd::successEvent
	FsmEvent_t1258573736 * ___successEvent_17;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.HashTableAdd::keyExistsAlreadyEvent
	FsmEvent_t1258573736 * ___keyExistsAlreadyEvent_18;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_key_14() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___key_14)); }
	inline FsmString_t2414474701 * get_key_14() const { return ___key_14; }
	inline FsmString_t2414474701 ** get_address_of_key_14() { return &___key_14; }
	inline void set_key_14(FsmString_t2414474701 * value)
	{
		___key_14 = value;
		Il2CppCodeGenWriteBarrier(&___key_14, value);
	}

	inline static int32_t get_offset_of_variable_15() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___variable_15)); }
	inline FsmVar_t2872592513 * get_variable_15() const { return ___variable_15; }
	inline FsmVar_t2872592513 ** get_address_of_variable_15() { return &___variable_15; }
	inline void set_variable_15(FsmVar_t2872592513 * value)
	{
		___variable_15 = value;
		Il2CppCodeGenWriteBarrier(&___variable_15, value);
	}

	inline static int32_t get_offset_of_convertIntToByte_16() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___convertIntToByte_16)); }
	inline bool get_convertIntToByte_16() const { return ___convertIntToByte_16; }
	inline bool* get_address_of_convertIntToByte_16() { return &___convertIntToByte_16; }
	inline void set_convertIntToByte_16(bool value)
	{
		___convertIntToByte_16 = value;
	}

	inline static int32_t get_offset_of_successEvent_17() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___successEvent_17)); }
	inline FsmEvent_t1258573736 * get_successEvent_17() const { return ___successEvent_17; }
	inline FsmEvent_t1258573736 ** get_address_of_successEvent_17() { return &___successEvent_17; }
	inline void set_successEvent_17(FsmEvent_t1258573736 * value)
	{
		___successEvent_17 = value;
		Il2CppCodeGenWriteBarrier(&___successEvent_17, value);
	}

	inline static int32_t get_offset_of_keyExistsAlreadyEvent_18() { return static_cast<int32_t>(offsetof(HashTableAdd_t3571497505, ___keyExistsAlreadyEvent_18)); }
	inline FsmEvent_t1258573736 * get_keyExistsAlreadyEvent_18() const { return ___keyExistsAlreadyEvent_18; }
	inline FsmEvent_t1258573736 ** get_address_of_keyExistsAlreadyEvent_18() { return &___keyExistsAlreadyEvent_18; }
	inline void set_keyExistsAlreadyEvent_18(FsmEvent_t1258573736 * value)
	{
		___keyExistsAlreadyEvent_18 = value;
		Il2CppCodeGenWriteBarrier(&___keyExistsAlreadyEvent_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
