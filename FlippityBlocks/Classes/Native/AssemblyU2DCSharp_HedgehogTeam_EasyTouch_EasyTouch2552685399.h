﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// HedgehogTeam.EasyTouch.Finger
struct Finger_t4254747445;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/DoubleTap
struct  DoubleTap_t2552685399  : public Il2CppObject
{
public:
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::inDoubleTap
	bool ___inDoubleTap_0;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::inWait
	bool ___inWait_1;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::time
	float ___time_2;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::count
	int32_t ___count_3;
	// HedgehogTeam.EasyTouch.Finger HedgehogTeam.EasyTouch.EasyTouch/DoubleTap::finger
	Finger_t4254747445 * ___finger_4;

public:
	inline static int32_t get_offset_of_inDoubleTap_0() { return static_cast<int32_t>(offsetof(DoubleTap_t2552685399, ___inDoubleTap_0)); }
	inline bool get_inDoubleTap_0() const { return ___inDoubleTap_0; }
	inline bool* get_address_of_inDoubleTap_0() { return &___inDoubleTap_0; }
	inline void set_inDoubleTap_0(bool value)
	{
		___inDoubleTap_0 = value;
	}

	inline static int32_t get_offset_of_inWait_1() { return static_cast<int32_t>(offsetof(DoubleTap_t2552685399, ___inWait_1)); }
	inline bool get_inWait_1() const { return ___inWait_1; }
	inline bool* get_address_of_inWait_1() { return &___inWait_1; }
	inline void set_inWait_1(bool value)
	{
		___inWait_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(DoubleTap_t2552685399, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(DoubleTap_t2552685399, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_finger_4() { return static_cast<int32_t>(offsetof(DoubleTap_t2552685399, ___finger_4)); }
	inline Finger_t4254747445 * get_finger_4() const { return ___finger_4; }
	inline Finger_t4254747445 ** get_address_of_finger_4() { return &___finger_4; }
	inline void set_finger_4(Finger_t4254747445 * value)
	{
		___finger_4 = value;
		Il2CppCodeGenWriteBarrier(&___finger_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
