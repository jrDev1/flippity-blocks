﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget
struct PlayMakerEventTarget_t2104288969;
// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent
struct PlayMakerEvent_t1571596806;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiDropEventsProxy
struct  PlayMakerUGuiDropEventsProxy_t1929450712  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean PlayMakerUGuiDropEventsProxy::debug
	bool ___debug_2;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEventTarget PlayMakerUGuiDropEventsProxy::eventTarget
	PlayMakerEventTarget_t2104288969 * ___eventTarget_3;
	// HutongGames.PlayMaker.Ecosystem.Utils.PlayMakerEvent PlayMakerUGuiDropEventsProxy::onDropEvent
	PlayMakerEvent_t1571596806 * ___onDropEvent_4;

public:
	inline static int32_t get_offset_of_debug_2() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDropEventsProxy_t1929450712, ___debug_2)); }
	inline bool get_debug_2() const { return ___debug_2; }
	inline bool* get_address_of_debug_2() { return &___debug_2; }
	inline void set_debug_2(bool value)
	{
		___debug_2 = value;
	}

	inline static int32_t get_offset_of_eventTarget_3() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDropEventsProxy_t1929450712, ___eventTarget_3)); }
	inline PlayMakerEventTarget_t2104288969 * get_eventTarget_3() const { return ___eventTarget_3; }
	inline PlayMakerEventTarget_t2104288969 ** get_address_of_eventTarget_3() { return &___eventTarget_3; }
	inline void set_eventTarget_3(PlayMakerEventTarget_t2104288969 * value)
	{
		___eventTarget_3 = value;
		Il2CppCodeGenWriteBarrier(&___eventTarget_3, value);
	}

	inline static int32_t get_offset_of_onDropEvent_4() { return static_cast<int32_t>(offsetof(PlayMakerUGuiDropEventsProxy_t1929450712, ___onDropEvent_4)); }
	inline PlayMakerEvent_t1571596806 * get_onDropEvent_4() const { return ___onDropEvent_4; }
	inline PlayMakerEvent_t1571596806 ** get_address_of_onDropEvent_4() { return &___onDropEvent_4; }
	inline void set_onDropEvent_4(PlayMakerEvent_t1571596806 * value)
	{
		___onDropEvent_4 = value;
		Il2CppCodeGenWriteBarrier(&___onDropEvent_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
