﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRF.UI.Layout.VirtualVerticalLayoutGroup/Row[]
struct RowU5BU5D_t2258440169;
// System.Collections.Generic.EqualityComparer`1<SRF.UI.Layout.VirtualVerticalLayoutGroup/Row>
struct EqualityComparer_1_t3594172159;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SRF.UI.Layout.VirtualVerticalLayoutGroup/Row>
struct ReadOnlyCollection_1_t911355284;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.SRList`1<SRF.UI.Layout.VirtualVerticalLayoutGroup/Row>
struct  SRList_1_t718200173  : public Il2CppObject
{
public:
	// T[] SRF.SRList`1::_buffer
	RowU5BU5D_t2258440169* ____buffer_0;
	// System.Int32 SRF.SRList`1::_count
	int32_t ____count_1;
	// System.Collections.Generic.EqualityComparer`1<T> SRF.SRList`1::_equalityComparer
	EqualityComparer_1_t3594172159 * ____equalityComparer_2;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<T> SRF.SRList`1::_readOnlyWrapper
	ReadOnlyCollection_1_t911355284 * ____readOnlyWrapper_3;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(SRList_1_t718200173, ____buffer_0)); }
	inline RowU5BU5D_t2258440169* get__buffer_0() const { return ____buffer_0; }
	inline RowU5BU5D_t2258440169** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(RowU5BU5D_t2258440169* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier(&____buffer_0, value);
	}

	inline static int32_t get_offset_of__count_1() { return static_cast<int32_t>(offsetof(SRList_1_t718200173, ____count_1)); }
	inline int32_t get__count_1() const { return ____count_1; }
	inline int32_t* get_address_of__count_1() { return &____count_1; }
	inline void set__count_1(int32_t value)
	{
		____count_1 = value;
	}

	inline static int32_t get_offset_of__equalityComparer_2() { return static_cast<int32_t>(offsetof(SRList_1_t718200173, ____equalityComparer_2)); }
	inline EqualityComparer_1_t3594172159 * get__equalityComparer_2() const { return ____equalityComparer_2; }
	inline EqualityComparer_1_t3594172159 ** get_address_of__equalityComparer_2() { return &____equalityComparer_2; }
	inline void set__equalityComparer_2(EqualityComparer_1_t3594172159 * value)
	{
		____equalityComparer_2 = value;
		Il2CppCodeGenWriteBarrier(&____equalityComparer_2, value);
	}

	inline static int32_t get_offset_of__readOnlyWrapper_3() { return static_cast<int32_t>(offsetof(SRList_1_t718200173, ____readOnlyWrapper_3)); }
	inline ReadOnlyCollection_1_t911355284 * get__readOnlyWrapper_3() const { return ____readOnlyWrapper_3; }
	inline ReadOnlyCollection_1_t911355284 ** get_address_of__readOnlyWrapper_3() { return &____readOnlyWrapper_3; }
	inline void set__readOnlyWrapper_3(ReadOnlyCollection_1_t911355284 * value)
	{
		____readOnlyWrapper_3 = value;
		Il2CppCodeGenWriteBarrier(&____readOnlyWrapper_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
