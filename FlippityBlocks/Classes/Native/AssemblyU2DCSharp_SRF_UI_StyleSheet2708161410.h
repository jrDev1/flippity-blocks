﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<SRF.UI.Style>
struct List_1_t948119185;
// SRF.UI.StyleSheet
struct StyleSheet_t2708161410;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.StyleSheet
struct  StyleSheet_t2708161410  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<System.String> SRF.UI.StyleSheet::_keys
	List_1_t1398341365 * ____keys_2;
	// System.Collections.Generic.List`1<SRF.UI.Style> SRF.UI.StyleSheet::_styles
	List_1_t948119185 * ____styles_3;
	// SRF.UI.StyleSheet SRF.UI.StyleSheet::Parent
	StyleSheet_t2708161410 * ___Parent_4;

public:
	inline static int32_t get_offset_of__keys_2() { return static_cast<int32_t>(offsetof(StyleSheet_t2708161410, ____keys_2)); }
	inline List_1_t1398341365 * get__keys_2() const { return ____keys_2; }
	inline List_1_t1398341365 ** get_address_of__keys_2() { return &____keys_2; }
	inline void set__keys_2(List_1_t1398341365 * value)
	{
		____keys_2 = value;
		Il2CppCodeGenWriteBarrier(&____keys_2, value);
	}

	inline static int32_t get_offset_of__styles_3() { return static_cast<int32_t>(offsetof(StyleSheet_t2708161410, ____styles_3)); }
	inline List_1_t948119185 * get__styles_3() const { return ____styles_3; }
	inline List_1_t948119185 ** get_address_of__styles_3() { return &____styles_3; }
	inline void set__styles_3(List_1_t948119185 * value)
	{
		____styles_3 = value;
		Il2CppCodeGenWriteBarrier(&____styles_3, value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(StyleSheet_t2708161410, ___Parent_4)); }
	inline StyleSheet_t2708161410 * get_Parent_4() const { return ___Parent_4; }
	inline StyleSheet_t2708161410 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(StyleSheet_t2708161410 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
