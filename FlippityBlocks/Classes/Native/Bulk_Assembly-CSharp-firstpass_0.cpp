﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_In1084123187.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_In3900751951.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_In3666444239.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Poo707327633.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp1520791760.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Pref85422674.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp2419717525.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2178968864.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "UnityEngine_UnityEngine_SendMessageOptions1414041951.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Pr1896461731.h"
#include "UnityEngine_UnityEngine_MissingReferenceException1416808650.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Pr1067013873.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Pr1121107021.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge894930024.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3378427795.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22947242542.h"
#include "mscorlib_System_Collections_Generic_KeyNotFoundExc1722175009.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3892957163.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2214954726.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Pr3584992064.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3749511102.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3417634846.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2000201936.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp3700441677.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp3092628401.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3284240776.h"
#include "mscorlib_System_Exception1927440687.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_ParticleSystem3394631041.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp4059494550.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp1067573800.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp3614466689.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp2230806469.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp2170770705.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4164115705.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen39529491.h"
#include "AssemblyU2DCSharpU2Dfirstpass_PathologicalGames_Sp2249336443.h"
#include "mscorlib_System_Delegate3022476291.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2523027262.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1359554193.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22091842009.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PathologicalGames.InstanceHandler/InstantiateDelegate
struct InstantiateDelegate_t3900751951;
// System.Object
struct Il2CppObject;
// PathologicalGames.InstanceHandler/DestroyDelegate
struct DestroyDelegate_t3666444239;
// UnityEngine.Object
struct Object_t1021602117;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// PathologicalGames.SpawnPoolsDict
struct SpawnPoolsDict_t1520791760;
// PathologicalGames.PrefabPool
struct PrefabPool_t85422674;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Component
struct Component_t3819376471;
// System.String
struct String_t;
// PathologicalGames.SpawnPool
struct SpawnPool_t2419717525;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Transform>
struct IEnumerable_1_t3567245103;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0
struct U3CCullDespawnedU3Ec__Iterator0_t1896461731;
// UnityEngine.MissingReferenceException
struct MissingReferenceException_t1416808650;
// PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1
struct U3CPreloadOverTimeU3Ec__Iterator1_t1067013873;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// PathologicalGames.PrefabsDict
struct PrefabsDict_t1121107021;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>
struct Dictionary_2_t894930024;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.Transform>
struct KeyCollection_t3378427795;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t470039898;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.NotImplementedException
struct NotImplementedException_t2785117854;
// System.Collections.Generic.KeyNotFoundException
struct KeyNotFoundException_t1722175009;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.ICollection`1<UnityEngine.Transform>
struct ICollection_1_t4227193363;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,UnityEngine.Transform>
struct ValueCollection_t3892957163;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t984569266;
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>[]
struct KeyValuePair_2U5BU5D_t3482534235;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>>
struct IEnumerator_1_t422766369;
// PathologicalGames.PreRuntimePoolItem
struct PreRuntimePoolItem_t3584992064;
// System.Collections.Generic.List`1<PathologicalGames.PrefabPool>
struct List_1_t3749511102;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t3417634846;
// System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.PrefabPool>
struct Dictionary_2_t2000201936;
// PathologicalGames.SpawnPool/InstantiateDelegate
struct InstantiateDelegate_t3700441677;
// PathologicalGames.SpawnPool/DestroyDelegate
struct DestroyDelegate_t3092628401;
// System.Exception
struct Exception_t1927440687;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1
struct U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550;
// PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2
struct U3CListForAudioStopU3Ec__Iterator2_t1067573800;
// PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3
struct U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform>
struct IEnumerator_1_t750641885;
// PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4
struct U3CGetEnumeratorU3Ec__Iterator4_t2230806469;
// PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0
struct U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705;
// System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate>
struct Dictionary_2_t4164115705;
// System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>
struct Dictionary_2_t39529491;
// PathologicalGames.SpawnPoolsDict/OnCreatedDelegate
struct OnCreatedDelegate_t2249336443;
// System.Delegate
struct Delegate_t3022476291;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PathologicalGames.SpawnPool>
struct KeyCollection_t2523027262;
// System.Collections.Generic.ICollection`1<PathologicalGames.SpawnPool>
struct ICollection_1_t3371792830;
// System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>[]
struct KeyValuePair_2U5BU5D_t498405028;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>>
struct IEnumerator_1_t3862333132;
extern Il2CppClass* InstanceHandler_t1084123187_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3186302158_MethodInfo_var;
extern const uint32_t InstanceHandler_InstantiatePrefab_m309003507_MetadataUsageId;
extern const uint32_t InstanceHandler_DestroyInstance_m331796129_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t InstantiateDelegate_BeginInvoke_m2182565173_MetadataUsageId;
extern Il2CppClass* SpawnPoolsDict_t1520791760_il2cpp_TypeInfo_var;
extern Il2CppClass* PoolManager_t707327633_il2cpp_TypeInfo_var;
extern const uint32_t PoolManager__cctor_m740501879_MetadataUsageId;
extern Il2CppClass* List_1_t2644239190_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2292054616_MethodInfo_var;
extern const uint32_t PrefabPool__ctor_m2549027878_MetadataUsageId;
extern const uint32_t PrefabPool__ctor_m3674034909_MetadataUsageId;
extern const uint32_t PrefabPool_inspectorInstanceConstructor_m824700707_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1527845545_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2375004233_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m273259033_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m686407845_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1324927225_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral318652711;
extern const uint32_t PrefabPool_SelfDestruct_m1395427007_MetadataUsageId;
extern const MethodInfo* List_1__ctor_m2538210300_MethodInfo_var;
extern const uint32_t PrefabPool_get_spawned_m365923230_MetadataUsageId;
extern const uint32_t PrefabPool_get_despawned_m3902220117_MetadataUsageId;
extern const MethodInfo* List_1_get_Count_m936427142_MethodInfo_var;
extern const uint32_t PrefabPool_get_totalCount_m1479453083_MetadataUsageId;
extern const MethodInfo* List_1_Remove_m2326856859_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3606265932_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1039352855;
extern Il2CppCodeGenString* _stringLiteral3199955346;
extern const uint32_t PrefabPool_DespawnInstance_m2725861114_MetadataUsageId;
extern Il2CppClass* U3CCullDespawnedU3Ec__Iterator0_t1896461731_il2cpp_TypeInfo_var;
extern const uint32_t PrefabPool_CullDespawned_m3756497222_MetadataUsageId;
extern Il2CppClass* MissingReferenceException_t1416808650_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m563292115_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m4270546658_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4048292431;
extern Il2CppCodeGenString* _stringLiteral448805854;
extern Il2CppCodeGenString* _stringLiteral3734970799;
extern const uint32_t PrefabPool_SpawnInstance_m3748491065_MetadataUsageId;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3760092750;
extern Il2CppCodeGenString* _stringLiteral95200360;
extern const uint32_t PrefabPool_SpawnNew_m1137185856_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t3275118058_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t PrefabPool_SetRecursively_m1140085826_MetadataUsageId;
extern const uint32_t PrefabPool_AddUnpooled_m2140288438_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3321977572;
extern Il2CppCodeGenString* _stringLiteral3789224074;
extern Il2CppCodeGenString* _stringLiteral2943401089;
extern Il2CppCodeGenString* _stringLiteral3649213935;
extern Il2CppCodeGenString* _stringLiteral3361875080;
extern const uint32_t PrefabPool_PreloadInstances_m3084730458_MetadataUsageId;
extern Il2CppClass* U3CPreloadOverTimeU3Ec__Iterator1_t1067013873_il2cpp_TypeInfo_var;
extern const uint32_t PrefabPool_PreloadOverTime_m604118577_MetadataUsageId;
extern const MethodInfo* List_1_Contains_m2540959346_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral885759378;
extern const uint32_t PrefabPool_Contains_m3543239565_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1104291695;
extern const uint32_t PrefabPool_nameInstance_m1724321706_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3800559023;
extern Il2CppCodeGenString* _stringLiteral4274858464;
extern Il2CppCodeGenString* _stringLiteral249778527;
extern Il2CppCodeGenString* _stringLiteral595297118;
extern const uint32_t U3CCullDespawnedU3Ec__Iterator0_MoveNext_m1056465212_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCullDespawnedU3Ec__Iterator0_Reset_m3236476073_MetadataUsageId;
extern const uint32_t U3CPreloadOverTimeU3Ec__Iterator1_MoveNext_m2163773428_MetadataUsageId;
extern const uint32_t U3CPreloadOverTimeU3Ec__Iterator1_Reset_m204175095_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t894930024_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1861750071_MethodInfo_var;
extern const uint32_t PrefabsDict__ctor_m4168763172_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m2934043879_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m2836166341_MethodInfo_var;
extern const MethodInfo* KeyCollection_CopyTo_m1254677401_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1555060386;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern const uint32_t PrefabsDict_ToString_m2224302563_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Add_m3540954367_MethodInfo_var;
extern const uint32_t PrefabsDict__Add_m2864700459_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Remove_m708479600_MethodInfo_var;
extern const uint32_t PrefabsDict__Remove_m2445122851_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Clear_m1781593424_MethodInfo_var;
extern const uint32_t PrefabsDict__Clear_m3612909952_MetadataUsageId;
extern const uint32_t PrefabsDict_get_Count_m3292685014_MetadataUsageId;
extern const MethodInfo* Dictionary_2_ContainsKey_m315213716_MethodInfo_var;
extern const uint32_t PrefabsDict_ContainsKey_m1851170860_MetadataUsageId;
extern const MethodInfo* Dictionary_2_TryGetValue_m3825486608_MethodInfo_var;
extern const uint32_t PrefabsDict_TryGetValue_m3983585977_MetadataUsageId;
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2250443503;
extern const uint32_t PrefabsDict_Add_m3234735052_MetadataUsageId;
extern const uint32_t PrefabsDict_Remove_m4224709130_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral440679323;
extern const uint32_t PrefabsDict_Contains_m3265497923_MetadataUsageId;
extern Il2CppClass* KeyNotFoundException_t1722175009_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m777678513_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral815531349;
extern const uint32_t PrefabsDict_get_Item_m254611088_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral142138905;
extern const uint32_t PrefabsDict_set_Item_m3790954465_MetadataUsageId;
extern const uint32_t PrefabsDict_get_Keys_m473500759_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Values_m1688729445_MethodInfo_var;
extern const uint32_t PrefabsDict_get_Values_m4202167090_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3473335183;
extern const uint32_t PrefabsDict_Add_m443739205_MetadataUsageId;
extern const uint32_t PrefabsDict_Clear_m1374396897_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3818382634;
extern const uint32_t PrefabsDict_CopyTo_m3923091189_MetadataUsageId;
extern const uint32_t PrefabsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CUnityEngine_TransformU3EU3E_CopyTo_m2846345044_MetadataUsageId;
extern const uint32_t PrefabsDict_Remove_m2402712344_MetadataUsageId;
extern Il2CppClass* Enumerator_t2214954726_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2685922707_MethodInfo_var;
extern const uint32_t PrefabsDict_GetEnumerator_m2554566656_MetadataUsageId;
extern const uint32_t PrefabsDict_System_Collections_IEnumerable_GetEnumerator_m1785244313_MetadataUsageId;
extern const uint32_t PreRuntimePoolItem__ctor_m1575543045_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2858802052;
extern const uint32_t PreRuntimePoolItem_Start_m823330565_MetadataUsageId;
extern Il2CppClass* List_1_t3749511102_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3417634846_il2cpp_TypeInfo_var;
extern Il2CppClass* PrefabsDict_t1121107021_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2177220103_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m729743939_MethodInfo_var;
extern const uint32_t SpawnPool__ctor_m783789446_MetadataUsageId;
extern const uint32_t SpawnPool_set_dontDestroyOnLoad_m624769966_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t2000201936_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m723632974_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3756596590_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m3140732121_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m954114191_MethodInfo_var;
extern const uint32_t SpawnPool_get_prefabPools_m3784917882_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral204393016;
extern Il2CppCodeGenString* _stringLiteral2559083266;
extern Il2CppCodeGenString* _stringLiteral2477534294;
extern Il2CppCodeGenString* _stringLiteral911846749;
extern const uint32_t SpawnPool_Awake_m3559340717_MetadataUsageId;
extern const MethodInfo* List_1_GetEnumerator_m3835178332_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m742159042_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4077480560_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m4282189368_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m676629708_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1791777211;
extern const uint32_t SpawnPool_OnDestroy_m331578691_MetadataUsageId;
extern Il2CppClass* Exception_t1927440687_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m3209201747_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1021754050;
extern Il2CppCodeGenString* _stringLiteral4052841889;
extern const uint32_t SpawnPool_CreatePrefabPool_m932413996_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1648406572;
extern Il2CppCodeGenString* _stringLiteral3949685996;
extern Il2CppCodeGenString* _stringLiteral1166636355;
extern const uint32_t SpawnPool_Add_m1019377620_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2671840987;
extern const uint32_t SpawnPool_Add_m2283877882_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2075577691;
extern const uint32_t SpawnPool_Remove_m1060805601_MetadataUsageId;
extern Il2CppClass* PrefabPool_t85422674_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1094175583;
extern const uint32_t SpawnPool_Spawn_m2610273213_MetadataUsageId;
extern const uint32_t SpawnPool_Spawn_m2615344182_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m2637777121_MethodInfo_var;
extern const uint32_t SpawnPool_Spawn_m3921451213_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisParticleSystem_t3394631041_m1847115563_MethodInfo_var;
extern const uint32_t SpawnPool_Spawn_m2956151321_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2264326193;
extern Il2CppCodeGenString* _stringLiteral2897442291;
extern const uint32_t SpawnPool_Despawn_m501567261_MetadataUsageId;
extern const uint32_t SpawnPool_Despawn_m2337310686_MetadataUsageId;
extern Il2CppClass* U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550_il2cpp_TypeInfo_var;
extern const uint32_t SpawnPool_DoDespawnAfterSeconds_m2454494_MetadataUsageId;
extern const uint32_t SpawnPool_DespawnAll_m114589995_MetadataUsageId;
extern const uint32_t SpawnPool_IsSpawned_m561723107_MetadataUsageId;
extern const uint32_t SpawnPool_GetPrefabPool_m2162742_MetadataUsageId;
extern const uint32_t SpawnPool_GetPrefabPool_m1999940987_MetadataUsageId;
extern const uint32_t SpawnPool_GetPrefab_m228367079_MetadataUsageId;
extern const uint32_t SpawnPool_GetPrefab_m1836877887_MetadataUsageId;
extern Il2CppClass* U3CListForAudioStopU3Ec__Iterator2_t1067573800_il2cpp_TypeInfo_var;
extern const uint32_t SpawnPool_ListForAudioStop_m4210871781_MetadataUsageId;
extern Il2CppClass* U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689_il2cpp_TypeInfo_var;
extern const uint32_t SpawnPool_ListenForEmitDespawn_m3364909241_MetadataUsageId;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m948919263_MethodInfo_var;
extern const uint32_t SpawnPool_ToString_m3120155651_MetadataUsageId;
extern const uint32_t SpawnPool_get_Item_m2234631157_MetadataUsageId;
extern const uint32_t SpawnPool_set_Item_m3752828756_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1196577561;
extern const uint32_t SpawnPool_Contains_m3051551352_MetadataUsageId;
extern const MethodInfo* List_1_CopyTo_m3358051392_MethodInfo_var;
extern const uint32_t SpawnPool_CopyTo_m2515519044_MetadataUsageId;
extern const uint32_t SpawnPool_get_Count_m522886060_MetadataUsageId;
extern Il2CppClass* U3CGetEnumeratorU3Ec__Iterator4_t2230806469_il2cpp_TypeInfo_var;
extern const uint32_t SpawnPool_GetEnumerator_m3048857063_MetadataUsageId;
extern Il2CppClass* U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705_il2cpp_TypeInfo_var;
extern const uint32_t SpawnPool_System_Collections_IEnumerable_GetEnumerator_m2186170833_MetadataUsageId;
extern const uint32_t SpawnPool_IndexOf_m2455463034_MetadataUsageId;
extern const uint32_t SpawnPool_Insert_m3265689621_MetadataUsageId;
extern const uint32_t SpawnPool_RemoveAt_m655898584_MetadataUsageId;
extern const uint32_t SpawnPool_Clear_m1056223761_MetadataUsageId;
extern const uint32_t SpawnPool_get_IsReadOnly_m1411436997_MetadataUsageId;
extern const uint32_t SpawnPool_System_Collections_Generic_ICollectionU3CUnityEngine_TransformU3E_Remove_m497770936_MetadataUsageId;
extern const uint32_t U3CDoDespawnAfterSecondsU3Ec__Iterator1_Reset_m3479680684_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m635155626_MetadataUsageId;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator4_Reset_m1633375387_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3308707187;
extern const uint32_t U3CListenForEmitDespawnU3Ec__Iterator3_MoveNext_m3826550086_MetadataUsageId;
extern const uint32_t U3CListenForEmitDespawnU3Ec__Iterator3_Reset_m3962734311_MetadataUsageId;
extern const uint32_t U3CListForAudioStopU3Ec__Iterator2_Reset_m3922951534_MetadataUsageId;
extern const uint32_t U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_MoveNext_m654851924_MetadataUsageId;
extern const uint32_t U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_Reset_m290490135_MetadataUsageId;
extern const uint32_t InstantiateDelegate_BeginInvoke_m426211799_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t4164115705_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t39529491_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1084170225_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1111865929_MethodInfo_var;
extern const uint32_t SpawnPoolsDict__ctor_m2459636125_MetadataUsageId;
extern Il2CppClass* OnCreatedDelegate_t2249336443_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1081054922_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m2575601273_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1130184155_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m3828308582_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2033998413;
extern const uint32_t SpawnPoolsDict_AddOnCreatedDelegate_m3725830929_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4011475393;
extern Il2CppCodeGenString* _stringLiteral1617874521;
extern Il2CppCodeGenString* _stringLiteral4098646607;
extern const uint32_t SpawnPoolsDict_RemoveOnCreatedDelegate_m2862845276_MetadataUsageId;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisSpawnPool_t2419717525_m2522654191_MethodInfo_var;
extern const uint32_t SpawnPoolsDict_Create_m2130217657_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_Create_m2712319509_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2999319186;
extern Il2CppCodeGenString* _stringLiteral267774189;
extern const uint32_t SpawnPoolsDict_assertValidPoolName_m1240830686_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Count_m1267632521_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m339431039_MethodInfo_var;
extern const MethodInfo* KeyCollection_CopyTo_m861994567_MethodInfo_var;
extern const uint32_t SpawnPoolsDict_ToString_m724599862_MetadataUsageId;
extern const MethodInfo* Dictionary_2_TryGetValue_m3478201132_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m775250656_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3388612068;
extern const uint32_t SpawnPoolsDict_Destroy_m3596035525_MetadataUsageId;
extern const MethodInfo* Dictionary_2_GetEnumerator_m209731277_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1934570397_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3480818628_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2014802852_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2095172018_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m1196654984_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1834820150;
extern const uint32_t SpawnPoolsDict_DestroyAll_m1889592154_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Add_m3678661409_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1835617696;
extern Il2CppCodeGenString* _stringLiteral2041708912;
extern const uint32_t SpawnPoolsDict_Add_m2638912137_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2866193509;
extern const uint32_t SpawnPoolsDict_Add_m2690135699_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3823589648;
extern const uint32_t SpawnPoolsDict_Remove_m3353331162_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3763332682;
extern const uint32_t SpawnPoolsDict_Remove_m3364582693_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_get_Count_m945703525_MetadataUsageId;
extern const MethodInfo* Dictionary_2_ContainsKey_m1243307708_MethodInfo_var;
extern const uint32_t SpawnPoolsDict_ContainsKey_m2077114099_MetadataUsageId;
extern const MethodInfo* Dictionary_2_ContainsValue_m2866665949_MethodInfo_var;
extern const uint32_t SpawnPoolsDict_ContainsValue_m3660170656_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_TryGetValue_m4237209346_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2393457341;
extern const uint32_t SpawnPoolsDict_Contains_m1346561444_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Item_m2588609791_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4025903905;
extern const uint32_t SpawnPoolsDict_get_Item_m3547080165_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3484800581;
extern const uint32_t SpawnPoolsDict_set_Item_m2690855718_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1595821356;
extern const uint32_t SpawnPoolsDict_get_Keys_m3831953038_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_get_Values_m2310424001_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_Add_m4237682212_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral628749495;
extern const uint32_t SpawnPoolsDict_Clear_m597009206_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3287840122;
extern const uint32_t SpawnPoolsDict_CopyTo_m757354890_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CPathologicalGames_SpawnPoolU3EU3E_CopyTo_m519111753_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_Remove_m492150273_MetadataUsageId;
extern Il2CppClass* Enumerator_t1359554193_il2cpp_TypeInfo_var;
extern const uint32_t SpawnPoolsDict_GetEnumerator_m1344881667_MetadataUsageId;
extern const uint32_t SpawnPoolsDict_System_Collections_IEnumerable_GetEnumerator_m2792900310_MetadataUsageId;

// System.Object[]
struct ObjectU5BU5D_t3614634134  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>[]
struct KeyValuePair_2U5BU5D_t3482534235  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2947242542  m_Items[1];

public:
	inline KeyValuePair_2_t2947242542  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2947242542 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2947242542  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2947242542  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2947242542 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2947242542  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Transform[]
struct TransformU5BU5D_t3764228911  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Transform_t3275118058 * m_Items[1];

public:
	inline Transform_t3275118058 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_t3275118058 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3275118058 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_t3275118058 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>[]
struct KeyValuePair_2U5BU5D_t498405028  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) KeyValuePair_2_t2091842009  m_Items[1];

public:
	inline KeyValuePair_2_t2091842009  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline KeyValuePair_2_t2091842009 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, KeyValuePair_2_t2091842009  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline KeyValuePair_2_t2091842009  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline KeyValuePair_2_t2091842009 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, KeyValuePair_2_t2091842009  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3692334404_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1__ctor_m2848015482_gshared (List_1_t2058570427 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m3615096820_gshared (List_1_t2058570427 * __this, int32_t p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
extern "C"  bool List_1_Contains_m1658838094_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3636113691_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  KeyCollection_t470039898 * Dictionary_2_get_Keys_m2664427549_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>::CopyTo(!0[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2201414821_gshared (KeyCollection_t470039898 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4209421183_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m112127646_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m2325793156_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m3321918434_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m3975825838_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject ** p1, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m4062719145_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
extern "C"  ValueCollection_t984569266 * Dictionary_2_get_Values_m2233445381_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3601534125  Dictionary_2_GetEnumerator_m3077639147_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C"  void Dictionary_2__ctor_m729743939_gshared (Dictionary_2_t3417634846 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t3614634134* List_1_ToArray_m546658539_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(!0[],System.Int32)
extern "C"  void List_1_CopyTo_m2123980726_gshared (List_1_t2058570427 * __this, ObjectU5BU5D_t3614634134* p0, int32_t p1, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3946215804_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1091361971_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsValue(!1)
extern "C"  bool Dictionary_2_ContainsValue_m2749423671_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, const MethodInfo* method);

// UnityEngine.GameObject PathologicalGames.InstanceHandler/InstantiateDelegate::Invoke(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * InstantiateDelegate_Invoke_m3998791625 (InstantiateDelegate_t3900751951 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3186302158(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3692334404_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void PathologicalGames.InstanceHandler/DestroyDelegate::Invoke(UnityEngine.GameObject)
extern "C"  void DestroyDelegate_Invoke_m1154097644 (DestroyDelegate_t3666444239 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPoolsDict::.ctor()
extern "C"  void SpawnPoolsDict__ctor_m2459636125 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
#define List_1__ctor_m2292054616(__this, method) ((  void (*) (List_1_t2644239190 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.PrefabPool::get_logMessages()
extern "C"  bool PrefabPool_get_logMessages_m4160510898 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m1811873526 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Transform>::GetEnumerator()
#define List_1_GetEnumerator_m1527845545(__this, method) ((  Enumerator_t2178968864  (*) (List_1_t2644239190 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::get_Current()
#define Enumerator_get_Current_m2375004233(__this, method) ((  Transform_t3275118058 * (*) (Enumerator_t2178968864 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool::DestroyInstance(UnityEngine.GameObject)
extern "C"  void SpawnPool_DestroyInstance_m2432744115 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::MoveNext()
#define Enumerator_MoveNext_m273259033(__this, method) ((  bool (*) (Enumerator_t2178968864 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::Dispose()
#define Enumerator_Dispose_m686407845(__this, method) ((  void (*) (Enumerator_t2178968864 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear()
#define List_1_Clear_m1324927225(__this, method) ((  void (*) (List_1_t2644239190 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m2538210300(__this, p0, method) ((  void (*) (List_1_t2644239190 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2848015482_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count()
#define List_1_get_Count_m936427142(__this, method) ((  int32_t (*) (List_1_t2644239190 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Boolean PathologicalGames.PrefabPool::DespawnInstance(UnityEngine.Transform,System.Boolean)
extern "C"  bool PrefabPool_DespawnInstance_m2725861114 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___xform0, bool ___sendEventMessage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
extern "C"  String_t* String_Format_m4262916296 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, Il2CppObject * p2, Il2CppObject * p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Remove(!0)
#define List_1_Remove_m2326856859(__this, p0, method) ((  bool (*) (List_1_t2644239190 *, Transform_t3275118058 *, const MethodInfo*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
#define List_1_Add_m3606265932(__this, p0, method) ((  void (*) (List_1_t2644239190 *, Transform_t3275118058 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void UnityEngine.GameObject::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void GameObject_BroadcastMessage_m3472370570 (GameObject_t1756533147 * __this, String_t* p0, Il2CppObject * p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PathologicalGames.PrefabPool::get_totalCount()
extern "C"  int32_t PrefabPool_get_totalCount_m1479453083 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PathologicalGames.PrefabPool::CullDespawned()
extern "C"  Il2CppObject * PrefabPool_CullDespawned_m3756497222 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::.ctor()
extern "C"  void U3CCullDespawnedU3Ec__Iterator0__ctor_m2159543816 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32)
#define List_1_get_Item_m563292115(__this, p0, method) ((  Transform_t3275118058 * (*) (List_1_t2644239190 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Boolean PathologicalGames.PrefabPool::DespawnInstance(UnityEngine.Transform)
extern "C"  bool PrefabPool_DespawnInstance_m1597075065 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___xform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.PrefabPool::SpawnNew(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * PrefabPool_SpawnNew_m1137185856 (PrefabPool_t85422674 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m4270546658(__this, p0, method) ((  void (*) (List_1_t2644239190 *, int32_t, const MethodInfo*))List_1_RemoveAt_m3615096820_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MissingReferenceException::.ctor(System.String)
extern "C"  void MissingReferenceException__ctor_m658435003 (MissingReferenceException_t1416808650 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m305888255 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.SpawnPool::get_group()
extern "C"  Transform_t3275118058 * SpawnPool_get_group_m224041552 (SpawnPool_t2419717525 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m2308156925 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  p0, Quaternion_t4030073918  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m1033555130 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PathologicalGames.SpawnPool::InstantiatePrefab(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * SpawnPool_InstantiatePrefab_m3003297369 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::nameInstance(UnityEngine.Transform)
extern "C"  void PrefabPool_nameInstance_m1724321706 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m1963830867 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t2243707580  Vector3_get_one_m627547232 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m2325460848 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m725607808 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::SetRecursively(UnityEngine.Transform,System.Int32)
extern "C"  void PrefabPool_SetRecursively_m1140085826 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___xform0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m2712461877 (GameObject_t1756533147 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  Il2CppObject * Transform_GetEnumerator_m3479720613 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.PrefabPool::get_preloaded()
extern "C"  bool PrefabPool_get_preloaded_m270443884 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::set_preloaded(System.Boolean)
extern "C"  void PrefabPool_set_preloaded_m1695494847 (PrefabPool_t85422674 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PathologicalGames.PrefabPool::PreloadOverTime()
extern "C"  Il2CppObject * PrefabPool_PreloadOverTime_m604118577 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.PrefabPool::SpawnNew()
extern "C"  Transform_t3275118058 * PrefabPool_SpawnNew_m3775111208 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::.ctor()
extern "C"  void U3CPreloadOverTimeU3Ec__Iterator1__ctor_m2540324496 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Transform> PathologicalGames.PrefabPool::get_spawned()
extern "C"  List_1_t2644239190 * PrefabPool_get_spawned_m365923230 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Contains(!0)
#define List_1_Contains_m2540959346(__this, p0, method) ((  bool (*) (List_1_t2644239190 *, Transform_t3275118058 *, const MethodInfo*))List_1_Contains_m1658838094_gshared)(__this, p0, method)
// System.Collections.Generic.List`1<UnityEngine.Transform> PathologicalGames.PrefabPool::get_despawned()
extern "C"  List_1_t2644239190 * PrefabPool_get_despawned_m3902220117 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString(System.String)
extern "C"  String_t* Int32_ToString_m1064459878 (int32_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m4157836998 (Object_t1021602117 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m1263743648 (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t3614634134* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::.ctor()
#define Dictionary_2__ctor_m1861750071(__this, method) ((  void (*) (Dictionary_2_t894930024 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::get_Count()
#define Dictionary_2_get_Count_m2934043879(__this, method) ((  int32_t (*) (Dictionary_2_t894930024 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::get_Keys()
#define Dictionary_2_get_Keys_m2836166341(__this, method) ((  KeyCollection_t3378427795 * (*) (Dictionary_2_t894930024 *, const MethodInfo*))Dictionary_2_get_Keys_m2664427549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.Transform>::CopyTo(!0[],System.Int32)
#define KeyCollection_CopyTo_m1254677401(__this, p0, p1, method) ((  void (*) (KeyCollection_t3378427795 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2201414821_gshared)(__this, p0, p1, method)
// System.String System.String::Join(System.String,System.String[])
extern "C"  String_t* String_Join_m1966872927 (Il2CppObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1642385972* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::Add(!0,!1)
#define Dictionary_2_Add_m3540954367(__this, p0, p1, method) ((  void (*) (Dictionary_2_t894930024 *, String_t*, Transform_t3275118058 *, const MethodInfo*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::Remove(!0)
#define Dictionary_2_Remove_m708479600(__this, p0, method) ((  bool (*) (Dictionary_2_t894930024 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m112127646_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::Clear()
#define Dictionary_2_Clear_m1781593424(__this, method) ((  void (*) (Dictionary_2_t894930024 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m315213716(__this, p0, method) ((  bool (*) (Dictionary_2_t894930024 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3825486608(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t894930024 *, String_t*, Transform_t3275118058 **, const MethodInfo*))Dictionary_2_TryGetValue_m3975825838_gshared)(__this, p0, p1, method)
// System.Void System.NotImplementedException::.ctor(System.String)
extern "C"  void NotImplementedException__ctor_m1795163961 (NotImplementedException_t2785117854 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::get_Item(!0)
#define Dictionary_2_get_Item_m777678513(__this, p0, method) ((  Transform_t3275118058 * (*) (Dictionary_2_t894930024 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyNotFoundException::.ctor(System.String)
extern "C"  void KeyNotFoundException__ctor_m264393096 (KeyNotFoundException_t1722175009 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/ValueCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::get_Values()
#define Dictionary_2_get_Values_m1688729445(__this, method) ((  ValueCollection_t3892957163 * (*) (Dictionary_2_t894930024 *, const MethodInfo*))Dictionary_2_get_Values_m2233445381_gshared)(__this, method)
// System.Void System.NotImplementedException::.ctor()
extern "C"  void NotImplementedException__ctor_m808189835 (NotImplementedException_t2785117854 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Transform>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2685922707(__this, method) ((  Enumerator_t2214954726  (*) (Dictionary_2_t894930024 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3077639147_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.SpawnPoolsDict::TryGetValue(System.String,PathologicalGames.SpawnPool&)
extern "C"  bool SpawnPoolsDict_TryGetValue_m4237209346 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, SpawnPool_t2419717525 ** ___spawnPool1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool::Add(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
extern "C"  void SpawnPool_Add_m1019377620 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, String_t* ___prefabName1, bool ___despawn2, bool ___parent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PathologicalGames.PrefabPool>::.ctor()
#define List_1__ctor_m2177220103(__this, method) ((  void (*) (List_1_t3749511102 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
#define Dictionary_2__ctor_m729743939(__this, method) ((  void (*) (Dictionary_2_t3417634846 *, const MethodInfo*))Dictionary_2__ctor_m729743939_gshared)(__this, method)
// System.Void PathologicalGames.PrefabsDict::.ctor()
extern "C"  void PrefabsDict__ctor_m4168763172 (PrefabsDict_t1121107021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.PrefabPool>::.ctor()
#define Dictionary_2__ctor_m723632974(__this, method) ((  void (*) (Dictionary_2_t2000201936 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<PathologicalGames.PrefabPool>::get_Item(System.Int32)
#define List_1_get_Item_m3756596590(__this, p0, method) ((  PrefabPool_t85422674 * (*) (List_1_t3749511102 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.PrefabPool>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3140732121(__this, p0, p1, method) ((  void (*) (Dictionary_2_t2000201936 *, String_t*, PrefabPool_t85422674 *, const MethodInfo*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<PathologicalGames.PrefabPool>::get_Count()
#define List_1_get_Count_m954114191(__this, method) ((  int32_t (*) (List_1_t3749511102 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void PathologicalGames.SpawnPool::set_group(UnityEngine.Transform)
extern "C"  void SpawnPool_set_group_m2060151509 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Replace(System.String,System.String)
extern "C"  String_t* String_Replace_m1941156251 (String_t* __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::inspectorInstanceConstructor()
extern "C"  void PrefabPool_inspectorInstanceConstructor_m824700707 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool::CreatePrefabPool(PathologicalGames.PrefabPool)
extern "C"  void SpawnPool_CreatePrefabPool_m932413996 (SpawnPool_t2419717525 * __this, PrefabPool_t85422674 * ___prefabPool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPoolsDict::Add(PathologicalGames.SpawnPool)
extern "C"  void SpawnPoolsDict_Add_m2638912137 (SpawnPoolsDict_t1520791760 * __this, SpawnPool_t2419717525 * ___spawnPool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PathologicalGames.SpawnPool/InstantiateDelegate::Invoke(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * InstantiateDelegate_Invoke_m2830317127 (InstantiateDelegate_t3700441677 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PathologicalGames.InstanceHandler::InstantiatePrefab(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * InstanceHandler_InstantiatePrefab_m309003507 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool/DestroyDelegate::Invoke(UnityEngine.GameObject)
extern "C"  void DestroyDelegate_Invoke_m1938022676 (DestroyDelegate_t3092628401 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.InstanceHandler::DestroyInstance(UnityEngine.GameObject)
extern "C"  void InstanceHandler_DestroyInstance_m331796129 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.SpawnPoolsDict::ContainsValue(PathologicalGames.SpawnPool)
extern "C"  bool SpawnPoolsDict_ContainsValue_m3660170656 (SpawnPoolsDict_t1520791760 * __this, SpawnPool_t2419717525 * ___pool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.SpawnPoolsDict::Remove(PathologicalGames.SpawnPool)
extern "C"  bool SpawnPoolsDict_Remove_m3353331162 (SpawnPoolsDict_t1520791760 * __this, SpawnPool_t2419717525 * ___spawnPool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern "C"  void MonoBehaviour_StopAllCoroutines_m1675795839 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PathologicalGames.PrefabPool>::GetEnumerator()
#define List_1_GetEnumerator_m3835178332(__this, method) ((  Enumerator_t3284240776  (*) (List_1_t3749511102 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<PathologicalGames.PrefabPool>::get_Current()
#define Enumerator_get_Current_m742159042(__this, method) ((  PrefabPool_t85422674 * (*) (Enumerator_t3284240776 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Void PathologicalGames.PrefabPool::SelfDestruct()
extern "C"  void PrefabPool_SelfDestruct_m1395427007 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<PathologicalGames.PrefabPool>::MoveNext()
#define Enumerator_MoveNext_m4077480560(__this, method) ((  bool (*) (Enumerator_t3284240776 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PathologicalGames.PrefabPool>::Dispose()
#define Enumerator_Dispose_m4282189368(__this, method) ((  void (*) (Enumerator_t3284240776 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<PathologicalGames.PrefabPool>::Clear()
#define List_1_Clear_m676629708(__this, method) ((  void (*) (List_1_t3749511102 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Void PathologicalGames.PrefabsDict::_Clear()
extern "C"  void PrefabsDict__Clear_m3612909952 (PrefabsDict_t1121107021 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PathologicalGames.PrefabPool PathologicalGames.SpawnPool::GetPrefabPool(UnityEngine.Transform)
extern "C"  PrefabPool_t85422674 * SpawnPool_GetPrefabPool_m2162742 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m485833136 (Exception_t1927440687 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PathologicalGames.PrefabPool>::Add(!0)
#define List_1_Add_m3209201747(__this, p0, method) ((  void (*) (List_1_t3749511102 *, PrefabPool_t85422674 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void PathologicalGames.PrefabsDict::_Add(System.String,UnityEngine.Transform)
extern "C"  void PrefabsDict__Add_m2864700459 (PrefabsDict_t1121107021 * __this, String_t* ___prefabName0, Transform_t3275118058 * ___prefab1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::PreloadInstances()
extern "C"  void PrefabPool_PreloadInstances_m3084730458 (PrefabPool_t85422674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::AddUnpooled(UnityEngine.Transform,System.Boolean)
extern "C"  void PrefabPool_AddUnpooled_m2140288438 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___inst0, bool ___despawn1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.PrefabPool::SpawnInstance(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * PrefabPool_SpawnInstance_m3748491065 (PrefabPool_t85422674 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.PrefabPool::.ctor(UnityEngine.Transform)
extern "C"  void PrefabPool__ctor_m2549027878 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___prefab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m2610273213 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m2615344182 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3091291392 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3993902991 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, Transform_t3275118058 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform PathologicalGames.PrefabsDict::get_Item(System.String)
extern "C"  Transform_t3275118058 * PrefabsDict_get_Item_m254611088 (PrefabsDict_t1121107021 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource PathologicalGames.SpawnPool::Spawn(UnityEngine.AudioSource,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  AudioSource_t1135106623 * SpawnPool_Spawn_m3921451213 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m2637777121(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m353744792 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PathologicalGames.SpawnPool::ListForAudioStop(UnityEngine.AudioSource)
extern "C"  Il2CppObject * SpawnPool_ListForAudioStop_m4210871781 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___src0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem PathologicalGames.SpawnPool::Spawn(UnityEngine.ParticleSystem,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  ParticleSystem_t3394631041 * SpawnPool_Spawn_m2956151321 (SpawnPool_t2419717525 * __this, ParticleSystem_t3394631041 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
#define Component_GetComponent_TisParticleSystem_t3394631041_m1847115563(__this, method) ((  ParticleSystem_t3394631041 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Collections.IEnumerator PathologicalGames.SpawnPool::ListenForEmitDespawn(UnityEngine.ParticleSystem)
extern "C"  Il2CppObject * SpawnPool_ListenForEmitDespawn_m3364909241 (SpawnPool_t2419717525 * __this, ParticleSystem_t3394631041 * ___emitter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool::Despawn(UnityEngine.Transform)
extern "C"  void SpawnPool_Despawn_m501567261 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PathologicalGames.SpawnPool::DoDespawnAfterSeconds(UnityEngine.Transform,System.Single,System.Boolean,UnityEngine.Transform)
extern "C"  Il2CppObject * SpawnPool_DoDespawnAfterSeconds_m2454494 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, float ___seconds1, bool ___useParent2, Transform_t3275118058 * ___parent3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::.ctor()
extern "C"  void U3CDoDespawnAfterSecondsU3Ec__Iterator1__ctor_m208252327 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.PrefabPool::Contains(UnityEngine.Transform)
extern "C"  bool PrefabPool_Contains_m3543239565 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___transform0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::.ctor()
extern "C"  void U3CListForAudioStopU3Ec__Iterator2__ctor_m2241042125 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::.ctor()
extern "C"  void U3CListenForEmitDespawnU3Ec__Iterator3__ctor_m3129400790 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m3854603248(__this, method) ((  void (*) (List_1_t1398341365 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m4061286785(__this, p0, method) ((  void (*) (List_1_t1398341365 *, String_t*, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<System.String>::ToArray()
#define List_1_ToArray_m948919263(__this, method) ((  StringU5BU5D_t1642385972* (*) (List_1_t1398341365 *, const MethodInfo*))List_1_ToArray_m546658539_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::CopyTo(!0[],System.Int32)
#define List_1_CopyTo_m3358051392(__this, p0, p1, method) ((  void (*) (List_1_t2644239190 *, TransformU5BU5D_t3764228911*, int32_t, const MethodInfo*))List_1_CopyTo_m2123980726_gshared)(__this, p0, p1, method)
// System.Void PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator4__ctor_m1958432518 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0__ctor_m3790315952 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m4242915935 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathologicalGames.SpawnPool::Despawn(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void SpawnPool_Despawn_m2337310686 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, Transform_t3275118058 * ___parent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem::get_startDelay()
extern "C"  float ParticleSystem_get_startDelay_m3593315006 (ParticleSystem_t3394631041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m2793794644 (ParticleSystem_t3394631041 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C"  void ParticleSystem_Clear_m4048064080 (ParticleSystem_t3394631041 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AudioSource::get_isPlaying()
extern "C"  bool AudioSource_get_isPlaying_m3677592677 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Stop()
extern "C"  void AudioSource_Stop_m3452679614 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate>::.ctor()
#define Dictionary_2__ctor_m1084170225(__this, method) ((  void (*) (Dictionary_2_t4164115705 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::.ctor()
#define Dictionary_2__ctor_m1111865929(__this, method) ((  void (*) (Dictionary_2_t39529491 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1081054922(__this, p0, method) ((  bool (*) (Dictionary_2_t4164115705 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate>::Add(!0,!1)
#define Dictionary_2_Add_m2575601273(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4164115705 *, String_t*, OnCreatedDelegate_t2249336443 *, const MethodInfo*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// System.Object System.Delegate::get_Target()
extern "C"  Il2CppObject * Delegate_get_Target_m896795953 (Delegate_t3022476291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate>::get_Item(!0)
#define Dictionary_2_get_Item_m1130184155(__this, p0, method) ((  OnCreatedDelegate_t2249336443 * (*) (Dictionary_2_t4164115705 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m3828308582(__this, p0, p1, method) ((  void (*) (Dictionary_2_t4164115705 *, String_t*, OnCreatedDelegate_t2249336443 *, const MethodInfo*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (Il2CppObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m962601984 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<PathologicalGames.SpawnPool>()
#define GameObject_AddComponent_TisSpawnPool_t2419717525_m2522654191(__this, method) ((  SpawnPool_t2419717525 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3946215804_gshared)(__this, method)
// System.Boolean PathologicalGames.SpawnPoolsDict::assertValidPoolName(System.String)
extern "C"  bool SpawnPoolsDict_assertValidPoolName_m1240830686 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern "C"  GameObject_t1756533147 * GameObject_get_gameObject_m3662236595 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathologicalGames.SpawnPoolsDict::ContainsKey(System.String)
extern "C"  bool SpawnPoolsDict_ContainsKey_m2077114099 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::get_Count()
#define Dictionary_2_get_Count_m1267632521(__this, method) ((  int32_t (*) (Dictionary_2_t39529491 *, const MethodInfo*))Dictionary_2_get_Count_m3636113691_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<!0,!1> System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::get_Keys()
#define Dictionary_2_get_Keys_m339431039(__this, method) ((  KeyCollection_t2523027262 * (*) (Dictionary_2_t39529491 *, const MethodInfo*))Dictionary_2_get_Keys_m2664427549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,PathologicalGames.SpawnPool>::CopyTo(!0[],System.Int32)
#define KeyCollection_CopyTo_m861994567(__this, p0, p1, method) ((  void (*) (KeyCollection_t2523027262 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2201414821_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m3478201132(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t39529491 *, String_t*, SpawnPool_t2419717525 **, const MethodInfo*))Dictionary_2_TryGetValue_m3975825838_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::Remove(!0)
#define Dictionary_2_Remove_m775250656(__this, p0, method) ((  bool (*) (Dictionary_2_t39529491 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m112127646_gshared)(__this, p0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m209731277(__this, method) ((  Enumerator_t1359554193  (*) (Dictionary_2_t39529491 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3077639147_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,PathologicalGames.SpawnPool>::get_Current()
#define Enumerator_get_Current_m1934570397(__this, method) ((  KeyValuePair_2_t2091842009  (*) (Enumerator_t1359554193 *, const MethodInfo*))Enumerator_get_Current_m1091361971_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>::get_Value()
#define KeyValuePair_2_get_Value_m3480818628(__this, method) ((  SpawnPool_t2419717525 * (*) (KeyValuePair_2_t2091842009 *, const MethodInfo*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,PathologicalGames.SpawnPool>::MoveNext()
#define Enumerator_MoveNext_m2014802852(__this, method) ((  bool (*) (Enumerator_t1359554193 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,PathologicalGames.SpawnPool>::Dispose()
#define Enumerator_Dispose_m2095172018(__this, method) ((  void (*) (Enumerator_t1359554193 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::Clear()
#define Dictionary_2_Clear_m1196654984(__this, method) ((  void (*) (Dictionary_2_t39529491 *, const MethodInfo*))Dictionary_2_Clear_m2325793156_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::Add(!0,!1)
#define Dictionary_2_Add_m3678661409(__this, p0, p1, method) ((  void (*) (Dictionary_2_t39529491 *, String_t*, SpawnPool_t2419717525 *, const MethodInfo*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// System.Void PathologicalGames.SpawnPoolsDict/OnCreatedDelegate::Invoke(PathologicalGames.SpawnPool)
extern "C"  void OnCreatedDelegate_Invoke_m3309419437 (OnCreatedDelegate_t2249336443 * __this, SpawnPool_t2419717525 * ___pool0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m4091950718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1243307708(__this, p0, method) ((  bool (*) (Dictionary_2_t39529491 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m3321918434_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::ContainsValue(!1)
#define Dictionary_2_ContainsValue_m2866665949(__this, p0, method) ((  bool (*) (Dictionary_2_t39529491 *, SpawnPool_t2419717525 *, const MethodInfo*))Dictionary_2_ContainsValue_m2749423671_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.SpawnPool>::get_Item(!0)
#define Dictionary_2_get_Item_m2588609791(__this, p0, method) ((  SpawnPool_t2419717525 * (*) (Dictionary_2_t39529491 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m4062719145_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.GameObject PathologicalGames.InstanceHandler::InstantiatePrefab(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * InstanceHandler_InstantiatePrefab_m309003507 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstanceHandler_InstantiatePrefab_m309003507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InstantiateDelegate_t3900751951 * L_0 = ((InstanceHandler_t1084123187_StaticFields*)InstanceHandler_t1084123187_il2cpp_TypeInfo_var->static_fields)->get_InstantiateDelegates_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		InstantiateDelegate_t3900751951 * L_1 = ((InstanceHandler_t1084123187_StaticFields*)InstanceHandler_t1084123187_il2cpp_TypeInfo_var->static_fields)->get_InstantiateDelegates_0();
		GameObject_t1756533147 * L_2 = ___prefab0;
		Vector3_t2243707580  L_3 = ___pos1;
		Quaternion_t4030073918  L_4 = ___rot2;
		NullCheck(L_1);
		GameObject_t1756533147 * L_5 = InstantiateDelegate_Invoke_m3998791625(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0018:
	{
		GameObject_t1756533147 * L_6 = ___prefab0;
		Vector3_t2243707580  L_7 = ___pos1;
		Quaternion_t4030073918  L_8 = ___rot2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_9 = Object_Instantiate_TisGameObject_t1756533147_m3186302158(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3186302158_MethodInfo_var);
		return L_9;
	}
}
// System.Void PathologicalGames.InstanceHandler::DestroyInstance(UnityEngine.GameObject)
extern "C"  void InstanceHandler_DestroyInstance_m331796129 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstanceHandler_DestroyInstance_m331796129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DestroyDelegate_t3666444239 * L_0 = ((InstanceHandler_t1084123187_StaticFields*)InstanceHandler_t1084123187_il2cpp_TypeInfo_var->static_fields)->get_DestroyDelegates_1();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		DestroyDelegate_t3666444239 * L_1 = ((InstanceHandler_t1084123187_StaticFields*)InstanceHandler_t1084123187_il2cpp_TypeInfo_var->static_fields)->get_DestroyDelegates_1();
		GameObject_t1756533147 * L_2 = ___instance0;
		NullCheck(L_1);
		DestroyDelegate_Invoke_m1154097644(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0020;
	}

IL_001a:
	{
		GameObject_t1756533147 * L_3 = ___instance0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void PathologicalGames.InstanceHandler/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DestroyDelegate__ctor_m3622658344 (DestroyDelegate_t3666444239 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PathologicalGames.InstanceHandler/DestroyDelegate::Invoke(UnityEngine.GameObject)
extern "C"  void DestroyDelegate_Invoke_m1154097644 (DestroyDelegate_t3666444239 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DestroyDelegate_Invoke_m1154097644((DestroyDelegate_t3666444239 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PathologicalGames.InstanceHandler/DestroyDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DestroyDelegate_BeginInvoke_m3041110811 (DestroyDelegate_t3666444239 * __this, GameObject_t1756533147 * ___instance0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PathologicalGames.InstanceHandler/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DestroyDelegate_EndInvoke_m4263916646 (DestroyDelegate_t3666444239 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PathologicalGames.InstanceHandler/InstantiateDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void InstantiateDelegate__ctor_m2022012498 (InstantiateDelegate_t3900751951 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.GameObject PathologicalGames.InstanceHandler/InstantiateDelegate::Invoke(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * InstantiateDelegate_Invoke_m3998791625 (InstantiateDelegate_t3900751951 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		InstantiateDelegate_Invoke_m3998791625((InstantiateDelegate_t3900751951 *)__this->get_prev_9(),___prefab0, ___pos1, ___rot2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef GameObject_t1756533147 * (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___prefab0, ___pos1, ___rot2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef GameObject_t1756533147 * (*FunctionPointerType) (void* __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___prefab0, ___pos1, ___rot2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef GameObject_t1756533147 * (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___prefab0, ___pos1, ___rot2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PathologicalGames.InstanceHandler/InstantiateDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InstantiateDelegate_BeginInvoke_m2182565173 (InstantiateDelegate_t3900751951 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstantiateDelegate_BeginInvoke_m2182565173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___prefab0;
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___pos1);
	__d_args[2] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___rot2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// UnityEngine.GameObject PathologicalGames.InstanceHandler/InstantiateDelegate::EndInvoke(System.IAsyncResult)
extern "C"  GameObject_t1756533147 * InstantiateDelegate_EndInvoke_m100210449 (InstantiateDelegate_t3900751951 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (GameObject_t1756533147 *)__result;
}
// System.Void PathologicalGames.PoolManager::.cctor()
extern "C"  void PoolManager__cctor_m740501879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PoolManager__cctor_m740501879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpawnPoolsDict_t1520791760 * L_0 = (SpawnPoolsDict_t1520791760 *)il2cpp_codegen_object_new(SpawnPoolsDict_t1520791760_il2cpp_TypeInfo_var);
		SpawnPoolsDict__ctor_m2459636125(L_0, /*hidden argument*/NULL);
		((PoolManager_t707327633_StaticFields*)PoolManager_t707327633_il2cpp_TypeInfo_var->static_fields)->set_Pools_0(L_0);
		return;
	}
}
// System.Void PathologicalGames.PrefabPool::.ctor(UnityEngine.Transform)
extern "C"  void PrefabPool__ctor_m2549027878 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool__ctor_m2549027878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_preloadAmount_2(1);
		__this->set_preloadFrames_4(2);
		__this->set_limitAmount_7(((int32_t)100));
		__this->set_cullAbove_10(((int32_t)50));
		__this->set_cullDelay_11(((int32_t)60));
		__this->set_cullMaxPerPass_12(5);
		List_1_t2644239190 * L_0 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_0, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__spawned_17(L_0);
		List_1_t2644239190 * L_1 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_1, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__despawned_18(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = ___prefab0;
		__this->set_prefab_0(L_2);
		Transform_t3275118058 * L_3 = ___prefab0;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		__this->set_prefabGO_1(L_4);
		return;
	}
}
// System.Void PathologicalGames.PrefabPool::.ctor()
extern "C"  void PrefabPool__ctor_m3674034909 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool__ctor_m3674034909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_preloadAmount_2(1);
		__this->set_preloadFrames_4(2);
		__this->set_limitAmount_7(((int32_t)100));
		__this->set_cullAbove_10(((int32_t)50));
		__this->set_cullDelay_11(((int32_t)60));
		__this->set_cullMaxPerPass_12(5);
		List_1_t2644239190 * L_0 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_0, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__spawned_17(L_0);
		List_1_t2644239190 * L_1 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_1, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__despawned_18(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.PrefabPool::get_logMessages()
extern "C"  bool PrefabPool_get_logMessages_m4160510898 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_forceLoggingSilent_14();
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		SpawnPool_t2419717525 * L_1 = __this->get_spawnPool_15();
		NullCheck(L_1);
		bool L_2 = L_1->get_logMessages_7();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		SpawnPool_t2419717525 * L_3 = __this->get_spawnPool_15();
		NullCheck(L_3);
		bool L_4 = L_3->get_logMessages_7();
		return L_4;
	}

IL_0029:
	{
		bool L_5 = __this->get__logMessages_13();
		return L_5;
	}
}
// System.Void PathologicalGames.PrefabPool::inspectorInstanceConstructor()
extern "C"  void PrefabPool_inspectorInstanceConstructor_m824700707 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_inspectorInstanceConstructor_m824700707_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_prefab_0();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		__this->set_prefabGO_1(L_1);
		List_1_t2644239190 * L_2 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_2, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__spawned_17(L_2);
		List_1_t2644239190 * L_3 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_3, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__despawned_18(L_3);
		return;
	}
}
// System.Void PathologicalGames.PrefabPool::SelfDestruct()
extern "C"  void PrefabPool_SelfDestruct_m1395427007 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_SelfDestruct_m1395427007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Enumerator_t2178968864  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t3275118058 * V_2 = NULL;
	Enumerator_t2178968864  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = PrefabPool_get_logMessages_m4160510898(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		SpawnPool_t2419717525 * L_1 = __this->get_spawnPool_15();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_poolName_2();
		GameObject_t1756533147 * L_3 = __this->get_prefabGO_1();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral318652711, L_2, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0030:
	{
		List_1_t2644239190 * L_6 = __this->get__despawned_18();
		NullCheck(L_6);
		Enumerator_t2178968864  L_7 = List_1_GetEnumerator_m1527845545(L_6, /*hidden argument*/List_1_GetEnumerator_m1527845545_MethodInfo_var);
		V_1 = L_7;
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0077;
		}

IL_0041:
		{
			Transform_t3275118058 * L_8 = Enumerator_get_Current_m2375004233((&V_1), /*hidden argument*/Enumerator_get_Current_m2375004233_MethodInfo_var);
			V_0 = L_8;
			Transform_t3275118058 * L_9 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_10 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0077;
			}
		}

IL_0055:
		{
			SpawnPool_t2419717525 * L_11 = __this->get_spawnPool_15();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_0077;
			}
		}

IL_0066:
		{
			SpawnPool_t2419717525 * L_13 = __this->get_spawnPool_15();
			Transform_t3275118058 * L_14 = V_0;
			NullCheck(L_14);
			GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(L_14, /*hidden argument*/NULL);
			NullCheck(L_13);
			SpawnPool_DestroyInstance_m2432744115(L_13, L_15, /*hidden argument*/NULL);
		}

IL_0077:
		{
			bool L_16 = Enumerator_MoveNext_m273259033((&V_1), /*hidden argument*/Enumerator_MoveNext_m273259033_MethodInfo_var);
			if (L_16)
			{
				goto IL_0041;
			}
		}

IL_0083:
		{
			IL2CPP_LEAVE(0x96, FINALLY_0088);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0088;
	}

FINALLY_0088:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_1), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(136)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(136)
	{
		IL2CPP_JUMP_TBL(0x96, IL_0096)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0096:
	{
		List_1_t2644239190 * L_17 = __this->get__spawned_17();
		NullCheck(L_17);
		Enumerator_t2178968864  L_18 = List_1_GetEnumerator_m1527845545(L_17, /*hidden argument*/List_1_GetEnumerator_m1527845545_MethodInfo_var);
		V_3 = L_18;
	}

IL_00a2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00dd;
		}

IL_00a7:
		{
			Transform_t3275118058 * L_19 = Enumerator_get_Current_m2375004233((&V_3), /*hidden argument*/Enumerator_get_Current_m2375004233_MethodInfo_var);
			V_2 = L_19;
			Transform_t3275118058 * L_20 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_21 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_20, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_21)
			{
				goto IL_00dd;
			}
		}

IL_00bb:
		{
			SpawnPool_t2419717525 * L_22 = __this->get_spawnPool_15();
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_23 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_22, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00dd;
			}
		}

IL_00cc:
		{
			SpawnPool_t2419717525 * L_24 = __this->get_spawnPool_15();
			Transform_t3275118058 * L_25 = V_2;
			NullCheck(L_25);
			GameObject_t1756533147 * L_26 = Component_get_gameObject_m3105766835(L_25, /*hidden argument*/NULL);
			NullCheck(L_24);
			SpawnPool_DestroyInstance_m2432744115(L_24, L_26, /*hidden argument*/NULL);
		}

IL_00dd:
		{
			bool L_27 = Enumerator_MoveNext_m273259033((&V_3), /*hidden argument*/Enumerator_MoveNext_m273259033_MethodInfo_var);
			if (L_27)
			{
				goto IL_00a7;
			}
		}

IL_00e9:
		{
			IL2CPP_LEAVE(0xFC, FINALLY_00ee);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ee;
	}

FINALLY_00ee:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_3), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(238)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(238)
	{
		IL2CPP_JUMP_TBL(0xFC, IL_00fc)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00fc:
	{
		List_1_t2644239190 * L_28 = __this->get__spawned_17();
		NullCheck(L_28);
		List_1_Clear_m1324927225(L_28, /*hidden argument*/List_1_Clear_m1324927225_MethodInfo_var);
		List_1_t2644239190 * L_29 = __this->get__despawned_18();
		NullCheck(L_29);
		List_1_Clear_m1324927225(L_29, /*hidden argument*/List_1_Clear_m1324927225_MethodInfo_var);
		__this->set_prefab_0((Transform_t3275118058 *)NULL);
		__this->set_prefabGO_1((GameObject_t1756533147 *)NULL);
		__this->set_spawnPool_15((SpawnPool_t2419717525 *)NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Transform> PathologicalGames.PrefabPool::get_spawned()
extern "C"  List_1_t2644239190 * PrefabPool_get_spawned_m365923230 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_get_spawned_m365923230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2644239190 * L_0 = __this->get__spawned_17();
		List_1_t2644239190 * L_1 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2538210300(L_1, L_0, /*hidden argument*/List_1__ctor_m2538210300_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Transform> PathologicalGames.PrefabPool::get_despawned()
extern "C"  List_1_t2644239190 * PrefabPool_get_despawned_m3902220117 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_get_despawned_m3902220117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2644239190 * L_0 = __this->get__despawned_18();
		List_1_t2644239190 * L_1 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2538210300(L_1, L_0, /*hidden argument*/List_1__ctor_m2538210300_MethodInfo_var);
		return L_1;
	}
}
// System.Int32 PathologicalGames.PrefabPool::get_totalCount()
extern "C"  int32_t PrefabPool_get_totalCount_m1479453083 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_get_totalCount_m1479453083_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		List_1_t2644239190 * L_1 = __this->get__spawned_17();
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m936427142(L_1, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_2));
		int32_t L_3 = V_0;
		List_1_t2644239190 * L_4 = __this->get__despawned_18();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m936427142(L_4, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)L_5));
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Boolean PathologicalGames.PrefabPool::get_preloaded()
extern "C"  bool PrefabPool_get_preloaded_m270443884 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__preloaded_19();
		return L_0;
	}
}
// System.Void PathologicalGames.PrefabPool::set_preloaded(System.Boolean)
extern "C"  void PrefabPool_set_preloaded_m1695494847 (PrefabPool_t85422674 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set__preloaded_19(L_0);
		return;
	}
}
// System.Boolean PathologicalGames.PrefabPool::DespawnInstance(UnityEngine.Transform)
extern "C"  bool PrefabPool_DespawnInstance_m1597075065 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___xform0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___xform0;
		bool L_1 = PrefabPool_DespawnInstance_m2725861114(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PathologicalGames.PrefabPool::DespawnInstance(UnityEngine.Transform,System.Boolean)
extern "C"  bool PrefabPool_DespawnInstance_m2725861114 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___xform0, bool ___sendEventMessage1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_DespawnInstance_m2725861114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = PrefabPool_get_logMessages_m4160510898(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		SpawnPool_t2419717525 * L_1 = __this->get_spawnPool_15();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_poolName_2();
		Transform_t3275118058 * L_3 = __this->get_prefab_0();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = ___xform0;
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral1039352855, L_2, L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0036:
	{
		List_1_t2644239190 * L_8 = __this->get__spawned_17();
		Transform_t3275118058 * L_9 = ___xform0;
		NullCheck(L_8);
		List_1_Remove_m2326856859(L_8, L_9, /*hidden argument*/List_1_Remove_m2326856859_MethodInfo_var);
		List_1_t2644239190 * L_10 = __this->get__despawned_18();
		Transform_t3275118058 * L_11 = ___xform0;
		NullCheck(L_10);
		List_1_Add_m3606265932(L_10, L_11, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		bool L_12 = ___sendEventMessage1;
		if (!L_12)
		{
			goto IL_006c;
		}
	}
	{
		Transform_t3275118058 * L_13 = ___xform0;
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m3105766835(L_13, /*hidden argument*/NULL);
		SpawnPool_t2419717525 * L_15 = __this->get_spawnPool_15();
		NullCheck(L_14);
		GameObject_BroadcastMessage_m3472370570(L_14, _stringLiteral3199955346, L_15, 1, /*hidden argument*/NULL);
	}

IL_006c:
	{
		Transform_t3275118058 * L_16 = ___xform0;
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)0, /*hidden argument*/NULL);
		bool L_18 = __this->get_cullingActive_16();
		if (L_18)
		{
			goto IL_00b8;
		}
	}
	{
		bool L_19 = __this->get_cullDespawned_9();
		if (!L_19)
		{
			goto IL_00b8;
		}
	}
	{
		int32_t L_20 = PrefabPool_get_totalCount_m1479453083(__this, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_cullAbove_10();
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_00b8;
		}
	}
	{
		__this->set_cullingActive_16((bool)1);
		SpawnPool_t2419717525 * L_22 = __this->get_spawnPool_15();
		Il2CppObject * L_23 = PrefabPool_CullDespawned_m3756497222(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		MonoBehaviour_StartCoroutine_m2470621050(L_22, L_23, /*hidden argument*/NULL);
	}

IL_00b8:
	{
		return (bool)1;
	}
}
// System.Collections.IEnumerator PathologicalGames.PrefabPool::CullDespawned()
extern "C"  Il2CppObject * PrefabPool_CullDespawned_m3756497222 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_CullDespawned_m3756497222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CCullDespawnedU3Ec__Iterator0_t1896461731 * V_0 = NULL;
	{
		U3CCullDespawnedU3Ec__Iterator0_t1896461731 * L_0 = (U3CCullDespawnedU3Ec__Iterator0_t1896461731 *)il2cpp_codegen_object_new(U3CCullDespawnedU3Ec__Iterator0_t1896461731_il2cpp_TypeInfo_var);
		U3CCullDespawnedU3Ec__Iterator0__ctor_m2159543816(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCullDespawnedU3Ec__Iterator0_t1896461731 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CCullDespawnedU3Ec__Iterator0_t1896461731 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Transform PathologicalGames.PrefabPool::SpawnInstance(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * PrefabPool_SpawnInstance_m3748491065 (PrefabPool_t85422674 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_SpawnInstance_m3748491065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	String_t* V_2 = NULL;
	{
		bool L_0 = __this->get_limitInstances_6();
		if (!L_0)
		{
			goto IL_0084;
		}
	}
	{
		bool L_1 = __this->get_limitFIFO_8();
		if (!L_1)
		{
			goto IL_0084;
		}
	}
	{
		List_1_t2644239190 * L_2 = __this->get__spawned_17();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m936427142(L_2, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		int32_t L_4 = __this->get_limitAmount_7();
		if ((((int32_t)L_3) < ((int32_t)L_4)))
		{
			goto IL_0084;
		}
	}
	{
		List_1_t2644239190 * L_5 = __this->get__spawned_17();
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = List_1_get_Item_m563292115(L_5, 0, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_0 = L_6;
		bool L_7 = PrefabPool_get_logMessages_m4160510898(__this, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006a;
		}
	}
	{
		SpawnPool_t2419717525 * L_8 = __this->get_spawnPool_15();
		NullCheck(L_8);
		String_t* L_9 = L_8->get_poolName_2();
		Transform_t3275118058 * L_10 = __this->get_prefab_0();
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m2079638459(L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4048292431, L_9, L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_006a:
	{
		Transform_t3275118058 * L_14 = V_0;
		PrefabPool_DespawnInstance_m1597075065(__this, L_14, /*hidden argument*/NULL);
		SpawnPool_t2419717525 * L_15 = __this->get_spawnPool_15();
		NullCheck(L_15);
		List_1_t2644239190 * L_16 = L_15->get__spawned_15();
		Transform_t3275118058 * L_17 = V_0;
		NullCheck(L_16);
		List_1_Remove_m2326856859(L_16, L_17, /*hidden argument*/List_1_Remove_m2326856859_MethodInfo_var);
	}

IL_0084:
	{
		List_1_t2644239190 * L_18 = __this->get__despawned_18();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m936427142(L_18, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		if (L_19)
		{
			goto IL_00a2;
		}
	}
	{
		Vector3_t2243707580  L_20 = ___pos0;
		Quaternion_t4030073918  L_21 = ___rot1;
		Transform_t3275118058 * L_22 = PrefabPool_SpawnNew_m1137185856(__this, L_20, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		goto IL_0130;
	}

IL_00a2:
	{
		List_1_t2644239190 * L_23 = __this->get__despawned_18();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = List_1_get_Item_m563292115(L_23, 0, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_1 = L_24;
		List_1_t2644239190 * L_25 = __this->get__despawned_18();
		NullCheck(L_25);
		List_1_RemoveAt_m4270546658(L_25, 0, /*hidden argument*/List_1_RemoveAt_m4270546658_MethodInfo_var);
		List_1_t2644239190 * L_26 = __this->get__spawned_17();
		Transform_t3275118058 * L_27 = V_1;
		NullCheck(L_26);
		List_1_Add_m3606265932(L_26, L_27, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		Transform_t3275118058 * L_28 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_29 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_28, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00e0;
		}
	}
	{
		V_2 = _stringLiteral448805854;
		String_t* L_30 = V_2;
		MissingReferenceException_t1416808650 * L_31 = (MissingReferenceException_t1416808650 *)il2cpp_codegen_object_new(MissingReferenceException_t1416808650_il2cpp_TypeInfo_var);
		MissingReferenceException__ctor_m658435003(L_31, L_30, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_31);
	}

IL_00e0:
	{
		bool L_32 = PrefabPool_get_logMessages_m4160510898(__this, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0116;
		}
	}
	{
		SpawnPool_t2419717525 * L_33 = __this->get_spawnPool_15();
		NullCheck(L_33);
		String_t* L_34 = L_33->get_poolName_2();
		Transform_t3275118058 * L_35 = __this->get_prefab_0();
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m2079638459(L_35, /*hidden argument*/NULL);
		Transform_t3275118058 * L_37 = V_1;
		NullCheck(L_37);
		String_t* L_38 = Object_get_name_m2079638459(L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_39 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral3734970799, L_34, L_36, L_38, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
	}

IL_0116:
	{
		Transform_t3275118058 * L_40 = V_1;
		Vector3_t2243707580  L_41 = ___pos0;
		NullCheck(L_40);
		Transform_set_position_m2469242620(L_40, L_41, /*hidden argument*/NULL);
		Transform_t3275118058 * L_42 = V_1;
		Quaternion_t4030073918  L_43 = ___rot1;
		NullCheck(L_42);
		Transform_set_rotation_m3411284563(L_42, L_43, /*hidden argument*/NULL);
		Transform_t3275118058 * L_44 = V_1;
		NullCheck(L_44);
		GameObject_t1756533147 * L_45 = Component_get_gameObject_m3105766835(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		GameObject_SetActive_m2887581199(L_45, (bool)1, /*hidden argument*/NULL);
	}

IL_0130:
	{
		Transform_t3275118058 * L_46 = V_1;
		return L_46;
	}
}
// UnityEngine.Transform PathologicalGames.PrefabPool::SpawnNew()
extern "C"  Transform_t3275118058 * PrefabPool_SpawnNew_m3775111208 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_1 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = PrefabPool_SpawnNew_m1137185856(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Transform PathologicalGames.PrefabPool::SpawnNew(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * PrefabPool_SpawnNew_m1137185856 (PrefabPool_t85422674 * __this, Vector3_t2243707580  ___pos0, Quaternion_t4030073918  ___rot1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_SpawnNew_m1137185856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	bool V_2 = false;
	{
		bool L_0 = __this->get_limitInstances_6();
		if (!L_0)
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_1 = PrefabPool_get_totalCount_m1479453083(__this, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_limitAmount_7();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_004e;
		}
	}
	{
		bool L_3 = PrefabPool_get_logMessages_m4160510898(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		SpawnPool_t2419717525 * L_4 = __this->get_spawnPool_15();
		NullCheck(L_4);
		String_t* L_5 = L_4->get_poolName_2();
		Transform_t3275118058 * L_6 = __this->get_prefab_0();
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m2079638459(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3760092750, L_5, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return (Transform_t3275118058 *)NULL;
	}

IL_004e:
	{
		Vector3_t2243707580  L_9 = ___pos0;
		Vector3_t2243707580  L_10 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_11 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0070;
		}
	}
	{
		SpawnPool_t2419717525 * L_12 = __this->get_spawnPool_15();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = SpawnPool_get_group_m224041552(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t2243707580  L_14 = Transform_get_position_m1104419803(L_13, /*hidden argument*/NULL);
		___pos0 = L_14;
	}

IL_0070:
	{
		Quaternion_t4030073918  L_15 = ___rot1;
		Quaternion_t4030073918  L_16 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_17 = Quaternion_op_Equality_m2308156925(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0092;
		}
	}
	{
		SpawnPool_t2419717525 * L_18 = __this->get_spawnPool_15();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = SpawnPool_get_group_m224041552(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Quaternion_t4030073918  L_20 = Transform_get_rotation_m1033555130(L_19, /*hidden argument*/NULL);
		___rot1 = L_20;
	}

IL_0092:
	{
		SpawnPool_t2419717525 * L_21 = __this->get_spawnPool_15();
		GameObject_t1756533147 * L_22 = __this->get_prefabGO_1();
		Vector3_t2243707580  L_23 = ___pos0;
		Quaternion_t4030073918  L_24 = ___rot1;
		NullCheck(L_21);
		GameObject_t1756533147 * L_25 = SpawnPool_InstantiatePrefab_m3003297369(L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
		GameObject_t1756533147 * L_26 = V_0;
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = GameObject_get_transform_m909382139(L_26, /*hidden argument*/NULL);
		V_1 = L_27;
		Transform_t3275118058 * L_28 = V_1;
		PrefabPool_nameInstance_m1724321706(__this, L_28, /*hidden argument*/NULL);
		SpawnPool_t2419717525 * L_29 = __this->get_spawnPool_15();
		NullCheck(L_29);
		bool L_30 = L_29->get_dontReparent_5();
		if (L_30)
		{
			goto IL_00e3;
		}
	}
	{
		Transform_t3275118058 * L_31 = V_1;
		V_2 = (bool)((((int32_t)((!(((Il2CppObject*)(RectTransform_t3349966182 *)((RectTransform_t3349966182 *)IsInstSealed(L_31, RectTransform_t3349966182_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Transform_t3275118058 * L_32 = V_1;
		SpawnPool_t2419717525 * L_33 = __this->get_spawnPool_15();
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = SpawnPool_get_group_m224041552(L_33, /*hidden argument*/NULL);
		bool L_35 = V_2;
		NullCheck(L_32);
		Transform_SetParent_m1963830867(L_32, L_34, L_35, /*hidden argument*/NULL);
	}

IL_00e3:
	{
		SpawnPool_t2419717525 * L_36 = __this->get_spawnPool_15();
		NullCheck(L_36);
		bool L_37 = L_36->get_matchPoolScale_3();
		if (!L_37)
		{
			goto IL_00fe;
		}
	}
	{
		Transform_t3275118058 * L_38 = V_1;
		Vector3_t2243707580  L_39 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_38);
		Transform_set_localScale_m2325460848(L_38, L_39, /*hidden argument*/NULL);
	}

IL_00fe:
	{
		SpawnPool_t2419717525 * L_40 = __this->get_spawnPool_15();
		NullCheck(L_40);
		bool L_41 = L_40->get_matchPoolLayer_4();
		if (!L_41)
		{
			goto IL_0125;
		}
	}
	{
		Transform_t3275118058 * L_42 = V_1;
		SpawnPool_t2419717525 * L_43 = __this->get_spawnPool_15();
		NullCheck(L_43);
		GameObject_t1756533147 * L_44 = Component_get_gameObject_m3105766835(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45 = GameObject_get_layer_m725607808(L_44, /*hidden argument*/NULL);
		PrefabPool_SetRecursively_m1140085826(__this, L_42, L_45, /*hidden argument*/NULL);
	}

IL_0125:
	{
		List_1_t2644239190 * L_46 = __this->get__spawned_17();
		Transform_t3275118058 * L_47 = V_1;
		NullCheck(L_46);
		List_1_Add_m3606265932(L_46, L_47, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		bool L_48 = PrefabPool_get_logMessages_m4160510898(__this, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0167;
		}
	}
	{
		SpawnPool_t2419717525 * L_49 = __this->get_spawnPool_15();
		NullCheck(L_49);
		String_t* L_50 = L_49->get_poolName_2();
		Transform_t3275118058 * L_51 = __this->get_prefab_0();
		NullCheck(L_51);
		String_t* L_52 = Object_get_name_m2079638459(L_51, /*hidden argument*/NULL);
		Transform_t3275118058 * L_53 = V_1;
		NullCheck(L_53);
		String_t* L_54 = Object_get_name_m2079638459(L_53, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral95200360, L_50, L_52, L_54, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
	}

IL_0167:
	{
		Transform_t3275118058 * L_56 = V_1;
		return L_56;
	}
}
// System.Void PathologicalGames.PrefabPool::SetRecursively(UnityEngine.Transform,System.Int32)
extern "C"  void PrefabPool_SetRecursively_m1140085826 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___xform0, int32_t ___layer1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_SetRecursively_m1140085826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___xform0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer1;
		NullCheck(L_1);
		GameObject_set_layer_m2712461877(L_1, L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = ___xform0;
		NullCheck(L_3);
		Il2CppObject * L_4 = Transform_GetEnumerator_m3479720613(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0018:
		{
			Il2CppObject * L_5 = V_1;
			NullCheck(L_5);
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_5);
			V_0 = ((Transform_t3275118058 *)CastclassClass(L_6, Transform_t3275118058_il2cpp_TypeInfo_var));
			Transform_t3275118058 * L_7 = V_0;
			int32_t L_8 = ___layer1;
			PrefabPool_SetRecursively_m1140085826(__this, L_7, L_8, /*hidden argument*/NULL);
		}

IL_002c:
		{
			Il2CppObject * L_9 = V_1;
			NullCheck(L_9);
			bool L_10 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_9);
			if (L_10)
			{
				goto IL_0018;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x50, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_11 = V_1;
			Il2CppObject * L_12 = ((Il2CppObject *)IsInst(L_11, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_2 = L_12;
			if (!L_12)
			{
				goto IL_004f;
			}
		}

IL_0049:
		{
			Il2CppObject * L_13 = V_2;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_13);
		}

IL_004f:
		{
			IL2CPP_END_FINALLY(60)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0050:
	{
		return;
	}
}
// System.Void PathologicalGames.PrefabPool::AddUnpooled(UnityEngine.Transform,System.Boolean)
extern "C"  void PrefabPool_AddUnpooled_m2140288438 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___inst0, bool ___despawn1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_AddUnpooled_m2140288438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = ___inst0;
		PrefabPool_nameInstance_m1724321706(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___despawn1;
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		Transform_t3275118058 * L_2 = ___inst0;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		List_1_t2644239190 * L_4 = __this->get__despawned_18();
		Transform_t3275118058 * L_5 = ___inst0;
		NullCheck(L_4);
		List_1_Add_m3606265932(L_4, L_5, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		goto IL_0036;
	}

IL_002a:
	{
		List_1_t2644239190 * L_6 = __this->get__spawned_17();
		Transform_t3275118058 * L_7 = ___inst0;
		NullCheck(L_6);
		List_1_Add_m3606265932(L_6, L_7, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
	}

IL_0036:
	{
		return;
	}
}
// System.Void PathologicalGames.PrefabPool::PreloadInstances()
extern "C"  void PrefabPool_PreloadInstances_m3084730458 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_PreloadInstances_m3084730458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	{
		bool L_0 = PrefabPool_get_preloaded_m270443884(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		SpawnPool_t2419717525 * L_1 = __this->get_spawnPool_15();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_poolName_2();
		Transform_t3275118058 * L_3 = __this->get_prefab_0();
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3321977572, L_2, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0031:
	{
		PrefabPool_set_preloaded_m1695494847(__this, (bool)1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006f;
		}
	}
	{
		SpawnPool_t2419717525 * L_8 = __this->get_spawnPool_15();
		NullCheck(L_8);
		String_t* L_9 = L_8->get_poolName_2();
		Transform_t3275118058 * L_10 = __this->get_prefab_0();
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m2079638459(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3789224074, L_9, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return;
	}

IL_006f:
	{
		bool L_13 = __this->get_limitInstances_6();
		if (!L_13)
		{
			goto IL_00bc;
		}
	}
	{
		int32_t L_14 = __this->get_preloadAmount_2();
		int32_t L_15 = __this->get_limitAmount_7();
		if ((((int32_t)L_14) <= ((int32_t)L_15)))
		{
			goto IL_00bc;
		}
	}
	{
		SpawnPool_t2419717525 * L_16 = __this->get_spawnPool_15();
		NullCheck(L_16);
		String_t* L_17 = L_16->get_poolName_2();
		Transform_t3275118058 * L_18 = __this->get_prefab_0();
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m2079638459(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2943401089, L_17, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		int32_t L_21 = __this->get_limitAmount_7();
		__this->set_preloadAmount_2(L_21);
	}

IL_00bc:
	{
		bool L_22 = __this->get_cullDespawned_9();
		if (!L_22)
		{
			goto IL_00fd;
		}
	}
	{
		int32_t L_23 = __this->get_preloadAmount_2();
		int32_t L_24 = __this->get_cullAbove_10();
		if ((((int32_t)L_23) <= ((int32_t)L_24)))
		{
			goto IL_00fd;
		}
	}
	{
		SpawnPool_t2419717525 * L_25 = __this->get_spawnPool_15();
		NullCheck(L_25);
		String_t* L_26 = L_25->get_poolName_2();
		Transform_t3275118058 * L_27 = __this->get_prefab_0();
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m2079638459(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3649213935, L_26, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00fd:
	{
		bool L_30 = __this->get_preloadTime_3();
		if (!L_30)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_31 = __this->get_preloadFrames_4();
		int32_t L_32 = __this->get_preloadAmount_2();
		if ((((int32_t)L_31) <= ((int32_t)L_32)))
		{
			goto IL_014a;
		}
	}
	{
		SpawnPool_t2419717525 * L_33 = __this->get_spawnPool_15();
		NullCheck(L_33);
		String_t* L_34 = L_33->get_poolName_2();
		Transform_t3275118058 * L_35 = __this->get_prefab_0();
		NullCheck(L_35);
		String_t* L_36 = Object_get_name_m2079638459(L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3361875080, L_34, L_36, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		int32_t L_38 = __this->get_preloadAmount_2();
		__this->set_preloadFrames_4(L_38);
	}

IL_014a:
	{
		SpawnPool_t2419717525 * L_39 = __this->get_spawnPool_15();
		Il2CppObject * L_40 = PrefabPool_PreloadOverTime_m604118577(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		MonoBehaviour_StartCoroutine_m2470621050(L_39, L_40, /*hidden argument*/NULL);
		goto IL_0195;
	}

IL_0161:
	{
		__this->set_forceLoggingSilent_14((bool)1);
		goto IL_017d;
	}

IL_016d:
	{
		Transform_t3275118058 * L_41 = PrefabPool_SpawnNew_m3775111208(__this, /*hidden argument*/NULL);
		V_0 = L_41;
		Transform_t3275118058 * L_42 = V_0;
		PrefabPool_DespawnInstance_m2725861114(__this, L_42, (bool)0, /*hidden argument*/NULL);
	}

IL_017d:
	{
		int32_t L_43 = PrefabPool_get_totalCount_m1479453083(__this, /*hidden argument*/NULL);
		int32_t L_44 = __this->get_preloadAmount_2();
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_016d;
		}
	}
	{
		__this->set_forceLoggingSilent_14((bool)0);
	}

IL_0195:
	{
		return;
	}
}
// System.Collections.IEnumerator PathologicalGames.PrefabPool::PreloadOverTime()
extern "C"  Il2CppObject * PrefabPool_PreloadOverTime_m604118577 (PrefabPool_t85422674 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_PreloadOverTime_m604118577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * V_0 = NULL;
	{
		U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * L_0 = (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 *)il2cpp_codegen_object_new(U3CPreloadOverTimeU3Ec__Iterator1_t1067013873_il2cpp_TypeInfo_var);
		U3CPreloadOverTimeU3Ec__Iterator1__ctor_m2540324496(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_7(__this);
		U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * L_2 = V_0;
		return L_2;
	}
}
// System.Boolean PathologicalGames.PrefabPool::Contains(UnityEngine.Transform)
extern "C"  bool PrefabPool_Contains_m3543239565 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___transform0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_Contains_m3543239565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		GameObject_t1756533147 * L_0 = __this->get_prefabGO_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		SpawnPool_t2419717525 * L_2 = __this->get_spawnPool_15();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral885759378, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		List_1_t2644239190 * L_5 = PrefabPool_get_spawned_m365923230(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = ___transform0;
		NullCheck(L_5);
		bool L_7 = List_1_Contains_m2540959346(L_5, L_6, /*hidden argument*/List_1_Contains_m2540959346_MethodInfo_var);
		V_0 = L_7;
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0040;
		}
	}
	{
		return (bool)1;
	}

IL_0040:
	{
		List_1_t2644239190 * L_9 = PrefabPool_get_despawned_m3902220117(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = ___transform0;
		NullCheck(L_9);
		bool L_11 = List_1_Contains_m2540959346(L_9, L_10, /*hidden argument*/List_1_Contains_m2540959346_MethodInfo_var);
		V_0 = L_11;
		bool L_12 = V_0;
		if (!L_12)
		{
			goto IL_0055;
		}
	}
	{
		return (bool)1;
	}

IL_0055:
	{
		return (bool)0;
	}
}
// System.Void PathologicalGames.PrefabPool::nameInstance(UnityEngine.Transform)
extern "C"  void PrefabPool_nameInstance_m1724321706 (PrefabPool_t85422674 * __this, Transform_t3275118058 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabPool_nameInstance_m1724321706_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Transform_t3275118058 * L_0 = ___instance0;
		Transform_t3275118058 * L_1 = L_0;
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		int32_t L_3 = PrefabPool_get_totalCount_m1479453083(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
		String_t* L_4 = Int32_ToString_m1064459878((&V_0), _stringLiteral1104291695, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		Object_set_name_m4157836998(L_1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::.ctor()
extern "C"  void U3CCullDespawnedU3Ec__Iterator0__ctor_m2159543816 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::MoveNext()
extern "C"  bool U3CCullDespawnedU3Ec__Iterator0_MoveNext_m1056465212 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCullDespawnedU3Ec__Iterator0_MoveNext_m1056465212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	Transform_t3275118058 * V_2 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_00a3;
		}
		if (L_1 == 2)
		{
			goto IL_0228;
		}
		if (L_1 == 3)
		{
			goto IL_02a9;
		}
	}
	{
		goto IL_02b0;
	}

IL_0029:
	{
		PrefabPool_t85422674 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		bool L_3 = PrefabPool_get_logMessages_m4160510898(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0078;
		}
	}
	{
		PrefabPool_t85422674 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		SpawnPool_t2419717525 * L_5 = L_4->get_spawnPool_15();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_poolName_2();
		PrefabPool_t85422674 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = L_7->get_prefab_0();
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m2079638459(L_8, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_10 = __this->get_U24this_0();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_cullDelay_11();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral3800559023, L_6, L_9, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0078:
	{
		PrefabPool_t85422674 * L_15 = __this->get_U24this_0();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_cullDelay_11();
		WaitForSeconds_t3839502067 * L_17 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_17, (((float)((float)L_16))), /*hidden argument*/NULL);
		__this->set_U24current_1(L_17);
		bool L_18 = __this->get_U24disposing_2();
		if (L_18)
		{
			goto IL_009e;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_009e:
	{
		goto IL_02b2;
	}

IL_00a3:
	{
		goto IL_0228;
	}

IL_00a8:
	{
		V_1 = 0;
		goto IL_01ec;
	}

IL_00af:
	{
		PrefabPool_t85422674 * L_19 = __this->get_U24this_0();
		NullCheck(L_19);
		int32_t L_20 = PrefabPool_get_totalCount_m1479453083(L_19, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_21 = __this->get_U24this_0();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_cullAbove_10();
		if ((((int32_t)L_20) > ((int32_t)L_22)))
		{
			goto IL_00cf;
		}
	}
	{
		goto IL_01fd;
	}

IL_00cf:
	{
		PrefabPool_t85422674 * L_23 = __this->get_U24this_0();
		NullCheck(L_23);
		List_1_t2644239190 * L_24 = L_23->get__despawned_18();
		NullCheck(L_24);
		int32_t L_25 = List_1_get_Count_m936427142(L_24, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		if ((((int32_t)L_25) <= ((int32_t)0)))
		{
			goto IL_0194;
		}
	}
	{
		PrefabPool_t85422674 * L_26 = __this->get_U24this_0();
		NullCheck(L_26);
		List_1_t2644239190 * L_27 = L_26->get__despawned_18();
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = List_1_get_Item_m563292115(L_27, 0, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		V_2 = L_28;
		PrefabPool_t85422674 * L_29 = __this->get_U24this_0();
		NullCheck(L_29);
		List_1_t2644239190 * L_30 = L_29->get__despawned_18();
		NullCheck(L_30);
		List_1_RemoveAt_m4270546658(L_30, 0, /*hidden argument*/List_1_RemoveAt_m4270546658_MethodInfo_var);
		PrefabPool_t85422674 * L_31 = __this->get_U24this_0();
		NullCheck(L_31);
		SpawnPool_t2419717525 * L_32 = L_31->get_spawnPool_15();
		Transform_t3275118058 * L_33 = V_2;
		NullCheck(L_33);
		GameObject_t1756533147 * L_34 = Component_get_gameObject_m3105766835(L_33, /*hidden argument*/NULL);
		NullCheck(L_32);
		SpawnPool_DestroyInstance_m2432744115(L_32, L_34, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_35 = __this->get_U24this_0();
		NullCheck(L_35);
		bool L_36 = PrefabPool_get_logMessages_m4160510898(L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_018f;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_37 = ((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)4));
		PrefabPool_t85422674 * L_38 = __this->get_U24this_0();
		NullCheck(L_38);
		SpawnPool_t2419717525 * L_39 = L_38->get_spawnPool_15();
		NullCheck(L_39);
		String_t* L_40 = L_39->get_poolName_2();
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_40);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_40);
		ObjectU5BU5D_t3614634134* L_41 = L_37;
		PrefabPool_t85422674 * L_42 = __this->get_U24this_0();
		NullCheck(L_42);
		Transform_t3275118058 * L_43 = L_42->get_prefab_0();
		NullCheck(L_43);
		String_t* L_44 = Object_get_name_m2079638459(L_43, /*hidden argument*/NULL);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_44);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t3614634134* L_45 = L_41;
		PrefabPool_t85422674 * L_46 = __this->get_U24this_0();
		NullCheck(L_46);
		int32_t L_47 = L_46->get_cullAbove_10();
		int32_t L_48 = L_47;
		Il2CppObject * L_49 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_49);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_49);
		ObjectU5BU5D_t3614634134* L_50 = L_45;
		PrefabPool_t85422674 * L_51 = __this->get_U24this_0();
		NullCheck(L_51);
		int32_t L_52 = PrefabPool_get_totalCount_m1479453083(L_51, /*hidden argument*/NULL);
		int32_t L_53 = L_52;
		Il2CppObject * L_54 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, L_54);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_54);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Format_m1263743648(NULL /*static, unused*/, _stringLiteral4274858464, L_50, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
	}

IL_018f:
	{
		goto IL_01e8;
	}

IL_0194:
	{
		PrefabPool_t85422674 * L_56 = __this->get_U24this_0();
		NullCheck(L_56);
		bool L_57 = PrefabPool_get_logMessages_m4160510898(L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01e8;
		}
	}
	{
		PrefabPool_t85422674 * L_58 = __this->get_U24this_0();
		NullCheck(L_58);
		SpawnPool_t2419717525 * L_59 = L_58->get_spawnPool_15();
		NullCheck(L_59);
		String_t* L_60 = L_59->get_poolName_2();
		PrefabPool_t85422674 * L_61 = __this->get_U24this_0();
		NullCheck(L_61);
		Transform_t3275118058 * L_62 = L_61->get_prefab_0();
		NullCheck(L_62);
		String_t* L_63 = Object_get_name_m2079638459(L_62, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_64 = __this->get_U24this_0();
		NullCheck(L_64);
		int32_t L_65 = L_64->get_cullDelay_11();
		int32_t L_66 = L_65;
		Il2CppObject * L_67 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_66);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_68 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral249778527, L_60, L_63, L_67, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		goto IL_01fd;
	}

IL_01e8:
	{
		int32_t L_69 = V_1;
		V_1 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_01ec:
	{
		int32_t L_70 = V_1;
		PrefabPool_t85422674 * L_71 = __this->get_U24this_0();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_cullMaxPerPass_12();
		if ((((int32_t)L_70) < ((int32_t)L_72)))
		{
			goto IL_00af;
		}
	}

IL_01fd:
	{
		PrefabPool_t85422674 * L_73 = __this->get_U24this_0();
		NullCheck(L_73);
		int32_t L_74 = L_73->get_cullDelay_11();
		WaitForSeconds_t3839502067 * L_75 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_75, (((float)((float)L_74))), /*hidden argument*/NULL);
		__this->set_U24current_1(L_75);
		bool L_76 = __this->get_U24disposing_2();
		if (L_76)
		{
			goto IL_0223;
		}
	}
	{
		__this->set_U24PC_3(2);
	}

IL_0223:
	{
		goto IL_02b2;
	}

IL_0228:
	{
		PrefabPool_t85422674 * L_77 = __this->get_U24this_0();
		NullCheck(L_77);
		int32_t L_78 = PrefabPool_get_totalCount_m1479453083(L_77, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_79 = __this->get_U24this_0();
		NullCheck(L_79);
		int32_t L_80 = L_79->get_cullAbove_10();
		if ((((int32_t)L_78) > ((int32_t)L_80)))
		{
			goto IL_00a8;
		}
	}
	{
		PrefabPool_t85422674 * L_81 = __this->get_U24this_0();
		NullCheck(L_81);
		bool L_82 = PrefabPool_get_logMessages_m4160510898(L_81, /*hidden argument*/NULL);
		if (!L_82)
		{
			goto IL_0282;
		}
	}
	{
		PrefabPool_t85422674 * L_83 = __this->get_U24this_0();
		NullCheck(L_83);
		SpawnPool_t2419717525 * L_84 = L_83->get_spawnPool_15();
		NullCheck(L_84);
		String_t* L_85 = L_84->get_poolName_2();
		PrefabPool_t85422674 * L_86 = __this->get_U24this_0();
		NullCheck(L_86);
		Transform_t3275118058 * L_87 = L_86->get_prefab_0();
		NullCheck(L_87);
		String_t* L_88 = Object_get_name_m2079638459(L_87, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_89 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral595297118, L_85, L_88, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
	}

IL_0282:
	{
		PrefabPool_t85422674 * L_90 = __this->get_U24this_0();
		NullCheck(L_90);
		L_90->set_cullingActive_16((bool)0);
		__this->set_U24current_1(NULL);
		bool L_91 = __this->get_U24disposing_2();
		if (L_91)
		{
			goto IL_02a4;
		}
	}
	{
		__this->set_U24PC_3(3);
	}

IL_02a4:
	{
		goto IL_02b2;
	}

IL_02a9:
	{
		__this->set_U24PC_3((-1));
	}

IL_02b0:
	{
		return (bool)0;
	}

IL_02b2:
	{
		return (bool)1;
	}
}
// System.Object PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCullDespawnedU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2749439424 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCullDespawnedU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2574874296 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::Dispose()
extern "C"  void U3CCullDespawnedU3Ec__Iterator0_Dispose_m993551871 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void PathologicalGames.PrefabPool/<CullDespawned>c__Iterator0::Reset()
extern "C"  void U3CCullDespawnedU3Ec__Iterator0_Reset_m3236476073 (U3CCullDespawnedU3Ec__Iterator0_t1896461731 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCullDespawnedU3Ec__Iterator0_Reset_m3236476073_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::.ctor()
extern "C"  void U3CPreloadOverTimeU3Ec__Iterator1__ctor_m2540324496 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::MoveNext()
extern "C"  bool U3CPreloadOverTimeU3Ec__Iterator1_MoveNext_m2163773428 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPreloadOverTimeU3Ec__Iterator1_MoveNext_m2163773428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_10();
		V_0 = L_0;
		__this->set_U24PC_10((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
		if (L_1 == 2)
		{
			goto IL_0158;
		}
	}
	{
		goto IL_01ce;
	}

IL_0025:
	{
		PrefabPool_t85422674 * L_2 = __this->get_U24this_7();
		NullCheck(L_2);
		float L_3 = L_2->get_preloadDelay_5();
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_8(L_4);
		bool L_5 = __this->get_U24disposing_9();
		if (L_5)
		{
			goto IL_004a;
		}
	}
	{
		__this->set_U24PC_10(1);
	}

IL_004a:
	{
		goto IL_01d0;
	}

IL_004f:
	{
		PrefabPool_t85422674 * L_6 = __this->get_U24this_7();
		NullCheck(L_6);
		int32_t L_7 = L_6->get_preloadAmount_2();
		PrefabPool_t85422674 * L_8 = __this->get_U24this_7();
		NullCheck(L_8);
		int32_t L_9 = PrefabPool_get_totalCount_m1479453083(L_8, /*hidden argument*/NULL);
		__this->set_U3CamountU3E__0_0(((int32_t)((int32_t)L_7-(int32_t)L_9)));
		int32_t L_10 = __this->get_U3CamountU3E__0_0();
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_007d;
		}
	}
	{
		goto IL_01ce;
	}

IL_007d:
	{
		int32_t L_11 = __this->get_U3CamountU3E__0_0();
		PrefabPool_t85422674 * L_12 = __this->get_U24this_7();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_preloadFrames_4();
		__this->set_U3CremainderU3E__0_1(((int32_t)((int32_t)L_11%(int32_t)L_13)));
		int32_t L_14 = __this->get_U3CamountU3E__0_0();
		PrefabPool_t85422674 * L_15 = __this->get_U24this_7();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_preloadFrames_4();
		__this->set_U3CnumPerFrameU3E__0_2(((int32_t)((int32_t)L_14/(int32_t)L_16)));
		PrefabPool_t85422674 * L_17 = __this->get_U24this_7();
		NullCheck(L_17);
		L_17->set_forceLoggingSilent_14((bool)1);
		__this->set_U3CiU3E__1_3(0);
		goto IL_01a5;
	}

IL_00c5:
	{
		int32_t L_18 = __this->get_U3CnumPerFrameU3E__0_2();
		__this->set_U3CnumThisFrameU3E__2_4(L_18);
		int32_t L_19 = __this->get_U3CiU3E__1_3();
		PrefabPool_t85422674 * L_20 = __this->get_U24this_7();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_preloadFrames_4();
		if ((!(((uint32_t)L_19) == ((uint32_t)((int32_t)((int32_t)L_21-(int32_t)1))))))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_22 = __this->get_U3CnumThisFrameU3E__2_4();
		int32_t L_23 = __this->get_U3CremainderU3E__0_1();
		__this->set_U3CnumThisFrameU3E__2_4(((int32_t)((int32_t)L_22+(int32_t)L_23)));
	}

IL_00fc:
	{
		__this->set_U3CnU3E__3_5(0);
		goto IL_0166;
	}

IL_0108:
	{
		PrefabPool_t85422674 * L_24 = __this->get_U24this_7();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = PrefabPool_SpawnNew_m3775111208(L_24, /*hidden argument*/NULL);
		__this->set_U3CinstU3E__4_6(L_25);
		Transform_t3275118058 * L_26 = __this->get_U3CinstU3E__4_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_27 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_26, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_013d;
		}
	}
	{
		PrefabPool_t85422674 * L_28 = __this->get_U24this_7();
		Transform_t3275118058 * L_29 = __this->get_U3CinstU3E__4_6();
		NullCheck(L_28);
		PrefabPool_DespawnInstance_m2725861114(L_28, L_29, (bool)0, /*hidden argument*/NULL);
	}

IL_013d:
	{
		__this->set_U24current_8(NULL);
		bool L_30 = __this->get_U24disposing_9();
		if (L_30)
		{
			goto IL_0153;
		}
	}
	{
		__this->set_U24PC_10(2);
	}

IL_0153:
	{
		goto IL_01d0;
	}

IL_0158:
	{
		int32_t L_31 = __this->get_U3CnU3E__3_5();
		__this->set_U3CnU3E__3_5(((int32_t)((int32_t)L_31+(int32_t)1)));
	}

IL_0166:
	{
		int32_t L_32 = __this->get_U3CnU3E__3_5();
		int32_t L_33 = __this->get_U3CnumThisFrameU3E__2_4();
		if ((((int32_t)L_32) < ((int32_t)L_33)))
		{
			goto IL_0108;
		}
	}
	{
		PrefabPool_t85422674 * L_34 = __this->get_U24this_7();
		NullCheck(L_34);
		int32_t L_35 = PrefabPool_get_totalCount_m1479453083(L_34, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_36 = __this->get_U24this_7();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_preloadAmount_2();
		if ((((int32_t)L_35) <= ((int32_t)L_37)))
		{
			goto IL_0197;
		}
	}
	{
		goto IL_01bb;
	}

IL_0197:
	{
		int32_t L_38 = __this->get_U3CiU3E__1_3();
		__this->set_U3CiU3E__1_3(((int32_t)((int32_t)L_38+(int32_t)1)));
	}

IL_01a5:
	{
		int32_t L_39 = __this->get_U3CiU3E__1_3();
		PrefabPool_t85422674 * L_40 = __this->get_U24this_7();
		NullCheck(L_40);
		int32_t L_41 = L_40->get_preloadFrames_4();
		if ((((int32_t)L_39) < ((int32_t)L_41)))
		{
			goto IL_00c5;
		}
	}

IL_01bb:
	{
		PrefabPool_t85422674 * L_42 = __this->get_U24this_7();
		NullCheck(L_42);
		L_42->set_forceLoggingSilent_14((bool)0);
		__this->set_U24PC_10((-1));
	}

IL_01ce:
	{
		return (bool)0;
	}

IL_01d0:
	{
		return (bool)1;
	}
}
// System.Object PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPreloadOverTimeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3163636216 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Object PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPreloadOverTimeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3138599216 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_8();
		return L_0;
	}
}
// System.Void PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::Dispose()
extern "C"  void U3CPreloadOverTimeU3Ec__Iterator1_Dispose_m3214189537 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_9((bool)1);
		__this->set_U24PC_10((-1));
		return;
	}
}
// System.Void PathologicalGames.PrefabPool/<PreloadOverTime>c__Iterator1::Reset()
extern "C"  void U3CPreloadOverTimeU3Ec__Iterator1_Reset_m204175095 (U3CPreloadOverTimeU3Ec__Iterator1_t1067013873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPreloadOverTimeU3Ec__Iterator1_Reset_m204175095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.PrefabsDict::.ctor()
extern "C"  void PrefabsDict__ctor_m4168763172 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict__ctor_m4168763172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = (Dictionary_2_t894930024 *)il2cpp_codegen_object_new(Dictionary_2_t894930024_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1861750071(L_0, /*hidden argument*/Dictionary_2__ctor_m1861750071_MethodInfo_var);
		__this->set__prefabs_0(L_0);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PathologicalGames.PrefabsDict::ToString()
extern "C"  String_t* PrefabsDict_ToString_m2224302563 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_ToString_m2224302563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m2934043879(L_0, /*hidden argument*/Dictionary_2_get_Count_m2934043879_MethodInfo_var);
		V_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_1));
		Dictionary_2_t894930024 * L_2 = __this->get__prefabs_0();
		NullCheck(L_2);
		KeyCollection_t3378427795 * L_3 = Dictionary_2_get_Keys_m2836166341(L_2, /*hidden argument*/Dictionary_2_get_Keys_m2836166341_MethodInfo_var);
		StringU5BU5D_t1642385972* L_4 = V_0;
		NullCheck(L_3);
		KeyCollection_CopyTo_m1254677401(L_3, L_4, 0, /*hidden argument*/KeyCollection_CopyTo_m1254677401_MethodInfo_var);
		StringU5BU5D_t1642385972* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral811305474, L_5, /*hidden argument*/NULL);
		String_t* L_7 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1555060386, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void PathologicalGames.PrefabsDict::_Add(System.String,UnityEngine.Transform)
extern "C"  void PrefabsDict__Add_m2864700459 (PrefabsDict_t1121107021 * __this, String_t* ___prefabName0, Transform_t3275118058 * ___prefab1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict__Add_m2864700459_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		String_t* L_1 = ___prefabName0;
		Transform_t3275118058 * L_2 = ___prefab1;
		NullCheck(L_0);
		Dictionary_2_Add_m3540954367(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_Add_m3540954367_MethodInfo_var);
		return;
	}
}
// System.Boolean PathologicalGames.PrefabsDict::_Remove(System.String)
extern "C"  bool PrefabsDict__Remove_m2445122851 (PrefabsDict_t1121107021 * __this, String_t* ___prefabName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict__Remove_m2445122851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		String_t* L_1 = ___prefabName0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_Remove_m708479600(L_0, L_1, /*hidden argument*/Dictionary_2_Remove_m708479600_MethodInfo_var);
		return L_2;
	}
}
// System.Void PathologicalGames.PrefabsDict::_Clear()
extern "C"  void PrefabsDict__Clear_m3612909952 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict__Clear_m3612909952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		Dictionary_2_Clear_m1781593424(L_0, /*hidden argument*/Dictionary_2_Clear_m1781593424_MethodInfo_var);
		return;
	}
}
// System.Int32 PathologicalGames.PrefabsDict::get_Count()
extern "C"  int32_t PrefabsDict_get_Count_m3292685014 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_get_Count_m3292685014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m2934043879(L_0, /*hidden argument*/Dictionary_2_get_Count_m2934043879_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean PathologicalGames.PrefabsDict::ContainsKey(System.String)
extern "C"  bool PrefabsDict_ContainsKey_m1851170860 (PrefabsDict_t1121107021 * __this, String_t* ___prefabName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_ContainsKey_m1851170860_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		String_t* L_1 = ___prefabName0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m315213716(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m315213716_MethodInfo_var);
		return L_2;
	}
}
// System.Boolean PathologicalGames.PrefabsDict::TryGetValue(System.String,UnityEngine.Transform&)
extern "C"  bool PrefabsDict_TryGetValue_m3983585977 (PrefabsDict_t1121107021 * __this, String_t* ___prefabName0, Transform_t3275118058 ** ___prefab1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_TryGetValue_m3983585977_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		String_t* L_1 = ___prefabName0;
		Transform_t3275118058 ** L_2 = ___prefab1;
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m3825486608(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_TryGetValue_m3825486608_MethodInfo_var);
		return L_3;
	}
}
// System.Void PathologicalGames.PrefabsDict::Add(System.String,UnityEngine.Transform)
extern "C"  void PrefabsDict_Add_m3234735052 (PrefabsDict_t1121107021 * __this, String_t* ___key0, Transform_t3275118058 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_Add_m3234735052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral2250443503, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PathologicalGames.PrefabsDict::Remove(System.String)
extern "C"  bool PrefabsDict_Remove_m4224709130 (PrefabsDict_t1121107021 * __this, String_t* ___prefabName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_Remove_m4224709130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral2250443503, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PathologicalGames.PrefabsDict::Contains(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>)
extern "C"  bool PrefabsDict_Contains_m3265497923 (PrefabsDict_t1121107021 * __this, KeyValuePair_2_t2947242542  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_Contains_m3265497923_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral440679323;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// UnityEngine.Transform PathologicalGames.PrefabsDict::get_Item(System.String)
extern "C"  Transform_t3275118058 * PrefabsDict_get_Item_m254611088 (PrefabsDict_t1121107021 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_get_Item_m254611088_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = Dictionary_2_get_Item_m777678513(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_m777678513_MethodInfo_var);
		V_0 = L_2;
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (KeyNotFoundException_t1722175009_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0012;
		throw e;
	}

CATCH_0012:
	{ // begin catch(System.Collections.Generic.KeyNotFoundException)
		String_t* L_3 = ___key0;
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral815531349, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = V_1;
		KeyNotFoundException_t1722175009 * L_7 = (KeyNotFoundException_t1722175009 *)il2cpp_codegen_object_new(KeyNotFoundException_t1722175009_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m264393096(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	} // end catch (depth: 1)

IL_002c:
	{
		Transform_t3275118058 * L_8 = V_0;
		return L_8;
	}
}
// System.Void PathologicalGames.PrefabsDict::set_Item(System.String,UnityEngine.Transform)
extern "C"  void PrefabsDict_set_Item_m3790954465 (PrefabsDict_t1121107021 * __this, String_t* ___key0, Transform_t3275118058 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_set_Item_m3790954465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral142138905, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.ICollection`1<System.String> PathologicalGames.PrefabsDict::get_Keys()
extern "C"  Il2CppObject* PrefabsDict_get_Keys_m473500759 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_get_Keys_m473500759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		KeyCollection_t3378427795 * L_1 = Dictionary_2_get_Keys_m2836166341(L_0, /*hidden argument*/Dictionary_2_get_Keys_m2836166341_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.ICollection`1<UnityEngine.Transform> PathologicalGames.PrefabsDict::get_Values()
extern "C"  Il2CppObject* PrefabsDict_get_Values_m4202167090 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_get_Values_m4202167090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		ValueCollection_t3892957163 * L_1 = Dictionary_2_get_Values_m1688729445(L_0, /*hidden argument*/Dictionary_2_get_Values_m1688729445_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean PathologicalGames.PrefabsDict::get_IsReadOnly()
extern "C"  bool PrefabsDict_get_IsReadOnly_m1915445045 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean PathologicalGames.PrefabsDict::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,UnityEngine.Transform>>.get_IsReadOnly()
extern "C"  bool PrefabsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CUnityEngine_TransformU3EU3E_get_IsReadOnly_m3466472496 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void PathologicalGames.PrefabsDict::Add(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>)
extern "C"  void PrefabsDict_Add_m443739205 (PrefabsDict_t1121107021 * __this, KeyValuePair_2_t2947242542  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_Add_m443739205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral3473335183, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.PrefabsDict::Clear()
extern "C"  void PrefabsDict_Clear_m1374396897 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_Clear_m1374396897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.PrefabsDict::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>[],System.Int32)
extern "C"  void PrefabsDict_CopyTo_m3923091189 (PrefabsDict_t1121107021 * __this, KeyValuePair_2U5BU5D_t3482534235* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_CopyTo_m3923091189_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3818382634;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void PathologicalGames.PrefabsDict::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,UnityEngine.Transform>>.CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>[],System.Int32)
extern "C"  void PrefabsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CUnityEngine_TransformU3EU3E_CopyTo_m2846345044 (PrefabsDict_t1121107021 * __this, KeyValuePair_2U5BU5D_t3482534235* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CUnityEngine_TransformU3EU3E_CopyTo_m2846345044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3818382634;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Boolean PathologicalGames.PrefabsDict::Remove(System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>)
extern "C"  bool PrefabsDict_Remove_m2402712344 (PrefabsDict_t1121107021 * __this, KeyValuePair_2_t2947242542  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_Remove_m2402712344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral3473335183, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.Transform>> PathologicalGames.PrefabsDict::GetEnumerator()
extern "C"  Il2CppObject* PrefabsDict_GetEnumerator_m2554566656 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_GetEnumerator_m2554566656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		Enumerator_t2214954726  L_1 = Dictionary_2_GetEnumerator_m2685922707(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2685922707_MethodInfo_var);
		Enumerator_t2214954726  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t2214954726_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.Collections.IEnumerator PathologicalGames.PrefabsDict::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * PrefabsDict_System_Collections_IEnumerable_GetEnumerator_m1785244313 (PrefabsDict_t1121107021 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PrefabsDict_System_Collections_IEnumerable_GetEnumerator_m1785244313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t894930024 * L_0 = __this->get__prefabs_0();
		NullCheck(L_0);
		Enumerator_t2214954726  L_1 = Dictionary_2_GetEnumerator_m2685922707(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2685922707_MethodInfo_var);
		Enumerator_t2214954726  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t2214954726_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.Void PathologicalGames.PreRuntimePoolItem::.ctor()
extern "C"  void PreRuntimePoolItem__ctor_m1575543045 (PreRuntimePoolItem_t3584992064 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PreRuntimePoolItem__ctor_m1575543045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_poolName_2(L_0);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_prefabName_3(L_1);
		__this->set_despawnOnStart_4((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.PreRuntimePoolItem::Start()
extern "C"  void PreRuntimePoolItem_Start_m823330565 (PreRuntimePoolItem_t3584992064 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PreRuntimePoolItem_Start_m823330565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpawnPool_t2419717525 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PoolManager_t707327633_il2cpp_TypeInfo_var);
		SpawnPoolsDict_t1520791760 * L_0 = ((PoolManager_t707327633_StaticFields*)PoolManager_t707327633_il2cpp_TypeInfo_var->static_fields)->get_Pools_0();
		String_t* L_1 = __this->get_poolName_2();
		NullCheck(L_0);
		bool L_2 = SpawnPoolsDict_TryGetValue_m4237209346(L_0, L_1, (&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0035;
		}
	}
	{
		V_1 = _stringLiteral2858802052;
		String_t* L_3 = V_1;
		String_t* L_4 = Object_get_name_m2079638459(__this, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m1811873526(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0035:
	{
		SpawnPool_t2419717525 * L_7 = V_0;
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		String_t* L_9 = __this->get_prefabName_3();
		bool L_10 = __this->get_despawnOnStart_4();
		bool L_11 = __this->get_doNotReparent_5();
		NullCheck(L_7);
		SpawnPool_Add_m1019377620(L_7, L_8, L_9, L_10, (bool)((((int32_t)L_11) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::.ctor()
extern "C"  void SpawnPool__ctor_m783789446 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool__ctor_m783789446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_poolName_2(L_0);
		List_1_t3749511102 * L_1 = (List_1_t3749511102 *)il2cpp_codegen_object_new(List_1_t3749511102_il2cpp_TypeInfo_var);
		List_1__ctor_m2177220103(L_1, /*hidden argument*/List_1__ctor_m2177220103_MethodInfo_var);
		__this->set__perPrefabPoolOptions_8(L_1);
		Dictionary_2_t3417634846 * L_2 = (Dictionary_2_t3417634846 *)il2cpp_codegen_object_new(Dictionary_2_t3417634846_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m729743939(L_2, /*hidden argument*/Dictionary_2__ctor_m729743939_MethodInfo_var);
		__this->set_prefabsFoldOutStates_9(L_2);
		__this->set_maxParticleDespawnTime_10((300.0f));
		PrefabsDict_t1121107021 * L_3 = (PrefabsDict_t1121107021 *)il2cpp_codegen_object_new(PrefabsDict_t1121107021_il2cpp_TypeInfo_var);
		PrefabsDict__ctor_m4168763172(L_3, /*hidden argument*/NULL);
		__this->set_prefabs_12(L_3);
		Dictionary_2_t3417634846 * L_4 = (Dictionary_2_t3417634846 *)il2cpp_codegen_object_new(Dictionary_2_t3417634846_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m729743939(L_4, /*hidden argument*/Dictionary_2__ctor_m729743939_MethodInfo_var);
		__this->set__editorListItemStates_13(L_4);
		List_1_t3749511102 * L_5 = (List_1_t3749511102 *)il2cpp_codegen_object_new(List_1_t3749511102_il2cpp_TypeInfo_var);
		List_1__ctor_m2177220103(L_5, /*hidden argument*/List_1__ctor_m2177220103_MethodInfo_var);
		__this->set__prefabPools_14(L_5);
		List_1_t2644239190 * L_6 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2292054616(L_6, /*hidden argument*/List_1__ctor_m2292054616_MethodInfo_var);
		__this->set__spawned_15(L_6);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool::get_dontDestroyOnLoad()
extern "C"  bool SpawnPool_get_dontDestroyOnLoad_m2135567053 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__dontDestroyOnLoad_6();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool::set_dontDestroyOnLoad(System.Boolean)
extern "C"  void SpawnPool_set_dontDestroyOnLoad_m624769966 (SpawnPool_t2419717525 * __this, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_set_dontDestroyOnLoad_m624769966_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		__this->set__dontDestroyOnLoad_6(L_0);
		Transform_t3275118058 * L_1 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Transform_t3275118058 * L_3 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::get_group()
extern "C"  Transform_t3275118058 * SpawnPool_get_group_m224041552 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_U3CgroupU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool::set_group(UnityEngine.Transform)
extern "C"  void SpawnPool_set_group_m2060151509 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___value0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___value0;
		__this->set_U3CgroupU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,PathologicalGames.PrefabPool> PathologicalGames.SpawnPool::get_prefabPools()
extern "C"  Dictionary_2_t2000201936 * SpawnPool_get_prefabPools_m3784917882 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_get_prefabPools_m3784917882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t2000201936 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Dictionary_2_t2000201936 * L_0 = (Dictionary_2_t2000201936 *)il2cpp_codegen_object_new(Dictionary_2_t2000201936_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m723632974(L_0, /*hidden argument*/Dictionary_2__ctor_m723632974_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0039;
	}

IL_000d:
	{
		Dictionary_2_t2000201936 * L_1 = V_0;
		List_1_t3749511102 * L_2 = __this->get__prefabPools_14();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		PrefabPool_t85422674 * L_4 = List_1_get_Item_m3756596590(L_2, L_3, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_prefabGO_1();
		NullCheck(L_5);
		String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
		List_1_t3749511102 * L_7 = __this->get__prefabPools_14();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		PrefabPool_t85422674 * L_9 = List_1_get_Item_m3756596590(L_7, L_8, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_1);
		Dictionary_2_set_Item_m3140732121(L_1, L_6, L_9, /*hidden argument*/Dictionary_2_set_Item_m3140732121_MethodInfo_var);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_1;
		List_1_t3749511102 * L_12 = __this->get__prefabPools_14();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m954114191(L_12, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_000d;
		}
	}
	{
		Dictionary_2_t2000201936 * L_14 = V_0;
		return L_14;
	}
}
// System.Void PathologicalGames.SpawnPool::Awake()
extern "C"  void SpawnPool_Awake_m3559340717 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Awake_m3559340717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get__dontDestroyOnLoad_6();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		SpawnPool_set_group_m2060151509(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0072;
		}
	}
	{
		Transform_t3275118058 * L_6 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m2079638459(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_7);
		String_t* L_9 = String_Replace_m1941156251(L_7, _stringLiteral204393016, L_8, /*hidden argument*/NULL);
		__this->set_poolName_2(L_9);
		String_t* L_10 = __this->get_poolName_2();
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_10);
		String_t* L_12 = String_Replace_m1941156251(L_10, _stringLiteral2559083266, L_11, /*hidden argument*/NULL);
		__this->set_poolName_2(L_12);
	}

IL_0072:
	{
		bool L_13 = __this->get_logMessages_7();
		if (!L_13)
		{
			goto IL_0092;
		}
	}
	{
		String_t* L_14 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2477534294, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_0092:
	{
		V_0 = 0;
		goto IL_00f6;
	}

IL_0099:
	{
		List_1_t3749511102 * L_16 = __this->get__perPrefabPoolOptions_8();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		PrefabPool_t85422674 * L_18 = List_1_get_Item_m3756596590(L_16, L_17, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = L_18->get_prefab_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_20 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_19, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00cf;
		}
	}
	{
		String_t* L_21 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral911846749, L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00cf:
	{
		List_1_t3749511102 * L_23 = __this->get__perPrefabPoolOptions_8();
		int32_t L_24 = V_0;
		NullCheck(L_23);
		PrefabPool_t85422674 * L_25 = List_1_get_Item_m3756596590(L_23, L_24, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_25);
		PrefabPool_inspectorInstanceConstructor_m824700707(L_25, /*hidden argument*/NULL);
		List_1_t3749511102 * L_26 = __this->get__perPrefabPoolOptions_8();
		int32_t L_27 = V_0;
		NullCheck(L_26);
		PrefabPool_t85422674 * L_28 = List_1_get_Item_m3756596590(L_26, L_27, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		SpawnPool_CreatePrefabPool_m932413996(__this, L_28, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00f6:
	{
		int32_t L_30 = V_0;
		List_1_t3749511102 * L_31 = __this->get__perPrefabPoolOptions_8();
		NullCheck(L_31);
		int32_t L_32 = List_1_get_Count_m954114191(L_31, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_30) < ((int32_t)L_32)))
		{
			goto IL_0099;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PoolManager_t707327633_il2cpp_TypeInfo_var);
		SpawnPoolsDict_t1520791760 * L_33 = ((PoolManager_t707327633_StaticFields*)PoolManager_t707327633_il2cpp_TypeInfo_var->static_fields)->get_Pools_0();
		NullCheck(L_33);
		SpawnPoolsDict_Add_m2638912137(L_33, __this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject PathologicalGames.SpawnPool::InstantiatePrefab(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * SpawnPool_InstantiatePrefab_m3003297369 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	{
		InstantiateDelegate_t3700441677 * L_0 = __this->get_instantiateDelegates_16();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		InstantiateDelegate_t3700441677 * L_1 = __this->get_instantiateDelegates_16();
		GameObject_t1756533147 * L_2 = ___prefab0;
		Vector3_t2243707580  L_3 = ___pos1;
		Quaternion_t4030073918  L_4 = ___rot2;
		NullCheck(L_1);
		GameObject_t1756533147 * L_5 = InstantiateDelegate_Invoke_m2830317127(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_001a:
	{
		GameObject_t1756533147 * L_6 = ___prefab0;
		Vector3_t2243707580  L_7 = ___pos1;
		Quaternion_t4030073918  L_8 = ___rot2;
		GameObject_t1756533147 * L_9 = InstanceHandler_InstantiatePrefab_m309003507(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void PathologicalGames.SpawnPool::DestroyInstance(UnityEngine.GameObject)
extern "C"  void SpawnPool_DestroyInstance_m2432744115 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method)
{
	{
		DestroyDelegate_t3092628401 * L_0 = __this->get_destroyDelegates_17();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		DestroyDelegate_t3092628401 * L_1 = __this->get_destroyDelegates_17();
		GameObject_t1756533147 * L_2 = ___instance0;
		NullCheck(L_1);
		DestroyDelegate_Invoke_m1938022676(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_001c:
	{
		GameObject_t1756533147 * L_3 = ___instance0;
		InstanceHandler_DestroyInstance_m331796129(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::OnDestroy()
extern "C"  void SpawnPool_OnDestroy_m331578691 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_OnDestroy_m331578691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PrefabPool_t85422674 * V_0 = NULL;
	Enumerator_t3284240776  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_logMessages_7();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		String_t* L_1 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1791777211, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PoolManager_t707327633_il2cpp_TypeInfo_var);
		SpawnPoolsDict_t1520791760 * L_3 = ((PoolManager_t707327633_StaticFields*)PoolManager_t707327633_il2cpp_TypeInfo_var->static_fields)->get_Pools_0();
		NullCheck(L_3);
		bool L_4 = SpawnPoolsDict_ContainsValue_m3660170656(L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PoolManager_t707327633_il2cpp_TypeInfo_var);
		SpawnPoolsDict_t1520791760 * L_5 = ((PoolManager_t707327633_StaticFields*)PoolManager_t707327633_il2cpp_TypeInfo_var->static_fields)->get_Pools_0();
		NullCheck(L_5);
		SpawnPoolsDict_Remove_m3353331162(L_5, __this, /*hidden argument*/NULL);
	}

IL_003c:
	{
		MonoBehaviour_StopAllCoroutines_m1675795839(__this, /*hidden argument*/NULL);
		List_1_t2644239190 * L_6 = __this->get__spawned_15();
		NullCheck(L_6);
		List_1_Clear_m1324927225(L_6, /*hidden argument*/List_1_Clear_m1324927225_MethodInfo_var);
		List_1_t3749511102 * L_7 = __this->get__prefabPools_14();
		NullCheck(L_7);
		Enumerator_t3284240776  L_8 = List_1_GetEnumerator_m3835178332(L_7, /*hidden argument*/List_1_GetEnumerator_m3835178332_MethodInfo_var);
		V_1 = L_8;
	}

IL_0059:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_005e:
		{
			PrefabPool_t85422674 * L_9 = Enumerator_get_Current_m742159042((&V_1), /*hidden argument*/Enumerator_get_Current_m742159042_MethodInfo_var);
			V_0 = L_9;
			PrefabPool_t85422674 * L_10 = V_0;
			NullCheck(L_10);
			PrefabPool_SelfDestruct_m1395427007(L_10, /*hidden argument*/NULL);
		}

IL_006c:
		{
			bool L_11 = Enumerator_MoveNext_m4077480560((&V_1), /*hidden argument*/Enumerator_MoveNext_m4077480560_MethodInfo_var);
			if (L_11)
			{
				goto IL_005e;
			}
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x8B, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4282189368((&V_1), /*hidden argument*/Enumerator_Dispose_m4282189368_MethodInfo_var);
		IL2CPP_END_FINALLY(125)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x8B, IL_008b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_008b:
	{
		List_1_t3749511102 * L_12 = __this->get__prefabPools_14();
		NullCheck(L_12);
		List_1_Clear_m676629708(L_12, /*hidden argument*/List_1_Clear_m676629708_MethodInfo_var);
		PrefabsDict_t1121107021 * L_13 = __this->get_prefabs_12();
		NullCheck(L_13);
		PrefabsDict__Clear_m3612909952(L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::CreatePrefabPool(PathologicalGames.PrefabPool)
extern "C"  void SpawnPool_CreatePrefabPool_m932413996 (SpawnPool_t2419717525 * __this, PrefabPool_t85422674 * ___prefabPool0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_CreatePrefabPool_m932413996_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		PrefabPool_t85422674 * L_0 = ___prefabPool0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = L_0->get_prefab_0();
		PrefabPool_t85422674 * L_2 = SpawnPool_GetPrefabPool_m2162742(__this, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		V_0 = (bool)G_B3_0;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		PrefabPool_t85422674 * L_4 = ___prefabPool0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = L_4->get_prefab_0();
		String_t* L_6 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1021754050, L_5, L_6, /*hidden argument*/NULL);
		Exception_t1927440687 * L_8 = (Exception_t1927440687 *)il2cpp_codegen_object_new(Exception_t1927440687_il2cpp_TypeInfo_var);
		Exception__ctor_m485833136(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_003b:
	{
		PrefabPool_t85422674 * L_9 = ___prefabPool0;
		NullCheck(L_9);
		L_9->set_spawnPool_15(__this);
		List_1_t3749511102 * L_10 = __this->get__prefabPools_14();
		PrefabPool_t85422674 * L_11 = ___prefabPool0;
		NullCheck(L_10);
		List_1_Add_m3209201747(L_10, L_11, /*hidden argument*/List_1_Add_m3209201747_MethodInfo_var);
		PrefabsDict_t1121107021 * L_12 = __this->get_prefabs_12();
		PrefabPool_t85422674 * L_13 = ___prefabPool0;
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = L_13->get_prefab_0();
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_16 = ___prefabPool0;
		NullCheck(L_16);
		Transform_t3275118058 * L_17 = L_16->get_prefab_0();
		NullCheck(L_12);
		PrefabsDict__Add_m2864700459(L_12, L_15, L_17, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_18 = ___prefabPool0;
		NullCheck(L_18);
		bool L_19 = PrefabPool_get_preloaded_m270443884(L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00b1;
		}
	}
	{
		bool L_20 = __this->get_logMessages_7();
		if (!L_20)
		{
			goto IL_00ab;
		}
	}
	{
		String_t* L_21 = __this->get_poolName_2();
		PrefabPool_t85422674 * L_22 = ___prefabPool0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_preloadAmount_2();
		int32_t L_24 = L_23;
		Il2CppObject * L_25 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_24);
		PrefabPool_t85422674 * L_26 = ___prefabPool0;
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = L_26->get_prefab_0();
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m2079638459(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m4262916296(NULL /*static, unused*/, _stringLiteral4052841889, L_21, L_25, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00ab:
	{
		PrefabPool_t85422674 * L_30 = ___prefabPool0;
		NullCheck(L_30);
		PrefabPool_PreloadInstances_m3084730458(L_30, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::Add(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
extern "C"  void SpawnPool_Add_m1019377620 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, String_t* ___prefabName1, bool ___despawn2, bool ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Add_m1019377620_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		goto IL_00c0;
	}

IL_0007:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_prefabGO_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1648406572, /*hidden argument*/NULL);
		return;
	}

IL_002e:
	{
		List_1_t3749511102 * L_5 = __this->get__prefabPools_14();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		PrefabPool_t85422674 * L_7 = List_1_get_Item_m3756596590(L_5, L_6, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_prefabGO_1();
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m2079638459(L_8, /*hidden argument*/NULL);
		String_t* L_10 = ___prefabName1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00bc;
		}
	}
	{
		List_1_t3749511102 * L_12 = __this->get__prefabPools_14();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		PrefabPool_t85422674 * L_14 = List_1_get_Item_m3756596590(L_12, L_13, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		Transform_t3275118058 * L_15 = ___instance0;
		bool L_16 = ___despawn2;
		NullCheck(L_14);
		PrefabPool_AddUnpooled_m2140288438(L_14, L_15, L_16, /*hidden argument*/NULL);
		bool L_17 = __this->get_logMessages_7();
		if (!L_17)
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_18 = __this->get_poolName_2();
		Transform_t3275118058 * L_19 = ___instance0;
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_21 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3949685996, L_18, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
	}

IL_0088:
	{
		bool L_22 = ___parent3;
		if (!L_22)
		{
			goto IL_00a9;
		}
	}
	{
		Transform_t3275118058 * L_23 = ___instance0;
		V_1 = (bool)((((int32_t)((!(((Il2CppObject*)(RectTransform_t3349966182 *)((RectTransform_t3349966182 *)IsInstSealed(L_23, RectTransform_t3349966182_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Transform_t3275118058 * L_24 = ___instance0;
		Transform_t3275118058 * L_25 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		bool L_26 = V_1;
		NullCheck(L_24);
		Transform_SetParent_m1963830867(L_24, L_25, L_26, /*hidden argument*/NULL);
	}

IL_00a9:
	{
		bool L_27 = ___despawn2;
		if (L_27)
		{
			goto IL_00bb;
		}
	}
	{
		List_1_t2644239190 * L_28 = __this->get__spawned_15();
		Transform_t3275118058 * L_29 = ___instance0;
		NullCheck(L_28);
		List_1_Add_m3606265932(L_28, L_29, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
	}

IL_00bb:
	{
		return;
	}

IL_00bc:
	{
		int32_t L_30 = V_0;
		V_0 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_31 = V_0;
		List_1_t3749511102 * L_32 = __this->get__prefabPools_14();
		NullCheck(L_32);
		int32_t L_33 = List_1_get_Count_m954114191(L_32, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_31) < ((int32_t)L_33)))
		{
			goto IL_0007;
		}
	}
	{
		String_t* L_34 = __this->get_poolName_2();
		String_t* L_35 = ___prefabName1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral1166636355, L_34, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::Add(UnityEngine.Transform)
extern "C"  void SpawnPool_Add_m2283877882 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Add_m2283877882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2671840987;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void PathologicalGames.SpawnPool::Remove(UnityEngine.Transform)
extern "C"  void SpawnPool_Remove_m1060805601 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Remove_m1060805601_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2075577691;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m2610273213 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Spawn_m2610273213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	PrefabPool_t85422674 * V_3 = NULL;
	{
		V_2 = 0;
		goto IL_00c4;
	}

IL_0007:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_2;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_prefabGO_1();
		Transform_t3275118058 * L_4 = ___prefab0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00c0;
		}
	}
	{
		List_1_t3749511102 * L_7 = __this->get__prefabPools_14();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		PrefabPool_t85422674 * L_9 = List_1_get_Item_m3756596590(L_7, L_8, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		Vector3_t2243707580  L_10 = ___pos1;
		Quaternion_t4030073918  L_11 = ___rot2;
		NullCheck(L_9);
		Transform_t3275118058 * L_12 = PrefabPool_SpawnInstance_m3748491065(L_9, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Transform_t3275118058 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_13, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_004a;
		}
	}
	{
		return (Transform_t3275118058 *)NULL;
	}

IL_004a:
	{
		Transform_t3275118058 * L_15 = V_0;
		V_1 = (bool)((((int32_t)((!(((Il2CppObject*)(RectTransform_t3349966182 *)((RectTransform_t3349966182 *)IsInstSealed(L_15, RectTransform_t3349966182_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Transform_t3275118058 * L_16 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_16, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0072;
		}
	}
	{
		Transform_t3275118058 * L_18 = V_0;
		Transform_t3275118058 * L_19 = ___parent3;
		bool L_20 = V_1;
		NullCheck(L_18);
		Transform_SetParent_m1963830867(L_18, L_19, L_20, /*hidden argument*/NULL);
		goto IL_00a0;
	}

IL_0072:
	{
		bool L_21 = __this->get_dontReparent_5();
		if (L_21)
		{
			goto IL_00a0;
		}
	}
	{
		Transform_t3275118058 * L_22 = V_0;
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = Transform_get_parent_m147407266(L_22, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00a0;
		}
	}
	{
		Transform_t3275118058 * L_26 = V_0;
		Transform_t3275118058 * L_27 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		bool L_28 = V_1;
		NullCheck(L_26);
		Transform_SetParent_m1963830867(L_26, L_27, L_28, /*hidden argument*/NULL);
	}

IL_00a0:
	{
		List_1_t2644239190 * L_29 = __this->get__spawned_15();
		Transform_t3275118058 * L_30 = V_0;
		NullCheck(L_29);
		List_1_Add_m3606265932(L_29, L_30, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		Transform_t3275118058 * L_31 = V_0;
		NullCheck(L_31);
		GameObject_t1756533147 * L_32 = Component_get_gameObject_m3105766835(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		GameObject_BroadcastMessage_m3472370570(L_32, _stringLiteral1094175583, __this, 1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_33 = V_0;
		return L_33;
	}

IL_00c0:
	{
		int32_t L_34 = V_2;
		V_2 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c4:
	{
		int32_t L_35 = V_2;
		List_1_t3749511102 * L_36 = __this->get__prefabPools_14();
		NullCheck(L_36);
		int32_t L_37 = List_1_get_Count_m954114191(L_36, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_35) < ((int32_t)L_37)))
		{
			goto IL_0007;
		}
	}
	{
		Transform_t3275118058 * L_38 = ___prefab0;
		PrefabPool_t85422674 * L_39 = (PrefabPool_t85422674 *)il2cpp_codegen_object_new(PrefabPool_t85422674_il2cpp_TypeInfo_var);
		PrefabPool__ctor_m2549027878(L_39, L_38, /*hidden argument*/NULL);
		V_3 = L_39;
		PrefabPool_t85422674 * L_40 = V_3;
		SpawnPool_CreatePrefabPool_m932413996(__this, L_40, /*hidden argument*/NULL);
		PrefabPool_t85422674 * L_41 = V_3;
		Vector3_t2243707580  L_42 = ___pos1;
		Quaternion_t4030073918  L_43 = ___rot2;
		NullCheck(L_41);
		Transform_t3275118058 * L_44 = PrefabPool_SpawnInstance_m3748491065(L_41, L_42, L_43, /*hidden argument*/NULL);
		V_0 = L_44;
		Transform_t3275118058 * L_45 = V_0;
		V_1 = (bool)((((int32_t)((!(((Il2CppObject*)(RectTransform_t3349966182 *)((RectTransform_t3349966182 *)IsInstSealed(L_45, RectTransform_t3349966182_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Transform_t3275118058 * L_46 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_46, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0114;
		}
	}
	{
		Transform_t3275118058 * L_48 = V_0;
		Transform_t3275118058 * L_49 = ___parent3;
		bool L_50 = V_1;
		NullCheck(L_48);
		Transform_SetParent_m1963830867(L_48, L_49, L_50, /*hidden argument*/NULL);
		goto IL_0142;
	}

IL_0114:
	{
		bool L_51 = __this->get_dontReparent_5();
		if (L_51)
		{
			goto IL_0142;
		}
	}
	{
		Transform_t3275118058 * L_52 = V_0;
		NullCheck(L_52);
		Transform_t3275118058 * L_53 = Transform_get_parent_m147407266(L_52, /*hidden argument*/NULL);
		Transform_t3275118058 * L_54 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_55 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_0142;
		}
	}
	{
		Transform_t3275118058 * L_56 = V_0;
		Transform_t3275118058 * L_57 = SpawnPool_get_group_m224041552(__this, /*hidden argument*/NULL);
		bool L_58 = V_1;
		NullCheck(L_56);
		Transform_SetParent_m1963830867(L_56, L_57, L_58, /*hidden argument*/NULL);
	}

IL_0142:
	{
		List_1_t2644239190 * L_59 = __this->get__spawned_15();
		Transform_t3275118058 * L_60 = V_0;
		NullCheck(L_59);
		List_1_Add_m3606265932(L_59, L_60, /*hidden argument*/List_1_Add_m3606265932_MethodInfo_var);
		Transform_t3275118058 * L_61 = V_0;
		NullCheck(L_61);
		GameObject_t1756533147 * L_62 = Component_get_gameObject_m3105766835(L_61, /*hidden argument*/NULL);
		NullCheck(L_62);
		GameObject_BroadcastMessage_m3472370570(L_62, _stringLiteral1094175583, __this, 1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_63 = V_0;
		return L_63;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m2615344182 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Spawn_m2615344182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = ___pos1;
		Quaternion_t4030073918  L_2 = ___rot2;
		Transform_t3275118058 * L_3 = SpawnPool_Spawn_m2610273213(__this, L_0, L_1, L_2, (Transform_t3275118058 *)NULL, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t3275118058 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0019;
		}
	}
	{
		return (Transform_t3275118058 *)NULL;
	}

IL_0019:
	{
		Transform_t3275118058 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3091291392 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = SpawnPool_Spawn_m2615344182(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3993902991 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = ___parent1;
		Transform_t3275118058 * L_4 = SpawnPool_Spawn_m2610273213(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m1365222558 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___prefab0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___pos1;
		Quaternion_t4030073918  L_3 = ___rot2;
		Transform_t3275118058 * L_4 = ___parent3;
		Transform_t3275118058 * L_5 = SpawnPool_Spawn_m2610273213(__this, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m2674181413 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___prefab0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___pos1;
		Quaternion_t4030073918  L_3 = ___rot2;
		Transform_t3275118058 * L_4 = SpawnPool_Spawn_m2615344182(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.GameObject)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3907117647 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___prefab0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = SpawnPool_Spawn_m3091291392(__this, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(UnityEngine.GameObject,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3574744766 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___prefab0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m909382139(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = ___parent1;
		Transform_t3275118058 * L_3 = SpawnPool_Spawn_m3993902991(__this, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(System.String)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3629503513 (SpawnPool_t2419717525 * __this, String_t* ___prefabName0, const MethodInfo* method)
{
	Transform_t3275118058 * V_0 = NULL;
	{
		PrefabsDict_t1121107021 * L_0 = __this->get_prefabs_12();
		String_t* L_1 = ___prefabName0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = PrefabsDict_get_Item_m254611088(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = V_0;
		Transform_t3275118058 * L_4 = SpawnPool_Spawn_m3091291392(__this, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(System.String,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m4045201012 (SpawnPool_t2419717525 * __this, String_t* ___prefabName0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	Transform_t3275118058 * V_0 = NULL;
	{
		PrefabsDict_t1121107021 * L_0 = __this->get_prefabs_12();
		String_t* L_1 = ___prefabName0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = PrefabsDict_get_Item_m254611088(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = V_0;
		Transform_t3275118058 * L_4 = ___parent1;
		Transform_t3275118058 * L_5 = SpawnPool_Spawn_m3993902991(__this, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(System.String,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m719970411 (SpawnPool_t2419717525 * __this, String_t* ___prefabName0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	Transform_t3275118058 * V_0 = NULL;
	{
		PrefabsDict_t1121107021 * L_0 = __this->get_prefabs_12();
		String_t* L_1 = ___prefabName0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = PrefabsDict_get_Item_m254611088(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = V_0;
		Vector3_t2243707580  L_4 = ___pos1;
		Quaternion_t4030073918  L_5 = ___rot2;
		Transform_t3275118058 * L_6 = SpawnPool_Spawn_m2615344182(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::Spawn(System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_Spawn_m3254109804 (SpawnPool_t2419717525 * __this, String_t* ___prefabName0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	Transform_t3275118058 * V_0 = NULL;
	{
		PrefabsDict_t1121107021 * L_0 = __this->get_prefabs_12();
		String_t* L_1 = ___prefabName0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = PrefabsDict_get_Item_m254611088(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t3275118058 * L_3 = V_0;
		Vector3_t2243707580  L_4 = ___pos1;
		Quaternion_t4030073918  L_5 = ___rot2;
		Transform_t3275118058 * L_6 = ___parent3;
		Transform_t3275118058 * L_7 = SpawnPool_Spawn_m2610273213(__this, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.AudioSource PathologicalGames.SpawnPool::Spawn(UnityEngine.AudioSource,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  AudioSource_t1135106623 * SpawnPool_Spawn_m602140196 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = ___pos1;
		Quaternion_t4030073918  L_2 = ___rot2;
		AudioSource_t1135106623 * L_3 = SpawnPool_Spawn_m3921451213(__this, L_0, L_1, L_2, (Transform_t3275118058 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.AudioSource PathologicalGames.SpawnPool::Spawn(UnityEngine.AudioSource)
extern "C"  AudioSource_t1135106623 * SpawnPool_Spawn_m3863042602 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___prefab0, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = SpawnPool_Spawn_m3921451213(__this, L_0, L_1, L_2, (Transform_t3275118058 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.AudioSource PathologicalGames.SpawnPool::Spawn(UnityEngine.AudioSource,UnityEngine.Transform)
extern "C"  AudioSource_t1135106623 * SpawnPool_Spawn_m1349189195 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___prefab0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_2 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = ___parent1;
		AudioSource_t1135106623 * L_4 = SpawnPool_Spawn_m3921451213(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.AudioSource PathologicalGames.SpawnPool::Spawn(UnityEngine.AudioSource,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  AudioSource_t1135106623 * SpawnPool_Spawn_m3921451213 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Spawn_m3921451213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	AudioSource_t1135106623 * V_1 = NULL;
	{
		AudioSource_t1135106623 * L_0 = ___prefab0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___pos1;
		Quaternion_t4030073918  L_3 = ___rot2;
		Transform_t3275118058 * L_4 = ___parent3;
		Transform_t3275118058 * L_5 = SpawnPool_Spawn_m2610273213(__this, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t3275118058 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_001f;
		}
	}
	{
		return (AudioSource_t1135106623 *)NULL;
	}

IL_001f:
	{
		Transform_t3275118058 * L_8 = V_0;
		NullCheck(L_8);
		AudioSource_t1135106623 * L_9 = Component_GetComponent_TisAudioSource_t1135106623_m2637777121(L_8, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m2637777121_MethodInfo_var);
		V_1 = L_9;
		AudioSource_t1135106623 * L_10 = V_1;
		NullCheck(L_10);
		AudioSource_Play_m353744792(L_10, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_11 = V_1;
		Il2CppObject * L_12 = SpawnPool_ListForAudioStop_m4210871781(__this, L_11, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_12, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_13 = V_1;
		return L_13;
	}
}
// UnityEngine.ParticleSystem PathologicalGames.SpawnPool::Spawn(UnityEngine.ParticleSystem,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  ParticleSystem_t3394631041 * SpawnPool_Spawn_m2156511436 (SpawnPool_t2419717525 * __this, ParticleSystem_t3394631041 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___prefab0;
		Vector3_t2243707580  L_1 = ___pos1;
		Quaternion_t4030073918  L_2 = ___rot2;
		ParticleSystem_t3394631041 * L_3 = SpawnPool_Spawn_m2956151321(__this, L_0, L_1, L_2, (Transform_t3275118058 *)NULL, /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.ParticleSystem PathologicalGames.SpawnPool::Spawn(UnityEngine.ParticleSystem,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  ParticleSystem_t3394631041 * SpawnPool_Spawn_m2956151321 (SpawnPool_t2419717525 * __this, ParticleSystem_t3394631041 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Spawn_m2956151321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	ParticleSystem_t3394631041 * V_1 = NULL;
	{
		ParticleSystem_t3394631041 * L_0 = ___prefab0;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = ___pos1;
		Quaternion_t4030073918  L_3 = ___rot2;
		Transform_t3275118058 * L_4 = ___parent3;
		Transform_t3275118058 * L_5 = SpawnPool_Spawn_m2610273213(__this, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t3275118058 * L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_001f;
		}
	}
	{
		return (ParticleSystem_t3394631041 *)NULL;
	}

IL_001f:
	{
		Transform_t3275118058 * L_8 = V_0;
		NullCheck(L_8);
		ParticleSystem_t3394631041 * L_9 = Component_GetComponent_TisParticleSystem_t3394631041_m1847115563(L_8, /*hidden argument*/Component_GetComponent_TisParticleSystem_t3394631041_m1847115563_MethodInfo_var);
		V_1 = L_9;
		ParticleSystem_t3394631041 * L_10 = V_1;
		Il2CppObject * L_11 = SpawnPool_ListenForEmitDespawn_m3364909241(__this, L_10, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_11, /*hidden argument*/NULL);
		ParticleSystem_t3394631041 * L_12 = V_1;
		return L_12;
	}
}
// System.Void PathologicalGames.SpawnPool::Despawn(UnityEngine.Transform)
extern "C"  void SpawnPool_Despawn_m501567261 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Despawn_m501567261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		V_0 = (bool)0;
		V_1 = 0;
		goto IL_0079;
	}

IL_0009:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_1;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_2);
		List_1_t2644239190 * L_3 = L_2->get__spawned_17();
		Transform_t3275118058 * L_4 = ___instance0;
		NullCheck(L_3);
		bool L_5 = List_1_Contains_m2540959346(L_3, L_4, /*hidden argument*/List_1_Contains_m2540959346_MethodInfo_var);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		List_1_t3749511102 * L_6 = __this->get__prefabPools_14();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		PrefabPool_t85422674 * L_8 = List_1_get_Item_m3756596590(L_6, L_7, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		Transform_t3275118058 * L_9 = ___instance0;
		NullCheck(L_8);
		bool L_10 = PrefabPool_DespawnInstance_m1597075065(L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_008a;
	}

IL_003d:
	{
		List_1_t3749511102 * L_11 = __this->get__prefabPools_14();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		PrefabPool_t85422674 * L_13 = List_1_get_Item_m3756596590(L_11, L_12, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_13);
		List_1_t2644239190 * L_14 = L_13->get__despawned_18();
		Transform_t3275118058 * L_15 = ___instance0;
		NullCheck(L_14);
		bool L_16 = List_1_Contains_m2540959346(L_14, L_15, /*hidden argument*/List_1_Contains_m2540959346_MethodInfo_var);
		if (!L_16)
		{
			goto IL_0075;
		}
	}
	{
		String_t* L_17 = __this->get_poolName_2();
		Transform_t3275118058 * L_18 = ___instance0;
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m2079638459(L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2264326193, L_17, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}

IL_0075:
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_22 = V_1;
		List_1_t3749511102 * L_23 = __this->get__prefabPools_14();
		NullCheck(L_23);
		int32_t L_24 = List_1_get_Count_m954114191(L_23, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0009;
		}
	}

IL_008a:
	{
		bool L_25 = V_0;
		if (L_25)
		{
			goto IL_00ac;
		}
	}
	{
		String_t* L_26 = __this->get_poolName_2();
		Transform_t3275118058 * L_27 = ___instance0;
		NullCheck(L_27);
		String_t* L_28 = Object_get_name_m2079638459(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2897442291, L_26, L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}

IL_00ac:
	{
		List_1_t2644239190 * L_30 = __this->get__spawned_15();
		Transform_t3275118058 * L_31 = ___instance0;
		NullCheck(L_30);
		List_1_Remove_m2326856859(L_30, L_31, /*hidden argument*/List_1_Remove_m2326856859_MethodInfo_var);
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::Despawn(UnityEngine.Transform,UnityEngine.Transform)
extern "C"  void SpawnPool_Despawn_m2337310686 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, Transform_t3275118058 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Despawn_m2337310686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Transform_t3275118058 * L_0 = ___instance0;
		V_0 = (bool)((((int32_t)((!(((Il2CppObject*)(RectTransform_t3349966182 *)((RectTransform_t3349966182 *)IsInstSealed(L_0, RectTransform_t3349966182_il2cpp_TypeInfo_var))) <= ((Il2CppObject*)(Il2CppObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Transform_t3275118058 * L_1 = ___instance0;
		Transform_t3275118058 * L_2 = ___parent1;
		bool L_3 = V_0;
		NullCheck(L_1);
		Transform_SetParent_m1963830867(L_1, L_2, L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = ___instance0;
		SpawnPool_Despawn_m501567261(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::Despawn(UnityEngine.Transform,System.Single)
extern "C"  void SpawnPool_Despawn_m1171081306 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, float ___seconds1, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___instance0;
		float L_1 = ___seconds1;
		Il2CppObject * L_2 = SpawnPool_DoDespawnAfterSeconds_m2454494(__this, L_0, L_1, (bool)0, (Transform_t3275118058 *)NULL, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.SpawnPool::Despawn(UnityEngine.Transform,System.Single,UnityEngine.Transform)
extern "C"  void SpawnPool_Despawn_m2427167811 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, float ___seconds1, Transform_t3275118058 * ___parent2, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = ___instance0;
		float L_1 = ___seconds1;
		Transform_t3275118058 * L_2 = ___parent2;
		Il2CppObject * L_3 = SpawnPool_DoDespawnAfterSeconds_m2454494(__this, L_0, L_1, (bool)1, L_2, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator PathologicalGames.SpawnPool::DoDespawnAfterSeconds(UnityEngine.Transform,System.Single,System.Boolean,UnityEngine.Transform)
extern "C"  Il2CppObject * SpawnPool_DoDespawnAfterSeconds_m2454494 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, float ___seconds1, bool ___useParent2, Transform_t3275118058 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_DoDespawnAfterSeconds_m2454494_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * V_0 = NULL;
	{
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_0 = (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 *)il2cpp_codegen_object_new(U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550_il2cpp_TypeInfo_var);
		U3CDoDespawnAfterSecondsU3Ec__Iterator1__ctor_m208252327(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_1 = V_0;
		Transform_t3275118058 * L_2 = ___instance0;
		NullCheck(L_1);
		L_1->set_instance_0(L_2);
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_3 = V_0;
		float L_4 = ___seconds1;
		NullCheck(L_3);
		L_3->set_seconds_2(L_4);
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_5 = V_0;
		bool L_6 = ___useParent2;
		NullCheck(L_5);
		L_5->set_useParent_3(L_6);
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_7 = V_0;
		Transform_t3275118058 * L_8 = ___parent3;
		NullCheck(L_7);
		L_7->set_parent_4(L_8);
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_9 = V_0;
		NullCheck(L_9);
		L_9->set_U24this_5(__this);
		U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * L_10 = V_0;
		return L_10;
	}
}
// System.Void PathologicalGames.SpawnPool::DespawnAll()
extern "C"  void SpawnPool_DespawnAll_m114589995 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_DespawnAll_m114589995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2644239190 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t2644239190 * L_0 = __this->get__spawned_15();
		List_1_t2644239190 * L_1 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m2538210300(L_1, L_0, /*hidden argument*/List_1__ctor_m2538210300_MethodInfo_var);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0024;
	}

IL_0013:
	{
		List_1_t2644239190 * L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Transform_t3275118058 * L_4 = List_1_get_Item_m563292115(L_2, L_3, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		SpawnPool_Despawn_m501567261(__this, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_6 = V_1;
		List_1_t2644239190 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m936427142(L_7, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool::IsSpawned(UnityEngine.Transform)
extern "C"  bool SpawnPool_IsSpawned_m561723107 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_IsSpawned_m561723107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2644239190 * L_0 = __this->get__spawned_15();
		Transform_t3275118058 * L_1 = ___instance0;
		NullCheck(L_0);
		bool L_2 = List_1_Contains_m2540959346(L_0, L_1, /*hidden argument*/List_1_Contains_m2540959346_MethodInfo_var);
		return L_2;
	}
}
// PathologicalGames.PrefabPool PathologicalGames.SpawnPool::GetPrefabPool(UnityEngine.Transform)
extern "C"  PrefabPool_t85422674 * SpawnPool_GetPrefabPool_m2162742 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_GetPrefabPool_m2162742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_006a;
	}

IL_0007:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_prefabGO_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_5 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral885759378, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		List_1_t3749511102 * L_7 = __this->get__prefabPools_14();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		PrefabPool_t85422674 * L_9 = List_1_get_Item_m3756596590(L_7, L_8, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = L_9->get_prefabGO_1();
		Transform_t3275118058 * L_11 = ___prefab0;
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = Component_get_gameObject_m3105766835(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0066;
		}
	}
	{
		List_1_t3749511102 * L_14 = __this->get__prefabPools_14();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		PrefabPool_t85422674 * L_16 = List_1_get_Item_m3756596590(L_14, L_15, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		return L_16;
	}

IL_0066:
	{
		int32_t L_17 = V_0;
		V_0 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006a:
	{
		int32_t L_18 = V_0;
		List_1_t3749511102 * L_19 = __this->get__prefabPools_14();
		NullCheck(L_19);
		int32_t L_20 = List_1_get_Count_m954114191(L_19, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_0007;
		}
	}
	{
		return (PrefabPool_t85422674 *)NULL;
	}
}
// PathologicalGames.PrefabPool PathologicalGames.SpawnPool::GetPrefabPool(UnityEngine.GameObject)
extern "C"  PrefabPool_t85422674 * SpawnPool_GetPrefabPool_m1999940987 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_GetPrefabPool_m1999940987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0065;
	}

IL_0007:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_prefabGO_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		String_t* L_5 = __this->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral885759378, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0038:
	{
		List_1_t3749511102 * L_7 = __this->get__prefabPools_14();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		PrefabPool_t85422674 * L_9 = List_1_get_Item_m3756596590(L_7, L_8, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_9);
		GameObject_t1756533147 * L_10 = L_9->get_prefabGO_1();
		GameObject_t1756533147 * L_11 = ___prefab0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0061;
		}
	}
	{
		List_1_t3749511102 * L_13 = __this->get__prefabPools_14();
		int32_t L_14 = V_0;
		NullCheck(L_13);
		PrefabPool_t85422674 * L_15 = List_1_get_Item_m3756596590(L_13, L_14, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		return L_15;
	}

IL_0061:
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0065:
	{
		int32_t L_17 = V_0;
		List_1_t3749511102 * L_18 = __this->get__prefabPools_14();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m954114191(L_18, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0007;
		}
	}
	{
		return (PrefabPool_t85422674 *)NULL;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::GetPrefab(UnityEngine.Transform)
extern "C"  Transform_t3275118058 * SpawnPool_GetPrefab_m228367079 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_GetPrefab_m228367079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0034;
	}

IL_0007:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		Transform_t3275118058 * L_3 = ___instance0;
		NullCheck(L_2);
		bool L_4 = PrefabPool_Contains_m3543239565(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		List_1_t3749511102 * L_5 = __this->get__prefabPools_14();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		PrefabPool_t85422674 * L_7 = List_1_get_Item_m3756596590(L_5, L_6, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = L_7->get_prefab_0();
		return L_8;
	}

IL_0030:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_10 = V_0;
		List_1_t3749511102 * L_11 = __this->get__prefabPools_14();
		NullCheck(L_11);
		int32_t L_12 = List_1_get_Count_m954114191(L_11, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0007;
		}
	}
	{
		return (Transform_t3275118058 *)NULL;
	}
}
// UnityEngine.GameObject PathologicalGames.SpawnPool::GetPrefab(UnityEngine.GameObject)
extern "C"  GameObject_t1756533147 * SpawnPool_GetPrefab_m1836877887 (SpawnPool_t2419717525 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_GetPrefab_m1836877887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0007:
	{
		List_1_t3749511102 * L_0 = __this->get__prefabPools_14();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		PrefabPool_t85422674 * L_2 = List_1_get_Item_m3756596590(L_0, L_1, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		GameObject_t1756533147 * L_3 = ___instance0;
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = GameObject_get_transform_m909382139(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_5 = PrefabPool_Contains_m3543239565(L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		List_1_t3749511102 * L_6 = __this->get__prefabPools_14();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		PrefabPool_t85422674 * L_8 = List_1_get_Item_m3756596590(L_6, L_7, /*hidden argument*/List_1_get_Item_m3756596590_MethodInfo_var);
		NullCheck(L_8);
		GameObject_t1756533147 * L_9 = L_8->get_prefabGO_1();
		return L_9;
	}

IL_0035:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_11 = V_0;
		List_1_t3749511102 * L_12 = __this->get__prefabPools_14();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m954114191(L_12, /*hidden argument*/List_1_get_Count_m954114191_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0007;
		}
	}
	{
		return (GameObject_t1756533147 *)NULL;
	}
}
// System.Collections.IEnumerator PathologicalGames.SpawnPool::ListForAudioStop(UnityEngine.AudioSource)
extern "C"  Il2CppObject * SpawnPool_ListForAudioStop_m4210871781 (SpawnPool_t2419717525 * __this, AudioSource_t1135106623 * ___src0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_ListForAudioStop_m4210871781_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CListForAudioStopU3Ec__Iterator2_t1067573800 * V_0 = NULL;
	{
		U3CListForAudioStopU3Ec__Iterator2_t1067573800 * L_0 = (U3CListForAudioStopU3Ec__Iterator2_t1067573800 *)il2cpp_codegen_object_new(U3CListForAudioStopU3Ec__Iterator2_t1067573800_il2cpp_TypeInfo_var);
		U3CListForAudioStopU3Ec__Iterator2__ctor_m2241042125(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CListForAudioStopU3Ec__Iterator2_t1067573800 * L_1 = V_0;
		AudioSource_t1135106623 * L_2 = ___src0;
		NullCheck(L_1);
		L_1->set_src_0(L_2);
		U3CListForAudioStopU3Ec__Iterator2_t1067573800 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_2(__this);
		U3CListForAudioStopU3Ec__Iterator2_t1067573800 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator PathologicalGames.SpawnPool::ListenForEmitDespawn(UnityEngine.ParticleSystem)
extern "C"  Il2CppObject * SpawnPool_ListenForEmitDespawn_m3364909241 (SpawnPool_t2419717525 * __this, ParticleSystem_t3394631041 * ___emitter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_ListenForEmitDespawn_m3364909241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * V_0 = NULL;
	{
		U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * L_0 = (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 *)il2cpp_codegen_object_new(U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689_il2cpp_TypeInfo_var);
		U3CListenForEmitDespawnU3Ec__Iterator3__ctor_m3129400790(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * L_1 = V_0;
		ParticleSystem_t3394631041 * L_2 = ___emitter0;
		NullCheck(L_1);
		L_1->set_emitter_0(L_2);
		U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_3(__this);
		U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * L_4 = V_0;
		return L_4;
	}
}
// System.String PathologicalGames.SpawnPool::ToString()
extern "C"  String_t* SpawnPool_ToString_m3120155651 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_ToString_m3120155651_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1398341365 * V_0 = NULL;
	Transform_t3275118058 * V_1 = NULL;
	Enumerator_t2178968864  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		V_0 = L_0;
		List_1_t2644239190 * L_1 = __this->get__spawned_15();
		NullCheck(L_1);
		Enumerator_t2178968864  L_2 = List_1_GetEnumerator_m1527845545(L_1, /*hidden argument*/List_1_GetEnumerator_m1527845545_MethodInfo_var);
		V_2 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_0017:
		{
			Transform_t3275118058 * L_3 = Enumerator_get_Current_m2375004233((&V_2), /*hidden argument*/Enumerator_get_Current_m2375004233_MethodInfo_var);
			V_1 = L_3;
			List_1_t1398341365 * L_4 = V_0;
			Transform_t3275118058 * L_5 = V_1;
			NullCheck(L_5);
			String_t* L_6 = Object_get_name_m2079638459(L_5, /*hidden argument*/NULL);
			NullCheck(L_4);
			List_1_Add_m4061286785(L_4, L_6, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		}

IL_002b:
		{
			bool L_7 = Enumerator_MoveNext_m273259033((&V_2), /*hidden argument*/Enumerator_MoveNext_m273259033_MethodInfo_var);
			if (L_7)
			{
				goto IL_0017;
			}
		}

IL_0037:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_2), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004a:
	{
		List_1_t1398341365 * L_8 = V_0;
		NullCheck(L_8);
		StringU5BU5D_t1642385972* L_9 = List_1_ToArray_m948919263(L_8, /*hidden argument*/List_1_ToArray_m948919263_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral811305474, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool::get_Item(System.Int32)
extern "C"  Transform_t3275118058 * SpawnPool_get_Item_m2234631157 (SpawnPool_t2419717525 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_get_Item_m2234631157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2644239190 * L_0 = __this->get__spawned_15();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Transform_t3275118058 * L_2 = List_1_get_Item_m563292115(L_0, L_1, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		return L_2;
	}
}
// System.Void PathologicalGames.SpawnPool::set_Item(System.Int32,UnityEngine.Transform)
extern "C"  void SpawnPool_set_Item_m3752828756 (SpawnPool_t2419717525 * __this, int32_t ___index0, Transform_t3275118058 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_set_Item_m3752828756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral142138905, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PathologicalGames.SpawnPool::Contains(UnityEngine.Transform)
extern "C"  bool SpawnPool_Contains_m3051551352 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Contains_m3051551352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral1196577561;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void PathologicalGames.SpawnPool::CopyTo(UnityEngine.Transform[],System.Int32)
extern "C"  void SpawnPool_CopyTo_m2515519044 (SpawnPool_t2419717525 * __this, TransformU5BU5D_t3764228911* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_CopyTo_m2515519044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2644239190 * L_0 = __this->get__spawned_15();
		TransformU5BU5D_t3764228911* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck(L_0);
		List_1_CopyTo_m3358051392(L_0, L_1, L_2, /*hidden argument*/List_1_CopyTo_m3358051392_MethodInfo_var);
		return;
	}
}
// System.Int32 PathologicalGames.SpawnPool::get_Count()
extern "C"  int32_t SpawnPool_get_Count_m522886060 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_get_Count_m522886060_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2644239190 * L_0 = __this->get__spawned_15();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m936427142(L_0, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.Transform> PathologicalGames.SpawnPool::GetEnumerator()
extern "C"  Il2CppObject* SpawnPool_GetEnumerator_m3048857063 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_GetEnumerator_m3048857063_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * L_0 = (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 *)il2cpp_codegen_object_new(U3CGetEnumeratorU3Ec__Iterator4_t2230806469_il2cpp_TypeInfo_var);
		U3CGetEnumeratorU3Ec__Iterator4__ctor_m1958432518(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator PathologicalGames.SpawnPool::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SpawnPool_System_Collections_IEnumerable_GetEnumerator_m2186170833 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_System_Collections_IEnumerable_GetEnumerator_m2186170833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * V_0 = NULL;
	{
		U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * L_0 = (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 *)il2cpp_codegen_object_new(U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705_il2cpp_TypeInfo_var);
		U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0__ctor_m3790315952(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_1(__this);
		U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 PathologicalGames.SpawnPool::IndexOf(UnityEngine.Transform)
extern "C"  int32_t SpawnPool_IndexOf_m2455463034 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_IndexOf_m2455463034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool::Insert(System.Int32,UnityEngine.Transform)
extern "C"  void SpawnPool_Insert_m3265689621 (SpawnPool_t2419717525 * __this, int32_t ___index0, Transform_t3275118058 * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Insert_m3265689621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool::RemoveAt(System.Int32)
extern "C"  void SpawnPool_RemoveAt_m655898584 (SpawnPool_t2419717525 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_RemoveAt_m655898584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool::Clear()
extern "C"  void SpawnPool_Clear_m1056223761 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_Clear_m1056223761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PathologicalGames.SpawnPool::get_IsReadOnly()
extern "C"  bool SpawnPool_get_IsReadOnly_m1411436997 (SpawnPool_t2419717525 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_get_IsReadOnly_m1411436997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean PathologicalGames.SpawnPool::System.Collections.Generic.ICollection<UnityEngine.Transform>.Remove(UnityEngine.Transform)
extern "C"  bool SpawnPool_System_Collections_Generic_ICollectionU3CUnityEngine_TransformU3E_Remove_m497770936 (SpawnPool_t2419717525 * __this, Transform_t3275118058 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPool_System_Collections_Generic_ICollectionU3CUnityEngine_TransformU3E_Remove_m497770936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::.ctor()
extern "C"  void U3CDoDespawnAfterSecondsU3Ec__Iterator1__ctor_m208252327 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::MoveNext()
extern "C"  bool U3CDoDespawnAfterSecondsU3Ec__Iterator1_MoveNext_m3642642905 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0052;
		}
	}
	{
		goto IL_00c8;
	}

IL_0021:
	{
		Transform_t3275118058 * L_2 = __this->get_instance_0();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		__this->set_U3CgoU3E__0_1(L_3);
		goto IL_0079;
	}

IL_0037:
	{
		__this->set_U24current_6(NULL);
		bool L_4 = __this->get_U24disposing_7();
		if (L_4)
		{
			goto IL_004d;
		}
	}
	{
		__this->set_U24PC_8(1);
	}

IL_004d:
	{
		goto IL_00ca;
	}

IL_0052:
	{
		GameObject_t1756533147 * L_5 = __this->get_U3CgoU3E__0_1();
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeInHierarchy_m4242915935(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0067;
		}
	}
	{
		goto IL_00c8;
	}

IL_0067:
	{
		float L_7 = __this->get_seconds_2();
		float L_8 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_seconds_2(((float)((float)L_7-(float)L_8)));
	}

IL_0079:
	{
		float L_9 = __this->get_seconds_2();
		if ((((float)L_9) > ((float)(0.0f))))
		{
			goto IL_0037;
		}
	}
	{
		bool L_10 = __this->get_useParent_3();
		if (!L_10)
		{
			goto IL_00b0;
		}
	}
	{
		SpawnPool_t2419717525 * L_11 = __this->get_U24this_5();
		Transform_t3275118058 * L_12 = __this->get_instance_0();
		Transform_t3275118058 * L_13 = __this->get_parent_4();
		NullCheck(L_11);
		SpawnPool_Despawn_m2337310686(L_11, L_12, L_13, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_00b0:
	{
		SpawnPool_t2419717525 * L_14 = __this->get_U24this_5();
		Transform_t3275118058 * L_15 = __this->get_instance_0();
		NullCheck(L_14);
		SpawnPool_Despawn_m501567261(L_14, L_15, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		__this->set_U24PC_8((-1));
	}

IL_00c8:
	{
		return (bool)0;
	}

IL_00ca:
	{
		return (bool)1;
	}
}
// System.Object PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoDespawnAfterSecondsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2644588829 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoDespawnAfterSecondsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1157745077 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::Dispose()
extern "C"  void U3CDoDespawnAfterSecondsU3Ec__Iterator1_Dispose_m2230843758 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void PathologicalGames.SpawnPool/<DoDespawnAfterSeconds>c__Iterator1::Reset()
extern "C"  void U3CDoDespawnAfterSecondsU3Ec__Iterator1_Reset_m3479680684 (U3CDoDespawnAfterSecondsU3Ec__Iterator1_t4059494550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoDespawnAfterSecondsU3Ec__Iterator1_Reset_m3479680684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator4__ctor_m1958432518 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m635155626 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator4_MoveNext_m635155626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005d;
		}
	}
	{
		goto IL_008d;
	}

IL_0021:
	{
		__this->set_U3CiU3E__1_0(0);
		goto IL_006b;
	}

IL_002d:
	{
		SpawnPool_t2419717525 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		List_1_t2644239190 * L_3 = L_2->get__spawned_15();
		int32_t L_4 = __this->get_U3CiU3E__1_0();
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = List_1_get_Item_m563292115(L_3, L_4, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		__this->set_U24current_2(L_5);
		bool L_6 = __this->get_U24disposing_3();
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0058:
	{
		goto IL_008f;
	}

IL_005d:
	{
		int32_t L_7 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_006b:
	{
		int32_t L_8 = __this->get_U3CiU3E__1_0();
		SpawnPool_t2419717525 * L_9 = __this->get_U24this_1();
		NullCheck(L_9);
		List_1_t2644239190 * L_10 = L_9->get__spawned_15();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m936427142(L_10, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_11)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_008d:
	{
		return (bool)0;
	}

IL_008f:
	{
		return (bool)1;
	}
}
// UnityEngine.Transform PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::System.Collections.Generic.IEnumerator<UnityEngine.Transform>.get_Current()
extern "C"  Transform_t3275118058 * U3CGetEnumeratorU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CUnityEngine_TransformU3E_get_Current_m744811603 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2190249496 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator4_Dispose_m1325161577 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PathologicalGames.SpawnPool/<GetEnumerator>c__Iterator4::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator4_Reset_m1633375387 (U3CGetEnumeratorU3Ec__Iterator4_t2230806469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator4_Reset_m1633375387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::.ctor()
extern "C"  void U3CListenForEmitDespawnU3Ec__Iterator3__ctor_m3129400790 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::MoveNext()
extern "C"  bool U3CListenForEmitDespawnU3Ec__Iterator3_MoveNext_m3826550086 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CListenForEmitDespawnU3Ec__Iterator3_MoveNext_m3826550086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_6();
		V_0 = L_0;
		__this->set_U24PC_6((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
		if (L_1 == 2)
		{
			goto IL_00e3;
		}
	}
	{
		goto IL_013d;
	}

IL_0025:
	{
		ParticleSystem_t3394631041 * L_2 = __this->get_emitter_0();
		NullCheck(L_2);
		float L_3 = ParticleSystem_get_startDelay_m3593315006(L_2, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, ((float)((float)L_3+(float)(0.25f))), /*hidden argument*/NULL);
		__this->set_U24current_4(L_4);
		bool L_5 = __this->get_U24disposing_5();
		if (L_5)
		{
			goto IL_0050;
		}
	}
	{
		__this->set_U24PC_6(1);
	}

IL_0050:
	{
		goto IL_013f;
	}

IL_0055:
	{
		__this->set_U3CsafetimerU3E__0_1((0.0f));
		ParticleSystem_t3394631041 * L_6 = __this->get_emitter_0();
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		__this->set_U3CemitterGOU3E__0_2(L_7);
		goto IL_00e3;
	}

IL_0076:
	{
		float L_8 = __this->get_U3CsafetimerU3E__0_1();
		float L_9 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CsafetimerU3E__0_1(((float)((float)L_8+(float)L_9)));
		float L_10 = __this->get_U3CsafetimerU3E__0_1();
		SpawnPool_t2419717525 * L_11 = __this->get_U24this_3();
		NullCheck(L_11);
		float L_12 = L_11->get_maxParticleDespawnTime_10();
		if ((!(((float)L_10) > ((float)L_12))))
		{
			goto IL_00c8;
		}
	}
	{
		SpawnPool_t2419717525 * L_13 = __this->get_U24this_3();
		NullCheck(L_13);
		String_t* L_14 = L_13->get_poolName_2();
		SpawnPool_t2419717525 * L_15 = __this->get_U24this_3();
		NullCheck(L_15);
		float L_16 = L_15->get_maxParticleDespawnTime_10();
		float L_17 = L_16;
		Il2CppObject * L_18 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_17);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral3308707187, L_14, L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
	}

IL_00c8:
	{
		__this->set_U24current_4(NULL);
		bool L_20 = __this->get_U24disposing_5();
		if (L_20)
		{
			goto IL_00de;
		}
	}
	{
		__this->set_U24PC_6(2);
	}

IL_00de:
	{
		goto IL_013f;
	}

IL_00e3:
	{
		ParticleSystem_t3394631041 * L_21 = __this->get_emitter_0();
		NullCheck(L_21);
		bool L_22 = ParticleSystem_IsAlive_m2793794644(L_21, (bool)1, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0104;
		}
	}
	{
		GameObject_t1756533147 * L_23 = __this->get_U3CemitterGOU3E__0_2();
		NullCheck(L_23);
		bool L_24 = GameObject_get_activeInHierarchy_m4242915935(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_0076;
		}
	}

IL_0104:
	{
		GameObject_t1756533147 * L_25 = __this->get_U3CemitterGOU3E__0_2();
		NullCheck(L_25);
		bool L_26 = GameObject_get_activeInHierarchy_m4242915935(L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0136;
		}
	}
	{
		SpawnPool_t2419717525 * L_27 = __this->get_U24this_3();
		ParticleSystem_t3394631041 * L_28 = __this->get_emitter_0();
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = Component_get_transform_m2697483695(L_28, /*hidden argument*/NULL);
		NullCheck(L_27);
		SpawnPool_Despawn_m501567261(L_27, L_29, /*hidden argument*/NULL);
		ParticleSystem_t3394631041 * L_30 = __this->get_emitter_0();
		NullCheck(L_30);
		ParticleSystem_Clear_m4048064080(L_30, (bool)1, /*hidden argument*/NULL);
	}

IL_0136:
	{
		__this->set_U24PC_6((-1));
	}

IL_013d:
	{
		return (bool)0;
	}

IL_013f:
	{
		return (bool)1;
	}
}
// System.Object PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CListenForEmitDespawnU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1937722490 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CListenForEmitDespawnU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3231380738 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::Dispose()
extern "C"  void U3CListenForEmitDespawnU3Ec__Iterator3_Dispose_m347067937 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_5((bool)1);
		__this->set_U24PC_6((-1));
		return;
	}
}
// System.Void PathologicalGames.SpawnPool/<ListenForEmitDespawn>c__Iterator3::Reset()
extern "C"  void U3CListenForEmitDespawnU3Ec__Iterator3_Reset_m3962734311 (U3CListenForEmitDespawnU3Ec__Iterator3_t3614466689 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CListenForEmitDespawnU3Ec__Iterator3_Reset_m3962734311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::.ctor()
extern "C"  void U3CListForAudioStopU3Ec__Iterator2__ctor_m2241042125 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::MoveNext()
extern "C"  bool U3CListForAudioStopU3Ec__Iterator2_MoveNext_m2413295515 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_5();
		V_0 = L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0040;
		}
		if (L_1 == 2)
		{
			goto IL_0071;
		}
	}
	{
		goto IL_00be;
	}

IL_0025:
	{
		__this->set_U24current_3(NULL);
		bool L_2 = __this->get_U24disposing_4();
		if (L_2)
		{
			goto IL_003b;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_003b:
	{
		goto IL_00c0;
	}

IL_0040:
	{
		AudioSource_t1135106623 * L_3 = __this->get_src_0();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		__this->set_U3CsrcGameObjectU3E__0_1(L_4);
		goto IL_0071;
	}

IL_0056:
	{
		__this->set_U24current_3(NULL);
		bool L_5 = __this->get_U24disposing_4();
		if (L_5)
		{
			goto IL_006c;
		}
	}
	{
		__this->set_U24PC_5(2);
	}

IL_006c:
	{
		goto IL_00c0;
	}

IL_0071:
	{
		AudioSource_t1135106623 * L_6 = __this->get_src_0();
		NullCheck(L_6);
		bool L_7 = AudioSource_get_isPlaying_m3677592677(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0056;
		}
	}
	{
		GameObject_t1756533147 * L_8 = __this->get_U3CsrcGameObjectU3E__0_1();
		NullCheck(L_8);
		bool L_9 = GameObject_get_activeInHierarchy_m4242915935(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_00a1;
		}
	}
	{
		AudioSource_t1135106623 * L_10 = __this->get_src_0();
		NullCheck(L_10);
		AudioSource_Stop_m3452679614(L_10, /*hidden argument*/NULL);
		goto IL_00be;
	}

IL_00a1:
	{
		SpawnPool_t2419717525 * L_11 = __this->get_U24this_2();
		AudioSource_t1135106623 * L_12 = __this->get_src_0();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		SpawnPool_Despawn_m501567261(L_11, L_13, /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
}
// System.Object PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CListForAudioStopU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1846903803 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CListForAudioStopU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m2618174339 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::Dispose()
extern "C"  void U3CListForAudioStopU3Ec__Iterator2_Dispose_m1298046116 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void PathologicalGames.SpawnPool/<ListForAudioStop>c__Iterator2::Reset()
extern "C"  void U3CListForAudioStopU3Ec__Iterator2_Reset_m3922951534 (U3CListForAudioStopU3Ec__Iterator2_t1067573800 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CListForAudioStopU3Ec__Iterator2_Reset_m3922951534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::.ctor()
extern "C"  void U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0__ctor_m3790315952 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::MoveNext()
extern "C"  bool U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_MoveNext_m654851924 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_MoveNext_m654851924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_005d;
		}
	}
	{
		goto IL_008d;
	}

IL_0021:
	{
		__this->set_U3CiU3E__1_0(0);
		goto IL_006b;
	}

IL_002d:
	{
		SpawnPool_t2419717525 * L_2 = __this->get_U24this_1();
		NullCheck(L_2);
		List_1_t2644239190 * L_3 = L_2->get__spawned_15();
		int32_t L_4 = __this->get_U3CiU3E__1_0();
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = List_1_get_Item_m563292115(L_3, L_4, /*hidden argument*/List_1_get_Item_m563292115_MethodInfo_var);
		__this->set_U24current_2(L_5);
		bool L_6 = __this->get_U24disposing_3();
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0058:
	{
		goto IL_008f;
	}

IL_005d:
	{
		int32_t L_7 = __this->get_U3CiU3E__1_0();
		__this->set_U3CiU3E__1_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_006b:
	{
		int32_t L_8 = __this->get_U3CiU3E__1_0();
		SpawnPool_t2419717525 * L_9 = __this->get_U24this_1();
		NullCheck(L_9);
		List_1_t2644239190 * L_10 = L_9->get__spawned_15();
		NullCheck(L_10);
		int32_t L_11 = List_1_get_Count_m936427142(L_10, /*hidden argument*/List_1_get_Count_m936427142_MethodInfo_var);
		if ((((int32_t)L_8) < ((int32_t)L_11)))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_4((-1));
	}

IL_008d:
	{
		return (bool)0;
	}

IL_008f:
	{
		return (bool)1;
	}
}
// System.Object PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m187996952 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3836002208 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::Dispose()
extern "C"  void U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_Dispose_m1670470593 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void PathologicalGames.SpawnPool/<System_Collections_IEnumerable_GetEnumerator>c__Iterator0::Reset()
extern "C"  void U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_Reset_m290490135 (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_t2170770705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSystem_Collections_IEnumerable_GetEnumeratorU3Ec__Iterator0_Reset_m290490135_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PathologicalGames.SpawnPool/DestroyDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DestroyDelegate__ctor_m2665953088 (DestroyDelegate_t3092628401 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PathologicalGames.SpawnPool/DestroyDelegate::Invoke(UnityEngine.GameObject)
extern "C"  void DestroyDelegate_Invoke_m1938022676 (DestroyDelegate_t3092628401 * __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DestroyDelegate_Invoke_m1938022676((DestroyDelegate_t3092628401 *)__this->get_prev_9(),___instance0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, GameObject_t1756533147 * ___instance0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___instance0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PathologicalGames.SpawnPool/DestroyDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DestroyDelegate_BeginInvoke_m3881367365 (DestroyDelegate_t3092628401 * __this, GameObject_t1756533147 * ___instance0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___instance0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PathologicalGames.SpawnPool/DestroyDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DestroyDelegate_EndInvoke_m2579382982 (DestroyDelegate_t3092628401 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void PathologicalGames.SpawnPool/InstantiateDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void InstantiateDelegate__ctor_m617627450 (InstantiateDelegate_t3700441677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// UnityEngine.GameObject PathologicalGames.SpawnPool/InstantiateDelegate::Invoke(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  GameObject_t1756533147 * InstantiateDelegate_Invoke_m2830317127 (InstantiateDelegate_t3700441677 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		InstantiateDelegate_Invoke_m2830317127((InstantiateDelegate_t3700441677 *)__this->get_prev_9(),___prefab0, ___pos1, ___rot2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef GameObject_t1756533147 * (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___prefab0, ___pos1, ___rot2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef GameObject_t1756533147 * (*FunctionPointerType) (void* __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___prefab0, ___pos1, ___rot2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef GameObject_t1756533147 * (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___prefab0, ___pos1, ___rot2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PathologicalGames.SpawnPool/InstantiateDelegate::BeginInvoke(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * InstantiateDelegate_BeginInvoke_m426211799 (InstantiateDelegate_t3700441677 * __this, GameObject_t1756533147 * ___prefab0, Vector3_t2243707580  ___pos1, Quaternion_t4030073918  ___rot2, AsyncCallback_t163412349 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InstantiateDelegate_BeginInvoke_m426211799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___prefab0;
	__d_args[1] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___pos1);
	__d_args[2] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___rot2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// UnityEngine.GameObject PathologicalGames.SpawnPool/InstantiateDelegate::EndInvoke(System.IAsyncResult)
extern "C"  GameObject_t1756533147 * InstantiateDelegate_EndInvoke_m2396253683 (InstantiateDelegate_t3700441677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (GameObject_t1756533147 *)__result;
}
// System.Void PathologicalGames.SpawnPoolsDict::.ctor()
extern "C"  void SpawnPoolsDict__ctor_m2459636125 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict__ctor_m2459636125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4164115705 * L_0 = (Dictionary_2_t4164115705 *)il2cpp_codegen_object_new(Dictionary_2_t4164115705_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1084170225(L_0, /*hidden argument*/Dictionary_2__ctor_m1084170225_MethodInfo_var);
		__this->set_onCreatedDelegates_0(L_0);
		Dictionary_2_t39529491 * L_1 = (Dictionary_2_t39529491 *)il2cpp_codegen_object_new(Dictionary_2_t39529491_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1111865929(L_1, /*hidden argument*/Dictionary_2__ctor_m1111865929_MethodInfo_var);
		__this->set__pools_1(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::AddOnCreatedDelegate(System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate)
extern "C"  void SpawnPoolsDict_AddOnCreatedDelegate_m3725830929 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, OnCreatedDelegate_t2249336443 * ___createdDelegate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_AddOnCreatedDelegate_m3725830929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t4164115705 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Dictionary_2_t4164115705 * L_0 = __this->get_onCreatedDelegates_0();
		String_t* L_1 = ___poolName0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1081054922(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1081054922_MethodInfo_var);
		if (L_2)
		{
			goto IL_0035;
		}
	}
	{
		Dictionary_2_t4164115705 * L_3 = __this->get_onCreatedDelegates_0();
		String_t* L_4 = ___poolName0;
		OnCreatedDelegate_t2249336443 * L_5 = ___createdDelegate1;
		NullCheck(L_3);
		Dictionary_2_Add_m2575601273(L_3, L_4, L_5, /*hidden argument*/Dictionary_2_Add_m2575601273_MethodInfo_var);
		String_t* L_6 = ___poolName0;
		OnCreatedDelegate_t2249336443 * L_7 = ___createdDelegate1;
		NullCheck(L_7);
		Il2CppObject * L_8 = Delegate_get_Target_m896795953(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2033998413, L_6, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}

IL_0035:
	{
		Dictionary_2_t4164115705 * L_10 = __this->get_onCreatedDelegates_0();
		Dictionary_2_t4164115705 * L_11 = L_10;
		V_0 = L_11;
		String_t* L_12 = ___poolName0;
		String_t* L_13 = L_12;
		V_1 = L_13;
		Dictionary_2_t4164115705 * L_14 = V_0;
		String_t* L_15 = V_1;
		NullCheck(L_14);
		OnCreatedDelegate_t2249336443 * L_16 = Dictionary_2_get_Item_m1130184155(L_14, L_15, /*hidden argument*/Dictionary_2_get_Item_m1130184155_MethodInfo_var);
		OnCreatedDelegate_t2249336443 * L_17 = ___createdDelegate1;
		Delegate_t3022476291 * L_18 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_set_Item_m3828308582(L_11, L_13, ((OnCreatedDelegate_t2249336443 *)CastclassSealed(L_18, OnCreatedDelegate_t2249336443_il2cpp_TypeInfo_var)), /*hidden argument*/Dictionary_2_set_Item_m3828308582_MethodInfo_var);
		return;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::RemoveOnCreatedDelegate(System.String,PathologicalGames.SpawnPoolsDict/OnCreatedDelegate)
extern "C"  void SpawnPoolsDict_RemoveOnCreatedDelegate_m2862845276 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, OnCreatedDelegate_t2249336443 * ___createdDelegate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_RemoveOnCreatedDelegate_m2862845276_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t4164115705 * V_0 = NULL;
	String_t* V_1 = NULL;
	{
		Dictionary_2_t4164115705 * L_0 = __this->get_onCreatedDelegates_0();
		String_t* L_1 = ___poolName0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1081054922(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1081054922_MethodInfo_var);
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_3 = ___poolName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral4011475393, L_3, _stringLiteral1617874521, /*hidden argument*/NULL);
		KeyNotFoundException_t1722175009 * L_5 = (KeyNotFoundException_t1722175009 *)il2cpp_codegen_object_new(KeyNotFoundException_t1722175009_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m264393096(L_5, L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0027:
	{
		Dictionary_2_t4164115705 * L_6 = __this->get_onCreatedDelegates_0();
		Dictionary_2_t4164115705 * L_7 = L_6;
		V_0 = L_7;
		String_t* L_8 = ___poolName0;
		String_t* L_9 = L_8;
		V_1 = L_9;
		Dictionary_2_t4164115705 * L_10 = V_0;
		String_t* L_11 = V_1;
		NullCheck(L_10);
		OnCreatedDelegate_t2249336443 * L_12 = Dictionary_2_get_Item_m1130184155(L_10, L_11, /*hidden argument*/Dictionary_2_get_Item_m1130184155_MethodInfo_var);
		OnCreatedDelegate_t2249336443 * L_13 = ___createdDelegate1;
		Delegate_t3022476291 * L_14 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_7);
		Dictionary_2_set_Item_m3828308582(L_7, L_9, ((OnCreatedDelegate_t2249336443 *)CastclassSealed(L_14, OnCreatedDelegate_t2249336443_il2cpp_TypeInfo_var)), /*hidden argument*/Dictionary_2_set_Item_m3828308582_MethodInfo_var);
		String_t* L_15 = ___poolName0;
		OnCreatedDelegate_t2249336443 * L_16 = ___createdDelegate1;
		NullCheck(L_16);
		Il2CppObject * L_17 = Delegate_get_Target_m896795953(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4098646607, L_15, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return;
	}
}
// PathologicalGames.SpawnPool PathologicalGames.SpawnPoolsDict::Create(System.String)
extern "C"  SpawnPool_t2419717525 * SpawnPoolsDict_Create_m2130217657 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Create_m2130217657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		String_t* L_0 = ___poolName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral204393016, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		SpawnPool_t2419717525 * L_4 = GameObject_AddComponent_TisSpawnPool_t2419717525_m2522654191(L_3, /*hidden argument*/GameObject_AddComponent_TisSpawnPool_t2419717525_m2522654191_MethodInfo_var);
		return L_4;
	}
}
// PathologicalGames.SpawnPool PathologicalGames.SpawnPoolsDict::Create(System.String,UnityEngine.GameObject)
extern "C"  SpawnPool_t2419717525 * SpawnPoolsDict_Create_m2712319509 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, GameObject_t1756533147 * ___owner1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Create_m2712319509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	SpawnPool_t2419717525 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___poolName0;
		bool L_1 = SpawnPoolsDict_assertValidPoolName_m1240830686(__this, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (SpawnPool_t2419717525 *)NULL;
	}

IL_000e:
	{
		GameObject_t1756533147 * L_2 = ___owner1;
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = GameObject_get_gameObject_m3662236595(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m2079638459(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		GameObject_t1756533147 * L_5 = ___owner1;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = GameObject_get_gameObject_m3662236595(L_5, /*hidden argument*/NULL);
		String_t* L_7 = ___poolName0;
		NullCheck(L_6);
		Object_set_name_m4157836998(L_6, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = ___owner1;
		NullCheck(L_8);
		SpawnPool_t2419717525 * L_9 = GameObject_AddComponent_TisSpawnPool_t2419717525_m2522654191(L_8, /*hidden argument*/GameObject_AddComponent_TisSpawnPool_t2419717525_m2522654191_MethodInfo_var);
		V_1 = L_9;
		IL2CPP_LEAVE(0x3F, FINALLY_0032);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0032;
	}

FINALLY_0032:
	{ // begin finally (depth: 1)
		GameObject_t1756533147 * L_10 = ___owner1;
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = GameObject_get_gameObject_m3662236595(L_10, /*hidden argument*/NULL);
		String_t* L_12 = V_0;
		NullCheck(L_11);
		Object_set_name_m4157836998(L_11, L_12, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(50)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(50)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003f:
	{
		SpawnPool_t2419717525 * L_13 = V_1;
		return L_13;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::assertValidPoolName(System.String)
extern "C"  bool SpawnPoolsDict_assertValidPoolName_m1240830686 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_assertValidPoolName_m1240830686_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	{
		String_t* L_0 = ___poolName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m1941156251(L_0, _stringLiteral204393016, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		String_t* L_4 = ___poolName0;
		bool L_5 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		String_t* L_6 = ___poolName0;
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2999319186, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		String_t* L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		String_t* L_10 = V_0;
		___poolName0 = L_10;
	}

IL_0033:
	{
		String_t* L_11 = ___poolName0;
		bool L_12 = SpawnPoolsDict_ContainsKey_m2077114099(__this, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0051;
		}
	}
	{
		String_t* L_13 = ___poolName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral267774189, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0051:
	{
		return (bool)1;
	}
}
// System.String PathologicalGames.SpawnPoolsDict::ToString()
extern "C"  String_t* SpawnPoolsDict_ToString_m724599862 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_ToString_m724599862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m1267632521(L_0, /*hidden argument*/Dictionary_2_get_Count_m1267632521_MethodInfo_var);
		V_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_1));
		Dictionary_2_t39529491 * L_2 = __this->get__pools_1();
		NullCheck(L_2);
		KeyCollection_t2523027262 * L_3 = Dictionary_2_get_Keys_m339431039(L_2, /*hidden argument*/Dictionary_2_get_Keys_m339431039_MethodInfo_var);
		StringU5BU5D_t1642385972* L_4 = V_0;
		NullCheck(L_3);
		KeyCollection_CopyTo_m861994567(L_3, L_4, 0, /*hidden argument*/KeyCollection_CopyTo_m861994567_MethodInfo_var);
		StringU5BU5D_t1642385972* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Join_m1966872927(NULL /*static, unused*/, _stringLiteral811305474, L_5, /*hidden argument*/NULL);
		String_t* L_7 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1555060386, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::Destroy(System.String)
extern "C"  bool SpawnPoolsDict_Destroy_m3596035525 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Destroy_m3596035525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpawnPool_t2419717525 * V_0 = NULL;
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		String_t* L_1 = ___poolName0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_m3478201132(L_0, L_1, (&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m3478201132_MethodInfo_var);
		if (L_2)
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_3 = ___poolName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3388612068, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0025:
	{
		SpawnPool_t2419717525 * L_5 = V_0;
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Dictionary_2_t39529491 * L_7 = __this->get__pools_1();
		SpawnPool_t2419717525 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_poolName_2();
		NullCheck(L_7);
		Dictionary_2_Remove_m775250656(L_7, L_9, /*hidden argument*/Dictionary_2_Remove_m775250656_MethodInfo_var);
		return (bool)1;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::DestroyAll()
extern "C"  void SpawnPoolsDict_DestroyAll_m1889592154 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_DestroyAll_m1889592154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyValuePair_2_t2091842009  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Enumerator_t1359554193  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		NullCheck(L_0);
		Enumerator_t1359554193  L_1 = Dictionary_2_GetEnumerator_m209731277(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m209731277_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004a;
		}

IL_0011:
		{
			KeyValuePair_2_t2091842009  L_2 = Enumerator_get_Current_m1934570397((&V_1), /*hidden argument*/Enumerator_get_Current_m1934570397_MethodInfo_var);
			V_0 = L_2;
			SpawnPool_t2419717525 * L_3 = KeyValuePair_2_get_Value_m3480818628((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3480818628_MethodInfo_var);
			NullCheck(L_3);
			GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
			NullCheck(L_4);
			String_t* L_5 = Object_get_name_m2079638459(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1834820150, L_5, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_Log_m920475918(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			SpawnPool_t2419717525 * L_7 = KeyValuePair_2_get_Value_m3480818628((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m3480818628_MethodInfo_var);
			NullCheck(L_7);
			GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(L_7, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		}

IL_004a:
		{
			bool L_9 = Enumerator_MoveNext_m2014802852((&V_1), /*hidden argument*/Enumerator_MoveNext_m2014802852_MethodInfo_var);
			if (L_9)
			{
				goto IL_0011;
			}
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x69, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2095172018((&V_1), /*hidden argument*/Enumerator_Dispose_m2095172018_MethodInfo_var);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x69, IL_0069)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0069:
	{
		Dictionary_2_t39529491 * L_10 = __this->get__pools_1();
		NullCheck(L_10);
		Dictionary_2_Clear_m1196654984(L_10, /*hidden argument*/Dictionary_2_Clear_m1196654984_MethodInfo_var);
		return;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::Add(PathologicalGames.SpawnPool)
extern "C"  void SpawnPoolsDict_Add_m2638912137 (SpawnPoolsDict_t1520791760 * __this, SpawnPool_t2419717525 * ___spawnPool0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Add_m2638912137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpawnPool_t2419717525 * L_0 = ___spawnPool0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_poolName_2();
		bool L_2 = SpawnPoolsDict_ContainsKey_m2077114099(__this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		SpawnPool_t2419717525 * L_3 = ___spawnPool0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral1835617696, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		Dictionary_2_t39529491 * L_6 = __this->get__pools_1();
		SpawnPool_t2419717525 * L_7 = ___spawnPool0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_poolName_2();
		SpawnPool_t2419717525 * L_9 = ___spawnPool0;
		NullCheck(L_6);
		Dictionary_2_Add_m3678661409(L_6, L_8, L_9, /*hidden argument*/Dictionary_2_Add_m3678661409_MethodInfo_var);
		SpawnPool_t2419717525 * L_10 = ___spawnPool0;
		NullCheck(L_10);
		String_t* L_11 = L_10->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2041708912, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		Dictionary_2_t4164115705 * L_13 = __this->get_onCreatedDelegates_0();
		SpawnPool_t2419717525 * L_14 = ___spawnPool0;
		NullCheck(L_14);
		String_t* L_15 = L_14->get_poolName_2();
		NullCheck(L_13);
		bool L_16 = Dictionary_2_ContainsKey_m1081054922(L_13, L_15, /*hidden argument*/Dictionary_2_ContainsKey_m1081054922_MethodInfo_var);
		if (!L_16)
		{
			goto IL_007b;
		}
	}
	{
		Dictionary_2_t4164115705 * L_17 = __this->get_onCreatedDelegates_0();
		SpawnPool_t2419717525 * L_18 = ___spawnPool0;
		NullCheck(L_18);
		String_t* L_19 = L_18->get_poolName_2();
		NullCheck(L_17);
		OnCreatedDelegate_t2249336443 * L_20 = Dictionary_2_get_Item_m1130184155(L_17, L_19, /*hidden argument*/Dictionary_2_get_Item_m1130184155_MethodInfo_var);
		SpawnPool_t2419717525 * L_21 = ___spawnPool0;
		NullCheck(L_20);
		OnCreatedDelegate_Invoke_m3309419437(L_20, L_21, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::Add(System.String,PathologicalGames.SpawnPool)
extern "C"  void SpawnPoolsDict_Add_m2690135699 (SpawnPoolsDict_t1520791760 * __this, String_t* ___key0, SpawnPool_t2419717525 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Add_m2690135699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2866193509;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::Remove(PathologicalGames.SpawnPool)
extern "C"  bool SpawnPoolsDict_Remove_m3353331162 (SpawnPoolsDict_t1520791760 * __this, SpawnPool_t2419717525 * ___spawnPool0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Remove_m3353331162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SpawnPool_t2419717525 * L_0 = ___spawnPool0;
		bool L_1 = SpawnPoolsDict_ContainsValue_m3660170656(__this, L_0, /*hidden argument*/NULL);
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!((int32_t)((int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0)&(int32_t)L_2)))
		{
			goto IL_002c;
		}
	}
	{
		SpawnPool_t2419717525 * L_3 = ___spawnPool0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_poolName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral3823589648, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_002c:
	{
		Dictionary_2_t39529491 * L_6 = __this->get__pools_1();
		SpawnPool_t2419717525 * L_7 = ___spawnPool0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_poolName_2();
		NullCheck(L_6);
		Dictionary_2_Remove_m775250656(L_6, L_8, /*hidden argument*/Dictionary_2_Remove_m775250656_MethodInfo_var);
		return (bool)1;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::Remove(System.String)
extern "C"  bool SpawnPoolsDict_Remove_m3364582693 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Remove_m3364582693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3763332682;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Int32 PathologicalGames.SpawnPoolsDict::get_Count()
extern "C"  int32_t SpawnPoolsDict_get_Count_m945703525 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_get_Count_m945703525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m1267632521(L_0, /*hidden argument*/Dictionary_2_get_Count_m1267632521_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::ContainsKey(System.String)
extern "C"  bool SpawnPoolsDict_ContainsKey_m2077114099 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_ContainsKey_m2077114099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		String_t* L_1 = ___poolName0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsKey_m1243307708(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1243307708_MethodInfo_var);
		return L_2;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::ContainsValue(PathologicalGames.SpawnPool)
extern "C"  bool SpawnPoolsDict_ContainsValue_m3660170656 (SpawnPoolsDict_t1520791760 * __this, SpawnPool_t2419717525 * ___pool0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_ContainsValue_m3660170656_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		SpawnPool_t2419717525 * L_1 = ___pool0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsValue_m2866665949(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsValue_m2866665949_MethodInfo_var);
		return L_2;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::TryGetValue(System.String,PathologicalGames.SpawnPool&)
extern "C"  bool SpawnPoolsDict_TryGetValue_m4237209346 (SpawnPoolsDict_t1520791760 * __this, String_t* ___poolName0, SpawnPool_t2419717525 ** ___spawnPool1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_TryGetValue_m4237209346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		String_t* L_1 = ___poolName0;
		SpawnPool_t2419717525 ** L_2 = ___spawnPool1;
		NullCheck(L_0);
		bool L_3 = Dictionary_2_TryGetValue_m3478201132(L_0, L_1, L_2, /*hidden argument*/Dictionary_2_TryGetValue_m3478201132_MethodInfo_var);
		return L_3;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::Contains(System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>)
extern "C"  bool SpawnPoolsDict_Contains_m1346561444 (SpawnPoolsDict_t1520791760 * __this, KeyValuePair_2_t2091842009  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Contains_m1346561444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_0, _stringLiteral2393457341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// PathologicalGames.SpawnPool PathologicalGames.SpawnPoolsDict::get_Item(System.String)
extern "C"  SpawnPool_t2419717525 * SpawnPoolsDict_get_Item_m3547080165 (SpawnPoolsDict_t1520791760 * __this, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_get_Item_m3547080165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpawnPool_t2419717525 * V_0 = NULL;
	String_t* V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		SpawnPool_t2419717525 * L_2 = Dictionary_2_get_Item_m2588609791(L_0, L_1, /*hidden argument*/Dictionary_2_get_Item_m2588609791_MethodInfo_var);
		V_0 = L_2;
		goto IL_002c;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (KeyNotFoundException_t1722175009_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0012;
		throw e;
	}

CATCH_0012:
	{ // begin catch(System.Collections.Generic.KeyNotFoundException)
		String_t* L_3 = ___key0;
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral4025903905, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6 = V_1;
		KeyNotFoundException_t1722175009 * L_7 = (KeyNotFoundException_t1722175009 *)il2cpp_codegen_object_new(KeyNotFoundException_t1722175009_il2cpp_TypeInfo_var);
		KeyNotFoundException__ctor_m264393096(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	} // end catch (depth: 1)

IL_002c:
	{
		SpawnPool_t2419717525 * L_8 = V_0;
		return L_8;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::set_Item(System.String,PathologicalGames.SpawnPool)
extern "C"  void SpawnPoolsDict_set_Item_m2690855718 (SpawnPoolsDict_t1520791760 * __this, String_t* ___key0, SpawnPool_t2419717525 * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_set_Item_m2690855718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3484800581;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Collections.Generic.ICollection`1<System.String> PathologicalGames.SpawnPoolsDict::get_Keys()
extern "C"  Il2CppObject* SpawnPoolsDict_get_Keys_m3831953038 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_get_Keys_m3831953038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral1595821356;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Collections.Generic.ICollection`1<PathologicalGames.SpawnPool> PathologicalGames.SpawnPoolsDict::get_Values()
extern "C"  Il2CppObject* SpawnPoolsDict_get_Values_m2310424001 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_get_Values_m2310424001_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral1595821356;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::get_IsReadOnly()
extern "C"  bool SpawnPoolsDict_get_IsReadOnly_m1826047374 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,PathologicalGames.SpawnPool>>.get_IsReadOnly()
extern "C"  bool SpawnPoolsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CPathologicalGames_SpawnPoolU3EU3E_get_IsReadOnly_m2881418247 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::Add(System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>)
extern "C"  void SpawnPoolsDict_Add_m4237682212 (SpawnPoolsDict_t1520791760 * __this, KeyValuePair_2_t2091842009  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Add_m4237682212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral2866193509;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::Clear()
extern "C"  void SpawnPoolsDict_Clear_m597009206 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Clear_m597009206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral628749495;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>[],System.Int32)
extern "C"  void SpawnPoolsDict_CopyTo_m757354890 (SpawnPoolsDict_t1520791760 * __this, KeyValuePair_2U5BU5D_t498405028* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_CopyTo_m757354890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3287840122;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Void PathologicalGames.SpawnPoolsDict::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,PathologicalGames.SpawnPool>>.CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>[],System.Int32)
extern "C"  void SpawnPoolsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CPathologicalGames_SpawnPoolU3EU3E_CopyTo_m519111753 (SpawnPoolsDict_t1520791760 * __this, KeyValuePair_2U5BU5D_t498405028* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CPathologicalGames_SpawnPoolU3EU3E_CopyTo_m519111753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3287840122;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Boolean PathologicalGames.SpawnPoolsDict::Remove(System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>)
extern "C"  bool SpawnPoolsDict_Remove_m492150273 (SpawnPoolsDict_t1520791760 * __this, KeyValuePair_2_t2091842009  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_Remove_m492150273_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		V_0 = _stringLiteral3763332682;
		String_t* L_0 = V_0;
		NotImplementedException_t2785117854 * L_1 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m1795163961(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,PathologicalGames.SpawnPool>> PathologicalGames.SpawnPoolsDict::GetEnumerator()
extern "C"  Il2CppObject* SpawnPoolsDict_GetEnumerator_m1344881667 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_GetEnumerator_m1344881667_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		NullCheck(L_0);
		Enumerator_t1359554193  L_1 = Dictionary_2_GetEnumerator_m209731277(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m209731277_MethodInfo_var);
		Enumerator_t1359554193  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t1359554193_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.Collections.IEnumerator PathologicalGames.SpawnPoolsDict::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * SpawnPoolsDict_System_Collections_IEnumerable_GetEnumerator_m2792900310 (SpawnPoolsDict_t1520791760 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpawnPoolsDict_System_Collections_IEnumerable_GetEnumerator_m2792900310_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t39529491 * L_0 = __this->get__pools_1();
		NullCheck(L_0);
		Enumerator_t1359554193  L_1 = Dictionary_2_GetEnumerator_m209731277(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m209731277_MethodInfo_var);
		Enumerator_t1359554193  L_2 = L_1;
		Il2CppObject * L_3 = Box(Enumerator_t1359554193_il2cpp_TypeInfo_var, &L_2);
		return (Il2CppObject *)L_3;
	}
}
// System.Void PathologicalGames.SpawnPoolsDict/OnCreatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnCreatedDelegate__ctor_m1561109738 (OnCreatedDelegate_t2249336443 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void PathologicalGames.SpawnPoolsDict/OnCreatedDelegate::Invoke(PathologicalGames.SpawnPool)
extern "C"  void OnCreatedDelegate_Invoke_m3309419437 (OnCreatedDelegate_t2249336443 * __this, SpawnPool_t2419717525 * ___pool0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnCreatedDelegate_Invoke_m3309419437((OnCreatedDelegate_t2249336443 *)__this->get_prev_9(),___pool0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SpawnPool_t2419717525 * ___pool0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pool0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, SpawnPool_t2419717525 * ___pool0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pool0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pool0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult PathologicalGames.SpawnPoolsDict/OnCreatedDelegate::BeginInvoke(PathologicalGames.SpawnPool,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnCreatedDelegate_BeginInvoke_m3890724322 (OnCreatedDelegate_t2249336443 * __this, SpawnPool_t2419717525 * ___pool0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pool0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void PathologicalGames.SpawnPoolsDict/OnCreatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnCreatedDelegate_EndInvoke_m2520810440 (OnCreatedDelegate_t2249336443 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
