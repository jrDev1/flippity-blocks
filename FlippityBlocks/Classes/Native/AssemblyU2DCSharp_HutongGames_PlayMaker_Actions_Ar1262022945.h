﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmVar
struct FsmVar_t2872592513;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListSet
struct  ArrayListSet_t1262022945  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListSet::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListSet::reference
	FsmString_t2414474701 * ___reference_13;
	// HutongGames.PlayMaker.FsmInt HutongGames.PlayMaker.Actions.ArrayListSet::atIndex
	FsmInt_t1273009179 * ___atIndex_14;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListSet::forceResizeIdNeeded
	bool ___forceResizeIdNeeded_15;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListSet::everyFrame
	bool ___everyFrame_16;
	// HutongGames.PlayMaker.FsmVar HutongGames.PlayMaker.Actions.ArrayListSet::variable
	FsmVar_t2872592513 * ___variable_17;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListSet_t1262022945, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListSet_t1262022945, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_atIndex_14() { return static_cast<int32_t>(offsetof(ArrayListSet_t1262022945, ___atIndex_14)); }
	inline FsmInt_t1273009179 * get_atIndex_14() const { return ___atIndex_14; }
	inline FsmInt_t1273009179 ** get_address_of_atIndex_14() { return &___atIndex_14; }
	inline void set_atIndex_14(FsmInt_t1273009179 * value)
	{
		___atIndex_14 = value;
		Il2CppCodeGenWriteBarrier(&___atIndex_14, value);
	}

	inline static int32_t get_offset_of_forceResizeIdNeeded_15() { return static_cast<int32_t>(offsetof(ArrayListSet_t1262022945, ___forceResizeIdNeeded_15)); }
	inline bool get_forceResizeIdNeeded_15() const { return ___forceResizeIdNeeded_15; }
	inline bool* get_address_of_forceResizeIdNeeded_15() { return &___forceResizeIdNeeded_15; }
	inline void set_forceResizeIdNeeded_15(bool value)
	{
		___forceResizeIdNeeded_15 = value;
	}

	inline static int32_t get_offset_of_everyFrame_16() { return static_cast<int32_t>(offsetof(ArrayListSet_t1262022945, ___everyFrame_16)); }
	inline bool get_everyFrame_16() const { return ___everyFrame_16; }
	inline bool* get_address_of_everyFrame_16() { return &___everyFrame_16; }
	inline void set_everyFrame_16(bool value)
	{
		___everyFrame_16 = value;
	}

	inline static int32_t get_offset_of_variable_17() { return static_cast<int32_t>(offsetof(ArrayListSet_t1262022945, ___variable_17)); }
	inline FsmVar_t2872592513 * get_variable_17() const { return ___variable_17; }
	inline FsmVar_t2872592513 ** get_address_of_variable_17() { return &___variable_17; }
	inline void set_variable_17(FsmVar_t2872592513 * value)
	{
		___variable_17 = value;
		Il2CppCodeGenWriteBarrier(&___variable_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
