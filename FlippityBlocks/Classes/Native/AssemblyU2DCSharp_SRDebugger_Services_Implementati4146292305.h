﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SRDebugger_ConsoleAlignment2755495440.h"

// SRDebugger.UI.Other.DockConsoleController
struct DockConsoleController_t2913316086;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.DockConsoleServiceImpl
struct  DockConsoleServiceImpl_t4146292305  : public Il2CppObject
{
public:
	// SRDebugger.ConsoleAlignment SRDebugger.Services.Implementation.DockConsoleServiceImpl::_alignment
	int32_t ____alignment_0;
	// SRDebugger.UI.Other.DockConsoleController SRDebugger.Services.Implementation.DockConsoleServiceImpl::_consoleRoot
	DockConsoleController_t2913316086 * ____consoleRoot_1;
	// System.Boolean SRDebugger.Services.Implementation.DockConsoleServiceImpl::_didSuspendTrigger
	bool ____didSuspendTrigger_2;
	// System.Boolean SRDebugger.Services.Implementation.DockConsoleServiceImpl::_isExpanded
	bool ____isExpanded_3;
	// System.Boolean SRDebugger.Services.Implementation.DockConsoleServiceImpl::_isVisible
	bool ____isVisible_4;

public:
	inline static int32_t get_offset_of__alignment_0() { return static_cast<int32_t>(offsetof(DockConsoleServiceImpl_t4146292305, ____alignment_0)); }
	inline int32_t get__alignment_0() const { return ____alignment_0; }
	inline int32_t* get_address_of__alignment_0() { return &____alignment_0; }
	inline void set__alignment_0(int32_t value)
	{
		____alignment_0 = value;
	}

	inline static int32_t get_offset_of__consoleRoot_1() { return static_cast<int32_t>(offsetof(DockConsoleServiceImpl_t4146292305, ____consoleRoot_1)); }
	inline DockConsoleController_t2913316086 * get__consoleRoot_1() const { return ____consoleRoot_1; }
	inline DockConsoleController_t2913316086 ** get_address_of__consoleRoot_1() { return &____consoleRoot_1; }
	inline void set__consoleRoot_1(DockConsoleController_t2913316086 * value)
	{
		____consoleRoot_1 = value;
		Il2CppCodeGenWriteBarrier(&____consoleRoot_1, value);
	}

	inline static int32_t get_offset_of__didSuspendTrigger_2() { return static_cast<int32_t>(offsetof(DockConsoleServiceImpl_t4146292305, ____didSuspendTrigger_2)); }
	inline bool get__didSuspendTrigger_2() const { return ____didSuspendTrigger_2; }
	inline bool* get_address_of__didSuspendTrigger_2() { return &____didSuspendTrigger_2; }
	inline void set__didSuspendTrigger_2(bool value)
	{
		____didSuspendTrigger_2 = value;
	}

	inline static int32_t get_offset_of__isExpanded_3() { return static_cast<int32_t>(offsetof(DockConsoleServiceImpl_t4146292305, ____isExpanded_3)); }
	inline bool get__isExpanded_3() const { return ____isExpanded_3; }
	inline bool* get_address_of__isExpanded_3() { return &____isExpanded_3; }
	inline void set__isExpanded_3(bool value)
	{
		____isExpanded_3 = value;
	}

	inline static int32_t get_offset_of__isVisible_4() { return static_cast<int32_t>(offsetof(DockConsoleServiceImpl_t4146292305, ____isVisible_4)); }
	inline bool get__isVisible_4() const { return ____isVisible_4; }
	inline bool* get_address_of__isVisible_4() { return &____isVisible_4; }
	inline void set__isVisible_4(bool value)
	{
		____isVisible_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
