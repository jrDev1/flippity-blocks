﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HutongGames_PlayMaker_Actions_Ar4122909936.h"

// HutongGames.PlayMaker.FsmOwnerDefault
struct FsmOwnerDefault_t2023674184;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Func`3<System.Single,System.Single,System.Single>
struct Func_3_t121963928;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayListGetAverageValue
struct  ArrayListGetAverageValue_t1272662253  : public ArrayListActions_t4122909936
{
public:
	// HutongGames.PlayMaker.FsmOwnerDefault HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::gameObject
	FsmOwnerDefault_t2023674184 * ___gameObject_12;
	// HutongGames.PlayMaker.FsmString HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::reference
	FsmString_t2414474701 * ___reference_13;
	// System.Boolean HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::everyframe
	bool ___everyframe_14;
	// HutongGames.PlayMaker.FsmFloat HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::averageValue
	FsmFloat_t937133978 * ___averageValue_15;
	// System.Collections.Generic.List`1<System.Single> HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::_floats
	List_1_t1445631064 * ____floats_16;

public:
	inline static int32_t get_offset_of_gameObject_12() { return static_cast<int32_t>(offsetof(ArrayListGetAverageValue_t1272662253, ___gameObject_12)); }
	inline FsmOwnerDefault_t2023674184 * get_gameObject_12() const { return ___gameObject_12; }
	inline FsmOwnerDefault_t2023674184 ** get_address_of_gameObject_12() { return &___gameObject_12; }
	inline void set_gameObject_12(FsmOwnerDefault_t2023674184 * value)
	{
		___gameObject_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_12, value);
	}

	inline static int32_t get_offset_of_reference_13() { return static_cast<int32_t>(offsetof(ArrayListGetAverageValue_t1272662253, ___reference_13)); }
	inline FsmString_t2414474701 * get_reference_13() const { return ___reference_13; }
	inline FsmString_t2414474701 ** get_address_of_reference_13() { return &___reference_13; }
	inline void set_reference_13(FsmString_t2414474701 * value)
	{
		___reference_13 = value;
		Il2CppCodeGenWriteBarrier(&___reference_13, value);
	}

	inline static int32_t get_offset_of_everyframe_14() { return static_cast<int32_t>(offsetof(ArrayListGetAverageValue_t1272662253, ___everyframe_14)); }
	inline bool get_everyframe_14() const { return ___everyframe_14; }
	inline bool* get_address_of_everyframe_14() { return &___everyframe_14; }
	inline void set_everyframe_14(bool value)
	{
		___everyframe_14 = value;
	}

	inline static int32_t get_offset_of_averageValue_15() { return static_cast<int32_t>(offsetof(ArrayListGetAverageValue_t1272662253, ___averageValue_15)); }
	inline FsmFloat_t937133978 * get_averageValue_15() const { return ___averageValue_15; }
	inline FsmFloat_t937133978 ** get_address_of_averageValue_15() { return &___averageValue_15; }
	inline void set_averageValue_15(FsmFloat_t937133978 * value)
	{
		___averageValue_15 = value;
		Il2CppCodeGenWriteBarrier(&___averageValue_15, value);
	}

	inline static int32_t get_offset_of__floats_16() { return static_cast<int32_t>(offsetof(ArrayListGetAverageValue_t1272662253, ____floats_16)); }
	inline List_1_t1445631064 * get__floats_16() const { return ____floats_16; }
	inline List_1_t1445631064 ** get_address_of__floats_16() { return &____floats_16; }
	inline void set__floats_16(List_1_t1445631064 * value)
	{
		____floats_16 = value;
		Il2CppCodeGenWriteBarrier(&____floats_16, value);
	}
};

struct ArrayListGetAverageValue_t1272662253_StaticFields
{
public:
	// System.Func`3<System.Single,System.Single,System.Single> HutongGames.PlayMaker.Actions.ArrayListGetAverageValue::<>f__am$cache0
	Func_3_t121963928 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(ArrayListGetAverageValue_t1272662253_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Func_3_t121963928 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Func_3_t121963928 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Func_3_t121963928 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
