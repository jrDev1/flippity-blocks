﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "PlayMaker_PlayMakerAnimatorMove3213404038.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Int322071877448.h"
#include "PlayMaker_PlayMakerFSM437737208.h"
#include "PlayMaker_PlayMakerProxyBase3141506643.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "PlayMaker_HutongGames_PlayMaker_Fsm917886356.h"
#include "PlayMaker_PlayMakerApplicationEvents3759624703.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEvent1258573736.h"
#include "PlayMaker_PlayMakerCollisionEnter204200476.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "PlayMaker_PlayMakerCollisionEnter2D422695966.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "PlayMaker_PlayMakerCollisionExit3953531962.h"
#include "PlayMaker_PlayMakerCollisionExit2D2560737500.h"
#include "PlayMaker_PlayMakerCollisionStay2306635791.h"
#include "PlayMaker_PlayMakerCollisionStay2D4047738741.h"
#include "PlayMaker_PlayMakerControllerColliderHit182605873.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "PlayMaker_PlayMakerFixedUpdate960084629.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4101825636.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3636555310.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "PlayMaker_FsmTemplate1285897084.h"
#include "UnityEngine_UnityEngine_GUITexture1909122990.h"
#include "UnityEngine_UnityEngine_GUIText2411476300.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVariables630687169.h"
#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Behaviour955675639.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "PlayMaker_PlayMakerOnGUI2031863694.h"
#include "PlayMaker_PlayMakerFSM_U3CDoCoroutineU3Ed__14237467219.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmEventData2110469976.h"
#include "UnityEngine_UnityEngine_NetworkPlayer1243528291.h"
#include "UnityEngine_UnityEngine_NetworkDisconnection45590380.h"
#include "UnityEngine_UnityEngine_NetworkConnectionError1607866742.h"
#include "UnityEngine_UnityEngine_NetworkMessageInfo614064059.h"
#include "UnityEngine_UnityEngine_BitStream1979465639.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmString2414474701.h"
#include "mscorlib_System_Char3454481338.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmBool664485696.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmFloat937133978.h"
#include "mscorlib_System_Single2076509932.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmInt1273009179.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmQuaternion878438756.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector33996534004.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmColor118301965.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmVector22430450063.h"
#include "PlayMaker_HutongGames_PlayMaker_NamedVariable3026441313.h"
#include "UnityEngine_UnityEngine_MasterServerEvent2097711603.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmState1643911659.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmTransition1534990431.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "PlayMaker_PlayMakerGlobals2120229426.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "UnityEngine_UnityEngine_ScriptableObject1975622470.h"
#include "PlayMaker_PlayMakerGUI2662579489.h"
#include "UnityEngine_UnityEngine_GUISkin1436893342.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"
#include "UnityEngine_UnityEngine_GUIStyle1799908754.h"
#include "UnityEngine_UnityEngine_GUIStyleState3801000545.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Comparison_1_gen1699476059.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_CursorLockMode3372615096.h"
#include "mscorlib_System_Collections_Generic_List_1_gen287007488.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"
#include "PlayMaker_PlayMakerJointBreak1658177799.h"
#include "PlayMaker_PlayMakerJointBreak2D3128675709.h"
#include "UnityEngine_UnityEngine_Joint2D854621618.h"
#include "PlayMaker_PlayMakerMouseEvents3625748060.h"
#include "PlayMaker_PlayMakerParticleCollision1230092432.h"
#include "PlayMaker_PlayMakerPrefs1833055544.h"
#include "PlayMaker_PlayMakerTriggerEnter2991464208.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "PlayMaker_PlayMakerTriggerEnter2D2208085914.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "PlayMaker_PlayMakerTriggerExit2606889942.h"
#include "PlayMaker_PlayMakerTriggerExit2D1071223868.h"
#include "PlayMaker_PlayMakerTriggerStay2768254945.h"
#include "PlayMaker_PlayMakerTriggerStay2D3506020163.h"

// PlayMakerAnimatorMove
struct PlayMakerAnimatorMove_t3213404038;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// HutongGames.PlayMaker.Fsm
struct Fsm_t917886356;
// PlayMakerProxyBase
struct PlayMakerProxyBase_t3141506643;
// PlayMakerApplicationEvents
struct PlayMakerApplicationEvents_t3759624703;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// PlayMakerCollisionEnter
struct PlayMakerCollisionEnter_t204200476;
// UnityEngine.Collision
struct Collision_t2876846408;
// PlayMakerCollisionEnter2D
struct PlayMakerCollisionEnter2D_t422695966;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// PlayMakerCollisionExit
struct PlayMakerCollisionExit_t3953531962;
// PlayMakerCollisionExit2D
struct PlayMakerCollisionExit2D_t2560737500;
// PlayMakerCollisionStay
struct PlayMakerCollisionStay_t2306635791;
// PlayMakerCollisionStay2D
struct PlayMakerCollisionStay2D_t4047738741;
// PlayMakerControllerColliderHit
struct PlayMakerControllerColliderHit_t182605873;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t4070855101;
// PlayMakerFixedUpdate
struct PlayMakerFixedUpdate_t960084629;
// System.String
struct String_t;
// System.Collections.Generic.List`1<PlayMakerFSM>
struct List_1_t4101825636;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Object
struct Il2CppObject;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Object
struct Object_t1021602117;
// FsmTemplate
struct FsmTemplate_t1285897084;
// UnityEngine.GUITexture
struct GUITexture_t1909122990;
// UnityEngine.GUIText
struct GUIText_t2411476300;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// HutongGames.PlayMaker.FsmVariables
struct FsmVariables_t630687169;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// PlayMakerOnGUI
struct PlayMakerOnGUI_t2031863694;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PlayMakerFSM/<DoCoroutine>d__1
struct U3CDoCoroutineU3Ed__1_t4237467219;
// System.Collections.Generic.IEnumerable`1<PlayMakerFSM>
struct IEnumerable_1_t729864253;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// HutongGames.PlayMaker.FsmEventData
struct FsmEventData_t2110469976;
// UnityEngine.BitStream
struct BitStream_t1979465639;
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328;
// HutongGames.PlayMaker.NamedVariable
struct NamedVariable_t3026441313;
// HutongGames.PlayMaker.FsmString
struct FsmString_t2414474701;
// System.Char[]
struct CharU5BU5D_t1328083999;
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3830815681;
// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t4177556671;
// HutongGames.PlayMaker.FsmFloat
struct FsmFloat_t937133978;
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802;
// HutongGames.PlayMaker.FsmInt
struct FsmInt_t1273009179;
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t3489263757;
// HutongGames.PlayMaker.FsmQuaternion
struct FsmQuaternion_t878438756;
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t643261629;
// HutongGames.PlayMaker.FsmVector3
struct FsmVector3_t3996534004;
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t4100123680;
// HutongGames.PlayMaker.FsmColor
struct FsmColor_t118301965;
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t381696854;
// HutongGames.PlayMaker.FsmVector2
struct FsmVector2_t2430450063;
// HutongGames.PlayMaker.FsmState
struct FsmState_t1643911659;
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t1586422282;
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t287863993;
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t1091630918;
// HutongGames.PlayMaker.FsmTransition
struct FsmTransition_t1534990431;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// System.Type
struct Type_t;
// PlayMakerGlobals
struct PlayMakerGlobals_t2120229426;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470;
// PlayMakerGUI
struct PlayMakerGUI_t2662579489;
// UnityEngine.GUISkin
struct GUISkin_t1436893342;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.GUIStyle
struct GUIStyle_t1799908754;
// UnityEngine.GUIStyleState
struct GUIStyleState_t3801000545;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Comparison`1<PlayMakerFSM>
struct Comparison_1_t1699476059;
// System.Comparison`1<System.Object>
struct Comparison_1_t3951188146;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GUIContent
struct GUIContent_t4210063000;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>
struct List_1_t287007488;
// UnityEngine.Event
struct Event_t3028476042;
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1497896004;
// HutongGames.PlayMaker.FsmStateAction
struct FsmStateAction_t2862378169;
// PlayMakerJointBreak
struct PlayMakerJointBreak_t1658177799;
// PlayMakerJointBreak2D
struct PlayMakerJointBreak2D_t3128675709;
// UnityEngine.Joint2D
struct Joint2D_t854621618;
// PlayMakerMouseEvents
struct PlayMakerMouseEvents_t3625748060;
// PlayMakerParticleCollision
struct PlayMakerParticleCollision_t1230092432;
// PlayMakerPrefs
struct PlayMakerPrefs_t1833055544;
// System.String[]
struct StringU5BU5D_t1642385972;
// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t623924777;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// PlayMakerTriggerEnter
struct PlayMakerTriggerEnter_t2991464208;
// UnityEngine.Collider
struct Collider_t3497673348;
// PlayMakerTriggerEnter2D
struct PlayMakerTriggerEnter2D_t2208085914;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// PlayMakerTriggerExit
struct PlayMakerTriggerExit_t2606889942;
// PlayMakerTriggerExit2D
struct PlayMakerTriggerExit2D_t1071223868;
// PlayMakerTriggerStay
struct PlayMakerTriggerStay_t2768254945;
// PlayMakerTriggerStay2D
struct PlayMakerTriggerStay2D_t3506020163;
extern Il2CppClass* FsmEvent_t1258573736_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerApplicationEvents_OnApplicationFocus_m2511040277_MetadataUsageId;
extern const uint32_t PlayMakerApplicationEvents_OnApplicationPause_m3698503875_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t PlayMakerFSM_get_VersionNotes_m805927392_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_VersionLabel_m1627028815_MetadataUsageId;
extern Il2CppClass* PlayMakerFSM_t437737208_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_get_FsmList_m5507309_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4042834880_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4187226974_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3092022352_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3206985816_MethodInfo_var;
extern const uint32_t PlayMakerFSM_FindFsmOnGameObject_m2273894409_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_DrawGizmos_m1038294635_MetadataUsageId;
extern const uint32_t PlayMakerFSM_set_DrawGizmos_m2543964976_MetadataUsageId;
extern Il2CppClass* Fsm_t917886356_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_Reset_m222573198_MetadataUsageId;
extern Il2CppClass* FsmLog_t3672513366_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_Awake_m2941374002_MetadataUsageId;
extern const uint32_t PlayMakerFSM_Preprocess_m1370866543_MetadataUsageId;
extern const uint32_t PlayMakerFSM_Init_m2906566981_MetadataUsageId;
extern const uint32_t PlayMakerFSM_InitTemplate_m3833177745_MetadataUsageId;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1003321737;
extern const uint32_t PlayMakerFSM_InitFsm_m3268071385_MetadataUsageId;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t3625748060_m770497949_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t204200476_m4106104525_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3953531962_m4026045093_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2306635791_m1946610428_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2991464208_m2851453031_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2606889942_m3150703343_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t2768254945_m307590968_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t422695966_m922427891_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2560737500_m2696690659_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t4047738741_m3163478950_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t2208085914_m1354435365_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t1071223868_m163592089_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t3506020163_m241445942_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t1230092432_m3673912193_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t182605873_m313356344_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t1658177799_m1469739734_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t960084629_m2689148394_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisPlayMakerOnGUI_t2031863694_m2511685017_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPlayMakerOnGUI_t2031863694_m1458714166_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t3759624703_m404494050_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t3213404038_m1404375513_MethodInfo_var;
extern const MethodInfo* PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t1460122143_m1385004492_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3847144474;
extern const uint32_t PlayMakerFSM_AddEventHandlerComponents_m3901157882_MetadataUsageId;
extern Il2CppClass* FsmVariables_t630687169_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_SetFsmTemplate_m648066017_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisGUITexture_t1909122990_m3001290792_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisGUIText_t2411476300_m3620974608_MethodInfo_var;
extern const uint32_t PlayMakerFSM_Start_m3186900003_MetadataUsageId;
extern const MethodInfo* List_1_Add_m1863766553_MethodInfo_var;
extern const uint32_t PlayMakerFSM_OnEnable_m739653507_MetadataUsageId;
extern Il2CppClass* U3CDoCoroutineU3Ed__1_t4237467219_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_DoCoroutine_m2070243337_MetadataUsageId;
extern const MethodInfo* List_1_Remove_m3437895080_MethodInfo_var;
extern const uint32_t PlayMakerFSM_OnDisable_m1788428118_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnDestroy_m924941156_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnApplicationQuit_m1000198585_MetadataUsageId;
extern const uint32_t PlayMakerFSM_SendRemoteFsmEventWithData_m2864874037_MetadataUsageId;
extern const uint32_t PlayMakerFSM_BroadcastEvent_m1710776222_MetadataUsageId;
extern Il2CppClass* List_1_t4101825636_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m197216727_MethodInfo_var;
extern const uint32_t PlayMakerFSM_BroadcastEvent_m583362320_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnBecameVisible_m2356140445_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnBecameInvisible_m51942020_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnPlayerConnected_m1919487554_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnServerInitialized_m897882395_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnConnectedToServer_m1688810999_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnPlayerDisconnected_m1152869810_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnDisconnectedFromServer_m3587986053_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnFailedToConnect_m3759082883_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnNetworkInstantiate_m1787303400_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnSerializeNetworkView_m297962887_MetadataUsageId;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerFSM_NetworkSyncVariables_m1890220694_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnMasterServerEvent_m4091791057_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_ActiveStateName_m1650919959_MetadataUsageId;
extern const uint32_t PlayMakerFSM_get_UsesTemplate_m3598466416_MetadataUsageId;
extern const uint32_t PlayMakerFSM_OnAfterDeserialize_m3821310365_MetadataUsageId;
extern const MethodInfo* List_1__ctor_m2230797533_MethodInfo_var;
extern const uint32_t PlayMakerFSM__cctor_m2888832784_MetadataUsageId;
extern Il2CppClass* FsmExecutionStack_t503928862_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoCoroutineU3Ed__1_MoveNext_m3119791968_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m1228084302_MetadataUsageId;
extern Il2CppClass* PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGlobals_get_Initialized_m2540978452_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_Initialized_m2688991965_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsPlayingInEditor_m2116337626_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsPlayingInEditor_m2559612605_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsPlaying_m1998291744_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsPlaying_m3897118763_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsEditor_m2851556513_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsEditor_m1340019058_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_IsBuilding_m3989868652_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_set_IsBuilding_m4135337617_MetadataUsageId;
extern const Il2CppType* PlayMakerGlobals_t2120229426_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern const MethodInfo* ScriptableObject_CreateInstance_TisPlayMakerGlobals_t2120229426_m3104673918_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m596405649_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2848307944;
extern const uint32_t PlayMakerGlobals_Initialize_m3494352383_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_get_Instance_m4066551560_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_ResetInstance_m1747765483_MetadataUsageId;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const uint32_t PlayMakerGlobals_AddEvent_m677616813_MetadataUsageId;
extern const uint32_t PlayMakerGlobals_OnDestroy_m124376554_MetadataUsageId;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t PlayMakerGlobals__ctor_m3164927587_MetadataUsageId;
extern Il2CppClass* PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_get_EnableStateLabels_m3131360880_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_EnableStateLabels_m1834678665_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_EnableStateLabelsInBuild_m654835679_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_EnableStateLabelsInBuild_m2190807688_MetadataUsageId;
extern const Il2CppType* PlayMakerGUI_t2662579489_0_0_0_var;
extern const uint32_t PlayMakerGUI_InitInstance_m2322412903_MetadataUsageId;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisPlayMakerGUI_t2662579489_m366838623_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3390658007;
extern const uint32_t PlayMakerGUI_get_Instance_m3854846106_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_Enabled_m402578142_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUISkin_m2441732861_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUISkin_m4222880562_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIColor_m3278185336_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIColor_m3151620121_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIBackgroundColor_m2538762796_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIBackgroundColor_m3152511687_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIContentColor_m1794729359_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIContentColor_m3644451446_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_GUIMatrix_m1271908730_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_GUIMatrix_m1989428149_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_MouseCursor_m803897773_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_MouseCursor_m3353084044_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_LockCursor_m2699961842_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_LockCursor_m800390487_MetadataUsageId;
extern const uint32_t PlayMakerGUI_get_HideCursor_m1761535307_MetadataUsageId;
extern const uint32_t PlayMakerGUI_set_HideCursor_m2707817742_MetadataUsageId;
extern Il2CppClass* Texture2D_t3542995729_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIStyle_t1799908754_il2cpp_TypeInfo_var;
extern Il2CppClass* RectOffset_t3387826427_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_InitLabelStyle_m2898018055_MetadataUsageId;
extern Il2CppClass* Comparison_1_t1699476059_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m3348543408_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3488027205_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m3632092958_MethodInfo_var;
extern const MethodInfo* PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m2467571249_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m3162477570_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2622171247_MethodInfo_var;
extern const uint32_t PlayMakerGUI_DrawStateLabels_m1092711642_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI_DrawStateLabel_m1328883389_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2625992742;
extern const uint32_t PlayMakerGUI_GenerateStateLabel_m2774892031_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1556299699;
extern const uint32_t PlayMakerGUI_Awake_m3750694929_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_AddRange_m4240858977_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m2255084070_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2965087059_MethodInfo_var;
extern const uint32_t PlayMakerGUI_OnGUI_m507334058_MetadataUsageId;
extern const uint32_t PlayMakerGUI_OnDisable_m2748493449_MetadataUsageId;
extern const uint32_t PlayMakerGUI_DoEditGUI_m2014605058_MetadataUsageId;
extern const uint32_t PlayMakerGUI_OnApplicationQuit_m1056069416_MetadataUsageId;
extern const uint32_t PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m2467571249_MetadataUsageId;
extern Il2CppClass* GUIContent_t4210063000_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerGUI__cctor_m2174270851_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseEnter_m2895234989_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseDown_m3723689487_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseUp_m1713886120_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseUpAsButton_m3605824020_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseExit_m3008400505_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseDrag_m1969676181_MetadataUsageId;
extern const uint32_t PlayMakerMouseEvents_OnMouseOver_m2281620839_MetadataUsageId;
extern const uint32_t PlayMakerOnGUI_Start_m3270530941_MetadataUsageId;
extern const uint32_t PlayMakerOnGUI_OnGUI_m2763967087_MetadataUsageId;
extern const uint32_t PlayMakerOnGUI_DoEditGUI_m2385310713_MetadataUsageId;
extern const MethodInfo* ScriptableObject_CreateInstance_TisPlayMakerPrefs_t1833055544_m1276350044_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2561134062;
extern const uint32_t PlayMakerPrefs_get_Instance_m688943076_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_Colors_m3543534341_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_Colors_m3809722884_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_ColorNames_m494908428_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_set_ColorNames_m3843271929_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_ResetDefaultColors_m993607445_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_get_MinimapColors_m2639067296_MetadataUsageId;
extern const uint32_t PlayMakerPrefs_SaveChanges_m3585553949_MetadataUsageId;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern const uint32_t PlayMakerPrefs_UpdateMinimapColors_m1326282697_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3566263623;
extern Il2CppCodeGenString* _stringLiteral2395476974;
extern Il2CppCodeGenString* _stringLiteral3343466881;
extern Il2CppCodeGenString* _stringLiteral3510846499;
extern Il2CppCodeGenString* _stringLiteral777220966;
extern Il2CppCodeGenString* _stringLiteral3664749912;
extern Il2CppCodeGenString* _stringLiteral3021629811;
extern Il2CppCodeGenString* _stringLiteral4158023012;
extern const uint32_t PlayMakerPrefs__ctor_m1864640893_MetadataUsageId;
extern const uint32_t PlayMakerPrefs__cctor_m2817732924_MetadataUsageId;
extern const MethodInfo* Component_GetComponents_TisPlayMakerFSM_t437737208_m853540674_MethodInfo_var;
extern const uint32_t PlayMakerProxyBase_Reset_m3397845475_MetadataUsageId;

// PlayMakerFSM[]
struct PlayMakerFSMU5BU5D_t623924777  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PlayMakerFSM_t437737208 * m_Items[1];

public:
	inline PlayMakerFSM_t437737208 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline PlayMakerFSM_t437737208 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, PlayMakerFSM_t437737208 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline PlayMakerFSM_t437737208 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline PlayMakerFSM_t437737208 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, PlayMakerFSM_t437737208 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t1328083999  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.FsmString[]
struct FsmStringU5BU5D_t2699231328  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmString_t2414474701 * m_Items[1];

public:
	inline FsmString_t2414474701 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmString_t2414474701 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmString_t2414474701 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmString_t2414474701 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmString_t2414474701 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmString_t2414474701 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmBool[]
struct FsmBoolU5BU5D_t3830815681  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmBool_t664485696 * m_Items[1];

public:
	inline FsmBool_t664485696 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmBool_t664485696 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmBool_t664485696 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmBool_t664485696 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmBool_t664485696 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmBool_t664485696 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmFloat[]
struct FsmFloatU5BU5D_t4177556671  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmFloat_t937133978 * m_Items[1];

public:
	inline FsmFloat_t937133978 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmFloat_t937133978 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmFloat_t937133978 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmFloat_t937133978 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmFloat_t937133978 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmFloat_t937133978 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmInt[]
struct FsmIntU5BU5D_t2637547802  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmInt_t1273009179 * m_Items[1];

public:
	inline FsmInt_t1273009179 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmInt_t1273009179 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmInt_t1273009179 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmInt_t1273009179 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmInt_t1273009179 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmInt_t1273009179 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmQuaternion[]
struct FsmQuaternionU5BU5D_t3489263757  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmQuaternion_t878438756 * m_Items[1];

public:
	inline FsmQuaternion_t878438756 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmQuaternion_t878438756 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmQuaternion_t878438756 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmQuaternion_t878438756 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmQuaternion_t878438756 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmQuaternion_t878438756 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVector3[]
struct FsmVector3U5BU5D_t643261629  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVector3_t3996534004 * m_Items[1];

public:
	inline FsmVector3_t3996534004 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmVector3_t3996534004 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmVector3_t3996534004 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmVector3_t3996534004 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmVector3_t3996534004 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmVector3_t3996534004 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmColor[]
struct FsmColorU5BU5D_t4100123680  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmColor_t118301965 * m_Items[1];

public:
	inline FsmColor_t118301965 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmColor_t118301965 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmColor_t118301965 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmColor_t118301965 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmColor_t118301965 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmColor_t118301965 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmVector2[]
struct FsmVector2U5BU5D_t381696854  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmVector2_t2430450063 * m_Items[1];

public:
	inline FsmVector2_t2430450063 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmVector2_t2430450063 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmVector2_t2430450063 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmVector2_t2430450063 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmVector2_t2430450063 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmVector2_t2430450063 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmState[]
struct FsmStateU5BU5D_t1586422282  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmState_t1643911659 * m_Items[1];

public:
	inline FsmState_t1643911659 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmState_t1643911659 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmState_t1643911659 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmState_t1643911659 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmState_t1643911659 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmState_t1643911659 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmEvent[]
struct FsmEventU5BU5D_t287863993  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmEvent_t1258573736 * m_Items[1];

public:
	inline FsmEvent_t1258573736 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmEvent_t1258573736 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmEvent_t1258573736 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmEvent_t1258573736 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmEvent_t1258573736 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmEvent_t1258573736 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// HutongGames.PlayMaker.FsmTransition[]
struct FsmTransitionU5BU5D_t1091630918  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmTransition_t1534990431 * m_Items[1];

public:
	inline FsmTransition_t1534990431 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmTransition_t1534990431 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmTransition_t1534990431 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmTransition_t1534990431 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmTransition_t1534990431 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmTransition_t1534990431 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_t2020392075  m_Items[1];

public:
	inline Color_t2020392075  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2020392075 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2020392075  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2020392075  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2020392075 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2020392075  value)
	{
		m_Items[index] = value;
	}
};
// HutongGames.PlayMaker.FsmStateAction[]
struct FsmStateActionU5BU5D_t1497896004  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FsmStateAction_t2862378169 * m_Items[1];

public:
	inline FsmStateAction_t2862378169 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FsmStateAction_t2862378169 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FsmStateAction_t2862378169 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FsmStateAction_t2862378169 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FsmStateAction_t2862378169 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FsmStateAction_t2862378169 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void PlayMakerFSM::AddEventHandlerComponent<System.Object>(UnityEngine.HideFlags)
extern "C"  void PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared (PlayMakerFSM_t437737208 * __this, int32_t ___hide0, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3946215804_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1__ctor_m2848015482_gshared (List_1_t2058570427 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m2929820459_gshared (Comparison_1_t3951188146 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<!0>)
extern "C"  void List_1_Sort_m2895170076_gshared (List_1_t2058570427 * __this, Comparison_1_t3951188146 * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m3537433232_gshared (List_1_t2058570427 * __this, Il2CppObject* p0, const MethodInfo* method);
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponents_TisIl2CppObject_m2054297814_gshared (Component_t3819376471 * __this, const MethodInfo* method);

// System.Boolean PlayMakerFSM::get_Active()
extern "C"  bool PlayMakerFSM_get_Active_m2477371780 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C"  Fsm_t917886356 * PlayMakerFSM_get_Fsm_m3359411247 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorMove()
extern "C"  bool Fsm_get_HandleAnimatorMove_m1712909480 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnAnimatorMove()
extern "C"  void Fsm_OnAnimatorMove_m169525216 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerProxyBase::.ctor()
extern "C"  void PlayMakerProxyBase__ctor_m547361492 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleApplicationEvents()
extern "C"  bool Fsm_get_HandleApplicationEvents_m1537621535 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationFocus()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ApplicationFocus_m209182743 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(HutongGames.PlayMaker.FsmEvent)
extern "C"  void Fsm_Event_m4079224475 (Fsm_t917886356 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationPause()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ApplicationPause_m3384135749 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter()
extern "C"  bool Fsm_get_HandleCollisionEnter_m1456631574 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionEnter_m809557319 (Fsm_t917886356 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionEnter2D()
extern "C"  bool Fsm_get_HandleCollisionEnter2D_m1401630576 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionEnter2D_m1740000423 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit()
extern "C"  bool Fsm_get_HandleCollisionExit_m3450142898 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionExit_m2954176683 (Fsm_t917886356 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionExit2D()
extern "C"  bool Fsm_get_HandleCollisionExit2D_m3119743400 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionExit2D_m1642132107 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay()
extern "C"  bool Fsm_get_HandleCollisionStay_m1380478791 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay(UnityEngine.Collision)
extern "C"  void Fsm_OnCollisionStay_m3817365356 (Fsm_t917886356 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleCollisionStay2D()
extern "C"  bool Fsm_get_HandleCollisionStay2D_m226828777 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void Fsm_OnCollisionStay2D_m1881561292 (Fsm_t917886356 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleControllerColliderHit()
extern "C"  bool Fsm_get_HandleControllerColliderHit_m3942844253 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void Fsm_OnControllerColliderHit_m450115771 (Fsm_t917886356 * __this, ControllerColliderHit_t4070855101 * ___collider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleFixedUpdate()
extern "C"  bool Fsm_get_HandleFixedUpdate_m2387851797 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::FixedUpdate()
extern "C"  void Fsm_FixedUpdate_m4175912806 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PlayMakerFSM>::GetEnumerator()
#define List_1_GetEnumerator_m4042834880(__this, method) ((  Enumerator_t3636555310  (*) (List_1_t4101825636 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::get_Current()
#define Enumerator_get_Current_m4187226974(__this, method) ((  PlayMakerFSM_t437737208 * (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerFSM::get_FsmName()
extern "C"  String_t* PlayMakerFSM_get_FsmName_m2713674740 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::MoveNext()
#define Enumerator_MoveNext_m3092022352(__this, method) ((  bool (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<PlayMakerFSM>::Dispose()
#define Enumerator_Dispose_m3206985816(__this, method) ((  void (*) (Enumerator_t3636555310 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void HutongGames.PlayMaker.Fsm::.ctor()
extern "C"  void Fsm__ctor_m1745524161 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Reset(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Reset_m1808960153 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::Initialize()
extern "C"  void PlayMakerGlobals_Initialize_m3494352383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern "C"  bool PlayMakerGlobals_get_IsEditor_m2851556513 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmLog::set_LoggingEnabled(System.Boolean)
extern "C"  void FsmLog_set_LoggingEnabled_m2851754425 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Init()
extern "C"  void PlayMakerFSM_Init_m2906566981 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::InitTemplate()
extern "C"  void PlayMakerFSM_InitTemplate_m3833177745 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::InitFsm()
extern "C"  void PlayMakerFSM_InitFsm_m3268071385 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Preprocess(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Preprocess_m297661774 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponents()
extern "C"  void PlayMakerFSM_AddEventHandlerComponents_m3901157882 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m4091950718 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Preprocessed(System.Boolean)
extern "C"  void Fsm_set_Preprocessed_m1718706670 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Init(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Init_m1264563958 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Preprocessed()
extern "C"  bool Fsm_get_Preprocessed_m4033536981 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Name()
extern "C"  String_t* Fsm_get_Name_m2639317356 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ShowStateLabel()
extern "C"  bool Fsm_get_ShowStateLabel_m3886429386 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.Fsm::get_Variables()
extern "C"  FsmVariables_t630687169 * Fsm_get_Variables_m738201045 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::.ctor(HutongGames.PlayMaker.Fsm,HutongGames.PlayMaker.FsmVariables)
extern "C"  void Fsm__ctor_m2872900866 (Fsm_t917886356 * __this, Fsm_t917886356 * ___source0, FsmVariables_t630687169 * ___overrideVariables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Name(System.String)
extern "C"  void Fsm_set_Name_m629262121 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_UsedInTemplate(FsmTemplate)
extern "C"  void Fsm_set_UsedInTemplate_m2342808710 (Fsm_t917886356 * __this, FsmTemplate_t1285897084 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_ShowStateLabel(System.Boolean)
extern "C"  void Fsm_set_ShowStateLabel_m2790468581 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::Reset()
extern "C"  void PlayMakerFSM_Reset_m222573198 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m1796096907 (Behaviour_t955675639 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmUtility::GetFullFsmLabel(HutongGames.PlayMaker.Fsm)
extern "C"  String_t* FsmUtility_GetFullFsmLabel_m4134766633 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_MouseEvents()
extern "C"  bool Fsm_get_MouseEvents_m3020071934 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerMouseEvents>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t3625748060_m770497949(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionEnter>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t204200476_m4106104525(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionExit>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3953531962_m4026045093(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionStay>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2306635791_m1946610428(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter()
extern "C"  bool Fsm_get_HandleTriggerEnter_m790156034 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerEnter>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2991464208_m2851453031(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit()
extern "C"  bool Fsm_get_HandleTriggerExit_m362350026 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerExit>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2606889942_m3150703343(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay()
extern "C"  bool Fsm_get_HandleTriggerStay_m3688276885 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerStay>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t2768254945_m307590968(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionEnter2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t422695966_m922427891(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionExit2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2560737500_m2696690659(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerCollisionStay2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t4047738741_m3163478950(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerEnter2D()
extern "C"  bool Fsm_get_HandleTriggerEnter2D_m172380676 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerEnter2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t2208085914_m1354435365(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerExit2D()
extern "C"  bool Fsm_get_HandleTriggerExit2D_m766085676 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerExit2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t1071223868_m163592089(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleTriggerStay2D()
extern "C"  bool Fsm_get_HandleTriggerStay2D_m2610463627 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerTriggerStay2D>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t3506020163_m241445942(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleParticleCollision()
extern "C"  bool Fsm_get_HandleParticleCollision_m2116271836 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerParticleCollision>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t1230092432_m3673912193(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerControllerColliderHit>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t182605873_m313356344(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak()
extern "C"  bool Fsm_get_HandleJointBreak_m3453435899 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerJointBreak>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t1658177799_m1469739734(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleJointBreak2D()
extern "C"  bool Fsm_get_HandleJointBreak2D_m466287097 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerFixedUpdate>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t960084629_m2689148394(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleOnGUI()
extern "C"  bool Fsm_get_HandleOnGUI_m1667477422 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<PlayMakerOnGUI>()
#define Component_GetComponent_TisPlayMakerOnGUI_t2031863694_m2511685017(__this, method) ((  PlayMakerOnGUI_t2031863694 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<PlayMakerOnGUI>()
#define GameObject_AddComponent_TisPlayMakerOnGUI_t2031863694_m1458714166(__this, method) ((  PlayMakerOnGUI_t2031863694 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3946215804_gshared)(__this, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerApplicationEvents>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t3759624703_m404494050(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerAnimatorMove>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t3213404038_m1404375513(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Boolean HutongGames.PlayMaker.Fsm::get_HandleAnimatorIK()
extern "C"  bool Fsm_get_HandleAnimatorIK_m2492200159 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::AddEventHandlerComponent<PlayMakerAnimatorIK>(UnityEngine.HideFlags)
#define PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t1460122143_m1385004492(__this, ___hide0, method) ((  void (*) (PlayMakerFSM_t437737208 *, int32_t, const MethodInfo*))PlayMakerFSM_AddEventHandlerComponent_TisIl2CppObject_m2974529333_gshared)(__this, ___hide0, method)
// System.Void HutongGames.PlayMaker.Fsm::Clear(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_Clear_m341686841 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___component0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor(HutongGames.PlayMaker.FsmVariables)
extern "C"  void FsmVariables__ctor_m899263547 (FsmVariables_t630687169 * __this, FsmVariables_t630687169 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void Fsm_set_Variables_m694063032 (Fsm_t917886356 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUITexture>()
#define Component_GetComponent_TisGUITexture_t1909122990_m3001290792(__this, method) ((  GUITexture_t1909122990 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void PlayMakerFSM::set_GuiTexture(UnityEngine.GUITexture)
extern "C"  void PlayMakerFSM_set_GuiTexture_m2916730005 (PlayMakerFSM_t437737208 * __this, GUITexture_t1909122990 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.GUIText>()
#define Component_GetComponent_TisGUIText_t2411476300_m3620974608(__this, method) ((  GUIText_t2411476300 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void PlayMakerFSM::set_GuiText(UnityEngine.GUIText)
extern "C"  void PlayMakerFSM_set_GuiText_m190415149 (PlayMakerFSM_t437737208 * __this, GUIText_t2411476300 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Started()
extern "C"  bool Fsm_get_Started_m2972087901 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Start()
extern "C"  void Fsm_Start_m2000538737 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::Add(!0)
#define List_1_Add_m1863766553(__this, p0, method) ((  void (*) (List_1_t4101825636 *, PlayMakerFSM_t437737208 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Void HutongGames.PlayMaker.Fsm::OnEnable()
extern "C"  void Fsm_OnEnable_m1500513457 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Finished()
extern "C"  bool Fsm_get_Finished_m3585713968 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_ManualUpdate()
extern "C"  bool Fsm_get_ManualUpdate_m3049553031 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Update()
extern "C"  void Fsm_Update_m1801256204 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::set_GlobalVariablesSynced(System.Boolean)
extern "C"  void FsmVariables_set_GlobalVariablesSynced_m493809710 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::LateUpdate()
extern "C"  void Fsm_LateUpdate_m459470616 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM/<DoCoroutine>d__1::.ctor(System.Int32)
extern "C"  void U3CDoCoroutineU3Ed__1__ctor_m3023621983 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<PlayMakerFSM>::Remove(!0)
#define List_1_Remove_m3437895080(__this, p0, method) ((  bool (*) (List_1_t4101825636 *, PlayMakerFSM_t437737208 *, const MethodInfo*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Void HutongGames.PlayMaker.Fsm::OnDisable()
extern "C"  void Fsm_OnDisable_m2956643674 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDestroy()
extern "C"  void Fsm_OnDestroy_m940690992 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ApplicationQuit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ApplicationQuit_m409540578 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnDrawGizmos()
extern "C"  void Fsm_OnDrawGizmos_m1898914053 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::SetState(System.String)
extern "C"  void Fsm_SetState_m2915332810 (Fsm_t917886356 * __this, String_t* ___stateName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::Event(System.String)
extern "C"  void Fsm_Event_m603178779 (Fsm_t917886356 * __this, String_t* ___fsmEventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2802126737 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::GetFsmEvent(System.String)
extern "C"  FsmEvent_t1258573736 * FsmEvent_GetFsmEvent_m128779372 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_BroadcastEvent_m583362320 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern "C"  List_1_t4101825636 * PlayMakerFSM_get_FsmList_m5507309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m197216727(__this, p0, method) ((  void (*) (List_1_t4101825636 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2848015482_gshared)(__this, p0, method)
// System.Void HutongGames.PlayMaker.Fsm::ProcessEvent(HutongGames.PlayMaker.FsmEvent,HutongGames.PlayMaker.FsmEventData)
extern "C"  void Fsm_ProcessEvent_m3610580600 (Fsm_t917886356 * __this, FsmEvent_t1258573736 * ___fsmEvent0, FsmEventData_t2110469976 * ___eventData1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameVisible()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_BecameVisible_m1001535112 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_BecameInvisible()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_BecameInvisible_m877332285 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_PlayerConnected()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_PlayerConnected_m1570007781 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ServerInitialized()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ServerInitialized_m558810168 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_ConnectedToServer()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_ConnectedToServer_m2901903686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_PlayerDisconnected()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_PlayerDisconnected_m1895460251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_DisconnectedFromServer()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_DisconnectedFromServer_m700824159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_FailedToConnect()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_FailedToConnect_m1429252691 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_NetworkInstantiate()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_NetworkInstantiate_m2294396313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmVariables::get_GlobalVariablesSynced()
extern "C"  bool FsmVariables_get_GlobalVariablesSynced_m2538480389 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVariables HutongGames.PlayMaker.FsmVariables::get_GlobalVariables()
extern "C"  FsmVariables_t630687169 * FsmVariables_get_GlobalVariables_m583989627 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerFSM::NetworkSyncVariables(UnityEngine.BitStream,HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerFSM_NetworkSyncVariables_m1890220694 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___stream0, FsmVariables_t630687169 * ___variables1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.BitStream::get_isWriting()
extern "C"  bool BitStream_get_isWriting_m3519823449 (BitStream_t1979465639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmString[] HutongGames.PlayMaker.FsmVariables::get_StringVariables()
extern "C"  FsmStringU5BU5D_t2699231328* FsmVariables_get_StringVariables_m3984963863 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.NamedVariable::get_NetworkSync()
extern "C"  bool NamedVariable_get_NetworkSync_m1993448044 (NamedVariable_t3026441313 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmString::get_Value()
extern "C"  String_t* FsmString_get_Value_m3775166715 (FsmString_t2414474701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System.String::ToCharArray()
extern "C"  CharU5BU5D_t1328083999* String_ToCharArray_m870309954 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BitStream::Serialize(System.Int32&)
extern "C"  void BitStream_Serialize_m3312079847 (BitStream_t1979465639 * __this, int32_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BitStream::Serialize(System.Char&)
extern "C"  void BitStream_Serialize_m2742111985 (BitStream_t1979465639 * __this, Il2CppChar* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmBool[] HutongGames.PlayMaker.FsmVariables::get_BoolVariables()
extern "C"  FsmBoolU5BU5D_t3830815681* FsmVariables_get_BoolVariables_m1639951187 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmBool::get_Value()
extern "C"  bool FsmBool_get_Value_m3738134001 (FsmBool_t664485696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BitStream::Serialize(System.Boolean&)
extern "C"  void BitStream_Serialize_m390089021 (BitStream_t1979465639 * __this, bool* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmFloat[] HutongGames.PlayMaker.FsmVariables::get_FloatVariables()
extern "C"  FsmFloatU5BU5D_t4177556671* FsmVariables_get_FloatVariables_m3630743291 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HutongGames.PlayMaker.FsmFloat::get_Value()
extern "C"  float FsmFloat_get_Value_m1818441449 (FsmFloat_t937133978 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BitStream::Serialize(System.Single&)
extern "C"  void BitStream_Serialize_m2064522995 (BitStream_t1979465639 * __this, float* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmInt[] HutongGames.PlayMaker.FsmVariables::get_IntVariables()
extern "C"  FsmIntU5BU5D_t2637547802* FsmVariables_get_IntVariables_m2179857115 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmInt::get_Value()
extern "C"  int32_t FsmInt_get_Value_m3705703582 (FsmInt_t1273009179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmQuaternion[] HutongGames.PlayMaker.FsmVariables::get_QuaternionVariables()
extern "C"  FsmQuaternionU5BU5D_t3489263757* FsmVariables_get_QuaternionVariables_m3575404011 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion HutongGames.PlayMaker.FsmQuaternion::get_Value()
extern "C"  Quaternion_t4030073918  FsmQuaternion_get_Value_m2629833107 (FsmQuaternion_t878438756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Quaternion&)
extern "C"  void BitStream_Serialize_m2762694425 (BitStream_t1979465639 * __this, Quaternion_t4030073918 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector3[] HutongGames.PlayMaker.FsmVariables::get_Vector3Variables()
extern "C"  FsmVector3U5BU5D_t643261629* FsmVariables_get_Vector3Variables_m1350627003 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HutongGames.PlayMaker.FsmVector3::get_Value()
extern "C"  Vector3_t2243707580  FsmVector3_get_Value_m4242600139 (FsmVector3_t3996534004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BitStream::Serialize(UnityEngine.Vector3&)
extern "C"  void BitStream_Serialize_m753399111 (BitStream_t1979465639 * __this, Vector3_t2243707580 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmColor[] HutongGames.PlayMaker.FsmVariables::get_ColorVariables()
extern "C"  FsmColorU5BU5D_t4100123680* FsmVariables_get_ColorVariables_m3164034491 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color HutongGames.PlayMaker.FsmColor::get_Value()
extern "C"  Color_t2020392075  FsmColor_get_Value_m687626399 (FsmColor_t118301965 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmVector2[] HutongGames.PlayMaker.FsmVariables::get_Vector2Variables()
extern "C"  FsmVector2U5BU5D_t381696854* FsmVariables_get_Vector2Variables_m3290377243 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 HutongGames.PlayMaker.FsmVector2::get_Value()
extern "C"  Vector2_t2243707579  FsmVector2_get_Value_m2589490767 (FsmVector2_t2430450063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::CreateString(System.Char[])
extern "C"  String_t* String_CreateString_m3818307083 (String_t* __this, CharU5BU5D_t1328083999* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmString::set_Value(System.String)
extern "C"  void FsmString_set_Value_m1767060322 (FsmString_t2414474701 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmBool::set_Value(System.Boolean)
extern "C"  void FsmBool_set_Value_m2522230142 (FsmBool_t664485696 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmFloat::set_Value(System.Single)
extern "C"  void FsmFloat_set_Value_m3447553958 (FsmFloat_t937133978 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmInt::set_Value(System.Int32)
extern "C"  void FsmInt_set_Value_m4097648685 (FsmInt_t1273009179 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmQuaternion::set_Value(UnityEngine.Quaternion)
extern "C"  void FsmQuaternion_set_Value_m1061544654 (FsmQuaternion_t878438756 * __this, Quaternion_t4030073918  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector3::set_Value(UnityEngine.Vector3)
extern "C"  void FsmVector3_set_Value_m1785770740 (FsmVector3_t3996534004 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmColor::set_Value(UnityEngine.Color)
extern "C"  void FsmColor_set_Value_m92994086 (FsmColor_t118301965 * __this, Color_t2020392075  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVector2::set_Value(UnityEngine.Vector2)
extern "C"  void FsmVector2_set_Value_m1294693978 (FsmVector2_t2430450063 * __this, Vector2_t2243707579  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MasterServerEvent()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MasterServerEvent_m2238530620 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Owner(UnityEngine.MonoBehaviour)
extern "C"  void Fsm_set_Owner_m3780371132 (Fsm_t917886356 * __this, MonoBehaviour_t1158329972 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.Fsm::get_Description()
extern "C"  String_t* Fsm_get_Description_m204017195 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_Description(System.String)
extern "C"  void Fsm_set_Description_m2175047222 (Fsm_t917886356 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.Fsm::get_Active()
extern "C"  bool Fsm_get_Active_m1413219028 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_ActiveState()
extern "C"  FsmState_t1643911659 * Fsm_get_ActiveState_m1540121605 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String HutongGames.PlayMaker.FsmState::get_Name()
extern "C"  String_t* FsmState_get_Name_m2457110913 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState[] HutongGames.PlayMaker.Fsm::get_States()
extern "C"  FsmStateU5BU5D_t1586422282* Fsm_get_States_m1003125214 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent[] HutongGames.PlayMaker.Fsm::get_Events()
extern "C"  FsmEventU5BU5D_t287863993* Fsm_get_Events_m465635212 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmTransition[] HutongGames.PlayMaker.Fsm::get_GlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t1091630918* Fsm_get_GlobalTransitions_m387686343 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_Initialized()
extern "C"  bool PlayMakerGlobals_get_Initialized_m2540978452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::InitData()
extern "C"  void Fsm_InitData_m3521367119 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::.ctor()
#define List_1__ctor_m2230797533(__this, method) ((  void (*) (List_1_t4101825636 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PushFsm(HutongGames.PlayMaker.Fsm)
extern "C"  void FsmExecutionStack_PushFsm_m1873564309 (Il2CppObject * __this /* static, unused */, Fsm_t917886356 * ___executingFsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmExecutionStack::PopFsm()
extern "C"  void FsmExecutionStack_PopFsm_m3490719728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m2474583393 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlayingInEditor_m2559612605 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern "C"  bool PlayMakerGlobals_get_IsBuilding_m3989868652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlaying_m3897118763 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsEditor_m1340019058 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C"  void PlayMakerGlobals_InitApplicationFlags_m1473070140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t1021602117 * Resources_Load_m243305716 (Il2CppObject * __this /* static, unused */, String_t* p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern "C"  bool PlayMakerGlobals_get_IsPlayingInEditor_m2116337626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.ScriptableObject::CreateInstance<PlayMakerGlobals>()
#define ScriptableObject_CreateInstance_TisPlayMakerGlobals_t2120229426_m3104673918(__this /* static, unused */, method) ((  PlayMakerGlobals_t2120229426 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared)(__this /* static, unused */, method)
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerGlobals_set_Variables_m4203227634 (PlayMakerGlobals_t2120229426 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C"  List_1_t1398341365 * PlayMakerGlobals_get_Events_m2654294232 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1__ctor_m596405649(__this, p0, method) ((  void (*) (List_1_t1398341365 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m2848015482_gshared)(__this, p0, method)
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C"  void PlayMakerGlobals_set_Events_m1743941641 (PlayMakerGlobals_t2120229426 * __this, List_1_t1398341365 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern "C"  void PlayMakerGlobals_set_Initialized_m2688991965 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m4061286785(__this, p0, method) ((  void (*) (List_1_t1398341365 *, String_t*, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::FindEvent(System.String)
extern "C"  FsmEvent_t1258573736 * FsmEvent_FindEvent_m131869239 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmEvent::set_IsGlobal(System.Boolean)
extern "C"  void FsmEvent_set_IsGlobal_m2485871150 (FsmEvent_t1258573736 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.FsmVariables::.ctor()
extern "C"  void FsmVariables__ctor_m4065964934 (FsmVariables_t630687169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m3854603248(__this, method) ((  void (*) (List_1_t1398341365 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::InitInstance()
extern "C"  void PlayMakerGUI_InitInstance_m2322412903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m4079055610 (Behaviour_t955675639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t1021602117 * Object_FindObjectOfType_m2330404063 (Il2CppObject * __this /* static, unused */, Type_t * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m962601984 (GameObject_t1756533147 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<PlayMakerGUI>()
#define GameObject_AddComponent_TisPlayMakerGUI_t2662579489_m366838623(__this, method) ((  PlayMakerGUI_t2662579489 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3946215804_gshared)(__this, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m3598323350 (Texture2D_t3542995729 * __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m3987539815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Texture2D_SetPixel_m609991514 (Texture2D_t3542995729 * __this, int32_t p0, int32_t p1, Color_t2020392075  p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m3543341930 (Texture2D_t3542995729 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::.ctor()
extern "C"  void GUIStyle__ctor_m3665892801 (GUIStyle_t1799908754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyleState UnityEngine.GUIStyle::get_normal()
extern "C"  GUIStyleState_t3801000545 * GUIStyle_get_normal_m2789468942 (GUIStyle_t1799908754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_background(UnityEngine.Texture2D)
extern "C"  void GUIStyleState_set_background_m3931679679 (GUIStyleState_t3801000545 * __this, Texture2D_t3542995729 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
extern "C"  void GUIStyleState_set_textColor_m3970174237 (GUIStyleState_t3801000545 * __this, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C"  void GUIStyle_set_fontSize_m4015341543 (GUIStyle_t1799908754 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C"  void GUIStyle_set_alignment_m1024943876 (GUIStyle_t1799908754 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void RectOffset__ctor_m4133355596 (RectOffset_t3387826427 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyle::set_padding(UnityEngine.RectOffset)
extern "C"  void GUIStyle_set_padding_m3722809255 (GUIStyle_t1799908754 * __this, RectOffset_t3387826427 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::Clear()
#define List_1_Clear_m3348543408(__this, method) ((  void (*) (List_1_t4101825636 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<PlayMakerFSM>::get_Count()
#define List_1_get_Count_m3488027205(__this, method) ((  int32_t (*) (List_1_t4101825636 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<PlayMakerFSM>::get_Item(System.Int32)
#define List_1_get_Item_m3632092958(__this, p0, method) ((  PlayMakerFSM_t437737208 * (*) (List_1_t4101825636 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Void System.Comparison`1<PlayMakerFSM>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m3162477570(__this, p0, p1, method) ((  void (*) (Comparison_1_t1699476059 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m2929820459_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::Sort(System.Comparison`1<!0>)
#define List_1_Sort_m2622171247(__this, p0, method) ((  void (*) (List_1_t4101825636 *, Comparison_1_t1699476059 *, const MethodInfo*))List_1_Sort_m2895170076_gshared)(__this, p0, method)
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern "C"  void PlayMakerGUI_DrawStateLabel_m1328883389 (PlayMakerGUI_t2662579489 * __this, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::InitLabelStyle()
extern "C"  void PlayMakerGUI_InitLabelStyle_m2898018055 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern "C"  String_t* PlayMakerGUI_GenerateStateLabel_m2774892031 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_text(System.String)
extern "C"  void GUIContent_set_text_m1170206441 (GUIContent_t4210063000 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUIStyle::CalcSize(UnityEngine.GUIContent)
extern "C"  Vector2_t2243707579  GUIStyle_CalcSize_m4254746879 (GUIStyle_t1799908754 * __this, GUIContent_t4210063000 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m2354025655 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C"  GUITexture_t1909122990 * PlayMakerFSM_get_GuiTexture_m2496936866 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUITexture::get_pixelInset()
extern "C"  Rect_t3681755626  GUITexture_get_pixelInset_m1273695445 (GUITexture_t1909122990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m1393582490 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m1393582395 (Rect_t3681755626 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C"  GUIText_t2411476300 * PlayMakerFSM_get_GuiText_m3709088712 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m1859670022 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformPoint_m2648491174 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_WorldToScreenPoint_m638747266 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m1064335535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUI::get_backgroundColor()
extern "C"  Color_t2020392075  GUI_get_backgroundColor_m1228381085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUI::get_color()
extern "C"  Color_t2020392075  GUI_get_color_m1234367343 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HutongGames.PlayMaker.FsmState::get_ColorIndex()
extern "C"  int32_t FsmState_get_ColorIndex_m4175238002 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern "C"  ColorU5BU5D_t672350442* PlayMakerPrefs_get_Colors_m3543534341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_backgroundColor(UnityEngine.Color)
extern "C"  void GUI_set_backgroundColor_m1176185368 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_contentColor(UnityEngine.Color)
extern "C"  void GUI_set_contentColor_m4064322821 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String,UnityEngine.GUIStyle)
extern "C"  void GUI_Label_m2231582000 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  p0, String_t* p1, GUIStyle_t1799908754 * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C"  void GUI_set_color_m3547334264 (Il2CppObject * __this /* static, unused */, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern "C"  void MonoBehaviour_set_useGUILayout_m2666356651 (MonoBehaviour_t1158329972 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern "C"  GUISkin_t1436893342 * PlayMakerGUI_get_GUISkin_m2441732861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
extern "C"  void GUI_set_skin_m3391676555 (Il2CppObject * __this /* static, unused */, GUISkin_t1436893342 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIColor_m3278185336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIBackgroundColor_m2538762796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIContentColor_m1794729359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DoEditGUI()
extern "C"  void PlayMakerGUI_DoEditGUI_m2014605058 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<PlayMakerFSM>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m4240858977(__this, p0, method) ((  void (*) (List_1_t4101825636 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3537433232_gshared)(__this, p0, method)
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C"  void PlayMakerGUI_CallOnGUI_m3101561124 (PlayMakerGUI_t2662579489 * __this, Fsm_t917886356 * ___fsm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm> HutongGames.PlayMaker.Fsm::get_SubFsmList()
extern "C"  List_1_t287007488 * Fsm_get_SubFsmList_m106111173 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>::get_Item(System.Int32)
#define List_1_get_Item_m2255084070(__this, p0, method) ((  Fsm_t917886356 * (*) (List_1_t287007488 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<HutongGames.PlayMaker.Fsm>::get_Count()
#define List_1_get_Count_m2965087059(__this, method) ((  int32_t (*) (List_1_t287007488 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// UnityEngine.Event UnityEngine.Event::get_current()
extern "C"  Event_t3028476042 * Event_get_current_m2901774193 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C"  int32_t Event_get_type_m2426033198 (Event_t3028476042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.GUI::get_matrix()
extern "C"  Matrix4x4_t2933234003  GUI_get_matrix_m976981075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern "C"  Matrix4x4_t2933234003  Matrix4x4_get_identity_m3039560904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_matrix(UnityEngine.Matrix4x4)
extern "C"  void GUI_set_matrix_m3701966918 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern "C"  Texture_t2243626319 * PlayMakerGUI_get_MouseCursor_m803897773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void GUI_DrawTexture_m1191587896 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  p0, Texture_t2243626319 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern "C"  bool PlayMakerGUI_get_EnableStateLabels_m3131360880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::DrawStateLabels()
extern "C"  void PlayMakerGUI_DrawStateLabels_m1092711642 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern "C"  void PlayMakerGUI_set_GUIMatrix_m1989428149 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_LockCursor()
extern "C"  bool PlayMakerGUI_get_LockCursor_m2699961842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C"  void Cursor_set_lockState_m387168319 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayMakerGUI::get_HideCursor()
extern "C"  bool PlayMakerGUI_get_HideCursor_m1761535307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern "C"  void Cursor_set_visible_m860533511 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmStateAction[] HutongGames.PlayMaker.FsmState::get_Actions()
extern "C"  FsmStateActionU5BU5D_t1497896004* FsmState_get_Actions_m2945510972 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Active()
extern "C"  bool FsmStateAction_get_Active_m2969348681 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmState HutongGames.PlayMaker.Fsm::get_EditState()
extern "C"  FsmState_t1643911659 * Fsm_get_EditState_m1392825861 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmState::get_IsInitialized()
extern "C"  bool FsmState_get_IsInitialized_m297158777 (FsmState_t1643911659 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HutongGames.PlayMaker.FsmStateAction::get_Enabled()
extern "C"  bool FsmStateAction_get_Enabled_m3252509844 (FsmStateAction_t2862378169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::CompareOrdinal(System.String,System.String)
extern "C"  int32_t String_CompareOrdinal_m3421681586 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor()
extern "C"  void GUIContent__ctor_m3889310883 (GUIContent_t4210063000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak(System.Single)
extern "C"  void Fsm_OnJointBreak_m3216038832 (Fsm_t917886356 * __this, float ___breakForce0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  void Fsm_OnJointBreak2D_m2409422232 (Fsm_t917886356 * __this, Joint2D_t854621618 * ___brokenJoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseEnter()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseEnter_m3676296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDown()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseDown_m2455071712 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUp()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseUp_m2558660009 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_LastClickedObject(UnityEngine.GameObject)
extern "C"  void Fsm_set_LastClickedObject_m3675016442 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseUpAsButton()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseUpAsButton_m2531723093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseExit()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseExit_m2613472564 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseDrag()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseDrag_m3004494826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.FsmEvent::get_MouseOver()
extern "C"  FsmEvent_t1258573736 * FsmEvent_get_MouseOver_m4010778446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::set_HandleOnGUI(System.Boolean)
extern "C"  void Fsm_set_HandleOnGUI_m3808117791 (Fsm_t917886356 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerOnGUI::DoEditGUI()
extern "C"  void PlayMakerOnGUI_DoEditGUI_m2385310713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnGUI()
extern "C"  void Fsm_OnGUI_m3260270399 (Fsm_t917886356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void Fsm_OnParticleCollision_m784661054 (Fsm_t917886356 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t1021602117 * Resources_Load_m2041782325 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.ScriptableObject::CreateInstance<PlayMakerPrefs>()
#define ScriptableObject_CreateInstance_TisPlayMakerPrefs_t1833055544_m1276350044(__this /* static, unused */, method) ((  PlayMakerPrefs_t1833055544 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m926060499_gshared)(__this /* static, unused */, method)
// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern "C"  PlayMakerPrefs_t1833055544 * PlayMakerPrefs_get_Instance_m688943076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern "C"  void PlayMakerPrefs_UpdateMinimapColors_m1326282697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t2020392075  Color_get_grey_m1961362537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3811852957 (Color_t2020392075 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayMakerProxyBase::Reset()
extern "C"  void PlayMakerProxyBase_Reset_m3397845475 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponents<PlayMakerFSM>()
#define Component_GetComponents_TisPlayMakerFSM_t437737208_m853540674(__this, method) ((  PlayMakerFSMU5BU5D_t623924777* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m2054297814_gshared)(__this, method)
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerEnter_m2846356605 (Fsm_t917886356 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerEnter2D_m774284857 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerExit_m3786251381 (Fsm_t917886356 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerExit2D_m616971321 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay(UnityEngine.Collider)
extern "C"  void Fsm_OnTriggerStay_m3815556686 (Fsm_t917886356 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HutongGames.PlayMaker.Fsm::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void Fsm_OnTriggerStay2D_m1827537318 (Fsm_t917886356 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayMakerAnimatorMove::OnAnimatorMove()
extern "C"  void PlayMakerAnimatorMove_OnAnimatorMove_m1252704242 (PlayMakerAnimatorMove_t3213404038 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleAnimatorMove_m1712909480(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_OnAnimatorMove_m169525216(L_10, /*hidden argument*/NULL);
	}

IL_002d:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_13 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerAnimatorMove::.ctor()
extern "C"  void PlayMakerAnimatorMove__ctor_m3852680609 (PlayMakerAnimatorMove_t3213404038 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerApplicationEvents::OnApplicationFocus()
extern "C"  void PlayMakerApplicationEvents_OnApplicationFocus_m2511040277 (PlayMakerApplicationEvents_t3759624703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerApplicationEvents_OnApplicationFocus_m2511040277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_HandleApplicationEvents_m1537621535(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_ApplicationFocus_m209182743(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerApplicationEvents::OnApplicationPause()
extern "C"  void PlayMakerApplicationEvents_OnApplicationPause_m3698503875 (PlayMakerApplicationEvents_t3759624703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerApplicationEvents_OnApplicationPause_m3698503875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_HandleApplicationEvents_m1537621535(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_ApplicationPause_m3384135749(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerApplicationEvents::.ctor()
extern "C"  void PlayMakerApplicationEvents__ctor_m3882933954 (PlayMakerApplicationEvents_t3759624703 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionEnter::OnCollisionEnter(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionEnter_OnCollisionEnter_m40305123 (PlayMakerCollisionEnter_t204200476 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionEnter_m1456631574(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collision_t2876846408 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionEnter_m809557319(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionEnter::.ctor()
extern "C"  void PlayMakerCollisionEnter__ctor_m3411485885 (PlayMakerCollisionEnter_t204200476 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionEnter2D::OnCollisionEnter2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionEnter2D_OnCollisionEnter2D_m198816409 (PlayMakerCollisionEnter2D_t422695966 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionEnter2D_m1401630576(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collision2D_t1539500754 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionEnter2D_m1740000423(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionEnter2D::.ctor()
extern "C"  void PlayMakerCollisionEnter2D__ctor_m2564502335 (PlayMakerCollisionEnter2D_t422695966 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionExit::OnCollisionExit(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionExit_OnCollisionExit_m342138039 (PlayMakerCollisionExit_t3953531962 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionExit_m3450142898(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collision_t2876846408 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionExit_m2954176683(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionExit::.ctor()
extern "C"  void PlayMakerCollisionExit__ctor_m3708387017 (PlayMakerCollisionExit_t3953531962 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionExit2D::OnCollisionExit2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionExit2D_OnCollisionExit2D_m4121418717 (PlayMakerCollisionExit2D_t2560737500 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionExit2D_m3119743400(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collision2D_t1539500754 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionExit2D_m1642132107(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionExit2D::.ctor()
extern "C"  void PlayMakerCollisionExit2D__ctor_m4147787879 (PlayMakerCollisionExit2D_t2560737500 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionStay::OnCollisionStay(UnityEngine.Collision)
extern "C"  void PlayMakerCollisionStay_OnCollisionStay_m2505937539 (PlayMakerCollisionStay_t2306635791 * __this, Collision_t2876846408 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionStay_m1380478791(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collision_t2876846408 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionStay_m3817365356(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionStay::.ctor()
extern "C"  void PlayMakerCollisionStay__ctor_m508464772 (PlayMakerCollisionStay_t2306635791 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerCollisionStay2D::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void PlayMakerCollisionStay2D_OnCollisionStay2D_m3480195773 (PlayMakerCollisionStay2D_t4047738741 * __this, Collision2D_t1539500754 * ___collisionInfo0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleCollisionStay2D_m226828777(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collision2D_t1539500754 * L_11 = ___collisionInfo0;
		NullCheck(L_10);
		Fsm_OnCollisionStay2D_m1881561292(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerCollisionStay2D::.ctor()
extern "C"  void PlayMakerCollisionStay2D__ctor_m660356446 (PlayMakerCollisionStay2D_t4047738741 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerControllerColliderHit::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void PlayMakerControllerColliderHit_OnControllerColliderHit_m1972322520 (PlayMakerControllerColliderHit_t182605873 * __this, ControllerColliderHit_t4070855101 * ___hitCollider0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleControllerColliderHit_m3942844253(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		ControllerColliderHit_t4070855101 * L_11 = ___hitCollider0;
		NullCheck(L_10);
		Fsm_OnControllerColliderHit_m450115771(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerControllerColliderHit::.ctor()
extern "C"  void PlayMakerControllerColliderHit__ctor_m101360368 (PlayMakerControllerColliderHit_t182605873 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFixedUpdate::FixedUpdate()
extern "C"  void PlayMakerFixedUpdate_FixedUpdate_m535311859 (PlayMakerFixedUpdate_t960084629 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0031;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleFixedUpdate_m2387851797(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002d;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_FixedUpdate_m4175912806(L_10, /*hidden argument*/NULL);
	}

IL_002d:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_13 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerFixedUpdate::.ctor()
extern "C"  void PlayMakerFixedUpdate__ctor_m2011563730 (PlayMakerFixedUpdate_t960084629 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerFSM::get_VersionNotes()
extern "C"  String_t* PlayMakerFSM_get_VersionNotes_m805927392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_VersionNotes_m805927392_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral371857150;
	}
}
// System.String PlayMakerFSM::get_VersionLabel()
extern "C"  String_t* PlayMakerFSM_get_VersionLabel_m1627028815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_VersionLabel_m1627028815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral371857150;
	}
}
// System.Collections.Generic.List`1<PlayMakerFSM> PlayMakerFSM::get_FsmList()
extern "C"  List_1_t4101825636 * PlayMakerFSM_get_FsmList_m5507309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_FsmList_m5507309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = ((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		return L_0;
	}
}
// PlayMakerFSM PlayMakerFSM::FindFsmOnGameObject(UnityEngine.GameObject,System.String)
extern "C"  PlayMakerFSM_t437737208 * PlayMakerFSM_FindFsmOnGameObject_m2273894409 (Il2CppObject * __this /* static, unused */, GameObject_t1756533147 * ___go0, String_t* ___fsmName1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_FindFsmOnGameObject_m2273894409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayMakerFSM_t437737208 * V_0 = NULL;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	Enumerator_t3636555310  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = ((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		Enumerator_t3636555310  L_1 = List_1_GetEnumerator_m4042834880(L_0, /*hidden argument*/List_1_GetEnumerator_m4042834880_MethodInfo_var);
		V_2 = L_1;
	}

IL_000b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0035;
		}

IL_000d:
		{
			PlayMakerFSM_t437737208 * L_2 = Enumerator_get_Current_m4187226974((&V_2), /*hidden argument*/Enumerator_get_Current_m4187226974_MethodInfo_var);
			V_0 = L_2;
			PlayMakerFSM_t437737208 * L_3 = V_0;
			NullCheck(L_3);
			GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_5 = ___go0;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0035;
			}
		}

IL_0023:
		{
			PlayMakerFSM_t437737208 * L_7 = V_0;
			NullCheck(L_7);
			String_t* L_8 = PlayMakerFSM_get_FsmName_m2713674740(L_7, /*hidden argument*/NULL);
			String_t* L_9 = ___fsmName1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_10 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0035;
			}
		}

IL_0031:
		{
			PlayMakerFSM_t437737208 * L_11 = V_0;
			V_1 = L_11;
			IL2CPP_LEAVE(0x50, FINALLY_0040);
		}

IL_0035:
		{
			bool L_12 = Enumerator_MoveNext_m3092022352((&V_2), /*hidden argument*/Enumerator_MoveNext_m3092022352_MethodInfo_var);
			if (L_12)
			{
				goto IL_000d;
			}
		}

IL_003e:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3206985816((&V_2), /*hidden argument*/Enumerator_Dispose_m3206985816_MethodInfo_var);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x50, IL_0050)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_004e:
	{
		return (PlayMakerFSM_t437737208 *)NULL;
	}

IL_0050:
	{
		PlayMakerFSM_t437737208 * L_13 = V_1;
		return L_13;
	}
}
// FsmTemplate PlayMakerFSM::get_FsmTemplate()
extern "C"  FsmTemplate_t1285897084 * PlayMakerFSM_get_FsmTemplate_m3817745221 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		FsmTemplate_t1285897084 * L_0 = __this->get_fsmTemplate_7();
		return L_0;
	}
}
// UnityEngine.GUITexture PlayMakerFSM::get_GuiTexture()
extern "C"  GUITexture_t1909122990 * PlayMakerFSM_get_GuiTexture_m2496936866 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		GUITexture_t1909122990 * L_0 = __this->get_U3CGuiTextureU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_GuiTexture(UnityEngine.GUITexture)
extern "C"  void PlayMakerFSM_set_GuiTexture_m2916730005 (PlayMakerFSM_t437737208 * __this, GUITexture_t1909122990 * ___value0, const MethodInfo* method)
{
	{
		GUITexture_t1909122990 * L_0 = ___value0;
		__this->set_U3CGuiTextureU3Ek__BackingField_9(L_0);
		return;
	}
}
// UnityEngine.GUIText PlayMakerFSM::get_GuiText()
extern "C"  GUIText_t2411476300 * PlayMakerFSM_get_GuiText_m3709088712 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		GUIText_t2411476300 * L_0 = __this->get_U3CGuiTextU3Ek__BackingField_10();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_GuiText(UnityEngine.GUIText)
extern "C"  void PlayMakerFSM_set_GuiText_m190415149 (PlayMakerFSM_t437737208 * __this, GUIText_t2411476300 * ___value0, const MethodInfo* method)
{
	{
		GUIText_t2411476300 * L_0 = ___value0;
		__this->set_U3CGuiTextU3Ek__BackingField_10(L_0);
		return;
	}
}
// System.Boolean PlayMakerFSM::get_DrawGizmos()
extern "C"  bool PlayMakerFSM_get_DrawGizmos_m1038294635 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_DrawGizmos_m1038294635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->get_U3CDrawGizmosU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void PlayMakerFSM::set_DrawGizmos(System.Boolean)
extern "C"  void PlayMakerFSM_set_DrawGizmos_m2543964976 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_set_DrawGizmos_m2543964976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->set_U3CDrawGizmosU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void PlayMakerFSM::Reset()
extern "C"  void PlayMakerFSM_Reset_m222573198 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Reset_m222573198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Fsm_t917886356 * L_1 = (Fsm_t917886356 *)il2cpp_codegen_object_new(Fsm_t917886356_il2cpp_TypeInfo_var);
		Fsm__ctor_m1745524161(L_1, /*hidden argument*/NULL);
		__this->set_fsm_6(L_1);
	}

IL_0013:
	{
		__this->set_fsmTemplate_7((FsmTemplate_t1285897084 *)NULL);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_Reset_m1808960153(L_2, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Awake()
extern "C"  void PlayMakerFSM_Awake_m2941374002 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Awake_m2941374002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_Initialize_m3494352383(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = PlayMakerGlobals_get_IsEditor_m2851556513(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmLog_t3672513366_il2cpp_TypeInfo_var);
		FsmLog_set_LoggingEnabled_m2851754425(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
	}

IL_0012:
	{
		PlayMakerFSM_Init_m2906566981(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Preprocess()
extern "C"  void PlayMakerFSM_Preprocess_m1370866543 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Preprocess_m1370866543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1285897084 * L_0 = __this->get_fsmTemplate_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		PlayMakerFSM_InitTemplate_m3833177745(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		PlayMakerFSM_InitFsm_m3268071385(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_Preprocess_m297661774(L_2, __this, /*hidden argument*/NULL);
		PlayMakerFSM_AddEventHandlerComponents_m3901157882(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Init()
extern "C"  void PlayMakerFSM_Init_m2906566981 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Init_m2906566981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1285897084 * L_0 = __this->get_fsmTemplate_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		PlayMakerFSM_InitTemplate_m3833177745(__this, /*hidden argument*/NULL);
		goto IL_0023;
	}

IL_001d:
	{
		PlayMakerFSM_InitFsm_m3268071385(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		bool L_3 = PlayMakerGlobals_get_IsEditor_m2851556513(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		Fsm_t917886356 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_set_Preprocessed_m1718706670(L_4, (bool)0, /*hidden argument*/NULL);
		__this->set_eventHandlerComponentsAdded_8((bool)0);
	}

IL_003d:
	{
		Fsm_t917886356 * L_5 = __this->get_fsm_6();
		NullCheck(L_5);
		Fsm_Init_m1264563958(L_5, __this, /*hidden argument*/NULL);
		bool L_6 = __this->get_eventHandlerComponentsAdded_8();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		Fsm_t917886356 * L_7 = __this->get_fsm_6();
		NullCheck(L_7);
		bool L_8 = Fsm_get_Preprocessed_m4033536981(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0064;
		}
	}

IL_005e:
	{
		PlayMakerFSM_AddEventHandlerComponents_m3901157882(__this, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void PlayMakerFSM::InitTemplate()
extern "C"  void PlayMakerFSM_InitTemplate_m3833177745 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_InitTemplate_m3833177745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	Fsm_t917886356 * V_4 = NULL;
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Name_m2639317356(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = L_2->get_EnableDebugFlow_34();
		V_1 = L_3;
		Fsm_t917886356 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		bool L_5 = L_4->get_EnableBreakpoints_35();
		V_2 = L_5;
		Fsm_t917886356 * L_6 = __this->get_fsm_6();
		NullCheck(L_6);
		bool L_7 = Fsm_get_ShowStateLabel_m3886429386(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		FsmTemplate_t1285897084 * L_8 = __this->get_fsmTemplate_7();
		NullCheck(L_8);
		Fsm_t917886356 * L_9 = L_8->get_fsm_3();
		Fsm_t917886356 * L_10 = __this->get_fsm_6();
		NullCheck(L_10);
		FsmVariables_t630687169 * L_11 = Fsm_get_Variables_m738201045(L_10, /*hidden argument*/NULL);
		Fsm_t917886356 * L_12 = (Fsm_t917886356 *)il2cpp_codegen_object_new(Fsm_t917886356_il2cpp_TypeInfo_var);
		Fsm__ctor_m2872900866(L_12, L_9, L_11, /*hidden argument*/NULL);
		V_4 = L_12;
		Fsm_t917886356 * L_13 = V_4;
		String_t* L_14 = V_0;
		NullCheck(L_13);
		Fsm_set_Name_m629262121(L_13, L_14, /*hidden argument*/NULL);
		Fsm_t917886356 * L_15 = V_4;
		NullCheck(L_15);
		Fsm_set_UsedInTemplate_m2342808710(L_15, (FsmTemplate_t1285897084 *)NULL, /*hidden argument*/NULL);
		Fsm_t917886356 * L_16 = V_4;
		bool L_17 = V_1;
		NullCheck(L_16);
		L_16->set_EnableDebugFlow_34(L_17);
		Fsm_t917886356 * L_18 = V_4;
		bool L_19 = V_2;
		NullCheck(L_18);
		L_18->set_EnableBreakpoints_35(L_19);
		Fsm_t917886356 * L_20 = V_4;
		bool L_21 = V_3;
		NullCheck(L_20);
		Fsm_set_ShowStateLabel_m2790468581(L_20, L_21, /*hidden argument*/NULL);
		Fsm_t917886356 * L_22 = V_4;
		__this->set_fsm_6(L_22);
		return;
	}
}
// System.Void PlayMakerFSM::InitFsm()
extern "C"  void PlayMakerFSM_InitFsm_m3268071385 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_InitFsm_m3268071385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		PlayMakerFSM_Reset_m222573198(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1003321737, /*hidden argument*/NULL);
		Behaviour_set_enabled_m1796096907(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		return;
	}
}
// System.Void PlayMakerFSM::AddEventHandlerComponents()
extern "C"  void PlayMakerFSM_AddEventHandlerComponents_m3901157882 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_AddEventHandlerComponents_m3901157882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PlayMakerOnGUI_t2031863694 * V_0 = NULL;
	{
		bool L_0 = PlayMakerGlobals_get_IsEditor_m2851556513(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		String_t* L_2 = FsmUtility_GetFullFsmLabel_m4134766633(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3847144474, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		Fsm_t917886356 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		bool L_5 = Fsm_get_MouseEvents_m3020071934(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t3625748060_m770497949(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerMouseEvents_t3625748060_m770497949_MethodInfo_var);
	}

IL_0035:
	{
		Fsm_t917886356 * L_6 = __this->get_fsm_6();
		NullCheck(L_6);
		bool L_7 = Fsm_get_HandleCollisionEnter_m1456631574(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t204200476_m4106104525(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter_t204200476_m4106104525_MethodInfo_var);
	}

IL_0049:
	{
		Fsm_t917886356 * L_8 = __this->get_fsm_6();
		NullCheck(L_8);
		bool L_9 = Fsm_get_HandleCollisionExit_m3450142898(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3953531962_m4026045093(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit_t3953531962_m4026045093_MethodInfo_var);
	}

IL_005d:
	{
		Fsm_t917886356 * L_10 = __this->get_fsm_6();
		NullCheck(L_10);
		bool L_11 = Fsm_get_HandleCollisionStay_m1380478791(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0071;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2306635791_m1946610428(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay_t2306635791_m1946610428_MethodInfo_var);
	}

IL_0071:
	{
		Fsm_t917886356 * L_12 = __this->get_fsm_6();
		NullCheck(L_12);
		bool L_13 = Fsm_get_HandleTriggerEnter_m790156034(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0085;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2991464208_m2851453031(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter_t2991464208_m2851453031_MethodInfo_var);
	}

IL_0085:
	{
		Fsm_t917886356 * L_14 = __this->get_fsm_6();
		NullCheck(L_14);
		bool L_15 = Fsm_get_HandleTriggerExit_m362350026(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0099;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2606889942_m3150703343(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit_t2606889942_m3150703343_MethodInfo_var);
	}

IL_0099:
	{
		Fsm_t917886356 * L_16 = __this->get_fsm_6();
		NullCheck(L_16);
		bool L_17 = Fsm_get_HandleTriggerStay_m3688276885(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00ad;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t2768254945_m307590968(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay_t2768254945_m307590968_MethodInfo_var);
	}

IL_00ad:
	{
		Fsm_t917886356 * L_18 = __this->get_fsm_6();
		NullCheck(L_18);
		bool L_19 = Fsm_get_HandleCollisionEnter2D_m1401630576(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00c1;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t422695966_m922427891(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionEnter2D_t422695966_m922427891_MethodInfo_var);
	}

IL_00c1:
	{
		Fsm_t917886356 * L_20 = __this->get_fsm_6();
		NullCheck(L_20);
		bool L_21 = Fsm_get_HandleCollisionExit2D_m3119743400(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00d5;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2560737500_m2696690659(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionExit2D_t2560737500_m2696690659_MethodInfo_var);
	}

IL_00d5:
	{
		Fsm_t917886356 * L_22 = __this->get_fsm_6();
		NullCheck(L_22);
		bool L_23 = Fsm_get_HandleCollisionStay2D_m226828777(L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00e9;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t4047738741_m3163478950(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerCollisionStay2D_t4047738741_m3163478950_MethodInfo_var);
	}

IL_00e9:
	{
		Fsm_t917886356 * L_24 = __this->get_fsm_6();
		NullCheck(L_24);
		bool L_25 = Fsm_get_HandleTriggerEnter2D_m172380676(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00fd;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t2208085914_m1354435365(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerEnter2D_t2208085914_m1354435365_MethodInfo_var);
	}

IL_00fd:
	{
		Fsm_t917886356 * L_26 = __this->get_fsm_6();
		NullCheck(L_26);
		bool L_27 = Fsm_get_HandleTriggerExit2D_m766085676(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0111;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t1071223868_m163592089(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerExit2D_t1071223868_m163592089_MethodInfo_var);
	}

IL_0111:
	{
		Fsm_t917886356 * L_28 = __this->get_fsm_6();
		NullCheck(L_28);
		bool L_29 = Fsm_get_HandleTriggerStay2D_m2610463627(L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0125;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t3506020163_m241445942(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerTriggerStay2D_t3506020163_m241445942_MethodInfo_var);
	}

IL_0125:
	{
		Fsm_t917886356 * L_30 = __this->get_fsm_6();
		NullCheck(L_30);
		bool L_31 = Fsm_get_HandleParticleCollision_m2116271836(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0139;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t1230092432_m3673912193(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerParticleCollision_t1230092432_m3673912193_MethodInfo_var);
	}

IL_0139:
	{
		Fsm_t917886356 * L_32 = __this->get_fsm_6();
		NullCheck(L_32);
		bool L_33 = Fsm_get_HandleControllerColliderHit_m3942844253(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_014d;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t182605873_m313356344(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerControllerColliderHit_t182605873_m313356344_MethodInfo_var);
	}

IL_014d:
	{
		Fsm_t917886356 * L_34 = __this->get_fsm_6();
		NullCheck(L_34);
		bool L_35 = Fsm_get_HandleJointBreak_m3453435899(L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0161;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t1658177799_m1469739734(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t1658177799_m1469739734_MethodInfo_var);
	}

IL_0161:
	{
		Fsm_t917886356 * L_36 = __this->get_fsm_6();
		NullCheck(L_36);
		bool L_37 = Fsm_get_HandleJointBreak2D_m466287097(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0175;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t1658177799_m1469739734(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerJointBreak_t1658177799_m1469739734_MethodInfo_var);
	}

IL_0175:
	{
		Fsm_t917886356 * L_38 = __this->get_fsm_6();
		NullCheck(L_38);
		bool L_39 = Fsm_get_HandleFixedUpdate_m2387851797(L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0189;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t960084629_m2689148394(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerFixedUpdate_t960084629_m2689148394_MethodInfo_var);
	}

IL_0189:
	{
		Fsm_t917886356 * L_40 = __this->get_fsm_6();
		NullCheck(L_40);
		bool L_41 = Fsm_get_HandleOnGUI_m1667477422(L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_01b7;
		}
	}
	{
		PlayMakerOnGUI_t2031863694 * L_42 = Component_GetComponent_TisPlayMakerOnGUI_t2031863694_m2511685017(__this, /*hidden argument*/Component_GetComponent_TisPlayMakerOnGUI_t2031863694_m2511685017_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_43 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_42, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01b7;
		}
	}
	{
		GameObject_t1756533147 * L_44 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		PlayMakerOnGUI_t2031863694 * L_45 = GameObject_AddComponent_TisPlayMakerOnGUI_t2031863694_m1458714166(L_44, /*hidden argument*/GameObject_AddComponent_TisPlayMakerOnGUI_t2031863694_m1458714166_MethodInfo_var);
		V_0 = L_45;
		PlayMakerOnGUI_t2031863694 * L_46 = V_0;
		NullCheck(L_46);
		L_46->set_playMakerFSM_2(__this);
	}

IL_01b7:
	{
		Fsm_t917886356 * L_47 = __this->get_fsm_6();
		NullCheck(L_47);
		bool L_48 = Fsm_get_HandleApplicationEvents_m1537621535(L_47, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_01cb;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t3759624703_m404494050(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerApplicationEvents_t3759624703_m404494050_MethodInfo_var);
	}

IL_01cb:
	{
		Fsm_t917886356 * L_49 = __this->get_fsm_6();
		NullCheck(L_49);
		bool L_50 = Fsm_get_HandleAnimatorMove_m1712909480(L_49, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01df;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t3213404038_m1404375513(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorMove_t3213404038_m1404375513_MethodInfo_var);
	}

IL_01df:
	{
		Fsm_t917886356 * L_51 = __this->get_fsm_6();
		NullCheck(L_51);
		bool L_52 = Fsm_get_HandleAnimatorIK_m2492200159(L_51, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01f3;
		}
	}
	{
		PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t1460122143_m1385004492(__this, 2, /*hidden argument*/PlayMakerFSM_AddEventHandlerComponent_TisPlayMakerAnimatorIK_t1460122143_m1385004492_MethodInfo_var);
	}

IL_01f3:
	{
		__this->set_eventHandlerComponentsAdded_8((bool)1);
		return;
	}
}
// System.Void PlayMakerFSM::SetFsmTemplate(FsmTemplate)
extern "C"  void PlayMakerFSM_SetFsmTemplate_m648066017 (PlayMakerFSM_t437737208 * __this, FsmTemplate_t1285897084 * ___template0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_SetFsmTemplate_m648066017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1285897084 * L_0 = ___template0;
		__this->set_fsmTemplate_7(L_0);
		Fsm_t917886356 * L_1 = PlayMakerFSM_get_Fsm_m3359411247(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Fsm_Clear_m341686841(L_1, __this, /*hidden argument*/NULL);
		FsmTemplate_t1285897084 * L_2 = ___template0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Fsm_t917886356 * L_4 = PlayMakerFSM_get_Fsm_m3359411247(__this, /*hidden argument*/NULL);
		FsmTemplate_t1285897084 * L_5 = __this->get_fsmTemplate_7();
		NullCheck(L_5);
		Fsm_t917886356 * L_6 = L_5->get_fsm_3();
		NullCheck(L_6);
		FsmVariables_t630687169 * L_7 = Fsm_get_Variables_m738201045(L_6, /*hidden argument*/NULL);
		FsmVariables_t630687169 * L_8 = (FsmVariables_t630687169 *)il2cpp_codegen_object_new(FsmVariables_t630687169_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m899263547(L_8, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Fsm_set_Variables_m694063032(L_4, L_8, /*hidden argument*/NULL);
	}

IL_003c:
	{
		PlayMakerFSM_Init_m2906566981(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Start()
extern "C"  void PlayMakerFSM_Start_m3186900003 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_Start_m3186900003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUITexture_t1909122990 * L_0 = Component_GetComponent_TisGUITexture_t1909122990_m3001290792(__this, /*hidden argument*/Component_GetComponent_TisGUITexture_t1909122990_m3001290792_MethodInfo_var);
		PlayMakerFSM_set_GuiTexture_m2916730005(__this, L_0, /*hidden argument*/NULL);
		GUIText_t2411476300 * L_1 = Component_GetComponent_TisGUIText_t2411476300_m3620974608(__this, /*hidden argument*/Component_GetComponent_TisGUIText_t2411476300_m3620974608_MethodInfo_var);
		PlayMakerFSM_set_GuiText_m190415149(__this, L_1, /*hidden argument*/NULL);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = Fsm_get_Started_m2972087901(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0030;
		}
	}
	{
		Fsm_t917886356 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_Start_m2000538737(L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnEnable()
extern "C"  void PlayMakerFSM_OnEnable_m739653507 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnEnable_m739653507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = ((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		List_1_Add_m1863766553(L_0, __this, /*hidden argument*/List_1_Add_m1863766553_MethodInfo_var);
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		NullCheck(L_1);
		Fsm_OnEnable_m1500513457(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::Update()
extern "C"  void PlayMakerFSM_Update_m2935406016 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Finished_m3585713968(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = Fsm_get_ManualUpdate_m3049553031(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0025;
		}
	}
	{
		Fsm_t917886356 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_Update_m1801256204(L_4, /*hidden argument*/NULL);
	}

IL_0025:
	{
		return;
	}
}
// System.Void PlayMakerFSM::LateUpdate()
extern "C"  void PlayMakerFSM_LateUpdate_m1309297244 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		FsmVariables_set_GlobalVariablesSynced_m493809710(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Finished_m3585713968(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_LateUpdate_m459470616(L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Collections.IEnumerator PlayMakerFSM::DoCoroutine(System.Collections.IEnumerator)
extern "C"  Il2CppObject * PlayMakerFSM_DoCoroutine_m2070243337 (PlayMakerFSM_t437737208 * __this, Il2CppObject * ___routine0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_DoCoroutine_m2070243337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CDoCoroutineU3Ed__1_t4237467219 * V_0 = NULL;
	{
		U3CDoCoroutineU3Ed__1_t4237467219 * L_0 = (U3CDoCoroutineU3Ed__1_t4237467219 *)il2cpp_codegen_object_new(U3CDoCoroutineU3Ed__1_t4237467219_il2cpp_TypeInfo_var);
		U3CDoCoroutineU3Ed__1__ctor_m3023621983(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDoCoroutineU3Ed__1_t4237467219 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		U3CDoCoroutineU3Ed__1_t4237467219 * L_2 = V_0;
		Il2CppObject * L_3 = ___routine0;
		NullCheck(L_2);
		L_2->set_routine_3(L_3);
		U3CDoCoroutineU3Ed__1_t4237467219 * L_4 = V_0;
		return L_4;
	}
}
// System.Void PlayMakerFSM::OnDisable()
extern "C"  void PlayMakerFSM_OnDisable_m1788428118 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDisable_m1788428118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = ((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		List_1_Remove_m3437895080(L_0, __this, /*hidden argument*/List_1_Remove_m3437895080_MethodInfo_var);
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		bool L_3 = Fsm_get_Finished_m3585713968(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_002c;
		}
	}
	{
		Fsm_t917886356 * L_4 = __this->get_fsm_6();
		NullCheck(L_4);
		Fsm_OnDisable_m2956643674(L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnDestroy()
extern "C"  void PlayMakerFSM_OnDestroy_m924941156 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDestroy_m924941156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = ((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->get_fsmList_2();
		NullCheck(L_0);
		List_1_Remove_m3437895080(L_0, __this, /*hidden argument*/List_1_Remove_m3437895080_MethodInfo_var);
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		Fsm_OnDestroy_m940690992(L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnApplicationQuit()
extern "C"  void PlayMakerFSM_OnApplicationQuit_m1000198585 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnApplicationQuit_m1000198585_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_1 = FsmEvent_get_ApplicationQuit_m409540578(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m4079224475(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->set_ApplicationIsQuitting_4((bool)1);
		return;
	}
}
// System.Void PlayMakerFSM::OnDrawGizmos()
extern "C"  void PlayMakerFSM_OnDrawGizmos_m1328164963 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		NullCheck(L_1);
		Fsm_OnDrawGizmos_m1898914053(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerFSM::SetState(System.String)
extern "C"  void PlayMakerFSM_SetState_m3531642678 (PlayMakerFSM_t437737208 * __this, String_t* ___stateName0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___stateName0;
		NullCheck(L_0);
		Fsm_SetState_m2915332810(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::ChangeState(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_ChangeState_m4048370620 (PlayMakerFSM_t437737208 * __this, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		FsmEvent_t1258573736 * L_1 = ___fsmEvent0;
		NullCheck(L_0);
		Fsm_Event_m4079224475(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::ChangeState(System.String)
extern "C"  void PlayMakerFSM_ChangeState_m3369177710 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m603178779(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendEvent(System.String)
extern "C"  void PlayMakerFSM_SendEvent_m2107076673 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m603178779(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendRemoteFsmEvent(System.String)
extern "C"  void PlayMakerFSM_SendRemoteFsmEvent_m2943015349 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		Fsm_Event_m603178779(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::SendRemoteFsmEventWithData(System.String,System.String)
extern "C"  void PlayMakerFSM_SendRemoteFsmEventWithData_m2864874037 (PlayMakerFSM_t437737208 * __this, String_t* ___eventName0, String_t* ___eventData1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_SendRemoteFsmEventWithData_m2864874037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		String_t* L_1 = ___eventData1;
		NullCheck(L_0);
		L_0->set_StringData_8(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		String_t* L_3 = ___eventName0;
		NullCheck(L_2);
		Fsm_Event_m603178779(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::BroadcastEvent(System.String)
extern "C"  void PlayMakerFSM_BroadcastEvent_m1710776222 (Il2CppObject * __this /* static, unused */, String_t* ___fsmEventName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_BroadcastEvent_m1710776222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___fsmEventName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		String_t* L_2 = ___fsmEventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_GetFsmEvent_m128779372(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		PlayMakerFSM_BroadcastEvent_m583362320(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerFSM::BroadcastEvent(HutongGames.PlayMaker.FsmEvent)
extern "C"  void PlayMakerFSM_BroadcastEvent_m583362320 (Il2CppObject * __this /* static, unused */, FsmEvent_t1258573736 * ___fsmEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_BroadcastEvent_m583362320_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4101825636 * V_0 = NULL;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	Enumerator_t3636555310  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = PlayMakerFSM_get_FsmList_m5507309(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t4101825636 * L_1 = (List_1_t4101825636 *)il2cpp_codegen_object_new(List_1_t4101825636_il2cpp_TypeInfo_var);
		List_1__ctor_m197216727(L_1, L_0, /*hidden argument*/List_1__ctor_m197216727_MethodInfo_var);
		V_0 = L_1;
		List_1_t4101825636 * L_2 = V_0;
		NullCheck(L_2);
		Enumerator_t3636555310  L_3 = List_1_GetEnumerator_m4042834880(L_2, /*hidden argument*/List_1_GetEnumerator_m4042834880_MethodInfo_var);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003a;
		}

IL_0014:
		{
			PlayMakerFSM_t437737208 * L_4 = Enumerator_get_Current_m4187226974((&V_2), /*hidden argument*/Enumerator_get_Current_m4187226974_MethodInfo_var);
			V_1 = L_4;
			PlayMakerFSM_t437737208 * L_5 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_6 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (L_6)
			{
				goto IL_003a;
			}
		}

IL_0025:
		{
			PlayMakerFSM_t437737208 * L_7 = V_1;
			NullCheck(L_7);
			Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_003a;
			}
		}

IL_002d:
		{
			PlayMakerFSM_t437737208 * L_9 = V_1;
			NullCheck(L_9);
			Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
			FsmEvent_t1258573736 * L_11 = ___fsmEvent0;
			NullCheck(L_10);
			Fsm_ProcessEvent_m3610580600(L_10, L_11, (FsmEventData_t2110469976 *)NULL, /*hidden argument*/NULL);
		}

IL_003a:
		{
			bool L_12 = Enumerator_MoveNext_m3092022352((&V_2), /*hidden argument*/Enumerator_MoveNext_m3092022352_MethodInfo_var);
			if (L_12)
			{
				goto IL_0014;
			}
		}

IL_0043:
		{
			IL2CPP_LEAVE(0x53, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3206985816((&V_2), /*hidden argument*/Enumerator_Dispose_m3206985816_MethodInfo_var);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0053:
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnBecameVisible()
extern "C"  void PlayMakerFSM_OnBecameVisible_m2356140445 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnBecameVisible_m2356140445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_1 = FsmEvent_get_BecameVisible_m1001535112(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m4079224475(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnBecameInvisible()
extern "C"  void PlayMakerFSM_OnBecameInvisible_m51942020 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnBecameInvisible_m51942020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_1 = FsmEvent_get_BecameInvisible_m877332285(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m4079224475(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnPlayerConnected(UnityEngine.NetworkPlayer)
extern "C"  void PlayMakerFSM_OnPlayerConnected_m1919487554 (PlayMakerFSM_t437737208 * __this, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnPlayerConnected_m1919487554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NetworkPlayer_t1243528291  L_1 = ___player0;
		NullCheck(L_0);
		L_0->set_Player_16(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_get_PlayerConnected_m1570007781(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m4079224475(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnServerInitialized()
extern "C"  void PlayMakerFSM_OnServerInitialized_m897882395 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnServerInitialized_m897882395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_1 = FsmEvent_get_ServerInitialized_m558810168(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m4079224475(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnConnectedToServer()
extern "C"  void PlayMakerFSM_OnConnectedToServer_m1688810999 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnConnectedToServer_m1688810999_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_1 = FsmEvent_get_ConnectedToServer_m2901903686(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Fsm_Event_m4079224475(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnPlayerDisconnected(UnityEngine.NetworkPlayer)
extern "C"  void PlayMakerFSM_OnPlayerDisconnected_m1152869810 (PlayMakerFSM_t437737208 * __this, NetworkPlayer_t1243528291  ___player0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnPlayerDisconnected_m1152869810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NetworkPlayer_t1243528291  L_1 = ___player0;
		NullCheck(L_0);
		L_0->set_Player_16(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_get_PlayerDisconnected_m1895460251(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m4079224475(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnDisconnectedFromServer(UnityEngine.NetworkDisconnection)
extern "C"  void PlayMakerFSM_OnDisconnectedFromServer_m3587986053 (PlayMakerFSM_t437737208 * __this, int32_t ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnDisconnectedFromServer_m3587986053_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		int32_t L_1 = ___info0;
		NullCheck(L_0);
		L_0->set_DisconnectionInfo_17(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_get_DisconnectedFromServer_m700824159(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m4079224475(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnFailedToConnect(UnityEngine.NetworkConnectionError)
extern "C"  void PlayMakerFSM_OnFailedToConnect_m3759082883 (PlayMakerFSM_t437737208 * __this, int32_t ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnFailedToConnect_m3759082883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		int32_t L_1 = ___error0;
		NullCheck(L_0);
		L_0->set_ConnectionError_18(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_get_FailedToConnect_m1429252691(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m4079224475(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnNetworkInstantiate(UnityEngine.NetworkMessageInfo)
extern "C"  void PlayMakerFSM_OnNetworkInstantiate_m1787303400 (PlayMakerFSM_t437737208 * __this, NetworkMessageInfo_t614064059  ___info0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnNetworkInstantiate_m1787303400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		NetworkMessageInfo_t614064059  L_1 = ___info0;
		NullCheck(L_0);
		L_0->set_NetworkMessageInfo_19(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_get_NetworkInstantiate_m2294396313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m4079224475(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::OnSerializeNetworkView(UnityEngine.BitStream,UnityEngine.NetworkMessageInfo)
extern "C"  void PlayMakerFSM_OnSerializeNetworkView_m297962887 (PlayMakerFSM_t437737208 * __this, BitStream_t1979465639 * ___stream0, NetworkMessageInfo_t614064059  ___info1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnSerializeNetworkView_m297962887_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = FsmVariables_get_GlobalVariablesSynced_m2538480389(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		FsmVariables_set_GlobalVariablesSynced_m493809710(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		BitStream_t1979465639 * L_1 = ___stream0;
		FsmVariables_t630687169 * L_2 = FsmVariables_get_GlobalVariables_m583989627(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		PlayMakerFSM_NetworkSyncVariables_m1890220694(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		BitStream_t1979465639 * L_3 = ___stream0;
		Fsm_t917886356 * L_4 = PlayMakerFSM_get_Fsm_m3359411247(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmVariables_t630687169 * L_5 = Fsm_get_Variables_m738201045(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		PlayMakerFSM_NetworkSyncVariables_m1890220694(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::NetworkSyncVariables(UnityEngine.BitStream,HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerFSM_NetworkSyncVariables_m1890220694 (Il2CppObject * __this /* static, unused */, BitStream_t1979465639 * ___stream0, FsmVariables_t630687169 * ___variables1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_NetworkSyncVariables_m1890220694_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmString_t2414474701 * V_0 = NULL;
	CharU5BU5D_t1328083999* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	FsmBool_t664485696 * V_4 = NULL;
	bool V_5 = false;
	FsmFloat_t937133978 * V_6 = NULL;
	float V_7 = 0.0f;
	FsmInt_t1273009179 * V_8 = NULL;
	int32_t V_9 = 0;
	FsmQuaternion_t878438756 * V_10 = NULL;
	Quaternion_t4030073918  V_11;
	memset(&V_11, 0, sizeof(V_11));
	FsmVector3_t3996534004 * V_12 = NULL;
	Vector3_t2243707580  V_13;
	memset(&V_13, 0, sizeof(V_13));
	FsmColor_t118301965 * V_14 = NULL;
	Color_t2020392075  V_15;
	memset(&V_15, 0, sizeof(V_15));
	FsmVector2_t2430450063 * V_16 = NULL;
	Vector2_t2243707579  V_17;
	memset(&V_17, 0, sizeof(V_17));
	FsmString_t2414474701 * V_18 = NULL;
	int32_t V_19 = 0;
	CharU5BU5D_t1328083999* V_20 = NULL;
	int32_t V_21 = 0;
	FsmBool_t664485696 * V_22 = NULL;
	bool V_23 = false;
	FsmFloat_t937133978 * V_24 = NULL;
	float V_25 = 0.0f;
	FsmInt_t1273009179 * V_26 = NULL;
	int32_t V_27 = 0;
	FsmQuaternion_t878438756 * V_28 = NULL;
	Quaternion_t4030073918  V_29;
	memset(&V_29, 0, sizeof(V_29));
	FsmVector3_t3996534004 * V_30 = NULL;
	Vector3_t2243707580  V_31;
	memset(&V_31, 0, sizeof(V_31));
	FsmColor_t118301965 * V_32 = NULL;
	float V_33 = 0.0f;
	float V_34 = 0.0f;
	float V_35 = 0.0f;
	float V_36 = 0.0f;
	FsmVector2_t2430450063 * V_37 = NULL;
	float V_38 = 0.0f;
	float V_39 = 0.0f;
	FsmStringU5BU5D_t2699231328* V_40 = NULL;
	int32_t V_41 = 0;
	FsmBoolU5BU5D_t3830815681* V_42 = NULL;
	int32_t V_43 = 0;
	FsmFloatU5BU5D_t4177556671* V_44 = NULL;
	int32_t V_45 = 0;
	FsmIntU5BU5D_t2637547802* V_46 = NULL;
	int32_t V_47 = 0;
	FsmQuaternionU5BU5D_t3489263757* V_48 = NULL;
	int32_t V_49 = 0;
	FsmVector3U5BU5D_t643261629* V_50 = NULL;
	int32_t V_51 = 0;
	FsmColorU5BU5D_t4100123680* V_52 = NULL;
	int32_t V_53 = 0;
	FsmVector2U5BU5D_t381696854* V_54 = NULL;
	int32_t V_55 = 0;
	FsmStringU5BU5D_t2699231328* V_56 = NULL;
	int32_t V_57 = 0;
	FsmBoolU5BU5D_t3830815681* V_58 = NULL;
	int32_t V_59 = 0;
	FsmFloatU5BU5D_t4177556671* V_60 = NULL;
	{
		BitStream_t1979465639 * L_0 = ___stream0;
		NullCheck(L_0);
		bool L_1 = BitStream_get_isWriting_m3519823449(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0248;
		}
	}
	{
		FsmVariables_t630687169 * L_2 = ___variables1;
		NullCheck(L_2);
		FsmStringU5BU5D_t2699231328* L_3 = FsmVariables_get_StringVariables_m3984963863(L_2, /*hidden argument*/NULL);
		V_40 = L_3;
		V_41 = 0;
		goto IL_005d;
	}

IL_0018:
	{
		FsmStringU5BU5D_t2699231328* L_4 = V_40;
		int32_t L_5 = V_41;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		FsmString_t2414474701 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		FsmString_t2414474701 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = NamedVariable_get_NetworkSync_m1993448044(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0057;
		}
	}
	{
		FsmString_t2414474701 * L_10 = V_0;
		NullCheck(L_10);
		String_t* L_11 = FsmString_get_Value_m3775166715(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		CharU5BU5D_t1328083999* L_12 = String_ToCharArray_m870309954(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		CharU5BU5D_t1328083999* L_13 = V_1;
		NullCheck(L_13);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		BitStream_t1979465639 * L_14 = ___stream0;
		NullCheck(L_14);
		BitStream_Serialize_m3312079847(L_14, (&V_2), /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0053;
	}

IL_0042:
	{
		BitStream_t1979465639 * L_15 = ___stream0;
		CharU5BU5D_t1328083999* L_16 = V_1;
		int32_t L_17 = V_3;
		NullCheck(L_16);
		NullCheck(L_15);
		BitStream_Serialize_m2742111985(L_15, ((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17))), /*hidden argument*/NULL);
		int32_t L_18 = V_3;
		V_3 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_19 = V_3;
		int32_t L_20 = V_2;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0042;
		}
	}

IL_0057:
	{
		int32_t L_21 = V_41;
		V_41 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_005d:
	{
		int32_t L_22 = V_41;
		FsmStringU5BU5D_t2699231328* L_23 = V_40;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		FsmVariables_t630687169 * L_24 = ___variables1;
		NullCheck(L_24);
		FsmBoolU5BU5D_t3830815681* L_25 = FsmVariables_get_BoolVariables_m1639951187(L_24, /*hidden argument*/NULL);
		V_42 = L_25;
		V_43 = 0;
		goto IL_0099;
	}

IL_0072:
	{
		FsmBoolU5BU5D_t3830815681* L_26 = V_42;
		int32_t L_27 = V_43;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		FsmBool_t664485696 * L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_4 = L_29;
		FsmBool_t664485696 * L_30 = V_4;
		NullCheck(L_30);
		bool L_31 = NamedVariable_get_NetworkSync_m1993448044(L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0093;
		}
	}
	{
		FsmBool_t664485696 * L_32 = V_4;
		NullCheck(L_32);
		bool L_33 = FsmBool_get_Value_m3738134001(L_32, /*hidden argument*/NULL);
		V_5 = L_33;
		BitStream_t1979465639 * L_34 = ___stream0;
		NullCheck(L_34);
		BitStream_Serialize_m390089021(L_34, (&V_5), /*hidden argument*/NULL);
	}

IL_0093:
	{
		int32_t L_35 = V_43;
		V_43 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0099:
	{
		int32_t L_36 = V_43;
		FsmBoolU5BU5D_t3830815681* L_37 = V_42;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_0072;
		}
	}
	{
		FsmVariables_t630687169 * L_38 = ___variables1;
		NullCheck(L_38);
		FsmFloatU5BU5D_t4177556671* L_39 = FsmVariables_get_FloatVariables_m3630743291(L_38, /*hidden argument*/NULL);
		V_44 = L_39;
		V_45 = 0;
		goto IL_00d5;
	}

IL_00ae:
	{
		FsmFloatU5BU5D_t4177556671* L_40 = V_44;
		int32_t L_41 = V_45;
		NullCheck(L_40);
		int32_t L_42 = L_41;
		FsmFloat_t937133978 * L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		V_6 = L_43;
		FsmFloat_t937133978 * L_44 = V_6;
		NullCheck(L_44);
		bool L_45 = NamedVariable_get_NetworkSync_m1993448044(L_44, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_00cf;
		}
	}
	{
		FsmFloat_t937133978 * L_46 = V_6;
		NullCheck(L_46);
		float L_47 = FsmFloat_get_Value_m1818441449(L_46, /*hidden argument*/NULL);
		V_7 = L_47;
		BitStream_t1979465639 * L_48 = ___stream0;
		NullCheck(L_48);
		BitStream_Serialize_m2064522995(L_48, (&V_7), /*hidden argument*/NULL);
	}

IL_00cf:
	{
		int32_t L_49 = V_45;
		V_45 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_50 = V_45;
		FsmFloatU5BU5D_t4177556671* L_51 = V_44;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_51)->max_length)))))))
		{
			goto IL_00ae;
		}
	}
	{
		FsmVariables_t630687169 * L_52 = ___variables1;
		NullCheck(L_52);
		FsmIntU5BU5D_t2637547802* L_53 = FsmVariables_get_IntVariables_m2179857115(L_52, /*hidden argument*/NULL);
		V_46 = L_53;
		V_47 = 0;
		goto IL_0111;
	}

IL_00ea:
	{
		FsmIntU5BU5D_t2637547802* L_54 = V_46;
		int32_t L_55 = V_47;
		NullCheck(L_54);
		int32_t L_56 = L_55;
		FsmInt_t1273009179 * L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		V_8 = L_57;
		FsmInt_t1273009179 * L_58 = V_8;
		NullCheck(L_58);
		bool L_59 = NamedVariable_get_NetworkSync_m1993448044(L_58, /*hidden argument*/NULL);
		if (!L_59)
		{
			goto IL_010b;
		}
	}
	{
		FsmInt_t1273009179 * L_60 = V_8;
		NullCheck(L_60);
		int32_t L_61 = FsmInt_get_Value_m3705703582(L_60, /*hidden argument*/NULL);
		V_9 = L_61;
		BitStream_t1979465639 * L_62 = ___stream0;
		NullCheck(L_62);
		BitStream_Serialize_m3312079847(L_62, (&V_9), /*hidden argument*/NULL);
	}

IL_010b:
	{
		int32_t L_63 = V_47;
		V_47 = ((int32_t)((int32_t)L_63+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_64 = V_47;
		FsmIntU5BU5D_t2637547802* L_65 = V_46;
		NullCheck(L_65);
		if ((((int32_t)L_64) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_65)->max_length)))))))
		{
			goto IL_00ea;
		}
	}
	{
		FsmVariables_t630687169 * L_66 = ___variables1;
		NullCheck(L_66);
		FsmQuaternionU5BU5D_t3489263757* L_67 = FsmVariables_get_QuaternionVariables_m3575404011(L_66, /*hidden argument*/NULL);
		V_48 = L_67;
		V_49 = 0;
		goto IL_014d;
	}

IL_0126:
	{
		FsmQuaternionU5BU5D_t3489263757* L_68 = V_48;
		int32_t L_69 = V_49;
		NullCheck(L_68);
		int32_t L_70 = L_69;
		FsmQuaternion_t878438756 * L_71 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		V_10 = L_71;
		FsmQuaternion_t878438756 * L_72 = V_10;
		NullCheck(L_72);
		bool L_73 = NamedVariable_get_NetworkSync_m1993448044(L_72, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_0147;
		}
	}
	{
		FsmQuaternion_t878438756 * L_74 = V_10;
		NullCheck(L_74);
		Quaternion_t4030073918  L_75 = FsmQuaternion_get_Value_m2629833107(L_74, /*hidden argument*/NULL);
		V_11 = L_75;
		BitStream_t1979465639 * L_76 = ___stream0;
		NullCheck(L_76);
		BitStream_Serialize_m2762694425(L_76, (&V_11), /*hidden argument*/NULL);
	}

IL_0147:
	{
		int32_t L_77 = V_49;
		V_49 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_014d:
	{
		int32_t L_78 = V_49;
		FsmQuaternionU5BU5D_t3489263757* L_79 = V_48;
		NullCheck(L_79);
		if ((((int32_t)L_78) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_79)->max_length)))))))
		{
			goto IL_0126;
		}
	}
	{
		FsmVariables_t630687169 * L_80 = ___variables1;
		NullCheck(L_80);
		FsmVector3U5BU5D_t643261629* L_81 = FsmVariables_get_Vector3Variables_m1350627003(L_80, /*hidden argument*/NULL);
		V_50 = L_81;
		V_51 = 0;
		goto IL_0189;
	}

IL_0162:
	{
		FsmVector3U5BU5D_t643261629* L_82 = V_50;
		int32_t L_83 = V_51;
		NullCheck(L_82);
		int32_t L_84 = L_83;
		FsmVector3_t3996534004 * L_85 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		V_12 = L_85;
		FsmVector3_t3996534004 * L_86 = V_12;
		NullCheck(L_86);
		bool L_87 = NamedVariable_get_NetworkSync_m1993448044(L_86, /*hidden argument*/NULL);
		if (!L_87)
		{
			goto IL_0183;
		}
	}
	{
		FsmVector3_t3996534004 * L_88 = V_12;
		NullCheck(L_88);
		Vector3_t2243707580  L_89 = FsmVector3_get_Value_m4242600139(L_88, /*hidden argument*/NULL);
		V_13 = L_89;
		BitStream_t1979465639 * L_90 = ___stream0;
		NullCheck(L_90);
		BitStream_Serialize_m753399111(L_90, (&V_13), /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_91 = V_51;
		V_51 = ((int32_t)((int32_t)L_91+(int32_t)1));
	}

IL_0189:
	{
		int32_t L_92 = V_51;
		FsmVector3U5BU5D_t643261629* L_93 = V_50;
		NullCheck(L_93);
		if ((((int32_t)L_92) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_93)->max_length)))))))
		{
			goto IL_0162;
		}
	}
	{
		FsmVariables_t630687169 * L_94 = ___variables1;
		NullCheck(L_94);
		FsmColorU5BU5D_t4100123680* L_95 = FsmVariables_get_ColorVariables_m3164034491(L_94, /*hidden argument*/NULL);
		V_52 = L_95;
		V_53 = 0;
		goto IL_01f1;
	}

IL_019e:
	{
		FsmColorU5BU5D_t4100123680* L_96 = V_52;
		int32_t L_97 = V_53;
		NullCheck(L_96);
		int32_t L_98 = L_97;
		FsmColor_t118301965 * L_99 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		V_14 = L_99;
		FsmColor_t118301965 * L_100 = V_14;
		NullCheck(L_100);
		bool L_101 = NamedVariable_get_NetworkSync_m1993448044(L_100, /*hidden argument*/NULL);
		if (!L_101)
		{
			goto IL_01eb;
		}
	}
	{
		FsmColor_t118301965 * L_102 = V_14;
		NullCheck(L_102);
		Color_t2020392075  L_103 = FsmColor_get_Value_m687626399(L_102, /*hidden argument*/NULL);
		V_15 = L_103;
		BitStream_t1979465639 * L_104 = ___stream0;
		float* L_105 = (&V_15)->get_address_of_r_0();
		NullCheck(L_104);
		BitStream_Serialize_m2064522995(L_104, L_105, /*hidden argument*/NULL);
		BitStream_t1979465639 * L_106 = ___stream0;
		float* L_107 = (&V_15)->get_address_of_g_1();
		NullCheck(L_106);
		BitStream_Serialize_m2064522995(L_106, L_107, /*hidden argument*/NULL);
		BitStream_t1979465639 * L_108 = ___stream0;
		float* L_109 = (&V_15)->get_address_of_b_2();
		NullCheck(L_108);
		BitStream_Serialize_m2064522995(L_108, L_109, /*hidden argument*/NULL);
		BitStream_t1979465639 * L_110 = ___stream0;
		float* L_111 = (&V_15)->get_address_of_a_3();
		NullCheck(L_110);
		BitStream_Serialize_m2064522995(L_110, L_111, /*hidden argument*/NULL);
	}

IL_01eb:
	{
		int32_t L_112 = V_53;
		V_53 = ((int32_t)((int32_t)L_112+(int32_t)1));
	}

IL_01f1:
	{
		int32_t L_113 = V_53;
		FsmColorU5BU5D_t4100123680* L_114 = V_52;
		NullCheck(L_114);
		if ((((int32_t)L_113) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_114)->max_length)))))))
		{
			goto IL_019e;
		}
	}
	{
		FsmVariables_t630687169 * L_115 = ___variables1;
		NullCheck(L_115);
		FsmVector2U5BU5D_t381696854* L_116 = FsmVariables_get_Vector2Variables_m3290377243(L_115, /*hidden argument*/NULL);
		V_54 = L_116;
		V_55 = 0;
		goto IL_023f;
	}

IL_0206:
	{
		FsmVector2U5BU5D_t381696854* L_117 = V_54;
		int32_t L_118 = V_55;
		NullCheck(L_117);
		int32_t L_119 = L_118;
		FsmVector2_t2430450063 * L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		V_16 = L_120;
		FsmVector2_t2430450063 * L_121 = V_16;
		NullCheck(L_121);
		bool L_122 = NamedVariable_get_NetworkSync_m1993448044(L_121, /*hidden argument*/NULL);
		if (!L_122)
		{
			goto IL_0239;
		}
	}
	{
		FsmVector2_t2430450063 * L_123 = V_16;
		NullCheck(L_123);
		Vector2_t2243707579  L_124 = FsmVector2_get_Value_m2589490767(L_123, /*hidden argument*/NULL);
		V_17 = L_124;
		BitStream_t1979465639 * L_125 = ___stream0;
		float* L_126 = (&V_17)->get_address_of_x_0();
		NullCheck(L_125);
		BitStream_Serialize_m2064522995(L_125, L_126, /*hidden argument*/NULL);
		BitStream_t1979465639 * L_127 = ___stream0;
		float* L_128 = (&V_17)->get_address_of_y_1();
		NullCheck(L_127);
		BitStream_Serialize_m2064522995(L_127, L_128, /*hidden argument*/NULL);
	}

IL_0239:
	{
		int32_t L_129 = V_55;
		V_55 = ((int32_t)((int32_t)L_129+(int32_t)1));
	}

IL_023f:
	{
		int32_t L_130 = V_55;
		FsmVector2U5BU5D_t381696854* L_131 = V_54;
		NullCheck(L_131);
		if ((((int32_t)L_130) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_131)->max_length)))))))
		{
			goto IL_0206;
		}
	}
	{
		return;
	}

IL_0248:
	{
		FsmVariables_t630687169 * L_132 = ___variables1;
		NullCheck(L_132);
		FsmStringU5BU5D_t2699231328* L_133 = FsmVariables_get_StringVariables_m3984963863(L_132, /*hidden argument*/NULL);
		V_56 = L_133;
		V_57 = 0;
		goto IL_02ad;
	}

IL_0255:
	{
		FsmStringU5BU5D_t2699231328* L_134 = V_56;
		int32_t L_135 = V_57;
		NullCheck(L_134);
		int32_t L_136 = L_135;
		FsmString_t2414474701 * L_137 = (L_134)->GetAt(static_cast<il2cpp_array_size_t>(L_136));
		V_18 = L_137;
		FsmString_t2414474701 * L_138 = V_18;
		NullCheck(L_138);
		bool L_139 = NamedVariable_get_NetworkSync_m1993448044(L_138, /*hidden argument*/NULL);
		if (!L_139)
		{
			goto IL_02a7;
		}
	}
	{
		V_19 = 0;
		BitStream_t1979465639 * L_140 = ___stream0;
		NullCheck(L_140);
		BitStream_Serialize_m3312079847(L_140, (&V_19), /*hidden argument*/NULL);
		int32_t L_141 = V_19;
		V_20 = ((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)L_141));
		V_21 = 0;
		goto IL_0293;
	}

IL_027e:
	{
		BitStream_t1979465639 * L_142 = ___stream0;
		CharU5BU5D_t1328083999* L_143 = V_20;
		int32_t L_144 = V_21;
		NullCheck(L_143);
		NullCheck(L_142);
		BitStream_Serialize_m2742111985(L_142, ((L_143)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_144))), /*hidden argument*/NULL);
		int32_t L_145 = V_21;
		V_21 = ((int32_t)((int32_t)L_145+(int32_t)1));
	}

IL_0293:
	{
		int32_t L_146 = V_21;
		int32_t L_147 = V_19;
		if ((((int32_t)L_146) < ((int32_t)L_147)))
		{
			goto IL_027e;
		}
	}
	{
		FsmString_t2414474701 * L_148 = V_18;
		CharU5BU5D_t1328083999* L_149 = V_20;
		String_t* L_150 = String_CreateString_m3818307083(NULL, L_149, /*hidden argument*/NULL);
		NullCheck(L_148);
		FsmString_set_Value_m1767060322(L_148, L_150, /*hidden argument*/NULL);
	}

IL_02a7:
	{
		int32_t L_151 = V_57;
		V_57 = ((int32_t)((int32_t)L_151+(int32_t)1));
	}

IL_02ad:
	{
		int32_t L_152 = V_57;
		FsmStringU5BU5D_t2699231328* L_153 = V_56;
		NullCheck(L_153);
		if ((((int32_t)L_152) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_153)->max_length)))))))
		{
			goto IL_0255;
		}
	}
	{
		FsmVariables_t630687169 * L_154 = ___variables1;
		NullCheck(L_154);
		FsmBoolU5BU5D_t3830815681* L_155 = FsmVariables_get_BoolVariables_m1639951187(L_154, /*hidden argument*/NULL);
		V_58 = L_155;
		V_59 = 0;
		goto IL_02ec;
	}

IL_02c2:
	{
		FsmBoolU5BU5D_t3830815681* L_156 = V_58;
		int32_t L_157 = V_59;
		NullCheck(L_156);
		int32_t L_158 = L_157;
		FsmBool_t664485696 * L_159 = (L_156)->GetAt(static_cast<il2cpp_array_size_t>(L_158));
		V_22 = L_159;
		FsmBool_t664485696 * L_160 = V_22;
		NullCheck(L_160);
		bool L_161 = NamedVariable_get_NetworkSync_m1993448044(L_160, /*hidden argument*/NULL);
		if (!L_161)
		{
			goto IL_02e6;
		}
	}
	{
		V_23 = (bool)0;
		BitStream_t1979465639 * L_162 = ___stream0;
		NullCheck(L_162);
		BitStream_Serialize_m390089021(L_162, (&V_23), /*hidden argument*/NULL);
		FsmBool_t664485696 * L_163 = V_22;
		bool L_164 = V_23;
		NullCheck(L_163);
		FsmBool_set_Value_m2522230142(L_163, L_164, /*hidden argument*/NULL);
	}

IL_02e6:
	{
		int32_t L_165 = V_59;
		V_59 = ((int32_t)((int32_t)L_165+(int32_t)1));
	}

IL_02ec:
	{
		int32_t L_166 = V_59;
		FsmBoolU5BU5D_t3830815681* L_167 = V_58;
		NullCheck(L_167);
		if ((((int32_t)L_166) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_167)->max_length)))))))
		{
			goto IL_02c2;
		}
	}
	{
		FsmVariables_t630687169 * L_168 = ___variables1;
		NullCheck(L_168);
		FsmFloatU5BU5D_t4177556671* L_169 = FsmVariables_get_FloatVariables_m3630743291(L_168, /*hidden argument*/NULL);
		V_60 = L_169;
		V_41 = 0;
		goto IL_032f;
	}

IL_0301:
	{
		FsmFloatU5BU5D_t4177556671* L_170 = V_60;
		int32_t L_171 = V_41;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		FsmFloat_t937133978 * L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		V_24 = L_173;
		FsmFloat_t937133978 * L_174 = V_24;
		NullCheck(L_174);
		bool L_175 = NamedVariable_get_NetworkSync_m1993448044(L_174, /*hidden argument*/NULL);
		if (!L_175)
		{
			goto IL_0329;
		}
	}
	{
		V_25 = (0.0f);
		BitStream_t1979465639 * L_176 = ___stream0;
		NullCheck(L_176);
		BitStream_Serialize_m2064522995(L_176, (&V_25), /*hidden argument*/NULL);
		FsmFloat_t937133978 * L_177 = V_24;
		float L_178 = V_25;
		NullCheck(L_177);
		FsmFloat_set_Value_m3447553958(L_177, L_178, /*hidden argument*/NULL);
	}

IL_0329:
	{
		int32_t L_179 = V_41;
		V_41 = ((int32_t)((int32_t)L_179+(int32_t)1));
	}

IL_032f:
	{
		int32_t L_180 = V_41;
		FsmFloatU5BU5D_t4177556671* L_181 = V_60;
		NullCheck(L_181);
		if ((((int32_t)L_180) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_181)->max_length)))))))
		{
			goto IL_0301;
		}
	}
	{
		FsmVariables_t630687169 * L_182 = ___variables1;
		NullCheck(L_182);
		FsmIntU5BU5D_t2637547802* L_183 = FsmVariables_get_IntVariables_m2179857115(L_182, /*hidden argument*/NULL);
		V_46 = L_183;
		V_41 = 0;
		goto IL_036e;
	}

IL_0344:
	{
		FsmIntU5BU5D_t2637547802* L_184 = V_46;
		int32_t L_185 = V_41;
		NullCheck(L_184);
		int32_t L_186 = L_185;
		FsmInt_t1273009179 * L_187 = (L_184)->GetAt(static_cast<il2cpp_array_size_t>(L_186));
		V_26 = L_187;
		FsmInt_t1273009179 * L_188 = V_26;
		NullCheck(L_188);
		bool L_189 = NamedVariable_get_NetworkSync_m1993448044(L_188, /*hidden argument*/NULL);
		if (!L_189)
		{
			goto IL_0368;
		}
	}
	{
		V_27 = 0;
		BitStream_t1979465639 * L_190 = ___stream0;
		NullCheck(L_190);
		BitStream_Serialize_m3312079847(L_190, (&V_27), /*hidden argument*/NULL);
		FsmInt_t1273009179 * L_191 = V_26;
		int32_t L_192 = V_27;
		NullCheck(L_191);
		FsmInt_set_Value_m4097648685(L_191, L_192, /*hidden argument*/NULL);
	}

IL_0368:
	{
		int32_t L_193 = V_41;
		V_41 = ((int32_t)((int32_t)L_193+(int32_t)1));
	}

IL_036e:
	{
		int32_t L_194 = V_41;
		FsmIntU5BU5D_t2637547802* L_195 = V_46;
		NullCheck(L_195);
		if ((((int32_t)L_194) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_195)->max_length)))))))
		{
			goto IL_0344;
		}
	}
	{
		FsmVariables_t630687169 * L_196 = ___variables1;
		NullCheck(L_196);
		FsmQuaternionU5BU5D_t3489263757* L_197 = FsmVariables_get_QuaternionVariables_m3575404011(L_196, /*hidden argument*/NULL);
		V_48 = L_197;
		V_41 = 0;
		goto IL_03b1;
	}

IL_0383:
	{
		FsmQuaternionU5BU5D_t3489263757* L_198 = V_48;
		int32_t L_199 = V_41;
		NullCheck(L_198);
		int32_t L_200 = L_199;
		FsmQuaternion_t878438756 * L_201 = (L_198)->GetAt(static_cast<il2cpp_array_size_t>(L_200));
		V_28 = L_201;
		FsmQuaternion_t878438756 * L_202 = V_28;
		NullCheck(L_202);
		bool L_203 = NamedVariable_get_NetworkSync_m1993448044(L_202, /*hidden argument*/NULL);
		if (!L_203)
		{
			goto IL_03ab;
		}
	}
	{
		Quaternion_t4030073918  L_204 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_29 = L_204;
		BitStream_t1979465639 * L_205 = ___stream0;
		NullCheck(L_205);
		BitStream_Serialize_m2762694425(L_205, (&V_29), /*hidden argument*/NULL);
		FsmQuaternion_t878438756 * L_206 = V_28;
		Quaternion_t4030073918  L_207 = V_29;
		NullCheck(L_206);
		FsmQuaternion_set_Value_m1061544654(L_206, L_207, /*hidden argument*/NULL);
	}

IL_03ab:
	{
		int32_t L_208 = V_41;
		V_41 = ((int32_t)((int32_t)L_208+(int32_t)1));
	}

IL_03b1:
	{
		int32_t L_209 = V_41;
		FsmQuaternionU5BU5D_t3489263757* L_210 = V_48;
		NullCheck(L_210);
		if ((((int32_t)L_209) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_210)->max_length)))))))
		{
			goto IL_0383;
		}
	}
	{
		FsmVariables_t630687169 * L_211 = ___variables1;
		NullCheck(L_211);
		FsmVector3U5BU5D_t643261629* L_212 = FsmVariables_get_Vector3Variables_m1350627003(L_211, /*hidden argument*/NULL);
		V_50 = L_212;
		V_41 = 0;
		goto IL_03f4;
	}

IL_03c6:
	{
		FsmVector3U5BU5D_t643261629* L_213 = V_50;
		int32_t L_214 = V_41;
		NullCheck(L_213);
		int32_t L_215 = L_214;
		FsmVector3_t3996534004 * L_216 = (L_213)->GetAt(static_cast<il2cpp_array_size_t>(L_215));
		V_30 = L_216;
		FsmVector3_t3996534004 * L_217 = V_30;
		NullCheck(L_217);
		bool L_218 = NamedVariable_get_NetworkSync_m1993448044(L_217, /*hidden argument*/NULL);
		if (!L_218)
		{
			goto IL_03ee;
		}
	}
	{
		Vector3_t2243707580  L_219 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_31 = L_219;
		BitStream_t1979465639 * L_220 = ___stream0;
		NullCheck(L_220);
		BitStream_Serialize_m753399111(L_220, (&V_31), /*hidden argument*/NULL);
		FsmVector3_t3996534004 * L_221 = V_30;
		Vector3_t2243707580  L_222 = V_31;
		NullCheck(L_221);
		FsmVector3_set_Value_m1785770740(L_221, L_222, /*hidden argument*/NULL);
	}

IL_03ee:
	{
		int32_t L_223 = V_41;
		V_41 = ((int32_t)((int32_t)L_223+(int32_t)1));
	}

IL_03f4:
	{
		int32_t L_224 = V_41;
		FsmVector3U5BU5D_t643261629* L_225 = V_50;
		NullCheck(L_225);
		if ((((int32_t)L_224) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_225)->max_length)))))))
		{
			goto IL_03c6;
		}
	}
	{
		FsmVariables_t630687169 * L_226 = ___variables1;
		NullCheck(L_226);
		FsmColorU5BU5D_t4100123680* L_227 = FsmVariables_get_ColorVariables_m3164034491(L_226, /*hidden argument*/NULL);
		V_52 = L_227;
		V_41 = 0;
		goto IL_046f;
	}

IL_0409:
	{
		FsmColorU5BU5D_t4100123680* L_228 = V_52;
		int32_t L_229 = V_41;
		NullCheck(L_228);
		int32_t L_230 = L_229;
		FsmColor_t118301965 * L_231 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_230));
		V_32 = L_231;
		FsmColor_t118301965 * L_232 = V_32;
		NullCheck(L_232);
		bool L_233 = NamedVariable_get_NetworkSync_m1993448044(L_232, /*hidden argument*/NULL);
		if (!L_233)
		{
			goto IL_0469;
		}
	}
	{
		V_33 = (0.0f);
		BitStream_t1979465639 * L_234 = ___stream0;
		NullCheck(L_234);
		BitStream_Serialize_m2064522995(L_234, (&V_33), /*hidden argument*/NULL);
		V_34 = (0.0f);
		BitStream_t1979465639 * L_235 = ___stream0;
		NullCheck(L_235);
		BitStream_Serialize_m2064522995(L_235, (&V_34), /*hidden argument*/NULL);
		V_35 = (0.0f);
		BitStream_t1979465639 * L_236 = ___stream0;
		NullCheck(L_236);
		BitStream_Serialize_m2064522995(L_236, (&V_35), /*hidden argument*/NULL);
		V_36 = (0.0f);
		BitStream_t1979465639 * L_237 = ___stream0;
		NullCheck(L_237);
		BitStream_Serialize_m2064522995(L_237, (&V_36), /*hidden argument*/NULL);
		FsmColor_t118301965 * L_238 = V_32;
		float L_239 = V_33;
		float L_240 = V_34;
		float L_241 = V_35;
		float L_242 = V_36;
		Color_t2020392075  L_243;
		memset(&L_243, 0, sizeof(L_243));
		Color__ctor_m1909920690(&L_243, L_239, L_240, L_241, L_242, /*hidden argument*/NULL);
		NullCheck(L_238);
		FsmColor_set_Value_m92994086(L_238, L_243, /*hidden argument*/NULL);
	}

IL_0469:
	{
		int32_t L_244 = V_41;
		V_41 = ((int32_t)((int32_t)L_244+(int32_t)1));
	}

IL_046f:
	{
		int32_t L_245 = V_41;
		FsmColorU5BU5D_t4100123680* L_246 = V_52;
		NullCheck(L_246);
		if ((((int32_t)L_245) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_246)->max_length)))))))
		{
			goto IL_0409;
		}
	}
	{
		FsmVariables_t630687169 * L_247 = ___variables1;
		NullCheck(L_247);
		FsmVector2U5BU5D_t381696854* L_248 = FsmVariables_get_Vector2Variables_m3290377243(L_247, /*hidden argument*/NULL);
		V_54 = L_248;
		V_41 = 0;
		goto IL_04c8;
	}

IL_0484:
	{
		FsmVector2U5BU5D_t381696854* L_249 = V_54;
		int32_t L_250 = V_41;
		NullCheck(L_249);
		int32_t L_251 = L_250;
		FsmVector2_t2430450063 * L_252 = (L_249)->GetAt(static_cast<il2cpp_array_size_t>(L_251));
		V_37 = L_252;
		FsmVector2_t2430450063 * L_253 = V_37;
		NullCheck(L_253);
		bool L_254 = NamedVariable_get_NetworkSync_m1993448044(L_253, /*hidden argument*/NULL);
		if (!L_254)
		{
			goto IL_04c2;
		}
	}
	{
		V_38 = (0.0f);
		BitStream_t1979465639 * L_255 = ___stream0;
		NullCheck(L_255);
		BitStream_Serialize_m2064522995(L_255, (&V_38), /*hidden argument*/NULL);
		V_39 = (0.0f);
		BitStream_t1979465639 * L_256 = ___stream0;
		NullCheck(L_256);
		BitStream_Serialize_m2064522995(L_256, (&V_39), /*hidden argument*/NULL);
		FsmVector2_t2430450063 * L_257 = V_37;
		float L_258 = V_38;
		float L_259 = V_39;
		Vector2_t2243707579  L_260;
		memset(&L_260, 0, sizeof(L_260));
		Vector2__ctor_m3067419446(&L_260, L_258, L_259, /*hidden argument*/NULL);
		NullCheck(L_257);
		FsmVector2_set_Value_m1294693978(L_257, L_260, /*hidden argument*/NULL);
	}

IL_04c2:
	{
		int32_t L_261 = V_41;
		V_41 = ((int32_t)((int32_t)L_261+(int32_t)1));
	}

IL_04c8:
	{
		int32_t L_262 = V_41;
		FsmVector2U5BU5D_t381696854* L_263 = V_54;
		NullCheck(L_263);
		if ((((int32_t)L_262) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_263)->max_length)))))))
		{
			goto IL_0484;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnMasterServerEvent(UnityEngine.MasterServerEvent)
extern "C"  void PlayMakerFSM_OnMasterServerEvent_m4091791057 (PlayMakerFSM_t437737208 * __this, int32_t ___masterServerEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnMasterServerEvent_m4091791057_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		FsmEventData_t2110469976 * L_0 = ((Fsm_t917886356_StaticFields*)Fsm_t917886356_il2cpp_TypeInfo_var->static_fields)->get_EventData_4();
		int32_t L_1 = ___masterServerEvent0;
		NullCheck(L_0);
		L_0->set_MasterServerEvent_20(L_1);
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_get_MasterServerEvent_m2238530620(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Fsm_Event_m4079224475(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// HutongGames.PlayMaker.Fsm PlayMakerFSM::get_Fsm()
extern "C"  Fsm_t917886356 * PlayMakerFSM_get_Fsm_m3359411247 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		Fsm_set_Owner_m3780371132(L_0, __this, /*hidden argument*/NULL);
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		return L_1;
	}
}
// System.String PlayMakerFSM::get_FsmName()
extern "C"  String_t* PlayMakerFSM_get_FsmName_m2713674740 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Name_m2639317356(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::set_FsmName(System.String)
extern "C"  void PlayMakerFSM_set_FsmName_m3291949591 (PlayMakerFSM_t437737208 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Fsm_set_Name_m629262121(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerFSM::get_FsmDescription()
extern "C"  String_t* PlayMakerFSM_get_FsmDescription_m3515647765 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		String_t* L_1 = Fsm_get_Description_m204017195(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::set_FsmDescription(System.String)
extern "C"  void PlayMakerFSM_set_FsmDescription_m783281822 (PlayMakerFSM_t437737208 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		Fsm_set_Description_m2175047222(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayMakerFSM::get_Active()
extern "C"  bool PlayMakerFSM_get_Active_m2477371780 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		bool L_1 = Fsm_get_Active_m1413219028(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String PlayMakerFSM::get_ActiveStateName()
extern "C"  String_t* PlayMakerFSM_get_ActiveStateName_m1650919959 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_ActiveStateName_m1650919959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmState_t1643911659 * L_1 = Fsm_get_ActiveState_m1540121605(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		Fsm_t917886356 * L_2 = __this->get_fsm_6();
		NullCheck(L_2);
		FsmState_t1643911659 * L_3 = Fsm_get_ActiveState_m1540121605(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = FsmState_get_Name_m2457110913(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001e:
	{
		return _stringLiteral371857150;
	}
}
// HutongGames.PlayMaker.FsmState[] PlayMakerFSM::get_FsmStates()
extern "C"  FsmStateU5BU5D_t1586422282* PlayMakerFSM_get_FsmStates_m1657585614 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmStateU5BU5D_t1586422282* L_1 = Fsm_get_States_m1003125214(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmEvent[] PlayMakerFSM::get_FsmEvents()
extern "C"  FsmEventU5BU5D_t287863993* PlayMakerFSM_get_FsmEvents_m601072828 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmEventU5BU5D_t287863993* L_1 = Fsm_get_Events_m465635212(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmTransition[] PlayMakerFSM::get_FsmGlobalTransitions()
extern "C"  FsmTransitionU5BU5D_t1091630918* PlayMakerFSM_get_FsmGlobalTransitions_m1961813325 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmTransitionU5BU5D_t1091630918* L_1 = Fsm_get_GlobalTransitions_m387686343(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// HutongGames.PlayMaker.FsmVariables PlayMakerFSM::get_FsmVariables()
extern "C"  FsmVariables_t630687169 * PlayMakerFSM_get_FsmVariables_m2623807583 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		Fsm_t917886356 * L_0 = __this->get_fsm_6();
		NullCheck(L_0);
		FsmVariables_t630687169 * L_1 = Fsm_get_Variables_m738201045(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean PlayMakerFSM::get_UsesTemplate()
extern "C"  bool PlayMakerFSM_get_UsesTemplate_m3598466416 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_get_UsesTemplate_m3598466416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmTemplate_t1285897084 * L_0 = __this->get_fsmTemplate_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void PlayMakerFSM::OnBeforeSerialize()
extern "C"  void PlayMakerFSM_OnBeforeSerialize_m2468983921 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayMakerFSM::OnAfterDeserialize()
extern "C"  void PlayMakerFSM_OnAfterDeserialize_m3821310365 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM_OnAfterDeserialize_m3821310365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->set_NotMainThread_5((bool)1);
		bool L_0 = PlayMakerGlobals_get_Initialized_m2540978452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		Fsm_t917886356 * L_1 = __this->get_fsm_6();
		NullCheck(L_1);
		Fsm_InitData_m3521367119(L_1, /*hidden argument*/NULL);
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->set_NotMainThread_5((bool)0);
		return;
	}
}
// System.Void PlayMakerFSM::.ctor()
extern "C"  void PlayMakerFSM__ctor_m1051306911 (PlayMakerFSM_t437737208 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerFSM::.cctor()
extern "C"  void PlayMakerFSM__cctor_m2888832784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerFSM__cctor_m2888832784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4101825636 * L_0 = (List_1_t4101825636 *)il2cpp_codegen_object_new(List_1_t4101825636_il2cpp_TypeInfo_var);
		List_1__ctor_m2230797533(L_0, /*hidden argument*/List_1__ctor_m2230797533_MethodInfo_var);
		((PlayMakerFSM_t437737208_StaticFields*)PlayMakerFSM_t437737208_il2cpp_TypeInfo_var->static_fields)->set_fsmList_2(L_0);
		return;
	}
}
// System.Boolean PlayMakerFSM/<DoCoroutine>d__1::MoveNext()
extern "C"  bool U3CDoCoroutineU3Ed__1_MoveNext_m3119791968 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoCoroutineU3Ed__1_MoveNext_m3119791968_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_1();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_006a;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_1((-1));
	}

IL_001e:
	{
		PlayMakerFSM_t437737208 * L_2 = __this->get_U3CU3E4__this_2();
		NullCheck(L_2);
		Fsm_t917886356 * L_3 = L_2->get_fsm_6();
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t503928862_il2cpp_TypeInfo_var);
		FsmExecutionStack_PushFsm_m1873564309(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Il2CppObject * L_4 = __this->get_routine_3();
		NullCheck(L_4);
		bool L_5 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_4);
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t503928862_il2cpp_TypeInfo_var);
		FsmExecutionStack_PopFsm_m3490719728(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FsmExecutionStack_t503928862_il2cpp_TypeInfo_var);
		FsmExecutionStack_PopFsm_m3490719728(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_6 = __this->get_routine_3();
		NullCheck(L_6);
		Il2CppObject * L_7 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_6);
		__this->set_U3CU3E2__current_0(L_7);
		__this->set_U3CU3E1__state_1(1);
		return (bool)1;
	}

IL_0061:
	{
		__this->set_U3CU3E1__state_1((-1));
		goto IL_001e;
	}

IL_006a:
	{
		return (bool)0;
	}
}
// System.Object PlayMakerFSM/<DoCoroutine>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  Il2CppObject * U3CDoCoroutineU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1181596063 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__1::System.Collections.IEnumerator.Reset()
extern "C"  void U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m1228084302 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_m1228084302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__1::System.IDisposable.Dispose()
extern "C"  void U3CDoCoroutineU3Ed__1_System_IDisposable_Dispose_m3617530473 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object PlayMakerFSM/<DoCoroutine>d__1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoCoroutineU3Ed__1_System_Collections_IEnumerator_get_Current_m2252703192 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U3CU3E2__current_0();
		return L_0;
	}
}
// System.Void PlayMakerFSM/<DoCoroutine>d__1::.ctor(System.Int32)
extern "C"  void U3CDoCoroutineU3Ed__1__ctor_m3023621983 (U3CDoCoroutineU3Ed__1_t4237467219 * __this, int32_t ___U3CU3E1__state0, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_1(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_Initialized()
extern "C"  bool PlayMakerGlobals_get_Initialized_m2540978452 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_Initialized_m2540978452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_U3CInitializedU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Initialized(System.Boolean)
extern "C"  void PlayMakerGlobals_set_Initialized_m2688991965 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_Initialized_m2688991965_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_U3CInitializedU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsPlayingInEditor()
extern "C"  bool PlayMakerGlobals_get_IsPlayingInEditor_m2116337626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsPlayingInEditor_m2116337626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_U3CIsPlayingInEditorU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsPlayingInEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlayingInEditor_m2559612605 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsPlayingInEditor_m2559612605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_U3CIsPlayingInEditorU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsPlaying()
extern "C"  bool PlayMakerGlobals_get_IsPlaying_m1998291744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsPlaying_m1998291744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_U3CIsPlayingU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsPlaying(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsPlaying_m3897118763 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsPlaying_m3897118763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_U3CIsPlayingU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsEditor()
extern "C"  bool PlayMakerGlobals_get_IsEditor_m2851556513 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsEditor_m2851556513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_U3CIsEditorU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsEditor(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsEditor_m1340019058 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsEditor_m1340019058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_U3CIsEditorU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean PlayMakerGlobals::get_IsBuilding()
extern "C"  bool PlayMakerGlobals_get_IsBuilding_m3989868652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_IsBuilding_m3989868652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_U3CIsBuildingU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_IsBuilding(System.Boolean)
extern "C"  void PlayMakerGlobals_set_IsBuilding_m4135337617 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_set_IsBuilding_m4135337617_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_U3CIsBuildingU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void PlayMakerGlobals::InitApplicationFlags()
extern "C"  void PlayMakerGlobals_InitApplicationFlags_m1473070140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
	}

IL_000f:
	{
		PlayMakerGlobals_set_IsPlayingInEditor_m2559612605(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		bool L_2 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		bool L_3 = PlayMakerGlobals_get_IsBuilding_m3989868652(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_3));
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		PlayMakerGlobals_set_IsPlaying_m3897118763(NULL /*static, unused*/, (bool)G_B6_0, /*hidden argument*/NULL);
		bool L_4 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGlobals_set_IsEditor_m1340019058(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGlobals::Initialize()
extern "C"  void PlayMakerGlobals_Initialize_m3494352383 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_Initialize_m3494352383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t1021602117 * V_0 = NULL;
	PlayMakerGlobals_t2120229426 * V_1 = NULL;
	{
		bool L_0 = PlayMakerGlobals_get_Initialized_m2540978452(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_008e;
		}
	}
	{
		PlayMakerGlobals_InitApplicationFlags_m1473070140(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PlayMakerGlobals_t2120229426_0_0_0_var), /*hidden argument*/NULL);
		Object_t1021602117 * L_2 = Resources_Load_m243305716(NULL /*static, unused*/, _stringLiteral2848307944, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Object_t1021602117 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007e;
		}
	}
	{
		bool L_5 = PlayMakerGlobals_get_IsPlayingInEditor_m2116337626(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		Object_t1021602117 * L_6 = V_0;
		V_1 = ((PlayMakerGlobals_t2120229426 *)CastclassClass(L_6, PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var));
		PlayMakerGlobals_t2120229426 * L_7 = ScriptableObject_CreateInstance_TisPlayMakerGlobals_t2120229426_m3104673918(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerGlobals_t2120229426_m3104673918_MethodInfo_var);
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_7);
		PlayMakerGlobals_t2120229426 * L_8 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		PlayMakerGlobals_t2120229426 * L_9 = V_1;
		NullCheck(L_9);
		FsmVariables_t630687169 * L_10 = L_9->get_variables_3();
		FsmVariables_t630687169 * L_11 = (FsmVariables_t630687169 *)il2cpp_codegen_object_new(FsmVariables_t630687169_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m899263547(L_11, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		PlayMakerGlobals_set_Variables_m4203227634(L_8, L_11, /*hidden argument*/NULL);
		PlayMakerGlobals_t2120229426 * L_12 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		PlayMakerGlobals_t2120229426 * L_13 = V_1;
		NullCheck(L_13);
		List_1_t1398341365 * L_14 = PlayMakerGlobals_get_Events_m2654294232(L_13, /*hidden argument*/NULL);
		List_1_t1398341365 * L_15 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m596405649(L_15, L_14, /*hidden argument*/List_1__ctor_m596405649_MethodInfo_var);
		NullCheck(L_12);
		PlayMakerGlobals_set_Events_m1743941641(L_12, L_15, /*hidden argument*/NULL);
		goto IL_0088;
	}

IL_0071:
	{
		Object_t1021602117 * L_16 = V_0;
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_instance_2(((PlayMakerGlobals_t2120229426 *)IsInstClass(L_16, PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var)));
		goto IL_0088;
	}

IL_007e:
	{
		PlayMakerGlobals_t2120229426 * L_17 = ScriptableObject_CreateInstance_TisPlayMakerGlobals_t2120229426_m3104673918(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerGlobals_t2120229426_m3104673918_MethodInfo_var);
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_17);
	}

IL_0088:
	{
		PlayMakerGlobals_set_Initialized_m2688991965(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
	}

IL_008e:
	{
		return;
	}
}
// PlayMakerGlobals PlayMakerGlobals::get_Instance()
extern "C"  PlayMakerGlobals_t2120229426 * PlayMakerGlobals_get_Instance_m4066551560 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_get_Instance_m4066551560_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_Initialize_m3494352383(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGlobals_t2120229426 * L_0 = ((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::ResetInstance()
extern "C"  void PlayMakerGlobals_ResetInstance_m1747765483 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_ResetInstance_m1747765483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_instance_2((PlayMakerGlobals_t2120229426 *)NULL);
		return;
	}
}
// HutongGames.PlayMaker.FsmVariables PlayMakerGlobals::get_Variables()
extern "C"  FsmVariables_t630687169 * PlayMakerGlobals_get_Variables_m2249473751 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method)
{
	{
		FsmVariables_t630687169 * L_0 = __this->get_variables_3();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Variables(HutongGames.PlayMaker.FsmVariables)
extern "C"  void PlayMakerGlobals_set_Variables_m4203227634 (PlayMakerGlobals_t2120229426 * __this, FsmVariables_t630687169 * ___value0, const MethodInfo* method)
{
	{
		FsmVariables_t630687169 * L_0 = ___value0;
		__this->set_variables_3(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> PlayMakerGlobals::get_Events()
extern "C"  List_1_t1398341365 * PlayMakerGlobals_get_Events_m2654294232 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method)
{
	{
		List_1_t1398341365 * L_0 = __this->get_events_4();
		return L_0;
	}
}
// System.Void PlayMakerGlobals::set_Events(System.Collections.Generic.List`1<System.String>)
extern "C"  void PlayMakerGlobals_set_Events_m1743941641 (PlayMakerGlobals_t2120229426 * __this, List_1_t1398341365 * ___value0, const MethodInfo* method)
{
	{
		List_1_t1398341365 * L_0 = ___value0;
		__this->set_events_4(L_0);
		return;
	}
}
// HutongGames.PlayMaker.FsmEvent PlayMakerGlobals::AddEvent(System.String)
extern "C"  FsmEvent_t1258573736 * PlayMakerGlobals_AddEvent_m677616813 (PlayMakerGlobals_t2120229426 * __this, String_t* ___eventName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_AddEvent_m677616813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmEvent_t1258573736 * V_0 = NULL;
	FsmEvent_t1258573736 * G_B2_0 = NULL;
	FsmEvent_t1258573736 * G_B1_0 = NULL;
	{
		List_1_t1398341365 * L_0 = __this->get_events_4();
		String_t* L_1 = ___eventName0;
		NullCheck(L_0);
		List_1_Add_m4061286785(L_0, L_1, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		String_t* L_2 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_3 = FsmEvent_FindEvent_m131869239(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		FsmEvent_t1258573736 * L_4 = L_3;
		G_B1_0 = L_4;
		if (L_4)
		{
			G_B2_0 = L_4;
			goto IL_001c;
		}
	}
	{
		String_t* L_5 = ___eventName0;
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_6 = FsmEvent_GetFsmEvent_m128779372(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		G_B2_0 = L_6;
	}

IL_001c:
	{
		V_0 = G_B2_0;
		FsmEvent_t1258573736 * L_7 = V_0;
		NullCheck(L_7);
		FsmEvent_set_IsGlobal_m2485871150(L_7, (bool)1, /*hidden argument*/NULL);
		FsmEvent_t1258573736 * L_8 = V_0;
		return L_8;
	}
}
// System.Void PlayMakerGlobals::OnEnable()
extern "C"  void PlayMakerGlobals_OnEnable_m2794833831 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayMakerGlobals::OnDestroy()
extern "C"  void PlayMakerGlobals_OnDestroy_m124376554 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals_OnDestroy_m124376554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerGlobals_set_Initialized_m2688991965(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		((PlayMakerGlobals_t2120229426_StaticFields*)PlayMakerGlobals_t2120229426_il2cpp_TypeInfo_var->static_fields)->set_instance_2((PlayMakerGlobals_t2120229426 *)NULL);
		return;
	}
}
// System.Void PlayMakerGlobals::.ctor()
extern "C"  void PlayMakerGlobals__ctor_m3164927587 (PlayMakerGlobals_t2120229426 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGlobals__ctor_m3164927587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FsmVariables_t630687169 * L_0 = (FsmVariables_t630687169 *)il2cpp_codegen_object_new(FsmVariables_t630687169_il2cpp_TypeInfo_var);
		FsmVariables__ctor_m4065964934(L_0, /*hidden argument*/NULL);
		__this->set_variables_3(L_0);
		List_1_t1398341365 * L_1 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_1, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_events_4(L_1);
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_EnableStateLabels()
extern "C"  bool PlayMakerGUI_get_EnableStateLabels_m3131360880 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_EnableStateLabels_m3131360880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m2322412903(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_0 = Application_get_isEditor_m2474583393(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_1 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_3 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_3);
		bool L_4 = Behaviour_get_enabled_m4079055610(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_5 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_5);
		bool L_6 = L_5->get_drawStateLabels_8();
		return L_6;
	}

IL_0030:
	{
		return (bool)0;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_7 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_9 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_9);
		bool L_10 = Behaviour_get_enabled_m4079055610(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_11 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_11);
		bool L_12 = L_11->get_drawStateLabels_8();
		if (!L_12)
		{
			goto IL_0062;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_13 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_13);
		bool L_14 = L_13->get_enableStateLabelsInBuilds_9();
		return L_14;
	}

IL_0062:
	{
		return (bool)0;
	}
}
// System.Void PlayMakerGUI::set_EnableStateLabels(System.Boolean)
extern "C"  void PlayMakerGUI_set_EnableStateLabels_m1834678665 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_EnableStateLabels_m1834678665_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m2322412903(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_2 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		bool L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_drawStateLabels_8(L_3);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean PlayMakerGUI::get_EnableStateLabelsInBuild()
extern "C"  bool PlayMakerGUI_get_EnableStateLabelsInBuild_m654835679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_EnableStateLabelsInBuild_m654835679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m2322412903(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_2 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m4079055610(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_4 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_4);
		bool L_5 = L_4->get_enableStateLabelsInBuilds_9();
		return L_5;
	}

IL_0029:
	{
		return (bool)0;
	}
}
// System.Void PlayMakerGUI::set_EnableStateLabelsInBuild(System.Boolean)
extern "C"  void PlayMakerGUI_set_EnableStateLabelsInBuild_m2190807688 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_EnableStateLabelsInBuild_m2190807688_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m2322412903(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_2 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		bool L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_enableStateLabelsInBuilds_9(L_3);
	}

IL_001d:
	{
		return;
	}
}
// System.Void PlayMakerGUI::InitInstance()
extern "C"  void PlayMakerGUI_InitInstance_m2322412903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_InitInstance_m2322412903_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PlayMakerGUI_t2662579489_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_instance_19(((PlayMakerGUI_t2662579489 *)CastclassClass(L_3, PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var)));
	}

IL_0026:
	{
		return;
	}
}
// PlayMakerGUI PlayMakerGUI::get_Instance()
extern "C"  PlayMakerGUI_t2662579489 * PlayMakerGUI_get_Instance_m3854846106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_Instance_m3854846106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_InitInstance_m2322412903(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_2, _stringLiteral3390658007, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		PlayMakerGUI_t2662579489 * L_4 = GameObject_AddComponent_TisPlayMakerGUI_t2662579489_m366838623(L_3, /*hidden argument*/GameObject_AddComponent_TisPlayMakerGUI_t2662579489_m366838623_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_instance_19(L_4);
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_5 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		return L_5;
	}
}
// System.Boolean PlayMakerGUI::get_Enabled()
extern "C"  bool PlayMakerGUI_get_Enabled_m402578142 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_Enabled_m402578142_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_2 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_m4079055610(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0018:
	{
		return (bool)0;
	}
}
// UnityEngine.GUISkin PlayMakerGUI::get_GUISkin()
extern "C"  GUISkin_t1436893342 * PlayMakerGUI_get_GUISkin_m2441732861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUISkin_m2441732861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_guiSkin_20();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUISkin(UnityEngine.GUISkin)
extern "C"  void PlayMakerGUI_set_GUISkin_m4222880562 (Il2CppObject * __this /* static, unused */, GUISkin_t1436893342 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUISkin_m4222880562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUISkin_t1436893342 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiSkin_20(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIColor_m3278185336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIColor_m3278185336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Color_t2020392075  L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_guiColor_21();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIColor_m3151620121 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIColor_m3151620121_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiColor_21(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIBackgroundColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIBackgroundColor_m2538762796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIBackgroundColor_m2538762796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Color_t2020392075  L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_guiBackgroundColor_22();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIBackgroundColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIBackgroundColor_m3152511687 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIBackgroundColor_m3152511687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiBackgroundColor_22(L_0);
		return;
	}
}
// UnityEngine.Color PlayMakerGUI::get_GUIContentColor()
extern "C"  Color_t2020392075  PlayMakerGUI_get_GUIContentColor_m1794729359 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIContentColor_m1794729359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Color_t2020392075  L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_guiContentColor_23();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIContentColor(UnityEngine.Color)
extern "C"  void PlayMakerGUI_set_GUIContentColor_m3644451446 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIContentColor_m3644451446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Color_t2020392075  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiContentColor_23(L_0);
		return;
	}
}
// UnityEngine.Matrix4x4 PlayMakerGUI::get_GUIMatrix()
extern "C"  Matrix4x4_t2933234003  PlayMakerGUI_get_GUIMatrix_m1271908730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_GUIMatrix_m1271908730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_guiMatrix_24();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_GUIMatrix(UnityEngine.Matrix4x4)
extern "C"  void PlayMakerGUI_set_GUIMatrix_m1989428149 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_GUIMatrix_m1989428149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Matrix4x4_t2933234003  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiMatrix_24(L_0);
		return;
	}
}
// UnityEngine.Texture PlayMakerGUI::get_MouseCursor()
extern "C"  Texture_t2243626319 * PlayMakerGUI_get_MouseCursor_m803897773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_MouseCursor_m803897773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Texture_t2243626319 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_U3CMouseCursorU3Ek__BackingField_28();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_MouseCursor(UnityEngine.Texture)
extern "C"  void PlayMakerGUI_set_MouseCursor_m3353084044 (Il2CppObject * __this /* static, unused */, Texture_t2243626319 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_MouseCursor_m3353084044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture_t2243626319 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_U3CMouseCursorU3Ek__BackingField_28(L_0);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_LockCursor()
extern "C"  bool PlayMakerGUI_get_LockCursor_m2699961842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_LockCursor_m2699961842_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_U3CLockCursorU3Ek__BackingField_29();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_LockCursor(System.Boolean)
extern "C"  void PlayMakerGUI_set_LockCursor_m800390487 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_LockCursor_m800390487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_U3CLockCursorU3Ek__BackingField_29(L_0);
		return;
	}
}
// System.Boolean PlayMakerGUI::get_HideCursor()
extern "C"  bool PlayMakerGUI_get_HideCursor_m1761535307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_get_HideCursor_m1761535307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		bool L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_U3CHideCursorU3Ek__BackingField_30();
		return L_0;
	}
}
// System.Void PlayMakerGUI::set_HideCursor(System.Boolean)
extern "C"  void PlayMakerGUI_set_HideCursor_m2707817742 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_set_HideCursor_m2707817742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_U3CHideCursorU3Ek__BackingField_30(L_0);
		return;
	}
}
// System.Void PlayMakerGUI::InitLabelStyle()
extern "C"  void PlayMakerGUI_InitLabelStyle_m2898018055 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_InitLabelStyle_m2898018055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GUIStyle_t1799908754 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Texture2D_t3542995729 * L_2 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_26();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Texture2D_t3542995729 * L_3 = (Texture2D_t3542995729 *)il2cpp_codegen_object_new(Texture2D_t3542995729_il2cpp_TypeInfo_var);
		Texture2D__ctor_m3598323350(L_3, 1, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_stateLabelBackground_26(L_3);
		Texture2D_t3542995729 * L_4 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_26();
		Color_t2020392075  L_5 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Texture2D_SetPixel_m609991514(L_4, 0, 0, L_5, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_6 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_26();
		NullCheck(L_6);
		Texture2D_Apply_m3543341930(L_6, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_7 = (GUIStyle_t1799908754 *)il2cpp_codegen_object_new(GUIStyle_t1799908754_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m3665892801(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		GUIStyle_t1799908754 * L_8 = V_0;
		NullCheck(L_8);
		GUIStyleState_t3801000545 * L_9 = GUIStyle_get_normal_m2789468942(L_8, /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_10 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelBackground_26();
		NullCheck(L_9);
		GUIStyleState_set_background_m3931679679(L_9, L_10, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_11 = V_0;
		NullCheck(L_11);
		GUIStyleState_t3801000545 * L_12 = GUIStyle_get_normal_m2789468942(L_11, /*hidden argument*/NULL);
		Color_t2020392075  L_13 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		GUIStyleState_set_textColor_m3970174237(L_12, L_13, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_14 = V_0;
		float L_15 = __this->get_labelScale_15();
		NullCheck(L_14);
		GUIStyle_set_fontSize_m4015341543(L_14, (((int32_t)((int32_t)((float)((float)(10.0f)*(float)L_15))))), /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_16 = V_0;
		NullCheck(L_16);
		GUIStyle_set_alignment_m1024943876(L_16, 3, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_17 = V_0;
		RectOffset_t3387826427 * L_18 = (RectOffset_t3387826427 *)il2cpp_codegen_object_new(RectOffset_t3387826427_il2cpp_TypeInfo_var);
		RectOffset__ctor_m4133355596(L_18, 4, 4, 1, 1, /*hidden argument*/NULL);
		NullCheck(L_17);
		GUIStyle_set_padding_m3722809255(L_17, L_18, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_19 = V_0;
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_stateLabelStyle_25(L_19);
		float L_20 = __this->get_labelScale_15();
		__this->set_initLabelScale_27(L_20);
		return;
	}
}
// System.Void PlayMakerGUI::DrawStateLabels()
extern "C"  void PlayMakerGUI_DrawStateLabels_m1092711642 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DrawStateLabels_m1092711642_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	PlayMakerFSM_t437737208 * V_2 = NULL;
	int32_t V_3 = 0;
	PlayMakerFSM_t437737208 * V_4 = NULL;
	List_1_t4101825636 * G_B7_0 = NULL;
	List_1_t4101825636 * G_B6_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_16();
		NullCheck(L_0);
		List_1_Clear_m3348543408(L_0, /*hidden argument*/List_1_Clear_m3348543408_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_1 = PlayMakerFSM_get_FsmList_m5507309(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = List_1_get_Count_m3488027205(L_1, /*hidden argument*/List_1_get_Count_m3488027205_MethodInfo_var);
		V_0 = L_2;
		V_1 = 0;
		goto IL_003c;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_3 = PlayMakerFSM_get_FsmList_m5507309(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		NullCheck(L_3);
		PlayMakerFSM_t437737208 * L_5 = List_1_get_Item_m3632092958(L_3, L_4, /*hidden argument*/List_1_get_Item_m3632092958_MethodInfo_var);
		V_2 = L_5;
		PlayMakerFSM_t437737208 * L_6 = V_2;
		NullCheck(L_6);
		bool L_7 = PlayMakerFSM_get_Active_m2477371780(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_8 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_16();
		PlayMakerFSM_t437737208 * L_9 = V_2;
		NullCheck(L_8);
		List_1_Add_m1863766553(L_8, L_9, /*hidden argument*/List_1_Add_m1863766553_MethodInfo_var);
	}

IL_0038:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_13 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_16();
		Comparison_1_t1699476059 * L_14 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31();
		G_B6_0 = L_13;
		if (L_14)
		{
			G_B7_0 = L_13;
			goto IL_005d;
		}
	}
	{
		IntPtr_t L_15;
		L_15.set_m_value_0((void*)(void*)PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m2467571249_MethodInfo_var);
		Comparison_1_t1699476059 * L_16 = (Comparison_1_t1699476059 *)il2cpp_codegen_object_new(Comparison_1_t1699476059_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m3162477570(L_16, NULL, L_15, /*hidden argument*/Comparison_1__ctor_m3162477570_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31(L_16);
		G_B7_0 = G_B6_0;
	}

IL_005d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Comparison_1_t1699476059 * L_17 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_CSU24U3CU3E9__CachedAnonymousMethodDelegate2_31();
		NullCheck(G_B7_0);
		List_1_Sort_m2622171247(G_B7_0, L_17, /*hidden argument*/List_1_Sort_m2622171247_MethodInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_labelGameObject_17((GameObject_t1756533147 *)NULL);
		List_1_t4101825636 * L_18 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_16();
		NullCheck(L_18);
		int32_t L_19 = List_1_get_Count_m3488027205(L_18, /*hidden argument*/List_1_get_Count_m3488027205_MethodInfo_var);
		V_0 = L_19;
		V_3 = 0;
		goto IL_00a3;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_20 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SortedFsmList_16();
		int32_t L_21 = V_3;
		NullCheck(L_20);
		PlayMakerFSM_t437737208 * L_22 = List_1_get_Item_m3632092958(L_20, L_21, /*hidden argument*/List_1_get_Item_m3632092958_MethodInfo_var);
		V_4 = L_22;
		PlayMakerFSM_t437737208 * L_23 = V_4;
		NullCheck(L_23);
		Fsm_t917886356 * L_24 = PlayMakerFSM_get_Fsm_m3359411247(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		bool L_25 = Fsm_get_ShowStateLabel_m3886429386(L_24, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_009f;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_26 = V_4;
		PlayMakerGUI_DrawStateLabel_m1328883389(__this, L_26, /*hidden argument*/NULL);
	}

IL_009f:
	{
		int32_t L_27 = V_3;
		V_3 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_28 = V_3;
		int32_t L_29 = V_0;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_007c;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerGUI::DrawStateLabel(PlayMakerFSM)
extern "C"  void PlayMakerGUI_DrawStateLabel_m1328883389 (PlayMakerGUI_t2662579489 * __this, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DrawStateLabel_m1328883389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t2020392075  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Color_t2020392075  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Color_t2020392075  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Rect_t3681755626  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Rect_t3681755626  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelStyle_25();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		float L_1 = __this->get_initLabelScale_27();
		float L_2 = __this->get_labelScale_15();
		float L_3 = fabsf(((float)((float)L_1-(float)L_2)));
		if ((!(((float)L_3) > ((float)(0.1f)))))
		{
			goto IL_0026;
		}
	}

IL_0020:
	{
		PlayMakerGUI_InitLabelStyle_m2898018055(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		Camera_t189460977 * L_4 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		return;
	}

IL_0034:
	{
		PlayMakerFSM_t437737208 * L_6 = ___fsm0;
		NullCheck(L_6);
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m3105766835(L_6, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		return;
	}

IL_0047:
	{
		PlayMakerFSM_t437737208 * L_10 = ___fsm0;
		NullCheck(L_10);
		GameObject_t1756533147 * L_11 = Component_get_gameObject_m3105766835(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_12 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_labelGameObject_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_13 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		float L_14 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_fsmLabelIndex_18();
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_fsmLabelIndex_18(((float)((float)L_14+(float)(1.0f))));
		goto IL_0080;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_fsmLabelIndex_18((0.0f));
		PlayMakerFSM_t437737208 * L_15 = ___fsm0;
		NullCheck(L_15);
		GameObject_t1756533147 * L_16 = Component_get_gameObject_m3105766835(L_15, /*hidden argument*/NULL);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_labelGameObject_17(L_16);
	}

IL_0080:
	{
		PlayMakerFSM_t437737208 * L_17 = ___fsm0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		String_t* L_18 = PlayMakerGUI_GenerateStateLabel_m2774892031(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0090;
		}
	}
	{
		return;
	}

IL_0090:
	{
		Initobj (Vector2_t2243707579_il2cpp_TypeInfo_var, (&V_1));
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GUIContent_t4210063000 * L_21 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_labelContent_5();
		String_t* L_22 = V_0;
		NullCheck(L_21);
		GUIContent_set_text_m1170206441(L_21, L_22, /*hidden argument*/NULL);
		GUIStyle_t1799908754 * L_23 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelStyle_25();
		GUIContent_t4210063000 * L_24 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_labelContent_5();
		NullCheck(L_23);
		Vector2_t2243707579  L_25 = GUIStyle_CalcSize_m4254746879(L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = (&V_2)->get_x_0();
		float L_27 = __this->get_labelScale_15();
		float L_28 = __this->get_labelScale_15();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_29 = Mathf_Clamp_m2354025655(NULL /*static, unused*/, L_26, ((float)((float)(10.0f)*(float)L_27)), ((float)((float)(200.0f)*(float)L_28)), /*hidden argument*/NULL);
		(&V_2)->set_x_0(L_29);
		bool L_30 = __this->get_GUITextureStateLabels_10();
		if (!L_30)
		{
			goto IL_016c;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_31 = ___fsm0;
		NullCheck(L_31);
		GUITexture_t1909122990 * L_32 = PlayMakerFSM_get_GuiTexture_m2496936866(L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_32, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_016c;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_34 = ___fsm0;
		NullCheck(L_34);
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m3105766835(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t2243707580  L_37 = Transform_get_position_m1104419803(L_36, /*hidden argument*/NULL);
		float L_38 = L_37.get_x_1();
		int32_t L_39 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t437737208 * L_40 = ___fsm0;
		NullCheck(L_40);
		GUITexture_t1909122990 * L_41 = PlayMakerFSM_get_GuiTexture_m2496936866(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Rect_t3681755626  L_42 = GUITexture_get_pixelInset_m1273695445(L_41, /*hidden argument*/NULL);
		V_9 = L_42;
		float L_43 = Rect_get_x_m1393582490((&V_9), /*hidden argument*/NULL);
		(&V_1)->set_x_0(((float)((float)((float)((float)L_38*(float)(((float)((float)L_39)))))+(float)L_43)));
		PlayMakerFSM_t437737208 * L_44 = ___fsm0;
		NullCheck(L_44);
		GameObject_t1756533147 * L_45 = Component_get_gameObject_m3105766835(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = GameObject_get_transform_m909382139(L_45, /*hidden argument*/NULL);
		NullCheck(L_46);
		Vector3_t2243707580  L_47 = Transform_get_position_m1104419803(L_46, /*hidden argument*/NULL);
		float L_48 = L_47.get_y_2();
		int32_t L_49 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t437737208 * L_50 = ___fsm0;
		NullCheck(L_50);
		GUITexture_t1909122990 * L_51 = PlayMakerFSM_get_GuiTexture_m2496936866(L_50, /*hidden argument*/NULL);
		NullCheck(L_51);
		Rect_t3681755626  L_52 = GUITexture_get_pixelInset_m1273695445(L_51, /*hidden argument*/NULL);
		V_10 = L_52;
		float L_53 = Rect_get_y_m1393582395((&V_10), /*hidden argument*/NULL);
		(&V_1)->set_y_1(((float)((float)((float)((float)L_48*(float)(((float)((float)L_49)))))+(float)L_53)));
		goto IL_0260;
	}

IL_016c:
	{
		bool L_54 = __this->get_GUITextStateLabels_11();
		if (!L_54)
		{
			goto IL_01cd;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_55 = ___fsm0;
		NullCheck(L_55);
		GUIText_t2411476300 * L_56 = PlayMakerFSM_get_GuiText_m3709088712(L_55, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_57 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_56, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01cd;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_58 = ___fsm0;
		NullCheck(L_58);
		GameObject_t1756533147 * L_59 = Component_get_gameObject_m3105766835(L_58, /*hidden argument*/NULL);
		NullCheck(L_59);
		Transform_t3275118058 * L_60 = GameObject_get_transform_m909382139(L_59, /*hidden argument*/NULL);
		NullCheck(L_60);
		Vector3_t2243707580  L_61 = Transform_get_position_m1104419803(L_60, /*hidden argument*/NULL);
		float L_62 = L_61.get_x_1();
		int32_t L_63 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_x_0(((float)((float)L_62*(float)(((float)((float)L_63))))));
		PlayMakerFSM_t437737208 * L_64 = ___fsm0;
		NullCheck(L_64);
		GameObject_t1756533147 * L_65 = Component_get_gameObject_m3105766835(L_64, /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_t3275118058 * L_66 = GameObject_get_transform_m909382139(L_65, /*hidden argument*/NULL);
		NullCheck(L_66);
		Vector3_t2243707580  L_67 = Transform_get_position_m1104419803(L_66, /*hidden argument*/NULL);
		float L_68 = L_67.get_y_2();
		int32_t L_69 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_1)->set_y_1(((float)((float)L_68*(float)(((float)((float)L_69))))));
		goto IL_0260;
	}

IL_01cd:
	{
		bool L_70 = __this->get_filterLabelsWithDistance_12();
		if (!L_70)
		{
			goto IL_01ff;
		}
	}
	{
		Camera_t189460977 * L_71 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_t3275118058 * L_72 = Component_get_transform_m2697483695(L_71, /*hidden argument*/NULL);
		NullCheck(L_72);
		Vector3_t2243707580  L_73 = Transform_get_position_m1104419803(L_72, /*hidden argument*/NULL);
		PlayMakerFSM_t437737208 * L_74 = ___fsm0;
		NullCheck(L_74);
		Transform_t3275118058 * L_75 = Component_get_transform_m2697483695(L_74, /*hidden argument*/NULL);
		NullCheck(L_75);
		Vector3_t2243707580  L_76 = Transform_get_position_m1104419803(L_75, /*hidden argument*/NULL);
		float L_77 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_73, L_76, /*hidden argument*/NULL);
		V_3 = L_77;
		float L_78 = V_3;
		float L_79 = __this->get_maxLabelDistance_13();
		if ((!(((float)L_78) > ((float)L_79))))
		{
			goto IL_01ff;
		}
	}
	{
		return;
	}

IL_01ff:
	{
		Camera_t189460977 * L_80 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_80);
		Transform_t3275118058 * L_81 = Component_get_transform_m2697483695(L_80, /*hidden argument*/NULL);
		PlayMakerFSM_t437737208 * L_82 = ___fsm0;
		NullCheck(L_82);
		Transform_t3275118058 * L_83 = Component_get_transform_m2697483695(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		Vector3_t2243707580  L_84 = Transform_get_position_m1104419803(L_83, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector3_t2243707580  L_85 = Transform_InverseTransformPoint_m2648491174(L_81, L_84, /*hidden argument*/NULL);
		V_4 = L_85;
		float L_86 = (&V_4)->get_z_3();
		if ((!(((float)L_86) <= ((float)(0.0f)))))
		{
			goto IL_022a;
		}
	}
	{
		return;
	}

IL_022a:
	{
		Camera_t189460977 * L_87 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		PlayMakerFSM_t437737208 * L_88 = ___fsm0;
		NullCheck(L_88);
		Transform_t3275118058 * L_89 = Component_get_transform_m2697483695(L_88, /*hidden argument*/NULL);
		NullCheck(L_89);
		Vector3_t2243707580  L_90 = Transform_get_position_m1104419803(L_89, /*hidden argument*/NULL);
		NullCheck(L_87);
		Vector3_t2243707580  L_91 = Camera_WorldToScreenPoint_m638747266(L_87, L_90, /*hidden argument*/NULL);
		Vector2_t2243707579  L_92 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		V_1 = L_92;
		Vector2_t2243707579 * L_93 = (&V_1);
		float L_94 = L_93->get_x_0();
		float L_95 = (&V_2)->get_x_0();
		L_93->set_x_0(((float)((float)L_94-(float)((float)((float)L_95*(float)(0.5f))))));
	}

IL_0260:
	{
		int32_t L_96 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_97 = (&V_1)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		float L_98 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_fsmLabelIndex_18();
		float L_99 = __this->get_labelScale_15();
		(&V_1)->set_y_1(((float)((float)((float)((float)(((float)((float)L_96)))-(float)L_97))-(float)((float)((float)((float)((float)L_98*(float)(15.0f)))*(float)L_99)))));
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Color_t2020392075  L_100 = GUI_get_backgroundColor_m1228381085(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_100;
		Color_t2020392075  L_101 = GUI_get_color_m1234367343(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_101;
		V_7 = 0;
		PlayMakerFSM_t437737208 * L_102 = ___fsm0;
		NullCheck(L_102);
		Fsm_t917886356 * L_103 = PlayMakerFSM_get_Fsm_m3359411247(L_102, /*hidden argument*/NULL);
		NullCheck(L_103);
		FsmState_t1643911659 * L_104 = Fsm_get_ActiveState_m1540121605(L_103, /*hidden argument*/NULL);
		if (!L_104)
		{
			goto IL_02b8;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_105 = ___fsm0;
		NullCheck(L_105);
		Fsm_t917886356 * L_106 = PlayMakerFSM_get_Fsm_m3359411247(L_105, /*hidden argument*/NULL);
		NullCheck(L_106);
		FsmState_t1643911659 * L_107 = Fsm_get_ActiveState_m1540121605(L_106, /*hidden argument*/NULL);
		NullCheck(L_107);
		int32_t L_108 = FsmState_get_ColorIndex_m4175238002(L_107, /*hidden argument*/NULL);
		V_7 = L_108;
	}

IL_02b8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_109 = PlayMakerPrefs_get_Colors_m3543534341(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_110 = V_7;
		NullCheck(L_109);
		V_8 = (*(Color_t2020392075 *)((L_109)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_110))));
		float L_111 = (&V_8)->get_r_0();
		float L_112 = (&V_8)->get_g_1();
		float L_113 = (&V_8)->get_b_2();
		Color_t2020392075  L_114;
		memset(&L_114, 0, sizeof(L_114));
		Color__ctor_m1909920690(&L_114, L_111, L_112, L_113, (0.5f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_backgroundColor_m1176185368(NULL /*static, unused*/, L_114, /*hidden argument*/NULL);
		Color_t2020392075  L_115 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m4064322821(NULL /*static, unused*/, L_115, /*hidden argument*/NULL);
		float L_116 = (&V_1)->get_x_0();
		float L_117 = (&V_1)->get_y_1();
		float L_118 = (&V_2)->get_x_0();
		float L_119 = (&V_2)->get_y_1();
		Rect_t3681755626  L_120;
		memset(&L_120, 0, sizeof(L_120));
		Rect__ctor_m1220545469(&L_120, L_116, L_117, L_118, L_119, /*hidden argument*/NULL);
		String_t* L_121 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GUIStyle_t1799908754 * L_122 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_stateLabelStyle_25();
		GUI_Label_m2231582000(NULL /*static, unused*/, L_120, L_121, L_122, /*hidden argument*/NULL);
		Color_t2020392075  L_123 = V_5;
		GUI_set_backgroundColor_m1176185368(NULL /*static, unused*/, L_123, /*hidden argument*/NULL);
		Color_t2020392075  L_124 = V_6;
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_124, /*hidden argument*/NULL);
		return;
	}
}
// System.String PlayMakerGUI::GenerateStateLabel(PlayMakerFSM)
extern "C"  String_t* PlayMakerGUI_GenerateStateLabel_m2774892031 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___fsm0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_GenerateStateLabel_m2774892031_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSM_t437737208 * L_0 = ___fsm0;
		NullCheck(L_0);
		Fsm_t917886356 * L_1 = PlayMakerFSM_get_Fsm_m3359411247(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		FsmState_t1643911659 * L_2 = Fsm_get_ActiveState_m1540121605(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return _stringLiteral2625992742;
	}

IL_0013:
	{
		PlayMakerFSM_t437737208 * L_3 = ___fsm0;
		NullCheck(L_3);
		Fsm_t917886356 * L_4 = PlayMakerFSM_get_Fsm_m3359411247(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		FsmState_t1643911659 * L_5 = Fsm_get_ActiveState_m1540121605(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = FsmState_get_Name_m2457110913(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PlayMakerGUI::Awake()
extern "C"  void PlayMakerGUI_Awake_m3750694929 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_Awake_m3750694929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_instance_19(__this);
		return;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral1556299699, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerGUI::OnEnable()
extern "C"  void PlayMakerGUI_OnEnable_m857055774 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnGUI()
extern "C"  void PlayMakerGUI_OnGUI_m507334058 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnGUI_m507334058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	int32_t V_2 = 0;
	Fsm_t917886356 * V_3 = NULL;
	Matrix4x4_t2933234003  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Rect_t3681755626  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B26_0 = 0;
	{
		bool L_0 = __this->get_enableGUILayout_7();
		MonoBehaviour_set_useGUILayout_m2666356651(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_1 = PlayMakerGUI_get_GUISkin_m2441732861(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		GUISkin_t1436893342 * L_3 = PlayMakerGUI_get_GUISkin_m2441732861(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_skin_m3391676555(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Color_t2020392075  L_4 = PlayMakerGUI_get_GUIColor_m3278185336(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_color_m3547334264(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Color_t2020392075  L_5 = PlayMakerGUI_get_GUIBackgroundColor_m2538762796(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_backgroundColor_m1176185368(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Color_t2020392075  L_6 = PlayMakerGUI_get_GUIContentColor_m1794729359(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_contentColor_m4064322821(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		bool L_7 = __this->get_previewOnGUI_6();
		if (!L_7)
		{
			goto IL_0056;
		}
	}
	{
		bool L_8 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_DoEditGUI_m2014605058(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_9 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		NullCheck(L_9);
		List_1_Clear_m3348543408(L_9, /*hidden argument*/List_1_Clear_m3348543408_MethodInfo_var);
		List_1_t4101825636 * L_10 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerFSM_t437737208_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_11 = PlayMakerFSM_get_FsmList_m5507309(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_AddRange_m4240858977(L_10, L_11, /*hidden argument*/List_1_AddRange_m4240858977_MethodInfo_var);
		V_0 = 0;
		goto IL_00ee;
	}

IL_0073:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_12 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		PlayMakerFSM_t437737208 * L_14 = List_1_get_Item_m3632092958(L_12, L_13, /*hidden argument*/List_1_get_Item_m3632092958_MethodInfo_var);
		V_1 = L_14;
		PlayMakerFSM_t437737208 * L_15 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_17 = V_1;
		NullCheck(L_17);
		bool L_18 = PlayMakerFSM_get_Active_m2477371780(L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_19 = V_1;
		NullCheck(L_19);
		Fsm_t917886356 * L_20 = PlayMakerFSM_get_Fsm_m3359411247(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		FsmState_t1643911659 * L_21 = Fsm_get_ActiveState_m1540121605(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_22 = V_1;
		NullCheck(L_22);
		Fsm_t917886356 * L_23 = PlayMakerFSM_get_Fsm_m3359411247(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = Fsm_get_HandleOnGUI_m1667477422(L_23, /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00ea;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_25 = V_1;
		NullCheck(L_25);
		Fsm_t917886356 * L_26 = PlayMakerFSM_get_Fsm_m3359411247(L_25, /*hidden argument*/NULL);
		PlayMakerGUI_CallOnGUI_m3101561124(__this, L_26, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_00d7;
	}

IL_00ba:
	{
		PlayMakerFSM_t437737208 * L_27 = V_1;
		NullCheck(L_27);
		Fsm_t917886356 * L_28 = PlayMakerFSM_get_Fsm_m3359411247(L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		List_1_t287007488 * L_29 = Fsm_get_SubFsmList_m106111173(L_28, /*hidden argument*/NULL);
		int32_t L_30 = V_2;
		NullCheck(L_29);
		Fsm_t917886356 * L_31 = List_1_get_Item_m2255084070(L_29, L_30, /*hidden argument*/List_1_get_Item_m2255084070_MethodInfo_var);
		V_3 = L_31;
		Fsm_t917886356 * L_32 = V_3;
		PlayMakerGUI_CallOnGUI_m3101561124(__this, L_32, /*hidden argument*/NULL);
		int32_t L_33 = V_2;
		V_2 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_34 = V_2;
		PlayMakerFSM_t437737208 * L_35 = V_1;
		NullCheck(L_35);
		Fsm_t917886356 * L_36 = PlayMakerFSM_get_Fsm_m3359411247(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		List_1_t287007488 * L_37 = Fsm_get_SubFsmList_m106111173(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Count_m2965087059(L_37, /*hidden argument*/List_1_get_Count_m2965087059_MethodInfo_var);
		if ((((int32_t)L_34) < ((int32_t)L_38)))
		{
			goto IL_00ba;
		}
	}

IL_00ea:
	{
		int32_t L_39 = V_0;
		V_0 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_00ee:
	{
		int32_t L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		List_1_t4101825636 * L_41 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_fsmList_3();
		NullCheck(L_41);
		int32_t L_42 = List_1_get_Count_m3488027205(L_41, /*hidden argument*/List_1_get_Count_m3488027205_MethodInfo_var);
		if ((((int32_t)L_40) < ((int32_t)L_42)))
		{
			goto IL_0073;
		}
	}
	{
		bool L_43 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01e9;
		}
	}
	{
		Event_t3028476042 * L_44 = Event_get_current_m2901774193(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45 = Event_get_type_m2426033198(L_44, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_45) == ((uint32_t)7))))
		{
			goto IL_01e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Matrix4x4_t2933234003  L_46 = GUI_get_matrix_m976981075(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_46;
		Matrix4x4_t2933234003  L_47 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUI_set_matrix_m3701966918(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Texture_t2243626319 * L_48 = PlayMakerGUI_get_MouseCursor_m803897773(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_48, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_019e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_50 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_51 = L_50.get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Texture_t2243626319 * L_52 = PlayMakerGUI_get_MouseCursor_m803897773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_52);
		int32_t L_54 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_55 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_56 = L_55.get_y_2();
		Texture_t2243626319 * L_57 = PlayMakerGUI_get_MouseCursor_m803897773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_57);
		Texture_t2243626319 * L_59 = PlayMakerGUI_get_MouseCursor_m803897773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_59);
		int32_t L_60 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_59);
		Texture_t2243626319 * L_61 = PlayMakerGUI_get_MouseCursor_m803897773(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_61);
		int32_t L_62 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_61);
		Rect__ctor_m1220545469((&V_5), ((float)((float)L_51-(float)((float)((float)(((float)((float)L_53)))*(float)(0.5f))))), ((float)((float)((float)((float)(((float)((float)L_54)))-(float)L_56))-(float)((float)((float)(((float)((float)L_58)))*(float)(0.5f))))), (((float)((float)L_60))), (((float)((float)L_62))), /*hidden argument*/NULL);
		Rect_t3681755626  L_63 = V_5;
		Texture_t2243626319 * L_64 = PlayMakerGUI_get_MouseCursor_m803897773(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_DrawTexture_m1191587896(NULL /*static, unused*/, L_63, L_64, /*hidden argument*/NULL);
	}

IL_019e:
	{
		bool L_65 = __this->get_drawStateLabels_8();
		if (!L_65)
		{
			goto IL_01b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		bool L_66 = PlayMakerGUI_get_EnableStateLabels_m3131360880(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_01b3;
		}
	}
	{
		PlayMakerGUI_DrawStateLabels_m1092711642(__this, /*hidden argument*/NULL);
	}

IL_01b3:
	{
		Matrix4x4_t2933234003  L_67 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_matrix_m3701966918(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		Matrix4x4_t2933234003  L_68 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_set_GUIMatrix_m1989428149(NULL /*static, unused*/, L_68, /*hidden argument*/NULL);
		bool L_69 = __this->get_controlMouseCursor_14();
		if (!L_69)
		{
			goto IL_01e9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		bool L_70 = PlayMakerGUI_get_LockCursor_m2699961842(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_70)
		{
			goto IL_01d6;
		}
	}
	{
		G_B26_0 = 0;
		goto IL_01d7;
	}

IL_01d6:
	{
		G_B26_0 = 1;
	}

IL_01d7:
	{
		Cursor_set_lockState_m387168319(NULL /*static, unused*/, G_B26_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		bool L_71 = PlayMakerGUI_get_HideCursor_m1761535307(NULL /*static, unused*/, /*hidden argument*/NULL);
		Cursor_set_visible_m860533511(NULL /*static, unused*/, (bool)((((int32_t)L_71) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_01e9:
	{
		return;
	}
}
// System.Void PlayMakerGUI::CallOnGUI(HutongGames.PlayMaker.Fsm)
extern "C"  void PlayMakerGUI_CallOnGUI_m3101561124 (PlayMakerGUI_t2662579489 * __this, Fsm_t917886356 * ___fsm0, const MethodInfo* method)
{
	FsmStateActionU5BU5D_t1497896004* V_0 = NULL;
	FsmStateAction_t2862378169 * V_1 = NULL;
	FsmStateActionU5BU5D_t1497896004* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Fsm_t917886356 * L_0 = ___fsm0;
		NullCheck(L_0);
		FsmState_t1643911659 * L_1 = Fsm_get_ActiveState_m1540121605(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Fsm_t917886356 * L_2 = ___fsm0;
		NullCheck(L_2);
		FsmState_t1643911659 * L_3 = Fsm_get_ActiveState_m1540121605(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		FsmStateActionU5BU5D_t1497896004* L_4 = FsmState_get_Actions_m2945510972(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmStateActionU5BU5D_t1497896004* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0030;
	}

IL_001a:
	{
		FsmStateActionU5BU5D_t1497896004* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		FsmStateAction_t2862378169 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		FsmStateAction_t2862378169 * L_10 = V_1;
		NullCheck(L_10);
		bool L_11 = FsmStateAction_get_Active_m2969348681(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_002c;
		}
	}
	{
		FsmStateAction_t2862378169 * L_12 = V_1;
		NullCheck(L_12);
		VirtActionInvoker0::Invoke(34 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_12);
	}

IL_002c:
	{
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_14 = V_3;
		FsmStateActionU5BU5D_t1497896004* L_15 = V_2;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_001a;
		}
	}

IL_0036:
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnDisable()
extern "C"  void PlayMakerGUI_OnDisable_m2748493449 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnDisable_m2748493449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		PlayMakerGUI_t2662579489 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_instance_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_instance_19((PlayMakerGUI_t2662579489 *)NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void PlayMakerGUI::DoEditGUI()
extern "C"  void PlayMakerGUI_DoEditGUI_m2014605058 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_DoEditGUI_m2014605058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmState_t1643911659 * V_0 = NULL;
	FsmStateActionU5BU5D_t1497896004* V_1 = NULL;
	FsmStateAction_t2862378169 * V_2 = NULL;
	FsmStateActionU5BU5D_t1497896004* V_3 = NULL;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Fsm_t917886356 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		if (!L_0)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Fsm_t917886356 * L_1 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		NullCheck(L_1);
		bool L_2 = Fsm_get_HandleOnGUI_m1667477422(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Fsm_t917886356 * L_3 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		NullCheck(L_3);
		FsmState_t1643911659 * L_4 = Fsm_get_EditState_m1392825861(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		FsmState_t1643911659 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		FsmState_t1643911659 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = FsmState_get_IsInitialized_m297158777(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		FsmState_t1643911659 * L_8 = V_0;
		NullCheck(L_8);
		FsmStateActionU5BU5D_t1497896004* L_9 = FsmState_get_Actions_m2945510972(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		FsmStateActionU5BU5D_t1497896004* L_10 = V_1;
		V_3 = L_10;
		V_4 = 0;
		goto IL_0050;
	}

IL_0037:
	{
		FsmStateActionU5BU5D_t1497896004* L_11 = V_3;
		int32_t L_12 = V_4;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		FsmStateAction_t2862378169 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_2 = L_14;
		FsmStateAction_t2862378169 * L_15 = V_2;
		NullCheck(L_15);
		bool L_16 = FsmStateAction_get_Enabled_m3252509844(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_004a;
		}
	}
	{
		FsmStateAction_t2862378169 * L_17 = V_2;
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(34 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_17);
	}

IL_004a:
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_19 = V_4;
		FsmStateActionU5BU5D_t1497896004* L_20 = V_3;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
		{
			goto IL_0037;
		}
	}

IL_0057:
	{
		return;
	}
}
// System.Void PlayMakerGUI::OnApplicationQuit()
extern "C"  void PlayMakerGUI_OnApplicationQuit_m1056069416 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_OnApplicationQuit_m1056069416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_instance_19((PlayMakerGUI_t2662579489 *)NULL);
		return;
	}
}
// System.Void PlayMakerGUI::.ctor()
extern "C"  void PlayMakerGUI__ctor_m83179638 (PlayMakerGUI_t2662579489 * __this, const MethodInfo* method)
{
	{
		__this->set_previewOnGUI_6((bool)1);
		__this->set_enableGUILayout_7((bool)1);
		__this->set_drawStateLabels_8((bool)1);
		__this->set_GUITextureStateLabels_10((bool)1);
		__this->set_GUITextStateLabels_11((bool)1);
		__this->set_maxLabelDistance_13((10.0f));
		__this->set_controlMouseCursor_14((bool)1);
		__this->set_labelScale_15((1.0f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PlayMakerGUI::<DrawStateLabels>b__1(PlayMakerFSM,PlayMakerFSM)
extern "C"  int32_t PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m2467571249 (Il2CppObject * __this /* static, unused */, PlayMakerFSM_t437737208 * ___x0, PlayMakerFSM_t437737208 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI_U3CDrawStateLabelsU3Eb__1_m2467571249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSM_t437737208 * L_0 = ___x0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		PlayMakerFSM_t437737208 * L_3 = ___y1;
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5 = Object_get_name_m2079638459(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		int32_t L_6 = String_CompareOrdinal_m3421681586(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void PlayMakerGUI::.cctor()
extern "C"  void PlayMakerGUI__cctor_m2174270851 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerGUI__cctor_m2174270851_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4101825636 * L_0 = (List_1_t4101825636 *)il2cpp_codegen_object_new(List_1_t4101825636_il2cpp_TypeInfo_var);
		List_1__ctor_m2230797533(L_0, /*hidden argument*/List_1__ctor_m2230797533_MethodInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_fsmList_3(L_0);
		GUIContent_t4210063000 * L_1 = (GUIContent_t4210063000 *)il2cpp_codegen_object_new(GUIContent_t4210063000_il2cpp_TypeInfo_var);
		GUIContent__ctor_m3889310883(L_1, /*hidden argument*/NULL);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_labelContent_5(L_1);
		List_1_t4101825636 * L_2 = (List_1_t4101825636 *)il2cpp_codegen_object_new(List_1_t4101825636_il2cpp_TypeInfo_var);
		List_1__ctor_m2230797533(L_2, /*hidden argument*/List_1__ctor_m2230797533_MethodInfo_var);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_SortedFsmList_16(L_2);
		Color_t2020392075  L_3 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiColor_21(L_3);
		Color_t2020392075  L_4 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiBackgroundColor_22(L_4);
		Color_t2020392075  L_5 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiContentColor_23(L_5);
		Matrix4x4_t2933234003  L_6 = Matrix4x4_get_identity_m3039560904(NULL /*static, unused*/, /*hidden argument*/NULL);
		((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->set_guiMatrix_24(L_6);
		return;
	}
}
// System.Void PlayMakerJointBreak::OnJointBreak(System.Single)
extern "C"  void PlayMakerJointBreak_OnJointBreak_m1155960707 (PlayMakerJointBreak_t1658177799 * __this, float ___breakForce0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleJointBreak_m3453435899(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		float L_11 = ___breakForce0;
		NullCheck(L_10);
		Fsm_OnJointBreak_m3216038832(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerJointBreak::.ctor()
extern "C"  void PlayMakerJointBreak__ctor_m3305116438 (PlayMakerJointBreak_t1658177799 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerJointBreak2D::OnJointBreak2D(UnityEngine.Joint2D)
extern "C"  void PlayMakerJointBreak2D_OnJointBreak2D_m1506345733 (PlayMakerJointBreak2D_t3128675709 * __this, Joint2D_t854621618 * ___brokenJoint0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleJointBreak2D_m466287097(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Joint2D_t854621618 * L_11 = ___brokenJoint0;
		NullCheck(L_10);
		Fsm_OnJointBreak2D_m2409422232(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerJointBreak2D::.ctor()
extern "C"  void PlayMakerJointBreak2D__ctor_m3549857740 (PlayMakerJointBreak2D_t3128675709 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseEnter()
extern "C"  void PlayMakerMouseEvents_OnMouseEnter_m2895234989 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseEnter_m2895234989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseEnter_m3676296(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseDown()
extern "C"  void PlayMakerMouseEvents_OnMouseDown_m3723689487 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseDown_m3723689487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseDown_m2455071712(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseUp()
extern "C"  void PlayMakerMouseEvents_OnMouseUp_m1713886120 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseUp_m1713886120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseUp_m2558660009(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		Fsm_set_LastClickedObject_m3675016442(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_13 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseUpAsButton()
extern "C"  void PlayMakerMouseEvents_OnMouseUpAsButton_m3605824020 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseUpAsButton_m3605824020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0039;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseUpAsButton_m2531723093(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Fsm_t917886356_il2cpp_TypeInfo_var);
		Fsm_set_LastClickedObject_m3675016442(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0035:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_13 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseExit()
extern "C"  void PlayMakerMouseEvents_OnMouseExit_m3008400505 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseExit_m3008400505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseExit_m2613472564(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseDrag()
extern "C"  void PlayMakerMouseEvents_OnMouseDrag_m1969676181 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseDrag_m1969676181_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseDrag_m3004494826(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::OnMouseOver()
extern "C"  void PlayMakerMouseEvents_OnMouseOver_m2281620839 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerMouseEvents_OnMouseOver_m2281620839_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_002e;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = Fsm_get_MouseEvents_m3020071934(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_7 = V_1;
		NullCheck(L_7);
		Fsm_t917886356 * L_8 = PlayMakerFSM_get_Fsm_m3359411247(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FsmEvent_t1258573736_il2cpp_TypeInfo_var);
		FsmEvent_t1258573736 * L_9 = FsmEvent_get_MouseOver_m4010778446(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		Fsm_Event_m4079224475(L_8, L_9, /*hidden argument*/NULL);
	}

IL_002a:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_12 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerMouseEvents::.ctor()
extern "C"  void PlayMakerMouseEvents__ctor_m649736457 (PlayMakerMouseEvents_t3625748060 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerOnGUI::Start()
extern "C"  void PlayMakerOnGUI_Start_m3270530941 (PlayMakerOnGUI_t2031863694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_Start_m3270530941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSM_t437737208 * L_0 = __this->get_playMakerFSM_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_2 = __this->get_playMakerFSM_2();
		NullCheck(L_2);
		Fsm_t917886356 * L_3 = PlayMakerFSM_get_Fsm_m3359411247(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Fsm_set_HandleOnGUI_m3808117791(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::OnGUI()
extern "C"  void PlayMakerOnGUI_OnGUI_m2763967087 (PlayMakerOnGUI_t2031863694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_OnGUI_m2763967087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_previewInEditMode_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m4091950718(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		PlayMakerOnGUI_DoEditGUI_m2385310713(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		PlayMakerFSM_t437737208 * L_2 = __this->get_playMakerFSM_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_4 = __this->get_playMakerFSM_2();
		NullCheck(L_4);
		Fsm_t917886356 * L_5 = PlayMakerFSM_get_Fsm_m3359411247(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = __this->get_playMakerFSM_2();
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleOnGUI_m1667477422(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = __this->get_playMakerFSM_2();
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Fsm_OnGUI_m3260270399(L_10, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::DoEditGUI()
extern "C"  void PlayMakerOnGUI_DoEditGUI_m2385310713 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerOnGUI_DoEditGUI_m2385310713_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FsmState_t1643911659 * V_0 = NULL;
	FsmStateActionU5BU5D_t1497896004* V_1 = NULL;
	int32_t V_2 = 0;
	FsmStateAction_t2862378169 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Fsm_t917886356 * L_0 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		if (!L_0)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var);
		Fsm_t917886356 * L_1 = ((PlayMakerGUI_t2662579489_StaticFields*)PlayMakerGUI_t2662579489_il2cpp_TypeInfo_var->static_fields)->get_SelectedFSM_4();
		NullCheck(L_1);
		FsmState_t1643911659 * L_2 = Fsm_get_EditState_m1392825861(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		FsmState_t1643911659 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		FsmState_t1643911659 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = FsmState_get_IsInitialized_m297158777(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		FsmState_t1643911659 * L_6 = V_0;
		NullCheck(L_6);
		FsmStateActionU5BU5D_t1497896004* L_7 = FsmState_get_Actions_m2945510972(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		V_2 = 0;
		goto IL_003e;
	}

IL_0028:
	{
		FsmStateActionU5BU5D_t1497896004* L_8 = V_1;
		int32_t L_9 = V_2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		FsmStateAction_t2862378169 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_3 = L_11;
		FsmStateAction_t2862378169 * L_12 = V_3;
		NullCheck(L_12);
		bool L_13 = FsmStateAction_get_Active_m2969348681(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_003a;
		}
	}
	{
		FsmStateAction_t2862378169 * L_14 = V_3;
		NullCheck(L_14);
		VirtActionInvoker0::Invoke(34 /* System.Void HutongGames.PlayMaker.FsmStateAction::OnGUI() */, L_14);
	}

IL_003a:
	{
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_16 = V_2;
		FsmStateActionU5BU5D_t1497896004* L_17 = V_1;
		NullCheck(L_17);
		if ((((int32_t)L_16) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))))))
		{
			goto IL_0028;
		}
	}

IL_0044:
	{
		return;
	}
}
// System.Void PlayMakerOnGUI::.ctor()
extern "C"  void PlayMakerOnGUI__ctor_m3852551493 (PlayMakerOnGUI_t2031863694 * __this, const MethodInfo* method)
{
	{
		__this->set_previewInEditMode_3((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerParticleCollision::OnParticleCollision(UnityEngine.GameObject)
extern "C"  void PlayMakerParticleCollision_OnParticleCollision_m2510204134 (PlayMakerParticleCollision_t1230092432 * __this, GameObject_t1756533147 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleParticleCollision_m2116271836(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnParticleCollision_m784661054(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerParticleCollision::.ctor()
extern "C"  void PlayMakerParticleCollision__ctor_m2323843317 (PlayMakerParticleCollision_t1230092432 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// PlayMakerPrefs PlayMakerPrefs::get_Instance()
extern "C"  PlayMakerPrefs_t1833055544 * PlayMakerPrefs_get_Instance_m688943076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_Instance_m688943076_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t1833055544 * L_0 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		Object_t1021602117 * L_2 = Resources_Load_m2041782325(NULL /*static, unused*/, _stringLiteral2561134062, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->set_instance_2(((PlayMakerPrefs_t1833055544 *)IsInstClass(L_2, PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var)));
		PlayMakerPrefs_t1833055544 * L_3 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		PlayMakerPrefs_t1833055544 * L_5 = ScriptableObject_CreateInstance_TisPlayMakerPrefs_t1833055544_m1276350044(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisPlayMakerPrefs_t1833055544_m1276350044_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->set_instance_2(L_5);
	}

IL_0038:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t1833055544 * L_6 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		return L_6;
	}
}
// UnityEngine.Color[] PlayMakerPrefs::get_Colors()
extern "C"  ColorU5BU5D_t672350442* PlayMakerPrefs_get_Colors_m3543534341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_Colors_m3543534341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t1833055544 * L_0 = PlayMakerPrefs_get_Instance_m688943076(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ColorU5BU5D_t672350442* L_1 = L_0->get_colors_5();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_Colors(UnityEngine.Color[])
extern "C"  void PlayMakerPrefs_set_Colors_m3809722884 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_Colors_m3809722884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t1833055544 * L_0 = PlayMakerPrefs_get_Instance_m688943076(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_colors_5(L_1);
		return;
	}
}
// System.String[] PlayMakerPrefs::get_ColorNames()
extern "C"  StringU5BU5D_t1642385972* PlayMakerPrefs_get_ColorNames_m494908428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_ColorNames_m494908428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t1833055544 * L_0 = PlayMakerPrefs_get_Instance_m688943076(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringU5BU5D_t1642385972* L_1 = L_0->get_colorNames_6();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::set_ColorNames(System.String[])
extern "C"  void PlayMakerPrefs_set_ColorNames_m3843271929 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_set_ColorNames_m3843271929_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_t1833055544 * L_0 = PlayMakerPrefs_get_Instance_m688943076(NULL /*static, unused*/, /*hidden argument*/NULL);
		StringU5BU5D_t1642385972* L_1 = ___value0;
		NullCheck(L_0);
		L_0->set_colorNames_6(L_1);
		return;
	}
}
// System.Void PlayMakerPrefs::ResetDefaultColors()
extern "C"  void PlayMakerPrefs_ResetDefaultColors_m993607445 (PlayMakerPrefs_t1833055544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_ResetDefaultColors_m993607445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0038;
	}

IL_0004:
	{
		ColorU5BU5D_t672350442* L_0 = __this->get_colors_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_2 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_defaultColors_3();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		(*(Color_t2020392075 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))) = (*(Color_t2020392075 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_3))));
		StringU5BU5D_t1642385972* L_4 = __this->get_colorNames_6();
		int32_t L_5 = V_0;
		StringU5BU5D_t1642385972* L_6 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_defaultColorNames_4();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		String_t* L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_9);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (String_t*)L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0038:
	{
		int32_t L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_12 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_defaultColors_3();
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// UnityEngine.Color[] PlayMakerPrefs::get_MinimapColors()
extern "C"  ColorU5BU5D_t672350442* PlayMakerPrefs_get_MinimapColors_m2639067296 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_get_MinimapColors_m2639067296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_0 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_minimapColors_7();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_UpdateMinimapColors_m1326282697(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_1 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_minimapColors_7();
		return L_1;
	}
}
// System.Void PlayMakerPrefs::SaveChanges()
extern "C"  void PlayMakerPrefs_SaveChanges_m3585553949 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_SaveChanges_m3585553949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		PlayMakerPrefs_UpdateMinimapColors_m1326282697(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerPrefs::UpdateMinimapColors()
extern "C"  void PlayMakerPrefs_UpdateMinimapColors_m1326282697 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs_UpdateMinimapColors_m1326282697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_0 = PlayMakerPrefs_get_Colors_m3543534341(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->set_minimapColors_7(((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0059;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_1 = PlayMakerPrefs_get_Colors_m3543534341(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		V_1 = (*(Color_t2020392075 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		ColorU5BU5D_t672350442* L_3 = ((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->get_minimapColors_7();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		float L_5 = (&V_1)->get_r_0();
		float L_6 = (&V_1)->get_g_1();
		float L_7 = (&V_1)->get_b_2();
		Color_t2020392075  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m1909920690(&L_8, L_5, L_6, L_7, (0.5f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_4)))) = L_8;
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0059:
	{
		int32_t L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var);
		ColorU5BU5D_t672350442* L_11 = PlayMakerPrefs_get_Colors_m3543534341(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerPrefs::.ctor()
extern "C"  void PlayMakerPrefs__ctor_m1864640893 (PlayMakerPrefs_t1833055544 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs__ctor_m1864640893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColorU5BU5D_t672350442* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	{
		V_0 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		ColorU5BU5D_t672350442* L_0 = V_0;
		NullCheck(L_0);
		Color_t2020392075  L_1 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		ColorU5BU5D_t672350442* L_2 = V_0;
		NullCheck(L_2);
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m3811852957(&L_3, (0.545098066f), (0.670588255f), (0.9411765f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		ColorU5BU5D_t672350442* L_4 = V_0;
		NullCheck(L_4);
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m3811852957(&L_5, (0.243137255f), (0.7607843f), (0.6901961f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		ColorU5BU5D_t672350442* L_6 = V_0;
		NullCheck(L_6);
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m3811852957(&L_7, (0.431372553f), (0.7607843f), (0.243137255f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_7;
		ColorU5BU5D_t672350442* L_8 = V_0;
		NullCheck(L_8);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m3811852957(&L_9, (1.0f), (0.8745098f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))) = L_9;
		ColorU5BU5D_t672350442* L_10 = V_0;
		NullCheck(L_10);
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m3811852957(&L_11, (1.0f), (0.5529412f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))) = L_11;
		ColorU5BU5D_t672350442* L_12 = V_0;
		NullCheck(L_12);
		Color_t2020392075  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m3811852957(&L_13, (0.7607843f), (0.243137255f), (0.2509804f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))) = L_13;
		ColorU5BU5D_t672350442* L_14 = V_0;
		NullCheck(L_14);
		Color_t2020392075  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m3811852957(&L_15, (0.545098066f), (0.243137255f), (0.7607843f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))) = L_15;
		ColorU5BU5D_t672350442* L_16 = V_0;
		NullCheck(L_16);
		Color_t2020392075  L_17 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(8)))) = L_17;
		ColorU5BU5D_t672350442* L_18 = V_0;
		NullCheck(L_18);
		Color_t2020392075  L_19 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)9))))) = L_19;
		ColorU5BU5D_t672350442* L_20 = V_0;
		NullCheck(L_20);
		Color_t2020392075  L_21 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)10))))) = L_21;
		ColorU5BU5D_t672350442* L_22 = V_0;
		NullCheck(L_22);
		Color_t2020392075  L_23 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)11))))) = L_23;
		ColorU5BU5D_t672350442* L_24 = V_0;
		NullCheck(L_24);
		Color_t2020392075  L_25 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)12))))) = L_25;
		ColorU5BU5D_t672350442* L_26 = V_0;
		NullCheck(L_26);
		Color_t2020392075  L_27 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)13))))) = L_27;
		ColorU5BU5D_t672350442* L_28 = V_0;
		NullCheck(L_28);
		Color_t2020392075  L_29 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_28)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)14))))) = L_29;
		ColorU5BU5D_t672350442* L_30 = V_0;
		NullCheck(L_30);
		Color_t2020392075  L_31 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_30)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)15))))) = L_31;
		ColorU5BU5D_t672350442* L_32 = V_0;
		NullCheck(L_32);
		Color_t2020392075  L_33 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)16))))) = L_33;
		ColorU5BU5D_t672350442* L_34 = V_0;
		NullCheck(L_34);
		Color_t2020392075  L_35 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)17))))) = L_35;
		ColorU5BU5D_t672350442* L_36 = V_0;
		NullCheck(L_36);
		Color_t2020392075  L_37 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)18))))) = L_37;
		ColorU5BU5D_t672350442* L_38 = V_0;
		NullCheck(L_38);
		Color_t2020392075  L_39 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)19))))) = L_39;
		ColorU5BU5D_t672350442* L_40 = V_0;
		NullCheck(L_40);
		Color_t2020392075  L_41 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)20))))) = L_41;
		ColorU5BU5D_t672350442* L_42 = V_0;
		NullCheck(L_42);
		Color_t2020392075  L_43 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)21))))) = L_43;
		ColorU5BU5D_t672350442* L_44 = V_0;
		NullCheck(L_44);
		Color_t2020392075  L_45 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)22))))) = L_45;
		ColorU5BU5D_t672350442* L_46 = V_0;
		NullCheck(L_46);
		Color_t2020392075  L_47 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)23))))) = L_47;
		ColorU5BU5D_t672350442* L_48 = V_0;
		__this->set_colors_5(L_48);
		V_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24)));
		StringU5BU5D_t1642385972* L_49 = V_1;
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, _stringLiteral3566263623);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3566263623);
		StringU5BU5D_t1642385972* L_50 = V_1;
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, _stringLiteral2395476974);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2395476974);
		StringU5BU5D_t1642385972* L_51 = V_1;
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, _stringLiteral3343466881);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3343466881);
		StringU5BU5D_t1642385972* L_52 = V_1;
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, _stringLiteral3510846499);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3510846499);
		StringU5BU5D_t1642385972* L_53 = V_1;
		NullCheck(L_53);
		ArrayElementTypeCheck (L_53, _stringLiteral777220966);
		(L_53)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral777220966);
		StringU5BU5D_t1642385972* L_54 = V_1;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, _stringLiteral3664749912);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral3664749912);
		StringU5BU5D_t1642385972* L_55 = V_1;
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, _stringLiteral3021629811);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3021629811);
		StringU5BU5D_t1642385972* L_56 = V_1;
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, _stringLiteral4158023012);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral4158023012);
		StringU5BU5D_t1642385972* L_57 = V_1;
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, _stringLiteral371857150);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_58 = V_1;
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, _stringLiteral371857150);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_59 = V_1;
		NullCheck(L_59);
		ArrayElementTypeCheck (L_59, _stringLiteral371857150);
		(L_59)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_60 = V_1;
		NullCheck(L_60);
		ArrayElementTypeCheck (L_60, _stringLiteral371857150);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_61 = V_1;
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, _stringLiteral371857150);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_62 = V_1;
		NullCheck(L_62);
		ArrayElementTypeCheck (L_62, _stringLiteral371857150);
		(L_62)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_63 = V_1;
		NullCheck(L_63);
		ArrayElementTypeCheck (L_63, _stringLiteral371857150);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_64 = V_1;
		NullCheck(L_64);
		ArrayElementTypeCheck (L_64, _stringLiteral371857150);
		(L_64)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_65 = V_1;
		NullCheck(L_65);
		ArrayElementTypeCheck (L_65, _stringLiteral371857150);
		(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)16)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_66 = V_1;
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, _stringLiteral371857150);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)17)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_67 = V_1;
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, _stringLiteral371857150);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)18)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_68 = V_1;
		NullCheck(L_68);
		ArrayElementTypeCheck (L_68, _stringLiteral371857150);
		(L_68)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)19)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_69 = V_1;
		NullCheck(L_69);
		ArrayElementTypeCheck (L_69, _stringLiteral371857150);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)20)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_70 = V_1;
		NullCheck(L_70);
		ArrayElementTypeCheck (L_70, _stringLiteral371857150);
		(L_70)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)21)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_71 = V_1;
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, _stringLiteral371857150);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)22)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_72 = V_1;
		NullCheck(L_72);
		ArrayElementTypeCheck (L_72, _stringLiteral371857150);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)23)), (String_t*)_stringLiteral371857150);
		StringU5BU5D_t1642385972* L_73 = V_1;
		__this->set_colorNames_6(L_73);
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerPrefs::.cctor()
extern "C"  void PlayMakerPrefs__cctor_m2817732924 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerPrefs__cctor_m2817732924_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColorU5BU5D_t672350442* V_0 = NULL;
	StringU5BU5D_t1642385972* V_1 = NULL;
	{
		V_0 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)8));
		ColorU5BU5D_t672350442* L_0 = V_0;
		NullCheck(L_0);
		Color_t2020392075  L_1 = Color_get_grey_m1961362537(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		ColorU5BU5D_t672350442* L_2 = V_0;
		NullCheck(L_2);
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m3811852957(&L_3, (0.545098066f), (0.670588255f), (0.9411765f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		ColorU5BU5D_t672350442* L_4 = V_0;
		NullCheck(L_4);
		Color_t2020392075  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Color__ctor_m3811852957(&L_5, (0.243137255f), (0.7607843f), (0.6901961f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		ColorU5BU5D_t672350442* L_6 = V_0;
		NullCheck(L_6);
		Color_t2020392075  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m3811852957(&L_7, (0.431372553f), (0.7607843f), (0.243137255f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_7;
		ColorU5BU5D_t672350442* L_8 = V_0;
		NullCheck(L_8);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m3811852957(&L_9, (1.0f), (0.8745098f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)))) = L_9;
		ColorU5BU5D_t672350442* L_10 = V_0;
		NullCheck(L_10);
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m3811852957(&L_11, (1.0f), (0.5529412f), (0.1882353f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)))) = L_11;
		ColorU5BU5D_t672350442* L_12 = V_0;
		NullCheck(L_12);
		Color_t2020392075  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Color__ctor_m3811852957(&L_13, (0.7607843f), (0.243137255f), (0.2509804f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)))) = L_13;
		ColorU5BU5D_t672350442* L_14 = V_0;
		NullCheck(L_14);
		Color_t2020392075  L_15;
		memset(&L_15, 0, sizeof(L_15));
		Color__ctor_m3811852957(&L_15, (0.545098066f), (0.243137255f), (0.7607843f), /*hidden argument*/NULL);
		(*(Color_t2020392075 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)))) = L_15;
		ColorU5BU5D_t672350442* L_16 = V_0;
		((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->set_defaultColors_3(L_16);
		V_1 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)8));
		StringU5BU5D_t1642385972* L_17 = V_1;
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, _stringLiteral3566263623);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3566263623);
		StringU5BU5D_t1642385972* L_18 = V_1;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, _stringLiteral2395476974);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2395476974);
		StringU5BU5D_t1642385972* L_19 = V_1;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, _stringLiteral3343466881);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3343466881);
		StringU5BU5D_t1642385972* L_20 = V_1;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral3510846499);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3510846499);
		StringU5BU5D_t1642385972* L_21 = V_1;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral777220966);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral777220966);
		StringU5BU5D_t1642385972* L_22 = V_1;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, _stringLiteral3664749912);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral3664749912);
		StringU5BU5D_t1642385972* L_23 = V_1;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral3021629811);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3021629811);
		StringU5BU5D_t1642385972* L_24 = V_1;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral4158023012);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteral4158023012);
		StringU5BU5D_t1642385972* L_25 = V_1;
		((PlayMakerPrefs_t1833055544_StaticFields*)PlayMakerPrefs_t1833055544_il2cpp_TypeInfo_var->static_fields)->set_defaultColorNames_4(L_25);
		return;
	}
}
// System.Void PlayMakerProxyBase::Awake()
extern "C"  void PlayMakerProxyBase_Awake_m2433124823 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase_Reset_m3397845475(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerProxyBase::Reset()
extern "C"  void PlayMakerProxyBase_Reset_m3397845475 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayMakerProxyBase_Reset_m3397845475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = Component_GetComponents_TisPlayMakerFSM_t437737208_m853540674(__this, /*hidden argument*/Component_GetComponents_TisPlayMakerFSM_t437737208_m853540674_MethodInfo_var);
		__this->set_playMakerFSMs_2(L_0);
		return;
	}
}
// System.Void PlayMakerProxyBase::.ctor()
extern "C"  void PlayMakerProxyBase__ctor_m547361492 (PlayMakerProxyBase_t3141506643 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerEnter::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerEnter_OnTriggerEnter_m3165567559 (PlayMakerTriggerEnter_t2991464208 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerEnter_m790156034(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collider_t3497673348 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerEnter_m2846356605(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerEnter::.ctor()
extern "C"  void PlayMakerTriggerEnter__ctor_m2499662619 (PlayMakerTriggerEnter_t2991464208 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerEnter2D::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerEnter2D_OnTriggerEnter2D_m3902086821 (PlayMakerTriggerEnter2D_t2208085914 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerEnter2D_m172380676(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collider2D_t646061738 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerEnter2D_m774284857(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerEnter2D::.ctor()
extern "C"  void PlayMakerTriggerEnter2D__ctor_m576079909 (PlayMakerTriggerEnter2D_t2208085914 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerExit::OnTriggerExit(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerExit_OnTriggerExit_m732680115 (PlayMakerTriggerExit_t2606889942 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerExit_m362350026(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collider_t3497673348 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerExit_m3786251381(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerExit::.ctor()
extern "C"  void PlayMakerTriggerExit__ctor_m3002147931 (PlayMakerTriggerExit_t2606889942 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerExit2D::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerExit2D_OnTriggerExit2D_m1603338421 (PlayMakerTriggerExit2D_t1071223868 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerExit2D_m766085676(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collider2D_t646061738 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerExit2D_m616971321(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerExit2D::.ctor()
extern "C"  void PlayMakerTriggerExit2D__ctor_m2601655989 (PlayMakerTriggerExit2D_t1071223868 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerStay::OnTriggerStay(UnityEngine.Collider)
extern "C"  void PlayMakerTriggerStay_OnTriggerStay_m3328207047 (PlayMakerTriggerStay_t2768254945 * __this, Collider_t3497673348 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerStay_m3688276885(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collider_t3497673348 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerStay_m3815556686(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerStay::.ctor()
extern "C"  void PlayMakerTriggerStay__ctor_m221491136 (PlayMakerTriggerStay_t2768254945 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayMakerTriggerStay2D::OnTriggerStay2D(UnityEngine.Collider2D)
extern "C"  void PlayMakerTriggerStay2D_OnTriggerStay2D_m3541267045 (PlayMakerTriggerStay2D_t3506020163 * __this, Collider2D_t646061738 * ___other0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	PlayMakerFSM_t437737208 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0004:
	{
		PlayMakerFSMU5BU5D_t623924777* L_0 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		PlayMakerFSM_t437737208 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		PlayMakerFSM_t437737208 * L_4 = V_1;
		NullCheck(L_4);
		bool L_5 = PlayMakerFSM_get_Active_m2477371780(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_6 = V_1;
		NullCheck(L_6);
		Fsm_t917886356 * L_7 = PlayMakerFSM_get_Fsm_m3359411247(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Fsm_get_HandleTriggerStay2D_m2610463627(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002e;
		}
	}
	{
		PlayMakerFSM_t437737208 * L_9 = V_1;
		NullCheck(L_9);
		Fsm_t917886356 * L_10 = PlayMakerFSM_get_Fsm_m3359411247(L_9, /*hidden argument*/NULL);
		Collider2D_t646061738 * L_11 = ___other0;
		NullCheck(L_10);
		Fsm_OnTriggerStay2D_m1827537318(L_10, L_11, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_13 = V_0;
		PlayMakerFSMU5BU5D_t623924777* L_14 = ((PlayMakerProxyBase_t3141506643 *)__this)->get_playMakerFSMs_2();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		return;
	}
}
// System.Void PlayMakerTriggerStay2D::.ctor()
extern "C"  void PlayMakerTriggerStay2D__ctor_m1929906334 (PlayMakerTriggerStay2D_t3506020163 * __this, const MethodInfo* method)
{
	{
		PlayMakerProxyBase__ctor_m547361492(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
