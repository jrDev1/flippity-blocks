﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// HedgehogTeam.EasyTouch.EasyTouch
struct EasyTouch_t3578534067;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0
struct  U3CSingleOrDoubleU3Ec__Iterator0_t3365127336  : public Il2CppObject
{
public:
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::fingerIndex
	int32_t ___fingerIndex_0;
	// System.Single HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::<time2Wait>__0
	float ___U3Ctime2WaitU3E__0_1;
	// HedgehogTeam.EasyTouch.EasyTouch HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$this
	EasyTouch_t3578534067 * ___U24this_2;
	// System.Object HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 HedgehogTeam.EasyTouch.EasyTouch/<SingleOrDouble>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_fingerIndex_0() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t3365127336, ___fingerIndex_0)); }
	inline int32_t get_fingerIndex_0() const { return ___fingerIndex_0; }
	inline int32_t* get_address_of_fingerIndex_0() { return &___fingerIndex_0; }
	inline void set_fingerIndex_0(int32_t value)
	{
		___fingerIndex_0 = value;
	}

	inline static int32_t get_offset_of_U3Ctime2WaitU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t3365127336, ___U3Ctime2WaitU3E__0_1)); }
	inline float get_U3Ctime2WaitU3E__0_1() const { return ___U3Ctime2WaitU3E__0_1; }
	inline float* get_address_of_U3Ctime2WaitU3E__0_1() { return &___U3Ctime2WaitU3E__0_1; }
	inline void set_U3Ctime2WaitU3E__0_1(float value)
	{
		___U3Ctime2WaitU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t3365127336, ___U24this_2)); }
	inline EasyTouch_t3578534067 * get_U24this_2() const { return ___U24this_2; }
	inline EasyTouch_t3578534067 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(EasyTouch_t3578534067 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t3365127336, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t3365127336, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CSingleOrDoubleU3Ec__Iterator0_t3365127336, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
