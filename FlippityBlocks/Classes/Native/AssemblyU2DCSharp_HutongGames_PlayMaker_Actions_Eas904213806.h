﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchSetEnablePinch
struct  EasyTouchSetEnablePinch_t904213806  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EasyTouchSetEnablePinch::enable
	FsmBool_t664485696 * ___enable_11;

public:
	inline static int32_t get_offset_of_enable_11() { return static_cast<int32_t>(offsetof(EasyTouchSetEnablePinch_t904213806, ___enable_11)); }
	inline FsmBool_t664485696 * get_enable_11() const { return ___enable_11; }
	inline FsmBool_t664485696 ** get_address_of_enable_11() { return &___enable_11; }
	inline void set_enable_11(FsmBool_t664485696 * value)
	{
		___enable_11 = value;
		Il2CppCodeGenWriteBarrier(&___enable_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
