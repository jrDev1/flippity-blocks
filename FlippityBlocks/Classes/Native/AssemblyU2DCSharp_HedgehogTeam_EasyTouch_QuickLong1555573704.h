﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickBase1253264426.h"
#include "AssemblyU2DCSharp_HedgehogTeam_EasyTouch_QuickLongT779214600.h"

// HedgehogTeam.EasyTouch.QuickLongTap/OnLongTap
struct OnLongTap_t1547982150;
// HedgehogTeam.EasyTouch.Gesture
struct Gesture_t367995397;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HedgehogTeam.EasyTouch.QuickLongTap
struct  QuickLongTap_t1555573704  : public QuickBase_t1253264426
{
public:
	// HedgehogTeam.EasyTouch.QuickLongTap/OnLongTap HedgehogTeam.EasyTouch.QuickLongTap::onLongTap
	OnLongTap_t1547982150 * ___onLongTap_19;
	// HedgehogTeam.EasyTouch.QuickLongTap/ActionTriggering HedgehogTeam.EasyTouch.QuickLongTap::actionTriggering
	int32_t ___actionTriggering_20;
	// HedgehogTeam.EasyTouch.Gesture HedgehogTeam.EasyTouch.QuickLongTap::currentGesture
	Gesture_t367995397 * ___currentGesture_21;

public:
	inline static int32_t get_offset_of_onLongTap_19() { return static_cast<int32_t>(offsetof(QuickLongTap_t1555573704, ___onLongTap_19)); }
	inline OnLongTap_t1547982150 * get_onLongTap_19() const { return ___onLongTap_19; }
	inline OnLongTap_t1547982150 ** get_address_of_onLongTap_19() { return &___onLongTap_19; }
	inline void set_onLongTap_19(OnLongTap_t1547982150 * value)
	{
		___onLongTap_19 = value;
		Il2CppCodeGenWriteBarrier(&___onLongTap_19, value);
	}

	inline static int32_t get_offset_of_actionTriggering_20() { return static_cast<int32_t>(offsetof(QuickLongTap_t1555573704, ___actionTriggering_20)); }
	inline int32_t get_actionTriggering_20() const { return ___actionTriggering_20; }
	inline int32_t* get_address_of_actionTriggering_20() { return &___actionTriggering_20; }
	inline void set_actionTriggering_20(int32_t value)
	{
		___actionTriggering_20 = value;
	}

	inline static int32_t get_offset_of_currentGesture_21() { return static_cast<int32_t>(offsetof(QuickLongTap_t1555573704, ___currentGesture_21)); }
	inline Gesture_t367995397 * get_currentGesture_21() const { return ___currentGesture_21; }
	inline Gesture_t367995397 ** get_address_of_currentGesture_21() { return &___currentGesture_21; }
	inline void set_currentGesture_21(Gesture_t367995397 * value)
	{
		___currentGesture_21 = value;
		Il2CppCodeGenWriteBarrier(&___currentGesture_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
