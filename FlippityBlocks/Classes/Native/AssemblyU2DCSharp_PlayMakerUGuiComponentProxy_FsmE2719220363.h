﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_PlayMakerUGuiComponentProxy_PlayM445442825.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;
// PlayMakerFSM
struct PlayMakerFSM_t437737208;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerUGuiComponentProxy/FsmEventSetup
struct  FsmEventSetup_t2719220363 
{
public:
	// PlayMakerUGuiComponentProxy/PlayMakerProxyEventTarget PlayMakerUGuiComponentProxy/FsmEventSetup::target
	int32_t ___target_0;
	// UnityEngine.GameObject PlayMakerUGuiComponentProxy/FsmEventSetup::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// PlayMakerFSM PlayMakerUGuiComponentProxy/FsmEventSetup::fsmComponent
	PlayMakerFSM_t437737208 * ___fsmComponent_2;
	// System.String PlayMakerUGuiComponentProxy/FsmEventSetup::customEventName
	String_t* ___customEventName_3;
	// System.String PlayMakerUGuiComponentProxy/FsmEventSetup::builtInEventName
	String_t* ___builtInEventName_4;
	// System.Boolean PlayMakerUGuiComponentProxy/FsmEventSetup::sendtoChildren
	bool ___sendtoChildren_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(FsmEventSetup_t2719220363, ___target_0)); }
	inline int32_t get_target_0() const { return ___target_0; }
	inline int32_t* get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(int32_t value)
	{
		___target_0 = value;
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(FsmEventSetup_t2719220363, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier(&___gameObject_1, value);
	}

	inline static int32_t get_offset_of_fsmComponent_2() { return static_cast<int32_t>(offsetof(FsmEventSetup_t2719220363, ___fsmComponent_2)); }
	inline PlayMakerFSM_t437737208 * get_fsmComponent_2() const { return ___fsmComponent_2; }
	inline PlayMakerFSM_t437737208 ** get_address_of_fsmComponent_2() { return &___fsmComponent_2; }
	inline void set_fsmComponent_2(PlayMakerFSM_t437737208 * value)
	{
		___fsmComponent_2 = value;
		Il2CppCodeGenWriteBarrier(&___fsmComponent_2, value);
	}

	inline static int32_t get_offset_of_customEventName_3() { return static_cast<int32_t>(offsetof(FsmEventSetup_t2719220363, ___customEventName_3)); }
	inline String_t* get_customEventName_3() const { return ___customEventName_3; }
	inline String_t** get_address_of_customEventName_3() { return &___customEventName_3; }
	inline void set_customEventName_3(String_t* value)
	{
		___customEventName_3 = value;
		Il2CppCodeGenWriteBarrier(&___customEventName_3, value);
	}

	inline static int32_t get_offset_of_builtInEventName_4() { return static_cast<int32_t>(offsetof(FsmEventSetup_t2719220363, ___builtInEventName_4)); }
	inline String_t* get_builtInEventName_4() const { return ___builtInEventName_4; }
	inline String_t** get_address_of_builtInEventName_4() { return &___builtInEventName_4; }
	inline void set_builtInEventName_4(String_t* value)
	{
		___builtInEventName_4 = value;
		Il2CppCodeGenWriteBarrier(&___builtInEventName_4, value);
	}

	inline static int32_t get_offset_of_sendtoChildren_5() { return static_cast<int32_t>(offsetof(FsmEventSetup_t2719220363, ___sendtoChildren_5)); }
	inline bool get_sendtoChildren_5() const { return ___sendtoChildren_5; }
	inline bool* get_address_of_sendtoChildren_5() { return &___sendtoChildren_5; }
	inline void set_sendtoChildren_5(bool value)
	{
		___sendtoChildren_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PlayMakerUGuiComponentProxy/FsmEventSetup
struct FsmEventSetup_t2719220363_marshaled_pinvoke
{
	int32_t ___target_0;
	GameObject_t1756533147 * ___gameObject_1;
	PlayMakerFSM_t437737208 * ___fsmComponent_2;
	char* ___customEventName_3;
	char* ___builtInEventName_4;
	int32_t ___sendtoChildren_5;
};
// Native definition for COM marshalling of PlayMakerUGuiComponentProxy/FsmEventSetup
struct FsmEventSetup_t2719220363_marshaled_com
{
	int32_t ___target_0;
	GameObject_t1756533147 * ___gameObject_1;
	PlayMakerFSM_t437737208 * ___fsmComponent_2;
	Il2CppChar* ___customEventName_3;
	Il2CppChar* ___builtInEventName_4;
	int32_t ___sendtoChildren_5;
};
