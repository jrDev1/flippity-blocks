﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_PlayMakerCollectionProxy398511462.h"

// System.Collections.Hashtable
struct Hashtable_t909839986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayMakerHashTableProxy
struct  PlayMakerHashTableProxy_t3073922234  : public PlayMakerCollectionProxy_t398511462
{
public:
	// System.Collections.Hashtable PlayMakerHashTableProxy::_hashTable
	Hashtable_t909839986 * ____hashTable_35;
	// System.Collections.Hashtable PlayMakerHashTableProxy::_snapShot
	Hashtable_t909839986 * ____snapShot_36;

public:
	inline static int32_t get_offset_of__hashTable_35() { return static_cast<int32_t>(offsetof(PlayMakerHashTableProxy_t3073922234, ____hashTable_35)); }
	inline Hashtable_t909839986 * get__hashTable_35() const { return ____hashTable_35; }
	inline Hashtable_t909839986 ** get_address_of__hashTable_35() { return &____hashTable_35; }
	inline void set__hashTable_35(Hashtable_t909839986 * value)
	{
		____hashTable_35 = value;
		Il2CppCodeGenWriteBarrier(&____hashTable_35, value);
	}

	inline static int32_t get_offset_of__snapShot_36() { return static_cast<int32_t>(offsetof(PlayMakerHashTableProxy_t3073922234, ____snapShot_36)); }
	inline Hashtable_t909839986 * get__snapShot_36() const { return ____snapShot_36; }
	inline Hashtable_t909839986 ** get_address_of__snapShot_36() { return &____snapShot_36; }
	inline void set__snapShot_36(Hashtable_t909839986 * value)
	{
		____snapShot_36 = value;
		Il2CppCodeGenWriteBarrier(&____snapShot_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
