﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SRF_SRMonoBehaviour2352136145.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRF.UI.Unselectable
struct  Unselectable_t2595244145  : public SRMonoBehaviour_t2352136145
{
public:
	// System.Boolean SRF.UI.Unselectable::_suspectedSelected
	bool ____suspectedSelected_8;

public:
	inline static int32_t get_offset_of__suspectedSelected_8() { return static_cast<int32_t>(offsetof(Unselectable_t2595244145, ____suspectedSelected_8)); }
	inline bool get__suspectedSelected_8() const { return ____suspectedSelected_8; }
	inline bool* get_address_of__suspectedSelected_8() { return &____suspectedSelected_8; }
	inline void set__suspectedSelected_8(bool value)
	{
		____suspectedSelected_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
