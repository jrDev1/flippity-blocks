﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.TextMesh
struct TextMesh_t1641806576;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DragMe
struct  DragMe_t1948526048  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.TextMesh DragMe::textMesh
	TextMesh_t1641806576 * ___textMesh_2;
	// UnityEngine.Color DragMe::startColor
	Color_t2020392075  ___startColor_3;
	// UnityEngine.Vector3 DragMe::deltaPosition
	Vector3_t2243707580  ___deltaPosition_4;
	// System.Int32 DragMe::fingerIndex
	int32_t ___fingerIndex_5;

public:
	inline static int32_t get_offset_of_textMesh_2() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___textMesh_2)); }
	inline TextMesh_t1641806576 * get_textMesh_2() const { return ___textMesh_2; }
	inline TextMesh_t1641806576 ** get_address_of_textMesh_2() { return &___textMesh_2; }
	inline void set_textMesh_2(TextMesh_t1641806576 * value)
	{
		___textMesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_2, value);
	}

	inline static int32_t get_offset_of_startColor_3() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___startColor_3)); }
	inline Color_t2020392075  get_startColor_3() const { return ___startColor_3; }
	inline Color_t2020392075 * get_address_of_startColor_3() { return &___startColor_3; }
	inline void set_startColor_3(Color_t2020392075  value)
	{
		___startColor_3 = value;
	}

	inline static int32_t get_offset_of_deltaPosition_4() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___deltaPosition_4)); }
	inline Vector3_t2243707580  get_deltaPosition_4() const { return ___deltaPosition_4; }
	inline Vector3_t2243707580 * get_address_of_deltaPosition_4() { return &___deltaPosition_4; }
	inline void set_deltaPosition_4(Vector3_t2243707580  value)
	{
		___deltaPosition_4 = value;
	}

	inline static int32_t get_offset_of_fingerIndex_5() { return static_cast<int32_t>(offsetof(DragMe_t1948526048, ___fingerIndex_5)); }
	inline int32_t get_fingerIndex_5() const { return ___fingerIndex_5; }
	inline int32_t* get_address_of_fingerIndex_5() { return &___fingerIndex_5; }
	inline void set_fingerIndex_5(int32_t value)
	{
		___fingerIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
