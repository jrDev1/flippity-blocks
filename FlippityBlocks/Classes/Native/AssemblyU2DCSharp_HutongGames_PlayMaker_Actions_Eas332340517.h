﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmBool
struct FsmBool_t664485696;
// HutongGames.PlayMaker.FsmEvent
struct FsmEvent_t1258573736;
// EasyTouchObjectProxy
struct EasyTouchObjectProxy_t2542381986;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.EasyTouchIsOverUiElement
struct  EasyTouchIsOverUiElement_t332340517  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmBool HutongGames.PlayMaker.Actions.EasyTouchIsOverUiElement::isOverUIElement
	FsmBool_t664485696 * ___isOverUIElement_11;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EasyTouchIsOverUiElement::overEvent
	FsmEvent_t1258573736 * ___overEvent_12;
	// HutongGames.PlayMaker.FsmEvent HutongGames.PlayMaker.Actions.EasyTouchIsOverUiElement::notOverEvent
	FsmEvent_t1258573736 * ___notOverEvent_13;
	// EasyTouchObjectProxy HutongGames.PlayMaker.Actions.EasyTouchIsOverUiElement::proxy
	EasyTouchObjectProxy_t2542381986 * ___proxy_14;

public:
	inline static int32_t get_offset_of_isOverUIElement_11() { return static_cast<int32_t>(offsetof(EasyTouchIsOverUiElement_t332340517, ___isOverUIElement_11)); }
	inline FsmBool_t664485696 * get_isOverUIElement_11() const { return ___isOverUIElement_11; }
	inline FsmBool_t664485696 ** get_address_of_isOverUIElement_11() { return &___isOverUIElement_11; }
	inline void set_isOverUIElement_11(FsmBool_t664485696 * value)
	{
		___isOverUIElement_11 = value;
		Il2CppCodeGenWriteBarrier(&___isOverUIElement_11, value);
	}

	inline static int32_t get_offset_of_overEvent_12() { return static_cast<int32_t>(offsetof(EasyTouchIsOverUiElement_t332340517, ___overEvent_12)); }
	inline FsmEvent_t1258573736 * get_overEvent_12() const { return ___overEvent_12; }
	inline FsmEvent_t1258573736 ** get_address_of_overEvent_12() { return &___overEvent_12; }
	inline void set_overEvent_12(FsmEvent_t1258573736 * value)
	{
		___overEvent_12 = value;
		Il2CppCodeGenWriteBarrier(&___overEvent_12, value);
	}

	inline static int32_t get_offset_of_notOverEvent_13() { return static_cast<int32_t>(offsetof(EasyTouchIsOverUiElement_t332340517, ___notOverEvent_13)); }
	inline FsmEvent_t1258573736 * get_notOverEvent_13() const { return ___notOverEvent_13; }
	inline FsmEvent_t1258573736 ** get_address_of_notOverEvent_13() { return &___notOverEvent_13; }
	inline void set_notOverEvent_13(FsmEvent_t1258573736 * value)
	{
		___notOverEvent_13 = value;
		Il2CppCodeGenWriteBarrier(&___notOverEvent_13, value);
	}

	inline static int32_t get_offset_of_proxy_14() { return static_cast<int32_t>(offsetof(EasyTouchIsOverUiElement_t332340517, ___proxy_14)); }
	inline EasyTouchObjectProxy_t2542381986 * get_proxy_14() const { return ___proxy_14; }
	inline EasyTouchObjectProxy_t2542381986 ** get_address_of_proxy_14() { return &___proxy_14; }
	inline void set_proxy_14(EasyTouchObjectProxy_t2542381986 * value)
	{
		___proxy_14 = value;
		Il2CppCodeGenWriteBarrier(&___proxy_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
