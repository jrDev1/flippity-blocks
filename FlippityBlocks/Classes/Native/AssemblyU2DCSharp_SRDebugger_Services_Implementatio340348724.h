﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"

// SRDebugger.InfoEntry
struct InfoEntry_t2565792942;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SRDebugger.Services.Implementation.StandardSystemInformationService/<Add>c__AnonStorey0
struct  U3CAddU3Ec__AnonStorey0_t340348724  : public Il2CppObject
{
public:
	// SRDebugger.InfoEntry SRDebugger.Services.Implementation.StandardSystemInformationService/<Add>c__AnonStorey0::info
	InfoEntry_t2565792942 * ___info_0;

public:
	inline static int32_t get_offset_of_info_0() { return static_cast<int32_t>(offsetof(U3CAddU3Ec__AnonStorey0_t340348724, ___info_0)); }
	inline InfoEntry_t2565792942 * get_info_0() const { return ___info_0; }
	inline InfoEntry_t2565792942 ** get_address_of_info_0() { return &___info_0; }
	inline void set_info_0(InfoEntry_t2565792942 * value)
	{
		___info_0 = value;
		Il2CppCodeGenWriteBarrier(&___info_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
