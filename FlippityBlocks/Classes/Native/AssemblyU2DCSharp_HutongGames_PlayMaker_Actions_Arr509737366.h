﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "PlayMaker_HutongGames_PlayMaker_FsmStateAction2862378169.h"

// HutongGames.PlayMaker.FsmArray
struct FsmArray_t527459893;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HutongGames.PlayMaker.Actions.ArrayCopy
struct  ArrayCopy_t509737366  : public FsmStateAction_t2862378169
{
public:
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayCopy::array
	FsmArray_t527459893 * ___array_11;
	// HutongGames.PlayMaker.FsmArray HutongGames.PlayMaker.Actions.ArrayCopy::storeArray
	FsmArray_t527459893 * ___storeArray_12;

public:
	inline static int32_t get_offset_of_array_11() { return static_cast<int32_t>(offsetof(ArrayCopy_t509737366, ___array_11)); }
	inline FsmArray_t527459893 * get_array_11() const { return ___array_11; }
	inline FsmArray_t527459893 ** get_address_of_array_11() { return &___array_11; }
	inline void set_array_11(FsmArray_t527459893 * value)
	{
		___array_11 = value;
		Il2CppCodeGenWriteBarrier(&___array_11, value);
	}

	inline static int32_t get_offset_of_storeArray_12() { return static_cast<int32_t>(offsetof(ArrayCopy_t509737366, ___storeArray_12)); }
	inline FsmArray_t527459893 * get_storeArray_12() const { return ___storeArray_12; }
	inline FsmArray_t527459893 ** get_address_of_storeArray_12() { return &___storeArray_12; }
	inline void set_storeArray_12(FsmArray_t527459893 * value)
	{
		___storeArray_12 = value;
		Il2CppCodeGenWriteBarrier(&___storeArray_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
